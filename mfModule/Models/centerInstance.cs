﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace kmbi_core_master.coreMaster.Models
{

    public class centerInstance
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("referenceId")]
        public string referenceId { get; set; }

        [BsonElement("centerId")]
        public string centerId { get; set; }

        [BsonElement("branchId")]
        public string branchId { get; set; }

        [BsonElement("branchSlug")]
        public string branchSlug { get; set; }

        [BsonElement("unitId")]
        public string unitId { get; set; }

        [BsonElement("unitSlug")]
        public string unitSlug { get; set; }

        [BsonElement("officerId")]
        public string officerId { get; set; }

        [BsonElement("officerSlug")]
        public string officerSlug { get; set; }

        [BsonElement("address")]
        public string address { get; set; }

        [BsonElement("code")]
        public string code { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("loanCycle")]
        public int loanCycle { get; set; }

        [BsonElement("status")]
        public string status { get; set; }

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }
    }


    public class syncCenterInstance
    {
        [BsonId]
        public ObjectId Id { get; set; }

        [BsonElement("referenceId")]
        public string referenceId { get; set; }

        [BsonElement("centerId")]
        public string centerId { get; set; }

        [BsonElement("branchId")]
        public string branchId { get; set; }

        [BsonElement("branchSlug")]
        public string branchSlug { get; set; }

        [BsonElement("unitId")]
        public string unitId { get; set; }

        [BsonElement("unitSlug")]
        public string unitSlug { get; set; }

        [BsonElement("officerId")]
        public string officerId { get; set; }

        [BsonElement("officerSlug")]
        public string officerSlug { get; set; }

        [BsonElement("address")]
        public string address { get; set; }

        [BsonElement("code")]
        public string code { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("loanCycle")]
        public int loanCycle { get; set; }

        [BsonElement("status")]
        public string status { get; set; }

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }
    }


}
