﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Filters;
using Microsoft.AspNetCore.Cors;
using kmbi_core_master.spm.IRepository;
using kmbi_core_master.spm.Models;
using kmbi_core_master.coreMaster;
using System;
using kmbi_core_master_v2.helpers;
using System.Net.Http;
using System.Net;
using System.Net.Http.Headers;
using System.IO;
using CsvHelper;
using Newtonsoft.Json.Linq;
using System.Xml;
using Newtonsoft.Json;
using System.Data;
using System.Linq;
using ChoETL;
using OfficeOpenXml;
using kmbi_core_master.helpers.interfaces;
using OfficeOpenXml.Style;

namespace kmbi_core_master.spm.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/ppi")]
    [usersAuth]
    public class ppiController : Controller
    {
        private readonly ippiRepository _Repository;
        private readonly iusersRepository _logInfo;
        private string token;
        iupload _upload;
        public ppiController(ippiRepository Repositor, iusersRepository logInfo,iupload upload)
        {
            this._Repository = Repositor;
            _logInfo = logInfo;
            _upload = upload;
        }

        public void logger(string message)
        {
            //Insert Log//
            token = HttpContext.Request.Headers["token"];
            var lInfo = _logInfo.getLoginInfo(token);
            string log = message;
            global.createLog(lInfo.id, lInfo.fullname, lInfo.employment.branch, log);
            //Insert Log//
        }

        // GET: 
        [HttpGet]
        public IEnumerable<ppi> GetAll()
        {
            var c = _Repository.getAll();
            return c;

        }

        [HttpGet("loan/{loanSlug}")]
        public IEnumerable<ppi> GetByLoanSlug(string loanSlug)
        {
            var c = _Repository.getByLoanSlug(loanSlug);
            return c;
        }

        [HttpGet("{slug}")]
        public IActionResult getSlug(string slug)
        {

            var item = _Repository.getBySlug(slug);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        [HttpGet("client/{slug}")]
        public IActionResult getClientSlug(string slug)
        {

            var item = _Repository.getByClientSlug(slug);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }


        [HttpPost]
        [errorHandler]
        public IActionResult create([FromBody] ppi b)
        {

            _Repository.add(b);

            logger( "Insert new ppi for " + b.client.name);

            return Ok(new
            {
                type = "success",
                message = b.client.name + " created successfully.",
                slug = b.slug
            });

        }



        [HttpPut("{slug}")]
        public IActionResult slugupdate(string slug, [FromBody] ppi b)
        {
            var item = _Repository.getBySlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _Repository.update(slug, b);

           logger("Modified ppi for " + b.client.name);

            return Ok(new
            {
                type = "success",
                message = b.client.name + " updated successfully."
            });
        }




        [HttpDelete("{slug}")]
        public IActionResult slugdelete(string slug)
        {

            var item = _Repository.getBySlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.remove(slug);

            logger("Delete ppi for " + item.client.name);

            return Ok(new
            {
                type = "success",
                message = item.client.name + " deleted successfully."
            });
        }



        [HttpGet("ppi")]
        public IActionResult getByDateRange([FromQuery] DateTime from, [FromQuery] DateTime to,
            [FromQuery] string region,
            [FromQuery] string area,
            [FromQuery] string branch,
            [FromQuery] string unit,
            [FromQuery] string po,
            [FromQuery] string center)
        {
            var item = _Repository.getByDateRange(from, to, region, area, branch, unit, po, center);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }


        [HttpGet("{extractPPI}")]
        public void extractPPI()
        {

            var item = _Repository.extractPPI();
           
            using (var w = new ChoCSVWriter<ppiExtraction>("sample.csv").WithFirstLineHeader())
            {
                w.Write(item);
            }
        }

        [HttpGet("newPpi")]
        public IActionResult newGetByDateRange1([FromQuery] DateTime from, [FromQuery] DateTime to,
            [FromQuery] string region,
            [FromQuery] string area,
            [FromQuery] string branch,
            [FromQuery] string unit,
            [FromQuery] string po,
            [FromQuery] string center)
        {
            var item = _Repository.newGetByDateRange(from, to, region, area, branch, unit, po, center);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        [HttpGet("clientProfiling")]
        public IActionResult getAllClients([FromQuery] string branchSlug)
        {

            //if (item == null)
            //{
            //    return new JsonStringResult("[]");
            //}



            var item = _Repository.getAllClientsByBranch(branchSlug);
            var filename = "kmbidevreporeports/" + item.FirstOrDefault().branch + "_" + "sample.xlsx";
            OfficeOpenXml.ExcelPackage ep = new OfficeOpenXml.ExcelPackage();
            ExcelWorksheet ew = ep.Workbook.Worksheets.Add(item.FirstOrDefault().branch + "_" + "sample.xlsx");
            convertRepMiToSheet(item, ew);
            byte[] bytes = ep.GetAsByteArray();
            ep.Dispose();

            var link = _upload.upload(bytes, filename);
            // return Redirect(link);4
          

            return new ObjectResult(item);
        }

        private void convertRepMiToSheet(IEnumerable<ClientProfiling> rep, ExcelWorksheet ew)
        {


            ew.Cells["A1"].Value = "AREA";
            ew.Cells["B1"].Value = "BRANCH";
            ew.Cells["C1"].Value = "NAME";
            ew.Cells["D1"].Value = "LOANCYCLE";
            ew.Cells["E1"].Value = "AGE";
            ew.Cells["F1"].Value = "CIVIL STATUS";
            ew.Cells["G1"].Value = "NATURE OF BUSINESS";
            ew.Cells["H1"].Value = "CENTER CODE";




            var ctr = 2;

    

            foreach (var l in rep)
            {
                ew.Cells["A" + ctr].Value = l.area;
                ew.Cells["A" + ctr].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ew.Cells["B" + ctr].Value = l.branch;
                ew.Cells["B" + ctr].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ew.Cells["C" + ctr].Value = l.name;
                ew.Cells["C" + ctr].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                ew.Cells["D" + ctr].Value = l.cycle;
                ew.Cells["D" + ctr].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ew.Cells["E" + ctr].Value = l.age;
                ew.Cells["E" + ctr].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ew.Cells["F" + ctr].Value = l.civilStatus;
                ew.Cells["F" + ctr].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ew.Cells["G" + ctr].Value = l.business  ;
                ew.Cells["G" + ctr].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ew.Cells["H" + ctr].Value = l.code;
                ew.Cells["H" + ctr].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ctr++;
            }

            ew.Column(1).AutoFit();
            ew.Column(2).AutoFit();
            ew.Column(3).AutoFit();
            ew.Column(4).AutoFit();
            ew.Column(5).AutoFit();
            ew.Column(6).AutoFit();
            ew.Column(7).AutoFit();
            ew.Column(8).AutoFit();

        }

    }
}
