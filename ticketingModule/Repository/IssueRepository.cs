using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.ticketing.Models;
using kmbi_core_master.ticketing.IRepository;

namespace kmbi_core_master.ticketing.Repository {
  public class IssueRepository : dbCon, IIssueRepository
  {
    public IssueRepository(IOptions<dbSettings> settings) : base(settings) { }

    public IEnumerable<IssueFull> all()
    {
        var issues = _db.GetCollection<IssueFull>("issues")
          .Aggregate()
          .Lookup("branch", "branchSlug", "slug", "branch")
          .Lookup("users", "postedBy", "slug", "user")
          .Project<IssueFull>(new BsonDocument
          {
            {"id", 1},
            {"title", 1},
            {"slug", 1},
            {"branchSlug", 1},
            {"branch", 1},
            {"status", 1},
            {"type", 1},
            {"category", 1},
            {"comments", 1},
            {"seenBy", 1},
            {"file", 1},
            {"postedBy", 1},
            {"user", "$user[0].fullname"},
            {"createdAt", 1}
          })
          .ToListAsync()
          .Result;

        return issues;
    }

    public IssueFull find(String slug)
    {
      var query = Builders<IssueFull>.Filter.Eq(e => e.slug, slug);
      var issue = _db.GetCollection<IssueFull>("issues")
        .Aggregate()
        .Match(query)
        .Lookup("branch", "branchSlug", "slug", "branch")
        .Lookup("users", "postedBy", "slug", "user")
        .Unwind("user")
        .Project<IssueFull>(new BsonDocument
        {
          {"id", 1},
          {"title", 1},
          {"slug", 1},
          {"branchSlug", 1},
          {"branch", 1},
          {"status", 1},
          {"type", 1},
          {"category", 1},
          {"comments", 1},
          {"seenBy", 1},
          {"file", 1},
          {"postedBy", 1},
          {"user", "$user.fullname"},
          {"createdAt", 1}
        })
        .FirstOrDefault();

      for (var i = 0; i < issue.comments.Length; i++) {
        var employeeSlug = issue.comments[i].employerSlug;
        var employeeQuery = Builders<users>.Filter.Eq(e => e.slug, employeeSlug);
        var user = _db.GetCollection<users>("users").Find(employeeQuery).FirstOrDefault();
        issue.comments[i].employee = user.fullname;
      }

      return issue;
    }

    public Comment getCommentBySlug(String slug)
    {
      var filterEmployee = Builders<Issue>.Filter.Eq("comments.slug",slug);
      var inc = Builders<Issue>.Projection.Include("comments");
      Issue issue = _db.GetCollection<Issue>("issues").Find(filterEmployee).Project<Issue>(inc).FirstOrDefault();

      if(issue!=null)
      {
        return issue.comments[0];
      }
      else
      {
        return null;
      }
    }

    public void create(Issue issue)
    {
      issue.createdAt = DateTime.Now;
      for (var i = 0; i < issue.comments.Length; i++) {
        issue.comments[i].createdAt = DateTime.Now;
      }
      _db.GetCollection<Issue>("issues").InsertOneAsync(issue);
    }

    public void update(String slug, Issue issue)
    {
      issue.slug = slug;
      var query = Builders<Issue>.Filter.Eq(e => e.slug, slug);
      var update = Builders<Issue>.Update
        .Set("branchSlug", issue.branchSlug)
        .Set("title", issue.title)
        .Set("status", issue.status)
        .Set("type", issue.type)
        .Set("category", issue.category)
        .Set("comments", issue.comments)
        .Set("seenBy", issue.seenBy)
        .Set("file", issue.file)
        ;
      var d = _db.GetCollection<Issue>("issues").UpdateOneAsync(query, update);
    }
    
    public Boolean updateComment(String slug, Comment comment)
    {
      var filterContact = Builders<Issue>.Filter.Eq("comments.slug", slug);
      var filterCombine = Builders<Issue>.Filter.And(filterContact);

      var update = Builders<Issue>
        .Update
        .Set("comments.$.text", comment.text)
        .Set("comments.$.isEdited", comment.isEdited)
        .Set("comments.$.status", comment.status);
          
      var isSuccess = true;
      _db.GetCollection<Issue>("issues").UpdateOne(filterCombine, update); 
      
      if(isSuccess)
      {
        return true;
      }
      else
      {
        return false;
      }
    }

    public void delete(String slug)
    {
      var query = Builders<Issue>.Filter.Eq(e => e.slug, slug);
      var d = _db.GetCollection<Issue>("issues").DeleteOneAsync(query);
    }
  }
}