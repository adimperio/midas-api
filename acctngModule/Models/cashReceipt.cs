﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace kmbi_core_master.acctngModule.Models
{
    public class cashReceipt
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("slug")]
        public String slug { get; set; }

        [BsonElement("number")]
        public string number { get; set; }

        [BsonElement("dateDeposited")]
        public DateTime? dateDeposited { get; set; }

        [BsonElement("particulars")]
        public string particulars { get; set; }

        [BsonElement("accounts")]
        public crAccounts accounts { get; set; }
        
        [BsonElement("branch")]
        public crBranch branch { get; set; }

        [BsonElement("payor")]
        public crPayor payor { get; set; }

        [BsonElement("isCancelled")]
        public bool isCancelled { get; set; }

        [BsonElement("reason")]
        public string reason { get; set; }

        [BsonElement("status")]
        public string status { get; set; }

        [BsonElement("isClosed")]
        public bool isClosed { get; set; }

        [BsonElement("amount")]
        public crAmount amount { get; set; }

        [BsonElement("timestamps")]
        public crTimestamps timestamps { get; set; }
    }

    public class crAccounts
    {
        [BsonElement("credit")]
        public crCredit[] credit { get; set; }

        [BsonElement("debit")]
        public crDebit[] debit { get; set; }

    }

    public class crCredit
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("code")]
        public int code { get; set; }

        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("amount")]
        public double amount { get; set; }
    }

    public class crDebit
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("code")]
        public int code { get; set; }

        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("amount")]
        public double amount { get; set; }
    }

    public class crBranch
    {
        [BsonId]
        [BsonElement("id")]
        public string id { get; set; }

        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

    }

    public class crPayor
    {
        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("tin")]
        public string tin { get; set; }

        [BsonElement("address")]
        public string address { get; set; }

    }

    public class crAmount
    {
        [BsonElement("byNumbers")]
        public double byNumbers { get; set; }

        [BsonElement("byWords")]
        public string byWords { get; set; }
    }

    public class crTimestamps
    {
        [BsonElement("issuedAt")]
        public DateTime issuedAt { get; set; }

        [BsonElement("issuedBy")]
        public string issuedBy { get; set; }

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("createdBy")]
        public string createdBy { get; set; }

        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }

        [BsonElement("updatedBy")]
        public string updatedBy { get; set; }
    }

    //public class rfpCreditList
    //{
    //    [BsonId]
    //    [BsonRepresentation(BsonType.ObjectId)]
    //    public String Id { get; set; }

    //    [BsonElement("slug")]
    //    public string slug { get; set; }


    //    [BsonElement("accounts")]
    //    public rfpAccounts accounts { get; set; }

    //}

    //public class rfpDebitList
    //{
    //    [BsonId]
    //    [BsonRepresentation(BsonType.ObjectId)]
    //    public String Id { get; set; }

    //    [BsonElement("slug")]
    //    public string slug { get; set; }

    //    [BsonElement("accounts")]
    //    public rfpAccountsList accounts { get; set; }

    //}
}
