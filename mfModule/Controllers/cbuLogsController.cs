﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Filters;
using Microsoft.AspNetCore.Cors;
using System;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace kmbi_core_master.coreMaster.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/cbuLogs")]
    [usersAuth]
    public class cbuLogsController : Controller
    {
        private readonly icbuLogsRepository _Repository;
        private readonly iusersRepository _logInfo;
        private readonly iclientsRepository _clientrep;
        private string token;

        public cbuLogsController(icbuLogsRepository cbuLogsRepository, iusersRepository logInfo, iclientsRepository clientrep)
        {
            this._Repository = cbuLogsRepository;
            _logInfo = logInfo;
            _clientrep = clientrep;
        }

        public void logger(String message)
        {
            //Insert Log//
            token = HttpContext.Request.Headers["token"];
            var lInfo = _logInfo.getLoginInfo(token);
            string log = message;
            global.createLog(lInfo.id, lInfo.fullname, lInfo.employment.branch, log);
            //Insert Log//
        }

        // GET: 
        [HttpGet]
        public IEnumerable<cbuLogs> GetAll()
        {
            var c = _Repository.getAll();
            return c;

        }

        // GET id
        [HttpGet("{id:length(24)}")]
        public IActionResult Get(string id)
        {
            var item = _Repository.Get(id);
            if (item == null)
            {
               
                return Ok(new { });
            }

            return new ObjectResult(item);
        }

        
        [HttpGet("{slug}")]
        public IActionResult getSlug(string slug)
        {

            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return Ok(new { });
            }
           
            return new ObjectResult(item);
        }

        [HttpGet("clientId/{clientId}")]
        public IActionResult clientId(string clientId)
        {

            var item = _Repository.clientId(clientId);
            if (item == null)
            {
                return Ok(new { });
            }

            return new ObjectResult(item);
        }

        [HttpGet("logs")]
        public IActionResult clientIdDates([FromQuery]string clientId, [FromQuery]DateTime dateFrom, [FromQuery]DateTime dateTo)
        {
            var dt = dateTo.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
            var item = _Repository.clientIdDates(clientId, dateFrom, dt);
            if (item.Count() == 0)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        // GET check unique slug
        [HttpGet("checkSlug")]
        public IActionResult checkSlug([FromQuery]string slug)
        {

            var item = _Repository.checkSlug(slug);
            if (item == null)
            {
                return Ok(new
                {
                    available = 1
                });
            }

            return Ok(new
            {
                available = 0
            });
        }

        // GET by incremPrecentage
        [HttpGet("transactedAt")]
        public IActionResult incremPrecentage([FromQuery] DateTime from, [FromQuery] DateTime to, [FromQuery] string branchSlug)
        {

            var item = _Repository.incremPercentage(from, to, branchSlug);
            if (item == null)
            {

                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        // POST 
        [errorHandler]
        [HttpPost]
        public IActionResult createLoanTypes([FromBody] cbuLogs c)
        {
            
            _Repository.Add(c);

           
            logger("Insert new cbu log " + c.type);
           
            return Ok(new
            {
                type = "success",
                message = "CBU" + c.type + " created successfully.",
                slug = c.slug
            });

        }

        
        // PUT 
        [errorHandler]
        [HttpPut("{slug}")]
        public IActionResult Put(string slug, [FromBody] cbuLogs c)
        {
            var item = _Repository.checkSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.Update(slug, c);

           logger( "Modified cbu log " + c.type);

            return Ok(new
            {
                type = "success",
                message = "CBU" + c.type + " updated successfully."
            });
        }


        
        // DELETE

        [HttpDelete("{slug}")]
        public IActionResult delete(string slug)
        {

            var item = _Repository.checkSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.Remove(slug);

            logger( "Remove cbu log " + item.type);
           
            return Ok(new
            {
                type = "success",
                message = item.type + " deleted successfully.",
                slug = item.slug
            });
        }

        [HttpGet("firstEntry/{clientId}")]
        public IActionResult getFirstEntry(String clientId) {
            var result = _Repository.getFirstEntry(clientId);

            if(result!=null) {
                return Ok(result);
            } else {
                return Ok(new{});
            }
        }

        [HttpPut("firstEntry/{clientId}/{amount}")]
        public IActionResult updateFirstEntry(String clientId, Double amount) {
            var result = _Repository.updateFirstEntry(clientId,amount);

            if(result) {
                return Ok(new {
                    type = "success",
                    message = "CBU Logs First Entry update success"
                });
            } else {
                return Ok(new {
                    type = "failed",
                    message = "CBU Logs First Entry update failed"
                });
            }            
        }

        class logsPerBranchPerDateRange {
            public string Id {get;set;}
            public string type {get;set;}
            public string slug {get;set;}
            public string clientId {get;set;}
            public double amount {get;set;}
            public double cbuBalance {get;set;}
            public cbuLoan loan {get;set;}
            public DateTime? transactedAt {get;set;}
            public DateTime? createdAt {get;set;}
            public DateTime? updatedAt {get;set;}
            public string name {get;set;}
        }

        [HttpGet("logsPerBranchPerDateRange/{branchSlug}")]
        public IActionResult getCbuLogsPerBranchPerDateRange(String branchSlug, [FromQuery]DateTime from, [FromQuery]DateTime to)
        {
            var result = _Repository.getCbuLogsPerBranchPerDateRange(branchSlug,from, to);
            var clients = _clientrep.getClients(result.Select(x=>x.clientId).ToList());

            if(result!=null) {

                var items = new List<logsPerBranchPerDateRange>();
                result.ToList().ForEach(x=>{
                    items.Add(
                        new logsPerBranchPerDateRange {
                            Id = x.Id,
                            type = x.type,
                            slug = x.slug,
                            clientId = x.clientId,
                            amount = x.amount,
                            cbuBalance = x.cbuBalance,
                            loan = x.loan,
                            transactedAt = x.transactedAt,
                            createdAt = x.createdAt,
                            updatedAt = x.updatedAt,
                            name = (clients.Where(cl=>cl.Id == x.clientId).FirstOrDefault() != null) ? clients.Where(cl=>cl.Id == x.clientId).Select(z=>z.name.last + ", " + z.name.first + " " + z.name.middle).First() : ""
                        }
                    );
                });
                return Ok(items.OrderBy(x=>x.name).OrderByDescending(x=>x.createdAt));
            } else {
                return Ok(new  Object[]{});
            }
        }

        [HttpPost("fixBegCbu")]
        public IActionResult fixBegCbu()
        {
            
            _Repository.fixBegCbu();
            return Ok(new {
                type = "success",
                message = "Fixed"
            });
        }

        [HttpPut("updatecbubalance")]
        public IActionResult UpdateCbuBalance([FromBody]cbuBalanceModel request) {
            _Repository.UpdateCbuBalance(request.clientId, request.amount);
            return Ok(new {
                type = "success",
                message = "cbu balance updated"
            });
        }

        [HttpPut("updatecbubalancebyslug")]
        public IActionResult UpdateCbuBalanceBySlug([FromBody]cbuBalanceModelBySlug request) {
            _Repository.UpdateCbuBalanceBySlug(request.clientslug, request.amount);
            return Ok(new {
                type = "success",
                message = "cbu balance updated"
            });
        }

        public class cbuBalanceModel {
            public string clientId {get;set;}
            public double amount {get;set;}
        }

        public class cbuBalanceModelBySlug {
            public string clientslug {get;set;}
            public double amount {get;set;}
        }

        public class loanCycleModelBySlug {
            public string clientslug {get;set;}
            public int cycle {get;set;}
        }

        [HttpPut("updateloancyclebyslug")]
        public IActionResult UpdateLoanCycleBySlug([FromBody]loanCycleModelBySlug request) {
            _Repository.UpdateLoanCycleBySlug(request.clientslug, request.cycle);
            return Ok(new {
                type = "success",
                message = "loan cycle updated"
            });
        }

    }

    
}
