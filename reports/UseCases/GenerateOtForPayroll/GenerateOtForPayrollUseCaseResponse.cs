namespace UseCases.GenerateOtForPayroll
{
    public class GenerateOtForPayrollUseCaseResponse
    {
        public OtItem[] items {get;set;}
        public OtItemDet[] dets {get;set;}
    }
}