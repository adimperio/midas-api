﻿using System;

namespace kmbi_core_master.coreMaster.Models
{
    public class biometricSettings
    {
        public string server {get;set;}
        public string db {get;set;}
        public string uid {get;set;}
        public string pass {get;set;}
    }

}
