using System;
namespace kmbi_core_master.helpers.interfaces
{
    public interface iReport
    {
        String templatePath {get;set;}
        String templateFilename {get;set;}
        String orientation {get;set;}
        Boolean isHalf {get;set;}
        String footerText {get;set;}
        String title {get;set;}
        String pageType {get;set;}
        System.Dynamic.ExpandoObject viewBag {get;set;}
        
        Object model{get;set;}
        iResult generatePDF();
    }
}