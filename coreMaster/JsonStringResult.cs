﻿using Microsoft.AspNetCore.Mvc;

namespace kmbi_core_master.coreMaster
{
    public class JsonStringResult : ContentResult
    {
        public JsonStringResult(string json)
        {
            Content = json;
            ContentType = "application/json";
        }
    }
}
