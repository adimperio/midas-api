﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.coreMaster.Models
{
    public class upLoadResult
    {
        
        public create create { get; set; }
        public update update { get; set; }

        //[BsonElement("delete")]
        public deletions delete { get; set; }

    }

    public class create
    {
        public List<cbuLogs> cbuLogs { get; set; }

        //[BsonElement("centerInstance")]
        public List<centerInstance> centerInstance { get; set; }

        //[BsonElement("checkVoucher")]
        public List<checkVoucher> checkVoucher { get; set; }

        //[BsonElement("clients")]
        public List<clients> clients { get; set; }

        //[BsonElement("loans")]
        public List<loans> loans { get; set; }

        //[BsonElement("officers")]
        public List<officers> officers { get; set; }

        //[BsonElement("paymentLoans")]
        public List<paymentLoans> paymentLoans { get; set; }

        //[BsonElement("paymentUpfront")]
        public List<paymentUpfront> paymentUpfront { get; set; }
    }

    public class update
    {
      
        public List<cbuLogs> cbuLogs { get; set; }

     
        public List<centerInstance> centerInstance { get; set; }

    
        public List<checkVoucher> checkVoucher { get; set; }

      
        public List<clients> clients { get; set; }
  
        public List<loans> loans { get; set; }

       
        public List<officers> officers { get; set; }

       
        public List<paymentLoans> paymentLoans { get; set; }

       
        public List<paymentUpfront> paymentUpfront { get; set; }
    }
}
