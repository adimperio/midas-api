﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Filters;
using kmbi_core_master.hrModule.Interface;
using kmbi_core_master.hrModule.Models;
using Microsoft.AspNetCore.Cors;
using System;
using kmbi_core_master.coreMaster.Models;

namespace kmbi_core_master.coreMaster.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/payrollDeductions")]
    [usersAuth]
    public class payrollDeductionController : Controller
    {
        private readonly ipayrollDeductionRepository _Repository;

        public payrollDeductionController(ipayrollDeductionRepository Repositor)
        {
            this._Repository = Repositor;
        }

        private void logger(String message)
        {
            //global.log(message, 1, "link here");
        }

        [HttpGet]
        public IActionResult getPayrollDeductions()
        {
            var list = _Repository.getPayrollDeductions();
            if(list!=null) {
                return Ok(list);
            } else {
                return Ok(new List<String>());
            }
            
        }

        [HttpGet("{slug}")]
        public IActionResult getPayrollDeduction(String slug)
        {
            payrollDeduction pd = _Repository.getPayrollDeduction(slug);
            if (pd != null)
            {
                return Ok(pd);
            }
            else
            {
                return Ok(new { });
            }
        }

        [HttpPost]
        public IActionResult insertPayrollDeduction([FromBody]payrollDeduction pd)
        {
            if (pd != null)
            {
                _Repository.insertPayrollDeduction(pd);
                logger("Payroll Deduction with slug " + pd.slug + " created successfully");
                return Ok(new
                {
                    type = "success",
                    message = "Payroll Deduction created successfully"
                });
            }
            else
            {
                return BadRequest(new
                {
                    type = "failed",
                    message = "Invalid data"
                });
            }

        }

        [HttpPut("{slug}")]
        public IActionResult updatePayrollDeduction(String slug, [FromBody]payrollDeduction pd)
        {
            if (pd != null)
            {
                _Repository.updatePayrollDeduction(slug, pd);
                logger("Payroll Deduction with slug " + pd.slug + " updated successfully");
                return Ok(new
                {
                    type = "success",
                    message = "Payroll Deduction updated successfully"
                });
            } else {
                return BadRequest(new
                {
                    type = "failed",
                    message = "Invalid data"
                });
            }

        }

        [HttpDelete("{slug}")]
        public IActionResult deletePayrollDeduction(String slug)
        {
            _Repository.deletePayrollDeduction(slug);
            logger("Payroll Deduction with slug " + slug + " deleted successfully");
            return Ok(new
            {
                type = "success",
                message = "Payroll Deduction deleted successfully"
            });
        }

        [HttpGet("loanTypes/{pdSlug}/{ltSlug}")]
        public IActionResult getLoanType(String pdSlug, String ltSlug)
        {
            var item = _Repository.getLoanType(pdSlug, ltSlug);
            if(item!=null) {
                return Ok(item);
            } else {
                return Ok(new {});
            }
        }

        [HttpGet("loanTypes/{pdSlug}")]
        public IActionResult getLoanTypes(String pdSlug)
        {
            var list = _Repository.getLoanTypes(pdSlug);
            if(list!=null) {
                return Ok(list);
            } else {
                return Ok(new List<String>());
            }
        }

        [HttpPost("loanTypes/{pdSlug}")]
        public IActionResult insertLoanType(String pdSlug, [FromBody]payrollDeductionLoanType lt)
        {
            if(lt!=null) {
                _Repository.insertLoanType(pdSlug, lt);
                logger("Loan type with slug " + lt.slug + " created successfully");
                return Ok(new
                {
                    type = "success",
                    message = "Loan type created successfully"
                });
            } else {
                 return BadRequest(new
                {
                    type = "failed",
                    message = "Invalid data"
                });
            }
        }

        [HttpPut("loanTypes/{pdSlug}/{ltSlug}")]
        public IActionResult updateLoanType(String pdSlug, String ltSlug, [FromBody]payrollDeductionLoanType lt)
        {
            if(lt!=null) {
                _Repository.updateLoanType(pdSlug, ltSlug, lt);
                logger("Loan type with slug " + lt.slug + " updated successfully");
                return Ok(new
                {
                    type = "success",
                    message = "Loan type updated successfully"
                });
             } else {
                 return BadRequest(new
                {
                    type = "failed",
                    message = "Invalid data"
                });
            }
        }

        [HttpDelete("loanTypes/{pdSlug}/{ltSlug}")]
        public IActionResult deleteLoanType(String pdSlug, String ltSlug)
        {
            _Repository.deleteLoanType(pdSlug, ltSlug);
            logger("Loan type with slug " + ltSlug + " deleted successfully");
            return Ok(new
            {
                type = "success",
                message = "Loan type deleted successfully"
            });
        }

        [HttpGet("{employeeCode}/list")]
        public IActionResult getPayrollDeductionsByEmployeeCode(String employeeCode) {
            var pds = _Repository.getPayrollDeductionsByEmployeeCode(employeeCode);
            if (pds != null)
            {
                return Ok(pds);
            }
            else
            {
                return Ok(new {});
            }
        }


    }
}
