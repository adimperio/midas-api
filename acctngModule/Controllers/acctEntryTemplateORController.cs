﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Filters;
using Microsoft.AspNetCore.Cors;
using kmbi_core_master.acctngModule.Interface;
using kmbi_core_master.acctngModule.Models;
using kmbi_core_master.coreMaster;
using System;
using kmbi_core_master.coreMaster.IRepository;

namespace kmbi_core_master.acctngModule.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/acctEntryTemplateOR")]
    [usersAuth]
    public class acctEntryTemplateORController : Controller
    {
        private readonly iacctEntryTemplateORRepository _Repository;
        private readonly iusersRepository _logInfo;
        private string token;

        public acctEntryTemplateORController(iacctEntryTemplateORRepository Repositor, iusersRepository logInfo)
        {
            this._Repository = Repositor;
            _logInfo = logInfo;
        }

        public void logger(string message)
        {
            //Insert Log//
            token = HttpContext.Request.Headers["token"];
            var lInfo = _logInfo.getLoginInfo(token);
            string log = message;
            global.createLog(lInfo.id, lInfo.fullname, lInfo.employment.branch, log);
            //Insert Log//
        }

        // GET: 
        [HttpGet]
        public IEnumerable<acctEntryTemplateOR> GetAll()
        {
            var c = _Repository.all();
            return c;

        }

        //GET  Slug
        [HttpGet("{slug}")]
        public IActionResult getSlug(string slug)
        {

            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        
        [HttpPost]
        public IActionResult create([FromBody] acctEntryTemplateOR b)
        {

            _Repository.add(b);

            logger("Insert new OR Template " + b.slug);

            return Ok(new
            {
                type = "success",
                message = b.particulars + " created successfully.",
                slug = b.slug
            });

        }

        
        [HttpPut("{slug}")]
        public IActionResult update(string slug, [FromBody] acctEntryTemplateOR b)
        {
            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _Repository.update(slug, b);

            logger("Modified OR Template " + b.slug);

            return Ok(new
            {
                type = "success",
                message = b.particulars + " updated successfully."
            });
        }

        
        [HttpDelete("{slug}")]
        public IActionResult delete(string slug)
        {

            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.remove(slug);

            logger("Delete OR Template " + item.slug);

            return Ok(new
            {
                type = "success",
                message = item.particulars + " deleted successfully."
            });
        }

        
    }
}
