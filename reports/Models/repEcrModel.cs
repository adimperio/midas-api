using System;
using System.Collections.Generic;
using kmbi_core_master.coreMaster.Models;

namespace kmbi_core_master.reports.Models
{
    public class repEcrModel
    {
        public String centerDescription {get;set;}
        public String centerAddress {get;set;}
        public String assignedPA {get;set;}
        public Int32 cycleNo {get;set;}

        public DateTime loanDate {get;set;}
        public DateTime maturityDate {get;set;}

        public Double loanAmount {get;set;}
        public Double interestRate {get;set;}
        public String interestType {get;set;}
        public Int32 term {get;set;}
        public String transactionNo {get;set;}
        
        public List<repEcrLine> ecrLines {get;set;}    

        public centerInstance center { get; set; }

        public String preparedBy {get;set;}
        public String checkedBy {get;set;}
        public String approvedBy {get;set;}

        public DateTime paymentSchedule {get;set;}
        public Int32 weekNo {get;set;}
    }

    public class repEcrLine
    {
        public Int32 weekNo {get;set;}
        public DateTime weeklyRepaymentDate {get;set;}
        public String memberId {get;set;}
        public String nameOfClient {get;set;}
        
        public Double balancePrincipal {get;set;}
        public Double balanceInterest {get;set;}
        public Double balanceCBU {get;set;}

        public Double currentAmountDuePrincipal {get;set;}
        public Double currentAmountDueInterest {get;set;}
        public Double currentAmountDueCBU {get;set;}

        public Double paymentPrincipal {get;set;}
        public Double paymentInterest {get;set;}
        public Double paymentCBU {get;set;}

        public List<repEcrPayment> payments {get;set;}

        public Double pastDuePrincipal {get;set;}
        public Double pastDueInterest {get;set;}
        public Double pastDueCBU {get;set;}

        public Double amountDueLoan {get;set;}
        public Double amountDueCBU {get;set;}

        public Double actualCollectionDueLoan {get;set;}
        public Double actualCollectionDueCBU {get;set;}

        public String lagda {get;set;}
        public Double loanCyle {get;set;}
    }

    public class repEcrPayment
    {
        public DateTime orDate {get;set;}
        public String orNo {get;set;}
        public Double principal {get;set;}
        public Double interest {get;set;}
        public Double cbu {get;set;}

        public Double orprincipal {get;set;}
        public Double orinterest {get;set;}
        public Double orcbu {get;set;}

        public int weekNo {get;set;}
    }
}