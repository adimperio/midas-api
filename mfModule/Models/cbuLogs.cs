﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.coreMaster.Models
{
    public class cbuLogs
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }
       
        [BsonElement("type")]
        public string type { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("clientId")]
        public string clientId { get; set; }

        [BsonElement("amount")]
        public double amount { get; set; }

        [BsonElement("cbuBalance")]
        public double cbuBalance { get; set; }

        [BsonElement("loan")]
        public cbuLoan loan { get; set; }

        [BsonElement("transactedAt")]
        public DateTime? transactedAt { get; set; }

        [BsonElement("createdAt")]
        public DateTime? createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime? updatedAt { get; set; }
    }

    public class cbuLoan
    {
        [BsonElement("_id")]
        public string id { get; set; }

        [BsonElement("termNo")]
        public int? termNo { get; set; }
    }


    public class syncCbuLogs
    {
        [BsonId]
        //[BsonRepresentation(BsonType.ObjectId)]
        public ObjectId Id { get; set; }

        [BsonElement("type")]
        public string type { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("clientId")]
        public string clientId { get; set; }

        [BsonElement("amount")]
        public double amount { get; set; }

        [BsonElement("cbuBalance")]
        public double cbuBalance { get; set; }

        [BsonElement("loan")]
        public cbuLoan loan { get; set; }

        [BsonElement("transactedAt")]
        public DateTime? transactedAt { get; set; }

        [BsonElement("createdAt")]
        public DateTime? createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime? updatedAt { get; set; }
    }

    public class cbuLogsBytrasactedAt
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("type")]
        public string type { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("clientId")]
        public string clientId { get; set; }

        [BsonElement("amount")]
        public double amount { get; set; }

        [BsonElement("cbuBalance")]
        public double cbuBalance { get; set; }

        [BsonElement("loan")]
        public cbuLoanBytrasactedAt loan { get; set; }

        [BsonElement("transactedAt")]
        public DateTime? transactedAt { get; set; }

        [BsonElement("createdAt")]
        public DateTime? createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime? updatedAt { get; set; }
    }

    public class cbuLoanBytrasactedAt
    {
        [BsonElement("id")]
        public string id { get; set; }

        [BsonElement("termNo")]
        public int? termNo { get; set; }

        [BsonElement("loanType")]
        public string loanType { get; set; }

        [BsonElement("centerName")]
        public string centerName { get; set; }

        [BsonElement("noClients")]
        public int noClients { get; set; }
    }
}
