using System;
using kmbi_core_master.coreMaster.Models;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using kmbi_core_master.acctngModule.Models;

namespace kmbi_core_master.reports.Models
{

    public class repBalanceSheet {
        public DateTime asOf {get;set;}
        public Int32 currentYear {get;set;}
        public Int32 previousYear {get;set;}
        public repBalanceSheetAssets assets {get;set;}
        public repBalanceSheetliabilitiesAndFundBalance liabilitiesAndFundBalance {get;set;}
        public repIncomeStatementLineItem totals {get;set;}  
        public List<transactionDetailedIs> details {get;set;}
    }

    public class repBalanceSheetAssets {
        public repBalanceSheetCurrentAssets currentAssets {get;set;}
        public repBalanceSheetNonCurrentAssets nonCurrentAssets {get;set;}
        public repIncomeStatementLineItem totalAssets {get;set;}
    }

    public class repBalanceSheetCurrentAssets {
        public repIncomeStatementLineItem cashAndCashEquivalents {get;set;}
        public repIncomeStatementLineItem marketableEquitySecuritiesNet {get;set;}
        public repIncomeStatementLineItem loansReceivableNet {get;set;}
        public repIncomeStatementLineItem interestReceivable {get;set;}
        public repIncomeStatementLineItem otherReceivablesNet {get;set;}
        public repIncomeStatementLineItem otherCurrentAssets {get;set;}
        public repIncomeStatementLineItem totalCurrentAssets {get;set;}
    }

    public class repBalanceSheetNonCurrentAssets {
        public repIncomeStatementLineItem availableForSaleAfsFinancialAsset {get;set;}
        public repIncomeStatementLineItem restrictedCash {get;set;}
        public repIncomeStatementLineItem propertyAndEquipment {get;set;}
        public repIncomeStatementLineItem advancesToAffiliates {get;set;}
        public repIncomeStatementLineItem otherNoncurrentAssets {get;set;}
        public repIncomeStatementLineItem totalNonCurrentAssets {get;set;}
    }

    public class repBalanceSheetliabilitiesAndFundBalance {
        public repBalanceSheetCurrentLiabilities currentLiabilities {get;set;}
        public repBalanceSheetNonCurrentLiabilities nonCurrentLiabilities {get;set;}
        public repIncomeStatementLineItem totalLiabilities {get;set;}
        public repBalanceSheetFundBalance fundBalance {get;set;}
        public repIncomeStatementLineItem totalLiabilitiesAndFundBalance {get;set;}
    }

    public class repBalanceSheetCurrentLiabilities {
        public repIncomeStatementLineItem tradeAndOtherPayables {get;set;}
        public repIncomeStatementLineItem clientsCbu {get;set;}
        public repIncomeStatementLineItem taxPayable {get;set;}
        public repIncomeStatementLineItem miCgliPayables {get;set;}
        public repIncomeStatementLineItem unearnedInterestIncome {get;set;}
        public repIncomeStatementLineItem provisionForContingencies {get;set;}
        public repIncomeStatementLineItem totalCurrentLiabilities {get;set;}
    }

    public class repBalanceSheetNonCurrentLiabilities {
        public repIncomeStatementLineItem deferredTaxLiability {get;set;}
        public repIncomeStatementLineItem retirementBenefitLiability {get;set;}
        public repIncomeStatementLineItem totalNonCurrentLiabilities {get;set;}
    }

    public class repBalanceSheetFundBalance {
        public repIncomeStatementLineItem retainedEarnings {get;set;}
        public repIncomeStatementLineItem cumulativeRemeasurementGainsOnRetirementBenefit {get;set;}
        public repIncomeStatementLineItem fairValueReserveOnAfsFinancialAsset {get;set;}
        public repIncomeStatementLineItem totalFundBalance {get;set;}

    }

    public class repBalanceSheetLineItem {
        public String name {get;set;}
        public String note {get;set;}
        public Decimal currentYear {get;set;}
        public Decimal previousYear {get;set;}
        public Decimal increaseDecrease {get;set;}
        public Decimal percentage {get;set;}
    }
}