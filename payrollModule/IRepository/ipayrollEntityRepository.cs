﻿using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.payrollModule.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.payrollModule.IRepository
{
    public interface ipayrollEntityRepository
    {
        IEnumerable<payrollEntity> getAll();

        payrollEntity Get(String id);

        payrollEntity getSlug(String slug);
               
        void Add(payrollEntity c);

        void Update(String slug, payrollEntity c);
              
        void Remove(String slug);

        payrollEntity checkSlug(String slug);

        IEnumerable<payrollEntity> search(string name);

    }
}
