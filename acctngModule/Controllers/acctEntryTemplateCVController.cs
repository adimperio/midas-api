﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Filters;
using Microsoft.AspNetCore.Cors;
using kmbi_core_master.acctngModule.Interface;
using kmbi_core_master.acctngModule.Models;
using kmbi_core_master.coreMaster;
using System;
using kmbi_core_master.coreMaster.IRepository;

namespace kmbi_core_master.acctngModule.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/acctEntryTemplateCV")]
    [usersAuth]
    public class acctEntryTemplateCVController : Controller
    {
        private readonly iacctEntryTemplateCVRepository _Repository;
        private readonly iusersRepository _logInfo;
        private string token;

        public acctEntryTemplateCVController(iacctEntryTemplateCVRepository Repositor, iusersRepository logInfo)
        {
            this._Repository = Repositor;
            _logInfo = logInfo;
        }

        public void logger(string message)
        {
            //Insert Log//
            token = HttpContext.Request.Headers["token"];
            var lInfo = _logInfo.getLoginInfo(token);
            string log = message;
            global.createLog(lInfo.id, lInfo.fullname, lInfo.employment.branch, log);
            //Insert Log//
        }

        // GET: 
        [HttpGet]
        public IEnumerable<acctEntryTemplateCV> GetAll()
        {
            var c = _Repository.all();
            return c;

        }

        //GET cv Slug
        [HttpGet("{slug}")]
        public IActionResult getSlug(string slug)
        {

            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }


        [HttpPost]
        public IActionResult create([FromBody] acctEntryTemplateCV b)
        {

            _Repository.add(b);
            logger("Insert CV Tenplate " + b.slug);
            return Ok(new
            {
                type = "success",
                message = b.particulars + " created successfully.",
                slug = b.slug
            });

        }


        [HttpPut("{slug}")]
        public IActionResult update(string slug, [FromBody] acctEntryTemplateCV b)
        {
            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _Repository.update(slug, b);
            logger("Update CV Tenplate " + b.slug);

            return Ok(new
            {
                type = "success",
                message = b.particulars + " updated successfully."
            });
        }


        [HttpDelete("{slug}")]
        public IActionResult delete(string slug)
        {

            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.remove(slug);
            logger("Delete CV Tenplate " + item.slug);

            return Ok(new
            {
                type = "success",
                message = item.particulars + " deleted successfully."
            });
        }

    } 
}
