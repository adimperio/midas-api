using System.Collections.Generic;
using Microsoft.Extensions.Options;
using System;
using MongoDB.Driver;
using MongoDB.Bson;
using kmbi_core_master.documentationModule.Models;

namespace kmbi_core_master.documentationModule.Service
{

    public class ticketService
    {
        context _context;
        activityService logger;

        public ticketService(IOptions<kmbi_core_master.coreMaster.Models.dbSettings> settings)
        {
            _context = new context(settings);
            logger = new activityService(settings);
        }

        public void log(String _log)
        {
            logger.log(_log);
        }

        public IEnumerable<ticket> GetTickets()
        {
            return _context.tickets.GetAll();
        }
        public ticket GetTicketBySlug(String slug)
        {
            return _context.tickets.GetOne(Builders<ticket>.Filter.Eq("slug",slug));
        }

        public message GetSpecificMessageByTicketSlugMessageSlug(String ticketSlug, String messageId)
        {
            var filterTicket = Builders<ticket>.Filter.Eq("slug",ticketSlug);
            var filterMessage = Builders<ticket>.Filter.Eq("messages.Id",messageId);
            var filterCombine = Builders<ticket>.Filter.And(filterTicket,filterMessage);

            var inc = Builders<ticket>.Projection.Include("messages.$");

            var ticket = _context.tickets.Find(filterCombine,inc);

            if (ticket!=null)
            {
                if (ticket.messages != null)
                {
                    if(ticket.messages.Count > 0)
                    {
                        return ticket.messages[0];
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }  
            }
            else
            {
                return null;
            }
        }

        public Boolean InsertTicket(ticket ticket)
        {
            foreach (message m in ticket.messages)
            {
                m.Id = ObjectId.GenerateNewId().ToString();
                m.createdAt = DateTime.Now;
                m.updatedAt = DateTime.Now;
            }
            
            ticket.createdAt = DateTime.Now;
            ticket.updatedAt = DateTime.Now;

            _context.tickets.Insert(ticket);

            ticket _tick = GetTicketBySlug(ticket.slug);

            if(_tick!=null)
            {
                log("Insert new ticket " + ticket.slug);
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean InsertMessage(String ticketSlug, message message)
        {
            var filterTicket = Builders<ticket>.Filter.Eq("slug",ticketSlug);
            var update = Builders<ticket>
                .Update
                .AddToSet("messages",new message{
                    Id = ObjectId.GenerateNewId().ToString(),
                    createdAt = DateTime.Now,
                    updatedAt = DateTime.Now
                })
                .CurrentDate("updatedAt");
            
            _context.tickets.Update(filterTicket,update);
            message mes = GetSpecificMessageByTicketSlugMessageSlug(ticketSlug, message.Id.ToString());

            if(mes!=null)
            {
                log("Insert new message " + message.Id.ToString());
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean UpdateTicket(String ticketSlug, ticket ticket)
        {
            var filterTicket = Builders<ticket>.Filter.Eq("slug",ticketSlug);
            var update = Builders<ticket>
                .Update
                .Set("subject",ticket.subject)
                .Set("category", ticket.category)
                .Set("status", ticket.status)
                .CurrentDate("updatedAt");

            var IsSuccess = _context.tickets.Update(filterTicket,update);
            if(IsSuccess)
            {
                log("Modified ticket " + ticket.slug);
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean UpdateMessage(String ticketSlug, String messageId, message message)
        {
            var filterTicket = Builders<ticket>.Filter.Eq("slug",ticketSlug);
            var filterMessage = Builders<ticket>.Filter.Eq("messages.Id", messageId);
            var filterCombine = Builders<ticket>.Filter.And(filterTicket, filterMessage);

            var update = Builders<ticket>
                .Update
                .Set("messages.$.content",message.content)
                .CurrentDate("updatedAt");
            
            var IsSuccess = _context.tickets.Update(filterCombine,update);
            if (IsSuccess)
            {
                log("Modified message " + messageId);
                return true;
            }
            else
            {
                return false;
            }

            
        }

        public Boolean DeleteTicket(String ticketSlug)
        {
            ticket tick = GetTicketBySlug(ticketSlug);
            var IsSucess = _context.tickets.Remove(Builders<ticket>.Filter.Eq("slug",ticketSlug));
            if(IsSucess)
            {
                log("Delete ticket " + tick.slug);
                return true;    
            }
            else
            {
                return false;
            }
        }

        public Boolean DeleteMessage(String ticketSlug, String messageId)
        {   
            var filterTicket = Builders<ticket>.Filter.Eq("slug",ticketSlug);
            var filterMessage = Builders<message>.Filter.Eq("Id",messageId);

            var update = Builders<ticket>
                .Update
                .CurrentDate("updatedAt")
                .PullFilter("messages",filterMessage);
            message mess = GetSpecificMessageByTicketSlugMessageSlug(ticketSlug, messageId);

            var IsSucess = _context.tickets.Update(filterTicket,update);
            if(IsSucess)
            {
                log("Delete message " + mess.Id);
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}