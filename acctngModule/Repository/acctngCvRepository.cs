﻿using Microsoft.Extensions.Options;

using System;
using System.Collections.Generic;
using System.Linq;

using MongoDB.Bson;
using MongoDB.Driver;

using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.acctngModule.Models;
using kmbi_core_master.acctngModule.Interface;

namespace kmbi_core_master.acctngModule.Repository
{
    public class acctngCvRepository : dbCon, iacctngCvRepository
    {
        
        public acctngCvRepository(IOptions<dbSettings> settings) : base(settings) {}

        public void add(acctngCv b)
        {

            b.timestamps.createdAt = DateTime.Now;
            b.timestamps.updatedAt = DateTime.Now;
            for (int i = 0; i <= b.accounts.credit.Count() - 1; i++)
            {
                b.accounts.credit[i].Id = ObjectId.GenerateNewId().ToString();
            }

            for (int i = 0; i <= b.accounts.debit.Count() - 1; i++)
            {
                b.accounts.debit[i].Id = ObjectId.GenerateNewId().ToString();
            }
            var coll = _db.GetCollection<acctngCv>("acctngCv");
            _db.GetCollection<acctngCv>("acctngCv").InsertOneAsync(b);
        }
        

        public IEnumerable<acctngCv> all()
        {
            var c = _db.GetCollection<acctngCv>("acctngCv").Find(new BsonDocument()).ToListAsync();
            return c.Result;
        }

        public IEnumerable<acctngCv> all(DateTime from, DateTime to)
        {
            var filterFrom = Builders<acctngCv>.Filter.Gte(cv=>cv.timestamps.issuedAt, from);
            var filterTo = Builders<acctngCv>.Filter.Lte(cv=>cv.timestamps.issuedAt, to);
            var c = _db.GetCollection<acctngCv>("acctngCv").Find(Builders<acctngCv>.Filter.And(filterFrom,filterTo)).ToListAsync();
            return c.Result;
        }

        public acctngCv getSlug(String slug)
        {
            var query = Builders<acctngCv>.Filter.Eq(e => e.slug, slug);
            var c = _db.GetCollection<acctngCv>("acctngCv").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }
       
        public void update(String slug, acctngCv p)
        {
            p.slug = slug;
            var query = Builders<acctngCv>.Filter.Eq(e => e.slug, slug);
            var update = Builders<acctngCv>.Update
                .Set("chequeNumber", p.chequeNumber)
                .Set("cvNumber", p.cvNumber)
                .Set("particulars", p.particulars)
                .Set("branch", p.branch)
                .Set("accounts", p.accounts)
                .Set("supplier", p.supplier)
                .Set("amount", p.amount)
                .Set("isCancelled", p.isCancelled)
                .Set("isClosed", p.isClosed)
                .Set("reason", p.reason)
                .Set("status", p.status)
                .Set("timestamps.issuedAt", p.timestamps.issuedAt)
                .Set("timestamps.issuedBy", p.timestamps.issuedBy)
                .Set("timestamps.createdBy", p.timestamps.createdBy)
                .Set("timestamps.updatedBy", p.timestamps.updatedBy)
                .CurrentDate("timestamps.updatedAt");

            var branch = _db.GetCollection<acctngCv>("acctngCv").UpdateOneAsync(query, update);

        }
        public void remove(String slug)
        {
            var query = Builders<acctngCv>.Filter.Eq(e => e.slug, slug);
            var result = _db.GetCollection<acctngCv>("acctngCv").DeleteOneAsync(query);
        }

        public List<acctngCv> issuedAt(DateTime from, DateTime to)
        {
            
            var fdate = Builders<acctngCv>.Filter.Gte(e => e.timestamps.issuedAt, from);
            var tdate = Builders<acctngCv>.Filter.Lte(e => e.timestamps.issuedAt, to);
            var sort = Builders<acctngCv>.Sort.Descending(e => e.timestamps.issuedAt);
            var query = Builders<acctngCv>.Filter.And(fdate, tdate);

            var c = _db.GetCollection<acctngCv>("acctngCv").Find(query).Sort(sort).ToListAsync();
            return c.Result;

        }

        //CREDIT
        public cvAccounts getCreditId(string creditId)
        {

            var query = Builders<cvAccounts>.Filter.ElemMatch(x => x.credit, x => x.Id == creditId);
            var fields = Builders<cvAccounts>.Projection
                .Include(u => u.credit)
                .ElemMatch(x => x.credit, x => x.Id == creditId);

            var unit = _db.GetCollection<cvAccounts>("acctngCv").Find(query).Project<cvAccounts>(fields).ToListAsync();
            return unit.Result.FirstOrDefault();
        }

        public void addCredit(cvCredit u, string crSlug)
        {
            var query = Builders<acctngCv>.Filter.Eq(e => e.slug, crSlug);
            var update = Builders<acctngCv>.Update.Push(e => e.accounts.credit, u);
            u.Id = ObjectId.GenerateNewId().ToString();
            _db.GetCollection<acctngCv>("acctngCv").FindOneAndUpdateAsync(query, update);
        }

        public void updateCredit(string crSlug, string creditId, cvCredit u)
        {

            var query = Builders<acctngCv>.Filter.And(Builders<acctngCv>.Filter.Eq(x => x.slug, crSlug),
            Builders<acctngCv>.Filter.ElemMatch(x => x.accounts.credit, x => x.Id == creditId));

            var update = Builders<acctngCv>.Update
                .Set("credit.$.code", u.code)
                .Set("credit.$.name", u.name)
                .Set("credit.$.department", u.department)
                .Set("credit.$.budget", u.budget)
                .Set("credit.$.amount", u.amount)
                .Set("credit.$.ewt", u.ewt)
                .Set("credit.$.rate", u.rate);
            var result = _db.GetCollection<acctngCv>("acctngCv").UpdateOneAsync(query, update);
        }

        public void removeCredit(String crSlug, string creditId)
        {
            var pull = Builders<acctngCv>.Update.PullFilter(x => x.accounts.credit, a => a.Id == creditId);

            var filter1 = Builders<acctngCv>.Filter.And(Builders<acctngCv>.Filter.Eq(a => a.slug, crSlug),
                          Builders<acctngCv>.Filter.ElemMatch(q => q.accounts.credit, t => t.Id == creditId));

            var result = _db.GetCollection<acctngCv>("acctngCv").UpdateOneAsync(filter1, pull);
        }


        //DEBIT
        public acctngCv getDebitId(string debitId)
        {

            var query = Builders<acctngCv>.Filter.ElemMatch(x => x.accounts.debit, x => x.Id == debitId);
            var fields = Builders<acctngCv>.Projection
                .Include(u => u.Id)
                .Include(u => u.slug)
                .ElemMatch(x => x.accounts.debit, x => x.Id == debitId);

            var unit = _db.GetCollection<acctngCv>("cashReceipt").Find(query).Project<acctngCv>(fields).ToListAsync();
            return unit.Result.FirstOrDefault();
        }

        public void addDebit(cvDebit u, string crSlug)
        {
            var query = Builders<acctngCv>.Filter.Eq(e => e.slug, crSlug);
            var update = Builders<acctngCv>.Update.Push(e => e.accounts.debit, u);
            u.Id = ObjectId.GenerateNewId().ToString();
            _db.GetCollection<acctngCv>("acctngCv").FindOneAndUpdateAsync(query, update);
        }

        public void updateDebit(string crSlug, string debitId, cvDebit u)
        {

            var query = Builders<acctngCv>.Filter.And(Builders<acctngCv>.Filter.Eq(x => x.slug, crSlug),
            Builders<acctngCv>.Filter.ElemMatch(x => x.accounts.debit, x => x.Id == debitId));

            var update = Builders<acctngCv>.Update
                .Set("debit.$.code", u.code)
                .Set("debit.$.name", u.name)
                .Set("debit.$.department", u.department)
                .Set("debit.$.budget", u.budget)
                .Set("debit.$.amount", u.amount)
                .Set("debit.$.ewt", u.ewt)
                .Set("debit.$.rate", u.rate);
            var result = _db.GetCollection<acctngCv>("acctngCv").UpdateOneAsync(query, update);
        }

        public void removeDebit(String crSlug, string debitId)
        {
            var pull = Builders<acctngCv>.Update.PullFilter(x => x.accounts.debit, a => a.Id == debitId);

            var filter1 = Builders<acctngCv>.Filter.And(Builders<acctngCv>.Filter.Eq(a => a.slug, crSlug),
                          Builders<acctngCv>.Filter.ElemMatch(q => q.accounts.debit, t => t.Id == debitId));

            var result = _db.GetCollection<acctngCv>("acctngCv").UpdateOneAsync(filter1, pull);
        }

        public acctngCv checkCv(String cvNo)
        {
            var query = Builders<acctngCv>.Filter.Eq(e => e.cvNumber, cvNo);
            var c = _db.GetCollection<acctngCv>("acctngCv").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }

        public acctngCv checkCheque(String chequeNo)
        {
            var query = Builders<acctngCv>.Filter.Eq(e => e.chequeNumber, chequeNo);
            var c = _db.GetCollection<acctngCv>("acctngCv").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }


        public void updateIsCancelled(String slug, acctngCv p)
        {
            p.slug = slug;
            var query = Builders<acctngCv>.Filter.Eq(e => e.slug, slug);
            var update = Builders<acctngCv>.Update
                .Set("isCancelled", true)
                .Set("reason", p.reason)
                .Set("timestamps.updatedBy", p.timestamps.updatedBy)
                .CurrentDate("timestamps.updatedAt");

            var branch = _db.GetCollection<acctngCv>("acctngCv").UpdateOneAsync(query, update);

        }

        public void updateIsClosed(DateTime from, DateTime to, acctngCv p)
        {
            var fdate = Builders<acctngCv>.Filter.Gte(e => e.timestamps.issuedAt, from);
            var tdate = Builders<acctngCv>.Filter.Lte(e => e.timestamps.issuedAt, to);
            //var c = _db.GetCollection<cashReceipt>("cashReceipt").Find(query).Sort(sort).ToListAsync();
            //return c.Result;
            var query = Builders<acctngCv>.Filter.And(fdate, tdate);
            var update = Builders<acctngCv>.Update
                .Set("isClosed", true)
                .Set("timestamps.updatedBy", p.timestamps.updatedBy)
                .CurrentDate("timestamps.updatedAt");

            var branch = _db.GetCollection<acctngCv>("acctngCv").UpdateOneAsync(query, update);

        }
    }
}
