﻿using Microsoft.Extensions.Options;

using System;
using System.Collections.Generic;
using System.Linq;

using MongoDB.Bson;
using MongoDB.Driver;

using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.acctngModule.Models;
using kmbi_core_master.acctngModule.Interface;

namespace kmbi_core_master.acctngModule.Repository
{
    public class coaRepository : dbCon, icoaRepository
    {
        
        public coaRepository(IOptions<dbSettings> settings) : base(settings) {}

        public void add(coa b)
        {

            b.createdAt = DateTime.Now;
            b.updatedAt = DateTime.Now;
            var coll = _db.GetCollection<coa>("coa");
            _db.GetCollection<coa>("coa").InsertOneAsync(b);
        }

    
        public IEnumerable<coa> all()
        {
            var c = _db.GetCollection<coa>("coa").Find(new BsonDocument()).ToListAsync();
            return c.Result;
        }

        public coa getSlug(String slug)
        {
            var query = Builders<coa>.Filter.Eq(e => e.slug, slug);
            var c = _db.GetCollection<coa>("coa").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }
       
        public void update(String slug, coa p)
        {
            p.slug = slug;
            var query = Builders<coa>.Filter.Eq(e => e.slug, slug);
            var update = Builders<coa>.Update
                .Set("code", p.code)    
                .Set("name", p.name)
                .Set("description", p.description)
                .Set("statement", p.statement)
                .Set("element", p.element)
                .Set("type", p.type)
                .Set("subsidiary", p.subsidiary)
                .CurrentDate("updatedAt");

            var branch = _db.GetCollection<coa>("coa").UpdateOneAsync(query, update);

        }
        public void remove(String slug)
        {
            var query = Builders<coa>.Filter.Eq(e => e.slug, slug);
            var result = _db.GetCollection<coa>("coa").DeleteOneAsync(query);
        }

        public coa checkParentCode(int code)
        {
            var query = Builders<coa>.Filter.Eq(e => e.code, code);
            var c = _db.GetCollection<coa>("coa").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }

        public coa checkSubCode(int code)
        {
            var query = Builders<coa>.Filter.Eq(e => e.subsidiary[0].code, code);
            var c = _db.GetCollection<coa>("coa").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }
    }
}
