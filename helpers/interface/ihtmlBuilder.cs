using System;
using System.Dynamic;

namespace kmbi_core_master.helpers.interfaces
{
    public interface ihtmlBuilder
    {
        String htmlBuild(String templatePath, String templateName, Object model, ExpandoObject viewBag = null);
    }
}




