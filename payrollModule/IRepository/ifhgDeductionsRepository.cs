﻿using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.payrollModule.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.payrollModule.IRepository
{
    public interface ifhgDeductionsRepository
    {
        fhgDeduction[] getAll();
        fhgDeduction[] getByCutOff(DateTime from, DateTime to);
        void insertAll(fhgDeduction[] deductions);
        void deleteAll(DateTime from, DateTime to);
    }

}
