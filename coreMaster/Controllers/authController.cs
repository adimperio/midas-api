﻿using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Filters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Cors;
using kmbi_core_master.spm.IRepository;
using System.Collections.Generic;
using kmbi_core_master.spm.Models;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace kmbi_core_master.coreMaster.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/auth")]
    //[exception]
    public class authController : Controller
    {
        private readonly iauthRepository _Repository;
        private readonly iusersRepository _userRepo;
        private readonly icenterInstanceRepository _cInstanceRepo;
        private readonly ibranchRepository _branchRepository;

        public iquestionairesRepository _questionairesRepository { get; }
        public ippiRepository _ppiRepository { get; }

        //private string token;

        public authController(iauthRepository authRepository, iusersRepository userRepo, icenterInstanceRepository cInstanceRepo, ibranchRepository branchRepositor, iquestionairesRepository questionairesRepository, ippiRepository ppiRepository)
        {
            _Repository = authRepository;
            _userRepo = userRepo;
            _cInstanceRepo = cInstanceRepo;
            _branchRepository = branchRepositor;
            _questionairesRepository = questionairesRepository;
            _ppiRepository = ppiRepository;
        }

        public void logger(string message, string token)
        {
            //Insert Log//
            //token = HttpContext.Request.Headers["token"];
            var lInfo = _userRepo.getLoginInfo(token);
            string log = message;
            global.createLog(lInfo.id, lInfo.fullname, lInfo.employment.branch, log);
            //Insert Log//
        }

        [errorHandler]
        [HttpPut("changeEmail")]
        public IActionResult updateEmail([FromBody] auth user)
        {
            var item = _Repository.getCredential(user.email);
            
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = user.email + " not found."
                });
            }

            if (user.email != user.oldEmail)
            {
               
                return BadRequest(new
                {
                    type = "error",
                    message = user.oldEmail +  "not found."
                });
            }
            _Repository.updateEmail(user.email, user);
            var token = item.token;
            logger("Changed Email ", token);
            return Ok(new
            {
                type = "success",
                message = user.newEmail + " updated successfully."
            });
        }

        [errorHandler]
        [HttpPut("changePassword")]
        public IActionResult updatePassword([FromBody] auth user)
        {
            var item = _Repository.getCredential(user.email);
            var pw = item.password;

            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = user.email + " not found."
                });
            }

            bool validPassword = BCrypt.Net.BCrypt.Verify(user.oldPassword, pw);
            if (!validPassword)
            {
                return Ok(new
                {
                    type = "error",
                    message = "Password does not match."
                });
            }
            _Repository.updatePassword(user.email, user);
            var token = item.token;
            logger("Changed Password ", token);
            return Ok(new
            {
                type = "success",
                message = "Password updated successfully."
            });
        }

        [HttpPut("email={email}&verificationToken={verToken}")]
        public IActionResult updateisVerified(string email, string verToken, [FromBody] auth user)
        {
            var item = _Repository.checkVerToken(email, verToken);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    title = "Verification failed",
                    message = "Please make sure you have an account or contact IT for support."
                });
            }

            if (item.isVerified == 1)
            {
                return Ok(new
                {
                    type = "info",
                    title = "You are already verified",
                    message = "You can login to the system."
                });
            } 

            _Repository.updateisVerified(email, verToken, user);
            return Ok(new
            {
                type = "success",
                title = "You are now verified",
                message = "Your account has been verified. You can now login to the system."
            });
        }

        [HttpPost("checkPassword")]
        public IActionResult checkPassword([FromBody] users user)
        {
            var item = _Repository.checkPassword(user.token);
            
            if (item == null)
            {
                return Ok(new
                {
                    valid = 0
                });
            }

            var usersPassword = item.password;
            var pw = user.password;
            bool validPassword = BCrypt.Net.BCrypt.Verify(pw, usersPassword);
            if (!validPassword)
            {
                return BadRequest(new
                {
                    valid = 0
                });
            }

            return Ok(new
            {
                valid = 1
            });
        }

        [HttpPost]
        public IActionResult login([FromBody] users user)
        {
           
            var item = _Repository.getCredential(user.email);


            if (user.email == "" || user.password == "")
            {
                return Ok(new
                {
                    type = "warning",
                    message = "E-mail address and password are required."
                });
            }

            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "E-mail/password combination does not match."
                });
            }
            
            var pw = user.password;
            var verify = user.isVerified;
            var active = item.isActive;
            var usersPassword = item.password;
            var usersEmail = item.email;

            if (verify == 0)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "User is not yet verified."
                });
            }
            else if (verify == 1)
            {
                if (active == 0)
                {
                    return BadRequest(new
                    {
                        type = "error",
                        message = "User is not active."
                    });
                }
            }

            bool validPassword = BCrypt.Net.BCrypt.Verify(pw, usersPassword);
            if (!validPassword)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "E-mail/password combination does not match."
                });
            }

            HttpContext.Session.SetString("userId", item.Id);
            HttpContext.Session.SetString("fullname", item.fullname);
            //global.userId = HttpContext.Session.GetString("userId");
            //global.fullname = HttpContext.Session.GetString("fullname");


            logger("Logged In ", item.token);

            users u = _userRepo.getBySlug(item.slug);
            users uis = _userRepo.getBySlug(u.immediateSupervisor);
            if(uis==null) {
                uis =new users {
                    Id="",
                    fullname="",
                    email = "",
                    employment = new employment {
                        position = ""
                    }
                };
            }
            
            return Ok(new
            {
                type = "success",
                id = item.Id,
                slug = item.slug,
                email = item.email,
                firstname = item.name.first,
                fullname = item.fullname,
                token = item.token,
                permissions = item.permissions,
                picture = item.picture,
                position = item.employment.position,
                addresses = item.addresses,
                contacts = item.contacts,
                branchSlug = item.employment.branch,
                immediateSupervisor = new {
                    id = uis.Id,
                    name =uis.fullname,
                    email = uis.email,
                    position = uis.employment.position
                },
                extension = item.extension,
                jobGrade = item.employment.jobGrade
            });
          
        }


        [HttpPost("mobileLogin")]
        public IActionResult mobileLogin([FromBody] users user)
        {

            var item = _Repository.getCredential(user.email);


            if (user.email == "" || user.password == "")
            {
                return Ok(new
                {
                    type = "warning",
                    message = "E-mail address and password are required."
                });
            }

            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "E-mail/password combination does not match."
                });
            }

            var pw = user.password;
            var verify = user.isVerified;
            var active = item.isActive;
            var usersPassword = item.password;
            var usersEmail = item.email;

            if (verify == 0)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "User is not yet verified."
                });
            }
            else if (verify == 1)
            {
                if (active == 0)
                {
                    return BadRequest(new
                    {
                        type = "error",
                        message = "User is not active."
                    });
                }
            }

            bool validPassword = BCrypt.Net.BCrypt.Verify(pw, usersPassword);
            if (!validPassword)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "E-mail/password combination does not match."
                });
            }
            else
            {
                var res = _cInstanceRepo.getByPOEmailAddress(user.email);
                var branch = _branchRepository.getBranchSlug(item.employment.branch);

                users u = _userRepo.getBySlug(item.slug);
                users uis = _userRepo.getBySlug(u.immediateSupervisor);
                if (uis == null)
                {
                    uis = new users
                    {
                        Id = "",
                        fullname = "",
                        email = "",
                        employment = new employment
                        {
                            position = ""
                        }
                    };
                }

                // get questionaires
                var questionaires = _questionairesRepository.getAll();
                if(questionaires==null) 
                    questionaires = new List<questionaires>();
                    
                // get ppis
                // var ppis = _ppiRepository.getAll();
                // if(ppis==null)
                //     ppis = new List<ppi>();

                return Ok(new
                {
                    type = "success",
                    id = item.Id,
                    slug = item.slug,
                    email = item.email,
                    firstname = item.name.first,
                    fullname = item.fullname,
                    token = item.token,
                    permissions = item.permissions,
                    picture = item.picture,
                    position = item.employment.position,
                    addresses = item.addresses,
                    contacts = item.contacts,
                    branchSlug = item.employment.branch,
                    immediateSupervisor = new
                    {
                        id = uis.Id,
                        name = uis.fullname,
                        email = uis.email,
                        position = uis.employment.position
                    },
                    extension = item.extension,
                    jobGrade = item.employment.jobGrade,
                    centerInstance = res,
                    branch = branch,
                    questionaires = questionaires,
                });
                    // ppis = ppis
            }

            //HttpContext.Session.SetString("userId", item.Id);
            //HttpContext.Session.SetString("fullname", item.fullname);
            ////global.userId = HttpContext.Session.GetString("userId");
            ////global.fullname = HttpContext.Session.GetString("fullname");


            //logger("Logged In ", item.token);

            //users u = _userRepo.getBySlug(item.slug);
            //users uis = _userRepo.getBySlug(u.immediateSupervisor);
            //if (uis == null)
            //{
            //    uis = new users
            //    {
            //        Id = "",
            //        fullname = "",
            //        email = "",
            //        employment = new employment
            //        {
            //            position = ""
            //        }
            //    };
            //}

            //return Ok(new
            //{
            //    type = "success",
            //    id = item.Id,
            //    slug = item.slug,
            //    email = item.email,
            //    firstname = item.name.first,
            //    fullname = item.fullname,
            //    token = item.token,
            //    permissions = item.permissions,
            //    picture = item.picture,
            //    position = item.employment.position,
            //    addresses = item.addresses,
            //    contacts = item.contacts,
            //    branchSlug = item.employment.branch,
            //    immediateSupervisor = new
            //    {
            //        id = uis.Id,
            //        name = uis.fullname,
            //        email = uis.email,
            //        position = uis.employment.position
            //    },
            //    extension = item.extension,
            //    jobGrade = item.employment.jobGrade
            //});

        }

        [HttpPost("logout")]
        public IActionResult logout([FromBody] users user)
        {
            var item = _Repository.getCredential(user.email);

            ////Insert Log//
            //string log = "Logged out ";
            //int level = 1;
            //string link = "link here";
            //global.log(log, level, link);
            ////Insert Log//

            return Ok(new
            {
                type = "success",
                message = "You have successfully logout."
            });
        }


        [HttpPut("resetPassword")]
        public IActionResult resetPassword([FromBody] auth user)
        {
        
            _Repository.resetPassword(user.email, user);
            return Ok(new
            {
                type = "success",
                message = "You have successfully logout."
            });

        }

    }
}
