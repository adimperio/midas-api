namespace kmbi_core_master.hrModule.UseCase {
    public interface IUploadDtrTemplate<Request, Response> {
        Response Handle(Request request);
    }
}