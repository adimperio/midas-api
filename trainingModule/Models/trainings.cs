﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace kmbi_core_master_v2.trainingModule.Models
{
    public class trainings
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string slug { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public string category { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
    }

    public class trainingInstances
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string slug { get; set; }
        public string name { get; set; }
        public string trainingSlug { get; set; }
        public duration duration { get; set; }
        public string sponsor { get; set; }
        public string venue { get; set; }
        public List<participants> participants { get; set; }
        public List<documents> documents { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
        public Double ActualCost { get; set; }
        public String[] ResourcePersons { get; set; } 
        public Double TotalNoOfHours { get; set; }
        public Double budget { get; set; }
    }

    public class duration
    {
        public DateTime from { get; set; }
        public DateTime to { get; set; }
    }

    public class participants
    {
        public employee employee { get; set; }
        public double cost { get; set; }
        public double personalCost { get; set; }
        public bool isResourceSpeaker { get; set; }
        public int noOfHours { get; set; }
        public int holdingPeriod { get; set; }
        public string holdingWord { get; set; }
        public DateTime holdingUntil { get; set; }
        public bool completed { get; set; }
        public List<participantDocuments> participantDocuments { get; set; }
    }

    public class employee
    {
        public string slug { get; set; }
        public string fullname { get; set; }
        public string position { get; set; }
        public string dateHired { get; set; }
    }
    
    public class participantDocuments
    {
        public string slug { get; set; }
        public string name { get; set; }
        public string filename { get; set; }
        public string path { get; set; }
    }

    public class documents
    {
        public string slug { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string filename { get; set; }
        public string path { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
    }
}
