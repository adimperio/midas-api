﻿using kmbi_core_master.coreMaster.IRepository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Threading.Tasks;

namespace kmbi_core_master.coreMaster.Filters
{
    public class validateUser : TypeFilterAttribute
    {
        public static string _userAccess { get; set; }
        public validateUser(string userAccess) : base(typeof(ValidateUserAccess))
        {
            _userAccess = userAccess;
        }

        private class ValidateUserAccess : IAsyncActionFilter
        {
            private readonly iusersRepository _usersRepository;
            public ValidateUserAccess(iusersRepository usersRepository)
            {
                _usersRepository = usersRepository;
            }

            public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
            {
                //if ((_usersRepository.getSlug).Contains(_userAccess))
                //{
                //    await next();
                //}
                //else
                //{
                //    context.Result = new NotFoundObjectResult("You are not authorized to do this action.");
                //    return;
                //}

                await next();

            }
        }
    }
}
