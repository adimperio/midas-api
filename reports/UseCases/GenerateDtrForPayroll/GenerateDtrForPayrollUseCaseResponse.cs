namespace UseCases.GenerateDtrForPayroll {
    public class GenerateDtrForPayrollUseCaseResponse {
        public DtrItem[] items {get;set;}
        public DtrItemDet[] itemDets {get;set;}
    }
}