using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.ComponentModel.DataAnnotations;

namespace kmbi_core_master.documentationModule.Models
{
    public class ticket
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("slug")]
        public String slug {get;set;}

        [BsonElement("ticketId")]
        public Int32 ticketId {get;set;}

        [BsonElementAttribute("requestorId")]
        public String requestorId {get;set;}

        [BsonElementAttribute("subject")]
        [RequiredAttribute(ErrorMessage="Subject is required")]
        public String subject {get;set;}

        [BsonElementAttribute("category")]
        [RequiredAttribute(ErrorMessage="Category is required")]
        public String category {get;set;}

        [BsonElementAttribute("status")]
        [RequiredAttribute(ErrorMessage="Status is required")]
        public String status {get;set;}

        [BsonElementAttribute("closedBy")]
        public String closedBy {get;set;}

        [BsonElementAttribute("closedDate")]
        public DateTime? closedDate {get;set;}

        public List<message> messages {get;set;}

        [BsonElement("createdAt")]
        public DateTime createdAt {get;set;}
        
        [BsonElement("updatedAt")]
        public DateTime updatedAt {get;set;}
    }
}