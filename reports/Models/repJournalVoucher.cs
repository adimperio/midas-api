using System;
using kmbi_core_master.coreMaster.Models;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using kmbi_core_master.acctngModule.Models;

namespace kmbi_core_master.reports.Models
{
    public class repJournalVoucher
    {
        public String jvNumber {get;set;}
        public String particulars {get;set;}
        public jvAccounts accounts {get;set;}
    }
}