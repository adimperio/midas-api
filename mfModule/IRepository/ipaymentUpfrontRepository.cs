﻿using kmbi_core_master.coreMaster.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.coreMaster.IRepository
{
    public interface ipaymentUpfrontRepository
    {
        IEnumerable<paymentUpfront> all();

        paymentUpfront getSlug(String slug);

        paymentUpfront getRefId(String refId);

        paymentUpfront getId(String id);

        void add(paymentUpfront b);

        void update(String id, paymentUpfront b);

        void updateSlug(String slug, paymentUpfront b);

        void remove(String id);

        void removeSlug(String slug);

    }
}
