namespace kmbi_core_master.reports.UseCases {
    public interface IPresent<Request, Response> {
        Response Present(Request request);
    }
}