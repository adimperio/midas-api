﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace kmbi_core_master.hrModule.Models
{
    public class leaveLog
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("slug")]
        public String slug { get; set; }

        [BsonElement("type")]
        public String type { get; set; }

        [BsonElement("code")]
        public String code { get; set; }

        [BsonElement("employee")]
        public leaveLogEmployee employee { get; set; }

        [BsonElement("dates")]
        public leaveLogDates dates { get; set; }

        [BsonElement("reason")]
        public String reason { get; set; }

        [BsonElement("attachmentPath")]
        public String attachmentPath { get; set; }

        [BsonElement("receivedBy")]
        public String receivedBy { get; set; }

        [BsonElement("withPay")]
        public Boolean? withPay { get; set; }

        [BsonElement("dateReceived")]
        public DateTime? dateReceived { get; set; }

        [BsonElement("dateFiled")]
        public DateTime? dateFiled { get; set; }

        [BsonElement("supervisor")]
        public String supervisor{get;set;}

        [BsonElement("comment")]
        public String comment {get;set;}

        [BsonElement("status")]
        public String status {get;set;}
        
        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }
    }

    public class leaveLogEmployee {

        [BsonElementAttribute("slug")]
        public String slug {get;set;}

        [BsonElementAttribute("code")]
        public String code {get;set;}

        [BsonElement("name")]
        public leaveLogEmployeeName name {get;set;}
    }

    public class leaveLogEmployeeName {
        [BsonElement("last")]
        public String last {get;set;} 

        [BsonElementAttribute("first")]
        public String first {get;set;}

        [BsonElementAttribute("middle")]
        public String middle {get;set;}

        [BsonElementAttribute("full")]
        public String full {get;set;}

        [BsonElementAttribute("extension")]
        public String extension { get; set; }
    }

    public class leaveLogDates {
        [BsonElementAttribute("from")]
        public DateTime from {get;set;}

        [BsonElementAttribute("to")]
        public DateTime to {get;set;}
    }
}
