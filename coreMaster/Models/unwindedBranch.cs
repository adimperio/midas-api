﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace kmbi_core_master.coreMaster.Models
{
    [BsonIgnoreExtraElements]
    public class unwindedBranch
    {
        [BsonElement("units")]
        public unwindedUnits units { get; set; }
      
    }
}
