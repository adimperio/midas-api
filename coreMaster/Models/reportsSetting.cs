﻿using System;

namespace kmbi_core_master.coreMaster.Models
{
    public class reportSetting
    {
        public Int32 pnVersion {get;set;}
        public reportSize reportSize {get;set;}
        public Int32 ecrBatchingVersion {get;set;}
    }

    public class reportSize
    {
        public xy defaultSize {get;set;}
        public xy legalSize {get;set;}
    }

    public class xy
    {
        public double x {get;set;}
        public double y {get;set;}
    }
}
