﻿using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Models;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace kmbi_core_master.coreMaster.Repository
{
    public class syncRepository : dbCon, isyncRepository
    {

        private static IMongoDatabase _cloudDB;

        public IMongoDatabase cloudConnect()
        {
            var client = new MongoClient("mongodb://dev:ojzyj1p2@52.221.154.25:27017/kmbi");
            var database = client.GetDatabase("kmbi");
            return database;
        }


        public syncRepository(IOptions<dbSettings> settings) : base(settings)
        {
            _cloudDB = cloudConnect();
        }

        public void sync(string slug)
        {
            //get Last Async from Branch
            var getLastAsync = Builders<branch>.Filter.Eq(x => x.slug, slug);
            var lastSyncAt = _db.GetCollection<branch>("branch").Find(getLastAsync).FirstOrDefault().lastSyncAt;
            
            var getCbuLogs = Builders<cbuLogs>.Filter.Gt(x => x.createdAt, lastSyncAt);
            var cbuLogsResult = _db.GetCollection<cbuLogs>("cbuLogs").Find(getCbuLogs).ToListAsync().Result;
            //Remove existing
            _cloudDB.GetCollection<cbuLogs>("cbuLogs").DeleteManyAsync(getCbuLogs);

            syncCbuLogs p = new syncCbuLogs();
            for (int i = 0; i <= cbuLogsResult.Count -1; i++)
            {
                p = new syncCbuLogs
                {
                    Id = new ObjectId(cbuLogsResult[i].Id),
                    type = cbuLogsResult[i].type,
                    slug = cbuLogsResult[i].slug,
                    clientId = cbuLogsResult[i].clientId,
                    amount = cbuLogsResult[i].amount,
                    cbuBalance = cbuLogsResult[i].cbuBalance,
                    //loan = cbuLogsResult[i].loan,
                    //transactedAt = cbuLogsResult[i].transactedAt,
                    createdAt = cbuLogsResult[i].createdAt,
                    updatedAt = cbuLogsResult[i].updatedAt
                };

                //Insert CBU Logs
                _cloudDB.GetCollection<syncCbuLogs>("cbuLogs").InsertOneAsync(p);
            }
            
        }
    }
}
