using kmbi_core_master.spm.Models;
using System;

namespace kmbi_core_master.spm.IRepository {
    public interface iLikelihoodRepository {
        Likelihood[] GetLikelihoods();
        Likelihood GetLikelihoodByScore(Int32 Score);
    }
}
