﻿using Amazon;
using Amazon.CloudWatch;
using Amazon.CloudWatch.Model;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace kmbi_core_master.coreMaster.Controllers
{
    [Route("api/awsS3")]
    public class awsS3Controller : Controller
    {
        private IHostingEnvironment hostingEnv;

        private static readonly string _awsAccessKey = "AKIAIHWBQPAYUMCPRFGA";
        private static readonly string _awsSecretKey = "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";
        private static readonly string _bucketName = "kmbidevrepo";
        

        public awsS3Controller(IHostingEnvironment env)
        {
            this.hostingEnv = env;
        }

        [HttpPost]
        public async Task<IActionResult> UploadToS3(IFormFile file)
        {
            
            //Upload file to local
            var uploads = Path.Combine(hostingEnv.ContentRootPath, @"uploads\employees");            
            if (file.Length > 0)
            {
                using (var fileStream = new FileStream(Path.Combine(uploads, file.FileName), FileMode.Create))
                {
                    await file.CopyToAsync(fileStream);
                }
            }

            //Upload file to S3
            string filePath = Path.Combine(uploads, file.FileName);
            TransferUtility fTransfer = new TransferUtility(new AmazonS3Client(_awsAccessKey, _awsSecretKey, RegionEndpoint.APSoutheast1));
            string keyName = string.Format("uploads/employees/{0}", file.FileName);
            fTransfer.Upload(filePath, _bucketName, keyName);


            //Delete file from local
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }

            //Return file URL
            var client = new AmazonS3Client(_awsAccessKey, _awsSecretKey, RegionEndpoint.APSoutheast1);
            
            GetPreSignedUrlRequest request = new GetPreSignedUrlRequest();
            request.BucketName = _bucketName;
            request.Key = keyName;
            request.Expires = DateTime.Now.AddHours(1);
            request.Protocol = Protocol.HTTP;
            string url = client.GetPreSignedURL(request);


            return Ok( new
            {
                message = "Uploaded successfully",
                //downloadPath = "https://s3-ap-southeast-1.amazonaws.com/" + _bucketName + "/" + keyName
                downloadPath = url
            });
            
        }


        [HttpGet("getMetrics")]
        public  IActionResult GetMetrics()
        {
            
            var client = new AmazonCloudWatchClient("AKIAJQYSIVR2SWWYYCXQ", "2mlF0W04FgJRQW52xQszGxBY2x59frmbk5YMQGYV", RegionEndpoint.APSoutheast1);

            Dimension dim = new Dimension()
                {
                    Name = "InstanceId",
                    //Type = "t2.micro",
                    //Value = "i-043766bd41360bf40"
                    Value = "i-0c446b55b1e14f806",
                    //Value = "DevDemo",
                };
            List<Dimension> dimensions = new List<Dimension>() { dim };
            DateTime end = DateTime.Now; //Convert.ToDateTime(DateTime.Now.ToUniversalTime().ToString("s")), //has to be in this format: 2010-09-29T14:00:00+01:00;
            DateTime start = DateTime.Now.AddHours(Convert.ToInt32(-15));// Convert.ToDateTime(startTime1),
            int period = 60 * 5;
            var cpu = new GetMetricStatisticsRequest
            {
                MetricName = "CPUUtilization",
                Period = period,
                Statistics = new List<string>() { "Average" },
                Dimensions = dimensions,
                Namespace = "AWS/EC2",
                EndTime = end,
                StartTime = start,
                Unit = StandardUnit.Percent
            };

            var resCpu = client.GetMetricStatisticsAsync(cpu);
            double aveCpu = 0;
            if (resCpu.Result.Datapoints.Count > 0)
            {
                foreach (var point in resCpu.Result.Datapoints)
                {
                    aveCpu = Math.Round(point.Average, 2);
                }
            }

            //CREDITBALANCE
            var cpuCreditbalance = new GetMetricStatisticsRequest
            {
                MetricName = "CPUCreditBalance",
                Period = period,
                Statistics = new List<string>() { "Average" },
                Dimensions = dimensions,
                Namespace = "AWS/EC2",
                EndTime = end,
                StartTime = start,
                Unit = StandardUnit.Count
            };
            var resCpuCreditbalance = client.GetMetricStatisticsAsync(cpuCreditbalance);
            double aveCpuCreditbalance = 0;
            if (resCpuCreditbalance.Result.Datapoints.Count > 0)
            {
                foreach (var point in resCpuCreditbalance.Result.Datapoints)
                {
                    aveCpuCreditbalance = Math.Round(point.Average, 0);
                }
            }


            //NETWORKPACKETSOUT
            var networkPacketsOut = new GetMetricStatisticsRequest
            {
                MetricName = "NetworkPacketsOut",
                Period = period,
                Statistics = new List<string>() { "Average" },
                Dimensions = dimensions,
                Namespace = "AWS/EC2",
                EndTime = end,
                StartTime = start,
                Unit = StandardUnit.Count
            };
            var resNetworkPacketsOut = client.GetMetricStatisticsAsync(networkPacketsOut);
            double aveNetworkPacketsOut = 0;
            if (resNetworkPacketsOut.Result.Datapoints.Count > 0)
            {
                foreach (var point in resNetworkPacketsOut.Result.Datapoints)
                {
                    aveNetworkPacketsOut = Math.Round(point.Average, 2);
                }
            }

            //DISKSPACEAVAILABLE
            var diskSpaceAvailable = new GetMetricStatisticsRequest
            {
                MetricName = "DiskSpaceAvailable",
                Period = period,
                Statistics = new List<string>() { "Average" },
                Dimensions = dimensions,
                Namespace = "System/Linux",
                EndTime = end,
                StartTime = start,
                Unit = StandardUnit.Gigabytes
            };
            var resDiskSpaceAvailable = client.GetMetricStatisticsAsync(diskSpaceAvailable);
            double aveDiskSpaceAvailable = 0;
            if (resDiskSpaceAvailable.Result.Datapoints.Count > 0)
            {
                foreach (var point in resDiskSpaceAvailable.Result.Datapoints)
                {
                    aveDiskSpaceAvailable = Math.Round(point.Average, 2);
                }
            }

            return Ok(new
            {
                cpuUtilization = aveCpu + " %",
                cpuCreditbalance = aveCpuCreditbalance,
                networkPacketsOut = aveNetworkPacketsOut,
                diskSpaceAvailable = aveDiskSpaceAvailable
            });

        }
    }
}

//var dimension = new Dimension
//{
//    Name = "Desktop Machine Metrics",
//    Value = "Virtual Desktop Machine Usage"
//};

//var metric1 = new MetricDatum
//{
//    Dimensions = new List<Dimension>(),
//    MetricName = "Desktop Machines Online",
//    StatisticValues = new StatisticSet(),
//    Timestamp = DateTime.Today,
//    Unit = StandardUnit.Count,
//    Value = 14
//};

//var metric2 = new MetricDatum
//{
//    Dimensions = new List<Dimension>(),
//    MetricName = "Desktop Machines Offline",
//    StatisticValues = new StatisticSet(),
//    Timestamp = DateTime.Today,
//    Unit = StandardUnit.Count,
//    Value = 7
//};

//var metric3 = new MetricDatum
//{
//    Dimensions = new List<Dimension>(),
//    MetricName = "Desktop Machines Online",
//    StatisticValues = new StatisticSet(),
//    Timestamp = DateTime.Today,
//    Unit = StandardUnit.Count,
//    Value = 12
//};

//var metric4 = new MetricDatum
//{
//    Dimensions = new List<Dimension>(),
//    MetricName = "Desktop Machines Offline",
//    StatisticValues = new StatisticSet(),
//    Timestamp = DateTime.Today,
//    Unit = StandardUnit.Count,
//    Value = 9
//};

//var request = new PutMetricDataRequest
//{
//    MetricData = new List<MetricDatum>() { metric1, metric2, metric3, metric4 },
//    Namespace = "Example.com Custom Metrics"
//};

//client.PutMetricDataAsync(request);



