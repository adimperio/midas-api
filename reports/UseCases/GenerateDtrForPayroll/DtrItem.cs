using System;

namespace UseCases.GenerateDtrForPayroll {
    public class DtrItem {
        public string EmployeeCode {get;set;}
        public string EmployeeName {get;set;}
        public Double RegDays {get;set;}
        public Double RegHrs {get;set;}
        public Double RegMins {get;set;}
        public Double ABDays {get;set;}
        public Double ABHrs {get;set;}
        public Double LTHrs {get;set;}
        public Double LTMins {get;set;}
        public Double UTHrs {get;set;}
        public Double UTMins {get;set;}
    }
    public class DtrItemDet : DtrItem {
        public DateTime Date {get;set;}
    }

}