﻿using kmbi_core_master.filesModule.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.filesModule.Interface
{
    public interface ifilesRepository
    {
        IEnumerable<files> all();

        files getSlug(String slug);

        void add(files b);

        void update(String slug, files b);

        void remove(String slug);

        List<files> search(string s);
    }
}
