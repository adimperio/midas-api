﻿using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.payrollModule.IRepository;
using kmbi_core_master.payrollModule.Models;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace kmbi_core_master.payrollModule.Repository
{
    public class taxRepository: dbCon, itaxRepository
    {
       
        public taxRepository(IOptions<dbSettings> settings) : base(settings)
        {
        }

        public tax getExemption(string taxCode)
        {
            var qtax = Builders<tax>.Filter.Eq("exemption.status", taxCode);
            var tax = _db.GetCollection<tax>("tax").Find(qtax).FirstOrDefault();

            return tax;
        }
    }
}
