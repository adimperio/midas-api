﻿using Microsoft.Extensions.Options;

using System;
using System.Collections.Generic;
using System.Linq;

using MongoDB.Bson;
using MongoDB.Driver;

using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.hrModule.Models;
using kmbi_core_master.hrModule.Interface;

namespace kmbi_core_master.hrModule.Repository
{
    public class dtrManualRepository : dbCon, idtrManualRepository
    {
        IMongoCollection<dtrManual> _coll;
        public dtrManualRepository(IOptions<dbSettings> settings) : base(settings) {
            _coll = _db.GetCollection<dtrManual>("dtrManual");
        }

        public dtrManual getDtrManualPerEmployeeCodePerDate(String employeeCode, DateTime date){
            var employeeCodeFilter = Builders<dtrManual>.Filter.Eq(d => d.employeeCode, employeeCode);
            var dateFilter = Builders<dtrManual>.Filter.Eq(d => d.date,date.Date);
            var andFilter = Builders<dtrManual>.Filter.And(employeeCodeFilter, dateFilter);

            return _coll.Find(andFilter).FirstOrDefault();
        }

        public List<dtrManual> getDtrManualPerEmployeeCodePerDateRange(String employeeCode, DateTime from, DateTime to){
            var employeeCodeFilter = Builders<dtrManual>.Filter.Eq(d => d.employeeCode, employeeCode);
            var fromFilter = Builders<dtrManual>.Filter.Gte(d => d.date,from);
            var toFilter = Builders<dtrManual>.Filter.Lte(d => d.date,to);
            var andFilter = Builders<dtrManual>.Filter.And(employeeCodeFilter, fromFilter, toFilter);

            return _coll.Find(andFilter).ToList();
        }

        private Boolean IsIfDtrManualExist(String slug) {
            var slugFilter = Builders<dtrManual>.Filter.Eq(d => d.slug, slug);
            dtrManual result = _coll.Find(slugFilter).FirstOrDefault();
            if(result!=null) {
                return true;
            } else {
                return false;
            }
        }

        public Boolean createDtrManual(dtrManual dtrManual){
            dtrManual.createdAt = DateTime.Now;
            dtrManual.updatedAt = DateTime.Now;
            _coll.InsertOne(dtrManual);
            return IsIfDtrManualExist(dtrManual.slug);
        }

        public Boolean updateDtrManual(String slug, dtrManual dtrManual){
            var slugFilter = Builders<dtrManual>.Filter.Eq(d => d.slug, slug);
            var update = Builders<dtrManual>.Update
                .Set(d => d.manual, dtrManual.manual)
                .Set(d => d.remarks, dtrManual.remarks)
                .Set(d => d.isAbsent, dtrManual.isAbsent)
                .CurrentDate("updatedAt");
            var result = _coll.UpdateOne(slugFilter,update);
            return result.ModifiedCount > 0;
        }

        public Boolean deleteDtrManual(String slug){
            var slugFilter = Builders<dtrManual>.Filter.Eq(d => d.slug, slug);
            var result = _coll.DeleteOne(slugFilter);
            return result.DeletedCount > 0;
        }

        public Boolean replaceDtrManualPerEmployeeCodePerDate(String employeeCode, DateTime date, dtrManual dtrManual){
            
            dtrManual existingdtrManual = getDtrManualPerEmployeeCodePerDate(employeeCode,date);
            while(existingdtrManual!=null) {
                deleteDtrManual(existingdtrManual.slug);
                existingdtrManual = getDtrManualPerEmployeeCodePerDate(employeeCode,date);
            }
            dtrManual.date = date;
            Boolean res = createDtrManual(dtrManual);

            return res;
        }
    }
}
