﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Filters;
using Microsoft.AspNetCore.Cors;
using kmbi_core_master.documentationModule.iRepositories;
using kmbi_core_master.helpers.interfaces;
using System;

namespace kmbi_core_master.coreMaster.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/branchNoAuth")]
    public class branchNoAuthController : Controller
    {
        private readonly ibranchRepository _branchRepository;
        private readonly iregionsRepository _regionRepository;
        private readonly iglobal2 _global2;
        private readonly iusersRepository _logInfo;
        
        public branchNoAuthController(ibranchRepository branchRepositor, iregionsRepository regionRepository, iglobal2 global2, iusersRepository logInfo)
        {
            this._branchRepository = branchRepositor;
            this._regionRepository = regionRepository;
            this._global2 = global2;
            _logInfo = logInfo;
        }

        

        //GET branch Slug
        [HttpGet("getBySlug")]
        public IActionResult getSlug([FromQuery] string slug)
        {

            var item = _branchRepository.getBranchSlug(slug);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        
    }
}
