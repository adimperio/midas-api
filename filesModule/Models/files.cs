﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.filesModule.Models
{
    public class files
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("slug")]
        public String slug { get; set; }

        [BsonElement("name")]
        public String name { get; set; }

        [BsonElement("description")]
        public String description { get; set; }

       
        [BsonElement("type")]
        public String type { get; set; }

        [BsonElement("file")]
        public file file { get; set; }

        [BsonElement("tags")]
        public List<String> tags { get; set; }

        [BsonElement("alwaysDownloadable")]
        public Boolean alwaysDownloadable { get; set; }

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }

        [BsonElement("createdBy")]
        public String createdBy { get; set; }

        [BsonElement("updatedBy")]
        public String updatedBy { get; set; }

        [BsonElement("categories")]
        public List<string> categories { get; set; }

        [BsonElement("inclusions")]
        public List<string> inclusions { get; set; }

        [BsonElement("jobLevel")]
        public List<string> jobLevel { get; set; }


    }
    public class file
    {
        [BsonElement("name")]
        public String name { get; set; }

        [BsonElement("path")]
        public String path { get; set; }

        [BsonElement("type")]
        public String type { get; set; }

        [BsonElement("size")]
        public long size { get; set; }
    }
    
}
