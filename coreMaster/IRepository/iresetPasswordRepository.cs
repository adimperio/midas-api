﻿using kmbi_core_master.coreMaster.Models;
using System;

namespace kmbi_core_master.coreMaster.IRepository
{
    public interface iresetPasswordRepository
    {
        void add(resetPassword user);

        void remove(String email);

        users getCredential(String email);

        resetPassword checkEmailToken(String token, string email);

        void updatePassword(String email, users p);

    }
}
