namespace kmbi_core_master.reports.Models {
    public class repAddress {
        public repAddressItem[] items {get;set;}
    }

    public class repAddressItem {
        public string regionSlug {get;set;}
        public string regionDesc {get;set;}

        public string provinceSlug {get;set;}
        public string provinceDesc {get;set;}

        public string municipalitySlug {get;set;}
        public string municipalityDesc {get;set;}

        public string barangaySlug {get;set;}
        public string barangayDesc {get;set;}

    }
}
