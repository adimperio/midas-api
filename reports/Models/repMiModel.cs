﻿using System;
using System.Collections.Generic;
using kmbi_core_master.coreMaster.Models;

namespace kmbi_core_master.reports.Models
{

    public class repMiModel
    {
        public String branchName {get;set;}
        public String unitName {get;set;}
        public centerInstance center {get;set;}
        public String centerCode {get;set;}
        public String month {get;set;}
        public List<repMiModelLine> lines {get;set;}
    }

    public class repMiModelLine
    {
        public String lastname {get;set;}
        public String firstname {get;set;}
        public String middlename {get;set;}
        public String principalBeneficiary {get;set;}
        public DateTime principalBeneficiaryBirthdate {get;set;}
        public Int32 principalBeneficiaryAge {get;set;}
        public DateTime dateOfLoanRelease {get;set;}
        public DateTime dateOfLoanMaturity {get;set;}
        public Int32 LoanCycle {get;set;}
        public DateTime memberDateOfBirth {get;set;}
        public Int32 age {get;set;}
        public String maritalStatus {get;set;}
        public String spouseName {get;set;}
        public DateTime spouseDateOfBirth {get;set;}
        public Int32 spouseAge {get;set;}
        public String child1Name {get;set;}
        public DateTime child1DateOfBirth {get;set;}
        public Int32 child1Age {get;set;}
        public String child2Name {get;set;}
        public DateTime child2DateOfBirth {get;set;}
        public Int32 child2Age {get;set;}
        public String child3Name {get;set;}
        public DateTime child3DateOfBirth {get;set;}
        public Int32 child3Age {get;set;}
        public String child4Name {get;set;}
        public DateTime child4DateOfBirth {get;set;}
        public Int32 child4Age {get;set;}
        public String child5Name {get;set;}
        public DateTime child5DateOfBirth {get;set;}
        public Int32 child5Age {get;set;}
        public String child6Name {get;set;}
        public DateTime child6DateOfBirth {get;set;}
        public Int32 child6Age {get;set;}
        public String child7Name {get;set;}
        public DateTime child7DateOfBirth {get;set;}
        public Int32 child7Age {get;set;}
        public String child8Name {get;set;}
        public DateTime child8DateOfBirth {get;set;}
        public Int32 child8Age {get;set;}
        public String child9Name {get;set;}
        public DateTime child9DateOfBirth {get;set;}
        public Int32 child9Age {get;set;}
        public String child10Name {get;set;}
        public DateTime child10DateOfBirth {get;set;}
        public Int32 child10Age {get;set;}
        public String parent1Name {get;set;}
        public DateTime parent1DateOfBirth {get;set;}
        public Int32 parent1Age {get;set;}
        public String parent2Name {get;set;}
        public DateTime parent2DateOfBirth {get;set;}
        public Int32 parent2Age {get;set;}
        public String sibling1Name {get;set;}
        public DateTime sibling1DateOfBirth {get;set;}
        public Int32 sibling1Age {get;set;}
        public String sibling2Name {get;set;}
        public DateTime sibling2DateOfBirth {get;set;}
        public Int32 sibling2Age {get;set;}
        public String sibling3Name {get;set;}
        public DateTime sibling3DateOfBirth {get;set;}
        public Int32 sibling3Age {get;set;}
        public String sibling4Name {get;set;}
        public DateTime sibling4DateOfBirth {get;set;}
        public Int32 sibling4Age {get;set;}
        public String sibling5Name {get;set;}
        public DateTime sibling5DateOfBirth {get;set;}
        public Int32 sibling5Age {get;set;}
        public Int32 clientCount {get;set;}
        public Int32 spouseCount {get;set;}
        public Int32 childCount {get;set;}
        public Int32 parentCount {get;set;}
        public Int32 siblingCount {get;set;}
        public Double premiumForFirstLife {get;set;} //upfrontFees.miPremium + sum(members.dependents.premiumAmount)
        public Double personalAccidentInsurance {get;set;} //35
        //public Double premiumForEastWest {get;set;}
        public Double feeRetainedByKmbi {get;set;} //loanType.processingFee
        public Double paidByClient {get;set;} //premiumForFirstLife + premiumForEastWest
    }

}
