﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace kmbi_core_master.coreMaster.Models
{
    public class extractCsvClients
    {

        [BsonElement("branch")]
        public string branch { get; set; }

        [BsonElement("centerCode")]
        public string centerCode { get; set; }

        [BsonElement("centerLoanCycle")]
        public Int32 centerLoanCycle { get; set; }

        [BsonElement("currentCbuBalance")]
        public double currentCbuBalance { get; set; }

        [BsonElement("loanAmount")]
        public double loanAmount { get; set; }

        [BsonElement("firstName")]
        public string firstName { get; set; }

        [BsonElement("middleName")]
        public string middleName { get; set; }

        [BsonElement("lastName")]
        public string lastName { get; set; }

        [BsonElement("birthdate")]
        public DateTime? birthdate { get; set; }

        [BsonElement("civilStatus")]
        public string civilStatus { get; set; }

        [BsonElement("educationalAttainment")]
        public string educationalAttainment { get; set; }

        [BsonElement("id")]
        public string id { get; set; }

        [BsonElement("natureOfBusiness")]
        public string natureOfBusiness { get; set; }

        [BsonElement("weeklyIncome")]
        public double weeklyIncome { get; set; }

        [BsonElement("weeklyExpense")]
        public double weeklyExpense { get; set; }

        [BsonElement("netDisIncome")]
        public double netDisIncome { get; set; }

        [BsonElement("creditLimit")]
        public double creditLimit { get; set; }

    }

    public class csvLoans
    {

        [BsonElement("branchName")]
        public string branchName { get; set; }

        [BsonElement("centerLoanCycle")]
        public Int32 centerLoanCycle { get; set; }

        [BsonElement("centerSlug")]
        public string centerSlug { get; set; }

        [BsonElement("memberSlug")]
        public string memberSlug { get; set; }

        [BsonElement("loanAmount")]
        public double loanAmount { get; set; }

        [BsonElement("loanCycle")]
        public Int32 loanCycle { get; set; }

        [BsonElement("creditLimitAmount")]
        public double creditLimitAmount { get; set; }

        [BsonElement("issuedId")]
        public string issuedId { get; set; }
    }
}
