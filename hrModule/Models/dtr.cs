﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace kmbi_core_master.hrModule.Models
{
    public class dtr
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("slug")]
        public String slug { get; set; }

        [BsonElement("employeeCode")]
        public String employeeCode { get; set; }

        [BsonElement("date")]
        public DateTime date { get; set; }

        [BsonElement("schedule")]
        public schedule schedule { get; set; }

        [BsonElement("actual")]
        public actual actual { get; set; }

        [BsonElement("manual")]
        public manual manual { get; set; }

        [BsonElement("late")]
        public late late { get; set; }

        [BsonElement("undertime")]
        public undertime undertime { get; set; }

        [BsonElement("absent")]
        public absent absent { get; set; }

        [BsonElement("overtime")]
        public overtime overtime { get; set; }

        [BsonElement("hoursWork")]
        public hoursWork hoursWork { get; set; }

        [BsonElement("nd")]
        public double nd{ get; set; }

        [BsonElement("remarks")]
        public string remarks { get; set; }

        public string jobGrade {get;set;}
        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }
    }

    public class schedule
    {
        [BsonElement("inAm")]
        public DateTime inAm { get; set; }

        [BsonElement("outAm")]
        public DateTime outAm { get; set; }

        [BsonElement("inPm")]
        public DateTime inPm { get; set; }

        [BsonElement("outPm")]
        public DateTime outPm { get; set; }

    }

    public class actual
    {
        [BsonElement("inAm")]
        public DateTime? inAm { get; set; }

        [BsonElement("outAm")]
        public DateTime? outAm { get; set; }

        [BsonElement("inPm")]
        public DateTime? inPm { get; set; }

        [BsonElement("outPm")]
        public DateTime? outPm { get; set; }

    }

    public class manual : actual 
    {

    }

    public class late
    {
        [BsonElement("am")]
        public double am { get; set; }

        [BsonElement("pm")]
        public double pm { get; set; }

    }

    public class undertime
    {
        [BsonElement("am")]
        public double am { get; set; }

        [BsonElement("pm")]
        public double pm { get; set; }

    }

    public class absent
    {
        [BsonElement("am")]
        public double am { get; set; }

        [BsonElement("pm")]
        public double pm { get; set; }

    }

    public class overtime
    {
        [BsonElement("am")]
        public double am { get; set; }

        [BsonElement("pm")]
        public double pm { get; set; }

    }

    public class hoursWork
    {
        [BsonElement("am")]
        public double am { get; set; }

        [BsonElement("pm")]
        public double pm { get; set; }

    }
}
