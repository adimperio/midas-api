﻿using kmbi_core_master.acctngModule.Models;
using kmbi_core_master.hrModule.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.acctngModule.Interface
{
    public interface iewtRepository
    {
        IEnumerable<ewt> all();

        ewt getSlug(String slug);

        void add(ewt b);

        void update(String slug, ewt b);

        void remove(String slug);
    }
}
