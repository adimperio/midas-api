﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.hrModule.Models
{
    public class salaryUpdate
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("slug")]
        public String slug { get; set; }

        [BsonElementAttribute("code")]
        public sName name { get; set; }

        [BsonElement("salary")]
        public string salary { get; set; }

        [BsonElement("isApproved")]
        public int isApproved { get; set; }

        [BsonElement("approvedBy")]
        public string approvedBy { get; set; }

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }
    }

   

    public class sName
    {
        [BsonElement("first")]
        public String first { get;set;} 

        [BsonElementAttribute("middle")]
        public String middle { get;set;}

        [BsonElementAttribute("last")]
        public String last { get;set;}

        [BsonElementAttribute("extension")]
        public String extension { get; set; }

        [BsonElementAttribute("fullname")]
        public String fullname { get;set;}
       
    }

}
