using System;
using System.Collections.Generic;
using System.Linq;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.documentationModule.iRepositories;
using kmbi_core_master.hrModule.Interface;
using kmbi_core_master.reports.UseCases;

namespace UseCases.GenerateOtForPayroll {
    public class GenerateOtForPayrollUseCase : IUseCase<GenerateOtForPayrollUseCaseRequest, GenerateOtForPayrollUseCaseResponse>
    {
        public GenerateOtForPayrollUseCase(idtrRepository dtrRepo, iusersRepository userRepo, iholidayRepository holidayRepo) {
            _dtrRepo = dtrRepo;
            _userRepo = userRepo;
            _holidayRepo = holidayRepo;
        }

        public idtrRepository _dtrRepo { get; }
        public iusersRepository _userRepo { get; }
        public iholidayRepository _holidayRepo { get; }

        public GenerateOtForPayrollUseCaseResponse Handle(GenerateOtForPayrollUseCaseRequest request) {
            var dtrs = _dtrRepo.getDtrPerDateRange(request.from.Date, request.to.Date);
            var holds = _holidayRepo.getNext365Holidays(request.from);
            var emps = _userRepo.AllUsers();
            var distEmps = dtrs.Select(xy=>xy.employeeCode).Distinct();

            var items = new List<OtItem>();
            var dets = new List<OtItemDet>();

            distEmps.ToList().ForEach(xy=>{
                var item = new OtItem();
                
                dtrs.Where(y=>y.employeeCode == xy).ToList().ForEach(z=>{
                    var itemdet = new OtItemDet();
                    var emp = emps.Where(aa=>aa.employeeCode == xy).FirstOrDefault();
                    var hol = holds.Where(aa=>aa.date.Date == z.date.Date).FirstOrDefault();

                    var empcode = "";
                    var empname = "";
                    if(emp!=null) {
                        empcode = emp.employeeCode;
                        empname = emp.fullname;
                    }
                    itemdet.EmployeeCode = empcode;
                    itemdet.EmployeeName = empname;

                    item.EmployeeCode = empcode;
                    item.EmployeeName = empname;
                    
                    var ot = z.overtime.am + z.overtime.pm;
                    var nd = z.nd;

                    //ottype, regular, restday
                    var ottype = "";
                    if(z.date.DayOfWeek == DayOfWeek.Saturday || z.date.DayOfWeek == DayOfWeek.Sunday)
                        ottype = "restday";
                    else
                        ottype = "regular";


                    //holidaytype, regular, special
                    var holidaytype = "";
                    if(hol!=null) {
                        if(hol.type.ToLower() == "non-working")
                            holidaytype = "special";
                        else
                            holidaytype = hol.type.ToLower();
                    }
                    
                    
                    if(ottype=="regular" && holidaytype == "regular") {
                        if(emp.employment.status.ToLower() == "regular")
                            itemdet.LEG = ot;
                        else
                            itemdet.LEGAL = ot;
                    }

                    if(ottype=="regular" && holidaytype == "") {
                        itemdet.REGND += nd;
                        itemdet.REGNDOT += 0;
                        itemdet.REGOT += ot;
                    }

                    if(ottype=="restday" && holidaytype == "") {
                        itemdet.RST += ot;
                        itemdet.RSTND += nd;
                        itemdet.RSTOT += 0;
                        itemdet.RSTNDOT += 0;
                    }

                    if(ottype=="restday" && holidaytype == "regular") {
                        itemdet.RSTLEG += ot;
                        itemdet.RSTLEGND += nd;
                        itemdet.RSTLEGNDOT += 0;
                        itemdet.RSTLEGOT += 0;
                    }

                    if(ottype=="restday" && holidaytype=="special") {
                        itemdet.RSTSPE += ot;
                        itemdet.RSTSPEND += nd;
                        itemdet.RSTSPENDOT += 0;
                        itemdet.RSTSPEOT += 0;
                    }

                    if(ottype == "regular" && holidaytype == "special") {

                        if(emp.employment.status.ToLower() == "regular") {
                            itemdet.SPECIAL += ot;
                            itemdet.SPECIALND += nd;
                            itemdet.SPECIALNDOT += 0;
                            itemdet.SPECIALOT += 0;

                        } else {
                            itemdet.SPE += ot;
                            itemdet.SPEND += nd;
                            itemdet.SPENDOT += 0;
                            itemdet.SPEOT += 0;

                        }

                    }
                    
                    itemdet.WLEGND += 0;
                    itemdet.WLEGNDOT += 0;
                    itemdet.WLEGOT += 0;
                    itemdet.WRKLEG += 0;
                    itemdet.Date = z.date;

                    dets.Add(itemdet);

                    item.LEG += itemdet.LEG;
                    item.LEGAL += itemdet.LEGAL;
                    item.REGND += itemdet.REGND;
                    item.REGNDOT += itemdet.REGNDOT;
                    item.REGOT += itemdet.REGOT;
                    item.RST += itemdet.RST;
                    item.RSTLEG += itemdet.RSTLEG;
                    item.RSTLEGND += itemdet.RSTLEGND;
                    item.RSTLEGNDOT += itemdet.RSTLEGNDOT;
                    item.RSTLEGOT += itemdet.RSTLEGOT;
                    item.RSTND += itemdet.RSTND;
                    item.RSTNDOT += itemdet.RSTNDOT;
                    item.RSTOT += itemdet.RSTOT;
                    item.RSTSPE += itemdet.RSTSPE;
                    item.RSTSPEND += itemdet.RSTSPEND;
                    item.RSTSPENDOT += itemdet.RSTSPENDOT;
                    item.RSTSPEOT += itemdet.RSTSPEOT;
                    item.SPE += itemdet.SPE;
                    item.SPEND += itemdet.SPEND;
                    item.SPENDOT += itemdet.SPENDOT;
                    item.SPEOT += itemdet.SPEOT;
                    item.SPECIAL += itemdet.SPECIAL;
                    item.SPECIALND += itemdet.SPECIALND;
                    item.SPECIALNDOT += itemdet.SPECIALNDOT;
                    item.SPECIALOT += itemdet.SPECIALOT;
                    item.WLEGND += itemdet.WLEGND;
                    item.WLEGNDOT += itemdet.WLEGNDOT;
                    item.WLEGOT += itemdet.WLEGOT;
                    item.WRKLEG += itemdet.WRKLEG;
                });
                
                items.Add(item);

            });

            return new GenerateOtForPayrollUseCaseResponse {
                items = items.ToArray(),
                dets = dets.ToArray()
            };
        }
    }
}