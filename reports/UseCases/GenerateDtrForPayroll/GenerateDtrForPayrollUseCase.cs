using System;
using System.Collections.Generic;
using System.Linq;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.hrModule.Interface;
using kmbi_core_master.reports.UseCases;

namespace UseCases.GenerateDtrForPayroll {
    public class GenerateDtrForPayrollUseCase : IUseCase<GenerateDtrForPayrollUseCaseRequest, GenerateDtrForPayrollUseCaseResponse>
    {
        public idtrRepository _dtrRepo { get; }
        public iusersRepository _userRepo { get; }
        public ileaveLogsRepository _leaveLogRepo {get; }

        public GenerateDtrForPayrollUseCase(idtrRepository dtrRepo, iusersRepository userRepo, ileaveLogsRepository leaveLogRepo) {
            _dtrRepo = dtrRepo;
            _userRepo = userRepo;
            _leaveLogRepo = leaveLogRepo;
        }

        public GenerateDtrForPayrollUseCaseResponse Handle(GenerateDtrForPayrollUseCaseRequest request) {
            var dtrPerDate = _dtrRepo.getDtrPerDateRange(request.From.Date, request.To.Date);
            
            var emps = _userRepo.AllUsers();
            // get leaves
            var leaves = _leaveLogRepo.leavesPerDateRange(request.From.Date, request.To.Date);
            
            var distEmps =dtrPerDate.Select(x=>x.employeeCode).Distinct();

            var items = new List<DtrItem>();
            var dets = new List<DtrItemDet>();
            distEmps.ToList().ForEach(xy=>{
                var item = new DtrItem();
                var emp = new users();
                emp = emps.Where(e=>e.employeeCode == xy).FirstOrDefault();
                if(emp!=null) {
                    item.EmployeeCode= emp.employeeCode;
                    item.EmployeeName= emp.fullname;

                    foreach (var dtrItem in dtrPerDate.Where(y=>y.employeeCode == xy)) {

                        // check if on leave
                        var leave = leaves.Where(x=>dtrItem.date >= x.dates.from && dtrItem.date <= x.dates.to && x.employee.code == dtrItem.employeeCode).FirstOrDefault();

                        var regDays = Math.Round((dtrItem.hoursWork.am + dtrItem.hoursWork.pm) / 8,2);
                        var absDays = 0;
                        if(leave!=null && !(dtrItem.date.DayOfWeek == DayOfWeek.Saturday || dtrItem.date.DayOfWeek == DayOfWeek.Sunday)) {
                            regDays = 1;
                            absDays = 0;
                        }
                        
                        item.RegDays += regDays;
                        item.RegHrs = 0;
                        item.RegMins = 0;
                        item.ABDays += absDays;
                        item.ABHrs = 0;
                        item.LTHrs += dtrItem.late.am + dtrItem.late.pm;
                        item.LTMins = 0;
                        item.UTHrs += dtrItem.undertime.am + dtrItem.undertime.pm;
                        item.UTMins = 0;

                        dets.Add(new DtrItemDet {
                            EmployeeCode = emp.employeeCode,
                            EmployeeName = emp.fullname,
                            RegDays = regDays,
                            RegHrs = 0,
                            RegMins = 0,
                            ABDays = absDays,
                            ABHrs = 0,
                            LTHrs = dtrItem.late.am + dtrItem.late.pm,
                            LTMins = 0,
                            UTHrs = dtrItem.undertime.am + dtrItem.undertime.pm,
                            UTMins = 0,
                            Date = dtrItem.date
                        });

                    }
                    items.Add(item);

                }

            });


            return new GenerateDtrForPayrollUseCaseResponse {
                items = items.ToArray(),
                itemDets = dets.ToArray()
            };
        }
    }
}