﻿using kmbi_core_master.reports.Models;
using System;
using System.Collections.Generic;
using kmbi_core_master.mfModule.Models;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.acctngModule.Models;

namespace kmbi_core_master.reports.IRepository
{
    public interface ireporting
    {
        repEcrModel getEcr(String refId, Int32 weekNo);
        repProspectiveClients getProspectiveClients(String refId);
        repSola getsummaryOfLoanApplication(String refId);
        repRfpCv getrfpcv(String refId);
        repUpfrontFees getupfrontFeeSchedule(String refId);
        repWeeklyScheduleModel getweeklyPaymentSchedule(String refId);
        repPromisorryNote getpromisoryNote (String refId);
        repDisclosure getDisclosure(String refId);
        repDisclosure getDisclosureSummary(String refId);
        repDstModel getDst(string branchId, string month, int? year);
        repMiModel getMiPerReferenceId(string refId);
        List<repMiModel> getMiPerMonthYearPerBranch(Int32 month, Int32 year, String branch);
        List<repCic> getCic();
        List<repMidas> getMidas(DateTime cutoffDate, string branchSlug);
        List<repOr> getOrList(DateTime dateFrom, DateTime dateto);
        List<repCashReceipt> getCashReceiptList(DateTime dateFrom, DateTime dateTo);
        //DTR
        List<repDtrDetailed> getDtrRegion(string p, DateTime dateFrom, DateTime dateTo);
        List<repDtrDetailed> getDtrArea(string p, DateTime dateFrom, DateTime dateTo);
        //List<repDtr> getDtrBranch(string p, DateTime dateFrom, DateTime dateTo);
        //List<repDtr> getDtrGroup(string p, DateTime dateFrom, DateTime dateTo);
        //List<repDtr> getDtrDepartment(string p, DateTime dateFrom, DateTime dateTo);
        //List<repDtr> getDtrDivision(string p, DateTime dateFrom, DateTime dateTo);
        //List<repDtr> getDtrSection(string p, DateTime dateFrom, DateTime dateTo);
        //List<repDtr> getDtrSlug(string p, DateTime dateFrom, DateTime dateTo);
        //List<repDtr> getDtrDates(DateTime dateFrom, DateTime dateTo);
        repLedgerIndividualModel getLedgerIndividual(String refId, String memberId);
        repRfpCvCBUWidthdrawal getRfpCvCBUWidthdrawal(String cbuWithdrawalId);
        List<repCashDisbursement> getCashDisbursementList(DateTime dateFrom, DateTime dateTo);
        repCheckVoucher checkVoucher(String cvNumber);
        //repDtrDetailed dtrDetailed(String employeeCode, DateTime from, DateTime to);
        List<repDtrDetailed> dtrDetailedPerEmployee(String employeeCode, DateTime from, DateTime to);
        List<repDtrDetailed> dtrDetailedPerBranch(String branchSlug, DateTime from, DateTime to);
        List<repDtrDetailed> dtrDetailedPerDepartment(String departmentName, DateTime from, DateTime to);
        List<repDtrDetailed> timekeepingAndAttendanceSummary(DateTime from, DateTime to);
        List<repDtrDetailed> timekeepingAndAttendanceSummary(DateTime from, DateTime to, String branchSlug);
        repLedgerPerWeekModel getLedgerPerWeek(String refId);
        repLedgerPerClientPerWeekModel getLedgerPerClientPerWeek(String refId, Double weekNo);
        List<repGeneralJournal> getGeneralJournalList(DateTime dateFrom, DateTime dateTo);
        repGeneralLedger getGeneralLedger(Int32 year);
        List<repJournalVoucher> journalVoucher(DateTime date);
        repPayrollModel getPayroll(DateTime from, DateTime to, Int32 jobGrade, String branchSlug);
		alphalist alphalist(Int32 year, String branchSlug, Int32 jobGrade);
        repAlphalistReportModel alphalist2(Int32 year, String branchSlug, Int32 jobGrade);
        String getDepartmentAcro(String department);    
        repMonthlyRemittance getMonthlyRemittance(Int32 month, Int32 year, String branchSlug);
        IEnumerable<repClientDetailedSearch> clientDetailedSearch(string branchSlug, String field, Double from, Double to);
        repGeneralLedgerSchedule generalLedgerSchedule(Int32 accountno, DateTime from, DateTime to);
        signatories GetSignatories(String reportName, String branchSlug, String unitSlug, String officerSlug);

        balanceSheet balanceSheet(DateTime asOf);
        incomeStatement incomeStatement(DateTime asOf);
        repIncomeStatement incomeStatement2(DateTime asOf);
        repBalanceSheet balanceSheet2(DateTime asOf);

        repLrCbu lrCbuReport(String branchSlug, DateTime date);

        repDailyBalance dailyBalance(DateTime from, DateTime to);
        
        orReportModel orPrinting(string orNo);

        repMafModel mafPrinting(clients client);

        repEcrBatching getEcrBatching(String refId, Int32 weekNo);

        repSola getsummaryOfLoanApplicationv2(String refId);

        repAddress exportAddress();

        ecrDetailsOutput getNewECR(ecrDetails b, string refId);
    }
}
