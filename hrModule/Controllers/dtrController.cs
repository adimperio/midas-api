﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Filters;
using kmbi_core_master.hrModule.Interface;
using kmbi_core_master.hrModule.Models;
using Microsoft.AspNetCore.Cors;
using System;
using Microsoft.AspNetCore.Http;
using kmbi_core_master.helpers.interfaces;
using kmbi_core_master.hrModule.UseCase;
namespace kmbi_core_master.coreMaster.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/dtr")]
    [usersAuth]
    public class dtrController : Controller
    {
        private readonly idtrRepository _Repository;
        public IUploadDtrTemplate<UploadDtrTemplateRequest, UploadDtrTemplateRespnse> _uploadDtrTemplate { get; }
        public iupload _upload { get; }

        public dtrController(
            idtrRepository Repositor,
            IUploadDtrTemplate<UploadDtrTemplateRequest, UploadDtrTemplateRespnse> uploadDtrTemplate)
            {
            
            _Repository = Repositor;
            _uploadDtrTemplate = uploadDtrTemplate;
        }

        // GET: 
        [HttpGet]
        public IEnumerable<dtr> GetAll()
        {
            var c = _Repository.all();
            return c;

        }

        //GET dtr Slug
        [HttpGet("{slug}")]
        public IActionResult getSlug(string slug)
        {

            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return Ok(new{});
            }
            return new ObjectResult(item);
        }

       

        [HttpPost]
        public IActionResult create([FromBody] dtr b)
        {

            _Repository.add(b);

            //Insert Log//
            // string log = "Insert new dtr " + b.employeeCode;
            // int level = 1;
            // string link = "link here";
            // global.log(log, level, link);
            //Insert Log//

            return Ok(new
            {
                type = "success",
                message = b.employeeCode + " created successfully.",
                slug = b.slug
            });

        }

        [HttpPost("list/")]
        public IActionResult create([FromBody]List<dtr> dtrs)
        {

            _Repository.add(dtrs);

            //Insert Log//
            // foreach(dtr dtr in dtrs)
            // {
            //     string log = "Insert dtr " + dtr.employeeCode;
            //     int level = 1;
            //     string link = "link here";
            //     global.log(log, level, link);
            // }
            
            //Insert Log//

            return Ok(new
            {
                type = "success",
                message = "List of DTR created successfully.",
                slug = ""
            });

        }
        

      
        [HttpPut("{slug}")]
        public IActionResult update(string slug, [FromBody] dtr b)
        {
            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _Repository.update(slug, b);

            //Insert Log//
            // string log = "Modified officer " + b.employeeCode;
            // int level = 1;
            // string link = "link here";
            // global.log(log, level, link);
            //Insert Log//

            return Ok(new
            {
                type = "success",
                message = b.employeeCode + " updated successfully."
            });
        }

        

        [HttpDelete("{slug}")]
        public IActionResult delete(string slug)
        {

            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.remove(slug);

            //Insert Log//
            // string log = "Delete officer " + item.employeeCode;
            // int level = 1;
            // string link = "link here";
            // global.log(log, level, link);
            //Insert Log//

            return Ok(new
            {
                type = "success",
                message = item.employeeCode + " deleted successfully."
            });
        }

        [HttpGetAttribute("getDtrPerEmployeeCodePerDateRange/{employeeCode}")]
        public IActionResult getDtrPerEmployeeCodePerDateRange(String employeeCode, [FromQueryAttribute]DateTime from, [FromQueryAttribute]DateTime to)
        {
            var list = _Repository.getDtrPerEmployeeCodePerDateRange(employeeCode, from, to);
            if(list!=null)
            {
                return Ok(list);
            }
            else
            {
                return Ok(new {message="no data to generate"});
            }
        }

        [HttpGetAttribute("getDtrPerBranchSlugPerDateRange/{branchSlug}")]
        public IActionResult getDtrPerBranchPerDateRange(String branchSlug, [FromQueryAttribute]DateTime from, [FromQueryAttribute]DateTime to)
        {
            var list = _Repository.getDtrPerBranchSlugPerDateRange(branchSlug, from, to);
            list.ForEach(x=>{
                x.name = x.name2.last + ", " + x.name2.first + " " + x.extension + " " + x.name2.middle;
            });
            if(list!=null)
            {
                return Ok(list);
            }
            else
            {
                return Ok(new {message="no data to generate"});
            }
        }

        [HttpGetAttribute("getOverlappedAttendances/{employeeCode}")]
        public IActionResult getOverlappedAttendances(String employeeCode, [FromQuery]DateTime from, [FromQuery]DateTime to) {
            //List<leaveLog> list = _Repository.getOverlappedLeaves(employeeCode,from,to);
            // if(list!=null) {
            //     if(list.Count>0) {
            //         String lv = "";
            //         foreach(var l in list) {
            //             lv += l.type + " - " + l.dates.from.ToString() + " to " + l.dates.to.ToString() + ", ";
            //         }
            //         lv = lv.Substring(0, lv.Length - 2);
            //         return Ok(new {
            //             type="failed",
            //             message="Leave application overlapped with the following existing leave(s) : [" + lv + "]"}
            //         );
            //     } else {
            //         return Ok(new List<leaveLog>());    
            //     }
                
            // } else{
            //     return Ok(new List<leaveLog>());
            // }

            // if(list!=null) {
            //     return Ok(from x in list select new {type = x.type, datefrom = x.dates.@from, dateto = x.dates.to});
            // } else {
            //     return Ok(new List<leaveLog>());
            // }

            return Ok(new {overlap = _Repository.getOverlappedAttendances(employeeCode,from,to)});
        }  

        [HttpPost("uploaddtrtemplate/{employeeSlug}")]
        public IActionResult ImportDtrTemplate(IFormFile template, String employeeSlug) {
            var response = _uploadDtrTemplate.Handle(new UploadDtrTemplateRequest {
                template = template,
                employeeSlug = employeeSlug
            });

            return Ok(new {
                type = (response.isSuccesfull) ? "success" : "failed",
                message = String.Join(", ",response.messages)
            });
        }  

    }
}
