using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Filters;
using Microsoft.AspNetCore.Cors;
using MongoDB.Driver;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Bson.IO;
using System;
using kmbi_core_master.spm.Models;

namespace kmbi_core_master.coreMaster.Controllers
{
    
    [EnableCors("allowCors")]
    [Route("api/sync")]
    public class syncController : Controller
    {
        
        private static IMongoDatabase _cloudDB;
        private static IMongoDatabase _db;
        private IOptions<dbSettings> _settings { get; }
        // public syncController()
        // {
            
        // }

        //bool isDBLive;
        private IOptions<syncSettings> _syncSettings;


        

        public IMongoDatabase cloudConnect()
        {
            var syncMongo = _syncSettings.Value.syncTo;
            var client = new MongoClient(syncMongo);
            //var client = new MongoClient("mongodb://dev:ojzyj1p2@52.221.154.25:27017/kmbi");
            var database = client.GetDatabase("kmbi");
            return database;
        }

        public IMongoDatabase localConnect()
        {
            

            var localMongo = _settings.Value.MongoConnection;
           // var client = new MongoClient("mongodb://dev:ojzyj1p2@124.105.71.170:27017/kmbi");
            // var client = new MongoClient("mongodb://localhost:27017/kmbi");
            var client = new MongoClient(localMongo);
            //var client = new MongoClient("mongodb://localhost:27017/kmbi");
            var database = client.GetDatabase("kmbi");
            return database;
        }


        public syncController(IOptions<syncSettings> syncSettings, IOptions<dbSettings> settings)
        {
            _settings = settings;
            _syncSettings = syncSettings;
            _cloudDB = cloudConnect();
            _db = localConnect();
        }

        

        public class lSyncAt
        {
            public DateTime lastSyncAt { get; set; }
        }

        [HttpPost("{slug}")]
        public IActionResult sync(string slug, [FromBody] lSyncAt syncAt)
        {
            int cntCbuLogs = 0;
            int cntCbuWithdrawal = 0;
            int cntCenterInstance = 0;
            int cntCheckVoucher = 0;
            int cntClient = 0;
            int cntLoan = 0;
            int cntPaymentLoans = 0;
            int cntPaymentUpfront = 0;
            int cntBranch = 0;
            int cntOfficers = 0;
            int cntDeletions = 0;
            int cntPpi = 0;

            List<Result> results = new List<Result>();
            try
            {
                ////CHECK INTERNET CONNECTION
                //isDBLive = _cloudDB.RunCommandAsync((Command<BsonDocument>)"{ping:1}").Wait(1000);
                //if (!isDBLive)
                //{
                //    results = new List<Result>()
                //    {
                //        new Result { type = "error", message = "Connection Failed! Please check your internet connection." },
                //    };
                //    return results;
                //}

                ////CHECK VERSIONING
                //string vBranch = "3015ca4f270b6938b27c95863174a8db";
                //string vCloud = "3015ca4f270b6938b27c95863174a8db";

                //if (vBranch != vCloud)
                //{
                //    results = new List<Result>()
                //      {
                //          new Result { type = "warning", message = "The current version published is not supported. Please contact your IT Department." },
                //      };
                //    return results;
                //}





                //get Last Sync from Branch
                var getLastSync = Builders<branch>.Filter.Eq(x => x.slug, slug);
                var lastSyncAt = _db.GetCollection<branch>("branch").Find(getLastSync).FirstOrDefault().lastSyncAt;

                
               
                //******************** CBU LOGS **********************//
                //var getCbuLogs = Builders<cbuLogs>.Filter.Gt(x => x.updatedAt, lastSyncAt);
                //var cbuLogsResult = _db.GetCollection<cbuLogs>("cbuLogs").Find(getCbuLogs).ToListAsync().Result;
                
                // var cbuLogsResult = _db.GetCollection<cbuLogs>("cbuLogs").Aggregate()
                //    .Match(e => e.updatedAt > lastSyncAt)
                //    .Lookup("clients", "slug", "slug", "logs")
                //    .Match(new BsonDocument { { "logs.branchSlug", slug } })
                //    .Project<cbuLogs>(new BsonDocument
                //            {
                            
                //             {"type",1},
                //             {"slug",1},
                //             {"clientId",1},
                //             {"amount",1},
                //             {"cbuBalance",1},
                //             {"loan",1},
                //             {"transactedAt",1},
                //             {"createdAt",1},
                //             {"updatedAt",1}
                //     })
                //    .ToListAsync().Result;
                // cntCbuLogs = cbuLogsResult.Count();
                
                //cbu logs sync optimize
                var _clients = _db.GetCollection<clients>("clients").Find(Builders<clients>.Filter.Eq("branchSlug", slug)).ToList(); 
 
                var cbuLogsResult = _db.GetCollection<cbuLogs>("cbuLogs").Find( 
                    Builders<cbuLogs>.Filter.And( 
                        Builders<cbuLogs>.Filter.Gt(c=>c.updatedAt, lastSyncAt),
                        Builders<cbuLogs>.Filter.In(c=> c.clientId, _clients.ToList().Select(c=>c.Id)) 
                    ) 
                ).ToList(); 
                cntCbuLogs = cbuLogsResult.Count();

                

                //if (cntCbuLogs > 0)
                //{ }
                //Remove existing
                _cloudDB.GetCollection<cbuLogs>("cbuLogs").DeleteMany(Builders<cbuLogs>.Filter.In(c => c.Id, cbuLogsResult.Select(c => c.Id)));
                //Insert 
                syncCbuLogs cLogs = new syncCbuLogs();
                foreach (var i in cbuLogsResult)
                {
                    cLogs = new syncCbuLogs
                    {
                        Id = new ObjectId(i.Id),
                        type = i.type,
                        slug = i.slug,
                        clientId = i.clientId,
                        amount = i.amount,
                        cbuBalance = i.cbuBalance,
                        loan = i.loan,
                        transactedAt = i.transactedAt,
                        createdAt = i.createdAt,
                        updatedAt = i.updatedAt
                    };
                    _cloudDB.GetCollection<syncCbuLogs>("cbuLogs").InsertOne(cLogs);
                }

                //******************** CBU WITHDRAWAL **********************//
                var getCbuWithdrawal1 = Builders<cbuWithdrawal>.Filter.Gt(x => x.updatedAt, lastSyncAt);
                var getCbuWithdrawal2 = Builders<cbuWithdrawal>.Filter.Eq(x => x.branch.slug, slug);
                var getCbuWithdrawal = Builders<cbuWithdrawal>.Filter.And(getCbuWithdrawal1, getCbuWithdrawal2);
                var cbuWithdrawalResult = _db.GetCollection<cbuWithdrawal>("cbuWithdrawal").Find(getCbuWithdrawal).ToListAsync().Result;
                cntCbuWithdrawal = cbuWithdrawalResult.Count();
                //Remove existing
                _cloudDB.GetCollection<cbuWithdrawal>("cbuWithdrawal").DeleteMany(Builders<cbuWithdrawal>.Filter.In(c => c.Id, cbuWithdrawalResult.Select(c => c.Id)));
                //Insert 
                syncCbuWithdrawal cWithdrawal = new syncCbuWithdrawal();
                foreach (var i in cbuWithdrawalResult)
                {
                    cWithdrawal = new syncCbuWithdrawal
                    {
                        Id = new ObjectId(i.Id),
                        slug = i.slug,
                        members = i.members,
                        rfp = i.rfp,
                        cv = i.cv,
                        branch = i.branch,
                        center = i.center,
                        loan = i.loan,
                        createdAt = i.createdAt,
                        updatedAt = i.updatedAt
                    };
                    _cloudDB.GetCollection<syncCbuWithdrawal>("cbuWithdrawal").InsertOne(cWithdrawal);
                }

                //******************** CENTER INSTANCE **********************//
                //var getCenterInstance = Builders<centerInstance>.Filter.Gt(x => x.updatedAt, lastSyncAt);
                //var centerInstanceResult = _db.GetCollection<centerInstance>("centerInstance").Find(getCenterInstance).ToListAsync().Result;
                var crCenterInsV1 = Builders<centerInstance>.Filter.Gt(x => x.updatedAt, lastSyncAt);
                var crCenterInsV2 = Builders<centerInstance>.Filter.Eq(x => x.branchSlug, slug);
                var crCenterIns = Builders<centerInstance>.Filter.And(crCenterInsV1, crCenterInsV2);
                var centerInstanceResult = _db.GetCollection<centerInstance>("centerInstance").Find(crCenterIns).ToListAsync().Result;

                cntCenterInstance = centerInstanceResult.Count();
                //Remove existing
                _cloudDB.GetCollection<centerInstance>("centerInstance").DeleteMany(Builders<centerInstance>.Filter.In(c => c.Id, centerInstanceResult.Select(c => c.Id)));
                //Insert 
                syncCenterInstance cInstance = new syncCenterInstance();
                foreach (var i in centerInstanceResult)
                {
                    cInstance = new syncCenterInstance
                    {
                        Id = new ObjectId(i.Id),
                        referenceId = i.referenceId,
                        centerId = i.centerId,
                        branchId = i.branchId,
                        branchSlug = i.branchSlug,
                        unitId = i.unitId,
                        unitSlug = i.unitSlug,
                        officerId = i.officerId,
                        officerSlug = i.officerSlug,
                        address = i.address,
                        code = i.code,
                        slug = i.slug,
                        loanCycle = i.loanCycle,
                        status = i.status,
                        createdAt = i.createdAt,
                        updatedAt = i.updatedAt
                    };
                    _cloudDB.GetCollection<syncCenterInstance>("centerInstance").InsertOne(cInstance);
                }

                //******************** CHECK VOUCHER **********************//
                //var getCV = Builders<checkVoucher>.Filter.Gt(x => x.updatedAt, lastSyncAt);
                //var cvResult = _db.GetCollection<checkVoucher>("checkVoucher").Find(getCV).ToListAsync().Result;
                var crCheckV1 = Builders<checkVoucher>.Filter.Gt(x => x.updatedAt, lastSyncAt);
                var crCheckV2 = Builders<checkVoucher>.Filter.Eq(x => x.branchSlug, slug);
                var crCheckV = Builders<checkVoucher>.Filter.And(crCheckV1, crCheckV2);
                var cvResult = _db.GetCollection<checkVoucher>("checkVoucher").Find(crCheckV).ToListAsync().Result;
                cntCheckVoucher = cvResult.Count();
                //Remove existing
                _cloudDB.GetCollection<checkVoucher>("checkVoucher").DeleteMany(Builders<checkVoucher>.Filter.In(c => c.Id, cvResult.Select(c => c.Id)));
                //Insert 
                syncCheckVoucher cv = new syncCheckVoucher();
                foreach (var i in cvResult)
                {
                    cv = new syncCheckVoucher
                    {
                        Id = new ObjectId(i.Id),
                        slug = i.slug,
                        referenceId = i.referenceId,
                        branchSlug = i.branchSlug,
                        centerSlug = i.centerSlug,
                        rfp = i.rfp,
                        cv = i.cv,
                        createdAt = i.createdAt,
                        updatedAt = i.updatedAt
                    };
                    _cloudDB.GetCollection<syncCheckVoucher>("checkVoucher").InsertOne(cv);
                }

                //******************** CLIENTS **********************//
                //var getClients = Builders<clients>.Filter.Gt(x => x.updatedAt, lastSyncAt);
                //var clientResults = _db.GetCollection<clients>("clients").Find(getClients).ToListAsync().Result;
                var crClient1 = Builders<clients>.Filter.Gt(x => x.updatedAt, lastSyncAt);
                var crClient2 = Builders<clients>.Filter.Eq(x => x.branchSlug, slug);
                var crClient = Builders<clients>.Filter.And(crClient1, crClient2);
                var clientResults = _db.GetCollection<clients>("clients").Find(crClient).ToListAsync().Result;
                cntClient = clientResults.Count();
                //Remove existing
                _cloudDB.GetCollection<clients>("clients").DeleteMany(Builders<clients>.Filter.In(c => c.Id, clientResults.Select(x => x.Id)));
                //Insert 
                syncClients clients = new syncClients();
                foreach (var i in clientResults)
                {
                    clients = new syncClients
                    {
                        Id = new ObjectId(i.Id),
                        applicationDate = i.applicationDate,
                        accountId = i.slug,
                        slug = i.slug,
                        name = i.name,
                        maidenName = i.maidenName,
                        fullname = i.fullname,
                        addresses = i.addresses,
                        contacts = i.contacts,
                        gender = i.gender,
                        birthdate = i.birthdate,
                        birthplace = i.birthplace,
                        civilStatus = i.civilStatus,
                        religion = i.religion,
                        educationalAttainment = i.educationalAttainment,
                        ids = i.ids,
                        spouse = i.spouse,
                        imageLocation = i.imageLocation,
                        branchId = i.branchId,
                        branchSlug = i.branchSlug,
                        officerId = i.officerId,
                        unitId = i.unitId,
                        centerId = i.centerId,
                        beneficiary = i.beneficiary,
                        dependents = i.dependents,
                        creditLimit = i.creditLimit,
                        business = i.business,
                        loanCycle = i.loanCycle,
                        cbuBalance = i.cbuBalance,
                        title = i.title,
                        status = i.status,
                        guarantor = i.guarantor,
                        interview = i.interview,
                        agri = i.agri,
                        unitSlug = i.unitSlug,
                        officerSlug = i.officerSlug,
                        centerSlug = i.centerSlug,
                        classification = i.classification,
                        processStatus = i.processStatus,
                        createdAt = i.createdAt,
                        updatedAt = i.updatedAt,
                        checkedAt = i.checkedAt,
                        approvedAt = i.approvedAt
                    };
                    _cloudDB.GetCollection<syncClients>("clients").InsertOne(clients);
                }

                //******************** LOANS **********************//
                //var getLoans = Builders<loans>.Filter.Gt(x => x.updatedAt, lastSyncAt);
                //var loanResults = _db.GetCollection<loans>("loans").Find(getLoans).ToListAsync().Result;
                var crLoan1 = Builders<loans>.Filter.Gt(x => x.updatedAt, lastSyncAt);
                var crLoan2 = Builders<loans>.Filter.Eq(x => x.branchSlug, slug);
                var crLoan = Builders<loans>.Filter.And(crLoan1, crLoan2);
                var loanResults = _db.GetCollection<loans>("loans").Find(crLoan).ToListAsync().Result;
                cntLoan = loanResults.Count();
                //Remove existing
                _cloudDB.GetCollection<loans>("loans").DeleteMany(Builders<loans>.Filter.In(c => c.Id, loanResults.Select(c => c.Id)));
                //Insert 
                syncLoans loans = new syncLoans();
                foreach (var i in loanResults)
                {
                    loans = new syncLoans
                    {
                        Id = new ObjectId(i.Id),
                        slug = i.slug,
                        referenceId = i.referenceId,
                        centerId = i.centerId,
                        branchId = i.branchId,
                        officerId = i.officerId,
                        officerName = i.officerName,
                        unitId = i.unitId,
                        unitCode = i.unitCode,
                        centerSlug = i.centerSlug,
                        centerName = i.centerName,
                        branchSlug = i.branchSlug,
                        branchName = i.branchName,
                        officerSlug = i.officerSlug,
                        unitSlug = i.unitSlug,
                        providerId = i.providerId,
                        centerLoanCycle = i.centerLoanCycle,
                        disbursementDate = i.disbursementDate,
                        firstPaymentDate = i.firstPaymentDate,
                        lastPaymentDate = i.lastPaymentDate,
                        loanType = i.loanType,
                        amounts = i.amounts,
                        status = i.status,
                        members = i.members,
                        previousMembers = i.previousMembers,
                        createdAt = i.createdAt,
                        updatedAt = i.updatedAt
                    };
                    _cloudDB.GetCollection<syncLoans>("loans").InsertOne(loans);
                }

                //******************** OFFICERS **********************//
                //var getOfficers = Builders<officers>.Filter.Gt(x => x.updatedAt, lastSyncAt);
                //var officerResult = _db.GetCollection<officers>("officers").Find(getOfficers).ToListAsync().Result;
                var officerResult = _db.GetCollection<officers>("officers").Aggregate()
                   .Match(e => e.updatedAt > lastSyncAt)
                   .Lookup("centerInstance", "slug", "officerSlug", "off")
                   .Match(new BsonDocument { { "off.branchSlug", slug } })
                   .Project<officers>(new BsonDocument
                           {
                            
                            {"unitId",1},
                            {"name",1},
                            {"slug",1},
                            {"designation",1},
                            {"centers",1},
                            {"createdAt",1},
                            {"updatedAt",1}
                    })
                   .ToListAsync().Result;
                cntOfficers = officerResult.Count();
                //Remove existing
                _cloudDB.GetCollection<officers>("officers").DeleteMany(Builders<officers>.Filter.In(c => c.Id, officerResult.Select(c => c.Id)));
                //Insert 
                syncOfficers officers = new syncOfficers();
                foreach (var i in officerResult)
                {
                    officers = new syncOfficers
                    {
                        Id = new ObjectId(i.Id),
                        unitId = i.unitId,
                        name = i.name,
                        slug = i.slug,
                        designation = i.designation,
                        centers = i.centers,
                        createdAt = i.createdAt,
                        updatedAt = i.updatedAt
                    };
                    _cloudDB.GetCollection<syncOfficers>("officers").InsertOne(officers);
                }

                //******************** PAYMENT LOANS **********************//
                //var getpaymentLoans = Builders<paymentLoans>.Filter.Gt(x => x.updatedAt, lastSyncAt);
                //var paymentLoanResults = _db.GetCollection<paymentLoans>("paymentLoans").Find(getpaymentLoans).ToListAsync().Result;
                var crPaymentLoan1 = Builders<paymentLoans>.Filter.Gt(x => x.updatedAt, lastSyncAt);
                var crPaymentLoan2 = Builders<paymentLoans>.Filter.Eq(x => x.loan.branch.slug, slug);
                var crPaymentLoan = Builders<paymentLoans>.Filter.And(crPaymentLoan1, crPaymentLoan2);
                var paymentLoanResults = _db.GetCollection<paymentLoans>("paymentLoans").Find(crPaymentLoan).ToListAsync().Result;
                cntPaymentLoans = paymentLoanResults.Count();
                //Remove existing
                _cloudDB.GetCollection<paymentLoans>("paymentLoans").DeleteMany(Builders<paymentLoans>.Filter.In(c => c.id, paymentLoanResults.Select(c => c.id)));
                //Insert 
                syncPaymentLoans paymentLoans = new syncPaymentLoans();
                foreach (var i in paymentLoanResults)
                {
                    paymentLoans = new syncPaymentLoans
                    {
                        id = new ObjectId(i.id),
                        slug = i.slug,
                        loan = i.loan,
                        members = i.members,
                        officialReceipt = i.officialReceipt,
                        cv = i.cv,
                        payee = i.payee,
                        mode = i.mode,
                        remarks = i.remarks,
                        createdAt = i.createdAt,
                        updatedAt = i.updatedAt
                    };
                    _cloudDB.GetCollection<syncPaymentLoans>("paymentLoans").InsertOne(paymentLoans);
                }

                //******************** PAYMENT UPFRONT **********************//
                //var getPaymentUpfront = Builders<paymentUpfront>.Filter.Gt(x => x.updatedAt, lastSyncAt);
                //var paymentUpfrontResults = _db.GetCollection<paymentUpfront>("paymentUpfront").Find(getPaymentUpfront).ToListAsync().Result;
                var crPaymentUp1 = Builders<paymentUpfront>.Filter.Gt(x => x.updatedAt, lastSyncAt);
                var crPaymentUp2 = Builders<paymentUpfront>.Filter.Eq(x => x.loan.branch.slug, slug);
                var crPaymentUp = Builders<paymentUpfront>.Filter.And(crPaymentUp1, crPaymentUp2);
                var paymentUpfrontResults = _db.GetCollection<paymentUpfront>("paymentUpfront").Find(crPaymentUp).ToListAsync().Result;
                cntPaymentUpfront = paymentUpfrontResults.Count();
                //Remove existing
                _cloudDB.GetCollection<paymentUpfront>("paymentUpfront").DeleteMany(Builders<paymentUpfront>.Filter.In(c => c.id, paymentUpfrontResults.Select(c => c.id)));
                //Insert 
                syncPaymentUpfront paymentUpfront = new syncPaymentUpfront();
                foreach (var i in paymentUpfrontResults)
                {
                    paymentUpfront = new syncPaymentUpfront
                    {
                        id = new ObjectId(i.id),
                        slug = i.slug,
                        loan = i.loan,
                        members = i.members,
                        officialReceipt = i.officialReceipt,
                        payee = i.payee,
                        mode = i.mode,
                        remarks = i.remarks,
                        createdAt = i.createdAt,
                        updatedAt = i.updatedAt
                    };
                    _cloudDB.GetCollection<syncPaymentUpfront>("paymentUpfront").InsertOne(paymentUpfront);
                }


                //******************** PPI - QUESTIONAIRES **********************//
                var ppi = _db.GetCollection<ppi>("ppi").Aggregate()
                  .Match(e => e.updatedAt > lastSyncAt)
                  .Lookup("loans", "loan.slug", "slug", "loans")
                  .Match(new BsonDocument { { "loans.branchSlug", slug } })
                  .Project<ppi>(new BsonDocument
                          {

                            {"Id",1},
                            {"slug",1},
                            {"client",1},
                            {"loan",1},
                            {"questionaire",1},
                            {"questions",1},
                            {"score",1},
                            {"createdAt",1},
                            {"updatedAt",1}
                   })
                  .ToListAsync().Result;

                cntPpi = ppi.Count();
                //Remove existing
                _cloudDB.GetCollection<ppi>("ppi").DeleteMany(Builders<ppi>.Filter.In(c => c.Id, ppi.Select(c => c.Id)));
                //Insert 
                syncPpi spmPPi = new syncPpi();
                foreach (var i in ppi)
                {
                    spmPPi = new syncPpi
                    {
                        Id = new ObjectId(i.Id),
                        client = i.client,
                        loan = i.loan,
                        questionaire = i.questionaire,
                        questions = i.questions,
                        score = i.score,
                        createdAt = i.createdAt,
                        updatedAt = i.updatedAt
                    };
                    _cloudDB.GetCollection<syncPpi>("ppi").InsertOne(spmPPi);
                }


                //DELETIONS
                //******************** DELETIONS **********************//
                var getDeletions1 = Builders<deletions>.Filter.Gt(x => x.deletedAt, lastSyncAt);
                var getDeletions2 = Builders<deletions>.Filter.Eq(x => x.branchSlug, slug);
                var getDeletions = Builders<deletions>.Filter.And(getDeletions1, getDeletions2);
                var deletionsResults = _db.GetCollection<deletions>("deletions").Find(getDeletions).ToListAsync().Result;
                cntDeletions = deletionsResults.Count();
                //Remove existing
                _cloudDB.GetCollection<deletions>("deletions").DeleteMany(Builders<deletions>.Filter.In(c => c.Id, deletionsResults.Select(c => c.Id)));
                //Insert 
                syncDeletions deletions = new syncDeletions();
                foreach (var i in deletionsResults)
                {
                    deletions = new syncDeletions
                    {
                        Id = new ObjectId(i.Id),
                        collection = i.collection,
                        slug = i.slug,
                        branchSlug = i.branchSlug,
                        deletedAt = i.deletedAt
                    };
                    _cloudDB.GetCollection<syncDeletions>("deletions").InsertOne(deletions);
                }



                 var querySyncLastUpdate = Builders<branch>.Filter.Eq(e => e.slug, slug);
                var updateLastUpdate = Builders<branch>.Update
                    .Set("lastSyncAt", syncAt.lastSyncAt)
                    .CurrentDate("updatedAt");

                _db.GetCollection<branch>("branch").UpdateOne(querySyncLastUpdate, updateLastUpdate);


                //******************** BRANCH **********************//
                var getBranch1 = Builders<branch>.Filter.Gt(x => x.updatedAt, lastSyncAt);
                var getBranch2 = Builders<branch>.Filter.Eq(x => x.slug, slug);
                var getBranch = Builders<branch>.Filter.And(getBranch1, getBranch2);
                var branchResult = _db.GetCollection<branch>("branch").Find(getBranch).ToListAsync().Result;
                cntBranch = branchResult.Count();
                //Remove existing
                _cloudDB.GetCollection<branch>("branch").DeleteMany(Builders<branch>.Filter.In(b => b.Id, branchResult.Select(b => b.Id)));

                // var list = _cloudDB.GetCollection<branch>("branch").Find(getBranch).ToList();
                //Insert 
                syncBranch branch = new syncBranch();
                foreach (var i in branchResult)
                {
                    branch = new syncBranch
                    {
                        Id = new ObjectId(i.Id),
                        name = i.name,
                        slug = i.slug,
                        code = i.code,
                        regionId = i.regionId,
                        areaId = i.areaId,
                        address = i.address,
                        coordinates = i.coordinates,
                        head = i.head,
                        ba = i.ba,
                        baa = i.baa,
                        isOperational = i.isOperational,
                        tinNumber = i.tinNumber,
                        holidays = i.holidays,
                        units = i.units,
                        banks = i.banks,
                        lastSyncAt = i.lastSyncAt,
                        createdAt = i.createdAt,
                        updatedAt = i.updatedAt
                    };
                    _cloudDB.GetCollection<syncBranch>("branch").InsertOne(branch);
                }

                //DELETE FROM CLOUD SERVER
                var fDeletions = Builders<deletions>.Filter.Eq(x => x.branchSlug, slug);
                var qGetDeletions = _cloudDB.GetCollection<deletions>("deletions").Find(fDeletions).ToListAsync().Result;

                foreach (var i in qGetDeletions)
                {
                    var query = Builders<deletions>.Filter.Eq(x => x.slug, i.slug);
                    _cloudDB.GetCollection<deletions>(i.collection).DeleteMany(query);
                }


                // //UPDATE LAST SYNC
                // var querySync = Builders<branch>.Filter.Eq(e => e.slug, slug);
                // var update = Builders<branch>.Update
                //     .Set("lastSyncAt", lastSyncAt)
                //     .CurrentDate("updatedAt");

                // _db.GetCollection<branch>("branch").UpdateOne(querySync, update);



                 // // RESULTS
                results = new List<Result>()
                    {
                        //new Result { type = "success", message = cntBranch.ToString() + " Branch." },
                        //new Result { type = "success", message = cntCbuLogs.ToString() + " CBU Logs." },
                        //new Result { type = "success", message = cntCbuWithdrawal.ToString() +  " CBU Withdrawals."},
                        //new Result { type = "success", message = cntCenterInstance.ToString() +  " Center Instances."},
                        //new Result { type = "success", message = cntCheckVoucher.ToString() +  " Check Vouchers."},
                        //new Result { type = "success", message = cntClient.ToString() +  " Clients." },
                        //new Result { type = "success", message = cntLoan.ToString() +  " Loans." },
                        //new Result { type = "success", message = cntOfficers.ToString() +  " Officers." },
                        //new Result { type = "success", message = cntPaymentLoans.ToString() +  " Loan Payments." },
                        //new Result { type = "success", message = cntPaymentUpfront.ToString() +  " Upfront Fees." }
                        new Result { type = "success", message = "Sync successful", details = new List<string>{
                                cntBranch.ToString() + " Branch",
                                cntCbuLogs.ToString() + " CBU Logs",
                                cntCbuWithdrawal.ToString() +  " CBU Withdrawals",
                                cntCenterInstance.ToString() +  " Center Instances",
                                cntCheckVoucher.ToString() +  " Check Vouchers",
                                cntClient.ToString() +  " Clients",
                                cntLoan.ToString() +  " Loans",
                                cntOfficers.ToString() +  " Officers",
                                cntPaymentLoans.ToString() +  " Loan Payments",
                                cntPaymentUpfront.ToString() +  " Upfront Fees",
                                cntPpi.ToString() + " PPI",
                                cntDeletions.ToString() + " Deletions"
                            } 
                        },
                    };
                return Ok(results);

            }

            catch (Exception err)
            {
                // results = new List<Result>()
                //   {
                //       new Result { type = "error", message = err.Message },
                //   };
                // return results;

                results = new List<Result>()
                    {
                        //new Result { type = "success", message = cntBranch.ToString() + " Branch." },
                        //new Result { type = "success", message = cntCbuLogs.ToString() + " CBU Logs." },
                        //new Result { type = "success", message = cntCbuWithdrawal.ToString() +  " CBU Withdrawals."},
                        //new Result { type = "success", message = cntCenterInstance.ToString() +  " Center Instances."},
                        //new Result { type = "success", message = cntCheckVoucher.ToString() +  " Check Vouchers."},
                        //new Result { type = "success", message = cntClient.ToString() +  " Clients." },
                        //new Result { type = "success", message = cntLoan.ToString() +  " Loans." },
                        //new Result { type = "success", message = cntOfficers.ToString() +  " Officers." },
                        //new Result { type = "success", message = cntPaymentLoans.ToString() +  " Loan Payments." },
                        //new Result { type = "success", message = cntPaymentUpfront.ToString() +  " Upfront Fees." }
                        new Result { type = "failed", message = "Sync failed", details = new List<string>{
                                err.Message
                            } 
                        },
                    };
                return BadRequest(results);
                //throw new ArgumentException(err.Message);
            }


           

            //return new List<Result>();
        }


        public class Result
        {
            public string type { get; set; }
            public string message { get; set; }
            public List<string> details {get;set;}
        }


        
   }
}
