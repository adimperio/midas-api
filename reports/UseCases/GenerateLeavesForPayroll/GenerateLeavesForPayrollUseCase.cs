using System;
using System.Collections.Generic;
using System.Linq;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.hrModule.Interface;
using kmbi_core_master.reports.UseCases;

namespace UseCases.GenerateLeavesForPayroll {
    public class GenerateLeavesForPayrollUseCase : IUseCase<GenerateLeavesForPayrollUseCaseRequest, GenerateLeavesForPayrollUseCaseResponse>
    {
        public GenerateLeavesForPayrollUseCase(ileaveLogsRepository leavesRepo, iusersRepository userRepo) {
            _leavesRepo = leavesRepo;
            _userRepo = userRepo;
        }

        public ileaveLogsRepository _leavesRepo { get; }
        public iusersRepository _userRepo { get; }

        public GenerateLeavesForPayrollUseCaseResponse Handle(GenerateLeavesForPayrollUseCaseRequest request) {
            var leavesPerDate = _leavesRepo.leavesPerDateRange(request.From.Date, request.To.Date.AddDays(1).AddSeconds(-1));
            var emps = _userRepo.AllUsers();

            var items = new List<LeaveItem>();
            var dets = new List<LeaveItemDet>();

            var distEmp = leavesPerDate.Select(xy=>xy.employee.code).Distinct();

            distEmp.ToList().ForEach(xy=>{
                var item = new LeaveItem();
                foreach (var leaveItem in leavesPerDate.Where(y=>y.employee.code == xy)) {
                    var emp = new users();
                    emp = emps.Where(e=>e.employeeCode == leaveItem.employee.code).FirstOrDefault();
                    Double h = 0;
                    var _f =  Convert.ToDateTime(request.From.Date.ToString("MM/dd/yyyy") + " " + emp.employeeSchedules[0].timeInAm);
                    var _t =  Convert.ToDateTime(request.To.Date.ToString("MM/dd/yyyy") + " " + emp.employeeSchedules[0].timeOutPm);
                    if(emp==null) {
                        emp.employeeCode = "";
                        emp.fullname = "";
                    } else {
                        if(request.From.Date == leaveItem.dates.from.Date) 
                            _f = leaveItem.dates.from;
                        
                        if(request.To.Date == leaveItem.dates.to.Date)
                            _t = leaveItem.dates.to;
                        
                        var hours = (_t - _f).TotalHours;
                        if(hours>9)
                            hours = 9;
                        
                        h = hours;
                        if (hours > 4)
                            h = (hours - 1) / 8;
                        else
                            h = hours / 8;
                    }

                    var _sl = 0.0;
                    var _vl = 0.0;
                    
                    if(leaveItem.type.ToLower() == "sick leave")
                        _sl = h;
                    else
                        _vl = h;

                    item.EmployeeCode = emp.employeeCode;
                    item.EmployeeName = emp.fullname;
                    item.TotalSL += _sl;
                    item.TotalVL += _vl;
                    
                    dets.Add(new LeaveItemDet {
                        EmployeeCode = emp.employeeCode,
                        EmployeeName = emp.fullname,
                        TotalSL = _sl,
                        TotalVL = _vl,
                        Date = leaveItem.dates.from
                    });
                }

                items.Add(item);

            });

            return new GenerateLeavesForPayrollUseCaseResponse {
                items = items.ToArray(),
                dets = dets.ToArray()
            };
        }
    }
}