using System;
using kmbi_core_master.coreMaster.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.Options;

namespace kmbi_core_master.helpers.interfaces
{
    public class UrlGetter
    {
        private IHttpContextAccessor _httpContextAccessor { get; set; }
        public IOptions<rootUrlSettings> _rootUrlSettings { get; }

        public UrlGetter(IOptions<rootUrlSettings> rootUrlSettings) {
            _rootUrlSettings = rootUrlSettings;
        }


        public Tuple<string, string> get()
        {
            var backend = _rootUrlSettings.Value.backEnd;
            var frontend = _rootUrlSettings.Value.frontEnd;

            return Tuple.Create(frontend, backend);
        }
    }
}