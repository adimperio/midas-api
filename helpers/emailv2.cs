using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

using kmbi_core_master.helpers.interfaces;

namespace kmbi_core_master.helpers
{
    public class emailv2 : iNotification {
        public String from {get;set;}
        public String to {get;set;}
        public String subject {get;set;}
        public String message {get;set;}
        public Boolean isHtml {get;set;}

        const String _displayName = "KMBI";
        const String _from = "no-reply@kmbi.org.ph";
        
        public iResult Send()
        {
            isHtml = true;
            const string DOMAIN = "mail.kmbidemo.com";
            const string API_KEY = "key-100d32ec9f2b3e73f0d3d8a58b050859";

            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(UTF8Encoding.UTF8.GetBytes("api" + ":" + API_KEY)));

            var form = new Dictionary<string, string>();
            form["from"] = _from;
            form["to"] = to;
            form["subject"] = subject;
            if(isHtml)
            {
                form["html"] = message;
            }
            else
            {
                form["text"] = message;
            }
            

            var response =  client.PostAsync("https://api.mailgun.net/v3/" + DOMAIN + "/messages", new FormUrlEncodedContent(form)).Result;
            
            iResult mr = new emailResult();
            if (response.StatusCode == HttpStatusCode.OK)
            {
                mr = new emailResult { code = "success", description= "Success"};
            }
            else
            {
                mr = new emailResult { code = "failed" , description = "[" + response.StatusCode.ToString() + "] " + response.ReasonPhrase};
            }

            return mr;
        }
    }
    
}