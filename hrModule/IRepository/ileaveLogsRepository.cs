﻿using kmbi_core_master.hrModule.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.hrModule.Interface
{
    public interface ileaveLogsRepository
    {
        IEnumerable<leaveLog> getLeaveLogs();
        leaveLog getLeaveLogBySlug(String slug);
        Boolean createLeaveLog(leaveLog leaveLog);
        Boolean updateLeavelogBySlug(String slug, leaveLog leaveLog);
        Boolean deleteLeaveLogBySlug(String slug);

        IEnumerable<leaveLog> getLeaveLogsByEmployeeCode(String code);
        IEnumerable<leaveLog> getLeaveLogsByEmployeeCodeByDate(String code, DateTime date);
        IEnumerable<leaveLog> getLeaveLogsByDate(DateTime date);
        IEnumerable<leaveLog> getLeaveLogsByEmployeeCodeByDateRange(String code, DateTime from, DateTime to);

        String uploadAttachment(String Filename, Byte[] bytes);

        Int32 getOverlappedLeaves(String employeeCode, DateTime from, DateTime to);
        IEnumerable<leaveLog> leavesPerBranchPerDateRange(String branchSlug, DateTime from, DateTime to);

        void deleteAttachment(String Filename);

        IEnumerable<leaveLog> leavesPerEmployeePerYear(String employeeCode, Int32 year);
        IEnumerable<leaveLog> leavesPerEmployeePerDateRange(String employeeCode, DateTime from, DateTime to);

        IEnumerable<leaveLog> leavesPerDateRange(DateTime from, DateTime to);
    }
}
