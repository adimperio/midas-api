﻿using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.payrollModule.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.payrollModule.IRepository
{
    public interface icutOffRepository
    {
        cutOff getCutOff(String slug);
        cutOff[] getCutOffs();
        cutOff getCurrentCutOff();
        void createCutOff(cutOff cutOff);
        void updateCutOff(String slug, cutOff cutOff);
        void deleteCutOff(String slug);
    }

}
