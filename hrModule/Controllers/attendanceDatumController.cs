﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Filters;
using kmbi_core_master.hrModule.Interface;
using kmbi_core_master.hrModule.Models;
using Microsoft.AspNetCore.Cors;

using System;
using System.Threading.Tasks;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.documentationModule.iRepositories;
using kmbi_core_master.coreMaster.Models;
using System.Linq;

namespace kmbi_core_master.coreMaster.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/attendanceDatum")]
    [usersAuth]
    public class attendanceDatumController : Controller
    {
        private ibranchRepository _branch;
        private iholidayRepository _holiday;
        private iusersRepository _user;
        private ileaveLogsRepository _leave;
        private idtrManualRepository _dtrManual;
        private iotLogsRepository _otLog;
        private iattendanceLogsRepository _attendanceLog;
        public attendanceDatumController(
            ibranchRepository branch,
            iholidayRepository holiday,
            iusersRepository user,
            ileaveLogsRepository leave,
            idtrManualRepository dtrManual,
            iotLogsRepository otLog,
            iattendanceLogsRepository attendanceLog
        ) {
            _branch = branch;
            _holiday = holiday;
            _user = user;
            _leave = leave;
            _dtrManual = dtrManual;
            _otLog = otLog;
            _attendanceLog = attendanceLog;
        }

        [HttpGet("byBranch/{branchSlug}")]
        public async Task<IActionResult> byBranch(String branchSlug, [FromQuery]DateTime from, [FromQuery]DateTime to) {

            
            

            branch branch = _branch.getBranchSlug(branchSlug);
            
            attendanceDatum ad = null;
            if(branch!=null) {
                ad = new attendanceDatum();
                ad.branchSlug = branch.slug;
                ad.branchName = branch.name;
                ad.holidays = _holiday.GetHolidays(from.Date.Month,from.Date.Year);
                ad.employees = new List<employee>();
                
                IEnumerable<users> users = _user.directoryByBranchSlug(branchSlug,"");
                List<String> codes = new List<String>();
                foreach(users u in users) {
                    codes.Add(u.biometricsCode);
                }
                Task asdf = downloadAsync(from, to, codes);
                foreach(users user in users) {
                    employee emp = new employee();
                    emp.employeeCode = user.employeeCode;
                    emp.schedules = _user.fixSkeds(user.employeeSchedules.ToArray()).ToList();
                    emp.fullname = user.fullname;
                    emp.jobGrade = user.employment.jobGrade;
                    emp.leaveLogs = _leave.getLeaveLogsByEmployeeCodeByDateRange(user.employeeCode, from, to);
                    emp.dtrManuals = _dtrManual.getDtrManualPerEmployeeCodePerDateRange(user.employeeCode, from, to);
                    emp.otLogs = _otLog.getOtLogPerEmployeeCodePerDateRange(user.employeeCode, from, to);
                    emp.attendanceLogs = _attendanceLog.getAttendanceLogsPerEmployeePerDateRange(user.biometricsCode, from, to);
                    ad.employees.Add(emp);
                }
                await asdf;
            }

            
            return Ok(ad);
        }

        [HttpGet("byEmployee/{employeeSlug}")]
        public IActionResult byEmployee(String employeeSlug, [FromQuery]DateTime from, [FromQuery]DateTime to) {
            users user = _user.getBySlug(employeeSlug);
            
            attendanceDatum ad = null;
            if(user!=null) {
                ad = new attendanceDatum();
                ad.branchSlug = user.employment.branch;
                ad.branchName = user.employment.branch;
                //ad.holidays = _holiday.GetHolidays(from.Date.Month,from.Date.Year);
                ad.holidays = _holiday.GetHolidaysDateRange(from, to);
                ad.employees = new List<employee>();
                
                // IEnumerable<users> users = _user.directoryByBranchSlug(user.employment.branch,"");
                // foreach(users user in users) {
                    employee emp = new employee();
                    emp.employeeCode = user.employeeCode;
                    emp.schedules = _user.fixSkeds(user.employeeSchedules.ToArray()).ToList();
                    emp.fullname = user.fullname;
                    emp.jobGrade = user.employment.jobGrade;
                    emp.position = user.employment.position;
                    emp.leaveLogs = _leave.getLeaveLogsByEmployeeCodeByDateRange(user.employeeCode, from, to);
                    emp.dtrManuals = _dtrManual.getDtrManualPerEmployeeCodePerDateRange(user.employeeCode, from, to);
                    emp.otLogs = _otLog.getOtLogPerEmployeeCodePerDateRange(user.employeeCode, from, to);
                    _attendanceLog.download(from, to, user.biometricsCode);
                    emp.attendanceLogs = _attendanceLog.getAttendanceLogsPerEmployeePerDateRange(user.biometricsCode, from, to);
                    ad.employees.Add(emp);
                // }
            }
            return Ok(ad);
        }

        private async Task downloadAsync(DateTime from, DateTime to, List<String> biometricsCode) {
            await Task.Run(() =>
                biometricsCode.ForEach(x => {
                    _attendanceLog.download(from, to, x);
                })
            );
        }
    }
}
