﻿using Microsoft.Extensions.Options;

using System;
using System.Collections.Generic;
using System.Linq;

using MongoDB.Bson;
using MongoDB.Driver;

using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.filesModule.Models;
using kmbi_core_master.filesModule.Interface;

namespace kmbi_core_master.filesModule.Repository
{
    public class categoryRepository : dbCon, icategoryRepository
    {
        
        public categoryRepository(IOptions<dbSettings> settings) : base(settings) {}

        public void add(category b)
        {
            _db.GetCollection<category>("category").InsertOneAsync(b);
        }

      
        public IEnumerable<category> all()
        {
            var c = _db.GetCollection<category>("category").Find(new BsonDocument()).ToList();
            return c;
        }

        public category getSlug(String slug)
        {
            var query = Builders<category>.Filter.Eq(e => e.slug, slug);
            var c = _db.GetCollection<category>("category").Find(query).ToList();

            return c.FirstOrDefault();
        }

       
        public void update(String slug, category p)
        {
            p.slug = slug;
            var query = Builders<category>.Filter.Eq(e => e.slug, slug);
            var update = Builders<category>.Update
                .Set("name", p.name)
                .Set("slug", p.slug);

            var cat = _db.GetCollection<category>("category").UpdateOne(query, update);
        }

        public void remove(String slug)
        {
            var query = Builders<category>.Filter.Eq(e => e.slug, slug);
            var result = _db.GetCollection<category>("category").DeleteOne(query);
        }

        public List<category> search(String s)
        {
            string[] searchStr = s.Split(default(string[]), StringSplitOptions.RemoveEmptyEntries);

            List<BsonRegularExpression> a = new List<BsonRegularExpression>();

            foreach (string x in searchStr)
            {
                a.Add(new BsonRegularExpression(x, options: "i"));
            }

            var query = Builders<category>.Filter.In(e => e.name, a);
            //var query2 = Builders<category>.Filter.In("tags", a);
            //var query3 = Builders<category>.Filter.In(e => e.name, a);
            //var query = Builders<category>.Filter.Or(query1, query2, query3);
            var list = _db.GetCollection<category>("category").Find(query);
            return list.ToList();
        }

    }
}