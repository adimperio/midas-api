﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace kmbi_core_master.coreMaster.Models
{
    //[BsonIgnoreExtraElements]
    public class branch
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("name")]
        [Required(ErrorMessage = "The Name field is required.")]
        public string name { get; set; }

        [BsonElement("address")]
        public string address { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("code")]
        [Required(ErrorMessage = "The Code field is required.")]
        public string code { get; set; }
        
        [BsonElement("regionId")]
        public string regionId { get; set; }

        [BsonElement("areaId")]
        public string areaId { get; set; }

        [BsonElement("coordinates")]
        public coordinates coordinates { get; set; }

        [BsonElement("head")]
        public string head { get; set; }

        [BsonElement("ba")]
        public string ba { get; set; }

        [BsonElement("baa")]
        public string baa { get; set; }

        [BsonElement("isOperational")]
        public int? isOperational { get; set; }

        [BsonElement("tinNumber")]
        public string tinNumber { get; set; }

        [BsonElementAttribute("holidays")]
        public List<documentationModule.Models.holiday> holidays {get;set;}
        
        [BsonElement("units")]
        public IList<units> units { get; set; }

        [BsonElement("banks")]
        public IList<banks> banks { get; set; }

        [BsonElement("province")]
        public branchProvince province { get; set; }

        [BsonElement("paisId")]
        public String paisId { get; set; }

        [BsonElement("paisLastSyncAt")]
        public DateTime? paisLastSyncAt { get; set; }

        [BsonElement("lastSyncAt")]
        public DateTime lastSyncAt { get; set; }

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }
    }

    public class coordinates
    {
        [BsonElement("latitude")]
        public double latitude { get; set; }

        [BsonElement("longitude")]
        public double longitude { get; set; }
    }
   

    public class units
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("code")]
        public string code { get; set; }

        [BsonElement("head")]
        public string head { get; set; }

        //[BsonElement("officers")]
        //public IList<officers> officers { get; set; }

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }
    }

    public class unitList
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("code")]
        public string code { get; set; }

        [BsonElement("units")]
        public IList<units> units { get; set; }


    }


    public class unitListO
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("code")]
        public string code { get; set; }

        [BsonElement("units")]
        public units units { get; set; }

        [BsonElement("officers")]
        public officers officers { get; set; }
    }


    public class banks
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("code")]
        public string code { get; set; }

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }
    }

    public class banksList
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("code")]
        public string code { get; set; }

        [BsonElement("banks")]
        public IList<banks> banks { get; set; }


    }

    public class syncBranch
    {
        [BsonId]
        public ObjectId Id { get; set; }

        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("address")]
        public string address { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("code")]
        public string code { get; set; }

        [BsonElement("regionId")]
        public string regionId { get; set; }

        [BsonElement("areaId")]
        public string areaId { get; set; }

        [BsonElement("coordinates")]
        public coordinates coordinates { get; set; }

        [BsonElement("head")]
        public string head { get; set; }

        [BsonElement("ba")]
        public string ba { get; set; }

        [BsonElement("baa")]
        public string baa { get; set; }

        [BsonElement("isOperational")]
        public int? isOperational { get; set; }

        [BsonElement("tinNumber")]
        public string tinNumber { get; set; }

        [BsonElementAttribute("holidays")]
        public List<documentationModule.Models.holiday> holidays { get; set; }

        [BsonElement("units")]
        public IList<units> units { get; set; }

        [BsonElement("banks")]
        public IList<banks> banks { get; set; }

        [BsonElement("province")]
        public branchProvince province { get; set; }

        [BsonElement("lastSyncAt")]
        public DateTime lastSyncAt { get; set; }

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }
    }

    public class branchEmployees
    {
        public string slug { get; set; }
        public string bmName { get; set; }
        public string baName { get; set; }
        public string baaName { get; set; }
        public List<beUnits> units { get; set; }
    }

    public class beUnits
    {
        public string slug { get; set; }
        public string puhName { get; set; }
        public List<string> poName { get; set; }
    }

    public class branchProvince
    {
        public string slug { get; set; }
        public string regionSlug { get; set; }
        public string name { get; set; }
    }
}
