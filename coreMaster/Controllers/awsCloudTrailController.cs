﻿using Amazon;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;
//using Amazon.CloudTrail;
//using Amazon.CloudTrail.Model;
using Microsoft.Extensions.Options;
using kmbi_core_master.coreMaster.Models;
using System.Data;
using Newtonsoft.Json;
using CsvHelper;
using System.Xml;
using System.Text;
using System.Data.Common;
using System.Collections.Generic;
using System.Net.Http;
using System.Net;
using System.Net.Http.Headers;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace kmbi_core_master.coreMaster.Controllers
{
    [Route("api/awsCloudTrail")]
    public class awsCloudTrailController : Controller, IDisposable
    {
        //private IHostingEnvironment hostingEnv;
        private awsSettings _settings;

        private static  string _awsAccessKey; 
        private static  string _awsSecretKey; 

        public awsCloudTrailController(IOptions<awsSettings> settings) 
        {
            _settings = settings.Value;
            _awsAccessKey = _settings.accessKey;
            _awsSecretKey = _settings.secretKey;
        }


        //[HttpGet]
        //[Route("data.csv")]
        //[Produces("text/csv")]
        //public IActionResult getCTrailLogs()
        //{

        //    //using (var ctClient = new AmazonCloudTrailClient(_awsAccessKey, _awsSecretKey, RegionEndpoint.APSoutheast1))
        //    //{

               
        //    //    var result = ctClient.LookupEventsAsync(new LookupEventsRequest()).Result.Events;

        //    //    //return new ObjectResult(result);
        //    //    //foreach (var r in result.Result.Events)
        //    //    //{

        //    //    //}
        //    //    List<events> dt = new List<events>();

        //    //    //StringWriter csvString = new StringWriter();
        //    //    //using (var toCsv = new CsvWriter(csvString))
        //    //    //{
        //    //    //    toCsv.Configuration.Delimiter = ",";


        //    //        for (int i=1; i <= result.Count - 1; i++)
        //    //        {
        //    //            dt.Add(
        //    //            new events
        //    //            {
        //    //                cloudTrailEvent = result[i].CloudTrailEvent,
        //    //                eventId = result[i].EventId,
        //    //                eventName = result[i].EventName,
        //    //                eventSource = result[i].EventSource,
        //    //                eventTime = result[i].EventTime,
        //    //                //resources = result[i].Resources,
        //    //                username = result[i].Username
        //    //            });

        //    //           // toCsv.WriteField(result[i].EventId);
        //    //        }
        //    //    //    toCsv.NextRecord();

        //    //    //}
        //    //    return Ok(dt);
        //    //    //return new ObjectResult(ctClient.LookupEventsAsync(new LookupEventsRequest()));
        //    //    //return new ObjectResult(dt);

        //    //    //string csv = jsonToCSV(result.ToString(), ",");
        //    //    //HttpResponseMessage csvResult = new HttpResponseMessage(HttpStatusCode.OK);
        //    //    //csvResult.Content = new StringContent(dt.ToString());
        //    //    //csvResult.Content.Headers.ContentType = new MediaTypeHeaderValue("text/csv");
        //    //    //csvResult.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment") { FileName = "export.csv" };
        //    //    //return csvResult;

        //    }

        //}



        //public static eventResult jsonStringToTable(string jsonContent)
        //{
        //    eventResult dt = JsonConvert.DeserializeObject<eventResult>(jsonContent);
        //    return dt;
        //}

        public static string jsonToCSV(string jsonContent, string delimiter)
        {
            StringWriter csvString = new StringWriter();
            using (var csv = new CsvWriter(csvString))
            {
                //csv.Configuration.SkipEmptyRecords = true;
                //csv.Configuration.WillThrowOnMissingField = false;
                csv.Configuration.Delimiter = delimiter;
                //eventResult dt = jsonStringToTable(jsonContent);
                eventResult dt = new eventResult();
                //using (var dt = jsonStringToTable(jsonContent))
                //{
                foreach (events column in dt.events)
                {
                    //for (var i = 0; i < dt.events.Count; i++)
                    //{
                    //    csv.WriteField(row[i]);
                    //}
                    csv.WriteField(column.eventId);
                }
                csv.NextRecord();

                //foreach (events row in dt.events)
                //{
                //    for (var i = 0; i < dt.events.Count; i++)
                //    {
                //        csv.WriteField(row.[i]);
                //    }
                //    csv.NextRecord();
                //}
                //}
            }
            return csvString.ToString();
        }

        public class eventResult
        {
            public List<events> events { get; set; }
            
        }

        public class events
        {
            public string cloudTrailEvent { get; set; }
            public string eventId { get; set; }
            public string eventName { get; set; }
            public string eventSource { get; set; }
            public DateTime eventTime { get; set; }
            public List<resources> resources { get; set; }
            public string username { get; set; }
        }
        public class resources
        {
            public string resourceName { get; set; }
            public string resourceType { get; set; }
        }
    }
    
}