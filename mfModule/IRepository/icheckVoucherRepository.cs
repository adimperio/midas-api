﻿using kmbi_core_master.coreMaster.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.coreMaster.IRepository
{
    public interface icheckVoucherRepository
    {
        IEnumerable<checkVoucher> all();

        checkVoucher getSlug(String slug);

        checkVoucher getId(String id);

        checkVoucher getRefId(String refId);

        //IEnumerable<getbyBranchloans> getBranchId(string branchId);


        void add(checkVoucher b);

        void update(String id, checkVoucher b);

        void updateSlug(String slug, checkVoucher b);

        void remove(String id);

        void removeSlug(String slug);

        void addRfp(rfp r, string cvSlug);

        void updateRfp(String cvSlug, string rfpNumber, rfp r);

        banksList getBranchSlug(String branchId);

        //void addCv(cv r, string cvSlug);

        //void updateCv(String cvSlug, string cvNumber, cv r);
    }
}
