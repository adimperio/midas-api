﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.coreMaster.Models
{

    public class branchOfficers

    {
        
        public string bm { get; set; }
        public string ba { get; set; }
        public string baa { get; set; }

        public List<branchUnits> units { get; set; }

    }

    public class branchUnits
    {
        public string puh { get; set; }
        public string po1 { get; set; }
        public string po2 { get; set; }
        public string po3 { get; set; }
        public string po4 { get; set; }
        public string po5 { get; set; }
        public string po6 { get; set; }
        public string po7 { get; set; }
    }

    

}
