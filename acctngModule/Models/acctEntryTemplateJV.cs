﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;
using System;

namespace kmbi_core_master.acctngModule.Models
{
    public class acctEntryTemplateJV
    {

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("slug")]
        public String slug { get; set; }

        [BsonElement("particulars")]
        public string particulars { get; set; }

        [BsonElement("accounts")]
        public jvAccounts accounts { get; set; }
        
        [BsonElement("amount")]
        public jvAmount amount { get; set; }

        [BsonElement("timestamps")]
        public jvTimestamps timestamps { get; set; }
    }

    
}
