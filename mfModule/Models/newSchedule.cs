using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace kmbi_core_master.mfModule.Models
{
    public class newSchedule
    {
        [BsonElement("weekNo")]
        public int weekNo { get;set;}

        [BsonElementAttribute("date")]
        public DateTime date {get;set;}
    }
}