﻿

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

using kmbi_core_master.coreMaster;
using kmbi_core_master.coreMaster.Filters;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.payrollModule.IRepository;
using kmbi_core_master.payrollModule.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace kmbi_core_master.payrollModule.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/payrollEntity")]
    [usersAuth]
    public class payrollEntityController : Controller
    {
        private readonly ipayrollEntityRepository _Repository;
        private readonly iusersRepository _logInfo;
        private string token;

        public payrollEntityController(ipayrollEntityRepository Repository, iusersRepository logInfo)
        {
            this._Repository = Repository;
            _logInfo = logInfo;
        }

        public void logger(string message)
        {
            //Insert Log//
            token = HttpContext.Request.Headers["token"];
            var lInfo = _logInfo.getLoginInfo(token);
            string log = message;
            global.createLog(lInfo.id, lInfo.fullname, lInfo.employment.branch, log);
            //Insert Log//
        }

        // GET: 
        [HttpGet]
        public IEnumerable<payrollEntity> GetAll()
        {
            var p = _Repository.getAll();
            return p;

        }

        // GET id
        [HttpGet("{id:length(24)}")]
        public IActionResult Get(string id)
        {
            var item = _Repository.Get(id);
            if (item == null)
            {
               
                return Ok(new { });
            }

            return new ObjectResult(item);
        }

        
        [HttpGet("{slug}")]
        public IActionResult getSlug(string slug)
        {

            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return Ok(new { });
            }
           
            return new ObjectResult(item);
        }

       

        // SEARCH name
        [HttpGet("search")]
        public IActionResult search([FromQuery]string name)
        {

            var item = _Repository.search(name);
            if (item.Count() == 0)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }



        // GET check unique slug
        [HttpGet("checkSlug")]
        public IActionResult checkSlug([FromQuery]string slug)
        {

            var item = _Repository.checkSlug(slug);
            if (item == null)
            {
                return Ok(new
                {
                    available = 1
                });
            }

            return Ok(new
            {
                available = 0
            });
        }


       
        // POST 
        [HttpPost]
        //[usersAccess(access: "user-add")]
        public IActionResult createEntity([FromBody] payrollEntity p)
        {
            
            _Repository.Add(p);

            logger( "Insert new payroll Entity " + p.name);
           
            return Ok(new
            {
                type = "success",
                message = p.name + " created successfully.",
                slug = p.slug
            });

        }

        
        // PUT 
        [errorHandler]
        [HttpPut("{slug}")]
        public IActionResult Put(string slug, [FromBody] payrollEntity p)
        {
            var item = _Repository.checkSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.Update(slug, p);

            logger("Modified Payroll Entity " + p.name);

            return Ok(new
            {
                type = "success",
                message = p.name + " updated successfully."
            });
        }


        
        // DELETE

        [HttpDelete("{slug}")]
        public IActionResult delete(string slug)
        {

            var item = _Repository.checkSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.Remove(slug);

            logger("Remove payroll Entity " + item.name);

            return Ok(new
            {
                type = "success",
                message = item.name + " deleted successfully.",
                slug = item.slug
            });
        }

        

    }

    
}
