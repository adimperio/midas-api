﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Filters;
using Microsoft.AspNetCore.Cors;
using System;

namespace kmbi_core_master.coreMaster.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/cbuWithdrawal")]
    [usersAuth]
    public class cbuWithdrawalController : Controller
    {
        private readonly icbuWithdrawalRepository _Repository;
        private readonly iusersRepository _logInfo;
        private string token;

        public cbuWithdrawalController(icbuWithdrawalRepository Repositor, iusersRepository logInfo)
        {
            this._Repository = Repositor;
            _logInfo = logInfo;
        }

        public void logger(String message)
        {
            //Insert Log//
            token = HttpContext.Request.Headers["token"];
            var lInfo = _logInfo.getLoginInfo(token);
            string log = message;
            global.createLog(lInfo.id, lInfo.fullname, lInfo.employment.branch, log);
            //Insert Log//
        }

        // GET: 
        [HttpGet]
        public IEnumerable<cbuWithdrawal> all()
        {
            var c = _Repository.all();
            return c;

        }

               
        [HttpGet("{slug}")]
        public IActionResult getSlug(string slug)
        {

            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }
        
        [HttpGet("branchSlug={branchSlug}")]
        public IActionResult getBSlug(String branchSlug, [FromQuery]DateTime from, [FromQuery]DateTime to)
        {

            var item = _Repository.getBSlug(branchSlug, from, to);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        // GET bank slug
        [HttpGet("branchId={branchId}")]
        public IActionResult getBanks(string branchId)
        {

            var item = _Repository.getBranchSlug(branchId);
            if (item == null)
            {

                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        // GET by date range
        [HttpGet("datePrepared")]
        public IActionResult getByDate([FromQuery] DateTime from, [FromQuery] DateTime to, [FromQuery] string branchSlug)
        {

            var item = _Repository.getByDate(from, to, branchSlug);
            if (item == null)
            {

                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        // GET by center slug
        [HttpGet("getByCenterSlug")]
        public IActionResult getByCenterSlug([FromQuery] string centerSlug)
        {

            var item = _Repository.getByCenterSulg(centerSlug);
            if (item == null)
            {

                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }


        [HttpPost]
        //[errorHandler]
        public IActionResult create([FromBody] cbuWithdrawal b)
        {

            _Repository.add(b);

            logger("Insert new check cbu withdrawal ");

            return Ok(new
            {
                type = "success",
                message = "CBU Withdrawal created successfully.",
                slug = b.slug
            });

        }


        [HttpPut("{slug}")]
        public IActionResult slugupdate(string slug, [FromBody] cbuWithdrawal b)
        {
            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _Repository.update(slug, b);

            logger("Modified CBU Withdrawal ");

            return Ok(new
            {
                type = "success",
                message = "CBU Withdrawal updated successfully."
            });
        }

        
        [HttpDelete("{slug}")]
        public IActionResult slugdelete(string slug)
        {

            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.remove(slug);

            logger("Delete cbu withdrawal " );

            return Ok(new
            {
                type = "success",
                message = "CBU Withdrawal deleted successfully."
            });
        }


    }
}
