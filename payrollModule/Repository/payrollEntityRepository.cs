﻿using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.payrollModule.IRepository;
using kmbi_core_master.payrollModule.Models;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace kmbi_core_master.payrollModule.Repository
{
    public class payrollEntityRepository: dbCon, ipayrollEntityRepository
    {
       
        public payrollEntityRepository(IOptions<dbSettings> settings) : base(settings)
        {
        }

        public void Add(payrollEntity c)
        {
            c.createdAt = DateTime.Now;
            c.updatedAt = DateTime.Now;
            
            _db.GetCollection<payrollEntity>("payrollEntity").InsertOneAsync(c);
        }

       
        public IEnumerable<payrollEntity> getAll()
        {
            var c = _db.GetCollection<payrollEntity>("payrollEntity").Find(new BsonDocument()).ToListAsync();
            return c.Result;
        }

        public payrollEntity Get(String id)
        {
            var query = Builders<payrollEntity>.Filter.Eq(e => e.Id, id);
            var c = _db.GetCollection<payrollEntity>("payrollEntity").Find(query).ToListAsync();
            
            return c.Result.FirstOrDefault();
        }

       
        public payrollEntity getSlug(String slug)
        {
            var query = Builders<payrollEntity>.Filter.Eq(e => e.slug, slug);
            var fields = Builders<payrollEntity>.Projection
                .Include(u => u.Id)
                .Include(u => u.slug)
                .Include(u => u.name)
                .Include(u => u.type)
                .Include(u => u.employee)
                .Include(u => u.monthlyAmount)
                .Include(u => u.dateInclusive)
                .Include(u => u.createdAt)
                .Include(u => u.updatedAt);

            var c = _db.GetCollection<payrollEntity>("payrollEntity").Find(query).Project<payrollEntity>(fields).ToListAsync();

            return c.Result.FirstOrDefault();
        }




        public void Update(String slug, payrollEntity p)
        {
            
            var filter = Builders<payrollEntity>.Filter.Eq(e => e.slug, slug);
            var update = Builders<payrollEntity>.Update
                .Set("name", p.name)
                .Set("type", p.type)
                .Set("employee", p.employee)
                .Set("monthlyAmount", p.monthlyAmount)
                .Set("dateInclusive", p.dateInclusive)
                .CurrentDate("updatedAt");
            var result = _db.GetCollection<payrollEntity>("payrollEntity").UpdateOneAsync(filter, update);

        }

       
        public void Remove(String slug)
        {
            var query = Builders<payrollEntity>.Filter.Eq(e => e.slug, slug);
            var result = _db.GetCollection<payrollEntity>("payrollEntity").DeleteOneAsync(query);
            
        }


        public payrollEntity checkSlug(String slug)
        {
            var query = Builders<payrollEntity>.Filter.Eq(e => e.slug, slug);
            var c = _db.GetCollection<payrollEntity>("payrollEntity").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }


        public IEnumerable<payrollEntity> search(String name)
        {
           
            var query = Builders<payrollEntity>.Filter.Regex("name", new BsonRegularExpression(name, options: "i"));
            
            var fields = Builders<payrollEntity>.Projection
                .Include(u => u.Id)
                .Include(u => u.slug)
                .Include(u => u.name)
                .Include(u => u.type)
                .Include(u => u.employee)
                .Include(u => u.monthlyAmount)
                .Include(u => u.dateInclusive);

            var c = _db.GetCollection<payrollEntity>("payrollEntity").Find(query).Project<payrollEntity>(fields).ToList().AsQueryable();
            return c;
        }


    }
}
