﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.payrollModule.Models
{
    public class payroll
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }
       
        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("employee")]
        public payrollEmployee employee { get; set; }

        [BsonElement("branch")]
        public payrollBranch branch { get; set; }

        [BsonElement("basicPay")]
        public double basicPay { get; set; }

        [BsonElement("monthlyAmount")]
        public double monthlyAmount { get; set; }

        [BsonElement("cutoffPeriod")]
        public cutoffPeriod cutoffPeriod { get; set; }

        [BsonElement("entities")]
        public List<payrollEntities> entities { get; set; }

        [BsonElement("totalAmount")]
        public double totalAmount { get; set; }

        [BsonElement("encryptedAmounts")]
        public String encryptedAmounts {get;set;}

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }
    }

    public class payrollEmployee
    {
        [BsonElement("id")]
        public string id { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("jobGrade")]
        public string jobGrade {get;set;}
    }

    public class payrollBranch
    {
        [BsonElement("id")]
        public string id { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("name")]
        public string name { get; set; }
    }

    public class cutoffPeriod
    {
        [BsonElement("from")]
        public DateTime? from { get; set; }

        [BsonElement("to")]
        public DateTime? to { get; set; }
    }

    public class payrollEntities
    {
        [BsonElement("id")]
        public string id { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("type")]
        public string type { get; set; }

        [BsonElement("amount")]
        public double amount { get; set; }
    }

    public class payrollEntityList
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("employee")]
        public payrollEmployee employee { get; set; }

        [BsonElement("entities")]
        public List<payrollEntities> entities { get; set; }
    }

    public class payEnt
    {
        [BsonElement("id")]
        public string id { get; set; }

        [BsonElement("empCode")]
        public string empCode { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("taxCode")]
        public string taxCode { get; set; }

        [BsonElement("exemptionAmount")]
        public double exemptionAmount { get; set; }

        [BsonElement("salary")]
        public double salary { get; set; }

        [BsonElement("philhealth")]
        public double philhealth { get; set; }

        [BsonElement("pagibig")]
        public double pagibig { get; set; }

        [BsonElement("sss")]
        public double sss { get; set; }

        [BsonElement("annualTaxableCompensation")]
        public double annualTaxableCompensation { get; set; }

        [BsonElement("annualGrossPay")]
        public double annualGrossPay { get; set; }
    }

    

}
