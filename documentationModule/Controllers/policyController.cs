using System;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Microsoft.Extensions.Options;
using System.Linq;

using kmbi_core_master.documentationModule.Models;
using kmbi_core_master.coreMaster.Filters;
using kmbi_core_master.coreMaster.Models;

using kmbi_core_master.documentationModule.Service;
using Microsoft.AspNetCore.Cors;

namespace kmbi_core_master.documentationModule.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/policy")]
    [errorHandler]
    public class policyController : Controller
    {
        private readonly policyService service;
        public policyController(IOptions<dbSettings> settings) 
        {
            service = new policyService(settings);
        }

        [HttpGet]
        public IActionResult Get()
        {
            IEnumerable<policy> list = service.GetPolicies();
            
            if(list != null)
            {
                return Ok(from x in list select new {_id = x.Id, name = x.name, slug = x.slug, order = x.order});
            }
            else
            {
                return Ok(new List<policy>());
            }
            
        }

        [HttpGet("{slug}")]
        public IActionResult Get(String slug)
        { 
            policy pol = null;
            pol = service.GetPolicyBySlug(slug);
            if (pol != null)
            {
                return Ok(pol);
            }
            else
            {
                return Ok(new{});
            }
        }

        [HttpGet("{policySlug}/{sectionSlug}")]
        public IActionResult Get(String policySlug, String sectionSlug)
        {
            policy pol = null;
            
            pol = service.GetPolicySpecificSection(policySlug, sectionSlug);
            
            if(pol != null)
            {
                if(pol.sections.Count > 0)
                {
                    return Ok(pol.sections[0]);    
                }
                else
                {
                    return Ok(new{});
                }
                
            }
            else
            {
                return Ok(new{});
            }
            
        }

       

        [HttpPost()]
        public IActionResult Post([FromBody]policy policy)
        {
            bool res = service.InsertPolicy(policy);
            
            if(res)
            {
                return Ok(new {
                    type="success",
                    message=policy.name + " created successfully.",
                    slug = policy.slug
                });
            }
            else
            {
                return BadRequest(new {
                    type="success",
                    message="Policy create failed."
                });
            }
        }

        [HttpPost("{slug}")]
        public IActionResult Post(String slug, [FromBody]section section)
        {
            bool res = service.InsertSection(slug,section);
            if(res)
            {


                return Ok(new {
                    type="success",
                    message=section.title + " created successfully.",
                    slug = section.slug
                });
            }
            else
            {
                return BadRequest(new {
                    type="error",
                    message="Section create failed."
                });
            }
        }



        [HttpPut("{slug}")]
        public IActionResult Put(String slug, [FromBody]policy policy)
        {
            bool res = service.UpdatePolicy(slug, policy);

            if(res)
            {


                return Ok(new {
                    type="success",
                    message= policy.name + " updated successfully."
                });
            }
            else
            {
                return BadRequest(new {
                    type="error",
                    message="Policy update failed."
                });
            }
        }

        [HttpPut("{policySlug}/{sectionSlug}")]
        public IActionResult Put(String policySlug, String sectionSlug, [FromBody]section section)
        {
            bool res = service.UpdateSection(policySlug, sectionSlug, section);

            if(res)
            {
                return Ok(new {
                    type="success",
                    message= section.title + " updated successfully."
                });
            }
            else
            {
                return BadRequest(new {
                    type="error",
                    message="Section update failed."
                });
            }
        }

        [HttpDelete("{slug}")]
        public IActionResult Delete(String slug)
        {
            policy pol = null;
            pol = service.GetPolicyBySlug(slug);

            bool res = service.DeletePolicy(slug);

            if(res)
            {
                return Ok(new {
                    type="success",
                    message= pol.name + " deleted successfully."
                });
            }
            else
            {
                return BadRequest(new {
                    type="error",
                    message="Policy delete failed."
                });
            }
        }

        [HttpDelete("{policySlug}/{sectionSlug}")]
        public IActionResult Delete(String policySlug, String sectionSlug)
        {
            section sec = null;
            policy pol = null;
            
            pol = service.GetPolicyBySlug(policySlug);
            if(pol!=null)
            {
                sec = pol.sections.Find(s => s.slug == sectionSlug);
            }

            bool res = service.DeleteSection(policySlug,sectionSlug);

            if(res && sec!= null)
            {
                return Ok(new {
                    type="success",
                    message= sec.title + " deleted successfully."
                });
            }
            else
            {
                return BadRequest(new {
                    type="error",
                    message="Selection delete failed."
                });
            }
        }
    }
}