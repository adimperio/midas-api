﻿using kmbi_core_master.acctngModule.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.acctngModule.Interface
{
    public interface iacctngCvRepository
    {
        IEnumerable<acctngCv> all();

        IEnumerable<acctngCv> all(DateTime from, DateTime to);

        acctngCv getSlug(String slug);

        void add(acctngCv b);

        void update(String slug, acctngCv b);

        void remove(String slug);

        List<acctngCv> issuedAt(DateTime from, DateTime to);

        //CREDIT
        cvAccounts getCreditId(string creditId);

        void addCredit(cvCredit u, string crSlug);

        void updateCredit(String crlug, string creditId, cvCredit u);

        void removeCredit(String crSlug, string creditId);


        //DEBIT
        acctngCv getDebitId(string debitId);

        void addDebit(cvDebit u, string crSlug);

        void updateDebit(String crSlug, string debitId, cvDebit u);

        void removeDebit(String crSlug, string debitId);

        //Check existing receiptNo
        acctngCv checkCv(String cvNo);

        acctngCv checkCheque(String chequeNo);

        void updateIsCancelled(String slug, acctngCv b);

        void updateIsClosed(DateTime from, DateTime to, acctngCv b);
    }
}
