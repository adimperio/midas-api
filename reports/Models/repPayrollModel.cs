﻿using kmbi_core_master.coreMaster.Models;
using System;
using System.Collections.Generic;
using kmbi_core_master.payrollModule.Models;

namespace kmbi_core_master.reports.Models
{
    public class repPayrollModel {
        
        public String jobGradeDesc {get;set;}
        public DateTime cutOffFrom {get;set;}
        public DateTime cutOffTo {get;set;}
        public DateTime periodFrom {get;set;}
        public DateTime periodTo {get;set;}

        public List<repPayrollModelDetail> details {get;set;}
        public repPayrollModelDetail totals {get;set;}
    }
    public class repPayrollModelDetail {
        
        public String branch {get;set;}
        public String department {get;set;}
        public String employeeCode {get;set;}
        public String fullname {get;set;}
        public String jobGrade {get;set;}
        public String taxCode {get;set;}
        public Decimal monthlyRate {get;set;}
        public Decimal ratePerDay {get;set;}
        public Decimal basicPay {get;set;}
        public Decimal adjustment {get;set;}
        public Decimal holidayPay {get;set;}
        public Decimal overtimePay {get;set;}
        public Decimal overtimeAdjustment {get;set;}
        public Decimal absentLate {get;set;}

        public Decimal grossTaxableIncome {get;set;}

        public Decimal riceSubsidy {get;set;}
        public Decimal ap {get;set;}

        public Decimal grossPay {get;set;}

        public Decimal witholdingTax {get;set;}

        public Decimal hdmfPremiumContribution {get;set;}
        public Decimal hdmfCalamity {get;set;}
        public Decimal hdmfHousing {get;set;}
        public Decimal hdmfMpl {get;set;}
        public Decimal hdmfMp2 {get;set;}
        
        public Decimal sssPremiumContribution {get;set;}
        public Decimal sssSalaryLoan {get;set;}

        public Decimal phicPremiumContribution {get;set;}

        public Decimal fidelity {get;set;}
        public Decimal fhgLoanPayable {get;set;}
        public Decimal fhgShareCapital {get;set;}
        public Decimal fhgSavings {get;set;}
        public Decimal fhgInsurance {get;set;}
        public Decimal fhgOthers {get;set;}

        public Decimal ar {get;set;}
        public Decimal arOthers {get;set;}
        public Decimal arMisc {get;set;}
        public Decimal arPersonalCalls {get;set;}
        
        public Decimal statutory {get;set;}
                
        public Decimal totalDeduction {get;set;}
        
        public Decimal netPay {get;set;}
    }
}
