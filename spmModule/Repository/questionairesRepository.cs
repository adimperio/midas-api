﻿using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.spm.IRepository;
using kmbi_core_master.spm.Models;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace kmbi_core_master.spm.Repository
{
    public class questionairesRepository : dbCon, iquestionairesRepository
    {
        public questionairesRepository(IOptions<dbSettings> settings) : base(settings)
        {
        }

        public void add(questionaires b)
        {

            b.createdAt = DateTime.Now;
            b.updatedAt = DateTime.Now;

            for (int i = 0; i <= b.questions.Count - 1; i++)
            {
                b.questions[i].Id = ObjectId.GenerateNewId().ToString();
            }
            _db.GetCollection<questionaires>("questionaires").InsertOneAsync(b);
        }

        

        public IEnumerable<questionaires> getAll()
        {
            var d = _db.GetCollection<questionaires>("questionaires").Find(new BsonDocument()).ToListAsync();
            return d.Result;
        }



        public questionaires getSlug(String slug)
        {
            var query = Builders<questionaires>.Filter.Eq(e => e.slug, slug);
            var d = _db.GetCollection<questionaires>("questionaires").Find(query).ToListAsync();

            return d.Result.FirstOrDefault();
        }

     
        public void update(String slug, questionaires p)
        {
            p.slug = slug;

           
            var query = Builders<questionaires>.Filter.Eq(e => e.slug, slug);
            var update = Builders<questionaires>.Update
                .Set("name", p.name)
                .Set("version", p.version)
                .Set("questions", p.questions)
                .CurrentDate("updatedAt");

            var d = _db.GetCollection<questionaires>("questionaires").UpdateOneAsync(query, update);
        }

        
        public void delete(String slug)
        {
            var query = Builders<questionaires>.Filter.Eq(e => e.slug, slug);
            var d = _db.GetCollection<questionaires>("questionaires").DeleteOneAsync(query);
        }

       

        public IEnumerable<questionaires> search(String name)
        {

            var query = Builders<questionaires>.Filter.Regex("name", new BsonRegularExpression(name, options: "i"));
            var d = _db.GetCollection<questionaires>("questionaires").Find(query).ToListAsync();

            return d.Result;
        }

        public void sortByCode(List<string> id, string slug)
        {
            
            var qryGetSlug = Builders<questionaires>.Filter.Eq(e => e.slug, slug);
            

            int ctr = 0;
            questionaires q = new questionaires();
            List<spmQuestions> spmQ = new List<spmQuestions>();

            for (int i = 0; i <= id.Count - 1; i++)
            //foreach (var item in id)
            {
                var qryGetId = Builders<questionaires>.Filter.ElemMatch(e => e.questions , x => x.Id == id[i] );
                var query = Builders<questionaires>.Filter.And(qryGetSlug, qryGetId);
                var qryGetOrder = _db.GetCollection<questionaires>("questionaires").Find(query).FirstOrDefault();

                //spmQ.Add(new spmQuestions {
                //    Id = item,
                //    question = qryGetOrder.questions[ctr].question,
                //    order = ctr,
                //    isVisible = qryGetOrder.questions[ctr].isVisible,
                //    hasChoices = qryGetOrder.questions[ctr].hasChoices,
                //    choices = qryGetOrder.questions[ctr].choices
                //});

                var update = Builders<questionaires>.Update
                 .Set("questions.$.order", i)
                 .CurrentDate("updatedAt");

                var d = _db.GetCollection<questionaires>("questionaires").UpdateOneAsync(query, update);

                ctr += 1;
            }
            //q.Id = qryGetOrder.Id;
            //q.slug = qryGetOrder.slug;
            //q.name = qryGetOrder.name;
            //q.version = qryGetOrder.version;
            //q.questions = spmQ;

            

            //
            //var sort = Builders<questionaires>.Sort.Ascending("questions.order");
            //var qryGetSlug1 = Builders<questionaires>.Filter.Eq(e => e.slug, slug);
            //var qryGetOrder1 = _db.GetCollection<questionaires>("questionaires").Find(qryGetSlug1).Sort(sort).FirstOrDefault();
            //return qryGetOrder1;

            //var aggR = _db.GetCollection<questionairesSort>("questionaires").Aggregate()
            //       .Match(e => e.slug == slug)
            //       .Unwind("questions")
            //       .Sort(new BsonDocument
            //            {
            //                {"questions.order", 1}
            //            })
            //       .Project<questionairesSort>(new BsonDocument
            //            {
            //                {"slug",1},
            //                {"questions",1 }
            //            })
            //       .ToListAsync().Result;

            //return aggR;
        }
    }
}
