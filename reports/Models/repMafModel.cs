﻿using kmbi_core_master.coreMaster.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.reports.Models
{
    public class repMafModel {
        public repMafModelPersonnalInformation PersonalInformation {get;set;}
        public repMafModelGuarantor Guarantor {get;set;}
        public repMafModelBusinessInformation BusinessInformation {get;set;}
        public repMafModelPreQualificationInquiries PreQualificationInquiries {get;set;}
        public repMafModelInterviewWithNeighbors InterviewWithNeighbors {get;set;}
        public repMafModelDunamisInfo DunamisInfo {get;set;}
        public repMafModelMi Mi {get;set;}
        public loans FirstLoan {get;set;}
        public String BranchName {get;set;}
        public DateTime CreatedAt {get;set;}
        public DateTime UpdatedAt {get;set;}
    }

    public class repMafModelPersonnalInformation {
        public string Title {get;set;}
        public string Firstname {get;set;}
        public string Middlename {get;set;}
        public string LastName {get;set;}
        public string FullName { get; set; }
        public DateTime? Birthdate {get;set;}
        public string CivilStatus {get;set;}
        public string EducationAttainment {get;set;}
        public repMafModelPersonnalInformationContact[] Contacts {get;set;}
        public repMafModelPersonnalInformationAddress Address {get;set;}
        public string PicturePath {get;set;}
        public string Signature {get;set;}
        public int Age {get;set;}
        public String Birthplace {get;set;}
    }

    public class repMafModelPersonnalInformationContact {
        public string Type {get;set;}
        public string Value {get;set;}
    }

    public class repMafModelPersonnalInformationAddress {
        public string Region {get;set;}
        public string Province {get;set;}
        public string Municipality {get;set;}
        public string Barangay {get;set;}
        public string Subdivision {get;set;}
        public string Street {get;set;}
        public string Address { get; set; }
        public string Postal {get;set;}
        public int YearOfStay {get;set;}
        public int Year {get;set;}
    }

    public class repMafModelGuarantor {
        public string Firstname {get;set;}
        public string Middlename {get;set;}
        public string Lastname {get;set;}
        public string FullName { get; set; }
        public string Relationship {get;set;}
        public DateTime? Birthdate {get;set;}
        public string BusinessTypeEmployerName {get;set;}
        public string Position {get;set;}
        public string Address {get;set;}
        public string OfficeTelephone {get;set;}
        public string MobileNumber {get;set;}
        public int Age {get;set;}
    }

    public class repMafModelBusinessInformation {
        public string TypeOfBusiness {get;set;}
        public string NatureOfBusiness {get;set;}
        public string BusinessAddress {get;set;}
        public int? NoOfYears {get;set;}
        public int? Year {get;set;}
        public string OtherBusinessOfTheFamily {get;set;}
        public repMafModelBusinessInformationOtherLoanProviders[] OtherLoanProviders {get;set;}
    }

    public class repMafModelBusinessInformationOtherLoanProviders {
        public string NameOfProvider {get;set;}
        public int? MemberSince {get;set;}
        public string LoanType {get;set;}
        public double? Amount {get;set;}
        public int? Term {get;set;}
    }

    public class repMafModelPreQualificationInquiries {
        public repMafModelIncomeAndFinances QualificationInquiries {get;set;}
    }

    public class repMafModelIncomeAndFinances {
        public double WeeklyIncome {get;set;}
        public double WeeklyExpense {get;set;}
        public double NetDisposableIncome {get;set;}
        public double CreditLimit {get;set;}
        public DateTime? DateOfCreditInvestigation {get;set;}
    }

    public class repMafModelInterviewWithNeighbors {
        public string Name {get;set;}
        public string FeedbackFromNeighbors {get;set;}
    }

    public class repMafModelDunamisInfo {
        public string Center {get;set;}
        public int CurrentLoanCycle {get;set;}
        public double CurrentCbuBalance {get;set;}
        public repMafModelDunamisInfoIds[] Ids {get;set;}
        public repMafModelDunamisInfoAgriLoan AgriLoan {get;set;}
    }

    public class repMafModelDunamisInfoIds {
        public string Type {get;set;}
        public string Value {get;set;}
        public DateTime? DateIssued {get;set;}
        public string PlaceIssued {get;set;}
    }

    public class repMafModelDunamisInfoAgriLoan {
        public string LandHectares {get;set;}
    }

    public class repMafModelMi {
        public repMafModelBeneficiary Beneficiary {get;set;}
        public repMafModelDependents[] Dependents {get;set;}
    }

    public class repMafModelBeneficiary {
        public string Relationship {get;set;}
        public string Firstname {get;set;}
        public string Middlename {get;set;}
        public string Lastname {get;set;}
        public string FullName { get; set; }
        public DateTime? Birthdate {get;set;}
        public string Signature {get;set;}
        public int Age {get;set;}
    }

    public class repMafModelDependents {
        public string Relationship {get;set;}
        public string Firstname {get;set;}
        public string Middlename {get;set;}
        public string Lastname {get;set;}
        public string FullName { get; set; }
        public DateTime? Birthdate {get;set;}
        public string SchoolIncomeSourceEmployer {get;set;}
        public string GradePosition {get;set;}
        public int Age {get;set;}
    }

}
