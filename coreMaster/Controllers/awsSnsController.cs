﻿using Amazon;
using Amazon.CloudWatch;
using Amazon.CloudWatch.Model;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Amazon.SimpleEmail;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Amazon.SimpleNotificationService;
using Amazon.SimpleNotificationService.Model;
using Amazon.Runtime.Internal.Transform;
using Microsoft.Extensions.Options;
using kmbi_core_master.coreMaster.Models;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace kmbi_core_master.coreMaster.Controllers
{
    [Route("api/awsSns")]
    public class snsController : Controller
    {
        //private IHostingEnvironment hostingEnv;
        private awsSettings _settings;

        private static  string _awsAccessKey; 
        private static  string _awsSecretKey; 

        public snsController(IOptions<awsSettings> settings) 
        {
            _settings = settings.Value;
            _awsAccessKey = _settings.accessKey;
            _awsSecretKey = _settings.secretKey;
        }

        //public snsController(IHostingEnvironment env)
        //{
        //    this.hostingEnv = env;
        //}

        //public static void sendSMSMessage(AmazonSimpleNotificationServiceClient snsClient, String message, String phoneNumber, Dictionary<String, MessageAttributeValue> smsAttributes)
        //{

        //    var result = snsClient.PublishAsync(new PublishRequest
        //    {
        //        Message = message,
        //        PhoneNumber = phoneNumber,
        //        MessageStructure = "string",
        //        MessageAttributes = smsAttributes
        //    });

        //    //System.out.println(result); // Prints the message ID.
        //}

        [HttpPost]
        public IActionResult sendSMS()
        {
            
            using (var snsClient = new AmazonSimpleNotificationServiceClient(_awsAccessKey,_awsSecretKey, RegionEndpoint.APSoutheast1))
            {
                //List<pn> pnum = new List<pn>();
                //pnum = new List<pn>()
                //{
                // new pn { number = "+639175336883"},
                // new pn { number = "+639274350281"},
                // new pn { number = "+639230471309"},
                // new pn { number = "+639175709062"},
                // new pn { number = "+639752606058"},
                //};
                String message = "Good Afternoon, " + "\r\n" + "\r\n" + "We would just like to remind you of the KMBI Transformational Development Forum tomorrow at Sequoia Hotel located at 91-93 Mother Ignacia Avenue corner Timog Avenue, Quezon City. Registration will start at 8:00 AM. Attire would be Smart Casual. Parking slots are limited. Thank you and see you at the TD Forum." + "\r\n" + "\r\n" + "-Kabalikat Para Sa Maunlad na Buhay Inc.";
                String phoneNumber = "+639176722799";
                Dictionary<String, MessageAttributeValue> smsAttributes = 
                        new Dictionary<String, MessageAttributeValue>();
                smsAttributes.Add("AWS.SNS.SMS.SenderID", new MessageAttributeValue
                {
                    StringValue = "ITKMBI",
                    DataType = "String"
                });
                smsAttributes.Add("AWS.SNS.SMS.MaxPrice", new MessageAttributeValue
                {
                    StringValue = "1.0",
                    DataType = "Number"
                });
                smsAttributes.Add("AWS.SNS.SMS.SMSType", new MessageAttributeValue
                {
                    StringValue = "Promotional",
                    DataType = "String"
                });
                //<set SMS attributes>
                //sendSMSMessage(snsClient, message, phoneNumber, smsAttributes);
                var result = snsClient.PublishAsync(new PublishRequest
                {
                    Message = message,
                    PhoneNumber = phoneNumber,
                    MessageStructure = "string",
                    MessageAttributes = smsAttributes
                });

                return Ok(new
                {
                    message = result
                });
                
            }

        }

        public class pn
        {
            public string number { get; set; }
        }
    }
}