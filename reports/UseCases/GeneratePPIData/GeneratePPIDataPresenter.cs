

using System.Linq;
using OfficeOpenXml;

namespace kmbi_core_master.reports.UseCases.GeneratePPIData
{
    public class GeneratePPIDataPresenter : IPresent<GeneratePPIDataResponse, GeneratePPIDataPresenterResponse> {
        
        public GeneratePPIDataPresenterResponse Present(GeneratePPIDataResponse request) {

            using (OfficeOpenXml.ExcelPackage ep = new OfficeOpenXml.ExcelPackage())
            {
                ExcelWorksheet ew = ep.Workbook.Worksheets.Add("PPI Data");
                
                ew.Cells[1,1].Value = "PPI Data";
                ew.Cells[2,1].Value = "As of " + request.AsOf.ToShortDateString();

                ew.Cells[3,1].Value = "Name";
                ew.Cells[3,2].Value = "Branch";
                ew.Cells[3,3].Value = "Center";

                ew.Cells[3,4].Value = "Question 1";
                ew.Cells[3,5].Value = "Answer 1";

                ew.Cells[3,6].Value = "Question 2";
                ew.Cells[3,7].Value = "Answer 2";

                ew.Cells[3,8].Value = "Question 3";
                ew.Cells[3,9].Value = "Answer 3";

                ew.Cells[3,10].Value = "Question 4";
                ew.Cells[3,11].Value = "Answer 4";

                ew.Cells[3,12].Value = "Question 5";
                ew.Cells[3,13].Value = "Answer 5";

                ew.Cells[3,14].Value = "Question 6";
                ew.Cells[3,15].Value = "Answer 6";

                ew.Cells[3,16].Value = "Question 7";
                ew.Cells[3,17].Value = "Answer 7";

                ew.Cells[3,18].Value = "Question 8";
                ew.Cells[3,19].Value = "Answer 8";

                ew.Cells[3,20].Value = "Question 9";
                ew.Cells[3,21].Value = "Answer 9";

                ew.Cells[3,22].Value = "Question 10";
                ew.Cells[3,23].Value = "Answer 10";

                ew.Cells[3,24].Value = "Score";
                
                var line = 4;

                request.Details.ToList().ForEach(det=>{

                    ew.Cells[line,1].Value = det.Name;
                    ew.Cells[line,2].Value = det.Branch;
                    ew.Cells[line,3].Value = det.Center;

                    ew.Cells[line,4].Value = det.QuestionsAnswers[0].Question;
                    ew.Cells[line,5].Value = det.QuestionsAnswers[0].Answer;

                    ew.Cells[line,6].Value = det.QuestionsAnswers[1].Question;
                    ew.Cells[line,7].Value = det.QuestionsAnswers[1].Answer;

                    ew.Cells[line,8].Value = det.QuestionsAnswers[2].Question;
                    ew.Cells[line,9].Value = det.QuestionsAnswers[2].Answer;

                    ew.Cells[line,10].Value = det.QuestionsAnswers[3].Question;
                    ew.Cells[line,11].Value = det.QuestionsAnswers[3].Answer;

                    ew.Cells[line,12].Value = det.QuestionsAnswers[4].Question;
                    ew.Cells[line,13].Value = det.QuestionsAnswers[4].Answer;

                    ew.Cells[line,14].Value = det.QuestionsAnswers[5].Question;
                    ew.Cells[line,15].Value = det.QuestionsAnswers[5].Answer;

                    ew.Cells[line,16].Value = det.QuestionsAnswers[6].Question;
                    ew.Cells[line,17].Value = det.QuestionsAnswers[6].Answer;

                    ew.Cells[line,18].Value = det.QuestionsAnswers[7].Question;
                    ew.Cells[line,19].Value = det.QuestionsAnswers[7].Answer;

                    ew.Cells[line,20].Value = det.QuestionsAnswers[8].Question;
                    ew.Cells[line,21].Value = det.QuestionsAnswers[8].Answer;

                    ew.Cells[line,22].Value = det.QuestionsAnswers[9].Question;
                    ew.Cells[line,23].Value = det.QuestionsAnswers[9].Answer;

                    ew.Cells[line,24].Value = det.Score.ToString();
                    
                    line++;
                });


                ew.Row(1).Style.Font.Bold = true;
                ew.Row(2).Style.Font.Bold = true;
                ew.Row(3).Style.Font.Bold = true;

                ew.Column(1).Width = 35;
                ew.Column(2).Width = 17;
                ew.Column(3).Width = 11;

                for(var ctr=4;ctr<=24;ctr++)
                    ew.Column(ctr).Width = 35;
                
                ew.Column(24).Width = 10;

                byte[] bytes = ep.GetAsByteArray();
                ep.Dispose();

                return new GeneratePPIDataPresenterResponse {
                    data = request,
                    bytes = bytes
                };
            }

        }
    }
}