﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.coreMaster.Models
{
    public class loanTypes
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }
       
        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }


        [BsonElement("interestRate")]
        public double interestRate { get; set; }

        public amountRange amountRange { get; set; }

        public IList<frequency> frequency { get; set; }

        [BsonElement("maxClients")]
        public int maxClients { get; set; }

   
        [BsonElement("minClients")]
        public int minClients { get; set; }

        public IList<string> interestType { get; set; }

        [BsonElement("processingFee")]
        public double processingFee { get; set; }

      
        [BsonElement("initialCBUpercentage")]
        public double initialCBUpercentage { get; set; }

        [BsonElement("initialCBU")]
        public double initialCBU { get; set; }

        [BsonElement("cbuAmountPerTerm")]
        public double cbuAmountPerTerm { get; set; }

        [BsonElement("allowDownpayment")]
        public bool allowDownpayment { get; set; }

        public IList<string> modesOfPayment { get; set; }

        [BsonElement("createdAt")]
        public DateTime? createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime? updatedAt { get; set; }
    }

    public class frequency
    {
        [BsonElement("name")]
        public string name { get; set; }

        public IList<settings> settings { get; set; }
    }

    public class settings
    {
        [BsonElement("term")]
        public int term { get; set; }

        [BsonElement("eir")]
        public double eir { get; set; }
    }

    public class amountRange
    {
        [BsonElement("min")]
        public int min { get; set; }

        [BsonElement("max")]
        public double max { get; set; }
    }
}
