﻿using kmbi_core_master.adminModule.Inv.IRepository;
using kmbi_core_master.adminModule.Inv.Models;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Models;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace kmbi_core_master.adminModule.Inv.Repository
{
    public class inventoryRepository : dbCon, iinventoryRepository
    {
        public inventoryRepository(IOptions<dbSettings> settings) : base(settings)
        {
        }

        public void Add(inventory i)
        {

            i.createdAt = DateTime.Now;
            i.updatedAt = DateTime.Now;

            _db.GetCollection<inventory>("inventory").InsertOneAsync(i);
        }

        public IEnumerable<inventory> getAll()
        {
            var c = _db.GetCollection<inventory>("inventory").Find(new BsonDocument()).ToListAsync();
            return c.Result;
        }

        public inventory getBySlug(String slug)
        {
            var query = Builders<inventory>.Filter.Eq(e => e.slug, slug);
            var c = _db.GetCollection<inventory>("inventory").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }

       
        public void update(String slug, inventory p)
        {
            var query = Builders<inventory>.Filter.Eq(e => e.slug, slug);
            var update = Builders<inventory>.Update
                .Set("name", p.name)
                .Set("referenceNumber", p.referenceNumber)
                .Set("itemSlug", p.itemSlug)
                .Set("serialNo", p.serialNo)
                .Set("location", p.location)
                .Set("purchase", p.purchase)
                .Set("photo", p.photo)
                .Set("amount", p.amount)
                .Set("unitOfMeasure", p.unitOfMeasure)
                .Set("quantity", p.quantity)
                .Set("status", p.status)
                .Set("supplierName", p.supplierName)
                .Set("remarks", p.remarks)
                .Set("history", p.history)
                .Set("updatedBy", p.updatedBy)
                .CurrentDate("updatedAt");

            var l = _db.GetCollection<inventory>("inventory").UpdateOneAsync(query, update);

        }
       

        public void remove(String slug)
        {
            var query = Builders<inventory>.Filter.Eq(e => e.slug, slug);
            var result = _db.GetCollection<inventory>("inventory").DeleteOneAsync(query);
        }

        public inventory getByTagNumber(string tagNumber)
        {
            var query = Builders<inventory>.Filter.Eq(e => e.referenceNumber, tagNumber);
            var c = _db.GetCollection<inventory>("inventory").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }

        public Int32 getCountByName(String name) {
            var query = Builders<inventory>.Filter.Eq(e => e.name, name);
            var c = (Int32)_db.GetCollection<inventory>("inventory").Find(query).Count();
            return c;
        }
    }
}
