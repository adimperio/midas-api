﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.payrollModule.Models
{
    public class sss
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }
       
        [BsonElement("salaryRange")]
        public sssSalaryRange salaryRange { get; set; }

        [BsonElement("monthlySalaryCredit")]
        public double monthlySalaryCredit { get; set; }

        [BsonElement("contribution")]
        public sssContribution contribution { get; set; }

        [BsonElement("total")]
        public double total { get; set; }
    }

    public class sssSalaryRange
    {

        [BsonElement("from")]
        public double from { get; set; }

        [BsonElement("to")]
        public double to { get; set; }
    }

    public class sssContribution
    {
        [BsonElement("er")]
        public double er { get; set; }

        [BsonElement("ee")]
        public double ee { get; set; }

        [BsonElement("ec")]
        public double ec { get; set; }
    }

}
