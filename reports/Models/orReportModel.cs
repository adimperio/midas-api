using System;

namespace kmbi_core_master.reports.Models {
    public class orReportModel {

        public string branch {get;set;}
        public DateTime dateIssued {get;set;}
        public string receivedFrom {get;set;}
        public string tin {get;set;}
        public string address {get;set;}
        public string businessStyle {get;set;}
        public string amountInWords {get;set;}
        public double amountInNumber {get;set;}
        public string paymentFor {get;set;}

        public double loanPrincipal {get;set;}
        public double loanInterest {get;set;}
        public double capitalBuildUp {get;set;}
        public double processingFee {get;set;}
        public double miPremium {get;set;}
        public double miFee {get;set;}
        public double cgliPremium {get;set;}
        public double cgliFee {get;set;}
        public double dst {get;set;}
        public double others1Description {get;set;}
        public double others1Amount {get;set;}
        public double others2Description {get;set;}
        public double others2Amount {get;set;}
        public double others3Description {get;set;}
        public double others3Amount {get;set;}

        public double totalSales {get;set;}
        public double lessScPwdDisc {get;set;}
        public double amountDuelessScPwdDisc {get;set;}
        public double lessWitholdingTax {get;set;}
        public double amountDuelessWitholdingTax {get;set;}
    }
}