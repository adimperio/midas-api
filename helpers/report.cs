using System;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.IO;
using kmbi_core_master.helpers.interfaces;
using RazorLight;
using Microsoft.Extensions.Options;
using kmbi_core_master.coreMaster.Models;

namespace kmbi_core_master.helpers
{
    public class report : iReport {

        public IOptions<reportSetting> _reportSetting { get; }
        public report(IOptions<reportSetting> reportSetting)
        {
            _reportSetting = reportSetting;
        }

        public String templatePath {get;set;}
        public String templateFilename {get;set;}
        //public String destinationPath {get;set;}
        public String orientation {get;set;}
        public Boolean isHalf {get;set;}
        public String footerText {get;set;}
        public String title {get;set;}
        public String pageType {get;set;}
        public System.Dynamic.ExpandoObject viewBag {get;set;}
        public object model{get;set;}

        public iResult generatePDF()
        {
            const float pt = 72;
            var defaultxx =_reportSetting.Value.reportSize.defaultSize.x;
            var defaultyy = _reportSetting.Value.reportSize.defaultSize.y;
            var legallxx = _reportSetting.Value.reportSize.legalSize.x;
            var legalyy = _reportSetting.Value.reportSize.legalSize.y;

            Rectangle ori = new Rectangle((float)defaultxx*pt,(float)defaultyy*pt);

            if(pageType=="legal")
            {
                ori = new Rectangle((float)legallxx*pt,(float)legalyy*pt);
            }

            if(isHalf)
            {
                ori = new Rectangle(ori.Width, ori.Height / 2);
            }

            if(orientation!=null)
            {
                if(orientation!="")
                {
                    if(orientation=="landscape")
                    {
                        ori = ori.Rotate();
                    }
                }
            }



            Document document = new Document(ori,15,15,15,15);
            
            var ms = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(document, ms);

            document.AddTitle(title);


            if (footerText!=null)
            {
                if(footerText!="")
                {
                    var b = new Phrase(footerText);
                    b.Font.Size = 6;
                    b.Font.SetStyle("font-style: italic");

                    var a = new HeaderFooter(b,false);
                    a.Border = 0;
                    document.Footer = a;
                }
            }



            document.Open();

            HtmlWorker worker = new HtmlWorker(document);

            worker.StartDocument();

            String builder = "";

            
            // var eningex = EngineFactory.CreatePhysical(templatePath);
            // builder = eningex.Parse(templateFilename,model,viewBag);
            var engine = new EngineFactory();
            builder = engine.ForFileSystem(templatePath).CompileRenderAsync(templateFilename,model,typeof(object),viewBag).Result;
            //worker.Parse(new StringReader(builder));

            

            string[] separator = new string[] { @"<div style='page-break-before:always'></div>" };
            string[] pages = builder.Split(separator, StringSplitOptions.None);

            foreach (string page in pages)
            {
                document.NewPage();
                var htmlarraylist = HtmlWorker.ParseToList(new StringReader(page), null);

                for (int k = 0; k < htmlarraylist.Count; k++)
                {
                    document.Add((IElement)htmlarraylist[k]);
                }
            }

            worker.EndDocument();



            worker.Close();
            document.Close();
            writer.Close();

            // using (FileStream file = new FileStream(destinationPath, FileMode.Create, FileAccess.Write)) {
            //     ms.WriteTo(file);
            // }
            // ms.Dispose();


            iResult res = new emailResult();
            res.code = "";
            res.description = "";
            res.raw = builder;
            res.bytes = ms.ToArray();

            return res;
        }
    }

}