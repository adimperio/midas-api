using System;
using kmbi_core_master.coreMaster.Models;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace kmbi_core_master.reports.Models
{
    public class repMidas
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id {get;set;}
        public String referenceId {get;set;}
        public DateTime? disbursementDate {get;set;}
        public DateTime? lastPaymentDate { get; set; }
        public loanloanTypes loanType {get;set;}
        public loanMembers members {get;set;}
        public lmaidenName lmaidenName { get; set; }
       // public String lbirthplace { get; set; }
    }

    public class lmaidenName
    {
        public maidenName maidenName { get; set; }
        public String birthplace { get; set; }
    }
    public class maidenName
    {
        [BsonElement("first")]
        public string first { get; set; }

        [BsonElement("middle")]
        public string middle { get; set; }

        [BsonElement("last")]
        public string last { get; set; }
    }
}