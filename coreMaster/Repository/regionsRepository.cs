﻿using System;
using System.Collections.Generic;
using System.Linq;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Models;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;

namespace kmbi_core_master.coreMaster.Repository
{

    public class regionsRepository : dbCon, iregionsRepository
    {
        
        public regionsRepository(IOptions<dbSettings> settings) : base(settings)
        {
        }

        
        public void addRegion(regions region)
        {

            region.createdAt = DateTime.Now;
            region.updatedAt = DateTime.Now;
            for (int i = 0; i <= region.areas.Count - 1; i++)
            {
                region.areas[i].Id = ObjectId.GenerateNewId().ToString();
            }
            _db.GetCollection<regions>("region").InsertOneAsync(region);
        }

        public void addArea(areas area, string Id)
        {
            var query = Builders<regions>.Filter.Eq(e => e.Id, Id);
            var update = Builders<regions>.Update.Push(e => e.areas, area);
            area.Id = ObjectId.GenerateNewId().ToString();
            _db.GetCollection<regions>("region").FindOneAndUpdateAsync(query, update);
        }

        public IEnumerable<regions> allRegion()
        {
            var region = _db.GetCollection<regions>("region").Find(new BsonDocument()).ToListAsync();
            return region.Result;
        }   

        public regions getRegion(String id)
        {
            var query = Builders<regions>.Filter.Eq(e => e.Id, id);
            var region = _db.GetCollection<regions>("region").Find(query).ToListAsync();

            return region.Result.FirstOrDefault();
        }

        public regions getArea(string areaSlug)
        {
            var query = Builders<regions>.Filter.ElemMatch(x => x.areas, x => x.slug == areaSlug);
            var fields = Builders<regions>.Projection
                .Include(u => u.name)
                .Include(u => u.areas)
                .Include(u => u.slug)
                .Include(u => u.code)
                .Include(u => u.head)
                .ElemMatch(x => x.areas, x => x.slug == areaSlug);

            var region = _db.GetCollection<regions>("region").Find(query).Project<regions>(fields).ToListAsync();
            return region.Result.FirstOrDefault();
        }

        public regions getRegionSlug(String slug)
        {
            var query = Builders<regions>.Filter.Eq(e => e.slug, slug);
            var region = _db.GetCollection<regions>("region").Find(query).ToListAsync();

            return region.Result.FirstOrDefault();
        }

        public regions updateRegion(String slug, regions p)
        {
          p.slug = slug;
          var areaCount = p.areas.Count;
          var query = Builders<regions>.Filter.Eq(e => e.slug, slug);
          var areas = _db.GetCollection<regions>("region").Find(query).FirstOrDefault().areas;

          for (int i = 0; i <= areaCount - 1; i++)
          {
            var areaIndex = Array.FindIndex(areas.ToArray(), a => a.slug == p.areas[i].slug);
            if (areaIndex < 0) {
              var id = ObjectId.GenerateNewId().ToString();
              p.areas[i].Id = ObjectId.GenerateNewId().ToString();
            }
          }

          var update = Builders<regions>.Update
              .Set("name", p.name)
              .Set("code", p.code)
              .Set("head", p.head)
              .Set("areas", p.areas)
              .CurrentDate("updatedAt");

          var region = _db.GetCollection<regions>("region").UpdateOneAsync(query, update);

          var getQuery = Builders<regions>.Filter.Eq(e => e.slug, p.slug);
          var getRegion = _db.GetCollection<regions>("region").Find(query).ToListAsync();
          return getRegion.Result.FirstOrDefault();
        }

        public void updateArea(string regId, string areaId,  areas area)
        {
            area.Id = regId;
            
            var query = Builders<regions>.Filter.And(Builders<regions>.Filter.Eq(x => x.Id, regId),
            Builders<regions>.Filter.ElemMatch(x => x.areas, x => x.Id == areaId));
           
            var update = Builders<regions>.Update
                .Set("areas.$.name", area.name)
                .Set("areas.$.code", area.code)
                .Set("areas.$.head", area.head)
                .CurrentDate("updatedAt");

            var result = _db.GetCollection<regions>("region").UpdateOneAsync(query, update);
            

        }

        public void removeRegion(String slug)
        {
            var query = Builders<regions>.Filter.Eq(e => e.slug, slug);
            var result = _db.GetCollection<regions>("region").DeleteOneAsync(query);

            //return getRegion(slug) == null;
            //return true;
        }

        public void removeArea(String regSlug, string areaSlug)
        {
           
            var pull = Builders<regions>.Update.PullFilter(x => x.areas, a => a.slug == areaSlug);

            var filter1 = Builders<regions>.Filter.And(Builders<regions>.Filter.Eq(a => a.slug, regSlug),
                          Builders<regions>.Filter.ElemMatch(q => q.areas, t => t.slug == areaSlug));

            var result = _db.GetCollection<regions>("region").UpdateOneAsync(filter1, pull);
            // return getRegion(regSlug) == null;
            //return true;
        }

        public IEnumerable<regions> searchRegion(String record)
        {

            var query = Builders<regions>.Filter.Regex("name", new BsonRegularExpression(record, options: "i"));
            var region = _db.GetCollection<regions>("region").Find(query).ToListAsync();

            return region.Result;
        }

        public regions checkSlug(String slug)
        {
            var query = Builders<regions>.Filter.Eq(e => e.slug, slug);
            var region = _db.GetCollection<regions>("region").Find(query).ToListAsync();

            return region.Result.FirstOrDefault();
        }

        public regions checkRegionCode(String regionCode)
        {
            var query = Builders<regions>.Filter.Eq(e => e.code, regionCode);
            var region = _db.GetCollection<regions>("region").Find(query).ToListAsync();

            return region.Result.FirstOrDefault();
        }

        public regions checkAreaCode(String areaCode)
        {
            var query = Builders<regions>.Filter.ElemMatch(e => e.areas, g => g.code == areaCode );
            var region = _db.GetCollection<regions>("region").Find(query).ToListAsync();

            return region.Result.FirstOrDefault();
        }
    }
}
