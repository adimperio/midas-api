using System;

namespace kmbi_core_master.helpers.interfaces
{
    public interface iupload
    {
        String upload(Byte[] bytes, String fullPath);
        void delete(String fullPath);
        String getRoot();
    }
}




