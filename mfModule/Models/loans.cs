﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.coreMaster.Models
{

    public class loans

    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("referenceId")]
        public string referenceId { get; set; }

        [BsonElement("centerId")]
        public string centerId { get; set; }

        [BsonElement("branchId")]
        public string branchId { get; set; }

        [BsonElement("officerId")]
        public string officerId { get; set; }

        [BsonElement("officerName")]
        public string officerName { get; set; }

        [BsonElement("unitId")]
        public string unitId { get; set; }

        [BsonElement("unitCode")]
        public string unitCode { get; set; }

        [BsonElement("centerSlug")]
        public string centerSlug { get; set; }

        [BsonElement("centerName")]
        public string centerName { get; set; }

        [BsonElement("branchSlug")]
        public string branchSlug { get; set; }

        [BsonElement("branchName")]
        public string branchName { get; set; }

        [BsonElement("officerSlug")]
        public string officerSlug { get; set; }

        [BsonElement("unitSlug")]
        public string unitSlug { get; set; }

        [BsonElement("providerId")]
        public string providerId { get; set; }

        [BsonElement("centerLoanCycle")]
        public int centerLoanCycle { get; set; }

        [BsonElement("disbursementDate")]
        public DateTime? disbursementDate { get; set; }

        [BsonElement("firstPaymentDate")]
        public DateTime? firstPaymentDate { get; set; }

        [BsonElement("lastPaymentDate")]
        public DateTime? lastPaymentDate { get; set; }

        [BsonElement("loanType")]
        public loanloanTypes loanType { get; set; }

        [BsonElement("amounts")]
        public amounts amounts { get; set; }

        [BsonElement("status")]
        public string status { get; set; }

        public IList<loanMembers> members { get; set; }

        public List<string> previousMembers { get; set; }

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }

        [BsonElement("approvalStatus")]
        public string approvalStatus { get; set; }

        [BsonElement("regionSlug")]
        public string regionSlug { get; set; }

        [BsonElement("areaSlug")]
        public string areaSlug { get; set; }
    }


    public class amounts
    {
        [BsonElement("principalAmount")]
        public double principalAmount { get; set; }

        [BsonElement("interestAmount")]
        public double interestAmount { get; set; }

        [BsonElement("vatAmount")]
        public double vatAmount { get; set; }

        [BsonElement("cbuAmount")]
        public double cbuAmount { get; set; }

        [BsonElement("loanProcessingFee")]
        public double loanProcessingFee { get; set; }

        [BsonElement("miPremium")]
        public double miPremium { get; set; }

        [BsonElement("cgliAmount")]
        public double cgliAmount { get; set; }
    }

    public class loanloanTypes
    {
        [BsonElement("id")]
        public string id { get; set; }

        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("interestRate")]
        public double interestRate { get; set; }

        [BsonElement("frequency")]
        public string frequency { get; set; }

        [BsonElement("term")]
        public int term { get; set; }

        [BsonElement("eir")]
        public double eir { get; set; }

        [BsonElement("maxClients")]
        public int maxClients { get; set; }

        [BsonElement("minClients")]
        public int minClients { get; set; }

        [BsonElement("interestType")]
        public string interestType { get; set; }

        [BsonElement("processingFee")]
        public double processingFee { get; set; }

        [BsonElement("initialCBUpercentage")]
        public double initialCBUpercentage { get; set; }

        [BsonElement("initialCBU")]
        public double initialCBU { get; set; }

        [BsonElement("allowDownpayment")]
        public bool allowDownpayment { get; set; }

        [BsonElement("dayOfWeek")]
        public int dayOfWeek { get; set; }

        [BsonElement("cbu")]
        public double cbu { get; set; }

        public IList<string> modesOfPayment { get; set; }
    }


    public class getloanTypes
    {

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("name")]
        public string name { get; set; }


        [BsonElement("interestRate")]
        public double interestRate { get; set; }

        public IList<frequency> frequency { get; set; }

        [BsonElement("maxClients")]
        public int maxClients { get; set; }


        [BsonElement("minClients")]
        public int minClients { get; set; }

        public IList<string> interestType { get; set; }

        [BsonElement("processingFee")]
        public double processingFee { get; set; }

        [BsonElement("initialCBUpercentage")]
        public double initialCBUpercentage { get; set; }

        [BsonElement("initialCBU")]
        public double initialCBU { get; set; }

        [BsonElement("cbuAmountPerTerm")]
        public double cbuAmountPerTerm { get; set; }

        [BsonElement("allowDownpayment")]
        public bool allowDownpayment { get; set; }

        public IList<string> modesOfPayment { get; set; }
    }



    public class getbyBranchloans

    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("referenceId")]
        public string referenceId { get; set; }

        [BsonElement("centerId")]
        public string centerId { get; set; }

        [BsonElement("branchId")]
        public string branchId { get; set; }

        [BsonElement("officerId")]
        public string officerId { get; set; }

        [BsonElement("officerName")]
        public string officerName { get; set; }

        [BsonElement("unitId")]
        public string unitId { get; set; }

        [BsonElement("unitCode")]
        public string unitCode { get; set; }

        [BsonElement("centerSlug")]
        public string centerSlug { get; set; }

        [BsonElement("centerName")]
        public string centerName { get; set; }

        [BsonElement("branchSlug")]
        public string branchSlug { get; set; }

        [BsonElement("branchName")]
        public string branchName { get; set; }

        [BsonElement("officerSlug")]
        public string officerSlug { get; set; }

        [BsonElement("unitSlug")]
        public string unitSlug { get; set; }

        [BsonElement("providerId")]
        public string providerId { get; set; }

        [BsonElement("centerLoanCycle")]
        public int centerLoanCycle { get; set; }

        [BsonElement("disbursementDate")]
        public DateTime? disbursementDate { get; set; }

        [BsonElement("firstPaymentDate")]
        public DateTime? firstPaymentDate { get; set; }

        [BsonElement("lastPaymentDate")]
        public DateTime? lastPaymentDate { get; set; }

        [BsonElement("loanType")]
        public loanloanTypes loanType { get; set; }

        [BsonElement("amounts")]
        public amounts amounts { get; set; }

        [BsonElement("status")]
        public string status { get; set; }

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("totalMembers")]
        public int totalMembers { get; set; }
    }

    public class getbyBranchCenterloans
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("referenceId")]
        public string referenceId { get; set; }

        [BsonElement("centerId")]
        public string centerId { get; set; }

        [BsonElement("branchId")]
        public string branchId { get; set; }

        [BsonElement("officerId")]
        public string officerId { get; set; }

        [BsonElement("officerName")]
        public string officerName { get; set; }

        [BsonElement("unitId")]
        public string unitId { get; set; }

        [BsonElement("unitCode")]
        public string unitCode { get; set; }

        [BsonElement("centerSlug")]
        public string centerSlug { get; set; }

        [BsonElement("centerName")]
        public string centerName { get; set; }

        [BsonElement("branchSlug")]
        public string branchSlug { get; set; }

        [BsonElement("branchName")]
        public string branchName { get; set; }

        [BsonElement("officerSlug")]
        public string officerSlug { get; set; }

        [BsonElement("unitSlug")]
        public string unitSlug { get; set; }

        [BsonElement("providerId")]
        public string providerId { get; set; }

        [BsonElement("centerLoanCycle")]
        public int centerLoanCycle { get; set; }

        [BsonElement("disbursementDate")]
        public DateTime? disbursementDate { get; set; }

        [BsonElement("firstPaymentDate")]
        public DateTime? firstPaymentDate { get; set; }

        [BsonElement("lastPaymentDate")]
        public DateTime? lastPaymentDate { get; set; }

        [BsonElement("loanType")]
        public loanloanTypes loanType { get; set; }

        [BsonElement("amounts")]
        public amounts amounts { get; set; }

        [BsonElement("status")]
        public string status { get; set; }

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        public IList<loanMembers> members { get; set; }

        [BsonElement("totalMembers")]
        public int totalMembers { get; set; }
    }

    public class loansCBU
    {
        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("cbu")]
        public double cbu { get; set; }

        [BsonElement("loan")]
        public cbuLoan loan { get; set; }

        [BsonElement("type")]
        public string type { get; set; }

        [BsonElement("transactedAt")]
        public DateTime? transactedAt { get; set; }
    }

    public class clientId
    {
        [BsonElement("memberId")]
        public string memberId { get; set; }
    }


    public class syncLoans

    {
        [BsonId]
        public ObjectId Id { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("referenceId")]
        public string referenceId { get; set; }

        [BsonElement("centerId")]
        public string centerId { get; set; }

        [BsonElement("branchId")]
        public string branchId { get; set; }

        [BsonElement("officerId")]
        public string officerId { get; set; }

        [BsonElement("officerName")]
        public string officerName { get; set; }

        [BsonElement("unitId")]
        public string unitId { get; set; }

        [BsonElement("unitCode")]
        public string unitCode { get; set; }

        [BsonElement("centerSlug")]
        public string centerSlug { get; set; }

        [BsonElement("centerName")]
        public string centerName { get; set; }

        [BsonElement("branchSlug")]
        public string branchSlug { get; set; }

        [BsonElement("branchName")]
        public string branchName { get; set; }

        [BsonElement("officerSlug")]
        public string officerSlug { get; set; }

        [BsonElement("unitSlug")]
        public string unitSlug { get; set; }

        [BsonElement("providerId")]
        public string providerId { get; set; }

        [BsonElement("centerLoanCycle")]
        public int centerLoanCycle { get; set; }

        [BsonElement("disbursementDate")]
        public DateTime? disbursementDate { get; set; }

        [BsonElement("firstPaymentDate")]
        public DateTime? firstPaymentDate { get; set; }

        [BsonElement("lastPaymentDate")]
        public DateTime? lastPaymentDate { get; set; }

        [BsonElement("loanType")]
        public loanloanTypes loanType { get; set; }

        [BsonElement("amounts")]
        public amounts amounts { get; set; }

        [BsonElement("status")]
        public string status { get; set; }

        public IList<loanMembers> members { get; set; }

        public List<string> previousMembers { get; set; }

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }
    }



    //public class arrayLoans
    //{
    //    public List<loans> loans { get; set; }
    //}


    public class arrayLoans
    {
        public string slug { get; set; }
        public List<loans> loans { get; set; }
    }

    public class loanBalances
    {
        public string slug { get; set; }
        public string referenceId { get; set; }
        public string centerName { get; set; }
        public loanRemaining amounts { get; set; }
    }

    public class loanRemaining
    {
        public double principal { get; set; }
        public double interest { get; set; }
        public double cbu { get; set; }
        public double total { get; set; }
    }

    public class loanPayRep
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        public loanPayMem members { get; set; }
        public loanPayLoans loans { get; set; }
        public double prinPayment { get; set; }
        public double intPayment { get; set; }
        public double cbuPayment { get; set; }
    }

    public class loanPayMem
    {
        public string slug { get; set; }
    }

    public class loanPayLoans
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }
        public string referenceId { get; set; }
        public string status { get; set; }
        public string centerName { get; set; }
        public loanMembers members { get; set; }
    }

    public class loanPortfolio
    {
        public double principal { get; set; }
        public double interest { get; set; }
        public double cbu { get; set; }
    }


    public class getLoansByBranch

    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("branchId")]
        public string branchId { get; set; }

        [BsonElement("referenceId")]
        public string referenceId { get; set; }

        [BsonElement("centerSlug")]
        public string centerSlug { get; set; }

        [BsonElement("amounts")]
        public amounts amounts { get; set; }

        [BsonElement("totalMembers")]
        public int totalMembers { get; set; }

        [BsonElement("centerLoanCycle")]
        public int centerLoanCycle { get; set; }

        [BsonElement("disbursementDate")]
        public DateTime? disbursementDate { get; set; }

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("status")]
        public string status { get; set; }

    }

    public class loansByTypeBranchStatus
    {

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("referenceId")]
        public string referenceId { get; set; }

        [BsonElement("centerName")]
        public string centerName { get; set; }

        [BsonElement("branchSlug")]
        public string branchSlug { get; set; }
        
        [BsonElement("unitSlug")]
        public string unitSlug { get; set; }

        [BsonElement("loanType")]
        public loanloanTypes loanType { get; set; }

        [BsonElement("amounts")]
        public amounts amounts { get; set; }

        [BsonElement("status")]
        public string status { get; set; }

        public IList<loanMembers> members { get; set; }

    }


    public class loansRegionArea

    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("referenceId")]
        public string referenceId { get; set; }

        [BsonElement("centerId")]
        public string centerId { get; set; }

        [BsonElement("branchId")]
        public string branchId { get; set; }

        [BsonElement("officerId")]
        public string officerId { get; set; }

        [BsonElement("officerName")]
        public string officerName { get; set; }

        [BsonElement("unitId")]
        public string unitId { get; set; }

        [BsonElement("unitCode")]
        public string unitCode { get; set; }

        [BsonElement("centerSlug")]
        public string centerSlug { get; set; }

        [BsonElement("centerName")]
        public string centerName { get; set; }

        [BsonElement("branchSlug")]
        public string branchSlug { get; set; }

        [BsonElement("branchName")]
        public string branchName { get; set; }

        [BsonElement("officerSlug")]
        public string officerSlug { get; set; }

        [BsonElement("unitSlug")]
        public string unitSlug { get; set; }

        [BsonElement("providerId")]
        public string providerId { get; set; }

        [BsonElement("centerLoanCycle")]
        public int centerLoanCycle { get; set; }

        [BsonElement("disbursementDate")]
        public DateTime? disbursementDate { get; set; }

        [BsonElement("firstPaymentDate")]
        public DateTime? firstPaymentDate { get; set; }

        [BsonElement("lastPaymentDate")]
        public DateTime? lastPaymentDate { get; set; }

        [BsonElement("loanType")]
        public loanloanTypes loanType { get; set; }

        [BsonElement("amounts")]
        public amounts amounts { get; set; }

        [BsonElement("status")]
        public string status { get; set; }

        public IList<loanMembers> members { get; set; }

        public List<string> previousMembers { get; set; }

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }

        public string regionName { get; set; }

        public string regionSlug { get; set; }

        public string areaName { get; set; }

        public string areaId { get; set; }
    }

    public class loansECR
    {
        public loans loans { get; set; }
        public List<paymentLoans> paymentLoans { get; set; }

    }


}
