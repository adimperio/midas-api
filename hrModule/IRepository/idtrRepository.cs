﻿using kmbi_core_master.hrModule.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.hrModule.Interface
{
    public interface idtrRepository
    {
        IEnumerable<dtr> all();

        dtr getSlug(String slug);

        void add(dtr b);

        void update(String slug, dtr b);

        void remove(String slug);

        void add(List<dtr> dtrs);

        List<dtr> getDtrPerEmployeeCodePerDateRange(String employeeCode, DateTime from, DateTime to);
        List<dtrNameTime> getDtrPerBranchSlugPerDateRange(String branchSlug, DateTime from, DateTime to);
        Int32 getOverlappedAttendances(String employeeCode, DateTime from, DateTime to);

        List<dtr> getDtrPerDate(DateTime date);
        List<dtr> getDtrPerDateRange(DateTime from, DateTime to);
    }
}
