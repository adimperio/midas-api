﻿using kmbi_core_master.acctngModule.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.acctngModule.Interface
{
    public interface iacctngRfpRepository
    {
        IEnumerable<acctngRfp> all();

        acctngRfp getSlug(String slug);

        void add(acctngRfp b);

        void update(String slug, acctngRfp b);

        void remove(String slug);

        //CREDIT
        rfpAccounts getCreditId(string creditId);

        void addCredit(rfpCredit u, string rfpSlug);

        void updateCredit(String rfpSlug, string creditId, rfpCredit u);

        void removeCredit(String rfpSlug, string creditId);


        //DEBIT
        acctngRfp getDebitId(string debitId);

        void addDebit(rfpDebit u, string rfpSlug);

        void updateDebit(String rfpSlug, string debitId, rfpDebit u);

        void removeDebit(String rfpSlug, string debitId);
    }
}
