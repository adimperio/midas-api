using System;
namespace kmbi_core_master.helpers.interfaces
{
    public interface iNotification
    {
        String from {get;set;}
        String to {get;set;}
        String subject {get;set;}
        String message {get;set;}
        
        iResult Send();
    }
}