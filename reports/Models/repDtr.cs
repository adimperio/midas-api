﻿using kmbi_core_master.coreMaster.Models;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.reports.Models
{

    public class repDtr
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("slug")]
        public String slug { get; set; }

        [BsonElement("employeeCode")]
        public String employeeCode { get; set; }

        [BsonElement("date")]
        public DateTime date { get; set; }

        [BsonElement("schedule")]
        public repSchedule schedule { get; set; }

        [BsonElement("actual")]
        public repActual actual { get; set; }

        [BsonElement("manual")]
        public repManual manual { get; set; }

        [BsonElement("late")]
        public repLate late { get; set; }

        [BsonElement("undertime")]
        public repUndertime undertime { get; set; }

        [BsonElement("absent")]
        public repAbsent absent { get; set; }

        [BsonElement("overtime")]
        public repOvertime overtime { get; set; }

        [BsonElement("hoursWork")]
        public repHoursWork hoursWork { get; set; }

        [BsonElement("nd")]
        public double nd { get; set; }

        [BsonElement("remarks")]
        public string remarks { get; set; }

        [BsonElement("name")]
        public String name { get; set; }

        [BsonElement("position")]
        public String position { get; set; }

        [BsonElement("group")]
        public String group { get; set; }

        [BsonElement("department")]
        public String department { get; set; }

        [BsonElement("division")]
        public String division { get; set; }

        [BsonElement("section")]
        public String section { get; set; }

        [BsonElement("region")]
        public String region { get; set; }

        [BsonElement("area")]
        public String area { get; set; }

        [BsonElement("branch")]
        public String branch { get; set; }

        [BsonElement("unit")]
        public String unit { get; set; }

        [BsonElement("status")]
        public String status { get; set; }

    }

    public class repSchedule
    {
        [BsonElement("inAm")]
        public DateTime inAm { get; set; }

        [BsonElement("outAm")]
        public DateTime outAm { get; set; }

        [BsonElement("inPm")]
        public DateTime inPm { get; set; }

        [BsonElement("outPm")]
        public DateTime outPm { get; set; }

    }

    public class repActual
    {
        [BsonElement("inAm")]
        public DateTime? inAm { get; set; }

        [BsonElement("outAm")]
        public DateTime? outAm { get; set; }

        [BsonElement("inPm")]
        public DateTime? inPm { get; set; }

        [BsonElement("outPm")]
        public DateTime? outPm { get; set; }

    }

    public class repManual : repActual
    {

    }

    public class repLate
    {
        [BsonElement("am")]
        public double am { get; set; }

        [BsonElement("pm")]
        public double pm { get; set; }

    }

    public class repUndertime
    {
        [BsonElement("am")]
        public double am { get; set; }

        [BsonElement("pm")]
        public double pm { get; set; }

    }

    public class repAbsent
    {
        [BsonElement("am")]
        public double am { get; set; }

        [BsonElement("pm")]
        public double pm { get; set; }

    }

    public class repOvertime
    {
        [BsonElement("am")]
        public double am { get; set; }

        [BsonElement("pm")]
        public double pm { get; set; }

    }

    public class repHoursWork
    {
        [BsonElement("am")]
        public double am { get; set; }

        [BsonElement("pm")]
        public double pm { get; set; }

    }


}
