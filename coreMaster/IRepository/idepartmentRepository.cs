﻿using kmbi_core_master.coreMaster.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.coreMaster.IRepository
{
    public interface idepartmentRepository
    {
        IEnumerable<departments> getAll();

        departments getSlug(String slug);

        void add(departments b);

        void update(String slug, departments b);

        void delete(String slug);

        IEnumerable<departments> search(string name);

    
    }
}
