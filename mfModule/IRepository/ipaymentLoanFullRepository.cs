﻿using kmbi_core_master.mfModule.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.coreMaster.IRepository
{
    public interface ipaymentLoanFullRepository
    {
        IEnumerable<paymentLoanFull> all();

        paymentLoanFull getSlug(String slug);

        paymentLoanFull getId(String id);

        IEnumerable<paymentLoanFull> getIdWeekNo(String id);

        void add(paymentLoanFull b);

        void update(String id, paymentLoanFull b);

        void updateSlug(String slug, paymentLoanFull b);

        void remove(String id);

        void removeSlug(String slug);

    }
}
