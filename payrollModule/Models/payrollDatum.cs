using System;
using System.Collections.Generic;
using kmbi_core_master.documentationModule.Models;
using kmbi_core_master.hrModule.Models;
using kmbi_core_master.payrollModule.Models;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace kmbi_core_master.coreMaster.Models {

    public class payrollDatum {
        public String branchSlug {get;set;}
        public String branchName {get;set;}
        public IEnumerable<holiday> holidays {get;set;}
        public List<payEnt> payrolls {get;set;}
        public List<payrollEmployee> employees {get;set;}
    }

    public class payrollEmployee {
        public String employeeSlug {get;set;}
        public String employeeCode {get;set;}
        public String fullname {get;set;}
        public String jobGrade {get;set;}
        public String status {get;set;}
        public List<benefit> benefits {get;set;}
        public payrollDeduction deduction {get;set;}
        public List<otLog> otLogs {get;set;}
        public List<dtr> dtrs {get;set;}
        public fhgDeductionExtractedForPayroll fhgDeduction {get;set;}
    }
}