﻿namespace kmbi_core_master.coreMaster.Models
{
    public class dbSettings
    {
        public string Database { get; set; }
        public string MongoConnection { get; set; }
        public string ServerAddress { get; set; }
    }
}
