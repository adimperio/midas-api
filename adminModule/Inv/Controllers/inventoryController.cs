﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Filters;
using Microsoft.AspNetCore.Cors;
using kmbi_core_master.adminModule.Inv.IRepository;
using kmbi_core_master.adminModule.Inv.Models;
using kmbi_core_master.coreMaster;

namespace kmbi_core_master.adminModule.Inv.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/inventory")]
    [usersAuth]
    public class inventoryController : Controller
    {
        private readonly iinventoryRepository _Repository;
        private readonly iusersRepository _logInfo;
        private string token;

        public inventoryController(iinventoryRepository Repositor, iusersRepository logInfo)
        {
            this._Repository = Repositor;
            _logInfo = logInfo;
        }

        public void logger(string message)
        {
            //Insert Log//
            token = HttpContext.Request.Headers["token"];
            var lInfo = _logInfo.getLoginInfo(token);
            string log = message;
            global.createLog(lInfo.id, lInfo.fullname, lInfo.employment.branch, log);
            //Insert Log//
        }

        // GET: 
        [HttpGet]
        public IEnumerable<inventory> GetAll()
        {
            var c = _Repository.getAll();
            return c;

        }

       
        [HttpGet("getBySlug")]
        public IActionResult getSlug([FromQuery] string slug)
        {

            var item = _Repository.getBySlug(slug);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }


        [HttpGet("getByRefNumber")]
        public IActionResult getByRefNumber([FromQuery] string referenceNumber)
        {

            var item = _Repository.getByTagNumber(referenceNumber);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        [HttpPost]
        [errorHandler]
        public IActionResult create([FromBody] inventory i)
        {

            _Repository.Add(i);

            logger("Insert new inventory " + i.name);

            return Ok(new
            {
                type = "success",
                message = i.name + " created successfully.",
                slug = i.slug
            });

        }



        [HttpPut]
        public IActionResult slugupdate([FromQuery] string slug, [FromBody] inventory i)
        {
            var item = _Repository.getBySlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _Repository.update(slug, i);

           logger("Modified inventory " + i.name);

            return Ok(new
            {
                type = "success",
                message = i.name + " updated successfully."
            });
        }



        [HttpDelete("remove")]
        public IActionResult remove([FromQuery] string slug)
        {

            var item = _Repository.getBySlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.remove(slug);

            logger("Delete inventory " + item.name);

            return Ok(new
            {
                type = "success",
                message = item.name + " deleted successfully."
            });
        }

        [HttpGet("getCountByName")]
        public IActionResult getCountByName([FromQuery]string name) {
            var count = _Repository.getCountByName(name);
            return Ok(new {
                count = count
            });
        }
    }
}
