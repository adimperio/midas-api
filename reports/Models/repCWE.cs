using System;

namespace kmbi_core_master.reports.Models
{
    public class repCWE
    {
        public String no {get;set;}
        public String accountno {get;set;}
        public String nameofclient {get;set;}
        public String loanCycle {get;set;}
        public String loanApplied {get;set;}
        public String cashFlowDecision {get;set;}
        public String recommendation {get;set;}
    }
}