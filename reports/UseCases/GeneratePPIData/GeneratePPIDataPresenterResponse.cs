namespace kmbi_core_master.reports.UseCases.GeneratePPIData
{
    public class GeneratePPIDataPresenterResponse {
        public GeneratePPIDataResponse data {get;set;}
        public byte[] bytes {get;set;}
    }
}