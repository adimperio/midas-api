﻿using kmbi_core_master.hrModule.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.hrModule.Interface
{
    public interface idtrManualRepository
    {
        dtrManual getDtrManualPerEmployeeCodePerDate(String employeeCode, DateTime date);
        List<dtrManual> getDtrManualPerEmployeeCodePerDateRange(String employeeCode, DateTime from, DateTime to);
        Boolean createDtrManual(dtrManual dtrManual);
        Boolean updateDtrManual(String slug, dtrManual dtrManual);
        Boolean deleteDtrManual(String slug);
        Boolean replaceDtrManualPerEmployeeCodePerDate(String employeeCode, DateTime date, dtrManual dtrManual);
    }
}
