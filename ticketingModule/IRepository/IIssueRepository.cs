using kmbi_core_master.coreMaster.Controllers;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.ticketing.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.ticketing.IRepository
{
  public interface IIssueRepository
  {
    IEnumerable<IssueFull> all();
    IssueFull find(String slug);
    Comment getCommentBySlug(string slug);
    void create(Issue issue);
    void update(String slug, Issue issue);
    void delete(string slug);
    Boolean updateComment(string slug, Comment comment);
  }
}
