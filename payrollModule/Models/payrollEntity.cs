﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.payrollModule.Models
{
    public class payrollEntity
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }
       
        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("type")]
        public string type { get; set; }

        [BsonElement("employee")]
        public payEmployee employee { get; set; }

        [BsonElement("monthlyAmount")]
        public double monthlyAmount { get; set; }

        [BsonElement("dateInclusive")]
        public payDateInclusive dateInclusive { get; set; }
     
        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }
    }

    public class payEmployee
    {
        [BsonElement("id")]
        public string id { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }
    }

    public class payDateInclusive
    {
        [BsonElement("from")]
        public DateTime? from { get; set; }

        [BsonElement("to")]
        public DateTime? to { get; set; }
    }
}
