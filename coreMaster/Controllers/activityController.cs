﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.IRepository;
using Microsoft.AspNetCore.Cors;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace kmbi_core_master.coreMaster.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/activities")]
    public class activityController : Controller
    {
        private readonly iactivityRepository _actRepository;

        public activityController(iactivityRepository actRepository)
        {
            this._actRepository = actRepository;
        }

        // SEARCH log
        [HttpGet("searchLog")]
        public IActionResult search([FromQuery]string log)
        {
            var item = _actRepository.search(log);
            if (item.Count() == 0)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }


        // SEARCH userId
        [HttpGet("user")]
        public IActionResult searchId([FromQuery]string userId, [FromQuery]DateTime dateFrom, [FromQuery]DateTime dateTo)
        {
            var dt = dateTo.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
            var item = _actRepository.searchId(userId, dateFrom, dt);
            if (item.Count() == 0)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        // SEARCH Dates
        [HttpGet("activityDate")]
        public IActionResult searchDate([FromQuery]DateTime dateFrom, [FromQuery]DateTime dateTo)
        {
            // dateTo = dateTo + " 11:59:59 PM";
            //DateTime parsedDate = DateTime.Parse(dateTo);
            var dt = dateTo.Date.AddHours(23).AddMinutes(59).AddSeconds(59);

            var item = _actRepository.searchDate(dateFrom, dt);
            if (item.Count() == 0)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }
    }
}
