using System;
using System.Linq;
using OfficeOpenXml;

namespace kmbi_core_master.reports.UseCases.GenerateResignedSummary {
    public class GenerateResignedSummaryPresenter : IPresent<GenerateResignedSummaryResponse, GenerateResignedSummaryPresenterResponse> {

        public GenerateResignedSummaryPresenter() {

        }

        public GenerateResignedSummaryPresenterResponse Present(GenerateResignedSummaryResponse request)
        {
            using (OfficeOpenXml.ExcelPackage ep = new OfficeOpenXml.ExcelPackage())
            {
                // resigned
                ExcelWorksheet resignedSheet = ep.Workbook.Worksheets.Add("resigned");

                resignedSheet.Cells["A1"].Value = "KABALIKAT PARA SA MAUNLAD NA BUHAY, INC(A MF NGO)";
                resignedSheet.Cells["A2"].Value = "SUMMARY OF RESIGNED CLIENTS";
                resignedSheet.Cells["A3"].Value = "AS OF " + request.AsOf.ToString("MMMM dd, yyyy");
                resignedSheet.Cells["A4"].Value = request.Branch;

                resignedSheet.Cells["A5"].Value = "NO";
                resignedSheet.Cells["B5"].Value = "Unit";
                resignedSheet.Cells["C5"].Value = "Center";
                resignedSheet.Cells["D5"].Value = "CLIENT'S NAME";
                resignedSheet.Cells["E5"].Value = "LOAN CYCLE";
                resignedSheet.Cells["F5"].Value = "REASON FOR WITHDRAWAL";
                resignedSheet.Cells["G5"].Value = "AMOUNT OF CBU WITHDRAWAL";
                resignedSheet.Cells["H5"].Value = "DATE OF CBU WITHDRAWAL";

                var line = 6;
                request.ResignedSummary.Resigned.Details.ToList().ForEach(resDet=>{
                    resignedSheet.Cells[line,1].Value = line - 5;
                    resignedSheet.Cells[line,2].Value = resDet.Unit;
                    resignedSheet.Cells[line,3].Value = resDet.Center;
                    resignedSheet.Cells[line,4].Value = resDet.ClientName;
                    resignedSheet.Cells[line,5].Value = resDet.LoanCycle;
                    resignedSheet.Cells[line,6].Value = resDet.ReasonForWithdrawal;
                    resignedSheet.Cells[line,7].Value = resDet.AmountOfCbuWithdrawal;
                    resignedSheet.Cells[line,8].Value = resDet.DateOfCbuWithdrawal.ToShortDateString();
                    line++;
                });

                resignedSheet.Cells[line,1].Value = request.ResignedSummary.Resigned.Total.NoOfClients;
                resignedSheet.Cells[line,7].Value = request.ResignedSummary.Resigned.Total.AmountOfCbuWithdrawal;
                
                resignedSheet.Column(1).Width = 5;
                resignedSheet.Column(2).Width = 6;
                resignedSheet.Column(3).Width = 11;
                resignedSheet.Column(4).Width = 35;
                resignedSheet.Column(5).Width = 12;
                resignedSheet.Column(6).Width = 27;
                resignedSheet.Column(7).Width = 30;
                resignedSheet.Column(8).Width = 12;

                resignedSheet.Cells[7,7,line,7].Style.Numberformat.Format = "#,##0.00;(#,##0.00);-";

                // offset
                ExcelWorksheet offsetSheet = ep.Workbook.Worksheets.Add("offsetting");

                offsetSheet.Cells["A1"].Value = "KABALIKAT PARA SA MAUNLAD NA BUHAY, INC.";
                offsetSheet.Cells["A2"].Value = request.Branch + " BRANCH";
                offsetSheet.Cells["A3"].Value = "SUMMARY OF ACCOUNTS FOR OFFSETTING";
                offsetSheet.Cells["A4"].Value = "AS OF " + request.AsOf.ToString("MMMM dd, yyyy");;
                
                offsetSheet.Cells["A7"].Value = "NO";
                offsetSheet.Cells["B7"].Value = "CENTER NAME";
                offsetSheet.Cells["C7"].Value = "SURNAME";
                offsetSheet.Cells["D7"].Value = "FIRST NAME";
                offsetSheet.Cells["E7"].Value = "LOAN CYCLE";
                offsetSheet.Cells["F7"].Value = "LOAN DISBURSED";

                offsetSheet.Cells["G6:I6"].Value = "BALANCE BEFORE OFFSETTING";
                offsetSheet.Cells["G6:I6"].Merge = true;
                                
                offsetSheet.Cells["G7"].Value = "LR";
                offsetSheet.Cells["H7"].Value = "IR";
                offsetSheet.Cells["I7"].Value = "CBU";

                offsetSheet.Cells["J6:K6"].Value = "AMOUNT FOR OS";
                offsetSheet.Cells["J6:K6"].Merge = true;

                offsetSheet.Cells["J7"].Value = "LR";
                offsetSheet.Cells["K7"].Value = "IR";

                offsetSheet.Cells["L6:N6"].Value = "BALANCE AFTER OFFSETTING";
                offsetSheet.Cells["L6:N6"].Merge = true;

                offsetSheet.Cells["L7"].Value = "LR";
                offsetSheet.Cells["M7"].Value = "IR";
                offsetSheet.Cells["N7"].Value = "CBU";
                

                line = 8;
                request.ResignedSummary.Offset.Details.ToList().ForEach(offDet=>{
                    offsetSheet.Cells[line,1].Value = line - 7;
                    offsetSheet.Cells[line,2].Value = offDet.CenterName;
                    offsetSheet.Cells[line,3].Value = offDet.Surname;
                    offsetSheet.Cells[line,4].Value = offDet.Firstname;
                    offsetSheet.Cells[line,5].Value = offDet.LoanCycle;
                    offsetSheet.Cells[line,6].Value = offDet.LoanDisbursed;

                    offsetSheet.Cells[line,7].Value = offDet.BalanceBeforeOffsetting.LR;
                    offsetSheet.Cells[line,8].Value = offDet.BalanceBeforeOffsetting.IR;
                    offsetSheet.Cells[line,9].Value = offDet.BalanceBeforeOffsetting.CBU;

                    offsetSheet.Cells[line,10].Value = offDet.AmountForOs.LR;
                    offsetSheet.Cells[line,11].Value = offDet.AmountForOs.IR;
                    
                    offsetSheet.Cells[line,12].Value = offDet.BalanceAfterOfsetting.LR;
                    offsetSheet.Cells[line,13].Value = offDet.BalanceAfterOfsetting.IR;
                    offsetSheet.Cells[line,14].Value = offDet.BalanceAfterOfsetting.CBU;

                    line++;
                });

                offsetSheet.Cells[line,7].Value = request.ResignedSummary.Offset.Total.BalanceBeforeOffsetting.LR;
                offsetSheet.Cells[line,8].Value = request.ResignedSummary.Offset.Total.BalanceBeforeOffsetting.IR;
                offsetSheet.Cells[line,9].Value = request.ResignedSummary.Offset.Total.BalanceBeforeOffsetting.CBU;

                offsetSheet.Cells[line,10].Value = request.ResignedSummary.Offset.Total.AmountForOs.LR;
                offsetSheet.Cells[line,11].Value = request.ResignedSummary.Offset.Total.AmountForOs.IR;
                
                offsetSheet.Cells[line,12].Value = request.ResignedSummary.Offset.Total.BalanceAfterOfsetting.LR;
                offsetSheet.Cells[line,13].Value = request.ResignedSummary.Offset.Total.BalanceAfterOfsetting.IR;
                offsetSheet.Cells[line,14].Value = request.ResignedSummary.Offset.Total.BalanceAfterOfsetting.CBU;

                offsetSheet.Column(1).Width = 5;
                offsetSheet.Column(2).Width = 15;
                offsetSheet.Column(3).Width = 14;
                offsetSheet.Column(4).Width = 14;
                offsetSheet.Column(5).Width = 11;
                offsetSheet.Column(6).Width = 17;
                offsetSheet.Column(7).Width = 13;
                offsetSheet.Column(8).Width = 13;
                offsetSheet.Column(9).Width = 13;
                offsetSheet.Column(10).Width = 13;
                offsetSheet.Column(11).Width = 13;
                offsetSheet.Column(12).Width = 13;
                offsetSheet.Column(13).Width = 13;
                offsetSheet.Column(14).Width = 13;

                offsetSheet.Cells[8,6,line,17].Style.Numberformat.Format = "#,##0.00;(#,##0.00);-";

                // write-off
                ExcelWorksheet writeOffSheet = ep.Workbook.Worksheets.Add("write-off");

                writeOffSheet.Cells["A1"].Value = "KABALIKAT PARA SA MAUNLAD NA BUHAY, INC.";
                writeOffSheet.Cells["A2"].Value = request.Branch + " BRANCH";
                writeOffSheet.Cells["A3"].Value = "SUMMARY OF ACCOUNTS FOR WRITE OFF";
                writeOffSheet.Cells["A4"].Value = "AS OF " + request.AsOf.ToString("MMMM dd, yyyy");;
                
                writeOffSheet.Cells["A7"].Value = "NO";
                writeOffSheet.Cells["B7"].Value = "CENTER NAME";
                writeOffSheet.Cells["C7"].Value = "SURNAME";
                writeOffSheet.Cells["D7"].Value = "FIRST NAME";
                writeOffSheet.Cells["E7"].Value = "LOAN CYCLE";
                writeOffSheet.Cells["F7"].Value = "LOAN DISBURSED";

                writeOffSheet.Cells["G6:I6"].Value = "BALANCE BEFORE OFFSETTING";
                writeOffSheet.Cells["G6:I6"].Merge = true;
                                
                writeOffSheet.Cells["G7"].Value = "LR";
                writeOffSheet.Cells["H7"].Value = "IR";
                writeOffSheet.Cells["I7"].Value = "CBU";

                writeOffSheet.Cells["J6:K6"].Value = "AMOUNT FOR OS";
                writeOffSheet.Cells["J6:K6"].Merge = true;

                writeOffSheet.Cells["J7"].Value = "LR";
                writeOffSheet.Cells["K7"].Value = "IR";

                writeOffSheet.Cells["L6:N6"].Value = "BALANCE AFTER OFFSETTING";
                writeOffSheet.Cells["L6:N6"].Merge = true;

                writeOffSheet.Cells["L7"].Value = "LR";
                writeOffSheet.Cells["M7"].Value = "IR";
                writeOffSheet.Cells["N7"].Value = "CBU";

                writeOffSheet.Cells["O6:Q6"].Value = "COLLECTION AFTER OFFSETTING";
                writeOffSheet.Cells["O6:Q6"].Merge = true;

                writeOffSheet.Cells["O7"].Value = "LR";
                writeOffSheet.Cells["P7"].Value = "IR";
                writeOffSheet.Cells["Q7"].Value = "CBU";

                writeOffSheet.Cells["R6:T6"].Value = "UPDATED BALANCE AFTER OFFSETTING";
                writeOffSheet.Cells["R6:T6"].Merge = true;

                writeOffSheet.Cells["R7"].Value = "LR";
                writeOffSheet.Cells["S7"].Value = "IR";
                writeOffSheet.Cells["T7"].Value = "CBU";

                writeOffSheet.Cells["U6:V6"].Value = "AMOUNT FOR WO";
                writeOffSheet.Cells["U6:V6"].Merge = true;

                writeOffSheet.Cells["U7"].Value = "LR";
                writeOffSheet.Cells["V7"].Value = "IR";

                
                writeOffSheet.Cells["W6"].Value = "Date Written Off";
                
                line = 8;
                request.ResignedSummary.WriteOff.Details.ToList().ForEach(writeOffDet=>{
                    writeOffSheet.Cells[line,1].Value = line - 7;
                    writeOffSheet.Cells[line,2].Value = writeOffDet.CenterName;
                    writeOffSheet.Cells[line,3].Value = writeOffDet.Surname;
                    writeOffSheet.Cells[line,4].Value = writeOffDet.Firstname;
                    writeOffSheet.Cells[line,5].Value = writeOffDet.LoanCycle;
                    writeOffSheet.Cells[line,6].Value = writeOffDet.LoanDisbursed;

                    writeOffSheet.Cells[line,7].Value = writeOffDet.BalanceBeforeOffsetting.LR;
                    writeOffSheet.Cells[line,8].Value = writeOffDet.BalanceBeforeOffsetting.IR;
                    writeOffSheet.Cells[line,9].Value = writeOffDet.BalanceBeforeOffsetting.CBU;

                    writeOffSheet.Cells[line,10].Value = writeOffDet.AmountForOs.LR;
                    writeOffSheet.Cells[line,11].Value = writeOffDet.AmountForOs.IR;
                    
                    writeOffSheet.Cells[line,12].Value = writeOffDet.BalanceAfterOfsetting.LR;
                    writeOffSheet.Cells[line,13].Value = writeOffDet.BalanceAfterOfsetting.IR;
                    writeOffSheet.Cells[line,14].Value = writeOffDet.BalanceAfterOfsetting.CBU;

                    writeOffSheet.Cells[line,15].Value = writeOffDet.CollectionAfterOfsetting.LR;
                    writeOffSheet.Cells[line,16].Value = writeOffDet.CollectionAfterOfsetting.IR;
                    writeOffSheet.Cells[line,17].Value = writeOffDet.CollectionAfterOfsetting.CBU;

                    writeOffSheet.Cells[line,18].Value = writeOffDet.UpdatedBalanceAfterOfsetting.LR;
                    writeOffSheet.Cells[line,19].Value = writeOffDet.UpdatedBalanceAfterOfsetting.IR;
                    writeOffSheet.Cells[line,20].Value = writeOffDet.UpdatedBalanceAfterOfsetting.CBU;

                    writeOffSheet.Cells[line,21].Value = writeOffDet.AmountForWriteOff.LR;
                    writeOffSheet.Cells[line,22].Value = writeOffDet.AmountForWriteOff.IR;

                    writeOffSheet.Cells[line,23].Value = writeOffDet.DateWrittenOff.ToShortDateString();

                    line++;
                });

                writeOffSheet.Cells[line,7].Value = request.ResignedSummary.WriteOff.Total.BalanceBeforeOffsetting.LR;
                writeOffSheet.Cells[line,8].Value = request.ResignedSummary.WriteOff.Total.BalanceBeforeOffsetting.IR;
                writeOffSheet.Cells[line,9].Value = request.ResignedSummary.WriteOff.Total.BalanceBeforeOffsetting.CBU;

                writeOffSheet.Cells[line,10].Value = request.ResignedSummary.WriteOff.Total.AmountForOs.LR;
                writeOffSheet.Cells[line,11].Value = request.ResignedSummary.WriteOff.Total.AmountForOs.IR;
                
                writeOffSheet.Cells[line,12].Value = request.ResignedSummary.WriteOff.Total.BalanceAfterOfsetting.LR;
                writeOffSheet.Cells[line,13].Value = request.ResignedSummary.WriteOff.Total.BalanceAfterOfsetting.IR;
                writeOffSheet.Cells[line,14].Value = request.ResignedSummary.WriteOff.Total.BalanceAfterOfsetting.CBU;

                writeOffSheet.Cells[line,15].Value = request.ResignedSummary.WriteOff.Total.CollectionAfterOfsetting.LR;
                writeOffSheet.Cells[line,16].Value = request.ResignedSummary.WriteOff.Total.CollectionAfterOfsetting.IR;
                writeOffSheet.Cells[line,17].Value = request.ResignedSummary.WriteOff.Total.CollectionAfterOfsetting.CBU;

                writeOffSheet.Cells[line,18].Value = request.ResignedSummary.WriteOff.Total.UpdatedBalanceAfterOfsetting.LR;
                writeOffSheet.Cells[line,19].Value = request.ResignedSummary.WriteOff.Total.UpdatedBalanceAfterOfsetting.IR;
                writeOffSheet.Cells[line,20].Value = request.ResignedSummary.WriteOff.Total.UpdatedBalanceAfterOfsetting.CBU;

                writeOffSheet.Cells[line,21].Value = request.ResignedSummary.WriteOff.Total.AmountForWriteOff.LR;
                writeOffSheet.Cells[line,22].Value = request.ResignedSummary.WriteOff.Total.AmountForWriteOff.IR;

                writeOffSheet.Column(1).Width = 5;
                writeOffSheet.Column(2).Width = 15;
                writeOffSheet.Column(3).Width = 14;
                writeOffSheet.Column(4).Width = 14;
                writeOffSheet.Column(5).Width = 11;
                writeOffSheet.Column(6).Width = 17;
                writeOffSheet.Column(7).Width = 13;
                writeOffSheet.Column(8).Width = 13;
                writeOffSheet.Column(9).Width = 13;
                writeOffSheet.Column(10).Width = 13;
                writeOffSheet.Column(11).Width = 13;
                writeOffSheet.Column(12).Width = 13;
                writeOffSheet.Column(13).Width = 13;
                writeOffSheet.Column(14).Width = 13;
                writeOffSheet.Column(15).Width = 13;
                writeOffSheet.Column(16).Width = 13;
                writeOffSheet.Column(17).Width = 13;
                writeOffSheet.Column(18).Width = 13;
                writeOffSheet.Column(19).Width = 13;
                writeOffSheet.Column(20).Width = 13;
                writeOffSheet.Column(21).Width = 13;
                writeOffSheet.Column(22).Width = 13;

                writeOffSheet.Cells[8,6,line,22].Style.Numberformat.Format = "#,##0.00;(#,##0.00);-";

                byte[] bytes = ep.GetAsByteArray();
                ep.Dispose();

                return new GenerateResignedSummaryPresenterResponse {
                    bytes = bytes,
                    test = "Hello"
                };
            }
        }
    }

    public class GenerateResignedSummaryPresenterResponse {
        public byte[] bytes {get;set;}
        public ResignedSummary ResignedSummary {get;set;}
        public String test {get;set;}
    }
}