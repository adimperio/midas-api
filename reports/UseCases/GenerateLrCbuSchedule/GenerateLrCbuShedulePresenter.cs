using System;
using System.Collections.Generic;
using System.Linq;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Models;
using OfficeOpenXml;

namespace kmbi_core_master.reports.UseCases.GenerateLrCbuSchedule {
    public class GenerateLrCbuShedulePresenter : IPresent<GenerateLrCbuScheduleResponse, GenerateLrCbuShedulePresenterResponse>
    {
        public GenerateLrCbuShedulePresenterResponse Present(GenerateLrCbuScheduleResponse request)
        {
            using (OfficeOpenXml.ExcelPackage ep = new OfficeOpenXml.ExcelPackage())
            {
                ExcelWorksheet ew = ep.Workbook.Worksheets.Add("CONSO");
                ew.Cells["A1"].Value = "KABALIKAT PARA SA MAUNLAD NA BUHAY, INC.";
                ew.Cells["A2"].Value = "LOAN AND CBU BALANCES AS OF " + request.asOf.ToString("MMMM dd, yyyy");

                ew.Cells["A3"].Value = "LN";
                ew.Cells["B3"].Value = "Area";
                ew.Cells["C3"].Value = "Branch";
                ew.Cells["D3"].Value = "Outstanding Loan Receivables as of " + request.asOf.ToString("MMMM dd, yyyy");
                ew.Cells["E3"].Value = "CBU Balance as of " + request.asOf.ToString("MMMM dd, yyyy");
                
                ew.Row(3).Height = 30;
                ew.Row(3).Style.WrapText = true;

                var linerConso = 4;
                var LN = 1;
                request.Schedule.Details.ToList().ForEach(b=>{
                    // conso
                    ew.Cells[linerConso,1].Value = LN.ToString();
                    ew.Cells[linerConso,2].Value = b.Area;
                    ew.Cells[linerConso,3].Value = b.Name;
                    ew.Cells[linerConso,4].Value = b.Total.OutstandingLoanReceivables;
                    ew.Cells[linerConso,5].Value = b.Total.CbuBalance;

                    ew.Cells[linerConso,4].Style.Numberformat.Format = "#,##0.00;(#,##0.00);-";
                    ew.Cells[linerConso,5].Style.Numberformat.Format = "#,##0.00;(#,##0.00);-";

                    linerConso++;

                    //details

                    ExcelWorksheet ewDet = ep.Workbook.Worksheets.Add(b.Name);
                    ewDet.Cells["A1"].Value = "BRANCH NAME: " + b.Name.ToUpper();
                    ewDet.Cells["A2"].Value = "LOAN AND CBU BALANCES AS OF " + request.asOf.ToString("MMMM dd, yyyy");
                    

                    ewDet.Cells["A3"].Value = "Area";
                    ewDet.Cells["B3"].Value = "Branch";
                    ewDet.Cells["C3"].Value = "No. of Client";
                    ewDet.Cells["D3"].Value = "Unit";
                    ewDet.Cells["E3"].Value = "Center";
                    ewDet.Cells["F3"].Value = "Center Status";
                    ewDet.Cells["G3"].Value = "Loan Amount";
                    ewDet.Cells["H3"].Value = "Outstanding Loan Receivables as of " + request.asOf.ToString("MMMM dd, yyyy");
                    ewDet.Cells["I3"].Value = "CBU Balance as of " + request.asOf.ToString("MMMM dd, yyyy");
                    
                    ewDet.Row(3).Height = 30;
                    ewDet.Row(3).Style.WrapText = true;

                    var linerDet = 4;

                    b.Details.ToList().ForEach(bdet=>{

                        ewDet.Cells[linerDet,1].Value = b.Area;
                        ewDet.Cells[linerDet,2].Value = b.Name;
                        ewDet.Cells[linerDet,3].Value = bdet.NoOfClients;
                        ewDet.Cells[linerDet,4].Value = bdet.Unit;
                        ewDet.Cells[linerDet,5].Value = bdet.Center;
                        ewDet.Cells[linerDet,6].Value = bdet.CenterStatus;
                        ewDet.Cells[linerDet,7].Value = bdet.LoanAmount;
                        ewDet.Cells[linerDet,8].Value = bdet.OutstandingLoanReceivables;
                        ewDet.Cells[linerDet,9].Value = bdet.CbuBalance;

                        ewDet.Cells[linerDet,7].Style.Numberformat.Format = "#,##0.00;(#,##0.00);-";
                        ewDet.Cells[linerDet,8].Style.Numberformat.Format = "#,##0.00;(#,##0.00);-";
                        ewDet.Cells[linerDet,9].Style.Numberformat.Format = "#,##0.00;(#,##0.00);-";

                        linerDet++;
                    });

                    linerDet++;

                    ewDet.Cells[linerDet,1].Value = "TOTAL";
                    ewDet.Cells[linerDet,3].Value = b.Details.Sum(x=>x.NoOfClients);
                    ewDet.Cells[linerDet,7].Value = b.Details.Sum(x=>x.LoanAmount);
                    ewDet.Cells[linerDet,8].Value = b.Details.Sum(x=>x.OutstandingLoanReceivables);
                    ewDet.Cells[linerDet,9].Value = b.Details.Sum(x=>x.CbuBalance);

                    ewDet.Cells[linerDet,7].Style.Numberformat.Format = "#,##0.00;(#,##0.00);-";
                    ewDet.Cells[linerDet,8].Style.Numberformat.Format = "#,##0.00;(#,##0.00);-";
                    ewDet.Cells[linerDet,9].Style.Numberformat.Format = "#,##0.00;(#,##0.00);-";

                    ewDet.Cells[linerDet,4, linerDet+2,4].Value = "CENTERS";
                    ewDet.Cells[linerDet,4, linerDet+2,4].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                    ewDet.Cells[linerDet,4, linerDet+2,4].Merge = true;

                    ewDet.Cells[linerDet,5].Value = "ACTIVE";
                    ewDet.Cells[linerDet,6].Value = 0;

                    ewDet.Cells[linerDet+1,5].Value = "INACTIVE";
                    ewDet.Cells[linerDet+1,6].Value = 0;

                    ewDet.Cells[linerDet+2,5].Value = "MATURED";
                    ewDet.Cells[linerDet+2,6].Value = 0;

                    
                    // format detail sheet

                    ewDet.Column(1).Width = 11.5;
                    ewDet.Column(2).Width = 25;
                    ewDet.Column(3).Width = 12;
                    ewDet.Column(4).Width = 10;
                    ewDet.Column(5).Width = 11;
                    ewDet.Column(6).Width = 12;
                    ewDet.Column(7).Width = 22;
                    ewDet.Column(8).Width = 22;
                    ewDet.Column(9).Width = 22;

                    LN++;
                });

                linerConso++;
                ew.Cells[linerConso,3].Value = "TOTAL";
                ew.Cells[linerConso,4].Value = request.Schedule.Details.ToList().Sum(x=>x.Total.OutstandingLoanReceivables);
                ew.Cells[linerConso,5].Value = request.Schedule.Details.ToList().Sum(x=>x.Total.CbuBalance);
                ew.Cells[linerConso,4].Style.Numberformat.Format = "#,##0.00;(#,##0.00);-";
                ew.Cells[linerConso,5].Style.Numberformat.Format = "#,##0.00;(#,##0.00);-";

                // format conso sheet

                ew.Column(1).Width = 4;
                ew.Column(2).Width = 22;
                ew.Column(3).Width = 22;
                ew.Column(4).Width = 30;
                ew.Column(5).Width = 30;


                byte[] bytes = ep.GetAsByteArray();
                ep.Dispose();

                return new GenerateLrCbuShedulePresenterResponse {
                    bytes = bytes
                };
            }
        }
    }

    public class GenerateLrCbuShedulePresenterResponse {
        public byte[] bytes {get;set;}
    }
}