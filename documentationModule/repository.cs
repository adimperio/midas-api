using MongoDB.Driver;
using System.Collections.Generic;
using System;

namespace kmbi_core_master.documentationModule
{
    public class repository<TEntity> where TEntity : class
    {
        protected readonly IMongoCollection<TEntity> _coll;
        public repository(IMongoDatabase db)
        {
            _coll = db.GetCollection<TEntity>(typeof(TEntity).Name);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _coll.Find(FilterDefinition<TEntity>.Empty).ToList();
        }   

        public TEntity GetOne(FilterDefinition<TEntity> filter)
        {
            return _coll.Find(filter).FirstOrDefault();
        }
        
        public IEnumerable<TEntity> Find(FilterDefinition<TEntity> filter)
        {
            return _coll.Find(filter).ToList();
        }

        public TEntity Find(FilterDefinition<TEntity> filter, ProjectionDefinition<TEntity> projection)
        {
            return _coll.Find(filter).Project<TEntity>(projection).FirstOrDefault();
        }

        public void Insert(TEntity entity)
        {
            _coll.InsertOne(entity);
        }

        public Boolean Remove(FilterDefinition<TEntity> filter)
        {
            return _coll.DeleteOne(filter).DeletedCount > 0;
        }

        public Boolean Update(FilterDefinition<TEntity> filter, UpdateDefinition<TEntity> update)
        {
            return _coll.UpdateOne(filter, update).ModifiedCount > 0;
        }
    }
}