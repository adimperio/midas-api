﻿using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System;
using System.Linq;

namespace kmbi_core_master.coreMaster.Repository
{
    public class authRepository : dbCon, iauthRepository
    {
        public authRepository(IOptions<dbSettings> settings) : base(settings)
        {
        }

        //Update Email Address
        public void updateEmail(String email, auth p)
        {

            var filter = Builders<auth>.Filter.Eq(e => e.email, email);
            var update = Builders<auth>.Update
                .Set("email", p.newEmail)
                .CurrentDate("updatedAt");
            var result = _db.GetCollection<auth>("users").UpdateOneAsync(filter, update);

        }

        //Update Password
        public void updatePassword(String email, auth p)
        {

            var filter = Builders<auth>.Filter.Eq(e => e.email, p.email);
            var update = Builders<auth>.Update
                .Set("password", BCrypt.Net.BCrypt.HashPassword(p.newPassword))
                .CurrentDate("updatedAt");
            var result = _db.GetCollection<auth>("users").UpdateOneAsync(filter, update);

        }

        //Update isVerified
        public void updateisVerified(string email, String verToken, auth p)
        {

            var query1 = Builders<auth>.Filter.Eq(e => e.verificationToken, verToken);
            var query2 = Builders<auth>.Filter.Eq(e => e.email, email);
            var filter = Builders<auth>.Filter.And(query1, query2);

            var update = Builders<auth>.Update
                .Set("isVerified", 1)
                .CurrentDate("updatedAt");
            var result = _db.GetCollection<auth>("users").UpdateOneAsync(filter, update);

        }

        public users checkVerToken(string email, String verToken)
        {
            // p.email = email;
            var query1 = Builders<users>.Filter.Eq(e => e.verificationToken, verToken);
            var query2 = Builders<users>.Filter.Eq(e => e.email, email);
            var query = Builders<users>.Filter.And(query1, query2);

            var result = _db.GetCollection<users>("users").Find(query).ToListAsync();
            return result.Result.FirstOrDefault();
        }

        public users getCredential(String email)
        {
           // p.email = email;
            var query = Builders<users>.Filter.Eq(e => e.email, email);
            var result = _db.GetCollection<users>("users").Find(query).ToListAsync();
            return  result.Result.FirstOrDefault();
           
        }

        public credentials getInfo(String email)
        {
            var query = Builders<credentials>.Filter.Eq(e => e.email, email);
            var fields = Builders<credentials>.Projection
                .Include(u => u.fullname)
                .Include(u => u.token)
                .Include(u => u.email)
                .Exclude(u => u.Id);

            var users = _db.GetCollection<credentials>("users").Find(query).Project<credentials>(fields).ToListAsync();

            return users.Result.FirstOrDefault();
        }

        public users checkPassword(string token)
        {
            
            var query = Builders<users>.Filter.Eq(e => e.token, token);
            var result = _db.GetCollection<users>("users").Find(query).ToListAsync();
            return result.Result.FirstOrDefault();

        }

        public void resetPassword(string email, auth p)
        {
            var filter = Builders<auth>.Filter.Eq(e => e.email, p.email);
            var update = Builders<auth>.Update
                .Set("password", BCrypt.Net.BCrypt.HashPassword(p.newPassword))
                .CurrentDate("updatedAt");
            var result = _db.GetCollection<auth>("users").UpdateOneAsync(filter, update);
        }
    }
}
