﻿using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.spm.IRepository;
using kmbi_core_master.spm.Models;
using kmbi_core_master.trainingModule.IRepository;
using kmbi_core_master_v2.trainingModule.Models;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace kmbi_core_master.trainingModule.Repository
{
    public class trainingRepository : dbCon, itrainingRepository
    {
        public trainingRepository(IOptions<dbSettings> settings) : base(settings)
        {
        }

        public void add(trainings b)
        {

            b.createdAt = DateTime.Now;
            b.updatedAt = DateTime.Now;

            _db.GetCollection<trainings>("trainings").InsertOneAsync(b);
        }

        public IEnumerable<trainings> getAll()
        {
            var c = _db.GetCollection<trainings>("trainings").Find(new BsonDocument()).ToListAsync();
            return c.Result;
        }

        public trainings getBySlug(String slug)
        {
            var query = Builders<trainings>.Filter.Eq(e => e.slug, slug);
            var c = _db.GetCollection<trainings>("trainings").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }

       
        public void update(String slug, trainings p)
        {
            var query = Builders<trainings>.Filter.Eq(e => e.slug, slug);
            var update = Builders<trainings>.Update
                .Set("name", p.name)
                .Set("type", p.type)
                .Set("category", p.category)
                .CurrentDate("updatedAt");

            var l = _db.GetCollection<trainings>("trainings").UpdateOneAsync(query, update);

        }

        public void remove(String slug)
        {
            var query = Builders<trainings>.Filter.Eq(e => e.slug, slug);
            var result = _db.GetCollection<trainings>("trainings").DeleteOneAsync(query);
        }

    }
}
