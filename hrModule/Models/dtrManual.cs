﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace kmbi_core_master.hrModule.Models
{
    public class dtrManual
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("slug")]
        public String slug { get; set; }

        [BsonElement("employeeCode")]
        public String employeeCode { get; set; }

        [BsonElement("date")]
        public DateTime date { get; set; }

        [BsonElement("manual")]
        public manual manual { get; set; }

        [BsonElement("remarks")]
        public string remarks { get; set; }

        [BsonElement("isAbsent")]
        public bool isAbsent { get; set; }

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }
    }
}
