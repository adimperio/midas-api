﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Filters;
using Microsoft.AspNetCore.Cors;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace kmbi_core_master.coreMaster.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/regions")]
    
    public class regionsController : Controller
    {

        private readonly iregionsRepository _regRepository;
        private readonly iusersRepository _logInfo;
        private string token;

        public regionsController(iregionsRepository genRepository, iusersRepository logInfo)
        {
            this._regRepository = genRepository;
            _logInfo = logInfo;
        }

        public void logger(string message)
        {
            //Insert Log//
            token = HttpContext.Request.Headers["token"];
            var lInfo = _logInfo.getLoginInfo(token);
            string log = message;
            global.createLog(lInfo.id, lInfo.fullname, lInfo.employment.branch, log);
            //Insert Log//
        }

        // GET: 
        [HttpGet]
        public IEnumerable<regions> GetAll()
        {
            var regions = _regRepository.allRegion();
            return regions;

        }

        // GET region
        [HttpGet("{id:length(24)}")]
        public IActionResult Get(string id)
        {

            var item = _regRepository.getRegion(id);
            if (item == null)
            {
                return new JsonStringResult("{}");
            }

            return new ObjectResult(item);
        }

        // GET area
        [HttpGet("areaSlug={areaSlug}")]
        public IActionResult GetArea(string areaSlug)
        {

            var item = _regRepository.getArea(areaSlug);
            if (item == null)
            {
               
                return new JsonStringResult("{}");
            }

            return new ObjectResult(item);
        }

        // GET Slug
        [HttpGet("{slug}")]
        public IActionResult GetSlug(string slug)
        {

            var item = _regRepository.getRegionSlug(slug);
            if (item == null)
            {
                return new JsonStringResult("{}");
            }

            return new ObjectResult(item);
        }

        // SEARCH name
        [HttpGet("searchName")]
        public IActionResult searchName([FromQuery]string name = "")
        {

            var item = _regRepository.searchRegion(name);
            if (item.Count() == 0)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        // GET check unique slug
        [HttpGet("checkSlug")]
        public IActionResult checkSlug([FromQuery]string slug)
        {

            var item = _regRepository.checkSlug(slug);
            if (item == null)
            {
                return Ok(new
                {
                    available = 1
                });
            }

            return Ok(new
            {
                available = 0
            });
        }

        // GET check region code
        [HttpGet("checkRegionCode")]
        public IActionResult checkRegionCode([FromQuery]string regionCode)
        {

            var item = _regRepository.checkRegionCode(regionCode);
            if (item == null)
            {
                return Ok(new
                {
                    available = 1
                });
            }

            return Ok(new
            {
                available = 0
            });
        }

        // GET check region code
        [HttpGet("checkAreaCode")]
        public IActionResult checkAreaCode([FromQuery]string areaCode)
        {

            var item = _regRepository.checkAreaCode(areaCode);
            if (item == null)
            {
                return Ok(new
                {
                    available = 1
                });
            }

            return Ok(new
            {
                available = 0
            });
        }

        // POST 
        [errorHandler]
        [HttpPost]
        public IActionResult addRegion([FromBody] regions record)
        {
            _regRepository.addRegion(record);

           logger("Insert new region " + record.name);

            return Ok(new
            {
                type = "success",
                message = record.name + " created successfully.",
                slug = record.slug
            });
        }

        // POST 
        [HttpPost("area/{id:length(24)}")]
        public IActionResult addArea([FromBody] areas area, string id)
        {
            _regRepository.addArea(area, id);

           logger("Insert new area " + area.name);

            return Ok(new
            {
                type = "success",
                message = area.name + " created successfully.",
                slug = area.slug,
                id = area.Id
            });
        }

        // PUT REGION
        [errorHandler]
        [HttpPut("{slug}")]
        public IActionResult updateRegion(string slug, [FromBody] regions record)
        {
            var item = _regRepository.getRegionSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            var region = _regRepository.updateRegion(slug, record);

            logger("Update region " + record.name);

            return Ok(new
            {
                type = "success",
                region = region,
                message = record.name + "updated successfully."
            });
        }


        // PUT AREA
        [errorHandler]
        [HttpPut("regId={regId}&areaId={areaId}")]
        public IActionResult updateArea(string regId, string areaId, [FromBody] areas record)
        {
            _regRepository.updateArea(regId, areaId,  record);

            logger( "Update area " + record.name);
            return Ok(new
            {
                type = "success",
                message = record.name + "updated successfully."
            });
           
        }

        // DELETE
        [HttpDelete("{slug}")]
        public IActionResult deleteRegion(string slug)
        {

            var item = _regRepository.getRegionSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _regRepository.removeRegion(slug);

            logger("Delete region " + item.name);

            return Ok(new
            {
                type = "success",
                message = item.name + "deleted successfully."
            });
        }

        [HttpDelete("id/{regSlug}&{areaSlug}")]
        public IActionResult deleteArea(string regSlug, string areaSlug)
        {
            var item = _regRepository.getArea(areaSlug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _regRepository.removeArea(regSlug, areaSlug);

            logger("Delete area " + item.areas[0].name);

            return Ok(new
            {
                type = "success",
                message = item.areas[0].name + " deleted successfully."
            });
        }

        [HttpGet("getAreas")]
        public IActionResult getAreas() {
            var list = _regRepository.allRegion();
            List<string> list2 = new List<string>();
            
            list.ToList().ForEach((x) => {
                x.areas.ToList().ForEach((y) => {
                    list2.Add(y.name);
                });
            });
            return Ok(list2);
        }

         
    }
}
