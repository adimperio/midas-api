﻿using kmbi_core_master.acctngModule.Models;
using kmbi_core_master.hrModule.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.acctngModule.Interface
{
    public interface iacctngEntityRepository
    {
        IEnumerable<acctngEntity> all();

        acctngEntity getSlug(String slug);

        void add(acctngEntity b);

        void update(String slug, acctngEntity b);

        void remove(String slug);
    }
}
