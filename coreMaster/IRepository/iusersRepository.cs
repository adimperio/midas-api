﻿using kmbi_core_master.coreMaster.Models;
using System;
using System.Collections.Generic;

using System.Drawing;

namespace kmbi_core_master.coreMaster.IRepository
{
    public interface iusersRepository
    {
        IEnumerable<users> AllUsers();
        users Get(String id);
        getBySlug getSlug(String slug);
        users getBySlug(String slug);
        void Add(users user);
        void Update(String slug, users p);
        void updateVer(String verToken);
        void UpdatePermission(String slug, users p);
        void Remove(String slug);
        IEnumerable<nameSearch> searchName(String fullname);
        IEnumerable<emailSearch> eMail(String email);
        users checkEmail(String email);
        users checkSlug(String slug);
        users checkVerToken(String verToken);
        getByPermission getPermission(String slug);
        List<String> getAccess();
        bool checkValidUserKey(String token);

        Boolean checkIfSSSIsExisting(String sss);
        Boolean checkIfPhilhealthIsExisting(String philhealth);
        Boolean checkIfPagibigIsExisting(String pagibig);
        Boolean checkIfEmployeeCodeIsExisting(String employeeCode);
        Boolean checkIfTINIsExisting(String tin);
        Boolean checkIfDocketNumberIsExisting(String docketNo);
        //hr
        IEnumerable<address> GetAddresses(String slug);
        address GetAddress(String employeeSlug, String addressSlug);
        IEnumerable<contact> GetContacts(String slug);
        contact GetContact(String employeeSlug, String contactSlug);
        IEnumerable<skill> GetSkills(String slug);
        skill GetSkill(String employeeSlug, String slug);
        IEnumerable<allowance> GetAllowances(String slug);
        allowance GetAllowance(String slug, String allowanceSlug);
        IEnumerable<benefit> GetBenefits(String slug);
        benefit GetBenefit(String slug, String benefitSlug);
        IEnumerable<familyMember> GetFamily(String slug);
        familyMember GetFamilyMember(String employeeSlug, String familyMemberSlug);
        IEnumerable<training> GetTrainings(String slug);
        training GetTraining(String employeeSlug, String trainingSlug);
        IEnumerable<education> GetEducations(String slug);
        education GetEducation(String employeeSlug, String educationSlug);
        IEnumerable<previousEmployment> GetPreviousEmployments(String slug);
        previousEmployment GetPreviousEmployment(String employeeSlug, String previousEmploymentSlug);
        IEnumerable<employeeRelation> GetEmployeeRelations(String slug);
        employeeRelation GetEmployeeRelation(String slug, String employeeRelationSlug);
        IEnumerable<employeeSchedule> GetEmployeeSchedules(String slug);
        employeeSchedule GetEmployeeSchedule(String employeeSlug, String employeeScheduleSlug);
        IEnumerable<emergencyContactPerson> GetEmergencyContactPersons(String slug);
        emergencyContactPerson GetEmergencyContactPerson(String employeeslug, String emergencyContactPersonSlug);
        IEnumerable<language> GetLanguagesSpoken(String slug);
        language GetLanguageSpoken(String employeeSlug, String languageSpokenSlug);
        IEnumerable<employmentHistoryItem> GetEmploymentHistory(String slug);
        employmentHistoryItem GetEmploymentHistoryItem(String employeeSlug, String employmentHistoryItemSlug);

        Boolean InsertAddress(String employeeSlug, address address);
        Boolean InsertContact(String employeeSlug, contact contact);
        Boolean InsertSkill(String employeeSlug, skill skill);
        Boolean InsertAllowance(String employeeSlug, allowance allowance);
        Boolean InsertBenefit(String employeeSlug, benefit benefit);
        Boolean InsertFamily(String employeeslug, familyMember familyMember);
        Boolean InsertTraining(String employeeslug, training training);
        Boolean InsertEducation(String employeeslug, education education);
        Boolean InsertPreviousEmployment(String employeeslug, previousEmployment previousEmployment);
        Boolean InsertEmployeeRelation(String employeeSlug, employeeRelation employeeRelation);
        Boolean InsertEmployeeSchedule(String employeeSlug, employeeSchedule employeeSchedule);
        Boolean InsertEmergencyContactPerson(String employeeslug, emergencyContactPerson emergencyContactPerson);
        Boolean InsertLanguageSpoken(String employeeSlug, language language);
        Boolean InsertEmploymentHistoryItem(String employeeSlug, employmentHistoryItem employmentHistoryItem);

        Boolean UpdateAddress(String employeeSlug, String addressSlug, address address);
        Boolean UpdateContact(String employeeSlug, String contactSlug, contact contact);
        Boolean UpdateSkill(String employeeSlug, String skillSlug, skill skill);
        Boolean UpdateAllowance(String employeeSlug, String allowanceSlug, allowance allowance);
        Boolean UpdateBenefit(String employeeSlug, String benefitSlug, benefit benefit);
        Boolean UpdateFamilyMember(String employeeSlug, String familyMemberSlug, familyMember familyMember);
        Boolean UpdateTraining(String employeeSlug, String trainingSlug, training training);
        Boolean UpdateEducation(String employeeSlug, String educationSlug, education education);
        Boolean UpdatePreviousEmployment(String employeeSlug, String historySlug, previousEmployment previousEmployment);
        Boolean UpdateEmployeeRelation(String employeeSlug, String employeeRelationSlug, employeeRelation employeeRelation);
        Boolean UpdateEmployeeSchedule(String employeeSlug, String employeeScheduleSlug, employeeSchedule employeeSchedule);
        Boolean UpdateEmergencyContactPerson(String employeeslug, String emergencyContactPersonSlug, emergencyContactPerson emergencyContactPerson);
        Boolean UpdateLanguageSpoken(String employeeSlug, String languageSlug, language language);
        Boolean UpdateEmploymentHistoryItem(String employeeSlug, String employmentHistoryItemSlug, employmentHistoryItem employmentHistoryItem);

        Boolean DeleteAddress(String employeeSlug, String addressSlug);
        Boolean DeleteContact(String employeeSlug, String contactSlug);
        Boolean DeleteSkill(String employeeSlug, String skill);
        Boolean DeleteAllowance(String employeeSlug, String allowanceSlug);
        Boolean DeleteBenefit(String employeeSlug, String benefitSlug);
        Boolean DeleteFamilyMember(String employeeSlug, String familyMemberSlug);
        Boolean DeleteTraining(String employeeSlug, String trainingSlug);
        Boolean DeleteEducation(String employeeSlug, String educationSlug);
        Boolean DeletePreviousEmployment(String employeeSlug, String previousEmploymentSlug);
        Boolean DeleteEmployeeRelation(String employeeSlug, String employeeRelationSlug);
        Boolean DeleteEmployeeSchedule(String employeeSlug, String employeeScheduleSlug);
        Boolean DeleteEmergencyContactPerson(String employeeslug, String emergencyContactPersonSlug);
        Boolean DeleteLanguageSpoken(String employeeSlug, String languageSlug);
        Boolean DeleteEmploymentHistoryItem(String employeeSlug, String employmentHistoryItemSlug);

        Boolean UploadPicture(String slug, Byte[] bytes);
        Boolean UploadEducationDocument(String employeeSlug, String educationSlug, String documentSlug, String Filename, Byte[] bytes);
        Boolean UploadTrainingDocument(String employeeSlug, String trainingSlug, String documentSlug, String Filename, Byte[] bytes);
        Boolean UploadPreviousEmploymentDocument(String employeeSlug, String previousEmploymentSlug, String documentSlug, String Filename, Byte[] bytes);
        Boolean UploadEmployeeRelationDocument(String employeeSlug, String employeeRelationSlug, String documentSlug, String Filename, Byte[] bytes);
        String UploadDocument(String Filename, Byte[] bytes);

        Image ResizeImage(Image originalImage, int width, int height);
        IEnumerable<kmbi_core_master.coreMaster.Models.users> Search(String searchString);

        IEnumerable<kmbi_core_master.coreMaster.Models.users> GetListOfPendingUsers();
        IEnumerable<kmbi_core_master.reports.Models.possibleRelative> GetListOfPossibleRelatives(String last, String middle, String mothersMaidenNameMiddle, String fathersMiddleName, String relationship);

        IEnumerable<users> AllProbationaryUsers();
        void updatePendingStatus(String slug, Boolean status);

        // leaves getLeavesByEmployeeSlug(String employeeSlug);
        // Boolean createLeaves(String employeeSlug);
        // Boolean updateLeavesByEmployeeSlug(String employeeSlug);
        // Boolean deleteLeavesByEmployeeSlug(String employeeSlug);

        IEnumerable<types> getTypesByEmployeeSlug(String employeeSlug);
        types getTypesByEmployeeSlugTypeSlug(String employeeSlug, String typeSlug);
        Boolean createTypesByEmployeeSlug(String employeeSlug, types types);
        Boolean updateTypesByEmployeeSlugTypeSlug(String employeeSlug, String typeSlug, types types);
        Boolean deleteTypesByEmployeeSlugTypeSlug(String employeeSlug, String typeSlug);
        IEnumerable<users> directoryByBranchSlug(String branchSlug, String searchString);
        IEnumerable<users> birthdatesByBranchSlug(String branchSlug, Int32 month);
        IEnumerable<users> regularizationByBranchSlug(String branchSlug, DateTime from, DateTime to);
        Double countLeaves(String employeeCode, DateTime date, String leaveType);
        IEnumerable<kmbi_core_master.coreMaster.Models.users> GetListOfPendingUsers(String branchSlug);
        IEnumerable<users> getListOfServiceAwardees(DateTime from, DateTime to, String branchSlug, Int32 lengthOfService);
        IEnumerable<users> getListOfServiceAwardees(DateTime from, DateTime to, Int32 lengthOfService);

        IEnumerable<users> universalSearch(universalSearch universalSearch);
        Boolean setEmployeeStatus(String employeeSlug, String status);
        IEnumerable<users> getSubordinates (String isSlug);
        Boolean updateSubordinatesIs(String isSlug, List<String> subordinatesSlug);
        void updateLeaveBalances(DateTime effectivityDate, List<users> users);

        loginInfo getLoginInfo(string token);

        employeeSchedule[] fixSkeds(employeeSchedule[] skeds);
    }
}
