using System;

namespace kmbi_core_master.reports.Models
{
    public class repLrCbu {
        public DateTime date {get;set;}
        public String branch {get;set;}
        public repLrCbuUnit[] unit {get;set;}
        public repLrCbuTotal totals {get;set;}
        public repLrCbuSummary summary {get;set;}
    }
    
    public class repLrCbuUnit {
        public String slug {get;set;}
        public String name {get;set;}
        public repLrCbuPo[] po {get;set;}
        public repLrCbuTotal totals {get;set;}
        public String preparedBy {get;set;}
        public String checkedBy {get;set;}
        public String notedBy {get;set;}
        public String preparedByPosition {get;set;}
        public String checkedByPosition {get;set;}
        public String notedByPosition {get;set;}

    }
    
    public class repLrCbuPo {
        public String slug {get;set;}
        public String name {get;set;}
        public repLrCbuCenter[] center {get;set;}
        public repLrCbuTotal totals {get;set;}
    }

    public class repLrCbuCenter {
        public String name {get;set;}
        public String centerName {get;set;}
        public repLrCbuBalances balances {get;set;}
        public repLrCbuClientLoanStatuses clientLoanStatuses {get;set;}
        public Double disbursementForTheMonth {get;set;}
        public repLrCbuDailyMatured dailyMatured {get;set;}
        public repLrCbuCenterStatus centerStatus {get;set;}
        public repLrCbuClientStatuses clientStatuses {get;set;}
        public repLrCbuFallOut fallout {get;set;}
        public repLrCbuRetention retention {get;set;}
    }

    public class repLrCbuBalances {
        public Double loanReceivable {get;set;}
        public Double interestReceivable {get;set;}
        public Double cbu {get;set;}
    }

    public class repLrCbuClientLoanStatuses {
        public Int32 withLoan {get;set;}
        
        public Int32 withoutLoan {get;set;}
        
        public Int32 pendingForReloan {get;set;}
        public Int32 dormantClientsWithCbu {get;set;}
        
        public Int32 defaultClients {get;set;}
        public Int32 total {get;set;}
        
        public Int32 returningClients {get;set;}
        public Int32 resigned {get;set;}
        
    }

    public class repLrCbuDailyMatured {
        public Double parAmount {get;set;}
    }

    public class repLrCbuCenterStatus {
        public String status {get;set;}
    }

    public class repLrCbuClientStatuses {
        public Int32 active {get;set;}
        public Int32 inActive {get;set;}
        public Int32 matured {get;set;}
        public Int32 total {get;set;}
    }

    public class repLrCbuFallOut {
        public Int32 resigned {get;set;}
        public Double rate {get;set;}
    }

    public class repLrCbuRetention {
        public Int32 newLoan {get;set;}
        public Int32 reloan {get;set;}
        public Double rate {get;set;}
    }
    
    public class repLrCbuTotal : repLrCbuCenter {
        public new repLrCbuCenterStatusTotal centerStatus {get;set;}
    }

    public class repLrCbuCenterStatusTotal {
        public Double active {get;set;}
        public Double inActive {get;set;}
        public Double matured {get;set;}
    }


    public class repLrCbuSummary {
        public repLrCbuSummaryUnit[] unit {get;set;}
        public repLrCbuSummaryPo total {get;set;}
        public String preparedBy {get;set;}
        public String checkedBy {get;set;}
        public String approvedBy {get;set;}
        public String preparedByPosition {get;set;}
        public String checkedByPosition {get;set;}
        public String approvedByPosition {get;set;}
    }

    public class repLrCbuSummaryUnit {
        public String name {get;set;}
        public repLrCbuSummaryPo[] po {get;set;}
        public repLrCbuSummaryTotal total {get;set;}
    }

    public class repLrCbuSummaryPo {
        public String name {get;set;}
        public repLrCbuSummaryBalances balances {get;set;}
        public repLrCbuSummaryCenters centers {get;set;}
        public repLrCbuSummaryClients clients {get;set;}
        public repLrCbuSummaryLoanDisbursement loanDisbursement {get;set;}
        public repLrCbuSummaryPo total {get;set;}
    }

    public class  repLrCbuSummaryBalances {
        public repLrCbuSummaryBalancesLoan loan {get;set;}
        public repLrCbuSummaryBalancesPar par {get;set;}
        public Double cbu {get;set;}
    }

    public class repLrCbuSummaryBalancesLoan {
        public Double lrPrincipal {get;set;}
        public Double eir {get;set;}
        public Double total {get;set;}
    }

    public class repLrCbuSummaryBalancesPar {
        public Double amount {get;set;}
        public Double rate {get;set;}
    }

    public class repLrCbuSummaryCenters {
        public Double active {get;set;}
        public Double inActive {get;set;}
        public Double matured {get;set;}
        public Double dissolved {get;set;}
        public Double total {get;set;}
    }

    public class repLrCbuSummaryClients {
        public repLrCbuSummaryClientsActive active {get;set;}
        public repLrCbuSummaryClientsInActive inActive {get;set;}
        public Int32 total {get;set;}
        public repLrCbuSummaryClientsForTheMonth forTheMonth {get;set;}
        public Int32 returningClients {get;set;}
        public repLrCbuSummaryClientsResigned resigned {get;set;}
        
        public repLrCbuSummaryClientsWithLoan withLoan {get;set;}
        public Int32 withoutLoan {get;set;}
    }

    public class repLrCbuSummaryClientsWithLoan {
        public Int32 active {get;set;}
        public Int32 matured {get;set;}
        public Int32 total {get;set;}
    }

    public class repLrCbuSummaryClientsForTheMonth {
        public Int32 newLoan {get;set;}
        public Int32 reloan {get;set;}
    }

    public class repLrCbuSummaryClientsResigned {
        public Int32 forTheMonth {get;set;}
        public Int32 cumulative {get;set;}
    }

    public class repLrCbuSummaryClientsActive {
        public Int32 withLoan {get;set;}
        public Int32 pendingForReloan {get;set;}
    }

    public class repLrCbuSummaryClientsInActive {
        public Int32 dormantClients {get;set;}
        public Int32 defaultClients {get;set;}
    }
    
    public class repLrCbuSummaryLoanDisbursement {
        public Double newLoan {get;set;}
        public Double reloanNotInLPR {get;set;}
        public Double reloanLastPaymentRelease {get;set;}
        public repLrCbuSummaryTotalCenterDisbursed totalCenterDisbursed {get;set;}
        public repLrCbuSummaryCenterForTheMonth centerForTheMonth {get;set;}
        public repLrCbuSummaryAmount amount {get;set;}
    }

    public class repLrCbuSummaryTotalCenterDisbursed {
        public Double cumulative {get;set;}
        public Double forTheMonth {get;set;}
    }

    public class repLrCbuSummaryCenterForTheMonth {
        public Double newLoan {get;set;}
        public Double reloan {get;set;}
    }

    public class repLrCbuSummaryAmount {
        public Double newLoan {get;set;}
        public Double reloan {get;set;}
        public Double cumulative {get;set;}
        public Double forTheMonth {get;set;}
    }

    public class repLrCbuSummaryTotal : repLrCbuSummaryPo {

    }
}