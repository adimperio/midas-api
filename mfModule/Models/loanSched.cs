﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.coreMaster.Models
{

    public class loanSched

    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("referenceId")]
        public string referenceId { get; set; }
        
        [BsonElement("branchSlug")]
        public string branchSlug { get; set; }

        [BsonElement("branchName")]
        public string branchName { get; set; }

        [BsonElement("status")]
        public string status { get; set; }

        public IList<loanMembersSched> members { get; set; }

       
    }


   

}
