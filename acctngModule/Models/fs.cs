using System;
using System.Collections.Generic;

namespace kmbi_core_master.acctngModule.Models
{
    public class fsItem {
        public DateTime asOf {get;set;}
        public Int32 currentYear {get;set;}
        public Int32 previousYear {get;set;}
    }

    public class fsItemDetail {
        public String name {get;set;}
        public Double currentYearAmount {get;set;}
        public Double previousYearAmount {get;set;}
        public Double increaseDecrease {get;set;}
        public Double percentage {get;set;}
    }


    //bs
    public class balanceSheet : fsItem {
        public String name {get;set;}
        public balanceSheetDetailLevel1[] details {get;set;}
        // public fsItemDetail totals {get;set;}
    }

    public class balanceSheetDetailLevel1 {
        public String name {get;set;}
        public balanceSheetDetailLevel2[] details {get;set;}
        // public fsItemDetail totals {get;set;}
    }

    public class balanceSheetDetailLevel2 {
        public String name {get;set;}
        public fsItemDetail[] details {get;set;}
        // public fsItemDetail totals {get;set;}
    }


    // is
    public class incomeStatement : fsItem {
        public String name {get;set;}
        public incomeStatementDetailLevel1[] details {get;set;}
        // public fsItemDetail totals {get;set;}
    }

    public class incomeStatementDetailLevel1 {
        public String name {get;set;}
        public incomeStatementDetailLevel2[] details {get;set;}
        // public fsItemDetail totals {get;set;}
    }

    public class incomeStatementDetailLevel2 {
        public String name {get;set;}
        public fsItemDetail[] details {get;set;}
        // public fsItemDetail totals {get;set;}
    }


    public class accountDetail {
        public Int32 accountCode {get;set;}
        public Double amount {get;set;}
    }

}
