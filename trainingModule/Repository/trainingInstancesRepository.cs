﻿using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.spm.IRepository;
using kmbi_core_master.spm.Models;
using kmbi_core_master.trainingModule.IRepository;
using kmbi_core_master_v2.trainingModule.Models;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace kmbi_core_master.trainingModule.Repository
{
    public class trainingInstancesRepository : dbCon, itrainingInstancesRepository
    {
        public trainingInstancesRepository(IOptions<dbSettings> settings) : base(settings)
        {
        }

        public void add(trainingInstances b)
        {

            b.createdAt = DateTime.Now;
            b.updatedAt = DateTime.Now;

            _db.GetCollection<trainingInstances>("trainingInstances").InsertOneAsync(b);
        }

        public IEnumerable<trainingInstances> getAll()
        {
            var c = _db.GetCollection<trainingInstances>("trainingInstances").Find(new BsonDocument()).ToListAsync();
            return c.Result;
        }

        public trainingInstances getBySlug(String slug)
        {
            var query = Builders<trainingInstances>.Filter.Eq(e => e.slug, slug);
            var c = _db.GetCollection<trainingInstances>("trainingInstances").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }

       
        public void update(String slug, trainingInstances p)
        {
            var query = Builders<trainingInstances>.Filter.Eq(e => e.slug, slug);
            var update = Builders<trainingInstances>.Update
                .Set("name", p.name)
                .Set("trainingSlug", p.trainingSlug)
                .Set("duration", p.duration)
                .Set("sponsor", p.sponsor)
                .Set("venue", p.venue)
                .Set("participants", p.participants)
                .Set("documents", p.documents)
                .Set("ActualCost", p.ActualCost)
                .Set("ResourcePersons", p.ResourcePersons)
                .Set("TotalNoOfHours", p.TotalNoOfHours)
                .Set("budget", p.budget)
                .CurrentDate("updatedAt");

            var l = _db.GetCollection<trainingInstances>("trainingInstances").UpdateOneAsync(query, update);

        }

        public void remove(String slug)
        {
            var query = Builders<trainingInstances>.Filter.Eq(e => e.slug, slug);
            var result = _db.GetCollection<trainingInstances>("trainingInstances").DeleteOneAsync(query);
        }

        public trainingInstances[] getByYear(int Year)
        {
            var collection = _db.GetCollection<trainingInstances>("trainingInstances");
            var queryDurationFrom = Builders<trainingInstances>.Filter.Gte("duration.from",new DateTime(Year,1,1,0,0,0));
            var queryDurationTo = Builders<trainingInstances>.Filter.Lte("duration.from",new DateTime(Year,12,21,23,59,59));

            var trainings = collection.Find(Builders<trainingInstances>.Filter.And(queryDurationFrom, queryDurationTo)).ToList();

            return trainings.ToArray();
        }
    }
}
