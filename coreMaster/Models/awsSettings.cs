﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace kmbi_core_master.coreMaster.Models
{
    public class awsSettings
    {
        public string accessKey { get; set; }
        public string secretKey { get; set; }
    }
}
