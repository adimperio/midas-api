using System;
using System.Collections.Generic;
using System.Linq;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Models;

namespace kmbi_core_master.reports.UseCases.GenerateTINIssuanceRepot {
    public class GenerateTINIssuanceReport : IUseCase<GenerateTINIssuanceReportRequest, GenerateTINIssuanceReportResponse> {


        public iloanRepository _loanRepo { get; }
        public iclientsRepository _clientRepo { get; }

        public GenerateTINIssuanceReport(
          iloanRepository loanRepo,
          iclientsRepository clientRepo
        ) {
            _loanRepo = loanRepo;
            _clientRepo = clientRepo;
        }

        public GenerateTINIssuanceReportResponse Handle(GenerateTINIssuanceReportRequest request) {

            // get all active loan's members from loan repo

            List<loans> loanList = _loanRepo.getActiveLoans(Convert.ToDateTime(request.from), Convert.ToDateTime(request.to));
            
            List<loanMembers> memberList = new List<loanMembers>();

            foreach(loans loan in loanList) {
                memberList.AddRange(loan.members);
            }

            // from list of members, get distinct IDs
            List<String> distinctClientsIds = memberList.Select(xy=>xy.memberId).Distinct().ToList();

            // from distinct IDs, get client info from client repo
            
            IEnumerable<clients> distinctClients = _clientRepo.GetClients(distinctClientsIds.ToArray());
            
            // populate to model

            List<TINIssuance> items = new List<TINIssuance>();

            foreach(clients client in distinctClients) {
                TINIssuance ti = new TINIssuance();
                ti.FirstName = client.name.first;
                ti.MiddleName = client.name.middle;
                ti.LastName = client.name.last;
                if(client.civilStatus.ToLower() == "married") {
                    if(client.maidenName !=null) {
                        ti.MaidenFirstName = client.maidenName.first;
                        ti.MaidenMiddleName = client.maidenName.middle;
                        ti.MaidenLastName = client.maidenName.last;
                    }

                    if(client.guarantor.relationship.ToLower() == "spouse") {
                        ti.SpouseFirstName = client.guarantor.name.first;
                        ti.SpouseMiddleName = client.guarantor.name.middle;
                        ti.SpouseLastName = client.guarantor.name.last;
                        ti.SpouseTIN = client.guarantor.tin;
                    } else {
                        if(client.beneficiary.relationship.ToLower() == "spouse") {
                            ti.SpouseFirstName = client.spouse.name.first;
                            ti.SpouseMiddleName = client.spouse.name.middle;
                            ti.SpouseLastName = client.spouse.name.last;
                            ti.SpouseTIN = "";
                        }
                    }
                }

                ti.Birthdate = Convert.ToDateTime(client.birthdate);
                ti.Gender = client.gender;
                ti.CivilStatus = client.civilStatus;
                ti.CompleteResidenceAddress = client.addresses[0].full;
                
                
                
                ti.ContactPerson = client.guarantor.fullname;
                var mobs = "";

                if(client.guarantor.mobile != null) {
                    if(client.guarantor.mobile != "0") {
                        mobs = client.guarantor.mobile;
                    }
                }
                    
                ti.ContactNo = mobs;
                items.Add(ti);
            }
            return new GenerateTINIssuanceReportResponse {from = Convert.ToDateTime(request.from), to = Convert.ToDateTime(request.to), items = items.ToArray()};
        }
    }

    public class GenerateTINIssuanceReportResponse {
        public DateTime from {get;set;}
        public DateTime to {get;set;}
        public TINIssuance[] items {get;set;}
    }

    public class GenerateTINIssuanceReportRequest {
        public GenerateTINIssuanceReportRequest()
        {
            var now = DateTime.Now.Date;
            from = now;
            to = now;
        }

        public DateTime? from {get;set;}
        public DateTime? to {get;set;}
    }

    public class TINIssuance {
        public TINIssuance()
        {
            var now = DateTime.Now.Date;
            FirstName = "";
            MiddleName = "";
            LastName = "";
            MaidenFirstName = "";
            MaidenMiddleName = "";
            MaidenLastName = "";
            Birthdate = now;
            Gender = "";
            CivilStatus = "";
            CompleteResidenceAddress = "";
            ContactPerson = "";
            ContactNo = "";
            SpouseFirstName = "";
            SpouseMiddleName = "";
            SpouseLastName = "";
            SpouseTIN = "";
        }

        public string FirstName {get;set;}
        public string MiddleName {get;set;}
        public string LastName {get;set;}
        public string MaidenFirstName {get;set;}
        public string MaidenMiddleName {get;set;}
        public string MaidenLastName {get;set;}
        public DateTime Birthdate {get;set;}
        public string Gender {get;set;}
        public string CivilStatus {get;set;}
        public string CompleteResidenceAddress {get;set;}
        public string ContactPerson {get;set;}
        public string ContactNumber {get;set;}
        public string ContactNo {get;set;}
        public string SpouseFirstName {get;set;}
        public string SpouseMiddleName {get;set;}
        public string SpouseLastName {get;set;}
        public string SpouseTIN {get;set;}
    }
}