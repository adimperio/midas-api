﻿using kmbi_core_master.acctngModule.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.acctngModule.Interface
{
    public interface idepartmentBudgetRepository
    {
        IEnumerable<departmentBudget> all();

        departmentBudget getSlug(String slug);

        List<departmentBudget> getbyYear(int year);

        void add(departmentBudget b);

        void update(String slug, departmentBudget b);

        void remove(String slug);

        void removeByDept(String name, int year, departmentBudget b);

        //BUDGET
        //budgetDept getId(string id);

        //void addBudget(string depSlug, budBudget u, string slug);

        //void updateBudget(String slug, string budId, budBudget u);

        //void removeBudget(String slug, string budId);
    }
}
