﻿using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.spm.IRepository;
using kmbi_core_master.spm.Models;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace kmbi_core_master.spm.Repository
{
    public class clientProfilingRepository : dbCon, iclientProfilinRepository
    {
        public clientProfilingRepository(IOptions<dbSettings> settings) : base(settings)
        {
        }

        public void add(clientProfiling b)
        {

            b.createdAt = DateTime.Now;
            b.updatedAt = DateTime.Now;

            _db.GetCollection<clientProfiling>("clientProfiling").InsertOneAsync(b);
        }

        public IEnumerable<clientProfiling> getAll()
        {
            var c = _db.GetCollection<clientProfiling>("clientProfiling").Find(new BsonDocument()).ToListAsync();
            return c.Result;
        }

        public clientProfiling getBySlug(String slug)
        {
            var query = Builders<clientProfiling>.Filter.Eq(e => e.slug, slug);
            var c = _db.GetCollection<clientProfiling>("clientProfiling").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }

       
        public void update(String slug, clientProfiling p)
        {
           // p.slug = slug;
            var query = Builders<clientProfiling>.Filter.Eq(e => e.slug, slug);
            var update = Builders<clientProfiling>.Update
                .Set("client", p.client)
                .Set("questionaire", p.questionaire)
                .Set("questions", p.questions)
                .CurrentDate("updatedAt");

            var l = _db.GetCollection<clientProfiling>("clientProfiling").UpdateOneAsync(query, update);

        }

        public void remove(String slug)
        {
            var query = Builders<clientProfiling>.Filter.Eq(e => e.slug, slug);
            var result = _db.GetCollection<clientProfiling>("clientProfiling").DeleteOneAsync(query);
        }
    }
}
