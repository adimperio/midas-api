using System;
using kmbi_core_master.coreMaster.Models;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;


namespace kmbi_core_master.reports.Models
{

    public class repDailyBalance {
        public DateTime from {get;set;}
        public DateTime to {get;set;}
        public repDailyBalanceDetail[] assetDetails {get;set;}
        public repDailyBalanceDetail[] liabilitiesDetails {get;set;}
        public repDailyBalanceDetail[] fundBalanceDetails {get;set;}
        public repDailyBalanceDetail[] incomeDetails {get;set;}
        public repDailyBalanceDetail[] expensesDetails {get;set;}
        public repDailyBalanceDetail variance {get;set;}
        public repDailyBalanceSummary summary {get;set;}
    }

    public class repDailyBalanceSummary {
        public repDailyBalanceDetail assetSummary {get;set;}
        public repDailyBalanceDetail liabilitiesSummary {get;set;}
        public repDailyBalanceDetail fundBalanceSummary {get;set;}
        public repDailyBalanceDetail incomeSummary {get;set;}
        public repDailyBalanceDetail expensesSummary {get;set;}
        public repDailyBalanceDetail netIncomeLoss {get;set;}
    }

    public class repDailyBalanceDetail {
        public String acct {get;set;}
        public String accountTitle {get;set;}
        public Decimal previous {get;set;}
        public Decimal today {get;set;}
        public Decimal toDate {get;set;}
        public repDailyBalanceDetailSubsi[] subsidiaries {get;set;}
    }

    public class repDailyBalanceDetailSubsi {
        public String acct {get;set;}
        public String accountTitle {get;set;}
        public Decimal previous {get;set;}
        public Decimal today {get;set;}
        public Decimal toDate {get;set;}
    }
}