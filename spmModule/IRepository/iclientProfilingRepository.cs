﻿using kmbi_core_master.coreMaster.Controllers;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.spm.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.spm.IRepository
{
    public interface iclientProfilinRepository
    {

        IEnumerable<clientProfiling> getAll();

        clientProfiling getBySlug(String slug);

        void add(clientProfiling b);

        void update(String slug, clientProfiling b);

        void remove(string slug);
    }
}
