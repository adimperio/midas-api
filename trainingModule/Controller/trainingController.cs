﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Filters;
using Microsoft.AspNetCore.Cors;
using kmbi_core_master.spm.IRepository;
using kmbi_core_master.spm.Models;
using kmbi_core_master.coreMaster;
using System;
using kmbi_core_master.trainingModule.IRepository;
using kmbi_core_master_v2.trainingModule.Models;
using Microsoft.AspNetCore.Http;
using Amazon.S3;
using System.IO;
using Amazon.S3.Model;
using Microsoft.Extensions.Options;
using Amazon;

namespace kmbi_core_master.trainingModule.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/trainings")]
    [usersAuth]
    public class trainingController : Controller
    {
        private readonly itrainingRepository _Repository;
        private readonly iusersRepository _logInfo;
        private string token;

        private awsSettings _settings;

        private static string _awsAccessKey;
        private static string _awsSecretKey;

        public trainingController(itrainingRepository Repositor, iusersRepository logInfo, IOptions<awsSettings> settings)
        {
            this._Repository = Repositor;
            _logInfo = logInfo;
            _settings = settings.Value;
            _awsAccessKey = _settings.accessKey;
            _awsSecretKey = _settings.secretKey;
        }

        public void logger(string message)
        {
            //Insert Log//
            token = HttpContext.Request.Headers["token"];
            var lInfo = _logInfo.getLoginInfo(token);
            string log = message;
            global.createLog(lInfo.id, lInfo.fullname, lInfo.employment.branch, log);
            //Insert Log//
        }

        // GET: 
        [HttpGet]
        public IEnumerable<trainings> GetAll()
        {
            var c = _Repository.getAll();
            return c;

        }

        [HttpGet("{slug}")]
        public IActionResult getSlug(string slug)
        {

            var item = _Repository.getBySlug(slug);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

       

        [HttpPost]
        [errorHandler]
        public IActionResult create([FromBody] trainings b)
        {

            _Repository.add(b);

            logger( "Insert new training " + b.name);

            return Ok(new
            {
                type = "success",
                message = b.name + " created successfully.",
                slug = b.slug,
                training = b
            });

        }



        [HttpPut("{slug}")]
        public IActionResult slugupdate(string slug, [FromBody] trainings b)
        {
            var item = _Repository.getBySlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _Repository.update(slug, b);

           logger("Modified training  " + b.name);

            return Ok(new
            {
                type = "success",
                message = b.name + " updated successfully.",
                training = b
            });
        }




        [HttpDelete("{slug}")]
        public IActionResult slugdelete(string slug)
        {

            var item = _Repository.getBySlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.remove(slug);

            logger("Delete training " + item.name);

            return Ok(new
            {
                type = "success",
                message = item.name + " deleted successfully."
            });
        }

        [HttpPost("toS3")]
        public IActionResult UploadToS3(IFormFile file)
        {
            var client = new AmazonS3Client(_awsAccessKey, _awsSecretKey, RegionEndpoint.APSoutheast1);
            var _bucketName = "kmbi-training";
            string keyName = string.Format("kmbi-training/{0}", file.FileName);

            BinaryReader reader = new BinaryReader(file.OpenReadStream());
            Byte[] bytes = reader.ReadBytes((int)file.Length);

            Stream stream = new MemoryStream(new MemoryStream(bytes).ToArray());
            PutObjectResponse response = client.PutObjectAsync(new PutObjectRequest
            {
                BucketName = _bucketName,
                Key = keyName,
                InputStream = stream
            }).Result;

            return Ok(new
            {
                message = "Uploaded successfully",
                path = "https://s3-ap-southeast-1.amazonaws.com/" + _bucketName + "/" + keyName,
                type = file.ContentType,
                size = file.Length
            });

        }


    }
}
