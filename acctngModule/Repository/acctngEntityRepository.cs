﻿using Microsoft.Extensions.Options;

using System;
using System.Collections.Generic;
using System.Linq;

using MongoDB.Bson;
using MongoDB.Driver;

using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.acctngModule.Models;
using kmbi_core_master.acctngModule.Interface;

namespace kmbi_core_master.acctngModule.Repository
{
    public class acctngEntityRepository : dbCon, iacctngEntityRepository
    {
        
        public acctngEntityRepository(IOptions<dbSettings> settings) : base(settings) {}

        public void add(acctngEntity b)
        {

            b.createdAt = DateTime.Now;
            b.updatedAt = DateTime.Now;
            var coll = _db.GetCollection<acctngEntity>("acctngEntity");
            _db.GetCollection<acctngEntity>("acctngEntity").InsertOneAsync(b);
        }

    
        public IEnumerable<acctngEntity> all()
        {
            var c = _db.GetCollection<acctngEntity>("acctngEntity").Find(new BsonDocument()).ToListAsync();
            return c.Result;
        }

        public acctngEntity getSlug(String slug)
        {
            var query = Builders<acctngEntity>.Filter.Eq(e => e.slug, slug);
            var c = _db.GetCollection<acctngEntity>("acctngEntity").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }
       
        public void update(String slug, acctngEntity p)
        {
            p.slug = slug;
            var query = Builders<acctngEntity>.Filter.Eq(e => e.slug, slug);
            var update = Builders<acctngEntity>.Update
                .Set("name", p.name)
                .Set("tin", p.tin)
                .Set("address", p.address)
                .Set("type", p.type)
                .Set("vatType", p.vatType)
                .Set("ewtType", p.ewtType)
                .Set("contacts", p.contacts)
                .CurrentDate("updatedAt");

            var branch = _db.GetCollection<acctngEntity>("acctngEntity").UpdateOneAsync(query, update);

        }
        public void remove(String slug)
        {
            var query = Builders<acctngEntity>.Filter.Eq(e => e.slug, slug);
            var result = _db.GetCollection<acctngEntity>("acctngEntity").DeleteOneAsync(query);
        }
    }
}
