﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.coreMaster.Models
{

    public class amort
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string slug { get; set; }
        public string loanType { get; set; }
        public double amount { get; set; }
        public int term { get; set; }
        public List<sched> schedule { get; set; }
    }

    public class sched
    {
        public int weekNo { get; set; }
        public double principal { get; set; }
        public double interest { get; set; }
    }

    

}
