﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace kmbi_core_master.coreMaster.Models
{


    public class paymentUpfront
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String id { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        public paymentUpfrontLoan loan { get; set; }
        public paymentUpfrontMember[] members { get; set; }
        public paymentUpfrontOfficialreceipt officialReceipt { get; set; }
        public paymentUpfrontPayee payee { get; set; }

        [BsonElement("mode")]
        public string mode { get; set; }

        [BsonElement("remarks")]
        public string remarks { get; set; }

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }
    }

    public class paymentUpfrontLoan
    {
        [BsonElement("referenceId")]
        public string referenceId { get; set; }

        public paymentUpfrontBranch branch { get; set; }
        public paymentUpfrontCenter center { get; set; }
        public paymentUpfrontBank bank { get; set; }
    }

    public class paymentUpfrontBranch
    {
        [BsonElement("id")]
        public string id { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("name")]
        public string name { get; set; }
    }

    public class paymentUpfrontCenter
    {
        [BsonElement("id")]
        public string id { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("code")]
        public string code { get; set; }
    }

    public class paymentUpfrontBank
    {
        [BsonElement("id")]
        public string id { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("name")]
        public string name { get; set; }
    }

    public class paymentUpfrontOfficialreceipt
    {
        [BsonElement("number")]
        public string number { get; set; }

        public paymentUpfrontOfficialreceiptFee[] fees { get; set; }

        [BsonElement("total")]
        public double total { get; set; }

        [BsonElement("paidAt")]
        public DateTime paidAt { get; set; }
    }

    public class paymentUpfrontOfficialreceiptFee
    {
        [BsonElement("type")]
        public string type { get; set; }

        [BsonElement("amount")]
        public double amount { get; set; }
    }

    public class paymentUpfrontPayee
    {
        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("address")]
        public string address { get; set; }
    }

    public class paymentUpfrontMember
    {
        [BsonElement("slug")]
        public string slug { get; set; }

        public paymentUpfrontName name { get; set; }
        public paymentUpfrontmemberFee[] fees { get; set; }

        [BsonElement("total")]
        public double total { get; set; }
    }

    public class paymentUpfrontName
    {
        [BsonElement("first")]
        public string first { get; set; }

        [BsonElement("middle")]
        public string middle { get; set; }

        [BsonElement("last")]
        public string last { get; set; }

        [BsonElement("fullname")]
        public string fullname { get; set; }
    }

    public class paymentUpfrontmemberFee
    {
        [BsonElement("type")]
        public string type { get; set; }

        [BsonElement("amount")]
        public double amount { get; set; }
    }

    public class syncPaymentUpfront
    {
        [BsonId]
        public ObjectId id { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        public paymentUpfrontLoan loan { get; set; }
        public paymentUpfrontMember[] members { get; set; }
        public paymentUpfrontOfficialreceipt officialReceipt { get; set; }
        public paymentUpfrontPayee payee { get; set; }

        [BsonElement("mode")]
        public string mode { get; set; }

        [BsonElement("remarks")]
        public string remarks { get; set; }

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }
    }
}
