using System.Collections.Generic;
using Microsoft.Extensions.Options;
using System;
using MongoDB.Driver;
using MongoDB.Bson;
using kmbi_core_master.documentationModule.Models;

namespace kmbi_core_master.documentationModule.Service
{
    public class commentService
    {
        context _context;
        activityService logger;
        public commentService(IOptions<kmbi_core_master.coreMaster.Models.dbSettings> settings)
        {
            _context = new context(settings);
            logger = new activityService(settings);
        }

        public void log(String _log)
        {
            logger.log(_log);
        }

        public IEnumerable<comment> GetComments()
        {
            return _context.comments.GetAll();
        }
        public comment GetCommentBySlug(String commentSlug)
        {
            return _context.comments.GetOne(Builders<comment>.Filter.Eq("slug",commentSlug));
        }

        public reply GetReplyByCommentSlugReplySlug(String commentSlug, String replySlug)
        {
            var filterComment = Builders<comment>.Filter.Eq("slug", commentSlug);
            var filterReply = Builders<comment>.Filter.Eq("replies.slug", replySlug);
            var filterCombine = Builders<comment>.Filter.And(filterComment,filterReply);

            var inc = Builders<comment>.Projection.Include("replies.$");

            comment comment = _context.comments.Find(filterReply,inc);

            if(comment!=null)   
            {
                if(comment.replies.Count > 0)
                {
                    return comment.replies[0];
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        public Boolean InsertComment(comment comment)
        {
            comment.Id = ObjectId.GenerateNewId().ToString();
            comment.createdAt = DateTime.Now;
            comment.updatedAt = DateTime.Now;

            _context.comments.Insert(comment);
            comment _comment = GetCommentBySlug(comment.slug);

            if(_comment!=null)
            {
                log("Insert new comment [slug:" + comment.slug + ", email:" + comment.email + "]");
                return true;    
            }
            else
            {
                return false;
            }
        }

        public Boolean InsertReply(String commentSlug, reply reply)
        {
            var filterComment = Builders<comment>
                .Filter
                .Eq("slug", commentSlug);

            var update = Builders<comment>
                .Update
                .AddToSet("replies",new reply {
                        Id = ObjectId.GenerateNewId().ToString(),
                        slug = reply.slug,
                        email = reply.email,
                        message = reply.message,
                        verifiedAnswer = false,
                        createdAt = DateTime.Now,
                        updatedAt = DateTime.Now
                    });

            _context.comments.Update(filterComment,update);

            reply _reply = GetReplyByCommentSlugReplySlug(commentSlug, reply.slug);
            
            if(_reply!=null)
            {
                log("Insert new reply [slug:" + reply.slug + ", email:" + reply.email + "]");
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean UpdateComment(String commentSlug, comment comment)
        {
            var filterComment = Builders<comment>
                .Filter
                .Eq("slug", commentSlug);

            var update = Builders<comment>
                .Update
                .Set("email", comment.email)
                .Set("message", comment.message
                ).CurrentDate("updatedAt");
            
            var isSuccess = _context.comments.Update(filterComment,update);

            if(isSuccess)
            {
                log("Modified comment [slug:" + comment.slug + ", email:" + comment.email + "]");
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean UpdateReply(String commentSlug, String replySlug, reply reply)
        {
            var filterComment = Builders<comment>.Filter.Eq("slug", commentSlug);
            var filterReply = Builders<comment>.Filter.Eq("replies.slug", replySlug);
            var filterCombine = Builders<comment>.Filter.And(filterComment,filterReply);

            var update = Builders<comment>
                .Update
                .Set("replies.$.email", reply.email)
                .Set("replies.$.message", reply.message)
                .CurrentDate("replies.$.updatedAt");
                
            var isSuccess = _context.comments.Update(filterCombine,update);
            if(isSuccess)
            {
                log("Modified reply [slug:" + reply.slug + ", email:" + reply.email + "]");
                return true;
            }
            else
            {
                return false;
            }
            
        }

        public Boolean DeleteComment(String commentSlug)
        {
            var filterComment = Builders<comment>.Filter.Eq("slug", commentSlug);

            comment com = GetCommentBySlug(commentSlug);
            var isSuccess = _context.comments.Remove(filterComment);
            if(isSuccess)
            {
                log("Delete comment [slug:" + com.slug + ", email:" + com.email + "]");
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean DeleteReply(String commentSlug, String replySlug)
        {
            var filterComment = Builders<comment>.Filter.Eq("slug", commentSlug);
            var filterReply = Builders<comment>.Filter.Eq("replies.slug", replySlug);
            var filterCombine = Builders<comment>.Filter.And(filterComment, filterReply);

            var update = Builders<comment>
                .Update
                .PullFilter("replies",Builders<reply>.Filter.Eq("slug", replySlug));

            reply rep = GetReplyByCommentSlugReplySlug(commentSlug, replySlug);
            var IsSuccess = _context.comments.Update(filterComment,update);
            if(IsSuccess)
            {
                log("Delete reply [slug:" + rep.slug + ", email:" + rep.email + "]");
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}