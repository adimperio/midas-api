﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Filters;
using Microsoft.AspNetCore.Cors;
using kmbi_core_master.documentationModule.iRepositories;
using kmbi_core_master.helpers.interfaces;
using kmbi_core_master.spm.IRepository;
using kmbi_core_master.spm.Models;
using kmbi_core_master.coreMaster;

namespace kmbi_core_master.spm.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/questionaires")]
    public class questionairesController : Controller
    {
        private readonly iquestionairesRepository _Repository;
        private readonly iusersRepository _logInfo;
        private string token;

        public questionairesController(iquestionairesRepository Repository, iusersRepository logInfo)
        {
            this._Repository = Repository;
            _logInfo = logInfo;
        }

        public void logger(string message)
        {
            //Insert Log//
            token = HttpContext.Request.Headers["token"];
            var lInfo = _logInfo.getLoginInfo(token);
            string log = message;
            global.createLog(lInfo.id, lInfo.fullname, lInfo.employment.branch, log);
            //Insert Log//
        }

        // GET: 
        [HttpGet]
        public IEnumerable<questionaires> GetAll()
        {
            var d = _Repository.getAll();
            return d;

        }

        //GET  Slug
        [HttpGet("{slug}")]
        public IActionResult getSlug(string slug)
        {

            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

      
        [HttpGet("search")]
        public IActionResult search([FromQuery]string name = "")
        {
            if(name!=null) {
                var item = _Repository.search(name);
                if(item!=null) {
                    return Ok(item);
                } else {
                    return new JsonStringResult("[]");    
                }
            } else {
                return new JsonStringResult("[]");
            }
        }

        // POST
        [errorHandler]
        [HttpPost]
        public IActionResult create([FromBody] questionaires b)
        {
            

            _Repository.add(b);

           logger( "Insert new questionaire " + b.name);

            return Ok(new
            {
                type = "success",
                message = b.name + " created successfully.",
                slug = b.slug
            });

        }

        
        // PUT 
        [errorHandler]
        [HttpPut("{slug}")]
        public IActionResult update(string slug, [FromBody] questionaires b)
        {
            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.update(slug, b);

            logger("Update questionaire " + b.name);

            return Ok(new
            {
                type = "success",
                message = b.name + " updated successfully."
            });
        }

        
        [HttpDelete("{slug}")]
        public IActionResult delete(string slug)
        {

            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.delete(slug);

            logger("Delete questionaire " + item.name);

            return Ok(new
            {
                type = "success",
                message = item.name + " deleted successfully."
            });
        }

        

        //GET  new order
        [HttpPut("newOrder")]
        public IActionResult getNewOrder([FromBody] List<string> id, [FromQuery] string slug)
        {

            var item = _Repository.getSlug(slug); 
            if (item == null)
            {
                return Ok(new {
                    type = "error",
                    message = "No record foud"
                });
            }

            _Repository.sortByCode(id, slug);

            logger("Update new order " + item.name);
            return Ok(new
            {
                type = "success",
                message = item.name + " successfully updated."
            });
        }


        //[HttpPut("sample")]
        //public IActionResult sample([FromBody] lOrder order)
        //{
        //    lOrder c = new lOrder();
        //    var something= 0;
        //    foreach (var item in order.order)
        //    {
        //         something = item;
        //        // return something;
        //        c.order.Add(item);

        //    }
        //    return new ObjectResult(c);
        //}

    }
}
