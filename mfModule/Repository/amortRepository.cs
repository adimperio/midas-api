﻿using kmbi_core_master.coreMaster.Controllers;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Models;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using kmbi_core_master.helpers;

namespace kmbi_core_master.coreMaster.Repository
{
    public class amortRepository : dbCon, iamortRepository
    {
        public amortRepository(IOptions<dbSettings> settings) : base(settings)
        {
        }

        

        public List<amort> getAmortByterm(int term)
        {
            var query = Builders<amort>.Filter.Eq(e => e.term, term);
            var amort = _db.GetCollection<amort>("amortization").Find(query).ToListAsync().Result;

            return amort;
        }

    }
}
