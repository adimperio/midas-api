using System;
using kmbi_core_master.coreMaster.Models;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;


namespace kmbi_core_master.reports.Models
{
    public class repGeneralJournalReportModel
    {
        public DateTime date {get;set;}
        public String jvNo {get;set;}
        public String particulars {get;set;}
        public String account {get;set;}
        public String accountType {get;set;}
        public Double debit {get;set;}
        public Double credit {get;set;}
    }
}