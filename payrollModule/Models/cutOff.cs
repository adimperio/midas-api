﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.payrollModule.Models
{
    public class cutOff {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String id { get; set; }
        public String slug { get; set; }
        public periodRange periodRange {get;set;}
        public cutOffRange cutOffRange {get;set;}
        public Boolean active {get;set;}
        public Boolean? attendanceApproved {get;set;}
        public DateTime? attendanceApprovedAt {get;set;}
        public String attendanceApprovedBy {get;set;}
        public Boolean? payrollApproved {get;set;}
        public DateTime? payrollApprovedAt {get;set;}
        public String payrollApprovedBy {get;set;}
        public DateTime createdAt {get;set;}
        public DateTime updatedAt {get;set;}
    }

    public class cutOffRange {
        public DateTime from {get;set;}
        public DateTime to {get;set;}
    }

    public class periodRange {
        public DateTime from {get;set;}
        public DateTime to {get;set;}
    }
}