using System;

namespace kmbi_core_master.helpers.interfaces
{
    public class reportResult : iResult
    {
        public String code{get;set;}
        public String description {get;set;}
        public byte[] bytes {get;set;}
        public String raw {get;set;}
    }
}