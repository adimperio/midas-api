using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using kmbi_core_master.acctngModule.Interface;
using kmbi_core_master.acctngModule.Models;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.helpers;

namespace kmbi_core_master.acctngModule.Shared
{
    public class AccountingShared : iAccountingShared
    {
        private icoaRepository _coa;
        private icashReceiptRepository _cr;
        private iacctngCvRepository _acv;
        private iacctngJvRepository _ajv;
        private iusersRepository _usr;
        private idepartmentBudgetRepository _bud;

        public AccountingShared(
            icashReceiptRepository cr,
            icoaRepository coa,
            iacctngCvRepository acv,
            iacctngJvRepository ajv,
            iusersRepository usr,
            idepartmentBudgetRepository bud
        )
        {
            _coa = coa;
            _cr = cr;
            _acv = acv;
            _ajv = ajv;
            _usr = usr;
            _bud = bud;
        }

        public IEnumerable<dynamic> dailyBalances(DateTime from, DateTime to)
        {
            var dateToProcessFrom = from.Date;
            var dateToProcessTo = to.Date;

            var coaList = _coa.all();
            var crList = _cr.all();
            var cvList = _acv.all();
            var jvList = _ajv.all();

            List<dynamic> gls = new List<dynamic>();
            foreach (coa c in coaList)
            {
                dynamic gl = new ExpandoObject();
                gl.code = c.code;
                gl.name = c.name;
                gl.element = c.element;
                var cToday = 0.0;
                var cPrevious = 0.0;
                var cToDate = 0.0;

                cToday =
                    ((from x in crList from y in x.accounts.debit where y.code == c.code && (x.timestamps.issuedAt.Date >= dateToProcessFrom && x.timestamps.issuedAt.Date <= dateToProcessTo) select y.amount).Sum(x => x) -
                    (from x in crList from y in x.accounts.credit where y.code == c.code && (x.timestamps.issuedAt.Date >= dateToProcessFrom && x.timestamps.issuedAt.Date <= dateToProcessTo) select y.amount).Sum(x => x)) +
                    ((from x in cvList from y in x.accounts.debit where y.code == c.code && (x.timestamps.issuedAt.Date >= dateToProcessFrom && x.timestamps.issuedAt.Date <= dateToProcessTo) select y.amount).Sum(x => x) -
                    (from x in cvList from y in x.accounts.credit where y.code == c.code && (x.timestamps.issuedAt.Date >= dateToProcessFrom && x.timestamps.issuedAt.Date <= dateToProcessTo) select y.amount).Sum(x => x)) +
                    ((from x in jvList from y in x.accounts.debit where y.code == c.code && (x.timestamps.issuedAt.Date >= dateToProcessFrom && x.timestamps.issuedAt.Date <= dateToProcessTo) select y.amount).Sum(x => x) -
                    (from x in jvList from y in x.accounts.credit where y.code == c.code && (x.timestamps.issuedAt.Date >= dateToProcessFrom && x.timestamps.issuedAt.Date <= dateToProcessTo) select y.amount).Sum(x => x));
                cPrevious =
                    ((from x in crList from y in x.accounts.debit where y.code == c.code && x.timestamps.issuedAt.Date < dateToProcessFrom select y.amount).Sum(x => x) -
                    (from x in crList from y in x.accounts.credit where y.code == c.code && x.timestamps.issuedAt.Date < dateToProcessFrom select y.amount).Sum(x => x)) +
                    ((from x in cvList from y in x.accounts.debit where y.code == c.code && x.timestamps.issuedAt.Date < dateToProcessFrom select y.amount).Sum(x => x) -
                    (from x in cvList from y in x.accounts.credit where y.code == c.code && x.timestamps.issuedAt.Date < dateToProcessFrom select y.amount).Sum(x => x)) +
                    ((from x in jvList from y in x.accounts.debit where y.code == c.code && x.timestamps.issuedAt.Date < dateToProcessFrom select y.amount).Sum(x => x) -
                    (from x in jvList from y in x.accounts.credit where y.code == c.code && x.timestamps.issuedAt.Date < dateToProcessFrom select y.amount).Sum(x => x));

                cToDate = cToday + cPrevious;

                List<dynamic> subsidiaries = new List<dynamic>();

                var sToday = 0.0;
                var sPrev = 0.0;
                var sToDate = 0.0;
                foreach (subsidiary s in c.subsidiary)
                {
                    dynamic subsidiary = new ExpandoObject();
                    subsidiary.code = s.code;
                    subsidiary.name = s.name;

                    var today = 0.0;
                    var prev = 0.0;

                    today =
                    ((from x in crList from y in x.accounts.debit where y.code == s.code && (x.timestamps.issuedAt.Date >= dateToProcessFrom && x.timestamps.issuedAt.Date <= dateToProcessTo) select y.amount).Sum(x => x) -
                    (from x in crList from y in x.accounts.credit where y.code == s.code && (x.timestamps.issuedAt.Date >= dateToProcessFrom && x.timestamps.issuedAt.Date <= dateToProcessTo) select y.amount).Sum(x => x)) +
                    ((from x in cvList from y in x.accounts.debit where y.code == s.code && (x.timestamps.issuedAt.Date >= dateToProcessFrom && x.timestamps.issuedAt.Date <= dateToProcessTo) select y.amount).Sum(x => x) -
                    (from x in cvList from y in x.accounts.credit where y.code == s.code && (x.timestamps.issuedAt.Date >= dateToProcessFrom && x.timestamps.issuedAt.Date <= dateToProcessTo) select y.amount).Sum(x => x)) +
                    ((from x in jvList from y in x.accounts.debit where y.code == s.code && (x.timestamps.issuedAt.Date >= dateToProcessFrom && x.timestamps.issuedAt.Date <= dateToProcessTo) select y.amount).Sum(x => x) -
                    (from x in jvList from y in x.accounts.credit where y.code == s.code && (x.timestamps.issuedAt.Date >= dateToProcessFrom && x.timestamps.issuedAt.Date <= dateToProcessTo) select y.amount).Sum(x => x));
                    sToday += today;
                    prev =
                    ((from x in crList from y in x.accounts.debit where y.code == s.code && x.timestamps.issuedAt.Date < dateToProcessFrom select y.amount).Sum(x => x) -
                    (from x in crList from y in x.accounts.credit where y.code == s.code && x.timestamps.issuedAt.Date < dateToProcessFrom select y.amount).Sum(x => x)) +
                    ((from x in cvList from y in x.accounts.debit where y.code == s.code && x.timestamps.issuedAt.Date < dateToProcessFrom select y.amount).Sum(x => x) -
                    (from x in cvList from y in x.accounts.credit where y.code == s.code && x.timestamps.issuedAt.Date < dateToProcessFrom select y.amount).Sum(x => x)) +
                    ((from x in jvList from y in x.accounts.debit where y.code == s.code && x.timestamps.issuedAt.Date < dateToProcessFrom select y.amount).Sum(x => x) -
                    (from x in jvList from y in x.accounts.credit where y.code == s.code && x.timestamps.issuedAt.Date < dateToProcessFrom select y.amount).Sum(x => x));
                    sPrev += prev;
                    sToDate += today + prev;

                    subsidiary.today = today;
                    subsidiary.previous = prev;
                    subsidiary.toDate = today + prev;

                    subsidiaries.Add(subsidiary);
                }

                gl.today = cToday + sToday;
                gl.previous = cPrevious + sPrev;
                gl.toDate = cToDate + sToDate;

                gl.subsidiaries = subsidiaries;
                gls.Add(gl);
            }
            return gls;

        }

        public IEnumerable<dynamic> transactionList(DateTime from, DateTime to) {
            var dateFrom = from.Date;
            var dateTo = to.Date;
            
            //get all books

            var crList = _cr.all();
            var cvList = _acv.all();
            var jvList = _ajv.all();

            //get transaction list per date
            List<dynamic> list = new List<dynamic>();

            list.AddRange(
                from cr in crList 
                where cr.timestamps.issuedAt.Date >= dateFrom && cr.timestamps.issuedAt <= dateTo
                select new {
                    book = "CRB",
                    number = cr.number,
                    particulars = cr.particulars,
                    isCancelled = cr.isCancelled,
                    reason = cr.reason,
                    isClosed = cr.isClosed,
                    amount = cr.amount
                }
            );

            list.AddRange(
                from cv in cvList 
                where cv.timestamps.issuedAt.Date >= dateFrom && cv.timestamps.issuedAt <= dateTo
                select new {
                    book = "CDB",
                    number = cv.cvNumber,
                    particulars = cv.particulars,
                    isCancelled = cv.isCancelled,
                    reason = cv.reason,
                    isClosed = cv.isClosed,
                    amount = cv.amount
                }
            );

            list.AddRange(
                from jv in jvList 
                where jv.timestamps.issuedAt.Date >= dateFrom && jv.timestamps.issuedAt <= dateTo
                select new {
                    book = "GJB",
                    number = jv.number,
                    particulars = jv.particulars,
                    isCancelled = jv.isCancelled,
                    reason = jv.reason,
                    isClosed = jv.isClosed,
                    amount = jv.amount
                }
            );
            
            return list;
        }

        public IEnumerable<dynamic> generalLedger(Int32? account, Int32? year) {
            
            DateTime outD = new DateTime();
            var isValidDate = DateTime.TryParse("1/1/" + year.ToString(), out outD);
            
            if(year==null) {
                isValidDate = true;
                outD = DateTime.Now.Date;
            }

            if(!isValidDate) {
                goto error1;
            }

            
            var firstDayOfYear = Convert.ToDateTime("1/1/" + outD.Year.ToString());
            var nextMonth = DateTime.Now.AddMonths(1);
            var currentMonth = Convert.ToDateTime(nextMonth.Month.ToString() + "/1/" + nextMonth.Year.ToString()).AddDays(-1);
            if(year!=null) {
                currentMonth = Convert.ToDateTime("12/31/" + outD.Year.ToString()).AddDays(-1);
            }
           
            var coaList = _coa.all();
            var crList = _cr.all();
            var cvList = _acv.all();
            var jvList = _ajv.all();

            var item = new coa();
            if(account != null) {
                item = (from x in coaList where x.code == Convert.ToInt32(account) select x).FirstOrDefault();
                if(item == null) {
                    foreach(var c in coaList) {
                        foreach(var s in c.subsidiary) {
                            if(s.code == Convert.ToInt32(account)) {
                                item = c;
                            }
                        }
                    }
                }
                if(item==null) {
                    goto error2;
                } else {
                    coaList = new List<coa>{item};
                }
            }
            
            List<dynamic> gls = new List<dynamic>();

            foreach(coa coa in coaList ) {
                dynamic gl = new ExpandoObject();    
                gl.code = coa.code;
                gl.name = coa.name;


                List<dynamic> months = new List<dynamic>();
                for(DateTime curDate = firstDayOfYear;curDate<=currentMonth;curDate = curDate.AddMonths(1))
                {                    
                    List<dynamic> crbdaily = new List<dynamic>();
                    List<dynamic> cdbdaily = new List<dynamic>();
                    List<dynamic> gjbdaily = new List<dynamic>();


                    for(DateTime d = curDate; d<= curDate.AddMonths(1).AddSeconds(-1);d=d.AddDays(1)) {
                        DateTime dFrom = Convert.ToDateTime(d.ToString("MM/dd/yyyy") + " 00:00");
                        DateTime dTo = Convert.ToDateTime(d.ToString("MM/dd/yyyy") + " 23:59");

                        List<dynamic> crbDetails = new List<dynamic>();
                        List<dynamic> cdbDetails = new List<dynamic>();
                        List<dynamic> gjbDetails = new List<dynamic>();

                        foreach(cashReceipt cr in from c in crList where c.timestamps.issuedAt >= dFrom && c.timestamps.issuedAt <= dTo select c) {

                            foreach(crDebit crDebit in from x in cr.accounts.debit where x.code == coa.code  select x) {
                                dynamic detail = new ExpandoObject();
                                detail.date = cr.timestamps.issuedAt;
                                detail.book = "CRB";
                                detail.refNo = cr.number;
                                detail.particulars = cr.particulars;
                                detail.custsup = cr.payor.name;
                                detail.debitAmount = crDebit.amount; 
                                detail.creditAmount = 0;
                                crbDetails.Add(detail);
                            }

                            foreach(subsidiary s in coa.subsidiary) {
                                foreach(crDebit crDebit in from x in cr.accounts.debit where x.code == s.code select x) {
                                    dynamic detail = new ExpandoObject();
                                    detail.date = cr.timestamps.issuedAt;
                                    detail.book = "CRB";
                                    detail.refNo = cr.number;
                                    detail.particulars = cr.particulars;
                                    detail.custsup = cr.payor.name;
                                    detail.debitAmount = crDebit.amount; 
                                    detail.creditAmount = 0;
                                    crbDetails.Add(detail);
                                }
                            }
                            
                            foreach(crCredit crCredit in from x in cr.accounts.credit where x.code == coa.code select x) {
                                dynamic detail = new ExpandoObject();
                                detail.date = cr.timestamps.issuedAt;
                                detail.book = "CRB";
                                detail.refNo = cr.number;
                                detail.particulars = cr.particulars;
                                detail.custsup = cr.payor.name;
                                detail.debitAmount = 0; 
                                detail.creditAmount = crCredit.amount;
                                crbDetails.Add(detail);
                            }

                            foreach(subsidiary s in coa.subsidiary) {
                                foreach(crCredit crCredit in from x in cr.accounts.credit where x.code == s.code select x) {
                                    dynamic detail = new ExpandoObject();
                                    detail.date = cr.timestamps.issuedAt;
                                    detail.book = "CRB";
                                    detail.refNo = cr.number;
                                    detail.particulars = cr.particulars;
                                    detail.custsup = cr.payor.name;
                                    detail.debitAmount = 0; 
                                    detail.creditAmount = crCredit.amount;
                                    crbDetails.Add(detail);
                                }
                            }

                        }

                        foreach(acctngCv cv in from c in cvList where c.timestamps.issuedAt >= dFrom && c.timestamps.issuedAt <= dTo select c) {
                            foreach(cvDebit cvDebit in from x in cv.accounts.debit where x.code == coa.code select x) {
                                dynamic detail = new ExpandoObject();
                                detail.date = cv.timestamps.issuedAt;
                                detail.book = "CDB";
                                detail.refNo = cv.cvNumber;
                                detail.particulars = cv.particulars;
                                detail.custsup = cv.supplier.name;
                                detail.debitAmount = cvDebit.amount;
                                detail.creditAmount = 0;
                                cdbDetails.Add(detail);
                            }

                            foreach(subsidiary s in coa.subsidiary) {
                                foreach(cvDebit cvDebit in from x in cv.accounts.debit where x.code == s.code select x) {
                                    dynamic detail = new ExpandoObject();
                                    detail.date = cv.timestamps.issuedAt;
                                    detail.book = "CDB";
                                    detail.refNo = cv.cvNumber;
                                    detail.particulars = cv.particulars;
                                    detail.custsup = cv.supplier.name;
                                    detail.debitAmount = cvDebit.amount;
                                    detail.creditAmount = 0;
                                    cdbDetails.Add(detail);
                                }
                            }

                            foreach(cvCredit cvCredit in from x in cv.accounts.credit where x.code == coa.code select x) {
                                dynamic detail = new ExpandoObject();
                                detail.date = cv.timestamps.issuedAt;
                                detail.book = "CDB";
                                detail.refNo = cv.cvNumber;
                                detail.particulars = cv.particulars;
                                detail.custsup = cv.supplier.name;
                                detail.debitAmount = 0;
                                detail.creditAmount = cvCredit.amount;
                                cdbDetails.Add(detail);
                            }
                            
                            foreach(subsidiary s in coa.subsidiary) {
                                foreach(cvCredit cvCredit in from x in cv.accounts.credit where x.code == s.code select x) {
                                    dynamic detail = new ExpandoObject();
                                    detail.date = cv.timestamps.issuedAt;
                                    detail.book = "CDB";
                                    detail.refNo = cv.cvNumber;
                                    detail.particulars = cv.particulars;
                                    detail.custsup = cv.supplier.name;
                                    detail.debitAmount = 0;
                                    detail.creditAmount = cvCredit.amount;
                                    cdbDetails.Add(detail);
                                }
                            }
                        }

                        foreach(acctngJv jv in from c in jvList where c.timestamps.issuedAt >= dFrom && c.timestamps.issuedAt <= dTo select c) {
                            foreach(jvDebit jvDebit in from x in jv.accounts.debit where x.code == coa.code select x) {
                                dynamic detail = new ExpandoObject();
                                detail.date = jv.timestamps.issuedAt;
                                detail.book = "GJB";
                                detail.refNo = jv.number;
                                detail.particulars = jv.particulars;
                                detail.custsup = "";
                                detail.debitAmount = jvDebit.amount;
                                detail.creditAmount = 0;
                                gjbDetails.Add(detail);
                            }

                            foreach(subsidiary s in coa.subsidiary) {
                                foreach(jvDebit jvDebit in from x in jv.accounts.debit where x.code == s.code select x) {
                                    dynamic detail = new ExpandoObject();
                                    detail.date = jv.timestamps.issuedAt;
                                    detail.book = "GJB";
                                    detail.refNo = jv.number;
                                    detail.particulars = jv.particulars;
                                    detail.custsup = "";
                                    detail.debitAmount = jvDebit.amount;
                                    detail.creditAmount = 0;
                                    gjbDetails.Add(detail);
                                }
                            }

                            foreach(jvCredit jvCredit in from x in jv.accounts.credit where x.code == coa.code select x) {
                                dynamic detail = new ExpandoObject();
                                detail.date = jv.timestamps.issuedAt;
                                detail.book = "GJB";
                                detail.refNo = jv.number;
                                detail.particulars = jv.particulars;
                                detail.custsup = "";
                                detail.debitAmount = 0;
                                detail.creditAmount = jvCredit.amount;
                                gjbDetails.Add(detail);
                            }

                            foreach(subsidiary s in coa.subsidiary) {
                                foreach(jvCredit jvCredit in from x in jv.accounts.credit where x.code == s.code select x) {
                                    dynamic detail = new ExpandoObject();
                                    detail.date = jv.timestamps.issuedAt;
                                    detail.book = "GJB";
                                    detail.refNo = jv.number;
                                    detail.particulars = jv.particulars;
                                    detail.custsup = "";
                                    detail.debitAmount = 0;
                                    detail.creditAmount = jvCredit.amount;
                                    gjbDetails.Add(detail); 
                                }
                            }
                        }

                        
                        if(crbDetails.Count>0) {
                            dynamic detail = new ExpandoObject();
                            detail.date = d.Date.ToString("MM/dd/yyyy");
                            detail.details = crbDetails;
                            crbdaily.Add(detail);
                        }

                        if(cdbDetails.Count>0) {
                            dynamic detail = new ExpandoObject();
                            detail.date = d.Date.ToString("MM/dd/yyyy");
                            detail.details = cdbDetails;
                            cdbdaily.Add(detail);
                        }

                        if(gjbDetails.Count>0) {
                            dynamic detail = new ExpandoObject();
                            detail.date = d.Date.ToString("MM/dd/yyyy");
                            detail.details = gjbDetails;
                            gjbdaily.Add(detail);
                        }
                    }
                    
                    List<dynamic> bookDet = new List<dynamic>();

                    dynamic bookCrb = new ExpandoObject();
                    bookCrb.book = "CRB";
                    bookCrb.details = crbdaily;
                    bookDet.Add(bookCrb);

                    dynamic bookCdb = new ExpandoObject();
                    bookCdb.book = "CDB";
                    bookCdb.details = cdbdaily;
                    bookDet.Add(bookCdb);
            
                    dynamic bookGjb = new ExpandoObject();
                    bookGjb.book = "GJB";
                    bookGjb.details = gjbdaily;
                    bookDet.Add(bookGjb);

                    dynamic mo = new ExpandoObject();
                    mo.month = curDate.ToString("MMMM yyyy");
                    mo.details = bookDet;
                    months.Add(mo);
                }

                gl.details = months;
                //prev years
                var coaAmount = 0.0;
                var crListPerCoa = (from cr in crList where cr.timestamps.issuedAt < firstDayOfYear select cr.accounts).ToList();
                var cvListPerCoa = (from cv in cvList where cv.timestamps.issuedAt < firstDayOfYear select cv.accounts).ToList();
                var jvListPerCoa = (from jv in jvList where jv.timestamps.issuedAt < firstDayOfYear select jv.accounts).ToList();

                crListPerCoa.ToList().ForEach((x) => {
                    x.credit.ToList().ForEach((y) => {
                        if (y.code == coa.code) {
                            coaAmount += y.amount;
                        }
                        
                        foreach(subsidiary s in coa.subsidiary) {
                            if(y.code==s.code) {
                                coaAmount += y.amount;
                            }
                        }
                    });

                    x.debit.ToList().ForEach((y) => {
                        if (y.code == coa.code) {
                            coaAmount += y.amount;
                        }
                        
                        foreach(subsidiary s in coa.subsidiary) {
                            if(y.code==s.code) {
                                coaAmount += y.amount;
                            }
                        }
                    });
                    
                });

                cvListPerCoa.ToList().ForEach((x) => {
                    x.credit.ToList().ForEach((y) => {
                        if (y.code == coa.code) {
                            coaAmount += y.amount;
                        }
                        
                        foreach(subsidiary s in coa.subsidiary) {
                            if(y.code==s.code) {
                                coaAmount += y.amount;
                            }
                        }
                    });

                    x.debit.ToList().ForEach((y) => {
                        if (y.code == coa.code) {
                            coaAmount += y.amount;
                        }
                        
                        foreach(subsidiary s in coa.subsidiary) {
                            if(y.code==s.code) {
                                coaAmount += y.amount;
                            }
                        }
                    });
                });

                jvListPerCoa.ToList().ForEach((x) => {
                    x.credit.ToList().ForEach((y) => {
                        if (y.code == coa.code) {
                            coaAmount += y.amount;
                        }

                        foreach(subsidiary s in coa.subsidiary) {
                            if(y.code==s.code) {
                                coaAmount += y.amount;
                            }
                        }
                    });

                    x.debit.ToList().ForEach((y) => {
                        if (y.code == coa.code) {
                            coaAmount += y.amount;
                        }
                        
                        foreach(subsidiary s in coa.subsidiary) {
                            if(y.code==s.code) {
                                coaAmount += y.amount;
                            }
                        }
                    });
                });
                
                dynamic fmonth = new ExpandoObject();
                fmonth.month = firstDayOfYear.AddDays(-1).ToString("MMMM yyyy");
                fmonth.amount = coaAmount;
                months.Insert(0,fmonth);
               
                //subsidiary
                List<dynamic> subsidiaries = new List<dynamic>();
                foreach(subsidiary subsidiary in coa.subsidiary) {
                    months = new List<dynamic>();
                    
                    for(DateTime curDate = firstDayOfYear;curDate<=currentMonth;curDate = curDate.AddMonths(1))
                    {
                        List<dynamic> crbdaily = new List<dynamic>();
                        List<dynamic> cdbdaily = new List<dynamic>();
                        List<dynamic> gjbdaily = new List<dynamic>();


                        for(DateTime d = curDate; d<= curDate.AddMonths(1).AddSeconds(-1);d=d.AddDays(1)) {
                            DateTime dFrom = Convert.ToDateTime(d.ToString("MM/dd/yyyy") + " 00:00");
                            DateTime dTo = Convert.ToDateTime(d.ToString("MM/dd/yyyy") + " 23:59");


                            List<dynamic> crbDetails = new List<dynamic>();
                            List<dynamic> cdbDetails = new List<dynamic>();
                            List<dynamic> gjbDetails = new List<dynamic>();
                            
                            foreach(cashReceipt cr in from c in crList where c.timestamps.issuedAt >= dFrom && c.timestamps.issuedAt <= dTo select c) {
                                foreach(crDebit crDebit in from x in cr.accounts.debit where x.code == subsidiary.code select x) {
                                    dynamic mCrb = new ExpandoObject();
                                    mCrb.date = cr.timestamps.issuedAt;
                                    mCrb.book = "CRB";
                                    mCrb.refNo = cr.number;
                                    mCrb.particulars = cr.particulars;
                                    mCrb.custsup = cr.payor.name;
                                    mCrb.debitAmount = crDebit.amount; 
                                    mCrb.creditAmount = 0;
                                    crbDetails.Add(mCrb);                          
                                }
                                foreach(crCredit crCredit in from x in cr.accounts.credit where x.code == subsidiary.code select x) {
                                    dynamic mCrb = new ExpandoObject();
                                    mCrb.date = cr.timestamps.issuedAt;
                                    mCrb.book = "CRB";
                                    mCrb.refNo = cr.number;
                                    mCrb.particulars = cr.particulars;
                                    mCrb.custsup = cr.payor.name;
                                    mCrb.debitAmount = 0; 
                                    mCrb.creditAmount = crCredit.amount;
                                    crbDetails.Add(mCrb);
                                }
                            }
                            foreach(acctngCv cv in from c in cvList where c.timestamps.issuedAt >= dFrom && c.timestamps.issuedAt <= dTo select c) {
                                foreach(cvDebit cvDebit in from x in cv.accounts.debit where x.code == subsidiary.code select x) {
                                    dynamic mCdb = new ExpandoObject();
                                    mCdb.date = cv.timestamps.issuedAt;
                                    mCdb.book = "CDB";
                                    mCdb.refNo = cv.cvNumber;
                                    mCdb.particulars = cv.particulars;
                                    mCdb.custsup = cv.supplier.name;
                                    mCdb.debitAmount = cvDebit.amount; 
                                    mCdb.creditAmount = 0;
                                    cdbDetails.Add(mCdb);
                                }
                                foreach(cvCredit cvCredit in from x in cv.accounts.credit where x.code == subsidiary.code select x) {
                                    dynamic mCdb = new ExpandoObject();
                                    mCdb.date = cv.timestamps.issuedAt;
                                    mCdb.book = "CDB";
                                    mCdb.refNo = cv.cvNumber;
                                    mCdb.particulars = cv.particulars;
                                    mCdb.custsup = cv.supplier.name;
                                    mCdb.debitAmount = 0; 
                                    mCdb.creditAmount = cvCredit.amount;
                                    cdbDetails.Add(mCdb);
                                }
                            }
                            foreach(acctngJv jv in from c in jvList where c.timestamps.issuedAt >= dFrom && c.timestamps.issuedAt <= dTo select c) {
                                foreach(jvDebit jvDebit in from x in jv.accounts.debit where x.code == subsidiary.code select x) {
                                    dynamic mGjb = new ExpandoObject();
                                    mGjb.date = jv.timestamps.issuedAt;
                                    mGjb.book = "GJB";
                                    mGjb.refNo = jv.number;
                                    mGjb.particulars = jv.particulars;
                                    mGjb.custsup = "";
                                    mGjb.debitAmount = jvDebit.amount; 
                                    mGjb.creditAmount = 0;
                                    gjbDetails.Add(mGjb);
                                }
                                foreach(jvCredit jvCredit in from x in jv.accounts.credit where x.code == subsidiary.code select x) {
                                    dynamic mGjb = new ExpandoObject();
                                    mGjb.date = jv.timestamps.issuedAt;
                                    mGjb.book = "GJB";
                                    mGjb.refNo = jv.number;
                                    mGjb.particulars = jv.particulars;
                                    mGjb.custsup = "";
                                    mGjb.debitAmount = 0;
                                    mGjb.creditAmount = jvCredit.amount;
                                    gjbDetails.Add(mGjb);
                                }
                            }

                            if(crbDetails.Count>0) {
                                dynamic mCrbdaily = new ExpandoObject();
                                mCrbdaily.date = d.Date.ToString("MM/dd/yyyy");
                                mCrbdaily.details = crbDetails;
                                crbdaily.Add(mCrbdaily);
                            }

                            if(cdbDetails.Count>0) {
                                dynamic mCdbdaily = new ExpandoObject();
                                mCdbdaily.date = d.Date.ToString("MM/dd/yyyy");
                                mCdbdaily.details = cdbDetails;
                                cdbdaily.Add(mCdbdaily);
                            }

                            if(gjbDetails.Count>0) {
                                dynamic mGjbdaily = new ExpandoObject();
                                mGjbdaily.date = d.Date.ToString("MM/dd/yyyy");
                                mGjbdaily.details = gjbDetails;
                                gjbdaily.Add(mGjbdaily);
                            }
                        }


                        List<dynamic> bookDet = new List<dynamic>();
                        
                        dynamic crbBook = new ExpandoObject();
                        crbBook.book = "CRB";
                        crbBook.details = crbdaily;
                        bookDet.Add(crbBook);

                        dynamic cdbBook = new ExpandoObject();
                        cdbBook.book = "CDB";
                        cdbBook.details = cdbdaily;
                        bookDet.Add(cdbBook);

                        dynamic gjbBook = new ExpandoObject();
                        gjbBook.book = "GJB";
                        gjbBook.details = gjbdaily;
                        bookDet.Add(gjbBook);
                    
                        dynamic subMonth = new ExpandoObject();
                        subMonth.month = curDate.ToString("MMMM yyyy");
                        subMonth.details = bookDet;
                        months.Add(subMonth);
                    }

                   //prev years
                    var subAmount = 0.0;
                    var crListPerSub = (from cr in crList where cr.timestamps.issuedAt < firstDayOfYear select cr.accounts).ToList();
                    var cvListPerSub = (from cv in cvList where cv.timestamps.issuedAt < firstDayOfYear select cv.accounts).ToList();
                    var jvListPerSub = (from jv in jvList where jv.timestamps.issuedAt < firstDayOfYear select jv.accounts).ToList();

                    crListPerSub.ToList().ForEach((x) => {
                        x.credit.ToList().ForEach((y) => {
                            if (y.code == subsidiary.code) {
                                subAmount += y.amount;
                            }
                        });

                        x.debit.ToList().ForEach((y) => {
                            if (y.code == subsidiary.code) {
                                subAmount += y.amount;
                            }
                        });
                    });

                    cvListPerSub.ToList().ForEach((x) => {
                        x.credit.ToList().ForEach((y) => {
                            if (y.code == subsidiary.code) {
                                subAmount += y.amount;
                            }
                        });

                        x.debit.ToList().ForEach((y) => {
                            if (y.code == subsidiary.code) {
                                subAmount += y.amount;
                            }
                        });
                    });

                    jvListPerSub.ToList().ForEach((x) => {
                        x.credit.ToList().ForEach((y) => {
                            if (y.code == subsidiary.code) {
                                subAmount += y.amount;
                            }
                        });

                        x.debit.ToList().ForEach((y) => {
                            if (y.code == subsidiary.code) {
                                subAmount += y.amount;
                            }
                        });
                    });

                    dynamic m = new ExpandoObject();
                    m.month = firstDayOfYear.AddDays(-1).ToString("MMMM yyyy");
                    m.amount = subAmount;
                    months.Insert(0,m);
                    
                    dynamic s = new ExpandoObject();
                    s.code = subsidiary.code;
                    s.name = subsidiary.name;
                    s.details = months;

                    subsidiaries.Add(s);
                }
                gl.subsidiaries = subsidiaries;
                gls.Add(gl);
            }
            goto final;

error1:
            return null;
error2:
            return null;
final:
            return gls;
        }

        public List<transactionDetailed> transactionDetailedList(DateTime from, DateTime to, Int32 accountCode = 0, Boolean includeSubsidiaryIfThereIs = false) {
            var dateFrom = from.Date;
            var dateTo = to.Date;
            
            //get all books
            var coa = _coa.all();
            var crList = _cr.all(from, to);
            var cvList = _acv.all(from, to);
            var jvList = _ajv.all(from, to);

            List<Int32> codes = new List<Int32>();

            if(accountCode> 0) {
                coa.ToList().ForEach(_coa=>{
                    if(accountCode==_coa.code) {
                        codes.Add(accountCode);
                        if(includeSubsidiaryIfThereIs) {
                            codes.AddRange(_coa.subsidiary.Select(_sub=>_sub.code).ToList());
                        }
                    }
                });
            } else {
                codes.AddRange(coa.Select(x=>x.code));
                coa.ToList().ForEach(x=>{
                    codes.AddRange(x.subsidiary.Select(y=>y.code));
                });
            }

            
            List<transactionDetailed> list = new List<transactionDetailed>();



            (from cr in crList where cr.timestamps.issuedAt.Date >= dateFrom && cr.timestamps.issuedAt <= dateTo select cr).ToList().ForEach(crItem=>{
                crItem.accounts.credit.ToList().ForEach(c=>{
                    if(codes.Find(f=>f == c.code) > 0) {
                        list.Add(new transactionDetailed {
                            slug = crItem.slug,
                            number = crItem.number,
                            book  = "CRB",
                            particulars = crItem.particulars,
                            accountType = "credit" ,
                            code = c.code,
                            name = c.name,
                            amount = c.amount,
                            issuedAt = crItem.timestamps.issuedAt
                        });
                    }
                });
                crItem.accounts.debit.ToList().ForEach(d=>{
                    if(codes.Find(f=>f == d.code) > 0) {
                        list.Add(new transactionDetailed {
                            slug = crItem.slug,
                            number = crItem.number,
                            book  = "CRB",
                            particulars = crItem.particulars,
                            accountType = "debit" ,
                            code = d.code,
                            name = d.name,
                            amount = d.amount,
                            issuedAt = crItem.timestamps.issuedAt
                        });
                    }
                });
            });

            (from cv in cvList where cv.timestamps.issuedAt.Date >= dateFrom && cv.timestamps.issuedAt <= dateTo select cv).ToList().ForEach(cvItem=>{
                cvItem.accounts.credit.ToList().ForEach(c=>{
                    if(codes.Find(f=>f == c.code) > 0) {
                        list.Add(new transactionDetailed {
                            slug = cvItem.slug,
                            number = cvItem.cvNumber,
                            book  = "CDB",
                            particulars = cvItem.particulars,
                            accountType = "credit" ,
                            code = c.code,
                            name = c.name,
                            amount = c.amount,
                            issuedAt = cvItem.timestamps.issuedAt
                        });
                    }
                });
                cvItem.accounts.debit.ToList().ForEach(d=>{
                    if(codes.Find(f=>f == d.code) > 0) {
                        list.Add(new transactionDetailed {
                            slug = cvItem.slug,
                            number = cvItem.cvNumber,
                            book  = "CDB",
                            particulars = cvItem.particulars,
                            accountType = "debit" ,
                            code = d.code,
                            name = d.name,
                            amount = d.amount,
                            issuedAt = cvItem.timestamps.issuedAt
                        });
                    }
                });
            });

            (from jv in jvList where jv.timestamps.issuedAt.Date >= dateFrom && jv.timestamps.issuedAt <= dateTo select jv).ToList().ForEach(jvItem=>{
                jvItem.accounts.credit.ToList().ForEach(c=>{
                    if(codes.Find(f=>f == c.code) > 0) {
                        list.Add(new transactionDetailed {
                            slug = jvItem.slug,
                            number = jvItem.jvNumber,
                            book  = "JV",
                            particulars = jvItem.particulars,
                            accountType = "credit" ,
                            code = c.code,
                            name = c.name,
                            amount = c.amount,
                            issuedAt = jvItem.timestamps.issuedAt
                        });
                    }
                });
                jvItem.accounts.debit.ToList().ForEach(d=>{
                    if(codes.Find(f=>f == d.code) > 0) {
                        list.Add(new transactionDetailed {
                            slug = jvItem.slug,
                            number = jvItem.jvNumber,
                            book  = "JV",
                            particulars = jvItem.particulars,
                            accountType = "debit" ,
                            code = d.code,
                            name = d.name,
                            amount = d.amount,
                            issuedAt = jvItem.timestamps.issuedAt
                        });
                    }
                });
            });

            list.ForEach(x=>{
                x.motherCode = Convert.ToInt32(x.code.ToString().Substring(0,3));
                x.codeElement = coa.Where(c=>c.code == x.motherCode).First().element;
            });
            return list.OrderBy(x=>x.issuedAt).ToList();
        }

        public IEnumerable<dynamic> budgetPerTransaction(Int32 year)
        {
            var budList = _bud.all().Where(c=>c.budgetYear >= year && c.budgetYear <= year);;

            List<dynamic> list = new List<dynamic>();
                        
            var cvList = _acv.all().Where(c=>c.timestamps.issuedAt >= Convert.ToDateTime("1/1/" + year.ToString()) && c.timestamps.issuedAt <= Convert.ToDateTime("12/31/" + year.ToString()));
            var jvList = _ajv.all().Where(c=>c.timestamps.issuedAt >= Convert.ToDateTime("1/1/" + year.ToString()) && c.timestamps.issuedAt <= Convert.ToDateTime("12/31/" + year.ToString()));

            budList.ToList().ForEach(b=>{
                b.budget.ToList().ForEach(_b=>{
                    
                    cvList.ToList().ForEach(cv=>{
                        cv.accounts.credit.ToList().ForEach(acv=>{
                            if(acv.budget == _b.code) {
                                dynamic item = new ExpandoObject();
                                item.budgetCode = _b.code;
                                item.departmentName = b.departmentName;

                                item.budget = _b.amount;
                                item.book = "CV";
                                item.no = acv.code;
                                item.amount = -acv.amount;
                                list.Add(item);
                            }
                        });

                        cv.accounts.debit.ToList().ForEach(acv=>{
                            if(acv.budget == _b.code) {
                                dynamic item = new ExpandoObject();
                                item.budgetCode = _b.code;
                                item.departmentName = b.departmentName;

                                item.budget = _b.amount;
                                item.book = "CV";
                                item.no = acv.code;
                                item.amount = acv.amount;
                                list.Add(item);
                            }
                        });
                    });

                    jvList.ToList().ForEach(jv=>{
                        jv.accounts.credit.ToList().ForEach(ajv=>{
                            if(ajv.budget == _b.code) {
                                dynamic item = new ExpandoObject();
                                item.budgetCode = _b.code;
                                item.departmentName = b.departmentName;

                                item.budget = _b.amount;
                                item.book = "JV";
                                item.no = ajv.code;
                                item.amount = -ajv.amount;
                                list.Add(item);
                            }
                        });

                        jv.accounts.debit.ToList().ForEach(ajv=>{
                            if(ajv.budget == _b.code) {
                                dynamic item = new ExpandoObject();
                                item.budgetCode = _b.code;
                                item.departmentName = b.departmentName;

                                item.budget = _b.amount;
                                item.book = "JV";
                                item.no = ajv.code;
                                item.amount = ajv.amount;
                                list.Add(item);
                            }
                        });
                    });

                   
                });
            });

            return list;
        }

        public IEnumerable<dynamic> budgetSummary(Int32 year)
        {
            var budList = _bud.all().Where(c=>c.budgetYear >= year && c.budgetYear <= year).ToList();

            List<dynamic> list = new List<dynamic>();
                        
            var cvList = _acv.all().Where(c=>c.timestamps.issuedAt >= Convert.ToDateTime("1/1/" + year.ToString()) && c.timestamps.issuedAt <= Convert.ToDateTime("12/31/" + year.ToString()));
            var jvList = _ajv.all().Where(c=>c.timestamps.issuedAt >= Convert.ToDateTime("1/1/" + year.ToString()) && c.timestamps.issuedAt <= Convert.ToDateTime("12/31/" + year.ToString()));

            List<String> distinctDepName = (from x in budList select x.departmentName).Distinct().ToList();

            distinctDepName.ForEach(dist=>{
                
                
                budList.ToList().ForEach(b=>{
                    
                    var debit = 0.0;
                    var credit = 0.0;
                    var depBudg = 0.0;
                    
                    if(dist == b.departmentName) {
                        dynamic department = new ExpandoObject();
                        department.departmentName = dist;
                        
                        List<dynamic> transList = new List<dynamic>();

                        

                        b.budget.ToList().ForEach(_b=>{
                            var bDebit = 0.0;
                            var bCredit = 0.0;

                            List<dynamic> itemDetails = new List<dynamic>();
                            var runBal = _b.amount;
                            cvList.ToList().ForEach(cv=>{
                                
                                cv.accounts.credit.ToList().ForEach(acv=>{
                                    dynamic itemDetail = new ExpandoObject();
                                    if(acv.budget == _b.code) {
                                        credit+= -acv.amount;
                                        bCredit+= -acv.amount;
                                        itemDetail.date = cv.timestamps.issuedAt;
                                        itemDetail.particulars = cv.particulars;
                                        itemDetail.book = "CV";
                                        itemDetail.referenceNo = cv.cvNumber;
                                        itemDetail.amount = -acv.amount;
                                        runBal -= -acv.amount;
                                        itemDetail.runningBalance = runBal;
                                        itemDetails.Add(itemDetail);
                                    }
                                });

                                cv.accounts.debit.ToList().ForEach(acv=>{
                                    dynamic itemDetail = new ExpandoObject();
                                    if(acv.budget == _b.code) {
                                        debit+= acv.amount;
                                        bDebit+= acv.amount;
                                        itemDetail.date = cv.timestamps.issuedAt;
                                        itemDetail.particulars = cv.particulars;
                                        itemDetail.book = "CV";
                                        itemDetail.referenceNo = cv.cvNumber;
                                        itemDetail.amount = acv.amount;
                                        runBal -= acv.amount;
                                        itemDetail.runningBalance = runBal;
                                        itemDetails.Add(itemDetail);
                                    }
                                });
                            });

                            jvList.ToList().ForEach(jv=>{
                                
                                jv.accounts.credit.ToList().ForEach(ajv=>{
                                    dynamic itemDetail = new ExpandoObject();
                                    if(ajv.budget == _b.code) {
                                        credit+= -ajv.amount;
                                        bCredit+= -ajv.amount;
                                        itemDetail.date = jv.timestamps.issuedAt;
                                        itemDetail.particulars = jv.particulars;
                                        itemDetail.book = "JV";
                                        itemDetail.referenceNo = jv.jvNumber;
                                        itemDetail.amount = -ajv.amount;
                                        runBal -= -ajv.amount;
                                        itemDetail.runningBalance = runBal;
                                        itemDetails.Add(itemDetail);
                                    }
                                });

                                jv.accounts.debit.ToList().ForEach(ajv=>{
                                    dynamic itemDetail = new ExpandoObject();
                                    if(ajv.budget == _b.code) {
                                        debit+= ajv.amount;
                                        bDebit+= ajv.amount;
                                        itemDetail.date = jv.timestamps.issuedAt;
                                        itemDetail.particulars = jv.particulars;
                                        itemDetail.book = "JV";
                                        itemDetail.referenceNo = jv.jvNumber;
                                        itemDetail.amount = ajv.amount;
                                        runBal -= ajv.amount;
                                        itemDetail.runningBalance = runBal;
                                        itemDetails.Add(itemDetail);
                                    }
                                });
                            });

                            depBudg += _b.amount;

                            dynamic item = new ExpandoObject();
                            item.budgetCode = _b.code;
                            item.budget = _b.amount;
                            item.actual = bCredit + bDebit;
                            item.remaining = _b.amount - (bCredit + bDebit);
                            item.details = itemDetails;
                            transList.Add(item);
                        });

                        department.budget = depBudg;
                        department.actual = credit + debit;
                        department.remaining = depBudg - (credit + debit);
                        department.details = transList;
                        list.Add(department);
                    }
                });
            });
            

            return list;
        }
    }
}
