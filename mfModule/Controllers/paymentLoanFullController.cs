﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Filters;
using Microsoft.AspNetCore.Cors;
using kmbi_core_master.mfModule.Models;

namespace kmbi_core_master.coreMaster.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/paymentLoanFull")]
    [usersAuth]
    public class paymentLoansFullController : Controller
    {
        private readonly ipaymentLoanFullRepository _Repository;
        private readonly iusersRepository _logInfo;
        private string token;

        public paymentLoansFullController(ipaymentLoanFullRepository Repositor, iusersRepository logInfo)
        {
            this._Repository = Repositor;
            _logInfo = logInfo;
        }

        public void logger(string message)
        {
            //Insert Log//
            token = HttpContext.Request.Headers["token"];
            var lInfo = _logInfo.getLoginInfo(token);
            string log = message;
            global.createLog(lInfo.id, lInfo.fullname, lInfo.employment.branch, log);
            //Insert Log//
        }

        // GET: 
        [HttpGet]
        public IEnumerable<paymentLoanFull> GetAll()
        {
            var c = _Repository.all();
            return c;

        }

        //GET id
        [HttpGet("{id}")]
        public IActionResult getId(string id)
        {

            var item = _Repository.getId(id);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }
                
        [HttpGet("slug/{slug}")]
        public IActionResult getSlug(string slug)
        {

            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        [HttpGet("get")]
        public IActionResult getIdWeekNo([FromQuery] string loan)
        {

            var item = _Repository.getIdWeekNo(loan);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        [HttpPost]
        //[errorHandler]
        public IActionResult create([FromBody] paymentLoanFull b)
        {
            //try
            //{
                _Repository.add(b);

                logger( "Insert full payment of " + b.member.name.fullname);

                return Ok(new
                {
                    type = "success",
                    message = "Full payment of " + b.member.name.fullname + " created successfully.",
                    slug = b.slug
                });
            //}
            //catch (Exception x)
            //{
            //    return BadRequest(new
            //        {
            //        type = "error",
            //        message = x.Message
            //    });
            //}


        }


        


        [HttpPut("{id}")]
        public IActionResult update(string id, [FromBody] paymentLoanFull b)
        {
            var item = _Repository.getId(id);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _Repository.update(id, b);

            logger("Modified full payment of " + b.member.name.fullname);

            return Ok(new
            {
                type = "success",
                message = "Full payment of " + b.member.name.fullname + " updated successfully.",
            });
        }



        [HttpPut("slug/{slug}")]
        public IActionResult slugupdate(string slug, [FromBody] paymentLoanFull b)
        {
            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _Repository.updateSlug(slug, b);

           logger("Modified full payment of " + b.member.name.fullname);

            return Ok(new
            {
                type = "success",
                message = "Full payment of " + b.member.name.fullname + " updated successfully.",
            });
        }



        [HttpDelete("{id}")]
        public IActionResult delete(string id)
        {

            var item = _Repository.getId(id);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.remove(id);

           logger("Delete full payment of " + item.member.name.fullname);

            return Ok(new
            {
                type = "success",
                message = "Full payment of " + item.member.name.fullname + " deleted successfully.",
            });
        }


        [HttpDelete("slug/{slug}")]
        public IActionResult slugdelete(string slug)
        {

            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.removeSlug(slug);

           logger("Delete full payment of " + item.member.name.fullname);

            return Ok(new
            {
                type = "success",
                message = "Full payment of " + item.member.name.fullname + " deleted successfully.",
            });
        }


    }
}
