﻿using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.payrollModule.IRepository;
using kmbi_core_master.payrollModule.Models;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace kmbi_core_master.payrollModule.Repository
{
    public class cutOffRepository : icutOffRepository
    {
        IMongoCollection<cutOff> _collection;
        public cutOffRepository(IOptions<dbSettings> option)
        {
            var client = new MongoClient(option.Value.MongoConnection);
            var database = client.GetDatabase(option.Value.Database);
            _collection = database.GetCollection<cutOff>("cutOff");
        }

        public void createCutOff(cutOff cutOff)
        {
            _collection.InsertOne(cutOff);
        }

        public void deleteCutOff(string slug)
        {
            _collection.DeleteOne(new BsonDocument {
                {"slug",slug}
            });
        }

        public cutOff getCutOff(String slug)
        {
            return _collection.Find(new BsonDocument {
                {"slug",slug}
            }).FirstOrDefault();
        }

        public cutOff getCurrentCutOff()
        {
            return _collection.Find(new BsonDocument {
                {"active",true}
            }).FirstOrDefault();
        }

        public cutOff[] getCutOffs()
        {
            return _collection.Find(Builders<cutOff>.Filter.Empty).ToList().ToArray();
        }

        public void updateCutOff(string slug, cutOff cutOff)
        {
            _collection.UpdateOne(
                Builders<cutOff>.Filter.Eq(c=>c.slug, slug), 
                Builders<cutOff>.Update
                    .Set("periodRange", cutOff.periodRange)
                    .Set("cutOffRange", cutOff.cutOffRange)
                    .Set("active", cutOff.active)
                    .Set("attendanceApproved", cutOff.attendanceApproved)
                    .Set("attendanceApprovedAt", cutOff.attendanceApprovedAt)
                    .Set("attendanceApprovedBy", cutOff.attendanceApprovedBy)
                    .Set("payrollApproved", cutOff.payrollApproved)
                    .Set("payrollApprovedAt", cutOff.payrollApprovedAt)
                    .Set("payrollApprovedBy", cutOff.payrollApprovedBy)
                    .Set("updatedAt", cutOff.updatedAt)
            );
        }
    }
}
