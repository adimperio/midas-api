﻿using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.payrollModule.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.payrollModule.IRepository
{
    public interface ipayrollRepository
    {
        IEnumerable<payroll> getAll();

        payroll Get(String id);

        payroll getSlug(String slug);
        
        void Add(payroll c);

        void Update(String slug, payroll c);
              
        void Remove(String slug);

        payroll checkSlug(String slug);

        IEnumerable<payroll> search(string name);

        void addEntity(payrollEntities u, string Id);

        void updateEntity(String payrollSlug, string entitySlug, payrollEntities u);

        void removeEntity(String payrollSlug, string entitySlug);

        payrollEntityList getEntitySlug(string entitySlug);

        List<payEnt> getPayroll(string branchSlug);

        List<payroll> getPayroll(String employeeSlug, DateTime from, DateTime to);

        void addPayrolls(List<payroll> payrolls, DateTime from, DateTime to);

        List<payroll> getPayrollByBranchSlug(String branchSlug, DateTime from, DateTime to);
    }

}
