﻿using kmbi_core_master.coreMaster.IRepository;
using System;
using System.Collections.Generic;
using kmbi_core_master.coreMaster.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using MongoDB.Bson;

namespace kmbi_core_master.coreMaster.Repository
{
    public class addressRepository : dbCon, iaddressRepository
    {
        public addressRepository(IOptions<dbSettings> settings) : base(settings)
        {
        }

        

        public List<addRegion> allRegion()
        {
            var region = _db.GetCollection<addRegion>("addRegion").Find(new BsonDocument()).ToListAsync();
            return region.Result;
        }

        public List<addProvince> allProvince()
        {
            var pro = _db.GetCollection<addProvince>("addProvince").Find(new BsonDocument()).ToListAsync();
            return pro.Result;
        }

        public addRegion getRegion(string slug)
        {
            var query = Builders<addRegion>.Filter.Eq(e => e.slug, slug);
            var region = _db.GetCollection<addRegion>("addRegion").Find(query).FirstOrDefault();

            return region;
        }

        public List<addProvince> getProvince(string regSlug)
        {
            var query = Builders<addProvince>.Filter.Eq(e => e.regionSlug, regSlug);
            var province = _db.GetCollection<addProvince>("addProvince").Find(query).ToListAsync();

            return province.Result;
        }

        public List<addMunicipality> getMunicipality(string proSlug)
        {
            var query = Builders<addMunicipality>.Filter.Eq(e => e.provinceSlug, proSlug);
            var mun = _db.GetCollection<addMunicipality>("addMunicipality").Find(query).ToListAsync();

            return mun.Result;
        }

        public List<addBarangay> getBarangay(string munSlug)
        {
            var query = Builders<addBarangay>.Filter.Eq(e => e.municipalitySlug, munSlug);
            var bar = _db.GetCollection<addBarangay>("addBarangay").Find(query).ToListAsync();

            return bar.Result;
        }

    }
}
