﻿using kmbi_core_master.adminModule.Inv.Models;
using kmbi_core_master.coreMaster.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.adminModule.Inv.IRepository
{
    public interface iitemsRepository
    {
        void Add(items i);

        IEnumerable<items> getAll();

        items getBySlug(String slug);

        void update(String slug, items p);

        void remove(String slug);
    }
}
