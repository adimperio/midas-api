﻿using kmbi_core_master.acctngModule.Models;
using kmbi_core_master.hrModule.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.acctngModule.Interface
{
    public interface ibudgetRepository
    {
        IEnumerable<budget> all();

        budget getSlug(String slug);

        void add(budget b);

        void update(String slug, budget b);

        void remove(String slug);
    }
}
