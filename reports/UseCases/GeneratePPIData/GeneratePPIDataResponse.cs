using System;
using kmbi_core_master.spm.Models;

namespace kmbi_core_master.reports.UseCases.GeneratePPIData
{
    public class GeneratePPIDataResponse {
        public DateTime AsOf {get;set;}
        public PPI[] Details {get;set;}
        public ppiScore PPIScore {get;set;}
    }


}