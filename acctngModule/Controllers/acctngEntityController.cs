﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Filters;
using kmbi_core_master.hrModule.Interface;
using kmbi_core_master.hrModule.Models;
using Microsoft.AspNetCore.Cors;
using System;
using kmbi_core_master.acctngModule.Interface;
using kmbi_core_master.acctngModule.Models;
using kmbi_core_master.coreMaster;
using kmbi_core_master.coreMaster.IRepository;

namespace kmbi_core_master.acctngModule.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/acctngEntity")]
    [usersAuth]
    public class acctngEntityController : Controller
    {
        private readonly iacctngEntityRepository _Repository;
        private readonly iusersRepository _logInfo;
        private string token;

        public acctngEntityController(iacctngEntityRepository Repositor, iusersRepository logInfo)
        {
            this._Repository = Repositor;
            _logInfo = logInfo;
        }

        public void logger(string message)
        {
            //Insert Log//
            token = HttpContext.Request.Headers["token"];
            var lInfo = _logInfo.getLoginInfo(token);
            string log = message;
            global.createLog(lInfo.id, lInfo.fullname, lInfo.employment.branch, log);
            //Insert Log//
        }

        // GET: 
        [HttpGet]
        public IEnumerable<acctngEntity> GetAll()
        {
            var c = _Repository.all();
            return c;

        }

        //GET ewt Slug
        [HttpGet("{slug}")]
        public IActionResult getSlug(string slug)
        {

            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

       

        [HttpPost]
        public IActionResult create([FromBody] acctngEntity b)
        {

            _Repository.add(b);

            logger("Insert new accounting entity " + b.name);

            return Ok(new
            {
                type = "success",
                message = "Item created successfully.",
                slug = b.slug
            });

        }

      
        [HttpPut("{slug}")]
        public IActionResult update(string slug, [FromBody] acctngEntity b)
        {
            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _Repository.update(slug, b);

            logger("Modified accounting entity " + b.name);

            return Ok(new
            {
                type = "success",
                message =  b.name + " updated successfully."
            });
        }

        

        [HttpDelete("{slug}")]
        public IActionResult delete(string slug)
        {

            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.remove(slug);

            logger("Delete accounting entity " + item.name);

            return Ok(new
            {
                type = "success",
                message = item.name + " deleted successfully."
            });
        }
        
    }
}
