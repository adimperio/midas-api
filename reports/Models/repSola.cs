﻿using kmbi_core_master.coreMaster.Models;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.reports.Models
{

    public class repSola
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string id { get; set; }
        public String referenceId { get; set; }
        public Int32 centerLoanCycle { get; set; }
        public String centerSlug {get;set;}

        public List<centerInstance> center { get; set; }
        
        public List<loanMembersExt> members { get; set; }

        public String preparedBy {get;set;}
        public String checkedBy {get;set;}
        public String approvedBy {get;set;}
        public DateTime disbursementDate {get;set;}
    }

    public class loanMembersExt : loanMembers
    {
        public Double prevLoan {get;set;}
    }

    

}
