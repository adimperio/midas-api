﻿using System;

namespace kmbi_core_master.coreMaster.Models
{
    public class signatoriesSettings
    {
        public list[] list {get;set;}
    }

    public class list {
        public string report {get;set;}
        public string preparedBy {get;set;}
        public string checkedBy {get;set;}
        public string approvedBy {get;set;}
        public string notedBy {get;set;}
    }

}
