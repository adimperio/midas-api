﻿using kmbi_core_master.coreMaster.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.coreMaster.IRepository
{
    public interface iactivityRepository
    {
        void log(activity act);

        IEnumerable<activity> search(string search);

        IEnumerable<activity> searchId(string Id, DateTime fromdate, DateTime todate);

        IEnumerable<activity> searchDate(DateTime fromdate, DateTime todate);
    }
}
