using System;

namespace UseCases.GenerateLeavesForPayroll {
    public class GenerateLeavesForPayrollUseCaseRequest {
        public DateTime From {get;set;}
        public DateTime To {get;set;}
    }
}