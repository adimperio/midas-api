using System;
using kmbi_core_master.coreMaster.Models;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;


namespace kmbi_core_master.reports.Models
{

    public class repAlphalistReportModel {
        public Int32 year {get;set;}
        public String position {get;set;}
        public List<baseYearlyData> gross {get;set;}
        public List<baseYearlyData> taxWithheld {get;set;}
        public List<baseYearlyData> sss {get;set;}
        public List<baseYearlyData> philHealth {get;set;}
        public List<baseYearlyData> pagibig {get;set;}
        public List<baseYearlyData> rice {get;set;}
        public List<baseYearlyData> hi {get;set;}
        public List<benefits> benefits {get;set;}
        public List<baseAlphalist> alphalist {get;set;}
    }

    public class baseYearlyData {
        public String code {get;set;}
        public String name {get;set;}
        public String empType {get;set;}
        public String status {get;set;}
        public Double jan {get;set;}
        public Double feb {get;set;}
        public Double mar {get;set;}
        public Double apr {get;set;}
        public Double may {get;set;}
        public Double jun {get;set;}
        public Double jul {get;set;}
        public Double aug {get;set;}
        public Double sep {get;set;}
        public Double oct {get;set;}
        public Double nov {get;set;}
        public Double dec {get;set;}
        public Double total {get;set;}
    }

    public class benefits {
        public Double mealAllowances {get;set;}
        public Double thirteenthMonthMay {get;set;}
        public Double thirteenthMonth {get;set;}
        public Double thirteenthMonthAdj {get;set;}
        public Double decBonus {get;set;}
        public Double taxOnBonus {get;set;}
        public Double serviceAwards {get;set;}
        public Double totalTaxable {get;set;}
        public Double holidayMeals {get;set;}
        public Double anniversaryGift {get;set;}
        public Double vlConversion {get;set;}
        public Double uniform {get;set;}
        public Double laundryAllowance {get;set;}
        public Double riceSubsidy {get;set;}
        public Double healthInsurance {get;set;}
        public Double mealOvertime {get;set;}
        public Double totalNonTaxable {get;set;}
    }

    public class baseAlphalist {
        public String code {get;set;}
        public String name {get;set;}
        public String empType {get;set;}
        public String status {get;set;}
        public Double columnI {get;set;}
        public Double columnJ {get;set;}
        public Double columnK {get;set;}
        public Double columnL {get;set;}
        public Double columnM {get;set;}
        public Double columnN {get;set;}
        public Double columnO {get;set;}
        public Double columnP {get;set;}
        public Double columnQ {get;set;}
        public Double columnR {get;set;}
        public Double columnS {get;set;}
        public Double columnT {get;set;}
        public Double columnU {get;set;}
        public Double columnV {get;set;}
        public Double columnW {get;set;}
        public Double columnX {get;set;}
        public Double columnY {get;set;}
        public Double columnZ {get;set;}
        public Double columnAA {get;set;}
    }
}