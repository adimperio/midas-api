using System;

namespace kmbi_core_master.reports.Models {
    public class repEcrBatching {
        public string centerDescription {get;set;}
        public string centerAddress {get;set;}
        public string assignedPO {get;set;}
        public string paymentSchedule {get;set;}
        public double interestRate {get;set;}
        public string interestType {get;set;}
        public repEcrBatchInfo[] batchInfo {get;set;}
        public repEcrBatchingItem[] items {get;set;}
        public repEcrBatchingItemLines total {get;set;}
        public string preparedBy {get;set;}
        public string checkedBy {get;set;}

        public repEcrBatchingItemLines[] asWholeItems {get;set;}        
    }

    public class repEcrBatchInfo {
        public DateTime? loanDate {get; set;}
        public DateTime? maturityDate {get; set;}
        public Double loanAmount {get; set;}
        public string referenceNo {get; set;}
        public Int32 cycleNo {get; set;}
        public Int32 weekNo {get;set;}
    }
    public class repEcrBatchingItem {
        public int cycleNo {get;set;}
        public DateTime loanDate {get;set;}
        public DateTime maturityDate {get;set;}
        public double loanAmount {get;set;}
        public double interestRate {get;set;}
        public String interestType {get;set;}
        public String referenceNo {get;set;}
        public int weekNo {get;set;}
        public DateTime paymentSchedule {get;set;}
        public repEcrBatchingItemLines[] lines {get;set;}
        public repEcrBatchingItemLines total {get;set;}
    }

    public class repEcrBatchingItemLines {
        public int counter {get;set;}
        public string name {get;set;}
        public Int32 weekNo {get;set;}
        public repEcrBatchingItemBreakdown balances {get;set;}
        public repEcrBatchingItemBreakdown currentDueAmount {get;set;}
        public repEcrBatchingItemBreakdown pastDueAmount {get;set;}
        public repEcrBatchingItemBreakdown totalAmountDue {get;set;}
        public string lagda {get;set;}

        public Double loanCycle {get;set;}
    }

    public class repEcrBatchingItemBreakdown {
        public double principal {get;set;}
        public double interest {get;set;}
        public double cbu {get;set;}
        public double total {
            get {
                return this.principal + this.interest + this.cbu;
            }
        }
        
    }
}