﻿using kmbi_core_master.hrModule.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.hrModule.Interface
{
    public interface iotLogsRepository
    {
        List<otLog> getOtLogPerEmployeeCodePerDateRange(String employeeCode, DateTime from, DateTime to);
        void createOtLog(otLog otLog);
        void updateOtLog(String slug, otLog otLog);
        void deleteOtLog(String slug);
        void replaceOtLogPerEmployeeCodePerDate(String employeeCode, DateTime date, otLog otLog);
    }
}
