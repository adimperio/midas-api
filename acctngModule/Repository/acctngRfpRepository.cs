﻿using Microsoft.Extensions.Options;

using System;
using System.Collections.Generic;
using System.Linq;

using MongoDB.Bson;
using MongoDB.Driver;

using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.acctngModule.Models;
using kmbi_core_master.acctngModule.Interface;

namespace kmbi_core_master.acctngModule.Repository
{
    public class acctngRfpRepository : dbCon, iacctngRfpRepository
    {
        
        public acctngRfpRepository(IOptions<dbSettings> settings) : base(settings) {}

        public void add(acctngRfp b)
        {

            b.timestamps.createdAt = DateTime.Now;
            b.timestamps.updatedAt = DateTime.Now;
            for (int i = 0; i <= b.accounts.credit.Count() - 1; i++)
            {
                b.accounts.credit[i].Id = ObjectId.GenerateNewId().ToString();
                b.accounts.debit[i].Id = ObjectId.GenerateNewId().ToString();
            }
            var coll = _db.GetCollection<acctngRfp>("acctngRfp");
            _db.GetCollection<acctngRfp>("acctngRfp").InsertOneAsync(b);
        }
        

        public IEnumerable<acctngRfp> all()
        {
            var c = _db.GetCollection<acctngRfp>("acctngRfp").Find(new BsonDocument()).ToListAsync();
            return c.Result;
        }

        public acctngRfp getSlug(String slug)
        {
            var query = Builders<acctngRfp>.Filter.Eq(e => e.slug, slug);
            var c = _db.GetCollection<acctngRfp>("acctngRfp").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }
       
        public void update(String slug, acctngRfp p)
        {
            p.slug = slug;
            var query = Builders<acctngRfp>.Filter.Eq(e => e.slug, slug);
            var update = Builders<acctngRfp>.Update
                .Set("number", p.number)
                .Set("particulars", p.particulars)
                .Set("accounts", p.accounts)
                .Set("payee", p.payee)
                .Set("attachments", p.attachments)
                .Set("amount", p.amount)
                .Set("remarks", p.remarks)
                .Set("timestamps.issuedAt", p.timestamps.issuedAt)
                .Set("timestamps.issuedBy", p.timestamps.issuedBy)
                .Set("timestamps.createdBy", p.timestamps.createdBy)
                .Set("timestamps.updatedBy", p.timestamps.updatedBy)
                .CurrentDate("timestamps.updatedAt");

            var branch = _db.GetCollection<acctngRfp>("acctngRfp").UpdateOneAsync(query, update);

        }
        public void remove(String slug)
        {
            var query = Builders<acctngRfp>.Filter.Eq(e => e.slug, slug);
            var result = _db.GetCollection<acctngRfp>("acctngRfp").DeleteOneAsync(query);
        }

        
        //CREDIT
        public rfpAccounts getCreditId(string creditId)
        {

            var query = Builders<rfpAccounts>.Filter.ElemMatch(x => x.credit, x => x.Id == creditId);
            var fields = Builders<rfpAccounts>.Projection
                .Include(u => u.credit)
                .ElemMatch(x => x.credit, x => x.Id == creditId);

            var unit = _db.GetCollection<rfpAccounts>("acctngRfp").Find(query).Project<rfpAccounts>(fields).ToListAsync();
            return unit.Result.FirstOrDefault();
        }

        public void addCredit(rfpCredit u, string rfpSlug)
        {
            var query = Builders<acctngRfp>.Filter.Eq(e => e.slug, rfpSlug);
            var update = Builders<acctngRfp>.Update.Push(e => e.accounts.credit, u);
            u.Id = ObjectId.GenerateNewId().ToString();
            _db.GetCollection<acctngRfp>("acctngRfp").FindOneAndUpdateAsync(query, update);
        }

        public void updateCredit(string rfpSlug, string creditId, rfpCredit u)
        {

            var query = Builders<acctngRfp>.Filter.And(Builders<acctngRfp>.Filter.Eq(x => x.slug, rfpSlug),
            Builders<acctngRfp>.Filter.ElemMatch(x => x.accounts.credit, x => x.Id == creditId));

            var update = Builders<acctngRfp>.Update
                .Set("credit.$.name", u.name)
                .Set("credit.$.amount", u.amount);
            var result = _db.GetCollection<acctngRfp>("acctngRfp").UpdateOneAsync(query, update);
        }

        public void removeCredit(String rfpSlug, string creditId)
        {
            var pull = Builders<acctngRfp>.Update.PullFilter(x => x.accounts.credit, a => a.Id == creditId);

            var filter1 = Builders<acctngRfp>.Filter.And(Builders<acctngRfp>.Filter.Eq(a => a.slug, rfpSlug),
                          Builders<acctngRfp>.Filter.ElemMatch(q => q.accounts.credit, t => t.Id == creditId));

            var result = _db.GetCollection<acctngRfp>("acctngRfp").UpdateOneAsync(filter1, pull);
        }


        //DEBIT
        public acctngRfp getDebitId(string debitId)
        {

            var query = Builders<acctngRfp>.Filter.ElemMatch(x => x.accounts.debit, x => x.Id == debitId);
            var fields = Builders<acctngRfp>.Projection
                .Include(u => u.Id)
                .Include(u => u.slug)
                .ElemMatch(x => x.accounts.debit, x => x.Id == debitId);

            var unit = _db.GetCollection<acctngRfp>("acctngRfp").Find(query).Project<acctngRfp>(fields).ToListAsync();
            return unit.Result.FirstOrDefault();
        }

        public void addDebit(rfpDebit u, string rfpSlug)
        {
            var query = Builders<acctngRfp>.Filter.Eq(e => e.slug, rfpSlug);
            var update = Builders<acctngRfp>.Update.Push(e => e.accounts.debit, u);
            u.Id = ObjectId.GenerateNewId().ToString();
            _db.GetCollection<acctngRfp>("acctngRfp").FindOneAndUpdateAsync(query, update);
        }

        public void updateDebit(string rfpSlug, string debitId, rfpDebit u)
        {

            var query = Builders<acctngRfp>.Filter.And(Builders<acctngRfp>.Filter.Eq(x => x.slug, rfpSlug),
            Builders<acctngRfp>.Filter.ElemMatch(x => x.accounts.debit, x => x.Id == debitId));

            var update = Builders<acctngRfp>.Update
                .Set("debit.$.name", u.name)
                .Set("debit.$.amount", u.amount);
            var result = _db.GetCollection<acctngRfp>("acctngRfp").UpdateOneAsync(query, update);
        }

        public void removeDebit(String rfpSlug, string debitId)
        {
            var pull = Builders<acctngRfp>.Update.PullFilter(x => x.accounts.debit, a => a.Id == debitId);

            var filter1 = Builders<acctngRfp>.Filter.And(Builders<acctngRfp>.Filter.Eq(a => a.slug, rfpSlug),
                          Builders<acctngRfp>.Filter.ElemMatch(q => q.accounts.debit, t => t.Id == debitId));

            var result = _db.GetCollection<acctngRfp>("acctngRfp").UpdateOneAsync(filter1, pull);
        }
    }
}
