﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Cors;
using System;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Filters;
using System.Dynamic;
using System.Collections;
using kmbi_core_master.coreMaster.Models;
using System.Net.Http;
using System.Text;
using System.Net;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace kmbi_core_master.coreMaster.Controllers
{
    [usersAuth]
    [EnableCors("allowCors")]
    [Route("api/pais")]
    // [usersAuth]
    public class paisController : Controller
    {
        private readonly ibranchRepository _b;
        private readonly iloanRepository _l;
        private readonly iclientsRepository _c;

        public paisController(ibranchRepository b, iloanRepository l, iclientsRepository c)
        {
            _b = b;
            _l = l;
            _c = c;
        }
        
        public class postObject {
            public DateTime lastSyncAt {get;set;}
        }

        [HttpPost("sync/{branchSlug}")]
        public IActionResult sync(String branchSlug, [FromBody]postObject postObject)
        {
            branch branch = null;
            // validation
            if(postObject!=null) {
                // check if branch is existing
                branch = _b.getBranchSlug(branchSlug);
                if(branch==null) {
                    return BadRequest(new {
                        type = "failed",
                        message = "branch not exists"
                    });
                }
                
                // check if lastSync is valid date
                if(postObject.lastSyncAt ==null) {
                    return BadRequest(new {
                        type = "failed",
                        message = "lastsync date is invalid"
                    });
                }
            } else {
                return BadRequest(new {
                    type = "failed",
                    message = "invalid input"
                });
            }

            
            // list all loans
            List<loans> loanList = _l.allLoans().ToList();
            // create array variable of clientId
            List<String> clientIdList = new List<String>();

            
            
            // get paisLastSyncedAt of selected branch
            DateTime paisLastSyncedAt = Convert.ToDateTime("01/01/0001");
            // if null, set lastSynced as oldest date
            if(branch.paisLastSyncAt!=null)
                paisLastSyncedAt = Convert.ToDateTime(branch.paisLastSyncAt);

            // loop through all loans of current branch
            loanList.Where(l=> l.branchSlug == branch.slug).ToList().ForEach(l=>{
                // get disbursement date from loan
                DateTime? disbursementDate = l.disbursementDate;
                // do not add if null
                if(disbursementDate!=null) {
                    if(disbursementDate>paisLastSyncedAt) {
                        l.members.ToList().ForEach(m=>{
                            clientIdList.Add(m.memberId);
                        });
                    }
                }
            });

            // distinct client
            List<String> distinctClientIdList = clientIdList.Distinct().ToList();
            List<dynamic> cs = clients();
            List<dynamic> filteredCs = new List<dynamic>();
            distinctClientIdList.ForEach(c => {
                cs.ForEach(_c => {
                    //&& _c.fullName == "GEMMA SINGSON BULTRON"
                    if(_c._id == c  ) {
                        filteredCs.Add(_c);
                    }
                });
            });
            
            // pass to pais
            
            HttpClient client = new HttpClient();
            
            // get existing pais clients
            HttpResponseMessage getResponse = client.GetAsync("http://dev.pais.com.ph/publications/KMBIEnrollment/").Result;
            dynamic item = Newtonsoft.Json.JsonConvert.DeserializeObject(getResponse.Content.ReadAsStringAsync().Result);
            List<String> existingIds = new List<String>();
            if(item.enrollments!=null) {
                if(item.enrollments.Count > 0) {
                    for(Int32 ctr=0;ctr<=item.enrollments.Count - 1;ctr++) {
                        existingIds.Add(item.enrollments[ctr]._id.Value);
                    }
                }
            }

            List<dynamic> errorReponses = new List<dynamic>();
            List<dynamic> successReponses = new List<dynamic>();

            if(filteredCs.Count > 0) {
                // var get = filteredCs[0];
                // filteredCs = new List<dynamic>();
                // filteredCs.Add(get);
                var operation = "";
                filteredCs.ForEach(f=> {
                    var content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(f), Encoding.UTF8, "application/json");
                    HttpResponseMessage response = new HttpResponseMessage();
                    if(!existingIds.Contains(f._id)) {
                        // create
                        response = client.PostAsync("http://dev.pais.com.ph/kmbi/enrollments/",content).Result;
                        operation = "create";
                    } else {
                        // update
                        response = client.PutAsync("http://dev.pais.com.ph/kmbi/enrollments/" + f._id,content).Result;
                        operation = "update";
                    }
                    
                    if(response.StatusCode == HttpStatusCode.InternalServerError || response.StatusCode == HttpStatusCode.BadRequest) {
                        errorReponses.Add(new {
                                    operation = operation,
                                    clientFullname= f.fullName,
                                    // response = response.ReasonPhrase
                                    response= f
                                });
                    } else {
                        try {
                            successReponses.Add(new {
                                    operation = operation,
                                    clientFullname= f.fullName
                                    // response = Newtonsoft.Json.JsonConvert.DeserializeObject(response.Content.ReadAsStringAsync().Result)
                                }
                            );
                        } catch(Exception ex) {
                            errorReponses.Add(new {
                                    operation = operation,
                                    clientFullname= f.fullName,
                                    response = ex.Message
                                });
                        }
                    }
                });
            } else {
                successReponses.Add(new {
                    type = "success",
                    message = "No clients to sync"
                });
            }

            // update paisLastSyncAt of branches
            branch.paisLastSyncAt = postObject.lastSyncAt;
            _b.updateBranch(branchSlug, branch);

            return Ok(new {
                type = "success",
                message = "Syncing of clients successfull. Last successfull sync at " + postObject.lastSyncAt.ToString("MM/dd/yyyy hh:mm:ss tt"),
                errorResponses = errorReponses,
                successReponses = successReponses
            });

        }

        [HttpDelete("deleteAll")]
        public IActionResult deleteAll()
        {
            HttpClient client = new HttpClient();
            HttpResponseMessage response = client.GetAsync("http://dev.pais.com.ph/publications/KMBIEnrollment/").Result;
            dynamic item = Newtonsoft.Json.JsonConvert.DeserializeObject(response.Content.ReadAsStringAsync().Result);
            List<String> ids = new List<String>();
            for(Int32 ctr=0;ctr<=item.enrollments.Count - 1;ctr++) {
                ids.Add(item.enrollments[ctr]._id.Value);

                var a = client.DeleteAsync("http://dev.pais.com.ph/kmbi/enrollments/" + item.enrollments[ctr]._id.Value).Result;
                
            }
            return Ok(ids);
        }

        private List<dynamic> clients() {
            var systemId = "bQzkwdh2nHxkneAHs";
            var versionToken = "f53060ad634552346af56478f466cff7";
            var productName = "mtxvJ7ubFiJCLtKvR";

            var clients = _c.allClients();
            var loans = _l.allLoans();
            var list = from l in loans 
                        from m in l.members
                        join c in clients on m.memberId equals c.Id
                        select new {
                            centerName = l.centerName, 
                            disbursementDate = l.disbursementDate, 
                            loanCycle = l.centerLoanCycle,
                            member= m, 
                            client = c
                        };
            
            
            List<dynamic> enrollments = new List<dynamic>();;
            list.ToList().ForEach((l) => {
                dynamic obj = new ExpandoObject();
                obj.systemId = systemId;
                obj.versionToken = versionToken;
                obj._id = l.member.memberId;
                obj.centerNumber = l.centerName;
                obj.fullName = l.member.fullname;
                var id = l.client.ids.FindAll(i=>i.type.ToLower() == "tin").FirstOrDefault();
                obj.tin = id != null ? id.number : "";
                obj.birthDate = l.member.birthdate.ToString("yyyy-MM-dd");
                obj.ageOfEnrollee = computeAge(l.member.birthdate);
                var contact = l.client.contacts.FirstOrDefault();
                obj.phone = contact != null ? contact.number : "";
                var address = l.client.addresses.FirstOrDefault();
                obj.address = address != null ? address.full : "";
                obj.city = address != null ? address.municipality.name : "";
                obj.gender = l.client.gender.ToUpper();
                String _marstas = l.client.civilStatus.ToUpper();

                switch(_marstas.Trim()) {
                    case "Divorced/Separated":
                        _marstas = "SEPARATED";
                        break;
                    default:
                        _marstas = "SINGLE";
                        break;
                }
                obj.maritalStatus = _marstas;
                obj.beneficiaryOne = l.client.beneficiary.last + ", " + l.client.beneficiary.first + " " + l.client.beneficiary.middle;

                if(l.client.beneficiary.birthdate != null) {
                    obj.beneficiaryOneBirthdate = Convert.ToDateTime(l.client.beneficiary.birthdate).ToString("yyyy-MM-dd");
                }
                
                var _rela = l.client.beneficiary.relationship.Substring(0,1).ToUpper() +  l.client.beneficiary.relationship.Substring(1);
                switch(_rela.Trim()) {
                    case "Child":
                        _rela = "Daughter";
                        break;
                    case "Common law partner":
                        _rela = "Live-In Partner";
                        break;
                    case "Grand mother":
                        _rela = "Grandparents";
                        break;
                    case "Live in partner":
                        _rela = "Live=In Partner";
                        break;
                    case "Husband":
                        _rela = "Spouse";
                        break;
                    case "Sibling":
                        if (l.client.gender == "MALE")
                            _rela = "Brother";
                        else
                            _rela = "Sister";
                        break;
                    case "-":
                        _rela = "Spouse";
                        break;
                    default:
                        break;
                }
                obj.beneficiaryOneRelationship = _rela;
                obj.beneficiaryOneAge = computeAge(Convert.ToDateTime(l.client.beneficiary.birthdate));
                obj.beneficiaryOneMaritalStatus = "SINGLE";
                obj.beneficiaryOneGender = "FEMALE";
                var children = l.client.dependents.ToList().FindAll(d=>d.relationship.ToLower()=="child");
                var counter = 0;
                if(children!=null) {
                    counter=0;
                    if(children.Count > 0) {
                        obj.children = convertNumberToWord(children.Count) + ((children.Count > 0 && children.Count == 1) ? " Child" : " Children");
                    }
                    children.ForEach((c) => {
                        ((IDictionary<String, object>)obj)["children" + convertNumberToWord(counter) + "Name"] = c.last + ", " + c.first + " " + c.middle;
                        if(c.birthdate!=null) {
                            ((IDictionary<String, object>)obj)["children" + convertNumberToWord(counter) + "Birthdate"] = Convert.ToDateTime(c.birthdate).ToString("yyyy-MM-dd");
                            ((IDictionary<String, object>)obj)["children" + convertNumberToWord(counter) + "Age"] = computeAge(Convert.ToDateTime(c.birthdate));
                        }
                        counter++;
                    });
                }
                var parent = l.client.dependents.ToList().FindAll(d=>d.relationship.ToLower()=="mother" || d.relationship.ToLower()=="father");
                if(parent!=null) {
                    counter=0;
                    if(parent.Count > 0) {
                        obj.parent = convertNumberToWord(parent.Count) + " Parent";
                    }
                    parent.ForEach((p) => {
                        ((IDictionary<String, object>)obj)["parent" + convertNumberToWord(counter) + "Name"] = p.last + ", " + p.first + " " + p.middle;
                        if(p.birthdate!=null) {
                            ((IDictionary<String, object>)obj)["parent" + convertNumberToWord(counter) + "Birthdate"] = Convert.ToDateTime(p.birthdate).ToString("yyyy-MM-dd");
                            ((IDictionary<String, object>)obj)["parent" + convertNumberToWord(counter) + "Age"] = computeAge(Convert.ToDateTime(p.birthdate));
                        }
                        ((IDictionary<String, object>)obj)["parent" + convertNumberToWord(counter) + "MaritalStatus"] = "MARRIED";
                        ((IDictionary<String, object>)obj)["parent" + convertNumberToWord(counter) + "Gender"] = p.relationship.ToLower() == "mother" ? "FEMALE" : "MALE";
                        counter++;
                        counter++;
                    });
                }
                var sibling = l.client.dependents.ToList().FindAll(d=>d.relationship.ToLower()=="sibling");
                if(sibling!=null) {
                    counter=0;
                    if(sibling.Count > 0) {
                        obj.sibling = convertNumberToWord(sibling.Count) + " Sibling";
                    }
                    sibling.ForEach((p) => {
                        ((IDictionary<String, object>)obj)["sibling" + convertNumberToWord(counter) + "Name"] = p.last + ", " + p.first + " " + p.middle;
                            if(p.birthdate!=null) {
                            ((IDictionary<String, object>)obj)["sibling" + convertNumberToWord(counter) + "Birthdate"] = Convert.ToDateTime(p.birthdate).ToString("yyyy-MM-dd");
                            ((IDictionary<String, object>)obj)["sibling" + convertNumberToWord(counter) + "Age"] = computeAge(Convert.ToDateTime(p.birthdate));
                        }
                        ((IDictionary<String, object>)obj)["sibling" + convertNumberToWord(counter) + "MaritalStatus"] = "SINGLE";
                        ((IDictionary<String, object>)obj)["sibling" + convertNumberToWord(counter) + "Gender"] = "MALE";
                        counter++;
                        counter++;
                    });
                }
                
                obj.productsAvailed = "One Product";
                obj.productName = productName;

                obj.productOffering = "Principal";
                counter=1;
                children.ForEach((c) => {
                     ((IDictionary<String, object>)obj)["productOffering" + (counter).ToString()] = "Children";
                     counter++;
                });

                parent.ForEach((c) => {
                     ((IDictionary<String, object>)obj)["productOffering" + (counter).ToString()] = "Parent";
                     counter++;
                });

                sibling.ForEach((c) => {
                     ((IDictionary<String, object>)obj)["productOffering" + (counter).ToString()] = "Sibling";
                     counter++;
                });

                obj.totalPremium = null;
                obj.insuredName = l.member.fullname;
            
                counter = 1;
                children.ForEach((c) => {
                     ((IDictionary<String, object>)obj)["insured" + (convertNumberToWord(counter + 1)).ToString() + "Name"] = c.first + ", " + c.middle + " " + c.last;
                     counter++;
                });

                parent.ForEach((c) => {
                     ((IDictionary<String, object>)obj)["insured" + (convertNumberToWord(counter + 1)).ToString() + "Name"] = c.first + ", " + c.middle + " " + c.last;
                     counter++;
                });

                sibling.ForEach((c) => {
                     ((IDictionary<String, object>)obj)["insured" + (convertNumberToWord(counter + 1)).ToString() + "Name"] = c.first + ", " + c.middle + " " + c.last;
                     counter++;
                });

                obj.effectivityDate = Convert.ToDateTime(l.disbursementDate).ToString("yyyy-MM-dd");
                obj.maturityDate = Convert.ToDateTime(l.disbursementDate).AddMonths(6).ToString("yyyy-MM-dd");
                obj.loanCycle = l.loanCycle;
                obj.createdAt = null;
                obj.company = systemId;
                obj.branch = ""; // pais id
                obj.insurer = new List<String>{
                };
                obj.products = new List<String>{
                    productName
                };
                obj.totalPremiums = null;
                obj.effectiveDates = new List<String>();
                obj.createdBy = null;
                obj.dateEnrolled = Convert.ToDateTime(l.client.createdAt).ToString("yyyy-MM-dd");
                obj.premium = 35;
                obj.productRange = 6;
                enrollments.Add(obj);
            });
                
            // return Ok(new {enrollments = enrollments});
            return enrollments;
        } 

        private String convertNumberToWord(Int32 number) {
            String word = null;

            switch (number) {
                case 0:
                    word = "";
                    break;
                case 1:
                    word ="One";
                    break;
                case 2:
                    word ="Two";
                    break;
                case 3:
                    word ="Three";
                    break;
                case 4:
                    word ="Four";
                    break;
                case 5:
                    word ="Five";
                    break;
                case 6:
                    word ="Six";
                    break;
                case 7:
                    word ="Seven";
                    break;
                case 8:
                    word ="Eight";
                    break;
                case 9:
                    word ="Nine";
                    break;
                case 10:
                    word ="Ten";
                    break;
                default :
                    break;
            }

            return word;
        }

        private Int32 computeAge(DateTime birthdate) {
            return (int.Parse(DateTime.Now.ToString("yyyyMMdd")) - int.Parse(birthdate.ToString("yyyyMMdd"))) / 10000;
        }
    }

    
}
