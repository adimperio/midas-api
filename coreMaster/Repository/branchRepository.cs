﻿using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Models;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace kmbi_core_master.coreMaster.Repository
{
    public class branchRepository : dbCon, ibranchRepository
    {
        public branchRepository(IOptions<dbSettings> settings) : base(settings)
        {
        }

        public void addBranch(branch b)
        {

            b.createdAt = DateTime.Now;
            b.updatedAt = DateTime.Now;
            for (int i = 0; i <= b.units.Count - 1; i++)
            {
                b.units[i].Id = ObjectId.GenerateNewId().ToString();
                b.units[i].createdAt = DateTime.Now;
                b.units[i].updatedAt = DateTime.Now;

                //for (int s = 0; s <= b.units[i].officers.Count - 1; s++)
                //{
                //    b.units[i].officers[s].Id = ObjectId.GenerateNewId().ToString();
                //    b.units[i].officers[s].createdAt = DateTime.Now;
                //    b.units[i].officers[s].updatedAt = DateTime.Now;

                //    for (int r = 0; r <= b.units[i].officers[s].centers.Count - 1; r++)
                //    {
                //        b.units[i].officers[s].centers[r].Id = ObjectId.GenerateNewId().ToString();
                //        b.units[i].officers[s].centers[r].createdAt = DateTime.Now;
                //        b.units[i].officers[s].centers[r].updatedAt = DateTime.Now;
                //    }
                //}
            }

            if(b.holidays!=null)
            {
                foreach(documentationModule.Models.holiday hol in b.holidays)
                {
                    hol.Id = ObjectId.GenerateNewId().ToString();
                    hol.createdAt = DateTime.Now;
                    hol.updatedAt = DateTime.Now;
                }
            }
            if(b.banks!=null)
            {
                for (int i = 0; i <= b.banks.Count - 1; i++)
                {
                    b.banks[i].Id = ObjectId.GenerateNewId().ToString();
                    b.banks[i].createdAt = DateTime.Now;
                    b.banks[i].updatedAt = DateTime.Now;
                }
            } else {
                b.banks =new List<banks>();
            }
            _db.GetCollection<branch>("branch").InsertOneAsync(b);
        }

        public void addUnits(units u, string branchSlug)
        {
            var query = Builders<branch>.Filter.Eq(e => e.slug, branchSlug);
            var update = Builders<branch>.Update.Push(e => e.units, u);
            u.Id = ObjectId.GenerateNewId().ToString();
            u.createdAt = DateTime.Now;
            u.updatedAt = DateTime.Now;
            _db.GetCollection<branch>("branch").FindOneAndUpdateAsync(query, update);
        }

        public void addBanks(banks u, string branchSlug)
        {
            var query = Builders<branch>.Filter.Eq(e => e.slug, branchSlug);
            var update = Builders<branch>.Update.Push(e => e.banks, u);
            u.Id = ObjectId.GenerateNewId().ToString();
            u.createdAt = DateTime.Now;
            u.updatedAt = DateTime.Now;
            _db.GetCollection<branch>("branch").FindOneAndUpdateAsync(query, update);
        }

        public IEnumerable<branch> allBranch()
        {
            var branch = _db.GetCollection<branch>("branch").Find(new BsonDocument()).ToListAsync();
            return branch.Result;
        }

        public unitList getUnitSlug(string unitSlug)
        {

            var query = Builders<unitList>.Filter.ElemMatch(x => x.units, x => x.slug == unitSlug);
            var fields = Builders<unitList>.Projection
                .Include(u => u.slug)
                .Include(u => u.code)
                .Include(u => u.name)
                .ElemMatch(x => x.units, x => x.slug == unitSlug);

            var unit = _db.GetCollection<unitList>("branch").Find(query).Project<unitList>(fields).ToListAsync();
            return unit.Result.FirstOrDefault();

            //var coll = _db.GetCollection<branch>("branch");
            //var unit = coll.Aggregate()
            //    .Match(x => x.slug == branchSlug)
            //    .Unwind<branch, unitListO>(x => x.units)
            //    .Lookup(new BsonDocument {
            //        {
            //            "from:"
            //        } })
            //    .Project<unitListO>(new BsonDocument
            //            {
            //                    {"slug", 1},
            //                    {"units.slug", 1},
            //                    {"units.officers.name",1},
            //                    {"units.officers.designation",1},
            //                    {"units.officers.slug",1},
            //                    {"units.officers.centers",1}
            //            })

            //.ToListAsync();

            //return unit.Result.FirstOrDefault();
        }

        public banksList getBankSlug(string bankSlug)
        {

            var query = Builders<banksList>.Filter.ElemMatch(x => x.banks, x => x.slug == bankSlug);
            var fields = Builders<banksList>.Projection
                .Include(u => u.slug)
                .Include(u => u.code)
                .ElemMatch(x => x.banks, x => x.slug == bankSlug);

            var bank = _db.GetCollection<banksList>("branch").Find(query).Project<banksList>(fields).ToListAsync();
            return bank.Result.FirstOrDefault();
            
        }

        public branch getBranchSlug(String slug)
        {
            var query = Builders<branch>.Filter.Eq(e => e.slug, slug);
            var branch = _db.GetCollection<branch>("branch").Find(query).ToListAsync();

            return branch.Result.FirstOrDefault();
        }

        //public unwindedUnits getOfficerSlug(string branchSlug, string unitSlug, string officerSlug)
        //{

            //var a = " [{ '$match':{ 'slug':'westSlug'} },";
            //var b = " { '$unwind':'$units'},";
            //var c = " { '$match':{ 'units.slug':'unitSlug'} },";
            //var d = " { '$unwind':'$units.officers'},";
            //var e = " { '$match':{ 'units.officers.slug':'officerSlug'} },";
            //var f = " { '$project':{ 'units._id' : 1, 'units.officers.name':1} }]";
            ////var q = new BsonDocument[] { new BsonDocument { { "slug", "westSlug" } } };
            //var bQuery = a + b + c + d + e + f;
            //var filter = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(bQuery);
            //var result = _db.GetCollection<branch>("branch").FindSync(filter).ToList();
            ////var agg = _db.GetCollection<branch>("branch").Aggregate(q);
            //return result.FirstOrDefault();

        //    var coll = _db.GetCollection<branch>("branch");
        //    var unit = coll.Aggregate()
        //        .Match(x => x.slug == branchSlug)
        //        .Unwind<branch, unwindedUnits>(x => x.units)
        //        .Match(x => x.units.slug == unitSlug)
        //        //.Match(new BsonDocument { { "units.slug", "unitSlug" } })
        //        .Unwind<unwindedUnits, unwindedOfficers>(x => x.units.officers)
        //        //.Match(x => x.officers.slug == officerSlug)
        //        .Match(new BsonDocument { { "units.officers.slug", officerSlug } })
        //        .Project<unwindedUnits>(new BsonDocument
        //                {
        //                    {"slug", 1},
        //                    {"units.slug", 1},
        //                    {"units.officers.name",1},
        //                    {"units.officers.designation",1},
        //                    {"units.officers.slug",1},
        //                    {"units.officers.centers",1}
        //                })
         
        //    .ToListAsync();

        //    return unit.Result.FirstOrDefault();


        //}

        public void updateBranch(String slug, branch p)
        {
            p.slug = slug;

            if(p.holidays!=null)
            {
                foreach(documentationModule.Models.holiday hol in p.holidays)
                {
                    hol.Id = ObjectId.GenerateNewId().ToString();
                    hol.updatedAt = DateTime.Now;
                }
            }

            var query = Builders<branch>.Filter.Eq(e => e.slug, slug);
            var banks = _db.GetCollection<branch>("branch").Find(query).FirstOrDefault().banks;
            var units = _db.GetCollection<branch>("branch").Find(query).FirstOrDefault().units;

            var update = Builders<branch>.Update
                .Set("name", p.name)
                .Set("address", p.address)
                .Set("code", p.code)
                .Set("regionId", p.regionId)
                .Set("areaId", p.areaId)
                .Set("head", p.head)
                .Set("ba", p.ba)
                .Set("baa", p.baa)
                .Set("tinNumber", p.tinNumber)
                //.Set("banks", p.banks)
                //.Set("units", p.units)
                // .Set("holidays", p.holidays)
                .Set("coordinates", p.coordinates)
                .Set("isOperational", p.isOperational)
                .Set("province", p.province)
                .Set("paisLastSyncAt",p.paisLastSyncAt )
                .Set("paisId",p.paisId)
                .CurrentDate("updatedAt");

            
            for(int i = 0; i <= p.banks.Count -1; i++)
            {
                var qBanks = Builders<branch>.Filter.And(query,Builders<branch>.Filter.ElemMatch(x => x.banks, x => x.Id == banks[i].Id));

                var qUpdate = Builders<branch>.Update
                    .Set("banks.$.name", p.banks[i].name)
                    .Set("banks.$.code", p.banks[i].code)
                    .Set("banks.$.updatedAt", DateTime.Now);

                var result = _db.GetCollection<branch>("branch").UpdateOneAsync(qBanks, qUpdate);
            }

            for (int i = 0; i <= p.units.Count - 1; i++)
            {
                var qUnits = Builders<branch>.Filter.And(query, Builders<branch>.Filter.ElemMatch(x => x.units, x => x.Id == units[i].Id));

                var quUpdate = Builders<branch>.Update
                   .Set("units.$.name", p.units[i].name)
                    .Set("units.$.code", p.units[i].code)
                    .Set("units.$.head", p.units[i].head);

                var result = _db.GetCollection<branch>("branch").UpdateOneAsync(qUnits, quUpdate);
            }

            var branch = _db.GetCollection<branch>("branch").UpdateOneAsync(query, update);
        }

        public void updateUnits(string branchSlug, string unitSlug, units u)
        {
           
            var query = Builders<branch>.Filter.And(Builders<branch>.Filter.Eq(x => x.slug, branchSlug),
            Builders<branch>.Filter.ElemMatch(x => x.units, x => x.slug == unitSlug));

            var update = Builders<branch>.Update
                .Set("units.$.name", u.name)
                .Set("units.$.code", u.code)
                .Set("units.$.head", u.head)
                .CurrentDate("updatedAt");

            var result = _db.GetCollection<branch>("branch").UpdateOneAsync(query, update);
        }

        public void updateBanks(string branchSlug, string bankSlug, banks u)
        {

            var query = Builders<branch>.Filter.And(Builders<branch>.Filter.Eq(x => x.slug, branchSlug),
            Builders<branch>.Filter.ElemMatch(x => x.banks, x => x.slug == bankSlug));

            var update = Builders<branch>.Update
                .Set("banks.$.name", u.name)
                .Set("banks.$.code", u.code)
                .Set("banks.$.updatedAt", DateTime.Now)
                .CurrentDate("updatedAt");

            var result = _db.GetCollection<branch>("branch").UpdateOneAsync(query, update);
        }
        //public void updateOfficer(string branchSlug, string unitSlug, string officerSlug, officers o)
        //{

        //    var q1 = Builders<branch>.Filter.Eq(x => x.slug, branchSlug);
        //    var q2 = Builders<branch>.Filter.ElemMatch(x => x.units, x => x.slug == unitSlug);
        //    var query = Builders<branch>.Filter.And(q1, q2);

        //    var update = Builders<branch>.Update
        //        .Set("units.$.officers.0.name", o.name)
        //        .Set("units.$.officers.0.updatedAt", DateTime.Now)
        //        .CurrentDate("updatedAt");

        //    var result = _db.GetCollection<branch>("branch").UpdateOneAsync(query, update);

        //}

        public void removeBranch(String slug)
        {
            var query = Builders<branch>.Filter.Eq(e => e.slug, slug);
            var result = _db.GetCollection<branch>("branch").DeleteOneAsync(query);
        }

        public void removeUnits(String branchSlug, string unitSlug)
        {

            var pull = Builders<branch>.Update.PullFilter(x => x.units, a => a.slug == unitSlug);

            var filter1 = Builders<branch>.Filter.And(Builders<branch>.Filter.Eq(a => a.slug, branchSlug),
                          Builders<branch>.Filter.ElemMatch(q => q.units, t => t.slug == unitSlug));

            var result = _db.GetCollection<branch>("branch").UpdateOneAsync(filter1, pull);
            // return getRegion(regSlug) == null;
            //return true;
        }

        public void removeBanks(String branchSlug, string bankSlug)
        {

            var pull = Builders<branch>.Update.PullFilter(x => x.banks, a => a.slug == bankSlug);

            var filter1 = Builders<branch>.Filter.And(Builders<branch>.Filter.Eq(a => a.slug, branchSlug),
                          Builders<branch>.Filter.ElemMatch(q => q.banks, t => t.slug == bankSlug));

            var result = _db.GetCollection<branch>("branch").UpdateOneAsync(filter1, pull);
            // return getRegion(regSlug) == null;
            //return true;
        }

        public IEnumerable<branch> searchBranch(String name)
        {

            var query = Builders<branch>.Filter.Regex("name", new BsonRegularExpression(name, options: "i"));
            var branch = _db.GetCollection<branch>("branch").Find(query).ToListAsync();

            return branch.Result;
        }

        public branch checkBranchSlug(String slug)
        {
            var query = Builders<branch>.Filter.Eq(e => e.slug, slug);
            var branch = _db.GetCollection<branch>("branch").Find(query).ToListAsync();

            return branch.Result.FirstOrDefault();
        }

        public branch checkBranchCode(String regionCode)
        {
            var query = Builders<branch>.Filter.Eq(e => e.code, regionCode);
            var branch = _db.GetCollection<branch>("branch").Find(query).ToListAsync();

            return branch.Result.FirstOrDefault();
        }

        public branch checkUnitCode(String unitCode)
        {
            var query = Builders<branch>.Filter.ElemMatch(e => e.units, g => g.code == unitCode);
            var branch = _db.GetCollection<branch>("branch").Find(query).ToListAsync();

            return branch.Result.FirstOrDefault();
        }

        public branch checkBankCode(String bankCode)
        {
            var query = Builders<branch>.Filter.ElemMatch(e => e.banks, g => g.code == bankCode);
            var branch = _db.GetCollection<branch>("branch").Find(query).ToListAsync();

            return branch.Result.FirstOrDefault();
        }

        public void updateLastSync(String slug)
        {
            var query = Builders<branch>.Filter.Eq(e => e.slug, slug);
            var update = Builders<branch>.Update
                .Set("lastSyncAt", DateTime.Now)
                .CurrentDate("updatedAt");

            var branch = _db.GetCollection<branch>("branch").UpdateOneAsync(query, update);
        }

        public void updatePaisLastSync(String slug)
        {
            var query = Builders<branch>.Filter.Eq(e => e.slug, slug);
            var update = Builders<branch>.Update
                .Set("paisLastSyncAt", DateTime.Now)
                .CurrentDate("updatedAt");

            var branch = _db.GetCollection<branch>("branch").UpdateOneAsync(query, update);
        }

        public void updateEmployees(branchEmployees p)
        {
            //update branch manager
            var query = Builders<branch>.Filter.Eq(e => e.slug, p.slug);
            var list = _db.GetCollection<branch>("branch").Find(query).FirstOrDefault();

            var updateBm = Builders<branch>.Update
                .Set("head", p.bmName)
                .Set("ba", p.baName)
                .Set("baa", p.baaName)
                .CurrentDate("updatedAt");
            var branchManager = _db.GetCollection<branch>("branch").UpdateOneAsync(query, updateBm);

           
            for (int i = 0; i <= p.units.Count - 1; i++)
            {
                var qPuh = Builders<branch>.Filter.ElemMatch(x => x.units, x => x.slug == p.units[i].slug);
                var q1 = Builders<branch>.Filter.And(query, qPuh);
                var listUnits = _db.GetCollection<branch>("branch").Find(q1).FirstOrDefault();
                var unitId = listUnits.units[i].Id;
                var unitSlug = listUnits.units[i].slug;
                // var unitId = listU

                var updatePuh = Builders<branch>.Update
                   .Set(e => e.units[i].head, p.units[i].puhName)
                   .CurrentDate("updatedAt");
                var branchPuh = _db.GetCollection<branch>("branch").UpdateOneAsync(q1, updatePuh);

                //UPDATE PO
                int z = 1;
                for (int r = 0; r <= p.units[i].poName.Count - 1; r++)
                {
                    var desig = "PO" + z;
                    var qPo = Builders<officers>.Filter.Eq(e => e.designation, desig);
                    var qUnitId = Builders<officers>.Filter.Eq(e => e.unitId, listUnits.units[i].Id);
                    var q2 = Builders<officers>.Filter.And(qPo, qUnitId);

                    var updatePo = Builders<officers>.Update
                    .Set(e => e.name, p.units[i].poName[r])
                    .CurrentDate("updatedAt");
                    var unitPo = _db.GetCollection<officers>("officers").UpdateOneAsync(q2, updatePo);
                    z += 1;
                }
            }

        }

        public branchEmployees getEmployees(String branchSlug) {
            // get branch
            var coll = _db.GetCollection<branch>("branch");
            var coll2 = _db.GetCollection<officers>("officers");
            var item = coll.Find(b=>b.slug == branchSlug).FirstOrDefault();
            var be = new branchEmployees();
            be.units = new List<beUnits>();
            if(item!=null) {
                be.slug = item.slug;
                be.bmName = item.head;
                be.baName = item.ba;
                be.baaName = item.baa;
                item.units.ToList().ForEach(u=>{
                    var item2 = coll2.Find(uu=>uu.unitId == u.Id).ToList();
                    
                    be.units.Add(new beUnits {
                        slug = u.slug,
                        puhName = u.head,
                        poName = (from x in item2 select x.name).ToList()
                    });
                });

                return be;
            } else {
                return null;
            }
        }

        public branchOfficers getBranchOfficers(String branchSlug)
        {
           
            // get branch
            var coll = _db.GetCollection<branch>("branch");
            var coll2 = _db.GetCollection<officers>("officers");
            var item = coll.Find(b => b.slug == branchSlug).FirstOrDefault();
            var be = new branchOfficers();
            be.units = new List<branchUnits>();
            if (item != null)
            {
                be.bm = item.head;
                be.ba = item.ba;
                be.baa = item.baa;

                foreach (var i in item.units)
                {
                    var qUnitId = Builders<officers>.Filter.Eq(e => e.unitId, i.Id);
                    var PO1 = "";
                    var PO2 = "";
                    var PO3 = "";
                    var PO4 = "";
                    var PO5 = "";
                    var PO6 = "";
                    var PO7 = "";

                    var qPo1 = Builders<officers>.Filter.Eq(e => e.designation, "PO1");
                    var q21 = Builders<officers>.Filter.And(qPo1, qUnitId);
                    var officer1 = _db.GetCollection<officers>("officers").Find(q21).FirstOrDefault();
                    if (officer1 == null) { PO1 = ""; }
                    else { PO1 = officer1.name; }
                    //}
                    //int x = 0;
                    var qPo2 = Builders<officers>.Filter.Eq(e => e.designation, "PO2");
                    var q22 = Builders<officers>.Filter.And(qPo2, qUnitId);
                    var officer2 = _db.GetCollection<officers>("officers").Find(q22).FirstOrDefault();
                    if (officer2 == null) { PO2 = ""; }
                    else { PO2 = officer2.name; }

                    var qPo3 = Builders<officers>.Filter.Eq(e => e.designation, "PO3");
                    var q23 = Builders<officers>.Filter.And(qPo3, qUnitId);
                    var officer3 = _db.GetCollection<officers>("officers").Find(q23).FirstOrDefault();
                    if (officer3 == null) { PO3 = ""; }
                    else { PO3 = officer3.name; }

                    var qPo4 = Builders<officers>.Filter.Eq(e => e.designation, "PO4");
                    var q24 = Builders<officers>.Filter.And(qPo4, qUnitId);
                    var officer4 = _db.GetCollection<officers>("officers").Find(q24).FirstOrDefault();
                    if (officer4 == null) { PO4 = ""; }
                    else { PO4 = officer4.name; }

                    var qPo5 = Builders<officers>.Filter.Eq(e => e.designation, "PO5");
                    var q25 = Builders<officers>.Filter.And(qPo5, qUnitId);
                    var officer5 = _db.GetCollection<officers>("officers").Find(q25).FirstOrDefault();
                    if (officer5 == null) { PO5 = ""; }
                     else { PO5 = officer5.name; }

                    var qPo6 = Builders<officers>.Filter.Eq(e => e.designation, "PO6");
                    var q26 = Builders<officers>.Filter.And(qPo6, qUnitId);
                    var officer6 = _db.GetCollection<officers>("officers").Find(q26).FirstOrDefault();
                    if (officer6 == null) { PO6 = ""; }
                    else { PO6 = officer6.name; }

                    var qPo7 = Builders<officers>.Filter.Eq(e => e.designation, "PO7");
                    var q27 = Builders<officers>.Filter.And(qPo7, qUnitId);
                    var officer7 = _db.GetCollection<officers>("officers").Find(q27).FirstOrDefault();
                    if (officer7 == null) { PO7 = ""; }
                    else { PO7 = officer7.name; }

                    be.units.Add(new branchUnits
                    {
                        puh = i.head,
                        po1 = PO1,
                        po2 = PO2,
                        po3 = PO3,
                        po4 = PO4,
                        po5 = PO5,
                        po6 = PO6,
                        po7 = PO7
                    });
                }
                
                return be;
            }
            else
            {
                return null;
            }
        }
    }
}
