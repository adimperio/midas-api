using System;

namespace UseCases.GenerateDtrForPayroll {
    public class GenerateDtrForPayrollUseCaseRequest {
        public DateTime From {get;set;}
        public DateTime To {get;set;}
    }
}