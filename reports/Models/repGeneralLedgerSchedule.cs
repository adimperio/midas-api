using System;
using System.Collections.Generic;

namespace kmbi_core_master.reports.Models
{
    public class repGeneralLedgerSchedule
    {
        public String accountNo {get;set;}
        public String accountName {get;set;}
        public DateTime from {get;set;}
        public DateTime to {get;set;}
        public List<repGeneralLedgerScheduleSubsidiary> subsidiary {get;set;}
    }

    public class repGeneralLedgerScheduleSubsidiary
    {
        public String particulars {get;set;}
        public Double previousEntryDebit {get;set;}
        public Double previousEntryCredit {get;set;}
        public Double previousEntry {get;set;}
        public List<repGeneralLedgerScheduleSubsidiaryPerMonth> details {get;set;}
        public Double balance {get;set;}
    }

    public class repGeneralLedgerScheduleSubsidiaryPerMonth
    {
        public DateTime month {get;set;}
        public List<repGeneralLedgerScheduleSubsidiaryDetails> details {get;set;}
        public Double balance {get;set;}
    }

    public class repGeneralLedgerScheduleSubsidiaryDetails
    {
        public DateTime date {get;set;}
        public String reference {get;set;}
        public String particulars {get;set;}
        public Double dr {get;set;}
        public Double cr {get;set;}
        public Double balance {get;set;}
    }
}