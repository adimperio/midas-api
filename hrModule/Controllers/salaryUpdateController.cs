﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Filters;
using Microsoft.AspNetCore.Cors;
using kmbi_core_master.documentationModule.iRepositories;
using kmbi_core_master.helpers.interfaces;
using kmbi_core_master.hrModule.Interface;
using kmbi_core_master.hrModule.Models;

namespace kmbi_core_master.coreMaster.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/salaryUpdate")]
    public class salaryUpdateController : Controller
    {
        private readonly isalaryUpdateRepository _Repository;
        private readonly iusersRepository _logInfo;
        private string token;

        public salaryUpdateController(isalaryUpdateRepository Repository, iusersRepository logInfo)
        {
            this._Repository = Repository;
            _logInfo = logInfo;
        }

        public void logger(string message)
        {
            //Insert Log//
            token = HttpContext.Request.Headers["token"];
            var lInfo = _logInfo.getLoginInfo(token);
            string log = message;
            global.createLog(lInfo.id, lInfo.fullname, lInfo.employment.branch, log);
            //Insert Log//
        }

        // GET: 
        [HttpGet]
        public List<salaryUpdate> GetAll()
        {
            var d = _Repository.getAll();
            return d;

        }

        //GET  Slug
        [HttpGet("{slug}")]
        public IActionResult getSlug(string slug)
        {

            var item = _Repository.getBySlug(slug);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        //GET  by isApproved
        [HttpGet("isApproved")]
        public IActionResult getByApproved([FromQuery]int approved)
        {
            var item = _Repository.getByApproved(approved);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }


        [HttpGet("search")]
        public IActionResult search([FromQuery]string name = "")
        {
            if(name!=null) {
                var item = _Repository.search(name);
                if(item!=null) {
                    return Ok(item);
                } else {
                    return new JsonStringResult("[]");    
                }
            } else {
                return new JsonStringResult("[]");
            }
        }

    
        [HttpPost]
        public IActionResult create([FromBody] salaryUpdate b)
        {
            

            _Repository.create(b);

            logger("Insert new salary update " + b.name.first);

            return Ok(new
            {
                type = "success",
                message = b.name.first + " created successfully.",
                slug = b.slug
            });


        }

        
        // PUT get by slug
        [errorHandler]
        [HttpPut("{slug}")]
        public IActionResult update(string slug, [FromBody] salaryUpdate b)
        {
            var item = _Repository.getBySlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.update(slug, b);

           logger( "Update new salary update " + b.name.first);

            return Ok(new
            {
                type = "success",
                message = b.name.first + " updated successfully."
            });
        }

        
        [HttpDelete("{slug}")]
        public IActionResult delete(string slug)
        {

            var item = _Repository.getBySlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.delete(slug);

           logger( "Delete salary update " + item.name.first);
            return Ok(new
            {
                type = "success",
                message = item.name.first + " deleted successfully."
            });
        }

        
    }
}
