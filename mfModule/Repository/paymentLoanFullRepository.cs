﻿using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.mfModule.Models;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace kmbi_core_master.coreMaster.Repository
{
    public class paymentLoansFullRepository : dbCon, ipaymentLoanFullRepository
    {
        public paymentLoansFullRepository(IOptions<dbSettings> settings) : base(settings)
        {
        }

        public void add(paymentLoanFull b)
        {

            b.createdAt = DateTime.Now;
            b.updatedAt = DateTime.Now;

            _db.GetCollection<paymentLoanFull>("paymentLoanFull").InsertOneAsync(b);
        }

        public IEnumerable<paymentLoanFull> all()
        {
            var c = _db.GetCollection<paymentLoanFull>("paymentLoanFull").Find(new BsonDocument()).ToListAsync();
            return c.Result;
        }

        public paymentLoanFull getSlug(String slug)
        {
            var query = Builders<paymentLoanFull>.Filter.Eq(e => e.slug, slug);
            var c = _db.GetCollection<paymentLoanFull>("paymentLoanFull").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }

        public paymentLoanFull getId(String id)
        {
            var query = Builders<paymentLoanFull>.Filter.Eq(e => e.id, id);
            var c = _db.GetCollection<paymentLoanFull>("paymentLoanFull").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }

        public IEnumerable<paymentLoanFull> getIdWeekNo(String id)
        {
            
            var query = Builders<paymentLoanFull>.Filter.Eq(e => e.loan.referenceId, id);
            var c = _db.GetCollection<paymentLoanFull>("paymentLoanFull").Find(query).ToListAsync();

            return c.Result;
        }

        public void update(String id, paymentLoanFull p)
        {
            p.id = id;
            var query = Builders<paymentLoanFull>.Filter.Eq(e => e.id, id);
            var update = Builders<paymentLoanFull>.Update
                .Set("loan", p.loan)
                .Set("terms", p.terms)
                .Set("member", p.member)
                .Set("officialReceipt", p.officialReceipt)
                .Set("payee", p.payee)
                .Set("reason", p.reason)
                .Set("mode", p.mode)
                .Set("remarks", p.remarks)
                .CurrentDate("updatedAt");

            var l = _db.GetCollection<paymentLoanFull>("paymentLoanFull").UpdateOneAsync(query, update);

        }


        public void updateSlug(String slug, paymentLoanFull p)
        {
            p.slug = slug;
            var query = Builders<paymentLoanFull>.Filter.Eq(e => e.slug, slug);
            var update = Builders<paymentLoanFull>.Update
                .Set("loan", p.loan)
                .Set("terms", p.terms)
                .Set("member", p.member)
                .Set("officialReceipt", p.officialReceipt)
                .Set("payee", p.payee)
                .Set("reason", p.reason)
                .Set("mode", p.mode)
                .Set("remarks", p.remarks)
                .CurrentDate("updatedAt");

            var l = _db.GetCollection<paymentLoanFull>("paymentLoanFull").UpdateOneAsync(query, update);

        }


        public void remove(String id)
        {
            var query = Builders<paymentLoanFull>.Filter.Eq(e => e.id, id);
            var result = _db.GetCollection<paymentLoanFull>("paymentLoanFull").DeleteOneAsync(query);
        }


        public void removeSlug(String slug)
        {
            var query = Builders<paymentLoanFull>.Filter.Eq(e => e.slug, slug);
            var result = _db.GetCollection<paymentLoanFull>("paymentLoanFull").DeleteOneAsync(query);
        }
    }
}
