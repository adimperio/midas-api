﻿using kmbi_core_master.coreMaster.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.reports.Models
{

    public class repWeeklyScheduleModel{
        public String referenceId { get; set; }
        public loanloanTypes loanType {get;set;}
        public List<centerInstance> center { get; set; }
        public List<repWeeklyScheduleModelLine> lines {get;set;}
    }

    public class repWeeklyScheduleModelLine {
        public int weekNo {get;set;}
        public DateTime collectionDate {get;set;}
        public double totalPrincipalDue {get;set;}
        public double totalInterestDue {get;set;}
        public double totalCBUDue {get;set;}
        public double total {get;set;}
    }
    

}
