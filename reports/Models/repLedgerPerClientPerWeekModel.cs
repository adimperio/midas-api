using System;
using System.Collections.Generic;

namespace kmbi_core_master.reports.Models
{
    public class repLedgerPerClientPerWeekModel {
        public String transactionNo {get;set;}
        public String centerName {get;set;}
        public Double loanAmountPrincipal {get;set;}
        public Double loanAmountInterest {get;set;}
        public Double loanBalancePrincipal {get;set;}
        public Double loanBalanceInterest {get;set;}
        public Double cbuBalance {get;set;}
        public List<repLedgerPerClientPerWeekLine> repLedgerPerClientPerWeekLine {get;set;}    
    }

    public class repLedgerPerClientPerWeekLine {
        public String clientName {get;set;}
        public Double weekNo {get;set;}
        public Int32 loanCycle {get;set;}
        public Double paymentPrincipal {get;set;}
        public Double paymentInterest {get;set;}
        public Double balancePrincipal {get;set;}
        public Double balanceInterest {get;set;}
        public Double cbuPayment {get;set;}
        public Double cbuBalance {get;set;}
    }
}