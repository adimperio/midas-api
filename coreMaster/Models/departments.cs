﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace kmbi_core_master.coreMaster.Models
{
    public class departments
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("name")]
        public string name { get; set; }
       
        [BsonElement("parent")]
        public parent parent { get; set; }

        [BsonElement("code")]
        public string code { get; set; }

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }
    }

    public class parent
    {
        [BsonElement("id")]
        public string id { get; set; }
        
        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }
    }
}
