﻿using Microsoft.Extensions.Options;

using System;
using System.Collections.Generic;
using System.Linq;

using MongoDB.Bson;
using MongoDB.Driver;

using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.hrModule.Models;
using kmbi_core_master.hrModule.Interface;

namespace kmbi_core_master.hrModule.Repository
{
    public class otLogRepository : dbCon, iotLogsRepository

    {
        IMongoCollection<otLog> _coll;

        public otLogRepository(IOptions<dbSettings> settings) : base(settings)
        {
            _coll = _db.GetCollection<otLog>("otLogs");
        }

        public List<otLog> getOtLogPerEmployeeCodePerDateRange(String employeeCode, DateTime from, DateTime to) {
            var filter = new BsonDocument {
                {"employee.code",employeeCode},
                {"$or",new BsonArray{
                    new BsonDocument {
                        {"$and",new BsonArray {
                            new BsonDocument {
                                {"dates.from",new BsonDocument{
                                    {"$gte",from}
                                }},
                                {"dates.to",new BsonDocument{
                                    {"$lte",from}
                                }}
                            }    
                        }}
                    },
                    new BsonDocument {
                        {"$and",new BsonArray {
                            new BsonDocument {
                                {"dates.from",new BsonDocument{
                                    {"$gte",to}
                                }},
                                {"dates.to",new BsonDocument{
                                    {"$lte",to}
                                }}
                            }    
                        }}
                    },
                    new BsonDocument {
                        {"$and",new BsonArray {
                            new BsonDocument {
                                {"dates.from",new BsonDocument{
                                    {"$gte",from}
                                }},
                                {"dates.to",new BsonDocument{
                                    {"$lte",to}
                                }}
                            }    
                        }}
                    }
                }},                
            };

            return _coll.Find(filter).ToList();

        }

        public void createOtLog(otLog otLog)
        {
            _coll.InsertOne(otLog);
        }
        
        public void updateOtLog(string slug, otLog otLog)
        {
            var filter = new BsonDocument {
                {"slug",slug}
            };

            var update = new BsonDocument {
                {"$set", new BsonDocument {
                    {"type", otLog.type},
                    {"code", otLog.code},
                    {"dates.from", otLog.dates.from},
                    {"dates.to", otLog.dates.to},
                    {"holidayType", otLog.holidayType},
                    {"duration", otLog.duration},
                    {"nd", otLog.nd},
                    {"reason", otLog.reason},
                    {"dateFiled", otLog.dateFiled},
                    {"supervisor", otLog.supervisor},
                    {"comment", otLog.comment},
                    {"updatedAt", otLog.updatedAt}
                }}
            };

            _coll.UpdateOne(filter,update);
        }

        public void deleteOtLog(string slug)
        {
            var filter = new BsonDocument {
                {"slug",slug}
            };

            _coll.DeleteOne(filter);
        }


        public void replaceOtLogPerEmployeeCodePerDate(string employeeCode, DateTime date, otLog otLog)
        {
            var list = getOtLogPerEmployeeCodePerDateRange(employeeCode,date,date);

            list.ForEach((x)=> {
                deleteOtLog(x.slug);
            });

            createOtLog(otLog);
        }

    }
}
