﻿using kmbi_core_master.coreMaster.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.coreMaster.IRepository
{
    public interface iaddressRepository
    {
        List<addRegion> allRegion();

        List<addProvince> allProvince();

        addRegion getRegion(string slug);

        List<addProvince> getProvince(string regSlug);

        List<addMunicipality> getMunicipality(string proSlug);

        List<addBarangay> getBarangay(string munSlug);

    }
}
