﻿using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.helpers.interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using MongoDB.Bson.Serialization.Attributes;

namespace kmbi_core_master.coreMaster.Repository
{
    public class clientsRepository: dbCon, iclientsRepository
    {

        private IHostingEnvironment hostingEnv;
        private iusersRepository _users;
        iupload _upload;

        public clientsRepository(IOptions<dbSettings> settings, IHostingEnvironment env, iusersRepository users, iupload iupload) : base(settings)
        {
            this.hostingEnv = env;
            _users = users;
            _upload = iupload;
        }

        private class clientImage {
            public string clientSlug {get;set;}
            public byte[] bytes {get;set;}
        }

        private class clientSignature : clientImage {}

        private class clientBeneficiarySignature : clientImage {}

        private Tuple<clients, cbuLogs, clientImage, clientSignature, clientBeneficiarySignature> PrepareClientForInsert(clients c) {
            
            var clientFullname = c.name.first + " " + c.name.middle + " " + c.name.last;
            c.fullname = clientFullname;
            c.createdAt = DateTime.Now;
            c.updatedAt = DateTime.Now;

            var year = DateTime.Now.Year.ToString();
            var month = DateTime.Now.Month.ToString();
            string lmonth;

            if (month.Length == 1)
            {
                lmonth = "0" + month;
            }
            else
            {
                lmonth = month;
            }

            var accountNo = year + lmonth + "-" + global.RandomNumber(4) + "-" + global.RandomLetter(4);

            
            c.accountId =  accountNo;

            for (int i = 0; i <= c.dependents.Count - 1; i++)
            {
                c.dependents[i].Id = ObjectId.GenerateNewId().ToString();
            }


            // image processing
            clientImage clientImage = null;
            byte[] clientImageBytes = null;
            try {
                if(c.imageLocation!=null) {
                    var splitted = c.imageLocation.Split(",");
                    clientImageBytes = Convert.FromBase64String(splitted[splitted.Length-1]);
                    clientImage = new clientImage {
                        clientSlug = c.slug,
                        bytes = clientImageBytes
                    };
                }
            } catch {
                c.imageLocation = null;
                clientImage = null;
                clientImageBytes = null;
            }

            // signature process
            clientSignature clientSignature = null;
            byte[] clientSignatureBytes = null;
            try {
                if(c.signature!=null) {
                    var splitted = c.signature.Split(",");
                    clientSignatureBytes = Convert.FromBase64String(splitted[splitted.Length-1]);
                    clientSignature = new clientSignature {
                        clientSlug = c.slug,
                        bytes = clientSignatureBytes
                    };
                }
            } catch {
                c.signature = null;
                clientSignature = null;
                clientSignatureBytes = null;
            }

            // client beneficiary process

            clientBeneficiarySignature clientBeneficiarySignature = null;
            byte[] clientBeneficiarySignatureBytes = null;
            try {
                if(c.beneficiary.signature!=null) {
                    var splitted = c.beneficiary.signature.Split(",");
                    clientBeneficiarySignatureBytes = Convert.FromBase64String(splitted[splitted.Length-1]);
                    clientBeneficiarySignature = new clientBeneficiarySignature {
                        clientSlug = c.slug,
                        bytes = clientBeneficiarySignatureBytes
                    };
                }
            } catch {
                c.beneficiary.signature = null;
                clientBeneficiarySignature = null;
                clientBeneficiarySignatureBytes = null;
            }

            cbuLogs log = new cbuLogs
            {
                Id = ObjectId.GenerateNewId().ToString(),
                type = "beginning balance",
                slug = c.slug + '-' + global.slugify(8),
                clientId = c.Id,
                amount = c.cbuBalance,
                cbuBalance =+ c.cbuBalance,
                createdAt = DateTime.Now,
                updatedAt = DateTime.Now
            };

             return Tuple.Create(c, log, clientImage, clientSignature, clientBeneficiarySignature);
        }

        private void persistClient(clients[] clients, cbuLogs[] cbuLogs, clientImage[] clientImages, clientSignature[] clientSignatures, clientBeneficiarySignature[] clientBeneficiarySignatures) {
            if(clients!=null) {
                if(clients.Count() != 0) {
                    _db.GetCollection<clients>("clients").InsertMany(clients);
                    _db.GetCollection<cbuLogs>("cbuLogs").InsertMany(cbuLogs);
                    clientImages.ToList().ForEach(c=>{
                        if(c!=null)
                            UploadPicture(c.clientSlug, c.bytes);
                    });

                    clientSignatures.ToList().ForEach(c=>{
                        if(c!=null)
                            UploadSignature(c.clientSlug, c.bytes);
                    });

                    clientBeneficiarySignatures.ToList().ForEach(c=>{
                        if(c!=null)
                            UploadBeneficiarySignature(c.clientSlug, c.bytes);
                    });
                }
            }
        }

        public void Add(clients c)
        {
            var result = PrepareClientForInsert(c);
            persistClient(new clients[] { result.Item1 }, new cbuLogs[] { result.Item2 }, new clientImage[] { result.Item3 }, new clientSignature[] { result.Item4 }, new clientBeneficiarySignature[] {result.Item5});
        }

        public void Add(clients[] clients) {
            var result = new List<Tuple<clients, cbuLogs, clientImage, clientSignature, clientBeneficiarySignature>>();
            foreach(clients c in clients) {
                result.Add(PrepareClientForInsert(c));
            }

            var _clients = result.Select(x=>x.Item1).ToArray();
            var _cbuLogs = result.Select(x=>x.Item2).ToArray();
            var _clientImages = result.Select(x=>x.Item3).ToArray();
            var _clientSignature = result.Select(x=>x.Item4).ToArray();
            var _clientBeneficiarySignatures = result.Select(x=>x.Item5).ToArray();
            persistClient(_clients, _cbuLogs, _clientImages, _clientSignature, _clientBeneficiarySignatures);
        }

        public clients lastAccountNo()
        {
            var sort = Builders<clients>.Sort.Descending(e => e.accountId);
            var c = _db.GetCollection<clients>("clients").Find(new BsonDocument()).Sort(sort).ToListAsync();
            return c.Result.FirstOrDefault();
        }

        public void addDependents(clientDependents u, string clientsSlug)
        {
            var query = Builders<clients>.Filter.Eq(e => e.slug, clientsSlug);
            var update = Builders<clients>.Update.Push(e => e.dependents, u);
            u.Id = ObjectId.GenerateNewId().ToString();
            _db.GetCollection<clients>("clients").FindOneAndUpdateAsync(query, update);
        }

        public void addCreditLimit(creditLimit u, string clientsSlug)
        {
            var query = Builders<clients>.Filter.Eq(e => e.slug, clientsSlug);
            var update = Builders<clients>.Update.Push(e => e.creditLimit, u);
            //u.Id = ObjectId.GenerateNewId().ToString();
            _db.GetCollection<clients>("clients").FindOneAndUpdateAsync(query, update);
        }


        public IEnumerable<clients> allClients()
        {
            var c = _db.GetCollection<clients>("clients").Find(new BsonDocument()).ToListAsync();
            return c.Result;
        }

        public IEnumerable<string> allClientId()
        {
            var c = _db.GetCollection<clients>("clients").Find(new BsonDocument()).ToListAsync().Result;
            IEnumerable<string> ids = from x in c select x.Id;
            return ids;
        }

        public clients Get(String id)
        {
            var query = Builders<clients>.Filter.Eq(e => e.Id, id);
            var c = _db.GetCollection<clients>("clients").Find(query).ToListAsync();
            
            return c.Result.FirstOrDefault();
        }

        public clients getDepId(string depId)
        {

            var query = Builders<clients>.Filter.ElemMatch(x => x.dependents, x => x.Id == depId);
            var fields = Builders<clients>.Projection
                .Include(u => u.slug)
                .ElemMatch(x => x.dependents, x => x.Id == depId);

            var unit = _db.GetCollection<clients>("clients").Find(query).Project<clients>(fields).ToListAsync();
            return unit.Result.FirstOrDefault();
        }

        //public clients getCreditLimitId(string creditLimitId)
        //{

        //    var query = Builders<clients>.Filter.ElemMatch(x => x.creditLimit, x => x.Id == creditLimitId);
        //    var fields = Builders<clients>.Projection
        //        .Include(u => u.slug)
        //        .ElemMatch(x => x.creditLimit, x => x.Id == creditLimitId);

        //    var unit = _db.GetCollection<clients>("clients").Find(query).Project<clients>(fields).ToListAsync();
        //    return unit.Result.FirstOrDefault();
        //}

        public clients getSlug(String slug)
        {
            var query = Builders<clients>.Filter.Eq(e => e.slug, slug);
            var fields = Builders<clients>.Projection
                .Include(u => u.Id)
                .Include(u => u.applicationDate)
                .Include(u => u.accountId)
                .Include(u => u.name)
                .Include(u => u.fullname)
                .Include(u => u.slug)
                .Include(u => u.birthdate)
                .Include(u => u.birthplace)
                .Include(u => u.contacts)
                .Include(u => u.addresses)
                .Include(u => u.gender)
                .Include(u => u.civilStatus)
                .Include(u => u.religion)
                .Include(u => u.educationalAttainment)
                .Include(u => u.ids)
                .Include(u => u.contacts)
                .Include(u => u.spouse)
                .Include(u => u.branchId)
                .Include(u => u.branchSlug)
                .Include(u => u.beneficiary)
                .Include(u => u.dependents)
                .Include(u => u.creditLimit)
                .Include(u => u.business)
                .Include(u => u.loanCycle)
                .Include(u => u.cbuBalance)
                .Include(u => u.title)
                .Include(u => u.status)
                .Include(u => u.imageLocation)
                .Include(u => u.guarantor)
                .Include(u => u.interview)
                .Include(u => u.agri)
                .Include(u => u.unitSlug)
                .Include(u => u.officerSlug)
                .Include(u => u.centerSlug)
                .Include(u => u.classification)
                .Include(u => u.processStatus)
                .Include(u => u.checkedAt)
                .Include(u => u.approvedAt)
                .Include(u => u.signature)
                .Include(u => u.maidenName);

            var c = _db.GetCollection<clients>("clients").Find(query).Project<clients>(fields).ToListAsync();

            return c.Result.FirstOrDefault();
        }


        public void Update(String slug, clients p)
        {
            
            var filter = Builders<clients>.Filter.Eq(e => e.slug, slug);
            var update = Builders<clients>.Update
                .Set("applicationDate", p.applicationDate)
                .Set("name.first", p.name.first)
                .Set("name.middle", p.name.middle)
                .Set("name.last", p.name.last)
                .Set("fullname", p.name.first + ' ' + p.name.middle + ' ' + p.name.last)
                .Set("maidenName.first", p.maidenName.first)
                .Set("maidenName.middle", p.maidenName.middle)
                .Set("maidenName.last", p.maidenName.last)
                .Set("gender", p.gender)
                .Set("birthdate", p.birthdate)
                .Set("birthplace", p.birthplace)
                .Set("addresses", p.addresses)
                .Set("contacts", p.contacts)
                .Set("civilStatus", p.civilStatus)
                .Set("religion", p.religion)
                .Set("educationalAttainment", p.educationalAttainment)
                .Set("ids", p.ids)
                .Set("spouse", p.spouse)
                .Set("branchId", p.branchId)
                .Set("branchSlug", p.branchSlug)
                .Set("officerId", p.officerId)
                .Set("unitId", p.unitId)
                .Set("centerId", p.centerId)
                // .Set("imageLocation", p.imageLocation)
                // .Set("beneficiary", p.beneficiary)
                .Set("beneficiary.first", p.beneficiary.first)
                .Set("beneficiary.middle", p.beneficiary.middle)
                .Set("beneficiary.last", p.beneficiary.last)
                .Set("beneficiary.relationship", p.beneficiary.relationship)
                .Set("beneficiary.birthdate", p.beneficiary.birthdate)
                .Set("dependents", p.dependents)
                .Set("creditLimit", p.creditLimit)
                .Set("business", p.business)
                .Set("loanCycle", p.loanCycle)
                .Set("cbuBalance", p.cbuBalance)
                .Set("title", p.title)
                .Set("status", p.status)
                .Set("guarantor", p.guarantor)
                .Set("interview", p.interview)
                .Set("agri", p.agri)
                .Set("unitSlug", p.unitSlug)
                .Set("officerSlug", p.officerSlug)
                .Set("centerSlug", p.centerSlug)
                .Set("classification", p.classification)
                .Set("processStatus", p.processStatus)
                .CurrentDate("updatedAt");
            var result = _db.GetCollection<clients>("clients").UpdateOneAsync(filter, update);

        }

        public void updateDependents(string clientsSlug, string dependentsId, clientDependents u)
        {

            var query = Builders<clients>.Filter.And(Builders<clients>.Filter.Eq(x => x.slug, clientsSlug),
            Builders<clients>.Filter.ElemMatch(x => x.dependents, x => x.Id == dependentsId));

            var update = Builders<clients>.Update
                .Set("dependents.$.last", u.last)
                .Set("dependents.$.first", u.first)
                .Set("dependents.$.middle", u.middle)
                .Set("dependents.$.relationship", u.relationship)
                .Set("dependents.$.birthDate", u.birthdate)
                .Set("dependents.$.school", u.school)
                .Set("dependents.$.gradePosition", u.gradePosition)
                .CurrentDate("updatedAt");

            var result = _db.GetCollection<clients>("clients").UpdateOneAsync(query, update);
        }


        //public void updateCreditLimit(string clientsSlug, string creditLimitId, creditLimit u)
        //{

        //    var query = Builders<clients>.Filter.And(Builders<clients>.Filter.Eq(x => x.slug, clientsSlug),
        //    Builders<clients>.Filter.ElemMatch(x => x.creditLimit, x => x.Id == creditLimitId));

        //    var update = Builders<clients>.Update
        //        .Set("creditLimit.$.income", u.income)
        //        .Set("creditLimit.$.expense", u.expense)
        //        .Set("creditLimit.$.netDisposableIncome", u.netDisposableIncome)
        //        .Set("creditLimit.$.creditLimitAmount", u.creditLimitAmount)
        //        .CurrentDate("updatedAt");

        //    var result = _db.GetCollection<clients>("clients").UpdateOneAsync(query, update);
        //}

        public void Remove(String slug)
        {
            var query = Builders<clients>.Filter.Eq(e => e.slug, slug);
            var result = _db.GetCollection<clients>("clients").DeleteOneAsync(query);
            
        }


        public void removeDependent(String clientsSlug, string dependentsId)
        {

            var pull = Builders<clients>.Update.PullFilter(x => x.dependents, a => a.Id == dependentsId);

            var filter1 = Builders<clients>.Filter.And(Builders<clients>.Filter.Eq(a => a.slug, clientsSlug),
                          Builders<clients>.Filter.ElemMatch(q => q.dependents, t => t.Id == dependentsId));

            var result = _db.GetCollection<clients>("clients").UpdateOneAsync(filter1, pull);
            // return getRegion(regSlug) == null;
            //return true;
        }

        //public void removeCreditLimit(String clientsSlug, string creditLimitId)
        //{

        //    var pull = Builders<clients>.Update.PullFilter(x => x.creditLimit, a => a.Id == creditLimitId);

        //    var filter1 = Builders<clients>.Filter.And(Builders<clients>.Filter.Eq(a => a.slug, clientsSlug),
        //                  Builders<clients>.Filter.ElemMatch(q => q.creditLimit, t => t.Id == creditLimitId));

        //    var result = _db.GetCollection<clients>("clients").UpdateOneAsync(filter1, pull);
        //    // return getRegion(regSlug) == null;
        //    //return true;
        //}

        public clients checkSlug(String slug)
        {
            var query = Builders<clients>.Filter.Eq(e => e.slug, slug);
            var c = _db.GetCollection<clients>("clients").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }

        public clients checkTin(String tinNumber)
        {
            // var query = Builders<clients>.Filter.Eq(e => e.tinNumber, tinNumber);
            // var c = _db.GetCollection<clients>("clients").Find(query).ToListAsync();
            //return c.Result.FirstOrDefault();
            
            var query = Builders<clients>.Filter.And(Builders<clients>.Filter.Eq("ids.number", tinNumber),Builders<clients>.Filter.Eq("ids.type", "tinNumber"));
            var c = _db.GetCollection<clients>("clients").Find(query).ToListAsync();
            return c.Result.FirstOrDefault();
            
            
            //return x;
        }

        public IEnumerable<clients> searchClientName(String fullname, string branchId)
        {
            string[] name = fullname.Split(default(string[]), StringSplitOptions.RemoveEmptyEntries);

            List<FilterDefinition<clients>> a = new List<FilterDefinition<clients>>();

            foreach (string x in name)
            {
                a.Add(Builders<clients>.Filter.Regex("fullname",new BsonRegularExpression(x, options: "i")));
            }

            var queryBranch = Builders<clients>.Filter.Eq(e => e.branchId, branchId);

            var querySearch = Builders<clients>.Filter.Or(a);
            
            var query = Builders<clients>.Filter.And(querySearch, queryBranch);
            var list = _db.GetCollection<clients>("clients").Find(query);
            return list.ToList();
        }

        public branch getBranchSlug(String branchId)
        {
            var query = Builders<branch>.Filter.Eq(e => e.Id, branchId);
            var branch = _db.GetCollection<branch>("branch").Find(query).ToListAsync();

            return branch.Result.First();
        }


        public void updateClients(List<loansCBU> p, string type )
        {
            //var list = _db.GetCollection<clients>("clients").Find(new BsonDocument()).ToListAsync().Result;
            //IEnumerable<string> slugs = from x in p select x.slug;
            //List<string> sl = slugs.ToList();
            //var ft = new BsonDocument("slug", new BsonDocument("$in", new BsonArray(slugs)));
            //var list = _db.GetCollection<clients>("clients").Find(ft).ToListAsync().Result;

            if (type == "increment")
            {

                for (int i = 0; i <= p.Count - 1; i++)
                {
                   
                    var query = Builders<clients>.Filter.Eq(e => e.slug, p[i].slug);
                    var result = _db.GetCollection<clients>("clients").Find(query).ToListAsync().Result.FirstOrDefault();
                    var cBal = result.cbuBalance;
                    var update = Builders<clients>.Update
                        .Set("cbuBalance", cBal + p[i].cbu)
                        .CurrentDate("updatedAt");
                    var l = _db.GetCollection<clients>("clients").UpdateOneAsync(query, update);

                    if (p[i].cbu > 0)
                    {
                        cbuLogs c = new cbuLogs
                        {
                            Id = ObjectId.GenerateNewId().ToString(),
                            type = p[i].type,
                            slug = result.slug,
                            clientId = result.Id,
                            amount = p[i].cbu,
                            cbuBalance = cBal + p[i].cbu,
                            loan = p[i].loan,
                            transactedAt = p[i].transactedAt,
                            createdAt = DateTime.Now,
                            updatedAt = DateTime.Now
                        };

                        _db.GetCollection<cbuLogs>("cbuLogs").InsertOneAsync(c);
                    }
                    

                }
            }
            else
            {
                for (int i = 0; i <= p.Count - 1; i++)
                {
                    
                    var query = Builders<clients>.Filter.Eq(e => e.slug, p[i].slug);
                    var result = _db.GetCollection<clients>("clients").Find(query).ToListAsync().Result.FirstOrDefault();
                    var cBal = result.cbuBalance;
                    var update = Builders<clients>.Update
                        .Set("cbuBalance", cBal - p[i].cbu)
                        .CurrentDate("updatedAt");
                    var l = _db.GetCollection<clients>("clients").UpdateOneAsync(query, update);

                    if (p[i].cbu > 0)
                    {
                        cbuLogs c = new cbuLogs
                        {
                            Id = ObjectId.GenerateNewId().ToString(),
                            type = p[i].type,
                            slug = result.slug,
                            clientId = result.Id,
                            amount = p[i].cbu,
                            cbuBalance = cBal - p[i].cbu,
                            loan = p[i].loan,
                            transactedAt = p[i].transactedAt,
                            createdAt = DateTime.Now,
                            updatedAt = DateTime.Now
                        };

                        _db.GetCollection<cbuLogs>("cbuLogs").InsertOneAsync(c);
                    }
                }
            }
        }


        public IEnumerable<clients> getClients(List<string> p)
        {
            var c = _db.GetCollection<clients>("clients").Find(new BsonDocument()).ToListAsync();

            List<clients> newClients = new List<clients>();
            for (int i = 0; i <= p.Count - 1; i++)
            {
                var query = Builders<clients>.Filter.Eq(e => e.Id, p[i].ToString());
                c = _db.GetCollection<clients>("clients").Find(query).ToListAsync();
                var result = c.Result.FirstOrDefault();

                newClients.Add(
                    new clients
                    {
                        Id = result.Id,
                        applicationDate = result.applicationDate,
                        accountId = result.accountId,
                        slug = result.slug,
                        name = result.name,
                        fullname =result.fullname,
                        addresses =result.addresses,
                        contacts =result.contacts,
                        gender=result.gender,
                        birthdate = result.birthdate,
                        birthplace = result.birthplace,
                        civilStatus=result.civilStatus,
                        religion =result.religion,
                        educationalAttainment=result.educationalAttainment,
                        ids =result.ids,
                        spouse =result.spouse,
                        imageLocation =result.imageLocation,
                        branchId =result.branchId,
                        branchSlug =result.branchSlug,
                        officerId=result.officerId,
                        unitId =result.unitId,
                        centerId =result.centerId,
                        beneficiary=result.beneficiary,
                        dependents=result.dependents,
                        creditLimit=result.creditLimit,
                        business=result.business,
                        loanCycle=result.loanCycle,
                        cbuBalance=result.cbuBalance,
                        title=result.title,
                        status=result.status,
                        createdAt=result.createdAt,
                        updatedAt=result.updatedAt,
                        guarantor=result.guarantor,
                        interview=result.interview,
                        agri=result.agri,
                        unitSlug = result.unitSlug,
                        officerSlug = result.officerSlug,
                        centerSlug = result.centerSlug,
                        classification = result.classification,
                        processStatus = result.processStatus,
                        checkedAt = result.checkedAt,
                        approvedAt = result.approvedAt
                    }
                    );
            }
            return newClients;
            //var c = _db.GetCollection<clients>("clients").Find(new BsonDocument()).ToListAsync();

        }

        public IEnumerable<idList> getIds(List<string> p)
        {
            var c = _db.GetCollection<clients>("clients").Find(new BsonDocument()).ToListAsync();

            List<idList> newIdList = new List<idList>();
            for (int i = 0; i <= p.Count - 1; i++)
            {
                var query = Builders<clients>.Filter.Eq(e => e.slug, p[i].ToString());
                c = _db.GetCollection<clients>("clients").Find(query).ToListAsync();
                var result = c.Result.FirstOrDefault();

                newIdList.Add(
                    new idList
                    {
                        ids = result.ids,
                    }
                    );
            }
            return newIdList;

        }


        public void incLoanCycle(List<string> p)
        {
          
            IEnumerable<string> slugs = from x in p select x;
            for (int i = 0; i <= p.Count - 1; i++)
            {
                var query = Builders<clients>.Filter.Eq(e => e.slug, p[i]);
                var result = _db.GetCollection<clients>("clients").Find(query).ToListAsync().Result.FirstOrDefault();
                var loanCycle = result.loanCycle;

                var update = Builders<clients>.Update
                    //.Set("cbuBalance", Convert.ToDouble(string.Format("{0:n2}", cBal * 1.01)))
                    .Set("loanCycle", result.loanCycle + 1)
                    .CurrentDate("updatedAt");
                var l = _db.GetCollection<clients>("clients").UpdateOneAsync(query, update);
                
            }
           
        }

        public List<clients> perBranch(string branchSlug)
        {
            var query = Builders<clients>.Filter.Eq(e => e.branchSlug, branchSlug);
            var client = _db.GetCollection<clients>("clients").Find(query).ToListAsync();

            return client.Result;
        }

        public long cntClients(string branchSlug)
        {

            var query = Builders<clients>.Filter.Eq(e => e.branchSlug, branchSlug);
            var client = _db.GetCollection<clients>("clients").Find(query).Count();

            return client;
        }

        public class sumCbu
        {
            [BsonId]
            [BsonRepresentation(BsonType.ObjectId)]
            public String _id { get; set; }
            public double sum {get;set;}
        }
        public double getSumCBU(String branchSlug)
        {
            var res = _db.GetCollection<clients>("clients").Aggregate()
                .Match(Builders<clients>.Filter.Eq(e => e.branchSlug, branchSlug))
                .Group<sumCbu>(new BsonDocument{
                    {"_id","null"},
                    {"sum",new BsonDocument {
                            {"$sum","$cbuBalance"}
                    }}
                }).ToList();

            if(res!=null) { 
                if(res.Count > 0) { 
                    return res[0].sum; 
                } else { 
                    return 0.0; 
                } 
            } else { 
                return 0.0; 
            }
        }


        public Boolean UploadPicture(String slug, Byte[] bytes)
        {
            clients clients = getSlug(slug);
            var _bytes = bytes;
            if (clients != null)
            {

                var rndString = global.RandomLetter(3) + global.RandomNumber(3);
                String corePath64fn = slug + "-" + rndString + "-64.jpg";
                String corePath256fn = slug + "-" + rndString + "-256.jpg";
                String corePath512fn = slug + "-" + rndString + "-512.jpg";

                MemoryStream ms = new MemoryStream(_bytes);

                Image resized64Image = _users.ResizeImage(Image.FromStream(ms), 64, 64);
                MemoryStream ms2 = new MemoryStream();
                resized64Image.Save(ms2, System.Drawing.Imaging.ImageFormat.Jpeg);

                Image resized256Image = _users.ResizeImage(Image.FromStream(ms), 256, 256);
                MemoryStream ms3 = new MemoryStream();
                resized256Image.Save(ms3, System.Drawing.Imaging.ImageFormat.Jpeg);

                Image resized512Image = _users.ResizeImage(Image.FromStream(ms), 512, 512);
                MemoryStream ms4 = new MemoryStream();
                resized512Image.Save(ms4, System.Drawing.Imaging.ImageFormat.Jpeg);

                String link64 = _upload.upload(ms2.ToArray(), "kmbidevrepo/uploads/clients/pictures/" + corePath64fn);
                String link256 = _upload.upload(ms3.ToArray(), "kmbidevrepo/uploads/clients/pictures/" + corePath256fn);
                String link512 = _upload.upload(ms4.ToArray(), "kmbidevrepo/uploads/clients/pictures/" + corePath512fn);

                try {
                    var imagePath = clients.imageLocation;
                    var filename = imagePath.Split("/").Last();
                    var clientSlug = filename.Split("-");
                    // var mainFilename = client.sp
                    List<String> filenamesToDelete = new List<String>();
                    clientSlug[clientSlug.Length-1] = "64.jpg";
                    filenamesToDelete.Add(string.Join("-",clientSlug));

                    clientSlug[clientSlug.Length-1] = "256.jpg";
                    filenamesToDelete.Add(string.Join("-",clientSlug));

                    clientSlug[clientSlug.Length-1] = "512.jpg";
                    filenamesToDelete.Add(string.Join("-",clientSlug));

                    filenamesToDelete.ForEach(x=>{
                        try {
                            _upload.delete("kmbidevrepo/uploads/clients/pictures/" + x);
                        } catch {

                        }
                    });
                } catch {
                    
                }
                

                if (link512 != "")
                {
                    //update employee
                    var isSuccess = true;
                    _db.GetCollection<clients>("clients").UpdateOne(
                    Builders<clients>
                        .Filter
                        .Eq("slug", slug),
                    Builders<clients>
                        .Update
                        .Set("imageLocation", link512)
                        .CurrentDate("updatedAt")
                    );

                    if (isSuccess)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public Boolean UploadSignature(String slug, Byte[] bytes, Boolean flip = false)
        {
            clients clients = getSlug(slug);
            var _bytes = bytes;
            if (clients != null)
            {
                var rndString = global.RandomLetter(3) + global.RandomNumber(3);
                
                MemoryStream ms = new MemoryStream(_bytes);
                String link = "";
                if(flip) {
                    Image imgRightOrient = Image.FromStream(ms);
                    imgRightOrient.RotateFlip(RotateFlipType.Rotate90FlipNone);
                    MemoryStream msRightOrient = new MemoryStream();
                    imgRightOrient.Save(msRightOrient, System.Drawing.Imaging.ImageFormat.Png);

                    link = _upload.upload(msRightOrient.ToArray(), "kmbidevrepo/uploads/clients/signatures/" + slug + "-" + rndString + "-sig.png");

                    Image imgForTablet = Image.FromStream(ms);
                    MemoryStream msForTable = new MemoryStream();
                    imgForTablet.Save(msForTable, System.Drawing.Imaging.ImageFormat.Png);

                    _upload.upload(msForTable.ToArray(), "kmbidevrepo/uploads/clients/signatures/" + slug + "-" + rndString + "-sig-fortablet.png");
                } else {
                    Image imgRightOrient = Image.FromStream(ms);
                    MemoryStream msRightOrient = new MemoryStream();
                    imgRightOrient.Save(msRightOrient, System.Drawing.Imaging.ImageFormat.Png);

                    link = _upload.upload(msRightOrient.ToArray(), "kmbidevrepo/uploads/clients/signatures/" + slug + "-" + rndString + "-sig.png");

                    Image imgForTablet = Image.FromStream(ms);
                    imgForTablet.RotateFlip(RotateFlipType.Rotate270FlipNone);
                    MemoryStream msForTable = new MemoryStream();
                    imgForTablet.Save(msForTable, System.Drawing.Imaging.ImageFormat.Png);

                    _upload.upload(msForTable.ToArray(), "kmbidevrepo/uploads/clients/signatures/" + slug + "-" + rndString + "-sig-fortablet.png");
                }
                
                
                

                

                
                
                var imagePath = "";
                var filename = "";

                try {
                    imagePath = clients.signature;
                    filename = imagePath.Split("/").Last();
                    var splittedFn = filename.Split("-");
                    splittedFn[splittedFn.Length-1] = "sig-fortablet.png";
                    var lastFn = String.Join("-",splittedFn);

                    _upload.delete("kmbidevrepo/uploads/clients/signatures/" + filename);
                    _upload.delete("kmbidevrepo/uploads/clients/signatures/" + lastFn);
                } catch {

                }

                if (link != "")
                {
                    //update employee
                    var isSuccess = true;
                    _db.GetCollection<clients>("clients").UpdateOne(
                    Builders<clients>
                        .Filter
                        .Eq("slug", slug),
                    Builders<clients>
                        .Update
                        .Set("signature", link)
                        .CurrentDate("updatedAt")
                    );

                    if (isSuccess)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public Boolean UploadBeneficiarySignature(String slug, Byte[] bytes, Boolean flip = false)
        {
            clients clients = getSlug(slug);
            var _bytes = bytes;
            if (clients != null)
            {
                var rndString = global.RandomLetter(3) + global.RandomNumber(3);
                
                MemoryStream ms = new MemoryStream(_bytes);
                String link = "";
                if(flip) {
                    Image imgRightOrient = Image.FromStream(ms);
                    imgRightOrient.RotateFlip(RotateFlipType.Rotate90FlipNone);
                    MemoryStream msRightOrient = new MemoryStream();
                    imgRightOrient.Save(msRightOrient, System.Drawing.Imaging.ImageFormat.Png);

                    link = _upload.upload(msRightOrient.ToArray(), "kmbidevrepo/uploads/clients/signatures/" + slug + "-" + rndString + "-bensig.png");

                    Image imgForTablet = Image.FromStream(ms);
                    MemoryStream msForTable = new MemoryStream();
                    imgForTablet.Save(msForTable, System.Drawing.Imaging.ImageFormat.Png);

                    _upload.upload(msForTable.ToArray(), "kmbidevrepo/uploads/clients/signatures/" + slug + "-" + rndString + "-bensig-fortablet.png");
                } else {
                    Image imgRightOrient = Image.FromStream(ms);
                    MemoryStream msRightOrient = new MemoryStream();
                    imgRightOrient.Save(msRightOrient, System.Drawing.Imaging.ImageFormat.Png);

                    link = _upload.upload(msRightOrient.ToArray(), "kmbidevrepo/uploads/clients/signatures/" + slug + "-" + rndString + "-bensig.png");

                    Image imgForTablet = Image.FromStream(ms);
                    imgForTablet.RotateFlip(RotateFlipType.Rotate270FlipNone);
                    MemoryStream msForTable = new MemoryStream();
                    imgForTablet.Save(msForTable, System.Drawing.Imaging.ImageFormat.Png);

                    _upload.upload(msForTable.ToArray(), "kmbidevrepo/uploads/clients/signatures/" + slug + "-" + rndString + "-bensig-fortablet.png");
                }

                var imagePath = "";
                var filename = "";

                try {
                    // imagePath = clients.beneficiary.signature;
                    // filename = imagePath.Split("/").Last();

                    imagePath = clients.beneficiary.signature;
                    filename = imagePath.Split("/").Last();
                    var splittedFn = filename.Split("-");
                    splittedFn[splittedFn.Length-1] = "bensig-fortablet.png";
                    var lastFn = String.Join("-",splittedFn);

                    _upload.delete("kmbidevrepo/uploads/clients/signatures/" + filename);
                    _upload.delete("kmbidevrepo/uploads/clients/signatures/" + lastFn);
              
                    // _upload.delete("kmbidevrepo/uploads/clients/signatures/" + filename);
                } catch {

                }

                if (link != "")
                {
                    //update employee
                    var isSuccess = true;
                    _db.GetCollection<clients>("clients").UpdateOne(
                    Builders<clients>
                        .Filter
                        .Eq("slug", slug),
                    Builders<clients>
                        .Update
                        .Set("beneficiary.signature", link)
                        .CurrentDate("updatedAt")
                    );

                    if (isSuccess)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        
        public IEnumerable<clients> clientsPerCenter(String centerSlug)
        {
            var coll = _db.GetCollection<clients>("clients");
            var clients = coll.Find(Builders<clients>.Filter.Eq(c=>c.centerSlug, centerSlug)).SortBy(c=>c.name.last).ToList();
            return clients;
        }

        public IEnumerable<clientsDetailedSearch> clientDetailedSearch(String field, Double from, Double to)
        {
            FilterDefinition<clients> query = Builders<clients>.Filter.Empty;
            

            switch (field.ToLower()) {
                case "loanamount":
                    var loans = _db.GetCollection<loans>("loans").Find(
                        Builders<loans>.Filter.And(
                            Builders<loans>.Filter.Gte("members.principalAmount",from),
                            Builders<loans>.Filter.Lte("members.principalAmount",to)
                        )
                    ).ToList();
                    
                    List<loanMembers> mems = new List<loanMembers>();
                    loans.ForEach(l=>{
                        mems.AddRange(l.members);
                    });

                    query = Builders<clients>.Filter.In(c=>c.Id,
                        mems.Select(m=>m.memberId).ToList()
                    );
                    break;
                case "cbuamount":
                    query = Builders<clients>.Filter.And(
                        Builders<clients>.Filter.Gte("cbuBalance", from),
                        Builders<clients>.Filter.Lte("cbuBalance", to)
                    );
                    break;
                case "age":
                    var now = DateTime.Now.Date;
                    query = Builders<clients>.Filter.And(
                        Builders<clients>.Filter.Gte("birthdate", now.AddYears((Int32)(to + 1) * -1)),
                        Builders<clients>.Filter.Lte("birthdate", now.AddYears((Int32)(from) * -1).AddSeconds(-1))
                    );
                    break;
                case "cycle":
                    query = Builders<clients>.Filter.And(
                        Builders<clients>.Filter.Gte(c=>c.loanCycle,from),
                        Builders<clients>.Filter.Lte(c=>c.loanCycle,to)
                    );
                    break;
            }
            
            List<clients> clients = new List<clients>();
            clients  = _db.GetCollection<clients>("clients").Find(query).ToList();

            var loans1 = _db.GetCollection<loans>("loans").Find(
                Builders<loans>.Filter.In("members.memberId",clients.Select(c=>c.Id).ToList())
            ).ToList();
            
            List<clientsDetailedSearch> detClients = new List<clientsDetailedSearch>();

            clients.ForEach(c=>{

                var loanAmount = 0.0;
                //var age = (c.birthdate == Convert.ToDateTime("01/01/0001 12:00 AM")) ? 0 : (DateTime.Now.Date.Subtract(DateTime.Parse(c.birthdate.ToString())).Days) / 365;              
                
                loans1.ForEach(l=>{
                    l.members.ToList().ForEach(m=>{
                        if(c.Id == m.memberId)
                            loanAmount += m.principalAmount;
                    });
                });

                Boolean toAdd = false;
                if(field.ToLower()=="loanamount") {
                    if(loanAmount >= from && loanAmount <= to) {
                        toAdd = true;
                    }
                } else {
                    toAdd = true;
                }

                if(toAdd) {
                    detClients.Add(new clientsDetailedSearch {
                        id = c.Id,
                        slug = c.slug,
                        name = c.name,
                        birthdate = c.birthdate,
                        cbuAmount = c.cbuBalance,
                        loanAmount = loanAmount,
                        classification = c.classification,
                        loanCycleNo = c.loanCycle
                    });
                }
            });

            return detClients;
        }

        public IEnumerable<clients> clientsByCreatedDate(DateTime createdDate, String branchSlug)
        {
            var dtTo = createdDate.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
            var fdate = Builders<clients>.Filter.Gte(e => e.createdAt, createdDate);
            var tdate = Builders<clients>.Filter.Lte(e => e.createdAt, dtTo);
            var branchSlugFilter = Builders<clients>.Filter.Lte(e => e.branchSlug, branchSlug);

            //var sort = Builders<clients>.Sort.Descending(e => e.fullname);
            var query = Builders<clients>.Filter.And(fdate, tdate, branchSlugFilter);
            var result = _db.GetCollection<clients>("clients").Find(query).ToListAsync();
            return result.Result;
        }

        public void updateClassification(changeClassification p)
        {
            for (int i = 0; i <= p.clients.Count - 1; i++)
            {
                var filter = Builders<clients>.Filter.Eq(e => e.slug, p.clients[i]);
                var update = Builders<clients>.Update
                    .Set("classification", p.classification);
                var result = _db.GetCollection<clients>("clients").UpdateOneAsync(filter, update);
            }

        }

        public void updateLoanCycle(changeLoanCycle p)
        {
            for (int i = 0; i <= p.clients.Count - 1; i++)
            {
                var filter = Builders<clients>.Filter.Eq(e => e.slug, p.clients[i]);
                var update = Builders<clients>.Update
                    .Set("loanCycle", p.loanCycle);
                var result = _db.GetCollection<clients>("clients").UpdateOneAsync(filter, update);
            }

        }

        public List<clients> getByProcessStatus(String status,string branchSlug)
        {
            var query1 = Builders<clients>.Filter.Eq(e => e.processStatus, status);
            var query2 = Builders<clients>.Filter.Eq(e => e.branchSlug , branchSlug);
            var query = Builders<clients>.Filter.And(query1, query2);
            var c = _db.GetCollection<clients>("clients").Find(query).ToListAsync();

            return c.Result;
        }

        public List<clientsByProcessStatus> getClientsByProcessStatus(String status, string branchSlug)
        {
            //var query1 = Builders<clientsByProcessStatus>.Filter.Eq(e => e.processStatus, status);
            //var query2 = Builders<clientsByProcessStatus>.Filter.Eq(e => e.branchSlug, branchSlug);
            //var query = Builders<clientsByProcessStatus>.Filter.And(query1, query2);
            //var c = _db.GetCollection<clientsByProcessStatus>("clients").Find(query).SortBy(x => x.centerSlug).ToListAsync();

            //return c.Result;

            var coll = _db.GetCollection<clientsByProcessStatus>("clients");
            var rep = coll.Aggregate()
                .Match(e => e.processStatus == status)
                .Match(e => e.branchSlug == branchSlug)
                .Project<clientsByProcessStatus>(new BsonDocument
                {
                    {"Id", 1},
                    {"slug",1},
                    {"name",1},
                    {"fullname", 1},
                    {"birthdate", 1},
                    {"branchSlug",1},
                    {"centerSlug",1},
                    {"unitSlug",1},
                    {"loanCycle",1},
                    {"cbuBalance",1},
                    {"processStatus",1}
                })
                .ToListAsync();
            return rep.Result;
        }

        public void updateByStatus(String slug, string status)
        {
            var checkedAt =  new DateTime();
            var approvedAt = new DateTime();

            if (status == "checked")
            {
                checkedAt = DateTime.Now;
                var filter = Builders<clients>.Filter.Eq(e => e.slug, slug);
                var update = Builders<clients>.Update
                    .Set("processStatus", status)
                    .Set("checkedAt", checkedAt)
                    .CurrentDate("updatedAt");
                var result = _db.GetCollection<clients>("clients").UpdateOneAsync(filter, update);
            }

            if (status == "approved")
            {
                approvedAt = DateTime.Now;
                var filter = Builders<clients>.Filter.Eq(e => e.slug, slug);
                var update = Builders<clients>.Update
                    .Set("processStatus", status)
                    .Set("approvedAt", approvedAt)
                    .CurrentDate("updatedAt");
                var result = _db.GetCollection<clients>("clients").UpdateOneAsync(filter, update);
            }

            

        }

        public mfModule.Models.activeClientsWithCbuBalance activeClients(string slug, string type)
        {
            var query2 = Builders<clients>.Filter.Eq(type + "Slug", slug);
            var query1 = Builders<clients>.Filter.Gt(e => e.cbuBalance, 0);
            var query = Builders<clients>.Filter.And(query1, query2);
            var c = _db.GetCollection<clients>("clients").Find(query).ToList();

            if(c.Count > 0) {
                return new mfModule.Models.activeClientsWithCbuBalance {
                    count = c.Count,
                    amount = c.Sum(x=>x.cbuBalance)
                };
            } else {
                return null;
            }
        }

        public Boolean UploadPicture(String slug, String b64String) {
            UploadPicture(slug, processb64stringimage(b64String));
            var client = getSlug(slug);
            return true;
        }

        public Boolean UploadSignature(String slug, String b64String, Boolean flip) {
            UploadSignature(slug, processb64stringimage(b64String), flip);
            var client = getSlug(slug);
            return true;
        }

        public Boolean UploadBeneficiarySignature(String slug, String b64String, Boolean flip) {
            UploadBeneficiarySignature(slug, processb64stringimage(b64String), flip);
            var client = getSlug(slug);
            return true;
        }

        private byte[] processb64stringimage(String b64String) {
            String[] splitted = b64String.Split(",");
            byte[] clientImageBytes = Convert.FromBase64String(splitted[splitted.Length-1]);
            return clientImageBytes;
        }

        public IEnumerable<clients> GetClients(string[] ids)
        {
            var clients = _db.GetCollection<clients>("clients").Find(Builders<clients>.Filter.In(x=>x.Id,ids)).ToList();
            return clients;
        }

        public List<Tuple<String, String, String>> checkIfClientSlugIsExisting(clients[] clients) {
            var items = new List<Tuple<String, String, String>>();

            
            clients.ToList().ForEach(x=>{
                x.fullname = String.Format("{0}, {1} {2}",x.name.last, x.name.first, x.name.middle);
                if(getSlug(x.slug)!=null) {
                    items.Add(new Tuple<String, String, String>(x.slug,x.fullname,"existing"));
                } else {
                    items.Add(new Tuple<String, String, String>(x.slug,x.fullname,"not-existing"));
                }
            });

            return items;
        }

        public List<extractCsvClients> extractClients()
        {
            //Get Clients
            var getClients = _db.GetCollection<clients>("clients").Find(new BsonDocument()).ToListAsync();

            List<csvLoans> eLoans = new List<csvLoans>();
            //Get Loans
            var getLoans = _db.GetCollection<loans>("loans").Find(new BsonDocument()).ToListAsync();
            foreach (var item in getLoans.Result)
            {
                foreach (var c in item.members)
                {
                    eLoans.Add(new csvLoans
                    {
                        branchName = item.branchName,
                        centerLoanCycle = item.centerLoanCycle,
                        centerSlug = item.centerSlug,
                        memberSlug = c.slug,
                        loanAmount = c.principalAmount,
                        loanCycle = c.loanCycle,
                        creditLimitAmount = c.creditLimitAmount,
                        issuedId = c.issuedId.name
                    });
                }
            }

            List<extractCsvClients> eClients = new List<extractCsvClients>();
            foreach (var item in getClients.Result)
            {
                

                //center
                var qCenter = Builders<centerInstance>.Filter.Eq(e => e.slug, item.centerSlug);
                var center = _db.GetCollection<centerInstance>("centerInstance").Find(qCenter).FirstOrDefault();
                var centerCode = "";
                if (center != null)
                {
                    centerCode = center.code;
                }

                

                ////if (clients != null)
                ////{
                //eClients.Add(new extractCsvClients
                //{
                //        branch = branchName,
                //        centerCode = centerCode,
                //        centerLoanCycle = center.loanCycle,
                //        currentCbuBalance = clients.name.middle,
                //        lastName = clients.name.last,
                //        loanCycle = clients.loanCycle,
                //        branch = branchName,
                //        center = centerName,
                //        score = item.score
                //    });
                ////}

            }

            return eClients;
        }
    }
}
