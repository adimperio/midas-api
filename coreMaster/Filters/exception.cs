﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Net.Http;
using System.Net;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace kmbi_core_master.coreMaster.Filters
{
    public class exception : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            var response = new HttpResponseMessage(HttpStatusCode.NotFound)
            {
                ReasonPhrase = "E-mail/password combination does not match",
                StatusCode = HttpStatusCode.NotFound
            };
            var exception = context.Exception;
            context.Result = new ObjectResult(response);

           // return new ObjectResult(response);
        }
    }
}
