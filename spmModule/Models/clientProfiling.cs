﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace kmbi_core_master.spm.Models
{

    public class clientProfiling
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string slug { get; set; }
        public cpClient client { get; set; }
        public cpQuestionaire questionaire { get; set; }
        public List<cpQuestions> questions { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
    }

    public class cpClient
    {
        public string id { get; set; }
        public string slug { get; set; }
        public string name { get; set; }
    }

    public class cpQuestionaire
    {
        public string id { get; set; }
        public string slug { get; set; }
        public string name { get; set; }
        public string version { get; set; }
    }

    public class cpQuestions
    {
        public string question { get; set; }
        public string type { get; set; }
        public string columns { get; set; }
        public string series { get; set; }
        public spAnswers answer { get; set; }
    }

    public class spAnswers
    {
        public string choice { get; set; }
        public string value { get; set; }
    }

}
