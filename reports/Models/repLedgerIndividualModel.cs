using System;
using System.Collections.Generic;

namespace kmbi_core_master.reports.Models
{
    public class repLedgerIndividualModel
    {
        public String accountNo {get;set;}
        public String clientName {get;set;}
        public String address {get;set;}
        public String center {get;set;}
        public String business {get;set;}
        public String transactionNo {get;set;}
        public Double principalAmount {get;set;}
        public Double interestAmount {get;set;}
        public List<repLedgerIndividualLine> repLedgerIndividualLine {get;set;}    
    }

    public class repLedgerIndividualLine
    {
        public Int32 weekNo {get;set;}
        public List<repEcrPayment> payments {get;set;}

        public Double pastDuePrincipal {get;set;}
        public Double pastDueInterest {get;set;}
        public Double pastDueCBU {get;set;}

        public Double paymentPrincipal {get;set;}
        public Double paymentInterest {get;set;}
        public Double paymentCBU {get;set;}

        public Double balancePrincipal {get;set;}
        public Double balanceInterest {get;set;}
        public Double balanceCBU {get;set;}

        public Double totalPayment {get;set;}
    }
}