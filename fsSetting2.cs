﻿using kmbi_core_master.coreMaster.Models;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace kmbi_core_master
{
    public class fsSetting2 {
        public fsSetting2IncomeStatement incomeStatement {get;set;}
        public fsSetting2BalanceSheet balanceSheet {get;set;}
    }

    public class fsSetting2Item {
        public String name {get;set;}
        public Int32 balance {get; set;}
        public Int32[] accounts {get; set;}
    }

    public class fsSetting2IncomeStatement {
        public fsSetting2Item interestIncomeOnLoansReceivable {get;set;}
        public fsSetting2Item serviceIncome {get;set;}
        public fsSetting2Item interestOnDepositAndPlacements {get;set;}
        public fsSetting2Item forexGainOrLoss {get;set;}
        public fsSetting2Item otherIncome {get;set;}
        public fsSetting2Item interestExpenseOnClientsCbu {get;set;}
        public fsSetting2Item badDebtsExpense {get;set;}
        public fsSetting2Item personnelCosts {get;set;}
        public fsSetting2Item outstation {get;set;}
        public fsSetting2Item fringeBenefit {get;set;}
        public fsSetting2Item retirement {get;set;}
        public fsSetting2Item outsideServices {get;set;}
        public fsSetting2Item professionalFees {get;set;}
        public fsSetting2Item meetingsTrainingsAndConferences {get;set;}
        public fsSetting2Item transformationExpense {get;set;}
        public fsSetting2Item litigation {get;set;}
        public fsSetting2Item supplies {get;set;}
        public fsSetting2Item utilities {get;set;}
        public fsSetting2Item communication {get;set;}
        public fsSetting2Item courierCharges {get;set;}
        public fsSetting2Item rental {get;set;}
        public fsSetting2Item insurance {get;set;}
        public fsSetting2Item transporationAndTravel {get;set;}
        public fsSetting2Item taxesAndLicenses {get;set;}
        public fsSetting2Item repairsAndMaintenance {get;set;}
        public fsSetting2Item depreciationAndAmortization {get;set;}
        public fsSetting2Item lossOnAssetWriteOff {get;set;}
        public fsSetting2Item others {get;set;}
        public fsSetting2Item incomeTaxExpense {get;set;}
    }

    public class fsSetting2BalanceSheet {
        public fsSetting2Item cashAndCashEquivalents {get;set;}
        public fsSetting2Item marketableEquitySecuritiesNet {get;set;}
        public fsSetting2Item loansReceivableNet {get;set;}
        public fsSetting2Item interestReceivable {get;set;}
        public fsSetting2Item otherReceivablesNet {get;set;}
        public fsSetting2Item otherCurrentAssets {get;set;}
        public fsSetting2Item availableForSaleAfsFinancialAsset {get;set;}
        public fsSetting2Item restrictedCash {get;set;}
        public fsSetting2Item propertyAndEquipment {get;set;}
        public fsSetting2Item advancesToAffiliates {get;set;}
        public fsSetting2Item otherNoncurrentAssets {get;set;}
        public fsSetting2Item tradeAndOtherPayables {get;set;}
        public fsSetting2Item clientsCbu {get;set;}
        public fsSetting2Item taxPayable {get;set;}
        public fsSetting2Item miCgliPayables {get;set;}
        public fsSetting2Item unearnedInterestIncome {get;set;}
        public fsSetting2Item provisionForContingencies {get;set;}
        public fsSetting2Item deferredTaxLiability {get;set;}
        public fsSetting2Item retirementBenefitLiability {get;set;}
        public fsSetting2Item retainedEarnings {get;set;}
        public fsSetting2Item cumulativeRemeasurementGainsOnRetirementBenefit {get;set;}
        public fsSetting2Item fairValueReserveOnAfsFinancialAsset {get;set;}
    }
    
}
