using System;
using kmbi_core_master.coreMaster.Models;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;


namespace kmbi_core_master.reports.Models
{
    public class repCashReceiptReportModel
    {
        public DateTime date {get;set;}
        public String receivedFrom {get;set;}
        public String particulars {get;set;}
        public String orNo {get;set;}
        public String account {get;set;}
        public String type {get;set;}
        public Double debit {get;set;}
        public Double credit {get;set;}
    }
}