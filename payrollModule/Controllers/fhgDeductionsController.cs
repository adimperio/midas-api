﻿using kmbi_core_master.coreMaster;
using kmbi_core_master.coreMaster.Filters;
using kmbi_core_master.payrollModule.IRepository;
using kmbi_core_master.payrollModule.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System;
using Microsoft.AspNetCore.Http;
using System.IO;
using OfficeOpenXml;

namespace kmbi_core_master.payrollModule.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/fhgdeductions")]
    [usersAuth]
    public class fhgDeductionsController : Controller
    {
        private readonly ifhgDeductionsRepository _repository;

        public fhgDeductionsController(ifhgDeductionsRepository repository)
        {
            _repository = repository;
        }

        [HttpGet("getall")]
        public IActionResult getAll() {
            return Ok(_repository.getAll());
        }

        [HttpGet("getbycutoff")]
        public IActionResult getByCutOff([FromQuery]DateTime from, [FromQuery]DateTime to) {
            return Ok(_repository.getByCutOff(from, to));
        }

        [HttpPost("extract")]
        public IActionResult extract([FromQuery]DateTime from, [FromQuery]DateTime to, [FromForm]IFormFile file) {
            Stream fs = file.OpenReadStream();
            ExcelPackage package = new ExcelPackage(fs);
            ExcelWorksheet sheet = package.Workbook.Worksheets[1];

            
            List<fhgDeductionExtracted> deductions = new List<fhgDeductionExtracted>();

            for(int i = sheet.Dimension.Start.Row; i <= sheet.Dimension.End.Row; i++) {
                if(sheet.Cells[i,1].Value!=null) {
                    if(sheet.Cells[i,1].Value.ToString() != "code") {
                        deductions.Add(new fhgDeductionExtracted {
                            from = from,
                            to = to,
                            fidelityBond = Convert.ToDouble(sheet.Cells[i,3].Value),
                            fhgLoanPayable = Convert.ToDouble(sheet.Cells[i,4].Value),
                            fhgShareCapital = Convert.ToDouble(sheet.Cells[i,5].Value),
                            fhgSavings = Convert.ToDouble(sheet.Cells[i,6].Value),
                            fhgInsurance = Convert.ToDouble(sheet.Cells[i,7].Value),
                            fhgOthers = Convert.ToDouble(sheet.Cells[i,8].Value),
                            employeeCode = sheet.Cells[i,1].Value.ToString(),
                            fullname = sheet.Cells[i,2].Value.ToString(),
                        });
                    }
                }
            }
            
            return Ok(deductions);
        }

        [HttpPost("insert")]
        public IActionResult insert([FromBody]fhgDeductionExtracted[] deductions) {
            if(deductions==null) {
                return Ok(new {
                    type = "failed",
                    message = "deductions insert failed"
                });
            };

            if(deductions.Length == 0) {
                return Ok(new {
                    type = "failed",
                    message = "deductions insert failed"
                });
            };

            var now = DateTime.Now;

            _repository.deleteAll(deductions[0].from, deductions[0].to);

            _repository.insertAll(deductions.Select(x=>new fhgDeduction {
                cutoff = new fhgDeductionCutOff {
                    from = x.from,
                    to = x.to
                },
                deductions = new fhgDeductionDeductions {
                    fidelityBond = x.fidelityBond,
                    fhgLoanPayable = x.fhgLoanPayable,
                    fhgShareCapital = x.fhgShareCapital,
                    fhgSavings = x.fhgSavings,
                    fhgInsurance = x.fhgInsurance,
                    fhgOthers = x.fhgOthers
                },
                employeeCode = x.employeeCode,
                fullname = x.fullname,
                createdAt = now,
                updatedAt = now
            }).ToArray());

            return Ok(new {
                type = "success",
                message = "deductions inserted sucessfully"
            });
        }

    }
}
