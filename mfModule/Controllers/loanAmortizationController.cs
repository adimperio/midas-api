﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Cors;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace kmbi_core_master.mfModule.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/loanAmortization")]
    public class loanAmortizationController : Controller
    {
        
        [HttpGet()]
        public IList<AmortPayment> CalculateLoan([FromQuery] double principalAmount, [FromQuery] int term, [FromQuery] DateTime disbursementDate)
        {
           
            double interestRate = 0;
            double weeklyInterest = 0;
           
            double currentBalance;
            double weeklyPrincipal = 0;
            double interestAmount = 0;
            double totalLoanAmount = 0;

            //double totalInterestAmount = 0;

            interestAmount = principalAmount * 0.2;
            totalLoanAmount = principalAmount + interestAmount;
            currentBalance = principalAmount;
            //totalLoanAmount = principalAmount;
            //totalInterestAmount = interestAmount;
            //currentBalance = totalLoanAmount;

            if (term == 24)
            {
                interestRate = 0.7868/52;
            }
            else
            {
                interestRate = 0.85558/52;
            }

            // Calculate the weekly payment and round it to 0 decimal places           
            var weeklyPayment = ((interestRate) / (1 - (Math.Pow((1 + (interestRate)), - (term))))) * principalAmount;
            weeklyPayment = Math.Round(weeklyPayment, 0);

            // Storage List
            List<AmortPayment> amortPaymentList = new List<AmortPayment>();

            for (int j = 0; j < term; j++)
            {
                // Calculate weekly cycle
                weeklyInterest = Math.Round((currentBalance * interestRate),2);
                weeklyPrincipal = Math.Round(weeklyPayment,2) - Math.Round(weeklyInterest,2);
                //weeklyPrincipal += weeklyPrincipal;
                currentBalance = Math.Round(currentBalance,2) - Math.Round(weeklyPrincipal,2);

                
                if (j == term - 1 && currentBalance != weeklyPayment)
                {
                    // Adjust the last payment to make sure the final balance is 0
                    weeklyPayment += currentBalance;
                    weeklyPrincipal = weeklyPayment - weeklyInterest;
                    weeklyInterest = Math.Round(weeklyPayment,0) - weeklyPrincipal;
                    currentBalance = 0;
                }

                disbursementDate = disbursementDate.AddDays(7);
               
                amortPaymentList.Add
                    (new AmortPayment
                    {
                        weekNumber = j + 1,
                        collectionDate = disbursementDate,
                        principal = Math.Round(weeklyPrincipal, 2),
                        interest = Math.Round(weeklyInterest, 2),
                        totalWeekly = Math.Round(weeklyPayment, 0),
                        RunningBalance = Math.Round(currentBalance, 0)
                    });


            }

            return amortPaymentList.ToList();
        }

        public class AmortPayment
        {
            public int weekNumber { get; set; }
            public DateTime collectionDate { get; set; }
            public double principal { get; set; }
            public double interest { get; set; }
            public double totalWeekly { get; set; }
            public double RunningBalance { get; set; }
        }
    }
}
