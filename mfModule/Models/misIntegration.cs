﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace kmbi_core_master.coreMaster.Models
{
    public class misIntegration
    {
        //[BsonId]
        //[BsonRepresentation(BsonType.ObjectId)]
        //public string Id { get; set; }
        //public string slug { get; set; }
        public string branch { get; set; }
        public string center { get; set; }
        public DateTime disbursementDate { get; set; }
        public DateTime maturityDate { get; set; }
        public Int32 cycle { get; set; }
        public double loanAmount { get; set; }
        public string lastName { get; set; }
        public string firstName { get; set; }
        public string middleName { get; set; }
        public DateTime birthdate { get; set; }
        public string gender { get; set; }
        public string type { get; set; }
        public string civilStatus { get; set; }
    }
}
