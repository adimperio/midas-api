﻿using kmbi_core_master.filesModule.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.filesModule.Interface
{
    public interface icategoryRepository
    {
        IEnumerable<category> all();

        category getSlug(String slug);

        void add(category b);

        void update(String slug, category b);

        void remove(string slug);

        List<category> search(string s);
    }
}
