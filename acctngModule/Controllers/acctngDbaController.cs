﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Filters;
using Microsoft.AspNetCore.Cors;
using kmbi_core_master.acctngModule.Interface;
using kmbi_core_master.acctngModule.Models;
using kmbi_core_master.coreMaster;
using System;
using kmbi_core_master.coreMaster.IRepository;

namespace kmbi_core_master.acctngModule.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/acctngDba")]
    [usersAuth]
    public class acctngDbaController : Controller
    {
        private readonly iacctngDbaRepository _Repository;
        private readonly iusersRepository _logInfo;
        private string token;

        public acctngDbaController(iacctngDbaRepository Repositor, iusersRepository logInfo)
        {
            this._Repository = Repositor;
            _logInfo = logInfo;
        }

        public void logger(string message)
        {
            //Insert Log//
            token = HttpContext.Request.Headers["token"];
            var lInfo = _logInfo.getLoginInfo(token);
            string log = message;
            global.createLog(lInfo.id, lInfo.fullname, lInfo.employment.branch, log);
            //Insert Log//
        }

        // GET: 
        [HttpGet]
        public IEnumerable<acctngDba> GetAll()
        {
            var c = _Repository.all();
            return c;

        }

        //GET  Slug
        [HttpGet("{slug}")]
        public IActionResult getSlug(string slug)
        {

            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        
        [HttpPost]
        public IActionResult create([FromBody] acctngDba b)
        {

            _Repository.add(b);

            logger( "Insert new dba " + b.slug);

            return Ok(new
            {
                type = "success",
                message = "Item created successfully.",
                slug = b.slug
            });

        }

        
        [HttpPut("{slug}")]
        public IActionResult update(string slug, [FromBody] acctngDba b)
        {
            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _Repository.update(slug, b);

            logger("Modified dba " + b.slug);

            return Ok(new
            {
                type = "success",
                message = "Item updated successfully."
            });
        }

        
        [HttpDelete("{slug}")]
        public IActionResult delete(string slug)
        {

            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.remove(slug);

            logger("Delete dba " + item.slug);

            return Ok(new
            {
                type = "success",
                message = "Item deleted successfully."
            });
        }

        
    }
}
