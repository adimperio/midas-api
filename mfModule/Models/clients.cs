﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace kmbi_core_master.coreMaster.Models
{
    public class clients
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("applicationDate")]
        public DateTime? applicationDate { get; set; }

        [BsonElement("accountId")]
        public string accountId { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        public clientFullname name { get; set; }

        public clientFullname maidenName {get;set;}
        
        [BsonElement("fullname")]
        public string fullname { get; set; }
    
        public List<clientAddress> addresses { get; set; }

        public IList<clientContact> contacts { get; set; }

        [BsonElement("gender")]
        public string gender { get; set; }

        [BsonElement("birthdate")]
        public DateTime? birthdate { get; set; }

        [BsonElement("birthplace")]
        public string birthplace { get; set; }

        [BsonElement("civilStatus")]
        public string civilStatus { get; set; }

        [BsonElement("religion")]
        public string religion { get; set; }

        [BsonElement("educationalAttainment")]
        public string educationalAttainment { get; set; }

        [BsonElement("ids")]
        public List<id> ids { get; set; }

        [BsonElement("spouse")]
        public spouse spouse { get; set; }

        [BsonElement("imageLocation")]
        public string imageLocation { get; set; }

        [BsonElement("branchId")]
        public string branchId { get; set; }

        [BsonElement("branchSlug")]
        public string branchSlug { get; set; }

        [BsonElement("officerId")]
        public string officerId {get;set;}
        
        [BsonElement("unitId")]
        public string unitId {get;set;}
        
        [BsonElement("centerId")]
        public string centerId {get;set;}

        public clientBeneficiary beneficiary { get; set; }

        public IList<clientDependents> dependents { get; set; }

        public IList<creditLimit> creditLimit { get; set; }

        [BsonElement("business")]
        public business business { get; set; }

        [BsonElement("loanCycle")]
        public int loanCycle { get; set; }

        [BsonElement("cbuBalance")]
        public double cbuBalance { get; set; }

        [BsonElement("title")]
        public String title {get;set;}

        [BsonElementAttribute("status")]
        public status status {get;set;}

        [BsonElementAttribute("guarantor")]
        public clientGuarantor guarantor { get; set; }

        [BsonElementAttribute("interview")]
        public clientInterview interview { get; set; }

        [BsonElementAttribute("agri")]
        public clientAgri agri { get; set; }

        [BsonElementAttribute("unitSlug")]
        public string unitSlug { get; set; }

        [BsonElementAttribute("officerSlug")]
        public string officerSlug { get; set; }

        [BsonElementAttribute("centerSlug")]
        public string centerSlug { get; set; }

        [BsonElementAttribute("classification")]
        public string classification { get; set; }

        [BsonElementAttribute("processStatus")]
        public string processStatus { get; set; }

        [BsonElement("checkedAt")]
        public DateTime? checkedAt { get; set; }

        [BsonElement("approvedAt")]
        public DateTime? approvedAt { get; set; }

        [BsonElement("createdAt")]
        public DateTime? createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime? updatedAt { get; set; }

        [BsonElementAttribute("signature")]
        public string signature { get; set; }

        [BsonElementAttribute("documents")]
        public List<clientDocuments> documents { get; set; }
    }

    public class spouse
    {
        [BsonElement("name")]
        public spouseName name { get; set; }

        [BsonElement("businessAddress")]
        public string businessAddress { get; set; }

        [BsonElement("occupation")]
        public string occupation { get; set; }

        [BsonElement("employerAddress")]
        public string employerAddress { get; set; }

        [BsonElement("officePhone")]
        public string officePhone { get; set; }

        [BsonElement("birthdate")]
        public DateTime? birthdate { get; set; }
    }

    public class spouseName
    {
        [BsonElement("first")]
        public string first { get; set; }

        [BsonElement("middle")]
        public string middle { get; set; }

        [BsonElement("last")]
        public string last { get; set; }

        [BsonElement("full")]
        public string full { get; set; }
    }

    public class clientFullname
    {
        [BsonElement("first")]
        public string first { get; set; }

        [BsonElement("middle")]
        public string middle { get; set; }

        [BsonElement("last")]
        public string last { get; set; }
    }

    public class clientContact
    {
        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("number")]
        public string number { get; set; }

    }

    public class clientAddress
    {
        [BsonElement("full")]
        public string full { get; set; }

        [BsonElement("region")]
        public region region { get; set; }

        [BsonElement("province")]
        public province province { get; set; }

        [BsonElement("municipality")]
        public municipality municipality { get; set; }

        [BsonElement("barangay")]
        public barangay barangay { get; set; }

        [BsonElement("street")]
        public string street { get; set; }

        [BsonElement("postal")]
        public string postal { get; set; }

        [BsonElement("subdivision")]
        public string subdivision { get; set; }

        [BsonElement("yearStayStarted")]
        public int yearStayStarted { get; set; }
    }

    public class clientBeneficiary
    {
        
        [BsonElement("last")]
        public string last { get; set; }

        [BsonElement("first")]
        public string first { get; set; }

        [BsonElement("middle")]
        public string middle { get; set; }

        [BsonElement("relationship")]
        public string relationship { get; set; }

        [BsonElement("birthdate")]
        [DataType(DataType.Date, ErrorMessage = "Please input valid Birthdate")]
        public DateTime? birthdate { get; set; }

        [BsonElement("signature")]
        public string signature { get; set; }
    }

    public class clientDependents
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("last")]
        public string last { get; set; }

        [BsonElement("first")]
        public string first { get; set; }

        [BsonElement("middle")]
        public string middle { get; set; }

        [BsonElement("relationship")]
        public string relationship { get; set; }

        [BsonElement("birthdate")]
        public DateTime? birthdate { get; set; }


        [BsonElement("school")]
        public string school { get; set; }

        [BsonElement("gradePosition")]
        public string gradePosition { get; set; }
    }

    public class creditLimit
    {
        
        [BsonElement("income")]
        public double income { get; set; }

        [BsonElement("expense")]
        public double expense { get; set; }


        [BsonElement("date")]
        public DateTime? date { get; set; }

        [BsonElement("netDisposableIncome")]
        public double netDisposableIncome { get; set; }

        [BsonElement("creditLimitAmount")]
        public double creditLimitAmount { get; set; }

    }

    public class business
    {
        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("address")]
        public string address { get; set; }

        [BsonElement("yearStayStarted")]
        public int? yearStayStarted { get; set; }

        [BsonElement("yearStarted")]
        public int? yearStarted { get; set; }

        [BsonElement("otherLoanProviders")]
        public List<busOtherLoanProviders> otherLoanProviders { get; set; }

        [BsonElement("type")]
        public string type { get; set; }

        [BsonElement("other")]
        public string other { get; set; }
    }

    public class busOtherLoanProviders
    {
        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("memberSince")]
        public int? memberSince { get; set; }

        [BsonElement("loanType")]
        public string loanType { get; set; }

        [BsonElement("amount")]
        public double? amount { get; set; }

        [BsonElement("term")]
        public int? term { get; set; }
    }

    public class clientSearch
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        public clientFullname name { get; set; }
        
        [BsonElement("fullname")]
        public string fullname { get; set; }

        [BsonElement("birthdate")]
        public DateTime birthdate { get; set; }

        public List<clientAddress> addresses { get; set; }

        [BsonElement("civilStatus")]
        public string civilStatus { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("branchId")]
        public string branchId { get; set; }

        [BsonElement("branchSlug")]
        public string branchSlug { get; set; }

        [BsonElement("ids")]
        public List<id> ids { get; set; }

        public IList<clientDependents> dependents { get; set; }

        public IList<creditLimit> creditLimit { get; set; }

        public clientBeneficiary beneficiary { get; set; }

        [BsonElement("spouse")]
        public spouse spouse { get; set; }

        [BsonElement("business")]
        public business business { get; set; }

        [BsonElement("loanCycle")]
        public int loanCycle { get; set; }

        [BsonElement("cbuBalance")]
        public double? cbuBalance { get; set; }

    }

    public class id
    {
        [BsonElement("type")]
        public String type {get;set;}

        [BsonElement("number")]
        public String number {get;set;}

        [BsonElement("dateIssued")]
        public DateTime? dateIssued {get;set;}

        [BsonElement("placeIssued")]
        public String placeIssued {get;set;}
    }

    public class status
    {
        public String type {get;set;}
        public classification classification {get;set;}
    }

    public class classification
    {
        public String code {get;set;}
        public String name {get;set;}
    }

    public class idList
    {
        [BsonElement("ids")]
        public List<id> ids { get; set; }
    }

    public class clientIds
    {
        [BsonElement("type")]
        public String type { get; set; }

        [BsonElement("number")]
        public String number { get; set; }

        [BsonElement("dateIssued")]
        public DateTime? dateIssued { get; set; }

        [BsonElement("placeIssued")]
        public String placeIssued { get; set; }
    }

    public class addMain {
        [BsonElement("name")]
        public String name { get; set; }

        [BsonElement("slug")]
        public String slug { get; set; }
    }

    public class region : addMain {}
    public class province : addMain {}
    public class municipality : addMain {}
    public class barangay : addMain {}


    public class syncClients
    {
        [BsonId]
        public ObjectId Id { get; set; }

        [BsonElement("applicationDate")]
        public DateTime? applicationDate { get; set; }

        [BsonElement("accountId")]
        public string accountId { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        public clientFullname name { get; set; }
        public clientFullname maidenName {get;set;}

        [BsonElement("fullname")]
        public string fullname { get; set; }

        public List<clientAddress> addresses { get; set; }

        public IList<clientContact> contacts { get; set; }

        [BsonElement("gender")]
        public string gender { get; set; }

        [BsonElement("birthdate")]
        public DateTime? birthdate { get; set; }

        [BsonElement("birthplace")]
        public string birthplace { get; set; }

        [BsonElement("civilStatus")]
        public string civilStatus { get; set; }

        [BsonElement("religion")]
        public string religion { get; set; }

        [BsonElement("educationalAttainment")]
        public string educationalAttainment { get; set; }

        [BsonElement("ids")]
        public List<id> ids { get; set; }

        [BsonElement("spouse")]
        public spouse spouse { get; set; }

        [BsonElement("imageLocation")]
        public string imageLocation { get; set; }

        [BsonElement("branchId")]
        public string branchId { get; set; }

        [BsonElement("branchSlug")]
        public string branchSlug { get; set; }

        [BsonElement("officerId")]
        public string officerId { get; set; }

        [BsonElement("unitId")]
        public string unitId { get; set; }

        [BsonElement("centerId")]
        public string centerId { get; set; }

        public clientBeneficiary beneficiary { get; set; }

        public IList<clientDependents> dependents { get; set; }

        public IList<creditLimit> creditLimit { get; set; }

        [BsonElement("business")]
        public business business { get; set; }

        [BsonElement("loanCycle")]
        public int loanCycle { get; set; }

        [BsonElement("cbuBalance")]
        public double cbuBalance { get; set; }

        [BsonElement("title")]
        public String title { get; set; }

        [BsonElementAttribute("status")]
        public status status { get; set; }

        [BsonElementAttribute("guarantor")]
        public clientGuarantor guarantor { get; set; }

        [BsonElementAttribute("interview")]
        public clientInterview interview { get; set; }

        [BsonElementAttribute("agri")]
        public clientAgri agri { get; set; }

        [BsonElementAttribute("unitSlug")]
        public string unitSlug { get; set; }

        [BsonElementAttribute("officerSlug")]
        public string officerSlug { get; set; }

        [BsonElementAttribute("centerSlug")]
        public string centerSlug { get; set; }

        [BsonElementAttribute("classification")]
        public string classification { get; set; }

        [BsonElementAttribute("processStatus")]
        public string processStatus { get; set; }

        [BsonElement("createdAt")]
        public DateTime? createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime? updatedAt { get; set; }
        
        public DateTime? checkedAt { get; set; }

        public DateTime? approvedAt { get; set; }
        public string signature { get; set; }
    }

    public class clientGuarantor
    {
        [BsonElementAttribute("name")]
        public guarName name { get; set; }

        [BsonElementAttribute("fullname")]
        public string fullname { get; set; }

        [BsonElementAttribute("relationship")]
        public string relationship { get; set; }

        [BsonElementAttribute("birthdate")]
        public DateTime? birthdate { get; set; }

        [BsonElementAttribute("business")]
        public guarBusiness business { get; set; }

        [BsonElementAttribute("mobile")]
        public string mobile { get; set; }

        [BsonElementAttribute("tin")]
        public string tin { get; set; }
    }

    public class guarName
    {
        [BsonElementAttribute("last")]
        public string last { get; set; }

        [BsonElementAttribute("first")]
        public string first { get; set; }

        [BsonElementAttribute("middle")]
        public string middle { get; set; }
    }

    public class guarBusiness
    {
        [BsonElementAttribute("type")]
        public string type { get; set; }

        [BsonElementAttribute("position")]
        public string position { get; set; }

        [BsonElementAttribute("address")]
        public string address { get; set; }

        [BsonElementAttribute("phone")]
        public string phone { get; set; }
    }


    //public class clientPreQua
    //{
    //    [BsonElementAttribute("housingUtilities")]
    //    public preQuaHousingUtilities housingUtilities { get; set; }

    //    [BsonElementAttribute("assets")]
    //    public preQuaAssets assets { get; set; }
    //}

    //public class preQuaHousingUtilities
    //{
    //    [BsonElementAttribute("noOfStorey")]
    //    public int noOfStorey { get; set; }

    //    [BsonElementAttribute("condition")]
    //    public int condition { get; set; }

    //    [BsonElementAttribute("roofing")]
    //    public int roofing { get; set; }

    //    [BsonElementAttribute("walls")]
    //    public int walls { get; set; }

    //    [BsonElementAttribute("floor")]
    //    public int floor { get; set; }

    //    [BsonElementAttribute("toilet")]
    //    public int toilet { get; set; }

    //    [BsonElementAttribute("water")]
    //    public int water { get; set; }

    //    [BsonElementAttribute("electricity")]
    //    public int electricity { get; set; }

    //    [BsonElementAttribute("lot")]
    //    public int lot { get; set; }

    //    [BsonElementAttribute("others")]
    //    public int others { get; set; }

    //    [BsonElementAttribute("total")]
    //    public int total { get; set; }

    //    [BsonElementAttribute("score")]
    //    public double score { get; set; }
    //}

    //public class preQuaAssets
    //{
    //    [BsonElementAttribute("stove")]
    //    public int stove { get; set; }

    //    [BsonElementAttribute("refrigerator")]
    //    public int refrigerator { get; set; }

    //    [BsonElementAttribute("television")]
    //    public int television { get; set; }

    //    [BsonElementAttribute("washingMachine")]
    //    public int washingMachine { get; set; }

    //    [BsonElementAttribute("vehicle")]
    //    public int vehicle { get; set; }

    //    [BsonElementAttribute("others")]
    //    public int others { get; set; }

    //    [BsonElementAttribute("total")]
    //    public int total { get; set; }

    //    [BsonElementAttribute("score")]
    //    public double score { get; set; }
    //}

    public class clientInterview
    {
        [BsonElementAttribute("name")]
        public string name { get; set; }

        [BsonElementAttribute("feedback")]
        public string feedback { get; set; }
    }

    public class clientAgri
    {
        [BsonElementAttribute("hectares")]
        public string hectares { get; set; }
    }

    public class clientsWithLoanAmount : clients {
        public Double loanAmount {get;set;}
    }

    public class changeClassification
    {
        [BsonElement("clients")]
        public List<string> clients { get; set; }

        [BsonElementAttribute("classification")]
        public string classification { get; set; }
    }

    public class changeLoanCycle
    {
        [BsonElement("clients")]
        public List<string> clients { get; set; }

        [BsonElementAttribute("loanCycle")]
        public int loanCycle { get; set; }
    }

    public class clientsByProcessStatus
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        public clientFullname name { get; set; }

        [BsonElement("fullname")]
        public string fullname { get; set; }

        [BsonElement("birthdate")]
        public DateTime? birthdate { get; set; }

        [BsonElement("branchSlug")]
        public string branchSlug { get; set; }

        [BsonElement("centerSlug")]
        public string centerSlug { get; set; }

        [BsonElement("unitSlug")]
        public string unitSlug { get; set; }

        [BsonElement("loanCycle")]
        public int loanCycle { get; set; }

        [BsonElement("cbuBalance")]
        public double cbuBalance { get; set; }

        [BsonElementAttribute("processStatus")]
        public string processStatus { get; set; }
    }


    public class clientDocuments
    {
        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("path")]
        public string path { get; set; }

        [BsonElement("description")]
        public string description { get; set; }

        [BsonElement("createdAt")]
        public DateTime? createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime? updatedAt { get; set; }
    }
}
