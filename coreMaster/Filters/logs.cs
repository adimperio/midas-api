﻿using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using MongoDB.Bson;
using System;
using System.Threading.Tasks;

namespace kmbi_core_master.coreMaster.Filters
{
    public class logs : TypeFilterAttribute
    {

        //public static string userId { get; set; }
        public static string branchId { get; set; }
        public static string log { get; set; }
        public static int level { get; set; }
        public static string link { get; set; }
        public logs(string _branchId, string _log, int _level, string _link) : base(typeof(auditTrail))
        {
           // userId = _userId;
            branchId = _branchId;
            log = _log;
            level = _level;
            link = _link;
        }

        private class auditTrail : IAsyncActionFilter
        {
            private readonly iactivityRepository _actRepository;
            public auditTrail(iactivityRepository actRepository)
            {
                _actRepository = actRepository;
            }

            public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
            {

                //_actRepository.getuserId(userId);
                activity act = new activity()
                {
                    Id = ObjectId.GenerateNewId().ToString(),
                    //userId = global.userId,
                    branchId = branchId,
                    log = log,
                    level = level,
                    link = link,
                    createdAt = DateTime.UtcNow
                };

                _actRepository.log(act);
                await next();
                
            }
        }
    }
}
