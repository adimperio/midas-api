﻿using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.hrModule.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.reports.Models
{

    public class repDtrDetailed {
        public String slug {get;set;}
        public String fullname {get;set;}
        public String employeeCode { get; set; }
        public List<branch> branch {get;set;}
        public employment employment {get;set;}
        public List<employeeSchedule> employeeSchedules {get;set;}
        public List<dtr> dtr {get;set;}
        public List<leaveLog> leaveLogs {get;set;}
    }
}
