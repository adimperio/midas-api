﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace kmbi_core_master.coreMaster.Models
{
    [BsonIgnoreExtraElements]
    public class unwindedUnits
    {
        
        [BsonElement("slug")]
        public string slug{ get; set; }

        [BsonElement("units")]
        public uUnits units { get; set; }

    }

    public class uUnits
    {
        
        [BsonElement("slug")]
        public string slug { get; set; }

       
        [BsonElement("officers")]
        public unwindedOfficers officers { get; set; }

    }


    public class unwindedOfficers
    {
       
        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("designation")]
        public string designation { get; set; }

        [BsonElement("centers")]
        public IList<centers> centers { get; set; }
    }

    public class uOfficers
    {
        //[BsonElement("name")]
        //public string name { get; set; }

        //[BsonElement("slug")]
        //public string slug { get; set; }

        //[BsonElement("designation")]
        //public string designation { get; set; }

        [BsonElement("centers")]
        public IList<centers> centers { get; set; }


    }
}
