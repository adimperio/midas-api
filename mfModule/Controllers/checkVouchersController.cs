﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Filters;
using Microsoft.AspNetCore.Cors;

namespace kmbi_core_master.coreMaster.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/checkVouchers")]
    [usersAuth]
    public class checkVouchersController : Controller
    {
        private readonly icheckVoucherRepository _Repository;
        private readonly iusersRepository _logInfo;
        private string token;

        public checkVouchersController(icheckVoucherRepository Repositor, iusersRepository logInfo)
        {
            this._Repository = Repositor;
            _logInfo = logInfo;
        }

        public void logger(string message)
        {
            //Insert Log//
            token = HttpContext.Request.Headers["token"];
            var lInfo = _logInfo.getLoginInfo(token);
            string log = message;
            global.createLog(lInfo.id, lInfo.fullname, lInfo.employment.branch, log);
            //Insert Log//
        }

        // GET: 
        [HttpGet]
        public IEnumerable<checkVoucher> all()
        {
            var c = _Repository.all();
            return c;

        }

        //GET cv Slug
        [HttpGet("{id}")]
        public IActionResult getId(string id)
        {

            var item = _Repository.getId(id);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

       
        [HttpGet("slug/{slug}")]
        public IActionResult getSlug(string slug)
        {

            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        [HttpGet("referenceId={refId}")]
        public IActionResult getRefId(string refId)
        {

            var item = _Repository.getRefId(refId);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        // GET bank slug
        [HttpGet("branchId={branchId}")]
        public IActionResult getBanks(string branchId)
        {

            var item = _Repository.getBranchSlug(branchId);
            if (item == null)
            {

                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }


        [HttpPost]
        //[errorHandler]
        public IActionResult create([FromBody] checkVoucher b)
        {

            _Repository.add(b);

            logger("Insert new check voucher " + b.referenceId);

            return Ok(new
            {
                type = "success",
                message = "Check voucher created successfully.",
                slug = b.slug
            });

        }


        


        [HttpPut("{id}")]
        public IActionResult update(string id, [FromBody] checkVoucher b)
        {
            var item = _Repository.getId(id);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _Repository.update(id, b);

            logger( "Modified check voucher " + b.referenceId);

            return Ok(new
            {
                type = "success",
                message = "Check voucher updated successfully."
            });
        }


        [HttpPut("slug/{slug}")]
        public IActionResult slugupdate(string slug, [FromBody] checkVoucher b)
        {
            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _Repository.updateSlug(slug, b);

            logger("Modified check voucher " + b.referenceId);

            return Ok(new
            {
                type = "success",
                message = "Check voucher updated successfully."
            });
        }



        [HttpDelete("{id}")]
        public IActionResult delete(string id)
        {

            var item = _Repository.getId(id);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.remove(id);

            logger( "Deleted check voucher " + item.cv[0].number);

            //Insert Deletions
            global.insertDeletion("checkVoucher", item.slug, item.branchSlug);

            return Ok(new
            {
                type = "success",
                message = item.cv[0].number + " deleted successfully."
            });
        }


        [HttpDelete("slug/{slug}")]
        public IActionResult slugdelete(string slug)
        {

            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.removeSlug(slug);

           logger("Delete check voucher " + item.cv[0].number);

            //Insert Deletions
            global.insertDeletion("checkVoucher", slug, item.branchSlug);

            return Ok(new
            {
                type = "success",
                message = item.cv[0].number + " deleted successfully."
            });
        }


    }
}
