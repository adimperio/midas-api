﻿using System;
using System.Collections.Generic;
using kmbi_core_master.coreMaster.Models;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using System.Linq.Expressions;
using kmbi_core_master.documentationModule.Models;
using kmbi_core_master.documentationModule.iRepositories;
using System.Linq;
using kmbi_core_master.coreMaster.IRepository;

namespace kmbi_core_master
{
    public class global : dbCon
    {
       // public static string userId { get; set; }
       // public static string branchId { get; set; } 
       // public static string fullname { get; set; }

        public static List<string> access;

        public global(IOptions<dbSettings> settings) : base(settings)
        {
        }

        public static string Slug(int length, string finalString)
        {

            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[length];
            var random = new Random(Guid.NewGuid().GetHashCode());
            

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            finalString = new String(stringChars);
            return finalString;
        }

        public static string slugify(int length)
        {

            var chars = "abcdefghijklmnopqrstuvwxyz";
            var stringChars = new char[length];
            var random = new Random(Guid.NewGuid().GetHashCode());


            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            var finalString = new String(stringChars);
            return finalString;
        }

        public static string slugify2(int length)
        {

            var chars = "abcdefghijklmnopqrstuvwxy0123456789";
            var stringChars = new char[length];
            var random = new Random(Guid.NewGuid().GetHashCode());


            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            var finalString = new String(stringChars);
            return finalString;
        }

        public static string RandomLetter(int length)
        {

            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var stringChars = new char[length];
            var random = new Random(Guid.NewGuid().GetHashCode());

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            var finalString = new String(stringChars);
            return finalString;
        }

        public static string RandomNumber(int length)
        {

            var chars = "0123456789";
            var stringChars = new char[length];
            var random = new Random(Guid.NewGuid().GetHashCode());

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            var finalString = new String(stringChars);
            return finalString;
        }

        //public static void log(string log, int level, string link)
        //{
        //    activity act = new activity()
        //    {
        //        Id = ObjectId.GenerateNewId().ToString(),
        //        userId = userId,
        //        fullname = fullname,
        //        branchId = branchId,
        //        log = log,
        //        level = level,
        //        link = link,
        //        createdAt = DateTime.Now
        //    };

        //    _db.GetCollection<activity>("activity").InsertOneAsync(act).ToString();

        //}

        public static void createLog(string userId, string fullname, string branchId, string log)
        {
            activity act = new activity()
            {
                Id = ObjectId.GenerateNewId().ToString(),
                userId = userId,
                fullname = fullname,
                branchId = branchId,
                log = log,
                level = 1,
                link = "",
                createdAt = DateTime.Now
            };

            _db.GetCollection<activity>("activity").InsertOneAsync(act).ToString();

        }

        public static void insertDeletion(string collection, string slug, string branchSlug)
        {
            deletions del = new deletions()
            {
                Id = ObjectId.GenerateNewId().ToString(),
                collection = collection,
                slug = slug,
                branchSlug = branchSlug,
                deletedAt = DateTime.Now
            };
            _db.GetCollection<deletions>("deletions").InsertOneAsync(del).ToString();
        }

        public static string AddDoubleQuotes(string value)
        {
            return "\"" + value + "\"";
        }
    }
}
