﻿using kmbi_core_master.coreMaster.IRepository;
using System;
using System.Collections.Generic;
using kmbi_core_master.coreMaster.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using MongoDB.Bson;

namespace kmbi_core_master.coreMaster.Repository
{
    public class activityRepository : dbCon, iactivityRepository
    {
        public activityRepository(IOptions<dbSettings> settings) : base(settings)
        {
        }

        public void log(activity act)
        {
            
            _db.GetCollection<activity>("activity").InsertOneAsync(act);

        }

        public IEnumerable<activity> search(String search)
        {
            var query = Builders<activity>.Filter.Regex("log", new BsonRegularExpression(search, options: "i"));
            var result = _db.GetCollection<activity>("activity").Find(query).ToListAsync();
            return result.Result;
        }

        public IEnumerable<activity> searchId(String Id,DateTime fromdate, DateTime todate)
        {
            var id = Builders<activity>.Filter.Eq(e => e.userId, Id);
            var fdate = Builders<activity>.Filter.Gte(e => e.createdAt, fromdate);
            var tdate = Builders<activity>.Filter.Lte(e => e.createdAt, todate);
            var sort = Builders<activity>.Sort.Descending(e => e.createdAt);
            var query = Builders<activity>.Filter.And(id, fdate, tdate);
            var result = _db.GetCollection<activity>("activity").Find(query).Sort(sort).ToListAsync();
            return result.Result;
        }

        public IEnumerable<activity> searchDate(DateTime fromdate, DateTime todate)
        {
            var fdate = Builders<activity>.Filter.Gte(e => e.createdAt, fromdate);
            var tdate = Builders<activity>.Filter.Lte(e => e.createdAt, todate);
            var sort = Builders<activity>.Sort.Descending(e => e.createdAt);
            var query = Builders<activity>.Filter.And(fdate, tdate);
            var result = _db.GetCollection<activity>("activity").Find(query).Sort(sort).ToListAsync();
            return result.Result;
        }
    }
}
