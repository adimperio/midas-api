using System;

using MailKit.Net.Smtp;
using MimeKit;
using MailKit.Security;

using kmbi_core_master.helpers.interfaces;

namespace kmbi_core_master.helpers
{
    public class email : iNotification {

        public String from {get;set;}
        public String to {get;set;}
        public String subject {get;set;}
        public String message {get;set;}

        public iResult Send()
        {
            const String _displayName = "KMBICORE";
            const String _from = "kmbicore@gmail.com";
            const String outgoingServer = "smtp.gmail.com";
            const Int32 port = 465;
            const String username = "kmbicore@gmail.com";
            const String password = "masteruser2005";

            var emailMessage = new MimeMessage();
            emailMessage.From.Add(new MailboxAddress(_displayName, _from));
            emailMessage.To.Add(new MailboxAddress("", to));
            emailMessage.Subject = subject;

            var bodyBuilder = new BodyBuilder();
            bodyBuilder.HtmlBody = message;
            emailMessage.Body = bodyBuilder.ToMessageBody();

            var client = new SmtpClient();

            iResult res = new emailResult();
            try
            {
                client.Connect(outgoingServer, port, SecureSocketOptions.SslOnConnect);
                client.Authenticate(username, password);
                client.Send(emailMessage);
                client.Disconnect(true);
                res = new emailResult { code = "OK", description = "OK"};
            } 
            catch(Exception ex) 
            {
                res = new emailResult { code = "ERROR", description = ex.Message};
            }
            
            return res;
        }
    }
    
}