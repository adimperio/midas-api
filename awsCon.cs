﻿using kmbi_core_master.coreMaster.Models;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace kmbi_core_master
{
    public class awsCon
    {
        public awsSettings _settings;
        public static string accessKey;
        public static string secretKey;

        public awsCon(IOptions<awsSettings> settings)
        {
            _settings = settings.Value;
            accessKey = _settings.accessKey;
            secretKey = _settings.secretKey;
        }

       
    }
}
