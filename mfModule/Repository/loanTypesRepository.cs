﻿using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Models;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace kmbi_core_master.coreMaster.Repository
{
    public class loanTypesRepository: dbCon, iloanTypesRepository
    {
       
        public loanTypesRepository(IOptions<dbSettings> settings) : base(settings)
        {
        }

        public void Add(loanTypes c)
        {
            c.createdAt = DateTime.Now;
            c.updatedAt = DateTime.Now;
            
            _db.GetCollection<loanTypes>("loanTypes").InsertOneAsync(c);
        }

       
        public IEnumerable<loanTypes> allLoanTypes()
        {
            var c = _db.GetCollection<loanTypes>("loanTypes").Find(new BsonDocument()).ToListAsync();
            return c.Result;
        }

        public loanTypes Get(String id)
        {
            var query = Builders<loanTypes>.Filter.Eq(e => e.Id, id);
            var c = _db.GetCollection<loanTypes>("loanTypes").Find(query).ToListAsync();
            
            return c.Result.FirstOrDefault();
        }

       
        public loanTypes getSlug(String slug)
        {
            var query = Builders<loanTypes>.Filter.Eq(e => e.slug, slug);
            var fields = Builders<loanTypes>.Projection
                .Include(u => u.Id)
                .Include(u => u.name)
                .Include(u => u.interestRate)
                .Include(u => u.amountRange)
                .Include(u => u.frequency)
                .Include(u => u.maxClients)
                .Include(u => u.minClients)
                .Include(u => u.interestType)
                .Include(u => u.processingFee)
                .Include(u => u.initialCBUpercentage)
                .Include(u => u.initialCBU)
                .Include(u => u.cbuAmountPerTerm)
                .Include(u => u.slug)
                .Include(u => u.allowDownpayment)
                .Include(u => u.modesOfPayment)
                .Include(u => u.createdAt)
                .Include(u => u.updatedAt);

            var c = _db.GetCollection<loanTypes>("loanTypes").Find(query).Project<loanTypes>(fields).ToListAsync();

            return c.Result.FirstOrDefault();
        }




        public void Update(String slug, loanTypes p)
        {
            
            var filter = Builders<loanTypes>.Filter.Eq(e => e.slug, slug);
            var update = Builders<loanTypes>.Update
                .Set("name", p.name)
                .Set("interestRate", p.interestRate)
                .Set("amountRange", p.amountRange)
                .Set("frequency", p.frequency)
                .Set("maxClients", p.maxClients)
                .Set("minClients", p.minClients)
                .Set("interestType", p.interestType)
                .Set("processingFee", p.processingFee)
                .Set("initialCBUpercentage", p.initialCBUpercentage)
                .Set("cbuAmountPerTerm", p.cbuAmountPerTerm)
                .Set("allowDownpayment", p.allowDownpayment)
                .Set("modesOfPayment", p.modesOfPayment)
                .CurrentDate("updatedAt");
            var result = _db.GetCollection<loanTypes>("loanTypes").UpdateOneAsync(filter, update);

        }

       
        public void Remove(String slug)
        {
            var query = Builders<loanTypes>.Filter.Eq(e => e.slug, slug);
            var result = _db.GetCollection<loanTypes>("loanTypes").DeleteOneAsync(query);
            
        }


        public loanTypes checkSlug(String slug)
        {
            var query = Builders<loanTypes>.Filter.Eq(e => e.slug, slug);
            var c = _db.GetCollection<loanTypes>("loanTypes").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }


        public IEnumerable<loanTypes> searchLoanTypes(String name)
        {
           
            var query = Builders<loanTypes>.Filter.Regex("name", new BsonRegularExpression(name, options: "i"));
            
            var fields = Builders<loanTypes>.Projection
                .Include(u => u.name)
                .Include(u => u.interestRate)
                .Include(u => u.amountRange)
                .Include(u => u.frequency)
                .Include(u => u.maxClients)
                .Include(u => u.minClients)
                .Include(u => u.interestType)
                .Include(u => u.processingFee)
                .Include(u => u.initialCBUpercentage)
                .Include(u => u.initialCBU)
                .Include(u => u.cbuAmountPerTerm)
                .Include(u => u.allowDownpayment)
                .Include(u => u.modesOfPayment)
                .Exclude(u => u.Id);

            var c = _db.GetCollection<loanTypes>("loanTypes").Find(query).Project<loanTypes>(fields).ToList().AsQueryable();
            return c;
        }


    }
}
