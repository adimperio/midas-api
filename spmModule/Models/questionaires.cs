﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace kmbi_core_master.spm.Models
{
    public class questionaires
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("name")]
        public string name { get; set; }
       
        [BsonElement("version")]
        public string version { get; set; }

        [BsonElement("questions")]
        public List<spmQuestions> questions { get; set; }

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }
    }

    public class spmQuestions
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("question")]
        public string question { get; set; }
        
        [BsonElement("order")]
        public int order { get; set; }

        [BsonElement("isVisible")]
        public bool isVisible { get; set; }

        [BsonElement("type")]
        public string type { get; set; }

        [BsonElement("columns")]
        public string columns { get; set; }

        [BsonElement("series")]
        public string series { get; set; }

        [BsonElement("choices")]
        public List<spmChoices> choices { get; set; }
    }

    public class spmChoices
    {
        [BsonElement("choice")]
        public string choice { get; set; }

        [BsonElement("order")]
        public int order { get; set; }

        [BsonElement("value")]
        public string value { get; set; }
    }

    public class lOrder
    {
        public List<string> id { get; set; }
    }


    public class questionairesSort
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("questions")]
        public spmQuestionsSort questions { get; set; }

       
    }

    public class spmQuestionsSort
    {
        [BsonElement("question")]
        public string question { get; set; }

        [BsonElement("order")]
        public int order { get; set; }

        [BsonElement("isVisible")]
        public bool isVisible { get; set; }

        [BsonElement("hasChoices")]
        public int hasChoices { get; set; }

        [BsonElement("choices")]
        public List<spmChoices> choices { get; set; }
    }

    public class spmChoicesSort
    {
        [BsonElement("choice")]
        public string choice { get; set; }

        [BsonElement("order")]
        public int order { get; set; }

        [BsonElement("value")]
        public string value { get; set; }
    }
}
