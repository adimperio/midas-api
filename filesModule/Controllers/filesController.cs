﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Filters;
using kmbi_core_master.hrModule.Interface;
using kmbi_core_master.hrModule.Models;
using Microsoft.AspNetCore.Cors;
using System;
using kmbi_core_master.filesModule.Interface;
using kmbi_core_master.filesModule.Models;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Amazon.S3.Transfer;
using Amazon.S3;
using Amazon;
using Microsoft.Extensions.Options;
using kmbi_core_master.coreMaster.Models;
using Amazon.S3.Model;

namespace kmbi_core_master.coreMaster.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/files")]
    [usersAuth]
    public class filesController : Controller
    {
        private readonly ifilesRepository _Repository;
        private IHostingEnvironment hostingEnv;
        private awsSettings _settings;

        private static string _awsAccessKey;
        private static string _awsSecretKey;

        public filesController(ifilesRepository Repositor, IHostingEnvironment env, IOptions<awsSettings> settings)
        {
            this._Repository = Repositor;
            this.hostingEnv = env;
            _settings = settings.Value;
            _awsAccessKey = _settings.accessKey;
            _awsSecretKey = _settings.secretKey;
        }

        // GET: 
        [HttpGet]
        public IEnumerable<files> GetAll()
        {
            var c = _Repository.all();
            return c;

        }

        //GET Slug
        [HttpGet("{slug}")]
        public IActionResult getSlug(string slug)
        {

            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

       

        [HttpPost]
        public IActionResult create([FromBody] files b)
        {

            _Repository.add(b);

            return Ok(new
            {
                type = "success",
                message = b.name + " created successfully.",
                slug = b.slug
            });

        }

      
        [HttpPut("{slug}")]
        public IActionResult update(string slug, [FromBody] files b)
        {
            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _Repository.update(slug, b);
         
            return Ok(new
            {
                type = "success",
                message = b.name + " updated successfully."
            });
        }

        

        [HttpDelete("{slug}")]
        public IActionResult delete(string slug)
        {

            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.remove(slug);

            return Ok(new
            {
                type = "success",
                message = item.name + " deleted successfully."
            });
        }

        // SEARCH files
        [HttpGet("search")]
        public IActionResult searchName([FromQuery]string keyword)
        {

            var item = _Repository.search(keyword);
            if (item.Count == 0)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        [HttpPost("toS3")]
        public IActionResult UploadToS3(IFormFile file)
        {
            var client = new AmazonS3Client(_awsAccessKey, _awsSecretKey, RegionEndpoint.APSoutheast1);
            var _bucketName = "kmbi-emo";
            string keyName = string.Format("kmbi-emo/{0}", file.FileName);

            BinaryReader reader = new BinaryReader(file.OpenReadStream());
            Byte[] bytes = reader.ReadBytes((int)file.Length);
            
            Stream stream = new MemoryStream(new MemoryStream(bytes).ToArray());
            PutObjectResponse response = client.PutObjectAsync(new PutObjectRequest
            {
                BucketName = _bucketName,
                Key = keyName,
                InputStream = stream
            }).Result;

            return Ok(new
            {
                message = "Uploaded successfully",
                path = "https://s3-ap-southeast-1.amazonaws.com/" + _bucketName + "/" + keyName,
                type = file.ContentType,
                size = file.Length
            });

        }
    }
}
