﻿using kmbi_core_master.coreMaster.Models;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.reports.Models
{

    public class repDisclosure
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string id { get; set; }
        public String referenceId { get; set; }
        public DateTime disbursementDate { get; set; }
        public DateTime firstPaymentDate { get; set; }

        public loanloanTypes loanType { get; set; }
        public amounts amounts { get; set; }

        public List<branch> branch { get; set; }
        public List<centerInstance> center { get; set; }
        
        public List<loanMembers> members { get; set; }
        
        public String preparedBy {get;set;}
        public String checkedBy {get;set;}
        public String approvedBy {get;set;}
    }

    

}
