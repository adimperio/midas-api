﻿using kmbi_core_master.coreMaster.Controllers;
using kmbi_core_master.coreMaster.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.coreMaster.IRepository
{
    public interface iamortRepository
    {
       
        List<amort> getAmortByterm(int term);
    }
}
