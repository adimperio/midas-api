﻿using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace kmbi_core_master.coreMaster
{
    public class handleBadRequest 
    {
        public static IActionResult badRequest(string msg)
        {
            return new ContentResult
            {
                Content = msg,
                ContentType = "application/json",
                StatusCode = (int?)HttpStatusCode.BadRequest
            };

        }
    }
}
