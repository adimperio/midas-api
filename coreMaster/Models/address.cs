﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace kmbi_core_master.coreMaster.Models
{
    public class addRegion
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("name")]
        public string name { get; set; }
    }

    public class addProvince
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("regionSlug")]
        public string regionSlug { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("name")]
        public string name { get; set; }
    }

    public class addMunicipality
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("provinceSlug")]
        public string provinceSlug { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("name")]
        public string name { get; set; }
    }

    public class addBarangay
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("municipalitySlug")]
        public string municipalitySlug { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("postalCode")]
        public int postalCode { get; set; }
    }
}
