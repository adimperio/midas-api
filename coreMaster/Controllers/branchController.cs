﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Filters;
using Microsoft.AspNetCore.Cors;
using kmbi_core_master.documentationModule.iRepositories;
using kmbi_core_master.helpers.interfaces;
using System;

namespace kmbi_core_master.coreMaster.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/branch")]
    [usersAuth]
    public class branchController : Controller
    {
        private readonly ibranchRepository _branchRepository;
        private readonly iregionsRepository _regionRepository;
        private readonly iglobal2 _global2;
        private readonly iusersRepository _logInfo;
        private string token;
        
        public branchController(ibranchRepository branchRepositor, iregionsRepository regionRepository, iglobal2 global2, iusersRepository logInfo)
        {
            this._branchRepository = branchRepositor;
            this._regionRepository = regionRepository;
            this._global2 = global2;
            _logInfo = logInfo;
        }

        public void logger(string message)
        {
            //Insert Log//
            token = HttpContext.Request.Headers["token"];
            var lInfo = _logInfo.getLoginInfo(token);
            string log = message;
            global.createLog(lInfo.id, lInfo.fullname, lInfo.employment.branch, log);
            //Insert Log//
        }

        // GET: 
        [HttpGet]
        public IEnumerable<branch> GetAll()
        {
            var branch = _branchRepository.allBranch();
            return branch;

        }

        //GET region Slug
        [HttpGet("{slug}")]
        public IActionResult getSlug(string slug)
        {

            var item = _branchRepository.getBranchSlug(slug);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        // GET unit slug
        [HttpGet("unitSlug={unitSlug}")]
        public IActionResult getUnits(string unitSlug)
        {

            var item = _branchRepository.getUnitSlug(unitSlug);
            if (item == null)
            {

                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        // GET bank slug
        [HttpGet("bankSlug={bankSlug}")]
        public IActionResult getBanks(string bankSlug)
        {

            var item = _branchRepository.getBankSlug(bankSlug);
            if (item == null)
            {

                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }


        // GET officer slug
        //[HttpGet("officerSlug={officerSlug}&unitSlug={unitSlug}&branchSlug={branchSlug}")]
        //public IActionResult getOfficer(string branchSlug, string unitSlug, string officerSlug)
        //{

        //    var item = _branchRepository.getOfficerSlug(branchSlug, unitSlug, officerSlug);
        //    if (item == null)
        //    {

        //        return new JsonStringResult("[]");
        //    }

        //    return new ObjectResult(item);
        //}


        // SEARCH branch name

        [HttpGet("searchBranch")]
        public IActionResult searchBranch([FromQuery]string name = "")
        {
            if(name!=null) {
                var item = _branchRepository.searchBranch(name);
                if(item!=null) {
                    return Ok(item);
                } else {
                    return new JsonStringResult("[]");    
                }
            } else {
                return new JsonStringResult("[]");
            }
        }

        // POST branch
        [errorHandler]
        [HttpPost]
        public IActionResult createBranch([FromBody] branch b)
        {
            var email = _branchRepository.checkBranchCode(b.code);
            if (email != null)
            {
                return Ok(new
                {
                    type = "info",
                    message = "Branch code already exist"
                });
            }

            _branchRepository.addBranch(b);
            //_global2.reupdateSchedule();
     
           
            logger("Insert new branch " + b.name);

            return Ok(new
            {
                type = "success",
                message = b.name + " created successfully.",
                slug = b.slug
            });

        }


        // POST units
        [HttpPost("units/{slug}")]
        public IActionResult addUnits([FromBody] units u, string slug)
        {
            _branchRepository.addUnits(u, slug);

           logger("Insert new unit " + u.name);

            return Ok(new
            {
                type = "success",
                message = u.name + " created successfully.",
                unit = u
            });
        }

        // POST bank
        [HttpPost("bank/{slug}")]
        public IActionResult addBanks([FromBody] banks u, string slug)
        {
            _branchRepository.addBanks(u, slug);

           logger("Insert new bank " + u.name);

            return Ok(new
            {
                type = "success",
                message = u.name + " created successfully.",
                slug = u.slug,
                id = u.Id
            });
        }


        // PUT branch
        [errorHandler]
        [HttpPut("{slug}")]
        public IActionResult updateBranch(string slug, [FromBody] branch b)
        {
            var item = _branchRepository.getBranchSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _branchRepository.updateBranch(slug, b);
            //_global2.reupdateSchedule();

            logger("Modified branch " + b.name);

            return Ok(new
            {
                type = "success",
                message = b.name + " updated successfully."
            });
        }

        // PUT units
        [errorHandler]
        [HttpPut("branchSlug={branchSlug}&unitSlug={unitSlug}")]
        public IActionResult updateUnits(string branchSlug, string unitSlug, [FromBody] units u)
        {
            var item = _branchRepository.getUnitSlug(unitSlug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _branchRepository.updateUnits(branchSlug, unitSlug, u);

            logger("Modified unit " + u.name);

            return Ok(new
            {
                type = "success",
                message = u.name + " updated successfully."
            });

        }

        // PUT banks
        [errorHandler]
        [HttpPut("branchSlug={branchSlug}&bankSlug={bankSlug}")]
        public IActionResult updateBanks(string branchSlug, string bankSlug, [FromBody] banks u)
        {
            var item = _branchRepository.getBankSlug(bankSlug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _branchRepository.updateBanks(branchSlug, bankSlug, u);

            logger( "Modified bank " + u.name);

            return Ok(new
            {
                type = "success",
                message = u.name + " updated successfully."
            });

        }
        // PUT officers
        //[errorHandler]
        //[HttpPut("updateOfficer/branchSlug={branchSlug}&unitSlug={unitSlug}&officerSlug={officerSlug}")]
        //public IActionResult updateOfficer(string branchSlug, string unitSlug, string officerSlug, [FromBody] officers o)
        //{

        //    //var item = _branchRepository.getOfficerSlug(officerSlug);
        //    //if (item == null)
        //    //{
        //    //    return BadRequest(new
        //    //    {
        //    //        type = "error",
        //    //        message = "No record found"
        //    //    });
        //    //}

        //    _branchRepository.updateOfficer(branchSlug, unitSlug, officerSlug, o);

        //    //Insert Log//
        //    string log = "Modified officer " + o.name;
        //    int level = 1;
        //    string link = "link here";
        //    global.log(log, level, link);
        //    //Insert Log//

        //    return Ok(new
        //    {
        //        type = "success",
        //        message = o.name + " updated successfully."
        //    });

        //}

        // DELETE branch

        [HttpDelete("{slug}")]
        public IActionResult deleteBranch(string slug)
        {

            var item = _branchRepository.getBranchSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _branchRepository.removeBranch(slug);
            //_global2.reupdateSchedule();
        
            logger("Delete branch " + item.name);

            return Ok(new
            {
                type = "success",
                message = item.name + " deleted successfully."
            });
        }

        //DELETE units
        [HttpDelete("deleteUnits/branchSlug={branchSlug}&unitSlug={unitSlug}")]
        public IActionResult deleteUnits(string branchSlug, string unitSlug)
        {
            var item = _branchRepository.getUnitSlug(unitSlug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _branchRepository.removeUnits(branchSlug, unitSlug);

           logger("Delete unit " + item.units[0].name);

            return Ok(new
            {
                type = "success",
                message = item.units[0].name + " deleted successfully."
            });
        }

        //DELETE banks
        [HttpDelete("branchSlug={branchSlug}&bankSlug={bankSlug}")]
        public IActionResult deleteBanks(string branchSlug, string bankSlug)
        {
            var item = _branchRepository.getBankSlug(bankSlug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _branchRepository.removeBanks(branchSlug, bankSlug);

            logger( "Delete bank " + item.banks[0].name);

            return Ok(new
            {
                type = "success",
                message = item.banks[0].name + " deleted successfully."
            });
        }

        [HttpGetAttribute("getRegionAreaByBranch/{branchSlug}")]
        public IActionResult getRegionAreaByBranch(string branchSlug)
        {
            branch b = _branchRepository.getBranchSlug(branchSlug);
            var reg = _regionRepository.getRegion(b.regionId);
            string regionName = reg.name;
            string areaName = reg.areas.ToList().Find(f => f.Id.ToString() == b.areaId.ToString()).name;

            return Ok(new {region = regionName, area = areaName});
        }


        // PUT branch
        [errorHandler]
        [HttpPut("updateLastSync/{slug}")]
        public IActionResult updateLastSysnc(string slug)
        {
            var item = _branchRepository.getBranchSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _branchRepository.updateLastSync(slug);

           logger( "Modified branch " + item.name);

            return Ok(new
            {
                type = "success",
                message = item.name + " updated successfully."
            });
        }

        // PUT branch
        [errorHandler]
        [HttpPut("updatePaisLastSync/{slug}")]
        public IActionResult updatePaisLastSysnc(string slug)
        {
            var item = _branchRepository.getBranchSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _branchRepository.updatePaisLastSync(slug);

           logger("Modified branch " + item.name);

            return Ok(new
            {
                type = "success",
                message = item.name + " updated successfully."
            });
        }


        // PUT branch employees
        [HttpPut("employees")]
        public IActionResult updateEmployees([FromBody] branchEmployees p)
        {
            var item = _branchRepository.getBranchSlug(p.slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _branchRepository.updateEmployees(p);
            //_global2.reupdateSchedule();

            logger("Modified branch Employees " + p.bmName);

            return Ok(new
            {
                type = "success",
                message = p.bmName + " updated successfully."
            });
        }

        [HttpGet("employees/{branchSlug}")]
        public IActionResult getEmployees(String branchSlug) {
           var item = _branchRepository.getEmployees(branchSlug);
           if(item!=null)
               return Ok(item);
           else
               return Ok(new {
                   type = "faled",
                   message = "invalid parameter"
               });
        }

        // [HttpGet("employees/{branchSlug}")]
        // public IActionResult getBranchEmployees(String branchSlug)
        // {
        //     var item = _branchRepository.getBranchOfficers(branchSlug);
        //     if (item != null)
        //         return Ok(item);
        //     else
        //         return Ok(new
        //         {
        //             type = "failed",
        //             message = "invalid parameter"
        //         });
        // }
    }
}
