﻿using Microsoft.Extensions.Options;

using System;
using System.Collections.Generic;
using System.Linq;

using MongoDB.Bson;
using MongoDB.Driver;

using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.acctngModule.Models;
using kmbi_core_master.acctngModule.Interface;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;

namespace kmbi_core_master.acctngModule.Repository
{
    public class acctngJvRepository : dbCon, iacctngJvRepository
    {
        
        public acctngJvRepository(IOptions<dbSettings> settings) : base(settings) {}

        public void add(acctngJv b)
        {

            b.timestamps.createdAt = DateTime.Now;
            b.timestamps.updatedAt = DateTime.Now;
            for (int i = 0; i <= b.accounts.credit.Count() - 1; i++)
            {
                b.accounts.credit[i].Id = ObjectId.GenerateNewId().ToString();
            }

            for (int i = 0; i <= b.accounts.debit.Count() - 1; i++)
            {
                b.accounts.debit[i].Id = ObjectId.GenerateNewId().ToString();
            }
            var coll = _db.GetCollection<acctngJv>("acctngJv");
            _db.GetCollection<acctngJv>("acctngJv").InsertOneAsync(b);
        }
        

        public IEnumerable<acctngJv> all()
        {
            var c = _db.GetCollection<acctngJv>("acctngJv").Find(new BsonDocument()).ToListAsync();
            return c.Result;
        }

        public IEnumerable<acctngJv> all(DateTime from, DateTime to)
        {
            var filterFrom = Builders<acctngJv>.Filter.Gte(cv=>cv.timestamps.issuedAt, from);
            var filterTo = Builders<acctngJv>.Filter.Lte(cv=>cv.timestamps.issuedAt, to);
            var c = _db.GetCollection<acctngJv>("acctngJv").Find(Builders<acctngJv>.Filter.And(filterFrom,filterTo)).ToListAsync();
            return c.Result;
        }

        public acctngJv getSlug(String slug)
        {
            var query = Builders<acctngJv>.Filter.Eq(e => e.slug, slug);
            var c = _db.GetCollection<acctngJv>("acctngJv").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }

        
        public void update(String slug, acctngJv p)
        {
            //BsonClassMap.RegisterClassMap<jvCredit>(cm =>
            //{
            //    //for (int i = 0; i <= p.accounts.credit.Count(); i++)
            //    //{
            //        cm.MapIdField(x => x.Id)
            //        //.SetSerializer(new StringSerializer(BsonType.ObjectId))
            //        .SetIgnoreIfDefault(true);
            //    //}
                
            //});

            p.slug = slug;
            var query = Builders<acctngJv>.Filter.Eq(e => e.slug, slug);
            var update = Builders<acctngJv>.Update
                .Set("number", p.number)
                .Set("jvNumber", p.jvNumber)
                .Set("particulars", p.particulars)
                .Set("branch", p.branch)
                .Set("accounts", p.accounts)
                .Set("amount", p.amount)
                .Set("isCancelled", p.isCancelled)
                .Set("isClosed", p.isClosed)
                .Set("reason", p.reason)
                .Set("status", p.status)
                .Set("timestamps.issuedAt", p.timestamps.issuedAt)
                .Set("timestamps.issuedBy", p.timestamps.issuedBy)
                .Set("timestamps.createdBy", p.timestamps.createdBy)
                .Set("timestamps.updatedBy", p.timestamps.updatedBy)
                .CurrentDate("timestamps.updatedAt");

            var jv = _db.GetCollection<acctngJv>("acctngJv").UpdateOneAsync(query, update);

        }
        public void remove(String slug)
        {
            var query = Builders<acctngJv>.Filter.Eq(e => e.slug, slug);
            var result = _db.GetCollection<acctngJv>("acctngJv").DeleteOneAsync(query);
        }

        public List<acctngJv> issuedAt(DateTime from, DateTime to)
        {
            
            var fdate = Builders<acctngJv>.Filter.Gte(e => e.timestamps.issuedAt, from);
            var tdate = Builders<acctngJv>.Filter.Lte(e => e.timestamps.issuedAt, to);
            var sort = Builders<acctngJv>.Sort.Descending(e => e.timestamps.issuedAt);
            var query = Builders<acctngJv>.Filter.And(fdate, tdate);

            var c = _db.GetCollection<acctngJv>("acctngJv").Find(query).Sort(sort).ToListAsync();
            return c.Result;

        }

        //CREDIT
        public jvAccounts getCreditId(string creditId)
        {

            var query = Builders<jvAccounts>.Filter.ElemMatch(x => x.credit, x => x.Id == creditId);
            var fields = Builders<jvAccounts>.Projection
                .Include(u => u.credit)
                .ElemMatch(x => x.credit, x => x.Id == creditId);

            var unit = _db.GetCollection<jvAccounts>("acctngJv").Find(query).Project<jvAccounts>(fields).ToListAsync();
            return unit.Result.FirstOrDefault();
        }

        public void addCredit(jvCredit u, string jvSlug)
        {
            var query = Builders<acctngJv>.Filter.Eq(e => e.slug, jvSlug);
            var update = Builders<acctngJv>.Update.Push(e => e.accounts.credit, u);
            u.Id = ObjectId.GenerateNewId().ToString();
            _db.GetCollection<acctngJv>("acctngJv").FindOneAndUpdateAsync(query, update);
        }

        public void updateCredit(string jvSlug, string creditId, jvCredit u)
        {

            var query = Builders<acctngJv>.Filter.And(Builders<acctngJv>.Filter.Eq(x => x.slug, jvSlug),
            Builders<acctngJv>.Filter.ElemMatch(x => x.accounts.credit, x => x.Id == creditId));

            var update = Builders<acctngJv>.Update
                .Set("credit.$.name", u.name)
                .Set("credit.$.code", u.code)
                .Set("credit.$.amount", u.amount)
                .Set("credit.$.particular", u.particular)
                .Set("credit.$.entity", u.entity)
                .Set("credit.$.budget", u.budget);
            var result = _db.GetCollection<acctngJv>("acctngJv").UpdateOneAsync(query, update);
        }

        public void removeCredit(String jvSlug, string creditId)
        {
            var pull = Builders<acctngJv>.Update.PullFilter(x => x.accounts.credit, a => a.Id == creditId);

            var filter1 = Builders<acctngJv>.Filter.And(Builders<acctngJv>.Filter.Eq(a => a.slug, jvSlug),
                          Builders<acctngJv>.Filter.ElemMatch(q => q.accounts.credit, t => t.Id == creditId));

            var result = _db.GetCollection<acctngJv>("acctngJv").UpdateOneAsync(filter1, pull);
        }


        //DEBIT
        public acctngJv getDebitId(string debitId)
        {

            var query = Builders<acctngJv>.Filter.ElemMatch(x => x.accounts.debit, x => x.Id == debitId);
            var fields = Builders<acctngJv>.Projection
                .Include(u => u.Id)
                .Include(u => u.slug)
                .ElemMatch(x => x.accounts.debit, x => x.Id == debitId);

            var unit = _db.GetCollection<acctngJv>("acctngJv").Find(query).Project<acctngJv>(fields).ToListAsync();
            return unit.Result.FirstOrDefault();
        }

        public void addDebit(jvDebit u, string jvSlug)
        {
            var query = Builders<acctngJv>.Filter.Eq(e => e.slug, jvSlug);
            var update = Builders<acctngJv>.Update.Push(e => e.accounts.debit, u);
            u.Id = ObjectId.GenerateNewId().ToString();
            _db.GetCollection<acctngJv>("acctngJv").FindOneAndUpdateAsync(query, update);
        }

        public void updateDebit(string jvSlug, string debitId, jvDebit u)
        {

            var query = Builders<acctngJv>.Filter.And(Builders<acctngJv>.Filter.Eq(x => x.slug, jvSlug),
            Builders<acctngJv>.Filter.ElemMatch(x => x.accounts.debit, x => x.Id == debitId));

            var update = Builders<acctngJv>.Update
                .Set("debit.$.name", u.name)
                .Set("debit.$.code", u.code)
                .Set("debit.$.amount", u.amount)
                .Set("debit.$.entity", u.entity)
                .Set("debit.$.particular", u.particular)
                .Set("debit.$.budget", u.budget);
            var result = _db.GetCollection<acctngJv>("acctngJv").UpdateOneAsync(query, update);
        }

        public void removeDebit(String jvSlug, string debitId)
        {
            var pull = Builders<acctngJv>.Update.PullFilter(x => x.accounts.debit, a => a.Id == debitId);

            var filter1 = Builders<acctngJv>.Filter.And(Builders<acctngJv>.Filter.Eq(a => a.slug, jvSlug),
                          Builders<acctngJv>.Filter.ElemMatch(q => q.accounts.debit, t => t.Id == debitId));

            var result = _db.GetCollection<acctngJv>("acctngJv").UpdateOneAsync(filter1, pull);
        }

        public acctngJv checkOr(String orNo)
        {
            var query = Builders<acctngJv>.Filter.Eq(e => e.number, Convert.ToInt32(orNo));
            var c = _db.GetCollection<acctngJv>("acctngJv").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }


        public void updateIsCancelled(String slug, acctngJv p)
        {
            p.slug = slug;
            var query = Builders<acctngJv>.Filter.Eq(e => e.slug, slug);
            var update = Builders<acctngJv>.Update
                .Set("isCancelled", true)
                .Set("reason", p.reason)
                .Set("timestamps.updatedBy", p.timestamps.updatedBy)
                .CurrentDate("timestamps.updatedAt");

            var jv = _db.GetCollection<acctngJv>("acctngJv").UpdateOneAsync(query, update);

        }

        public void updateIsClosed(DateTime from, DateTime to, acctngJv p)
        {
            var fdate = Builders<acctngJv>.Filter.Gte(e => e.timestamps.issuedAt, from);
            var tdate = Builders<acctngJv>.Filter.Lte(e => e.timestamps.issuedAt, to);
            //var c = _db.GetCollection<cashReceipt>("cashReceipt").Find(query).Sort(sort).ToListAsync();
            //return c.Result;
            var query = Builders<acctngJv>.Filter.And(fdate, tdate);
            var update = Builders<acctngJv>.Update
                .Set("isClosed", true)
                .Set("timestamps.updatedBy", p.timestamps.updatedBy)
                .CurrentDate("timestamps.updatedAt");

            var jv = _db.GetCollection<acctngJv>("acctngJv").UpdateOneAsync(query, update);

        }
    }
}
