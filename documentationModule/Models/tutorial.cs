using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.ComponentModel.DataAnnotations;

namespace kmbi_core_master.documentationModule.Models
{
    public class tutorial
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        [BsonIgnoreIfDefault]
        public String Id { get; set; }
        [BsonElement("name")]
        [RequiredAttribute(ErrorMessage="name is required")]
        public String name {get;set;}
        [BsonElement("slug")]
        public String slug {get;set;}
        [BsonElement("order")]
        public Int32 order {get;set;}
        public List<topic> topics {get;set;}
        [BsonElement("createdAt")]
        public DateTime createdAt {get;set;}
        [BsonElement("updatedAt")]
        public DateTime updatedAt {get;set;}
    }
}