﻿using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Models;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace kmbi_core_master.coreMaster.Repository
{
    public class loanMembersRepository : dbCon, iloanMembersRepository
    {
        public loanMembersRepository(IOptions<dbSettings> settings) : base(settings)
        {
        }

        public void add(loanMembers b)
        {

           // b.createdAt = DateTime.Now;
           // b.updatedAt = DateTime.Now;

            for (int i = 0; i <= b.schedule.Count - 1; i++)
            {
               // b.schedule[i].Id = ObjectId.GenerateNewId().ToString();
               // b.schedule[i].createdAt = DateTime.Now;
               // b.schedule[i].updatedAt = DateTime.Now;
            }
            _db.GetCollection<loanMembers>("loanMembers").InsertOneAsync(b);
        }

        public IEnumerable<loanMembers> getAll()
        {
            var c = _db.GetCollection<loanMembers>("loanMembers").Find(new BsonDocument()).ToListAsync();
            return c.Result;
        }

        public loanMembers getSlug(String slug)
        {
            var query = Builders<loanMembers>.Filter.Eq(e => e.slug, slug);
            var c = _db.GetCollection<loanMembers>("loanMembers").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }

        //public loanMembers getId(String id)
        //{
        //    var query = Builders<loanMembers>.Filter.Eq(e => e.Id, id);
        //    var c = _db.GetCollection<loanMembers>("loanMembers").Find(query).ToListAsync();

        //    return c.Result.FirstOrDefault();
        //}

        //public loanMembers getSchedule(String loanId, string memberId, string scheduleId)
        //{
        //    var query1 = Builders<loanMembers>.Filter.Eq(e => e.loanId, loanId);
        //    var query2 = Builders<loanMembers>.Filter.Eq(e => e.memberId, memberId);
        //    var query3= Builders<loanMembers>.Filter.ElemMatch(x => x.schedule, x => x.Id == scheduleId);
        //    var query = Builders<loanMembers>.Filter.And(query1, query2, query3);
        //    var c = _db.GetCollection<loanMembers>("loanMembers").Find(query).ToListAsync();

        //    return c.Result.FirstOrDefault();
        //}


        //public void update(String id, loanMembers p)
        //{
        //    p.Id = id;
        //    var query = Builders<loanMembers>.Filter.Eq(e => e.Id, id);
        //    var update = Builders<loanMembers>.Update
        //        .Set("name", p.name)
        //        .Set("memberId", p.memberId)
        //        .Set("position", p.position)
        //        .Set("loanId", p.loanId)
        //        .Set("totalAmount", p.totalAmount)
        //        .Set("schedule", p.schedule)
        //        .Set("upfrontFees", p.upfrontFees)
        //        .Set("health", p.health)
        //        .Set("issuedId", p.issuedId)
        //        .Set("dependents", p.dependents)
        //        .Set("creditLimitAmount", p.creditLimitAmount)
        //        .Set("loanCycle", p.loanCycle)
        //        .CurrentDate("updatedAt");

        //    var l = _db.GetCollection<loanMembers>("loanMembers").UpdateOneAsync(query, update);

        //}

        public void updateSlug(String slug, loanMembers p)
        {
            p.slug = slug;
            var query = Builders<loanMembers>.Filter.Eq(e => e.slug, slug);
            var update = Builders<loanMembers>.Update
                .Set("name", p.name)
                // .Set("maidenName", p.maidenName)
                .Set("memberId", p.memberId)
                .Set("position", p.position)
                //.Set("loanId", p.loanId)
                .Set("totalAmount", p.totalAmount)
                .Set("schedule", p.schedule)
                .Set("upfrontFees", p.upfrontFees)
                .Set("health", p.health)
                .Set("issuedId", p.issuedId)
                .Set("dependents", p.dependents)
                .Set("creditLimitAmount", p.creditLimitAmount)
                .Set("loanCycle", p.loanCycle)
                .Set("purpose", p.purpose)
                .CurrentDate("updatedAt");

            var branch = _db.GetCollection<loanMembers>("loanMembers").UpdateOneAsync(query, update);

        }


        //public void remove(String id)
        //{
        //    var query = Builders<loanMembers>.Filter.Eq(e => e.Id, id);
        //    var result = _db.GetCollection<loanMembers>("loanMembers").DeleteOneAsync(query);
        //}


        public void removeSlug(String slug)
        {
            var query = Builders<loanMembers>.Filter.Eq(e => e.slug, slug);
            var result = _db.GetCollection<loanMembers>("loanMembers").DeleteOneAsync(query);
        }
    }
}
