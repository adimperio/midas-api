﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace kmbi_core_master.acctngModule.Models
{
    public class acctEntryTemplateOR
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("slug")]
        public String slug { get; set; }

        [BsonElement("particulars")]
        public string particulars { get; set; }

        [BsonElement("accounts")]
        public crAccounts accounts { get; set; }

        [BsonElement("payor")]
        public crPayor payor { get; set; }

        [BsonElement("timestamps")]
        public crTimestamps timestamps { get; set; }
    }

    
}
