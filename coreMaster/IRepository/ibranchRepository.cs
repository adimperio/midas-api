﻿using kmbi_core_master.coreMaster.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.coreMaster.IRepository
{
    public interface ibranchRepository
    {
        IEnumerable<branch> allBranch();

        branch getBranchSlug(String slug);

        unitList getUnitSlug(string unitSlug);

        banksList getBankSlug(string bankSlug);

        //unwindedUnits getOfficerSlug(string branchSlug, string unitSlug, string officerSlug);

        void addBranch(branch b);

        void updateBranch(String slug, branch b);

        void addUnits(units u, string Id);

        void updateUnits(String branchSlug, string unitSlug, units u);

        void addBanks(banks b, string Id);

        void updateBanks(String branchSlug, string bankSlug, banks u);

        void removeBranch(String slug);

        void removeUnits(String branchSlug, string unitSlug);

        void removeBanks(String branchSlug, string bankSlug);

        IEnumerable<branch> searchBranch(string name);

        branch checkBranchSlug(String slug);

        branch checkBranchCode(String branchCode);

        branch checkUnitCode(String unitCode);

        branch checkBankCode(String bankCode);

        void updateLastSync(String branchSlug);
        
        void updatePaisLastSync(String branchSlug);

        void updateEmployees(branchEmployees p);

        branchEmployees getEmployees(String branchSlug);

        branchOfficers getBranchOfficers(String branchSlug);
    }
}
