﻿using kmbi_core_master.coreMaster.Models;
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
namespace kmbi_core_master.coreMaster.IRepository
{
    public interface iimportCsvRepository
    {
        List<messages> importClients(IFormFile file, String branchId);
        List<messages> importUsers(IFormFile file);
    }
}
