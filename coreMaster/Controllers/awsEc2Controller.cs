﻿using Amazon;
using Amazon.CloudWatch;
using Amazon.CloudWatch.Model;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Amazon.EC2;
using Amazon.EC2.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using kmbi_core_master.coreMaster.Models;
using Microsoft.Extensions.Options;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace kmbi_core_master.coreMaster.Controllers
{
    [Route("api/awsEc2")]
    public class awsEc2Controller : Controller
    {
        private awsSettings _settings;

        private static string _awsAccessKey;
        private static string _awsSecretKey;

        public awsEc2Controller(IOptions<awsSettings> settings)
        {
            _settings = settings.Value;
            _awsAccessKey = _settings.accessKey;
            _awsSecretKey = _settings.secretKey;
        }

        [HttpPost]
        public void stopInstance([FromBody] List<string> instanceId)
        {
            AmazonEC2Client ec2 = new AmazonEC2Client (_awsAccessKey, _awsSecretKey, RegionEndpoint.APSoutheast1);
            ec2.StopInstancesAsync(new StopInstancesRequest(instanceId));
        }
    }
}




