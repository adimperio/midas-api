﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace kmbi_core_master.acctngModule.Models
{
    public class coa
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("slug")]
        public String slug { get; set; }

        [BsonElement("code")]
        public int code { get; set; }

        [BsonElement("name")]
        public String name { get; set; }

        [BsonElement("description")]
        public string description { get; set; }

        [BsonElement("statement")]
        public string statement { get; set; }

        [BsonElement("element")]
        public string element { get; set; }

        [BsonElement("type")]
        public string type { get; set; }

        [BsonElement("subsidiary")]
        public subsidiary[] subsidiary { get; set; }

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }
    }


    public class subsidiary
    {
        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("code")]
        public int code { get; set; }

        [BsonElement("description")]
        public string description { get; set; }

        [BsonElement("area")]
        public string area { get; set; }
    }   
}
