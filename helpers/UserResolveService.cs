﻿using kmbi_core_master.coreMaster.Controllers;
using Microsoft.AspNetCore.Http;

namespace kmbi_core_master.helpers
{
    public class UserResolverService
    {
        private readonly IHttpContextAccessor _context;
       
        //private  string uId;
        //private  string uFName;

        public UserResolverService(IHttpContextAccessor context)
        {
            _context = context;
           
            //uId = _context.HttpContext.Session.GetString("userId");
            //uFName = _context.HttpContext.Session.GetString("fullname");
        }

        public void SetUser(string uId, string uFName)
        {
            _context.HttpContext.Session.SetString("userId", uFName);
            _context.HttpContext.Session.SetString("fullname", uFName);
        }

        public uIdentity GetUser()
        {
           
            return new uIdentity
            {
                uUserId = _context.HttpContext.Session.GetString("userId"),
                uFullname = _context.HttpContext.Session.GetString("fullname")
            };
        }

        public class uIdentity
        {
            public string uUserId { get; set; }
            public string uFullname { get; set; }
        }
    }
}
