﻿using kmbi_core_master.coreMaster.Models;
using System;

namespace kmbi_core_master.coreMaster.IRepository
{
    public interface iauthRepository
    {
        void updateEmail(String email, auth p);

        void updatePassword(String email, auth p);

        void updateisVerified(string email, String verToken, auth p);

        users checkVerToken(string email, String verToken);

        users getCredential(String email);

        credentials getInfo(String email);

        users checkPassword(String token);

        void resetPassword(String email, auth p);

    }
}
