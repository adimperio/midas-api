﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Filters;
using Microsoft.AspNetCore.Cors;

namespace kmbi_core_master.coreMaster.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/officers")]
    public class officersController : Controller
    {
        private readonly iofficersRepository _offRepository;
        private readonly iusersRepository _logInfo;
        private string token;

        public officersController(iofficersRepository offRepositor, iusersRepository logInfo)
        {
            this._offRepository = offRepositor;
            _logInfo = logInfo;
        }

        public void logger(string message)
        {
            //Insert Log//
            token = HttpContext.Request.Headers["token"];
            var lInfo = _logInfo.getLoginInfo(token);
            string log = message;
            global.createLog(lInfo.id, lInfo.fullname, lInfo.employment.branch, log);
            //Insert Log//
        }

        // GET: 
        [HttpGet]
        public IEnumerable<officersCenterList> GetAll()
        {
           var officer = _offRepository.allOfficers();
           return officer;

        }

        //GET officer Slug
        [HttpGet("{slug}")]
        public IActionResult getSlug(string slug)
        {

            var item = _offRepository.getOfficerSlug(slug);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        // GET center slug
        [HttpGet("centerSlug={centerSlug}")]
        public IActionResult getCenters(string centerSlug)
        {

            var item = _offRepository.getCenterSlug(centerSlug);
            if (item == null)
            {

                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }


        // GET unit Id
        [HttpGet("unitId={unitId}")]
        public IEnumerable<officersCenterList> getOfficers(string unitId)
        {

            var item = _offRepository.getOfficerunitId(unitId);
            return item.OrderBy(x=>x.designation).ToList();
        }


        // GET center code
        [HttpGet("centerCode={centerCode}")]
        public IActionResult getCenterCode(string centerCode)
        {

            var item = _offRepository.getCenterCode(centerCode);
            if (item == null)
            {

                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }


        // SEARCH officer name

        [HttpGet("searchOfficer")]
        public IActionResult searchOfficer([FromQuery]string name = "")
        {

            var item = _offRepository.searchOfficer(name);
            if (item.Count() == 0)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }


        // SEARCH center name

        [HttpGet("searchCenter")]
        public IActionResult searchCenter([FromQuery]string code = "")
        {

            var item = _offRepository.searchCenter(code);
            if (item.Count() == 0)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        // POST officer
        [errorHandler]
        [HttpPost]
        public IActionResult createOfficer([FromBody] officers b)
        {
           
            _offRepository.addOfficer(b);

            logger("Insert new officer " + b.name);

            return Ok(new
            {
                type = "success",
                message = b.designation + " created successfully.",
                slug = b.slug
            });

        }


        // POST center
        [HttpPost("centers/{slug}")]
        public IActionResult addCenters([FromBody] centers u, string slug)
        {
            var item = _offRepository.getOfficerSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _offRepository.addCenter(u, slug);

           logger("Insert new center " + item.name);

            return Ok(new
            {
                type = "success",
                message = u.code + " created successfully.",
                slug = u.slug,
                id = u.Id
            });
        }


        // PUT officer
        [errorHandler]
        [HttpPut("{slug}")]
        public IActionResult updateOfficer(string slug, [FromBody] officers b)
        {
            var item = _offRepository.getOfficerSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _offRepository.updateOfficer(slug, b);
            logger( "Update officer " + b.name);
            return Ok(new
            {
                type = "success",
                message = b.designation + " updated successfully."
            });
        }

        // PUT centers
        [errorHandler]
        [HttpPut("officerSlug={officerSlug}&centerSlug={centerSlug}")]
        public IActionResult updateUnits(string officerSlug, string centerSlug, [FromBody] centers u)
        {
            var item = _offRepository.getCenterSlug(centerSlug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _offRepository.updateCenter(officerSlug, centerSlug, u);

            logger( "Update center " + u.code);

            return Ok(new
            {
                type = "success",
                message = u.code + " updated successfully."
            });

        }


        [HttpDelete("{slug}")]
        public IActionResult deleteOfficer(string slug)
        {
            //GET branchSlug
            var bSlug = _offRepository.allOfficers();

            var item = _offRepository.getOfficerSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _offRepository.removeOfficer(slug);

            logger("Delete officer " + item.name);
            //Insert Deletions
            global.insertDeletion("officers", slug, bSlug.FirstOrDefault().centerInstances.FirstOrDefault().branchSlug);

            return Ok(new
            {
                type = "success",
                message = item.name + " deleted successfully."
            });
        }

        //DELETE units
        [HttpDelete("removeCenter/officerSlug={officerSlug}&centerSlug={centerSlug}")]
        public IActionResult deleteUnits(string officerSlug, string centerSlug)
        {
            var item = _offRepository.getCenterSlug(centerSlug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _offRepository.removeCenter(officerSlug, centerSlug);

            logger("Delete center " + item.centers[0].code);

            return Ok(new
            {
                type = "success",
                message = item.centers[0].code + " deleted successfully."
            });
        }

        [HttpGet("unitId")]
        public IActionResult getByUnit([FromQuery]string id)
        {
            var item = _offRepository.getByUnit(id);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }
            return new ObjectResult(item);
        }
    }
}
