﻿using kmbi_core_master.coreMaster;
using kmbi_core_master.coreMaster.Filters;
using kmbi_core_master.payrollModule.IRepository;
using kmbi_core_master.payrollModule.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System;

namespace kmbi_core_master.payrollModule.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/cutoff")]
    [usersAuth]
    public class cutOffController : Controller
    {
        private readonly icutOffRepository _repository;

        public cutOffController(icutOffRepository repository)
        {
            _repository = repository;
        }

        [HttpGet("{slug}")]
        public IActionResult getCutOff(String slug) {
            var item = _repository.getCutOff(slug);
            if(item!=null) {
                return Ok(item);
            } else {
                return Ok(new{});
            }
        }

        [HttpGet("")]
        public IActionResult getCutOffs() {
            return Ok(_repository.getCutOffs());
        }

        [HttpGet("current")]
        public IActionResult getCurrentCutOff() {
            return Ok(_repository.getCurrentCutOff());
        }

        [HttpPost()]
        public IActionResult createCutOff([FromBody]cutOff cutOff) {
            var now = DateTime.Now;
            cutOff.createdAt = now;
            cutOff.updatedAt = now;
            _repository.createCutOff(cutOff);

            var item = _repository.getCutOff(cutOff.slug);
            if(item!=null) {
                return Ok(new {
                    type="success",
                    message = "Cutoff create successfull"
                });
            } else {
                return BadRequest(new {
                    type="failed",
                    message = "Cutoff create failed"
                });
            }
        }

        [HttpPut("{slug}")]
        public IActionResult updateCutOff(String slug, [FromBody]cutOff cutOff) {
            var now = DateTime.Now;
            cutOff.updatedAt = now;
            _repository.updateCutOff(slug, cutOff);
            var item = _repository.getCutOff(slug);
            if(item!=null) {
                return Ok(new {
                    type="success",
                    message = "Cutoff update successfull"
                });
            } else {
                return BadRequest(new {
                    type="failed",
                    message = "Cutoff update failed"
                });
            }
        }

        [HttpDelete("{slug}")]
        public IActionResult deleteCutOff(String slug) {
            var item = _repository.getCutOff(slug);
            if(item!=null) {
                _repository.deleteCutOff(slug);
                var checkItem = _repository.getCutOff(slug);
                if(checkItem==null) {
                    return Ok(new {
                        type="success",
                        message = "Cutoff delete successfull"
                    });
                } else {
                    return BadRequest(new {
                        type="failed",
                        message = "Cutoff delete failed"
                    });
                }
            } else {
                return BadRequest(new {
                    type="failed",
                    message = "Cutoff not found"
                });
            }
            
            
        }
    }
}
