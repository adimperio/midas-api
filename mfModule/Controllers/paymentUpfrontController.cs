﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Filters;
using Microsoft.AspNetCore.Cors;

namespace kmbi_core_master.coreMaster.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/paymentUpfront")]
    [usersAuth]
    public class paymentUpfrontController : Controller
    {
        private readonly ipaymentUpfrontRepository _Repository;
        private readonly iusersRepository _logInfo;
        private string token;

        public paymentUpfrontController(ipaymentUpfrontRepository Repositor, iusersRepository logInfo)
        {
            this._Repository = Repositor;
            _logInfo = logInfo;
        }

        public void logger(string message)
        {
            //Insert Log//
            token = HttpContext.Request.Headers["token"];
            var lInfo = _logInfo.getLoginInfo(token);
            string log = message;
            global.createLog(lInfo.id, lInfo.fullname, lInfo.employment.branch, log);
            //Insert Log//
        }

        // GET: 
        [HttpGet]
        public IEnumerable<paymentUpfront> GetAll()
        {
            var c = _Repository.all();
            return c;

        }

        //GET id
        [HttpGet("{id}")]
        public IActionResult getId(string id)
        {

            var item = _Repository.getId(id);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        [HttpGet("refId/{refId}")]
        public IActionResult getRefId(string refId)
        {

            var item = _Repository.getRefId(refId);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        [HttpGet("slug/{slug}")]
        public IActionResult getSlug(string slug)
        {

            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        
        [HttpPost]
        //[errorHandler]
        public IActionResult create([FromBody] paymentUpfront b)
        {
            //try
            //{
                _Repository.add(b);

                logger("Insert new upfront fee payment ");

                return Ok(new
                {
                    type = "success",
                    message = "Payment created successfully.",
                    slug = b.slug
                });
            //}
            //catch (Exception x)
            //{
            //    return BadRequest(new
            //    {
            //        type = "error",
            //        message = x.Message
            //    });
            //}


        }


        


        [HttpPut("{id}")]
        public IActionResult update(string id, [FromBody] paymentUpfront b)
        {
            var item = _Repository.getId(id);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _Repository.update(id, b);

           logger("Modified upfront fee payment ");

            return Ok(new
            {
                type = "success",
                message = "Payment updated successfully."
            });
        }



        [HttpPut("slug/{slug}")]
        public IActionResult slugupdate(string slug, [FromBody] paymentUpfront b)
        {
            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _Repository.updateSlug(slug, b);

           logger("Modified upfront fee payment ");

            return Ok(new
            {
                type = "success",
                message = "Payment updated successfully."
            });
        }



        [HttpDelete("{id}")]
        public IActionResult delete(string id)
        {

            var item = _Repository.getId(id);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.remove(id);

            logger("Delete upfront fee payment ");

            //Insert Deletions
            global.insertDeletion("paymentUpfront", item.slug, item.loan.branch.slug);

            return Ok(new
            {
                type = "success",
                message = "Payment deleted successfully."
            });
        }


        [HttpDelete("slug/{slug}")]
        public IActionResult slugdelete(string slug)
        {

            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.removeSlug(slug);

            logger( "Delete upfront fee payment ");

            //Insert Deletions
            global.insertDeletion("paymentUpfront", slug, item.loan.branch.slug);

            return Ok(new
            {
                type = "success",
                message = "Payment deleted successfully."
            });
        }


    }
}
