﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Filters;
using Microsoft.AspNetCore.Cors;
using kmbi_core_master.acctngModule.Interface;
using kmbi_core_master.acctngModule.Models;
using kmbi_core_master.coreMaster;
using kmbi_core_master.coreMaster.IRepository;

namespace kmbi_core_master.acctngModule.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/acctngRfp")]
    [usersAuth]
    public class acctngRfpController : Controller
    {
        private readonly iacctngRfpRepository _Repository;
        private readonly iusersRepository _logInfo;
        private string token;

        public acctngRfpController(iacctngRfpRepository Repositor, iusersRepository logInfo)
        {
            this._Repository = Repositor;
            _logInfo = logInfo;
        }

        public void logger(string message)
        {
            //Insert Log//
            token = HttpContext.Request.Headers["token"];
            var lInfo = _logInfo.getLoginInfo(token);
            string log = message;
            global.createLog(lInfo.id, lInfo.fullname, lInfo.employment.branch, log);
            //Insert Log//
        }

        // GET: 
        [HttpGet]
        public IEnumerable<acctngRfp> GetAll()
        {
            var c = _Repository.all();
            return c;

        }

        //GET rfp Slug
        [HttpGet("{slug}")]
        public IActionResult getSlug(string slug)
        {

            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

       

        [HttpPost]
        public IActionResult create([FromBody] acctngRfp b)
        {

            _Repository.add(b);

            logger("Insert new rfp " + b.number);

            return Ok(new
            {
                type = "success",
                message = "RFP created successfully.",
                slug = b.slug
            });

        }

        // POST credit
        [HttpPost("insertCredit/{rfpSlug}")]
        public IActionResult addCredit([FromBody] rfpCredit u, string rfpSlug)
        {
            _Repository.addCredit(u, rfpSlug);

            logger("Insert new rfp credit " + u.name);

            return Ok(new
            {
                type = "success",
                message = u.name + " created successfully.",
                id = u.Id
            });
        }

        // POST debit
        [HttpPost("insertDebit/{rfpSlug}")]
        public IActionResult addDebit([FromBody] rfpDebit u, string rfpSlug)
        {
            _Repository.addDebit(u, rfpSlug);

            logger("Insert new rfp debit " + u.name);

            return Ok(new
            {
                type = "success",
                message = u.name + " created successfully.",
                id = u.Id
            });
        }

        [HttpPut("{slug}")]
        public IActionResult update(string slug, [FromBody] acctngRfp b)
        {
            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _Repository.update(slug, b);

            logger("Modify rfp " + b.number);

            return Ok(new
            {
                type = "success",
                message = "RFP updated successfully."
            });
        }

        // PUT credit
        [errorHandler]
        [HttpPut("updateCredit/rfpSlug={rfpSlug}&creditId={creditId}")]
        public IActionResult updateCredit(string rfpSlug, string creditId, [FromBody] rfpCredit u)
        {
            var item = _Repository.getCreditId(creditId);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _Repository.updateCredit(rfpSlug, creditId, u);

            logger("Modify rfp credit " + u.name);

            return Ok(new
            {
                type = "success",
                message = u.name + " updated successfully."
            });

        }

        // PUT debit
        [errorHandler]
        [HttpPut("updateDebit/rfpSlug={rfpSlug}&debitId={debitId}")]
        public IActionResult updateDebit(string rfpSlug, string debitId, [FromBody] rfpDebit u)
        {
            var item = _Repository.getDebitId(debitId);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _Repository.updateDebit(rfpSlug, debitId, u);

            logger("Insert new rfp debit " + u.name);

            return Ok(new
            {
                type = "success",
                message = u.name + " updated successfully."
            });

        }

        [HttpDelete("{slug}")]
        public IActionResult delete(string slug)
        {

            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.remove(slug);

            logger("Delete rfp " + item.number);

            return Ok(new
            {
                type = "success",
                message = "RFP deleted successfully."
            });
        }

        //DELETE credit
        [HttpDelete("removeCredit/rfpSlug={rfpSlug}&creditId={creditId}")]
        public IActionResult deleteCredit(string rfpSlug, string creditId)
        {
            var item = _Repository.getCreditId(creditId);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.removeCredit(rfpSlug, creditId);

            logger("delete rfp credit " + item.credit[0].name);

            return Ok(new
            {
                type = "success",
                message = item.credit[0].name + " deleted successfully."
            });
        }


        //DELETE debit
        [HttpDelete("removeDebit/rfpSlug={rfpSlug}&debitId={debitId}")]
        public IActionResult deleteDebit(string rfpSlug, string debitId)
        {
            var item = _Repository.getDebitId(debitId);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.removeDebit(rfpSlug, debitId);

            logger("Delete rfp debit " + item.accounts.debit[0].name);

            return Ok(new
            {
                type = "success",
                message = item.accounts.debit[0].name + " deleted successfully."
            });
        }

    }
}
