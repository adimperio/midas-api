﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Filters;
using Microsoft.AspNetCore.Cors;
using kmbi_core_master.acctngModule.Interface;
using kmbi_core_master.acctngModule.Models;
using kmbi_core_master.coreMaster;
using System;
using kmbi_core_master.coreMaster.IRepository;
using System.Linq;

namespace kmbi_core_master.acctngModule.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/acctngJv")]
    [usersAuth]
    public class acctngJvController : Controller
    {
        private readonly iacctngJvRepository _Repository;
        private readonly iusersRepository _logInfo;
        private string token;

        public acctngJvController(iacctngJvRepository Repositor, iusersRepository logInfo)
        {
            this._Repository = Repositor;
            _logInfo = logInfo;
        }

        public void logger(string message)
        {
            //Insert Log//
            token = HttpContext.Request.Headers["token"];
            var lInfo = _logInfo.getLoginInfo(token);
            string log = message;
            global.createLog(lInfo.id, lInfo.fullname, lInfo.employment.branch, log);
            //Insert Log//
        }

        // GET: 
        [HttpGet]
        public IEnumerable<acctngJv> GetAll()
        {
            var c = _Repository.all();
            return c;

        }

        //GET rfp Slug
        [HttpGet("{slug}")]
        public IActionResult getSlug(string slug)
        {

            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        [HttpGet("list")]
        public IActionResult issuedAt([FromQuery]DateTime dateFrom, [FromQuery]DateTime dateTo)
        {
            var dt = dateTo.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
            var item = _Repository.issuedAt(dateFrom, dt);
            if (item.Count == 0)
            {
                return new JsonStringResult("[]");
            }

            // sort
            var sorted = (from x in item orderby String.Join("",(from y in x.jvNumber.Split("-") select y.PadLeft(5,'0'))) descending select x).ToList();

            return new ObjectResult(sorted);
        }

        [HttpPost]
        public IActionResult create([FromBody] acctngJv b)
        {

            _Repository.add(b);

            logger("Insert new jv " + b.number);

            return Ok(new
            {
                type = "success",
                message = b.particulars + " created successfully.",
                slug = b.slug
            });

        }

        // POST credit
        [HttpPost("insertCredit/{jvSlug}")]
        public IActionResult addCredit([FromBody] jvCredit u, string jvSlug)
        {
            _Repository.addCredit(u, jvSlug);

            logger("Insert new jv credit " + u.name);

            return Ok(new
            {
                type = "success",
                message = u.name + " created successfully.",
                id = u.Id
            });
        }

        // POST debit
        [HttpPost("insertDebit/{jvSlug}")]
        public IActionResult addDebit([FromBody] jvDebit u, string jvSlug)
        {
            _Repository.addDebit(u, jvSlug);

            logger("Insert new jv debit " + u.name);

            return Ok(new
            {
                type = "success",
                message = u.name + " created successfully.",
                id = u.Id
            });
        }

        [HttpPut("{slug}")]
        public IActionResult update(string slug, [FromBody] acctngJv b)
        {
            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _Repository.update(slug, b);

            logger("Modified jv " + b.number);

            return Ok(new
            {
                type = "success",
                message = b.particulars + " updated successfully."
            });
        }

        // PUT credit
        [errorHandler]
        [HttpPut("updateCredit/jvSlug={jvSlug}&creditId={creditId}")]
        public IActionResult updateCredit(string jvSlug, string creditId, [FromBody] jvCredit u)
        {
            var item = _Repository.getCreditId(creditId);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _Repository.updateCredit(jvSlug, creditId, u);

            logger("Modified jv credit " + u.name);

            return Ok(new
            {
                type = "success",
                message = u.name + " updated successfully."
            });

        }

        // PUT debit
        [errorHandler]
        [HttpPut("updateDebit/jvSlug={jvSlug}&debitId={debitId}")]
        public IActionResult updateDebit(string jvSlug, string debitId, [FromBody] jvDebit u)
        {
            var item = _Repository.getDebitId(debitId);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _Repository.updateDebit(jvSlug, debitId, u);

            logger("Modified jv debit " + u.name);

            return Ok(new
            {
                type = "success",
                message = u.name + " updated successfully."
            });

        }

        [HttpDelete("{slug}")]
        public IActionResult delete(string slug)
        {

            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.remove(slug);

            logger("Delete jv  " + item.number);

            return Ok(new
            {
                type = "success",
                message = item.particulars + " deleted successfully."
            });
        }

        //DELETE credit
        [HttpDelete("removeCredit/jvSlug={jvSlug}&creditId={creditId}")]
        public IActionResult deleteCredit(string jvSlug, string creditId)
        {
            var item = _Repository.getCreditId(creditId);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.removeCredit(jvSlug, creditId);

            logger("Delete jv credit " + item.credit[0].name);

            return Ok(new
            {
                type = "success",
                message = item.credit[0].name + " deleted successfully."
            });
        }


        //DELETE debit
        [HttpDelete("removeDebit/jvSlug={jvSlug}&debitId={debitId}")]
        public IActionResult deleteDebit(string jvSlug, string debitId)
        {
            var item = _Repository.getDebitId(debitId);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.removeDebit(jvSlug, debitId);

            logger("Delete jv debit " + item.accounts.debit[0].name);

            return Ok(new
            {
                type = "success",
                message = item.accounts.debit[0].name + " deleted successfully."
            });
        }


        [HttpGet("jvNo/{orNo}")]
        public IActionResult checkOr(string orNo)
        {

            var item = _Repository.checkOr(orNo);
            if (item == null)
            {
                return Ok(new
                {
                    existing = 0
                });
            }

            return Ok(new
            {
                existing = 1
            });
        }

        [HttpPut("cancelled/{slug}")]
        public IActionResult updateIsCancelled(string slug, [FromBody] acctngJv b)
        {
            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _Repository.updateIsCancelled(slug, b);
            return Ok(new
            {
                type = "success",
                message = b.particulars + " cancelled successfully."
            });
        }


        [HttpPut("closed")]
        public IActionResult updateIsClosed([FromQuery]DateTime dateFrom, [FromQuery]DateTime dateTo, [FromBody] acctngJv b)
        {
            var dt = dateTo.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
            _Repository.updateIsClosed(dateFrom, dateTo, b);
            return Ok(new
            {
                type = "success",
                message = b.particulars + " closed successfully."
            });
        }

        [HttpGet("transactionCount/{month}/{year}")]
        public IActionResult transactionCount(Int32 month = 0, Int32 year=0) {
            String[] err = new String []{"",""};
            if(month==0) {
                err[0] += "Month is required.";
            }

            if(year==0) {
                err[1] += "Year is required.";
            }

            if((err[0] + err[1]).Length > 0) {
                return BadRequest(new {
                    type = "error",
                    message = err
                });
            } else {
                var from = Convert.ToDateTime(month.ToString() + "/1/" + year.ToString());
                var to = from.AddMonths(1).AddDays(-1);

                var jvList = _Repository.all();
                var count = 0;
                foreach(acctngJv jv in jvList) {
                    if(jv.timestamps.issuedAt >= from && jv.timestamps.issuedAt <= to) {
                        count++;
                    }
                }
                return Ok(new {
                    count = count
                });
            }
        }
    }
}
