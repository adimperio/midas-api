﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.coreMaster.Models
{

    public class cbuWithdrawal

    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("members")]
        public cbuMembers[] members { get; set; }

        [BsonElement("rfp")]
        public cbuRfp rfp { get; set; }

        [BsonElement("cv")]
        public cbuCv cv { get; set; }

        [BsonElement("branch")]
        public cbuBranch branch { get; set; }

        [BsonElement("center")]
        public cbuCenter center { get; set; }

        [BsonElement("loan")]
        public cbuWLoan loan { get; set; }

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }
    }


    public class cbuRfp
    {
        [BsonElement("number")]
        public string number { get; set; }
        
        [BsonElement("payee")]
        public string payee { get; set; }

        [BsonElement("particulars")]
        public string particulars { get; set; }
        
        [BsonElement("datePrepared")]
        public DateTime? datePrepared { get; set; }
        
        [BsonElement("totalAmount")]
        public double totalAmount { get; set; }
    }

    public class cbuCv
    {
        [BsonElement("number")]
        public string number { get; set; }

        [BsonElement("controlNumber")]
        public string controlNumber { get; set; }

        [BsonElement("payee")]
        public string payee { get; set; }

        [BsonElement("particulars")]
        public string particulars { get; set; }

        [BsonElement("chequeNumber")]
        public string chequeNumber { get; set; }

        [BsonElement("datePrepared")]
        public DateTime? datePrepared { get; set; }

        [BsonElement("bank")]
        public cbuBank bank { get; set; }

        [BsonElement("totalAmount")]
        public double totalAmount { get; set; }
    }


    public class cbuMembers
    {
        [BsonElement("id")]
        public string id { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("cbuBalance")]
        public double cbuBalance { get; set; }

        [BsonElement("amount")]
        public double amount { get; set; }

        [BsonElement("reason")]
        public string reason { get; set; }

        [BsonElement("remarks")]
        public string remarks { get; set; }

    }

    public class cbuBank
    {
        [BsonElement("id")]
        public string id { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("name")]
        public string name { get; set; }
    }
    public class cbuBranch
    {
        [BsonElement("id")]
        public string id { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("name")]
        public string name { get; set; }
    }
    public class cbuCenter
    {
        [BsonElement("id")]
        public string id { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("name")]
        public string name { get; set; }
        public string address {get;set;}
    }

    public class cbuWLoan
    {
        [BsonElement("id")]
        public string id { get; set; }

        [BsonElement("refId")]
        public string refId { get; set; }

        [BsonElement("type")]
        public string type { get; set; }
    }


    public class syncCbuWithdrawal

    {
        [BsonId]
        public ObjectId Id { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("members")]
        public cbuMembers[] members { get; set; }

        [BsonElement("rfp")]
        public cbuRfp rfp { get; set; }

        [BsonElement("cv")]
        public cbuCv cv { get; set; }

        [BsonElement("branch")]
        public cbuBranch branch { get; set; }

        [BsonElement("center")]
        public cbuCenter center { get; set; }

        [BsonElement("loan")]
        public cbuWLoan loan { get; set; }

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }
    }
}
