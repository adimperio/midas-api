﻿using kmbi_core_master.coreMaster.Controllers;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.spm.Models;
using kmbi_core_master_v2.trainingModule.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.trainingModule.IRepository
{
    public interface itrainingRepository
    {
        IEnumerable<trainings> getAll();

        trainings getBySlug(String slug);

        void add(trainings b);

        void update(String slug, trainings b);

        void remove(string slug);

    }
}
