using System;
using kmbi_core_master.coreMaster.Models;

namespace kmbi_core_master.reports.UseCases.GeneratePPIData
{
    public class GeneratePPIDataRequest {
        public clients[] clients {get;set;}
        public DateTime AsOf {get;set;}
    }


}