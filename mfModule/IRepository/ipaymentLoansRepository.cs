﻿using kmbi_core_master.coreMaster.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.coreMaster.IRepository
{
    public interface ipaymentLoansRepository
    {
        IEnumerable<paymentLoans> all();

        paymentLoans getSlug(String slug);

        paymentLoans getId(String id);

        //IEnumerable<paymentLoans> getIdWeekNo(String i,/ int? weekNo);

        IEnumerable<paymentLoans> getIdWeekNo(String id, string memberSlug, int? weekNo);

        IEnumerable<paymentLoans> getIdWeekNo(String id);

        void add(paymentLoans b);

        void update(String id, paymentLoans b);

        void updateSlug(String slug, paymentLoans b);

        void remove(String id);

        void removeSlug(String slug);

        List<paymentLoansWithSched> paidAt(DateTime p, string branchSlug);

        List<paymentLoans> getPayment(List<string> refId);

    }
}
