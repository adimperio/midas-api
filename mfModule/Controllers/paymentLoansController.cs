﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Filters;
using Microsoft.AspNetCore.Cors;
using System;

namespace kmbi_core_master.coreMaster.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/paymentLoans")]
    [usersAuth]
    public class paymentLoansController : Controller
    {
        private readonly ipaymentLoansRepository _Repository;
        private readonly iusersRepository _logInfo;
        private string token;

        public paymentLoansController(ipaymentLoansRepository Repositor, iusersRepository logInfo)
        {
            this._Repository = Repositor;
            _logInfo = logInfo;
        }

        public void logger(string message)
        {
            //Insert Log//
            token = HttpContext.Request.Headers["token"];
            var lInfo = _logInfo.getLoginInfo(token);
            string log = message;
            global.createLog(lInfo.id, lInfo.fullname, lInfo.employment.branch, log);
            //Insert Log//
        }

        // GET: 
        [HttpGet]
        public IEnumerable<paymentLoans> GetAll()
        {
            var c = _Repository.all();
            return c;

        }

        //GET id
        [HttpGet("{id}")]
        public IActionResult getId(string id)
        {

            var item = _Repository.getId(id);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }
                
        [HttpGet("slug/{slug}")]
        public IActionResult getSlug(string slug)
        {

            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        //[HttpGet("get")]
        //public IActionResult getIdWeekNo([FromQuery] string loan, [FromQuery] int? term)
        //{

        //    var item = _Repository.getIdWeekNo(loan, term);
        //    if (item == null)
        //    {
        //        return new JsonStringResult("[]");
        //    }

        //    return new ObjectResult(item);
        //}

        [HttpGet("getMember")]
        public IActionResult getIdWeekNo([FromQuery] string loan, [FromQuery] string slug, [FromQuery] int? term)
        {

            var item = _Repository.getIdWeekNo(loan, slug, term);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        [HttpGet("getId")]
        public IActionResult getIdWeekNo([FromQuery] string loan)
        {

            var item = _Repository.getIdWeekNo(loan);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }


        [HttpPost]
        //[errorHandler]
        public IActionResult create([FromBody] paymentLoans b)
        {
            //try
            //{
                _Repository.add(b);

                logger( "Insert new payment ");

                return Ok(new
                {
                    type = "success",
                    message = "Payment created successfully."
                    // slug = b.slug
                });
            
        }

        [HttpPut("{id}")]
        public IActionResult update(string id, [FromBody] paymentLoans b)
        {
            var item = _Repository.getId(id);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _Repository.update(id, b);

            logger("Modified payment ");

            return Ok(new
            {
                type = "success",
                message = "Payment updated successfully."
            });
        }



        [HttpPut("slug/{slug}")]
        public IActionResult slugupdate(string slug, [FromBody] paymentLoans b)
        {
            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _Repository.updateSlug(slug, b);
            logger("Modified payment ");

            return Ok(new
            {
                type = "success",
                message = "Payment updated successfully."
            });
        }



        [HttpDelete("{id}")]
        public IActionResult delete(string id)
        {

            var item = _Repository.getId(id);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.remove(id);

            logger("Delete payment ");

            //Insert Deletions
            global.insertDeletion("paymentLoans", item.slug, item.loan.branch.slug);

            return Ok(new
            {
                type = "success",
                message = "Payment deleted successfully."
            });
        }


        [HttpDelete("slug/{slug}")]
        public IActionResult slugdelete(string slug)
        {

            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.removeSlug(slug);

            logger("Delete payment ");

            //Insert Deletions
            global.insertDeletion("paymentLoans", slug, item.loan.branch.slug);

            return Ok(new
            {
                type = "success",
                message = "Payment deleted successfully."
            });
        }


        [HttpGet("paidAt")]
        public IActionResult paidAt([FromQuery] DateTime date, [FromQuery] string branchSlug)
        {

            var item = _Repository.paidAt(date, branchSlug);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        [HttpPost("listPayment")]
        //[errorHandler]
        public IActionResult getPayment([FromBody] List<string> refId)
        {

            var item = _Repository.getPayment(refId);
            if (item.Count == 0)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);

        }
    }
}
