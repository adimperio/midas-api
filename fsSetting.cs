﻿using System;
using System.Collections.Generic;

namespace kmbi_core_master
{
    public class fsSetting {
        public balanceSheetSettings balanceSheetSettings {get;set;}
        public incomeStatementSettings incomeStatementSettings {get;set;}
    }
    public class balanceSheetSettings {
        public String name {get;set;}
        public balanceSheetSettingsDetailLevel1[] details {get;set;}
    }

    public class balanceSheetSettingsDetailLevel1 {
        public String name {get;set;}
        public balanceSheetSettingsDetailLevel2[] details {get;set;}
    }

    public class balanceSheetSettingsDetailLevel2 {
        public String name {get;set;}
        public balanceSheetSettingsDetailLevel3[] details {get;set;}
    }

    public class balanceSheetSettingsDetailLevel3 {
        public String name {get;set;} 
        public Int32 balanceYear {get;set;}
        public Double balance {get;set;}
        public Int32[] accountCodes {get;set;}
    }

    public class incomeStatementSettings {
        public String name {get;set;}
        public incomeStatementSettingsDetailLevel1[] details {get;set;}
    }

    public class incomeStatementSettingsDetailLevel1 {
        public String name {get;set;}
        public incomeStatementSettingsDetailLevel2[] details {get;set;}
    }

    public class incomeStatementSettingsDetailLevel2 {
        public String name {get;set;}
        public incomeStatementSettingsDetailLevel3[] details {get;set;}
    }

    public class incomeStatementSettingsDetailLevel3 {
        public String name {get;set;} 
        public Int32 balanceYear {get;set;}
        public Double balance {get;set;}
        public Int32[] accountCodes {get;set;}
    }
}
