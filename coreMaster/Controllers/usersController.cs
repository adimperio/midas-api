﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Filters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;

using System.IO;

using kmbi_core_master.helpers.interfaces;
using Microsoft.AspNetCore.Cors;
using System.Net.Http;
using Microsoft.Extensions.Options;
// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace kmbi_core_master.coreMaster.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/users")]
    [usersAuth]
    public class usersController : Controller
    {
        private readonly iusersRepository _usersRepository;
        private readonly iNotification _email;
        private readonly ihtmlBuilder _htmlbuilder;
        private readonly IHostingEnvironment _env;
        private readonly UrlGetter _urlGetter;
        private string token;

        public usersController(iusersRepository usersRepository, IHostingEnvironment env, iNotification email, ihtmlBuilder htmlBuilder, UrlGetter urlGetter)
        {
            this._usersRepository = usersRepository;
            this._env = env;
            this._email = email;
            this._htmlbuilder = htmlBuilder;
            _urlGetter = urlGetter;
    }

        public void logger(String message)
        {
            // global.log(message, 1, "link here");

            //Insert Log//
            token = HttpContext.Request.Headers["token"];
            var lInfo = _usersRepository.getLoginInfo(token);
            string log = message;
            global.createLog(lInfo.id, lInfo.fullname, lInfo.employment.branch, log);
            //Insert Log//
        }

        // GET: 
        [HttpGet]
        public IEnumerable<users> GetAll()
        {
            var users = _usersRepository.AllUsers();
            return users;
        }

        
        // GET id
        [HttpGet("{id:length(24)}")]
        public IActionResult Get(string id)
        {
            var item = _usersRepository.Get(id);
            if (item == null)
            {
                //var msg = "{\"type\":\"error\",\"message\":\"No record found.\"}";
                //return handleBadRequest.badRequest(msg);
                return Ok(new { });
            }

            return new ObjectResult(item);
        }

        
        [HttpGet("{slug}")]
        public IActionResult getSlug(string slug)
        {

            var item = _usersRepository.getSlug(slug);
            if (item == null)
            {
                return Ok(new { });
            }

            return new ObjectResult(item);
        }

        [HttpGetAttribute("getBySlug/{slug}")]
        public IActionResult getBySlug(string slug)
        {

            var item = _usersRepository.getBySlug(slug);
            if (item == null)
            {
                return Ok(new { });
            }
           
            return new ObjectResult(item);
        }


        [HttpGet("permissions/{slug}")]
        public IActionResult getPermission(string slug)
        {

            var item = _usersRepository.getPermission(slug);
            if (item == null)
            {
                return Ok(new { });
            }

            return new ObjectResult(item.permissions);
        }

        // SEARCH fullname
        [HttpGet("searchName")]
        public IActionResult searchName([FromQuery]string name="")
        {

            var item = _usersRepository.searchName(name);
            if (item.Count() == 0)
            {
                //return new JsonStringResult("{\"type\":\"info\",\"message\":\"No record found.\"}");
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        // SEARCH email
        [HttpGet("searchEmail")]
        public IActionResult eMail([FromQuery]string email)
        {

            var item = _usersRepository.eMail(email);
            if (item.Count() == 0)
            {
                // return new JsonStringResult("{\"type\":\"info\",\"message\":\"No record found.\"}");
                return Ok( new { });
            }

            return new ObjectResult(item);
        }

        // GET check email if exist
        [HttpGet("checkEmail")]
        public IActionResult checkEmail([FromQuery]string email)
        {

            var item = _usersRepository.checkEmail(email);
            if (item == null)
            {
                return Ok(new
                {
                    available = 1
                });
            }
            
            return Ok(new
            {
                available = 0
            });
        }

        // GET check unique slug
        [HttpGet("checkSlug")]
        public IActionResult checkSlug([FromQuery]string slug)
        {

            var item = _usersRepository.checkSlug(slug);
            if (item == null)
            {
                return Ok(new
                {
                    available = 1
                });
            }

            return Ok(new
            {
                available = 0
            });
        }

        // GET  Verify verToken and Update 
        [HttpGet("verify/{verToken}")]
        public IActionResult updateVer(string verToken)
        {
            var item = _usersRepository.checkVerToken(verToken);
            if (item == null)
            {
                
                return new JsonStringResult("[]");
            }

            _usersRepository.updateVer(verToken);
            return Ok(new
            {
                type = "success",
                message = "Verified"
            });
        }

        //POST 
        [errorHandler]
        [HttpPost]
        //[usersAccess(access: "user-add")]
        public IActionResult CreateUsers([FromBody] users user)
        {
            var email = _usersRepository.checkEmail(user.email);
            if (email != null)
            {
                return Ok(new
                {
                    type = "info",
                    message = "Email already exist"
                });
            }

            _usersRepository.Add(user);

           
            string log = "Insert new Employee " + user.name.first + " " + user.name.last;
            logger(log);

            //email newly created user

            // _email.from = "no-reply@kmbi.org.ph";
            // _email.to = user.email;
            // _email.subject = "IT Department Notification";
            // //_email.message =  @"Hello, " + user.fullname +
            //  //   @"<br><br>" +
            //  //   @"The IT Department has registered your profile and created your account to the system. Before you login and begin working with the application, please confirm your account by clicking on this link:" +
            //  //   @"<br><br>" +
            //  //   @"http://localhost:5000/api/users/verify/" + user.verificationToken;

            //  _email.message = _htmlbuilder.htmlBuild(System.IO.Directory.GetCurrentDirectory() +"/wwwroot/templates/","sampleTemplate.cshtml",new SampleInfo { fullname=user.fullname,verificationToken=user.verificationToken});;



            // iResult res = _email.Send();

            return Ok(new
            {
                type = "success",
                message = user.name.first + " " + user.name.last + " created successfully.",
                slug = user.slug
            });

        }

        // [errorHandler]
        // [HttpPost]
        // public IActionResult CreateUsers([FromBody]users user,[FromBodyAttribute]IFormFile picture)
        // {
        //     var email = _usersRepository.checkEmail(user.email);
        //     if (email != null)
        //     {
        //         return Ok(new
        //         {
        //             type = "info",
        //             message = "Email already exist"
        //         });
        //     }


            
            
        //     _usersRepository.Add(user);



        //     users insertedUser = _usersRepository.getBySlug(user.slug);

        //     UploadPicture(insertedUser.slug,picture);

        //     //Insert Log//
        //     string log = "Insert new user " + user.name.first + " " + user.name.last;
        //     int level = 1;
        //     string link = "link here";
        //     global.log(log, level, link);
        //     //Insert Log//
           
        //     return Ok(new
        //     {
        //         type = "success",
        //         message = user.name.first + " " + user.name.last + " created successfully.",
        //         slug = user.slug
        //     });
        // }
        // PUT 
        [errorHandler]
        [HttpPut("{slug}")]
        public IActionResult Put(string slug, [FromBody] users user)
        {
            var item = _usersRepository.checkSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _usersRepository.Update(slug, user);

            //Insert Log//
           logger( "Update Employee " + user.name.first + " " + user.name.last);
            //Insert Log//

            return Ok(new
            {
                type = "success",
                message = user.name.first + " " + user.name.last + " updated successfully."
            });
        }

        
        [HttpPut("permissions/{slug}")]
        public IActionResult putPermissions(string slug, [FromBody] users user)
        {
            var item = _usersRepository.checkSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _usersRepository.UpdatePermission(slug, user);

          
            //Insert Log//
            string log = "Update " + item.name.first + " 's permissions";
            logger(log);
            //Insert Log//

            return Ok(new
            {
                type = "success",
                message = "Permissions of " + item.name.first + " updated successfully."
            });
        }



        // DELETE

        [HttpDelete("{slug}")]
        public IActionResult DeleteUsers(string slug)
        {

            var item = _usersRepository.checkSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _usersRepository.Remove(slug);

           
            //Insert Log//
            string log = "Delete user " + item.name.first + " " + item.name.last;
            logger(log);
            //Insert Log//

            return Ok(new
            {
                type = "success",
                message = item.name.first + " " + item.name.last + " deleted successfully."
            });
        }



        
        // GET check SSS if exist
        [HttpGet("checkSSS")]
        public IActionResult checkSSS([FromQuery]string sss)
        {

            var item = _usersRepository.checkIfSSSIsExisting(sss);
            
            if (item)
            {
                return Ok(new
                {
                    existing = 1
                });
            }
            
            return Ok(new
            {
                existing = 0
            });
        }

        // GET check Pagibig if exist
        [HttpGet("checkPagibig")]
        public IActionResult checkPagibig([FromQuery]string pagibig)
        {

            var item = _usersRepository.checkIfPagibigIsExisting(pagibig);
            
            if (item)
            {
                return Ok(new
                {
                    existing = 1
                });
            }
            
            return Ok(new
            {
                existing = 0
            });
        }

        // GET check Philhealth if exist
        [HttpGet("checkPhilhealth")]
        public IActionResult checkPhilhealth([FromQuery]string philhealth)
        {

            var item = _usersRepository.checkIfPhilhealthIsExisting(philhealth);
            
            if (item)
            {
                return Ok(new
                {
                    existing = 1
                });
            }
            
            return Ok(new
            {
                existing = 0
            });
        }

        // GET check Employee Code if exist
        [HttpGet("checkEmployeeCode")]
        public IActionResult checkEmployeeCode([FromQuery]string employeeCode)
        {

            var item = _usersRepository.checkIfEmployeeCodeIsExisting(employeeCode);
            
            if (item)
            {
                return Ok(new
                {
                    existing = 1
                });
            }
            
            return Ok(new
            {
                existing = 0
            });
        }

        // GET check TIN if exist
        [HttpGet("checkTIN")]
        public IActionResult checkTIN([FromQuery]string tin)
        {

            var item = _usersRepository.checkIfTINIsExisting(tin);
            
            if (item)
            {
                return Ok(new
                {
                    existing = 1
                });
            }
            
            return Ok(new
            {
                existing = 0
            });
        }

        // GET check Docket Number if exist
        [HttpGet("checkDocketNumber")]
        public IActionResult checkDocketNumber([FromQuery]string docketNumber)
        {

            var item = _usersRepository.checkIfDocketNumberIsExisting(docketNumber);
            
            if (item)
            {
                return Ok(new
                {
                    existing = 1
                });
            }
            
            return Ok(new
            {
                existing = 0
            });
        }

        //hr


        [HttpGetAttribute("{slug}/addresses")]
        public IActionResult GetAddresses(String slug)
        {
            return Ok(_usersRepository.GetAddresses(slug));    
        }

        [HttpGetAttribute("{employeeSlug}/addresses/{addressSlug}")]
        public IActionResult GetAddress(String employeeSlug, String addressSlug)
        {
            return Ok(_usersRepository.GetAddress(employeeSlug, addressSlug));
        }

        [HttpGetAttribute("{employeeSlug}/contacts")]
        public IActionResult GetContacts(String employeeSlug)
        {
            return Ok(_usersRepository.GetContacts(employeeSlug));
        }

        [HttpGetAttribute("{employeeSlug}/contacts/{contactSlug}")]
        public IActionResult GetContact(String employeeSlug, String contactSlug)
        {
            return Ok(_usersRepository.GetContact(employeeSlug, contactSlug));
        }

        [HttpGetAttribute("{employeeSlug}/skills")]
        public IActionResult GetSkills(String employeeSlug)
        {
            return Ok(_usersRepository.GetSkills(employeeSlug));
        }

        [HttpGetAttribute("{employeeSlug}/skills/{skill}")]
        public IActionResult GetSkill(String employeeSlug, String skill)
        {
            return Ok(_usersRepository.GetSkill(employeeSlug, skill));
        }

        [HttpGetAttribute("{employeeSlug}/allowances")]
        public IActionResult GetAllowances(String employeeSlug)
        {
            return Ok(_usersRepository.GetAllowances(employeeSlug));
        }

        [HttpGetAttribute("{employeeSlug}/allowances/{allowanceSlug}")]
        public IActionResult GetAllowance(String employeeSlug, String allowanceSlug)
        {
            return Ok(_usersRepository.GetAllowance(employeeSlug, allowanceSlug));
        }

        [HttpGetAttribute("{employeeSlug}/benefits")]
        public IActionResult GetBenefits(String employeeSlug)
        {
            return Ok(_usersRepository.GetBenefits(employeeSlug));
        }

        [HttpGetAttribute("{employeeSlug}/benefits/{benefitSlug}")]
        public IActionResult GetBenefit(String employeeslug, String benefitSlug)
        {
            return Ok(_usersRepository.GetBenefit(employeeslug, benefitSlug));
        }

        [HttpGetAttribute("{employeeSlug}/family")]
        public IActionResult GetFamily(String employeeSlug)
        {
            return Ok(_usersRepository.GetFamily(employeeSlug));
        }

        [HttpGetAttribute("{employeeSlug}/family/{familyMemberSlug}")]
        public IActionResult GetFamilyMember(String employeeSlug, String familyMemberSlug)
        {
            return Ok(_usersRepository.GetFamilyMember(employeeSlug, familyMemberSlug));
        }

        [HttpGetAttribute("{employeeSlug}/trainings")]
        public IActionResult GetTrainings(String employeeSlug)
        {
            return Ok(_usersRepository.GetTrainings(employeeSlug));
        }

        [HttpGetAttribute("{employeeSlug}/trainings/{trainingSlug}")]
        public IActionResult GetTraining(String employeeSlug, String trainingSlug)
        {
            return Ok(_usersRepository.GetTraining(employeeSlug, trainingSlug));
        }

        [HttpGetAttribute("{employeeSlug}/educations")]
        public IActionResult GetEducations(String employeeSlug)
        {
            return Ok(_usersRepository.GetEducations(employeeSlug));
        }


        [HttpGetAttribute("{employeeSlug}/educations/{educationSlug}")]
        public IActionResult GetEducation(String employeeSlug, String educationSlug)
        {
            return Ok(_usersRepository.GetEducation(employeeSlug, educationSlug));
        }

        [HttpGetAttribute("{employeeSlug}/previousEmployments")]
        public IActionResult GetPreviousEmployments(String employeeSlug)
        {
            return Ok(_usersRepository.GetPreviousEmployments(employeeSlug));
        }

        [HttpGetAttribute("{employeeSlug}/previousEmployments/{previousEmploymentSlug}")]
        public IActionResult GetPreviousEmployment(String employeeSlug, String previousEmploymentSlug)
        {
            return Ok(_usersRepository.GetPreviousEmployment(employeeSlug, previousEmploymentSlug));
        }

        [HttpGetAttribute("{employeeSlug}/employeeRelations")]
        public IActionResult GetEmployeeRelations(String employeeSlug)
        {
            return Ok(_usersRepository.GetEmployeeRelations(employeeSlug));
        }

        [HttpGetAttribute("{employeeSlug}/employeeRelations/{employeeRelationSlug}")]
        public IActionResult GetEmployeeRelation(String employeeSlug, String employeeRelationSlug)
        {
            return Ok(_usersRepository.GetEmployeeRelation(employeeSlug, employeeRelationSlug));
        }

        [HttpGetAttribute("{employeeSlug}/employeeSchedules/{employeeScheduleSlug}")]
        public IActionResult GetEmployeeSchedule(String employeeSlug, String employeeScheduleSlug)
        {
            return Ok(_usersRepository.GetEmployeeSchedule(employeeSlug, employeeScheduleSlug));
        }

        [HttpGetAttribute("{employeeSlug}/employeeSchedules")]
        public IActionResult GetEmployeeSchedules(String employeeSlug)
        {
            return Ok(_usersRepository.GetEmployeeSchedules(employeeSlug));
        }

        [HttpGetAttribute("{employeeSlug}/emergencyContactPersons/{emergencyContactPersonSlug}")]
        public IActionResult GetEmergencyContactPerson(String employeeSlug, String emergencyContactPersonSlug)
        {
            return Ok(_usersRepository.GetEmergencyContactPerson(employeeSlug, emergencyContactPersonSlug));
        }

        [HttpGetAttribute("{employeeSlug}/emergencyContactPersons")]
        public IActionResult GetEmergencyContactPersons(String employeeSlug)
        {
            return Ok(_usersRepository.GetEmergencyContactPersons(employeeSlug));
        }

        [HttpGetAttribute("{employeeSlug}/languagesSpoken/{languageSpokenSlug}")]
        public IActionResult GetLanguageSpoken(String employeeSlug, String languageSpokenSlug)
        {
            return Ok(_usersRepository.GetLanguageSpoken(employeeSlug, languageSpokenSlug));
        }

        [HttpGetAttribute("{employeeSlug}/languagesSpoken")]
        public IActionResult GetLanguagesSpoken(String employeeSlug)
        {
            return Ok(_usersRepository.GetLanguagesSpoken(employeeSlug));
        }

        [HttpGetAttribute("{employeeSlug}/employmentHistory/{employmentHistoryItemSlug}")]
        public IActionResult GetEmploymentHistoryItem(String employeeSlug, String employmentHistoryItemSlug)
        {
            return Ok(_usersRepository.GetEmploymentHistoryItem(employeeSlug, employmentHistoryItemSlug));
        }

        [HttpGetAttribute("{employeeSlug}/employmentHistory")]
        public IActionResult GetEmploymentHistory(String employeeSlug)
        {
            return Ok(_usersRepository.GetEmploymentHistory(employeeSlug));
        }

        [HttpPostAttribute("{employeeSlug}/addresses")]
        public IActionResult PostAddress(String employeeSlug, [FromBodyAttribute]address address)
        {
            Boolean IsSuccess = _usersRepository.InsertAddress(employeeSlug, address);

            if(IsSuccess)
            {
                string log = "Insert new Address " + address.slug;
                logger(log);

                return Ok(new {type="success", message="Employee address created successfully."});
            }
            else
            {
                return Ok(new {type="error", message="Employee address create failed."});
            }
        }

        [HttpPostAttribute("{employeeSlug}/contacts")]
        public IActionResult PostContact(String employeeSlug, [FromBodyAttribute]contact contact)
        {
            Boolean IsSuccess = _usersRepository.InsertContact(employeeSlug, contact);
            if(IsSuccess)
            {
                string log = "Insert new Contact " + contact.slug;
                logger(log);
                return Ok(new {type="success", message="Employee contact created successfully."});
            }
            else
            {
                return Ok(new {type="error", message="Employee contact create failed."});
            }
        }

        [HttpPostAttribute("{employeeSlug}/skills")]
        public IActionResult PostSkill(String employeeSlug, [FromBodyAttribute]skill skill)
        {
            Boolean IsSuccess = _usersRepository.InsertSkill(employeeSlug, skill);
            if(IsSuccess)
            {
                string log = "Insert new Skill " + skill;
                logger(log);
                return Ok(new {type="success", message="Employee skill created successfully."});
            }
            else
            {
                return Ok(new {type="error", message="Employee skill create failed."});
            }
        }

        [HttpPostAttribute("{employeeSlug}/allowances")]
        public IActionResult PostAllowance(String employeeSlug, [FromBodyAttribute]allowance allowance)
        {
            Boolean IsSuccess = _usersRepository.InsertAllowance(employeeSlug, allowance);
            if(IsSuccess)
            {
                string log = "Insert new allowance " + allowance.slug;
                logger(log);
                return Ok(new {type="success", message="Employee allowance created successfully."});
            }
            else
            {
                return Ok(new {type="error", message="Employee allowance create failed."});
            }
        }

        [HttpPostAttribute("{employeeSlug}/benefits")]
        public IActionResult PostBenefit(String employeeSlug, [FromBodyAttribute]benefit benefit)
        {
            Boolean IsSuccess = _usersRepository.InsertBenefit(employeeSlug, benefit);
            if(IsSuccess)
            {
                string log = "Insert new benefit [" + benefit.slug + "]";
                logger(log);
                return Ok(new {type="success", message="Employee benefit created successfully."});
            }
            else
            {
                return Ok(new {type="error", message="Employee benefit create failed."});
            }
        }

        [HttpPostAttribute("{employeeSlug}/family")]
        public IActionResult PostFamily(String employeeSlug, [FromBodyAttribute]familyMember familyMember)
        {
            Boolean IsSuccess = _usersRepository.InsertFamily(employeeSlug, familyMember);
            if(IsSuccess)
            {
                string log= "Insert new familyMember [" + familyMember.slug + "]";
                logger(log);
                return Ok(new {type="success", message="Employee family created successfully."});
            }
            else
            {
                return Ok(new {type="error", message="Employee family create failed."});
            }
        }

        [HttpPostAttribute("{employeeSlug}/trainings")]
        public IActionResult PostTraining(String employeeSlug, [FromBodyAttribute]training training)
        {
            Boolean IsSuccess = _usersRepository.InsertTraining(employeeSlug, training);
            if(IsSuccess)
            {
                string log = "Insert new training [" + training.slug + "]";
                logger(log);
                return Ok(new {type="success", message="Employee training created successfully."});
            }
            else
            {
                return Ok(new {type="error", message="Employee training create failed."});
            }
            
            
        }

        [HttpPostAttribute("{employeeSlug}/educations")]
        public IActionResult PostEducation(String employeeSlug, [FromBodyAttribute]education education)
        {
            Boolean IsSuccess = _usersRepository.InsertEducation(employeeSlug, education);
            if(IsSuccess)
            {
                string log = "Insert new education [" + education.slug + "]";
                logger(log);
                return Ok(new {type="success", message="Employee education created successfully."});
            }
            else
            {
                return Ok(new {type="error", message="Employee education create failed."});
            }
        }

        [HttpPostAttribute("{employeeSlug}/previousEmployments")]
        public IActionResult PostPreviousEmployment(String employeeSlug, [FromBodyAttribute]previousEmployment previousEmployment)
        {
            Boolean IsSuccess = _usersRepository.InsertPreviousEmployment(employeeSlug, previousEmployment);
            if(IsSuccess)
            {
                string log= "Insert new previousEmployment [" + previousEmployment.slug + "]";
                logger(log);
                return Ok(new {type="success", message="Employee Previous employment created successfully."});
            }
            else
            {
                return Ok(new {type="error", message="Employee Previous employment create failed."});
            }
        }

        [HttpPostAttribute("{employeeSlug}/employeeRelations")]
        public IActionResult PostEmployeeRelation(String employeeSlug, [FromBodyAttribute]employeeRelation employeeRelation)
        {
            Boolean IsSuccess = _usersRepository.InsertEmployeeRelation(employeeSlug, employeeRelation);
            if(IsSuccess)
            {
                return Ok(new {type="success", message="Employee Previous employment created successfully."});
            }
            else
            {
                return Ok(new {type="error", message="Employee Previous employment create failed."});
            }
        }

        [HttpPostAttribute("{employeeSlug}/employeeSchedules")]
        public IActionResult PostEmployeeSchedule(String employeeSlug, [FromBodyAttribute]employeeSchedule employeeSchedule)
        {
            Boolean IsSuccess = _usersRepository.InsertEmployeeSchedule(employeeSlug, employeeSchedule);
            if(IsSuccess)
            {
                return Ok(new {type="success", message="Employee schedule created successfully."});
            }
            else
            {
                return Ok(new {type="error", message="Employee schedule create failed."});
            }
        }

        [HttpPostAttribute("{employeeSlug}/emergencyContactPersons")]
        public IActionResult PostEmergencyContactPerson(String employeeSlug, [FromBodyAttribute]emergencyContactPerson emergencyContactPerson)
        {
            Boolean IsSuccess = _usersRepository.InsertEmergencyContactPerson(employeeSlug, emergencyContactPerson);
            if(IsSuccess)
            {
                return Ok(new {type="success", message="Employee Emergency Contact Person created successfully."});
            }
            else
            {
                return Ok(new {type="error", message="Employee Emergency Contact Person create failed."});
            }
        }

        [HttpPostAttribute("{employeeSlug}/languagesSpoken")]
        public IActionResult PostLanguageSpoken(String employeeSlug, [FromBodyAttribute]language language)
        {
            Boolean IsSuccess = _usersRepository.InsertLanguageSpoken(employeeSlug, language);
            if(IsSuccess)
            {
                return Ok(new {type="success", message="Employee Language Spoken created successfully."});
            }
            else
            {
                return Ok(new {type="error", message="Employee Language Spoken Person create failed."});
            }
        }

        [HttpPostAttribute("{employeeSlug}/employmentHistory")]
        public IActionResult PostEmploymentHistory(String employeeSlug, [FromBodyAttribute]employmentHistoryItem employmentHistoryItem)
        {
            Boolean IsSuccess = _usersRepository.InsertEmploymentHistoryItem(employeeSlug, employmentHistoryItem);
            if(IsSuccess)
            {
                return Ok(new {type="success", message="Employee Employment History created successfully."});
            }
            else
            {
                return Ok(new {type="error", message="Employee Employment History Person create failed."});
            }
        }
        
        [HttpPutAttribute("{employeeSlug}/addresses/{addressSlug}")]
        public IActionResult PutAddress(String employeeSlug, String addressSlug, [FromBodyAttribute]address address)
        {
            Boolean IsSuccess = _usersRepository.UpdateAddress(employeeSlug, addressSlug, address);
            if(IsSuccess)
            {
                string log ="Modified employee address [" + address.slug + "]";
                logger(log);
                return Ok(new {type="success", message="Employee address updated successfully."});
            }
            else
            {
                return Ok(new {type="success", message="Employee address update failed."});
            }
        }

        [HttpPutAttribute("{employeeSlug}/contacts/{contactSlug}")]
        public IActionResult PutContact(String employeeSlug, String contactSlug, [FromBodyAttribute]contact contact)
        {
            Boolean IsSuccess = _usersRepository.UpdateContact(employeeSlug, contactSlug, contact);
            if (IsSuccess)
            {
                string log = "Modified employee contact [" + contact.slug + "]";
                logger(log);
                return Ok(new {type="success", message="Employee contact updated successfully."});
            }
            else
            {
                return Ok(new {type="success", message="Employee contact update failed."});
            }
        }

        [HttpPutAttribute("{employeeSlug}/skills/{skillSlug}")]
        public IActionResult PutSkill(String employeeSlug, String skillSlug, [FromBodyAttribute]skill skill)
        {
            Boolean IsSuccess = _usersRepository.UpdateSkill(employeeSlug,skillSlug, skill);
            if (IsSuccess)
            {
                string log = "Modified employee skill [" + skill.slug + "]";
                logger(log);
                return Ok(new {type="success", message="Employee skill updated successfully."});
            }
            else
            {
                return Ok(new {type="success", message="Employee skill update failed."});
            }
        }

        [HttpPutAttribute("{employeeSlug}/allowances/{allowanceSlug}")]
        public IActionResult PutAllowance(String employeeSlug, String allowanceSlug, [FromBodyAttribute]allowance allowance)
        {
            Boolean IsSuccess = _usersRepository.UpdateAllowance(employeeSlug, allowanceSlug, allowance);
            if(IsSuccess)
            {
                string log = "Modified employee allowance [" + allowance.slug + "]";
                logger(log);
                return Ok(new {type="success", message="Employee allowance updated successfully."});
            }
            else
            {
                return Ok(new {type="success", message="Employee allowance update failed."});
            }
        }

        [HttpPutAttribute("{employeeSlug}/benefits/{benefitSlug}")]
        public IActionResult PutBenefit(String employeeSlug, String benefitSlug, [FromBodyAttribute]benefit benefit)
        {
            Boolean IsSuccess = _usersRepository.UpdateBenefit(employeeSlug, benefitSlug, benefit);
            if(IsSuccess)
            {
                string log = "Modified employee benefits [" + benefit.slug + "]";
                logger(log);
                return Ok(new {type="success", message="Employee benefit updated successfully."});
            }
            else
            {
                return Ok(new {type="success", message="Employee benefit update failed."});
            }
        }

        [HttpPutAttribute("{employeeSlug}/family/{familyMemberSlug}")]
        public IActionResult PutFamilyMember(String employeeSlug, String familyMemberSlug, [FromBodyAttribute]familyMember familyMember)
        {
            Boolean IsSuccess = _usersRepository.UpdateFamilyMember(employeeSlug, familyMemberSlug, familyMember);
            if(IsSuccess)
            {
                string log = "Modified employee familyMember [" + familyMember.slug + "]";
                logger(log);
                return Ok(new {type="success", message="Employee family updated successfully."});
            }
            else
            {
                return Ok(new {type="success", message="Employee family update failed."});
            }
        }

        [HttpPutAttribute("{employeeSlug}/trainings/{trainingSlug}")]
        public IActionResult PutTraining(String employeeSlug, String trainingSlug, [FromBodyAttribute]training training)
        {
            Boolean IsSuccess = _usersRepository.UpdateTraining(employeeSlug, trainingSlug, training);
            if(IsSuccess)
            {
                string log = "Modified employee training [" + training.slug + "]";
                logger(log);
                return Ok(new {type="success", message="Employee training updated successfully."});
            }
            else
            {
                return Ok(new {type="error", message="Employee training update failed."});
            }
        }

        [HttpPutAttribute("{employeeSlug}/educations/{educationSlug}")]
        public IActionResult PutEducation(String employeeSlug, String educationSlug, [FromBodyAttribute]education education)
        {
            Boolean IsSuccess = _usersRepository.UpdateEducation(employeeSlug, educationSlug, education);
            if(IsSuccess)
            {
                string log = "Modified employee education [" + education.slug + "]";
                logger(log);
                return Ok(new {type="success", message="Employee training updated successfully."});
            }
            else
            {
                return Ok(new {type="error", message="Employee training update failed."});
            }
        }

        [HttpPutAttribute("{employeeSlug}/previousEmployments/{previousEmploymentSlug}")]
        public IActionResult PutPreviousEmployment(String employeeSlug, String previousEmploymentSlug, [FromBodyAttribute]previousEmployment previousEmployment)
        {
            Boolean IsSuccess = _usersRepository.UpdatePreviousEmployment(employeeSlug, previousEmploymentSlug, previousEmployment);
            if(IsSuccess)
            {
                string log = "Modified employee previousEmployment [" + previousEmploymentSlug + "]";
                logger(log);
                return Ok(new {type="success", message="Employee Previous Employment updated successfully."});
            }
            else
            {
                return Ok(new {type="error", message="Employee Previous Employment update failed."});
            }
        }

        [HttpPutAttribute("{employeeSlug}/employeeRelations/{employeeRelationSlug}")]
        public IActionResult PutEmployeeRelation(String employeeSlug, String employeeRelationSlug, [FromBodyAttribute]employeeRelation employeeRelation)
        {
            Boolean IsSuccess = _usersRepository.UpdateEmployeeRelation(employeeSlug, employeeRelationSlug, employeeRelation);
            if(IsSuccess)
            {
                string log = "Modified employee relation [" + employeeRelationSlug + "]";
                logger(log);
                return Ok(new {type="success", message="Employee employee relation successfully."});
            }
            else
            {
                return Ok(new {type="error", message="Employee employee relation update failed."});
            }    
        }

        [HttpPutAttribute("{employeeSlug}/employeeSchedules/{employeeScheduleSlug}")]
        public IActionResult PutEmployeeSchedule(String employeeSlug, String employeeScheduleSlug, [FromBodyAttribute]employeeSchedule employeeSchedule)
        {
            Boolean IsSuccess = _usersRepository.UpdateEmployeeSchedule(employeeSlug, employeeScheduleSlug, employeeSchedule);
            if(IsSuccess)
            {
                string log = "Modified employee schedule [" + employeeScheduleSlug + "]";
                logger(log);
                return Ok(new {type="success", message="Employee employee schedule successfully."});
            }
            else
            {
                return Ok(new {type="error", message="Employee employee schedule update failed."});
            }   
        }

        [HttpPutAttribute("{employeeSlug}/emergencyContactPersons/{emergencyContactPersonSlug}")]
        public IActionResult PutEmergencyContactPerson(String employeeSlug, String emergencyContactPersonSlug, [FromBodyAttribute]emergencyContactPerson emergencyContactPerson)
        {
            Boolean IsSuccess = _usersRepository.UpdateEmergencyContactPerson(employeeSlug, emergencyContactPersonSlug, emergencyContactPerson);
            if(IsSuccess)
            {
                string log = "Modified employee emergency contact person [" + emergencyContactPersonSlug + "]";
                logger(log);
                return Ok(new {type="success", message="Employee employee emergency contact person successfully."});
            }
            else
            {
                return Ok(new {type="error", message="Employee employee emergency contact person update failed."});
            }   
        }

        [HttpPutAttribute("{employeeSlug}/languagesSpoken/{languageSpokenSlug}")]
        public IActionResult PutLanguageSpoken(String employeeSlug, String languageSpokenSlug, [FromBodyAttribute]language language)
        {
            Boolean IsSuccess = _usersRepository.UpdateLanguageSpoken(employeeSlug, languageSpokenSlug, language);
            if(IsSuccess)
            {
                string log = "Modified employee language spoken person [" + languageSpokenSlug + "]";
                logger(log);
                return Ok(new {type="success", message="Employee employee language spoken successfully."});
            }
            else
            {
                return Ok(new {type="error", message="Employee employee language spoken update failed."});
            }   
        }

        [HttpPutAttribute("{employeeSlug}/employmentHistory/{employmentHistoryItemSlug}")]
        public IActionResult PutEmploymentHistoryItem(String employeeSlug, String employmentHistoryItemSlug, [FromBodyAttribute]employmentHistoryItem employmentHistoryItem)
        {
            Boolean IsSuccess = _usersRepository.UpdateEmploymentHistoryItem(employeeSlug, employmentHistoryItemSlug, employmentHistoryItem);
            if(IsSuccess)
            {
                string log = "Modified employee employment history person [" + employmentHistoryItemSlug + "]";
                logger(log);
                return Ok(new {type="success", message="Employee employee employment history successfully."});
            }
            else
            {
                return Ok(new {type="error", message="Employee employee employment history update failed."});
            }   
        }

        [HttpDeleteAttribute("{employeeSlug}/addresses/{addressSlug}")]
        public IActionResult DeleteAddress(String employeeSlug, String addressSlug)
        {
            Boolean IsSuccess = _usersRepository.DeleteAddress(employeeSlug, addressSlug);
            if(IsSuccess)
            {
                string log = "Delete employee address [" + addressSlug + "]";
                logger(log);
                return Ok(new {type="success", message="Employee address deleted successfully."});
            }
            else
            {
                return Ok(new {type="error", message="Employee address delete failed."});
            }
        }

        [HttpDeleteAttribute("{employeeSlug}/contacts/{contactSlug}")]
        public IActionResult DeleteContact(String employeeSlug, String contactSlug)
        {
            Boolean IsSuccess = _usersRepository.DeleteContact(employeeSlug, contactSlug);
            if(IsSuccess)
            {
                
                string log = "Delete employee contact [" + contactSlug + "]";
                logger(log);
                return Ok(new {type="success", message="Employee contact deleted successfully."});
            }
            else
            {
                return Ok(new {type="error", message="Employee contact delete failed."});
            }
        }

        [HttpDeleteAttribute("{employeeSlug}/skills/{skill}")]
        public IActionResult DeleteSkill(String employeeSlug, String skill)
        {
            Boolean IsSuccess = _usersRepository.DeleteSkill(employeeSlug, skill);
            if(IsSuccess)
            {
                string log = "Delete employee skill [" + skill + "]";
                logger(log);
                return Ok(new {type="success", message="Employee skill deleted successfully."});
            }
            else
            {
                return Ok(new {type="error", message="Employee skill delete failed."});
            }
        }

        [HttpDeleteAttribute("{employeeSlug}/allowances/{allowanceSlug}")]
        public IActionResult DeleteAllowance(String employeeSlug, String allowanceSlug)
        {
            Boolean IsSuccess = _usersRepository.DeleteAllowance(employeeSlug, allowanceSlug);
            if(IsSuccess)
            {
                string log = "Delete employee allowance [" + allowanceSlug + "]";
                logger(log);
                return Ok(new {type="success", message="Employee allowance deleted successfully."});
            }
            else
            {
                return Ok(new {type="error", message="Employee allowance delete failed."});
            }
        }

        [HttpDeleteAttribute("{employeeSlug}/benefits/{benefitSlug}")]
        public IActionResult DeleteBenefit(String employeeSlug, String benefitSlug)
        {
            Boolean IsSuccess = _usersRepository.DeleteBenefit(employeeSlug, benefitSlug);
            if(IsSuccess)
            {
                string log = "Delete employee benefit [" + benefitSlug + "]";
                logger(log);
                return Ok(new {type="success", message="Employee benefit deleted successfully."});
            }
            else
            {
                return Ok(new {type="error", message="Employee benefit delete failed."});
            }
        }

        [HttpDeleteAttribute("{employeeSlug}/family/{familyMemberSlug}")]
        public IActionResult DeleteFamilyMember(String employeeSlug, String familyMemberSlug)
        {
            Boolean IsSuccess = _usersRepository.DeleteFamilyMember(employeeSlug, familyMemberSlug);
            if(IsSuccess)
            {
                string log = "Delete employee familyMember [" + familyMemberSlug + "]";
                logger(log);
                return Ok(new {type="success", message="Employee family deleted successfully."});
            }
            else
            {
                return Ok(new {type="error", message="Employee family delete failed."});
            }
        }

        [HttpDeleteAttribute("{employeeSlug}/trainings/{trainingSlug}")]
        public IActionResult DeleteTraining(String employeeSlug, String trainingSlug)
        {
            Boolean IsSuccess = _usersRepository.DeleteTraining(employeeSlug, trainingSlug);
            if(IsSuccess)
            {
                string log = "Delete employee training [" + trainingSlug + "]";
                logger(log);
                return Ok(new {type="success", message="Employee training deleted successfully."});
            }
            else
            {
                return Ok(new {type="error", message="Employee training delete failed."});
            }
        }

        [HttpDeleteAttribute("{employeeSlug}/educations/{educationSlug}")]
        public IActionResult DeleteEducation(String employeeSlug, String educationSlug)
        {
            Boolean IsSuccess = _usersRepository.DeleteEducation(employeeSlug, educationSlug);
            if(IsSuccess)
            {
                string log = "Delete employee education [" + educationSlug + "]";
                logger(log);
                return Ok(new {type="success", message="Employee education deleted successfully."});
            }
            else
            {
                return Ok(new {type="error", message="Employee education delete failed."});
            }
        }

        [HttpDeleteAttribute("{employeeSlug}/previousEmployments/{previousEmploymentSlug}")]
        public IActionResult DeletePreviousEmployment(String employeeSlug, String previousEmploymentSlug)
        {
            Boolean IsSuccess = _usersRepository.DeletePreviousEmployment(employeeSlug, previousEmploymentSlug);
            if(IsSuccess)
            {
                string log = "Delete employee previousEmployment [" + previousEmploymentSlug + "]";
                logger(log);
                return Ok(new {type="success", message="Employee Previous Employment deleted successfully."});
            }
            else
            {
                return Ok(new {type="error", message="Employee Previous Employment delete failed."});
            }
        }

        [HttpDeleteAttribute("{employeeSlug}/employeeRelations/{employeeRelationSlug}")]
        public IActionResult DeleteEmployeeRelation(String employeeSlug, String employeeRelationSlug)
        {
            Boolean IsSuccess = _usersRepository.DeleteEmployeeRelation(employeeSlug, employeeRelationSlug);
            if (IsSuccess)
            {
                logger("Delete employee relation [" + employeeRelationSlug + "]");
                return Ok(new {type="success", message="Employee relation deleted successfully."});
            }
            else
            {
                return Ok(new {type="error", message="Employee relation delete failed."});
            }
        }

        [HttpDeleteAttribute("{employeeSlug}/employeeSchedules/{employeeScheduleSlug}")]
        public IActionResult DeleteEmployeeSchedule(String employeeSlug, String employeeScheduleSlug)
        {
            Boolean IsSuccess = _usersRepository.DeleteEmployeeSchedule(employeeSlug, employeeScheduleSlug);
            if (IsSuccess)
            {
                logger("Delete employee shedule [" + employeeScheduleSlug + "]");
                return Ok(new {type="success", message="Employee schedule deleted successfully."});
            }
            else
            {
                return Ok(new {type="error", message="Employee schedule delete failed."});
            }
        }

        [HttpDeleteAttribute("{employeeSlug}/emergencyContactPersons/{emergencyContactPersonSlug}")]
        public IActionResult DeleteEmergencyContactPerson(String employeeSlug, String emergencyContactPersonSlug)
        {
            Boolean IsSuccess = _usersRepository.DeleteEmergencyContactPerson(employeeSlug, emergencyContactPersonSlug);
            if (IsSuccess)
            {
                logger("Delete employee emergency contact person [" + emergencyContactPersonSlug + "]");
                return Ok(new {type="success", message="Employee emergency contact person deleted successfully."});
            }
            else
            {
                return Ok(new {type="error", message="Employee emergency contact person delete failed."});
            }
        }

        [HttpDeleteAttribute("{employeeSlug}/languagesSpoken/{languageSpokenSlug}")]
        public IActionResult DeleteLanguageSpoken(String employeeSlug, String languageSpokenSlug)
        {
            Boolean IsSuccess = _usersRepository.DeleteLanguageSpoken(employeeSlug, languageSpokenSlug);
            if (IsSuccess)
            {
                logger("Delete employee language spoken [" + languageSpokenSlug + "]");
                return Ok(new {type="success", message="Employee language spoken deleted successfully."});
            }
            else
            {
                return Ok(new {type="error", message="Employee language spoken delete failed."});
            }
        }

        [HttpDeleteAttribute("{employeeSlug}/employmentHistory/{employmentHistoryItemSlug}")]
        public IActionResult DeleteEmploymentHistory(String employeeSlug, String employmentHistoryItemSlug)
        {
            Boolean IsSuccess = _usersRepository.DeleteEmploymentHistoryItem(employeeSlug, employmentHistoryItemSlug);
            if (IsSuccess)
            {
                logger("Delete employee employment history item [" + employmentHistoryItemSlug + "]");
                return Ok(new {type="success", message="Employee employment history item deleted successfully."});
            }
            else
            {
                return Ok(new {type="error", message="Employee employment history item delete failed."});
            }
        }

        public byte[] ConvertIFormFileToBytes(IFormFile file)
        {
            if(file!=null)
            {
                BinaryReader reader = new BinaryReader(file.OpenReadStream());
                return reader.ReadBytes((int)file.Length);
            }
            else
            {
                return null;
            }
            
        }

        [HttpPostAttribute("{employeeSlug}/uploadPicture")]
        public IActionResult UploadPicture(String employeeSlug, [FromFormAttribute]IFormFile picture)
        {
            
            byte[] bytes = new byte[]{};
            Boolean IsSuccess = false;;
            if(picture!=null)
            {
                BinaryReader reader = new BinaryReader(picture.OpenReadStream());
                bytes = reader.ReadBytes((int)picture.Length);

                var a = Request;
                IsSuccess = _usersRepository.UploadPicture(employeeSlug, bytes);
            }
            
            if(IsSuccess)
            {
                var users = _usersRepository.getBySlug(employeeSlug);
                logger("Picture upload [" + employeeSlug + "]");
                return Ok(new {type="success", message="Uploading of picture successfully.", path=users.picture});
            }
            else
            {
                return Ok(new {type="error", message="Uploading of picture failed."});
            }
        }

        [HttpPostAttribute("uploadEducationDocument/{employeeSlug}/{educationSlug}/{documentSlug}")]
        public IActionResult UploadEducationDocument(String employeeSlug, String educationSlug, String documentSlug, [FromFormAttribute]IFormFile document)
        {
            Boolean IsSuccess = _usersRepository.UploadEducationDocument(employeeSlug, educationSlug, documentSlug, document.FileName, ConvertIFormFileToBytes(document));
            if(IsSuccess)
            {
                var docs = _usersRepository.GetEducation(employeeSlug, educationSlug);
                var a = docs.documents.Find(f=>f.slug == documentSlug);
                logger("Education Document upload [" + documentSlug + "]");
                return Ok(new {type="success", message="Uploading of document successfully.", path=a.path});
            }
            else
            {
                return Ok(new {type="error", message="Uploading of document failed."});
            }
        }

        [HttpPostAttribute("uploadTrainingDocument/{employeeSlug}/{trainingSlug}/{documentSlug}")]
        public IActionResult UploadTrainingDocument(String employeeSlug, String trainingSlug, String documentSlug, [FromFormAttribute]IFormFile document)
        {
            Boolean IsSuccess = _usersRepository.UploadTrainingDocument(employeeSlug, trainingSlug, documentSlug, document.FileName, ConvertIFormFileToBytes(document));
            if(IsSuccess)
            {
                var docs = _usersRepository.GetTraining(employeeSlug, trainingSlug);
                var a = docs.documents.Find(f=>f.slug == documentSlug);
                logger("Training Document upload [" + documentSlug + "]");
                return Ok(new {type="success", message="Uploading of document successfully.", path=a.path});
            }
            else
            {
                return Ok(new {type="error", message="Uploading of document failed."});
            }
        }

        [HttpPostAttribute("uploadPreviousEmploymentDocument/{employeeSlug}/{previousEmploymentSlug}/{documentSlug}")]
        public IActionResult UploadPreviousEmploymentDocument(String employeeSlug, String previousEmploymentSlug, String documentSlug, [FromFormAttribute]IFormFile document)
        {
            Boolean IsSuccess = _usersRepository.UploadPreviousEmploymentDocument(employeeSlug, previousEmploymentSlug, documentSlug, document.FileName, ConvertIFormFileToBytes(document));
            if(IsSuccess)
            {
                var docs = _usersRepository.GetPreviousEmployment(employeeSlug, previousEmploymentSlug);
                var a = docs.documents.Find(f=>f.slug == documentSlug);
                logger("Previous Employment Document upload [" + documentSlug + "]");
                return Ok(new {type="success", message="Uploading of document successfully.", path=a.path});
            }
            else
            {
                return Ok(new {type="error", message="Uploading of document failed."});
            }
        }

        [HttpPostAttribute("uploadEmployeeRelationDocument/{employeeSlug}/{employeeRelationSlug}/{documentSlug}")]
        public IActionResult UploadEmployeeRelationDocument(String employeeSlug, String employeeRelationSlug, String documentSlug, [FromFormAttribute]IFormFile document)
        {
            Boolean IsSuccess = _usersRepository.UploadEmployeeRelationDocument(employeeSlug, employeeRelationSlug, documentSlug, document.FileName, ConvertIFormFileToBytes(document));
            if(IsSuccess)
            {
                var docs = _usersRepository.GetEmployeeRelation(employeeSlug, employeeRelationSlug);
                var a = docs.documents.Find(f=>f.slug == documentSlug);
                logger("Employee Relation Document upload [" + documentSlug + "]");
                return Ok(new {type="success", message="Uploading of document successfully.", path=a.path});
            }
            else
            {
                return Ok(new {type="error", message="Uploading of document failed."});
            }
        }

        [HttpPostAttribute("uploadDocument/")]
        public IActionResult UploadDocument([FromFormAttribute]IFormFile document)
        {
            return Ok(
                new 
                {
                    type="success",
                    message="Uploading of document succesfull",
                    path=_usersRepository.UploadDocument(document.FileName.Replace(" ","_"), ConvertIFormFileToBytes(document))
                }
            );
        }

        [HttpGetAttribute("searchEmployee/")]
        public IActionResult SearchEmployee([FromQueryAttribute]String searchString)
        {
            var list = _usersRepository.Search(searchString);
            return Ok(from x in list select new {fullname = x.name.last + ", " + x.name.first + " " + x.extension + " " + x.name.middle, first = x.name.first, middle = x.name.middle, last = x.name.last, extension = x.extension, slug = x.slug, picture = x.picture, position = x.employment.position, x.employeeCode, x.employment.branch, x.employment.department, x.dateHired, status = x.employment.status});
        }

        
        [HttpGetAttribute("pendingUsers/")]
        public IActionResult GetListOfPendingUsers()
        {
            var list = _usersRepository.GetListOfPendingUsers();
            return Ok(from x in list select new {slug=x.slug, fullname = x.name.last + ", " + x.name.first + " " + x.extension + " " + x.name.middle, picture = x.picture, position = x.employment.position, x.employeeCode, x.employment.branch, x.isVerified, x.permissions, x.extension});
        }

        [HttpGetAttribute("possibleRelatives/")]
        public IActionResult GetListOfPossibleRelatives([FromQueryAttribute]String last = "",[FromQueryAttribute]String middle = "",[FromQueryAttribute]String mothersMaidenNameMiddle="", [FromQueryAttribute]String fathersMiddleName = "", [FromQueryAttribute]String relationship="")
        {
            var list = _usersRepository.GetListOfPossibleRelatives(last,middle,mothersMaidenNameMiddle,fathersMiddleName,relationship);
            return Ok(list);
        }

        // [HttpGetAttribute("documentsPerOwnerSlug/")]
        // public IActionResult GetListOfDocumentsPerOwnerSlug([FromQueryAttribute]String ownerSlug = "")
        // {
        //     var list = _usersRepository.GetListOfDocumentsPerOwnerSlug(ownerSlug);
        //     return Ok(list);
        // }

        [HttpPostAttribute("updatePendingStatus/{slug}/{status}")]
        public IActionResult updatePendingStatus(String slug, Boolean status) {
            _usersRepository.updatePendingStatus(slug,status);
            users user = _usersRepository.getBySlug(slug);
            if(user!=null){
                return Ok(new {type="success",message="Status update successfully", isPendingStatus=user.isPending});
            } else {
                return Ok(new {type="failed",message="Status update failed"});
            }
        }

        //POST 
        [errorHandler]
        [HttpPostAttribute("email/{slug}")]
        public IActionResult EmailUserVerification(String slug)
        {
            var user = _usersRepository.getBySlug(slug);
            
            _email.from = "no-reply@kmbidemo.com";
            _email.to = user.email;
            _email.subject = "IT Department Notification";
            _email.message =  @"Hello, " + user.fullname +
               @"<br><br>" +
               @"The IT Department has registered your profile and created your account to the system. Before you login and begin working with the application, please confirm your account by clicking on this link:" +
               @"<br><br>" +
               @"http://localhost:5000/api/users/verify/" + user.verificationToken;

             _email.message = _htmlbuilder.htmlBuild(System.IO.Directory.GetCurrentDirectory() +"/wwwroot/templates/","emailTemplate.cshtml",new SampleInfo { fullname=user.fullname,verificationToken=user.verificationToken});;
                    
            iResult res = _email.Send();
            if (res.code=="SUCCESS") {
                return Ok(new
                    {
                        type = "success",
                        message = "Email verification for user " + user.fullname + " sent succesfully"
                    });
            } else {
                return Ok(new
                    {
                        type = "failed",
                        message = res.description
                    });
            }
            

        }

        [HttpGetAttribute("leaves/{employeeSlug}")]
        public IActionResult getTypesByEmployeeSlug(String employeeSlug)
        {
           var list = _usersRepository.getTypesByEmployeeSlug(employeeSlug);
           if(list!=null) {
                return Ok(list);
           } else {
               return Ok(new List<types>());
           }
           
        }

        [HttpGetAttribute("leaves/{employeeSlug}/{leaveTypeSlug}")]
        public IActionResult getTypesByEmployeeSlug(String employeeSlug, String leaveTypeSlug)
        {
           var list = _usersRepository.getTypesByEmployeeSlugTypeSlug(employeeSlug, leaveTypeSlug);
           if(list!=null) {
                return Ok(list);
           } else {
               return Ok(new {});
           }
           
        }

        [HttpPostAttribute("leaves/{employeeSlug}")]
        public IActionResult createTypesByEmployeeSlug(String employeeSlug, [FromBody]types types)
        {
           var success = _usersRepository.createTypesByEmployeeSlug(employeeSlug,types);
           if(success) {
                logger("Create leave type " + types.type + " successfull");
                return Ok(new {
                    type = "success",
                    messages = "Create leave type " + types.type + " successfull"
                });
           } else {
               return Ok(new {
                    type = "failed",
                    messages = "Create leave type " + types.type + " failed"
                });
           }
           
        }

        [HttpPutAttribute("leaves/{employeeSlug}/{leaveTypeSlug}")]
        public IActionResult updateTypesByEmployeeSlug(String employeeSlug, String leaveTypeSlug, [FromBody]types types)
        {
           var success = _usersRepository.updateTypesByEmployeeSlugTypeSlug(employeeSlug, leaveTypeSlug, types);
           if(success) {
                logger("Update leave type " + types.type + " successfull");
                return Ok(new {
                    type = "success",
                    messages = "Update leave type " + types.type + " successfull"
                });
           } else {
               return Ok(new {
                    type = "failed",
                    messages = "Update leave type " + types.type + " failed"
                });
           }
           
        }

        [HttpDeleteAttribute("leaves/{employeeSlug}/{leaveTypeSlug}")]
        public IActionResult deleteTypesByEmployeeSlug(String employeeSlug, String leaveTypeSlug)
        {
           var success = _usersRepository.deleteTypesByEmployeeSlugTypeSlug(employeeSlug, leaveTypeSlug);
           if(success) {
                logger("Delete leave type slug " + leaveTypeSlug + " successfull");
                return Ok(new {
                    type = "success",
                    messages = "Delete leave type slug " + leaveTypeSlug + " successfull"
                });
           } else {
               return Ok(new {
                    type = "failed",
                    messages = "Delete leave type slug " + leaveTypeSlug + " failed"
                });
           }
           
        }

        [HttpGetAttribute("directory/{branchSlug}")]
        public IActionResult directory(String branchSlug)
        {
            var list = _usersRepository.directoryByBranchSlug(branchSlug,"all");
            return Ok(from x in list select new {
                fullname = x.name.last + ", " + x.name.first + " " + x.extension + " " + x.name.middle, 
                slug = x.slug, 
                employeeCode = x.employeeCode, 
                extension = x.extension,
                x.biometricsCode,
                employeeSchedules = _usersRepository.fixSkeds(x.employeeSchedules.ToArray()).ToList(),
                first = x.name.first,
                middle = x.name.middle,
                last = x.name.last,
                immediateSupervisor = x.immediateSupervisor,
                id = x.Id,
                employment = x.employment,
                picture = x.picture
            });
        }

        [HttpGetAttribute("directory/{branchSlug}/{search}")]
        public IActionResult directory(String branchSlug, String search="all")
        {
            var list = _usersRepository.directoryByBranchSlug(branchSlug,search);
            return Ok(from x in list select new {
                fullname = x.name.last + ", " + x.name.first + " " + x.extension + " " + x.name.middle, 
                slug = x.slug, 
                employeeCode = x.employeeCode, 
                extension = x.extension,
                x.biometricsCode,
                employeeSchedules = _usersRepository.fixSkeds(x.employeeSchedules.ToArray()).ToList()
            });
        }

        [HttpGet("birthdates/{branchSlug}/{month}")]
        public IActionResult birtdates(String branchSlug, String month) {

            DateTime parsed;
            Boolean isValid = DateTime.TryParse(month + "/" + DateTime.Now.Day + "/" + DateTime.Now.Year,out parsed);

            if(isValid) {
                var list = _usersRepository.birthdatesByBranchSlug(branchSlug,parsed.Month);
                return Ok(from x in list select new {
                    employment = x.employment,
                    fullname = x.name.last + ", " + x.name.first + " " + x.extension + " " + x.name.middle, 
                    slug = x.slug,
                    birthdate = Convert.ToDateTime(x.birthdate.Date.Month.ToString() + "/" + x.birthdate.Date.Day.ToString() + "/" + DateTime.Now.Year.ToString())
                });    
            } else {
                return Ok(new {type="error", messages="Invalid month name"});
            }
            
        }

        [HttpGet("regularization/{branchSlug}")]
        public IActionResult regularization(String branchSlug, [FromQuery]DateTime from, [FromQuery]DateTime to) {
            var list = _usersRepository.regularizationByBranchSlug(branchSlug, from,to);
            return Ok(from x in list select new {name = x.name.last + ", " + x.name.first + " " +  x.name.middle, slug = x.slug, regularizationDate = x.employment.effectivityDateFrom.AddMonths(6).AddDays(1)});
        }

        [HttpGetAttribute("pendingUsers/{branchSlug}")]
        public IActionResult GetListOfPendingUsers(String branchSlug)
        {
            var list = _usersRepository.GetListOfPendingUsers(branchSlug);
            return Ok(from x in list select new {slug=x.slug, fullname = x.name.last + ", " + x.name.first + " " + x.extension + " " + x.name.middle, picture = x.picture, position = x.employment.position, x.employeeCode, x.employment.branch, x.isVerified, x.permissions});
        }

        [HttpGetAttribute("servicesAwardees/")]
        public IActionResult getListOfServiceAwardees([FromQuery]DateTime from, [FromQuery]DateTime to, Int32 lengthOfService)
        {
            var list = _usersRepository.getListOfServiceAwardees(from,to,lengthOfService);
            return Ok(from x in list select new { 
                name = x.name.last + ", " + x.name.first + " " + x.extension + " " + x.name.middle, 
                dateHired = x,
                employment = x.employment
            });
        }

        [HttpGetAttribute("servicesAwardees/{branchSlug}/")]
        public IActionResult getListOfServiceAwardees(String branchSlug, [FromQuery]DateTime from, [FromQuery]DateTime to, Int32 lengthOfService)
        {
            var list = _usersRepository.getListOfServiceAwardees(from,to,branchSlug,lengthOfService);
            return Ok(from x in list select new { name = x.name.last + ", " + x.name.first + " " + x.extension + " " + x.name.middle, dateHired = x.dateHired, employment = x.employment});
        }

        [HttpPostAttribute("universalSearch")]
        public IActionResult universalSearch([FromBody]universalSearch universalSearch) {
            var list = _usersRepository.universalSearch(universalSearch);
            return Ok(from x in list select new {
                fullname = x.name.last + ", " + x.name.first + " " + x.extension + " " + x.name.middle, 
                slug = x.slug,
                picture = x.picture, 
                position = x.employment.position, 
                x.employeeCode, 
                x.employment.branch, 
                x.employment.department, 
                x.extension,
                x.addresses,
                x.contacts,
                x.email
            });
        }

        [HttpPutAttribute("setEmployeeStatus/{employeeSlug}/{status}")]
        public IActionResult setEmployeeStatus(string employeeSlug, String status) {
            var employee = _usersRepository.getBySlug(employeeSlug);
            var prevStatus = employee.employment.status;
            Boolean isOk = false;
            if(employee!=null) {
                isOk = _usersRepository.setEmployeeStatus(employeeSlug,status);
                logger(employee.fullname + " changedStatus from " + prevStatus + " to " + status);
            } else {
                return Ok(new {
                    type= "failed",
                    message = @"Employee slug " + employeeSlug + " does not exist"
                });
            }
            
            if(isOk) {
                return Ok(new {
                    type= "success",
                    message = employee.fullname + " status successfully changed from " + prevStatus + " to " + status
                });
            } else {
                return Ok(new {
                    type= "failed",
                    message = employee.fullname + " status change failed"
                });
            }
        }
        
        [HttpGet("getSubordinates/{isSlug}")]
        public IActionResult getSubordinates (String isSlug) {
            var list = _usersRepository.getSubordinates(isSlug);
            return Ok(list.Select(x=>new {slug=x.slug,fullname=x.name.last + ", " + x.name.first + " " + x.extension + " " + x.name.middle,position=x.employment.position}));
        }

        [HttpPut("updateSubordinatesIs/{oldIsSlug}/{newIsSlug}")]
        public IActionResult updateSubordinatesIs(String oldIsSlug, String newIsSlug, [FromBody]List<String> subordinatesSlug) {
            var newIs = _usersRepository.getSlug(newIsSlug);
            var oldIs = _usersRepository.getSlug(oldIsSlug);
            var list = _usersRepository.getSubordinates(oldIs.slug);
            Boolean isOk = false;

            if(newIs!=null) {
                
                isOk = _usersRepository.updateSubordinatesIs(newIsSlug,subordinatesSlug);
                foreach(users u in list) {
                    logger(u.fullname + " IS Changed from " + oldIs.fullname + " to " + newIs.fullname);
                }
                
            } else {
                return Ok(new {
                    type= "failed",
                    message = "Immediate supervisor does not exist"
                });
            }
            
            if(isOk) {
                return Ok(new {
                    type= "success",
                    message = oldIs.fullname + "'s subordinates has been transfered to " + newIs.fullname
                });
            } else {
                return Ok(new {
                    type= "failed",
                    message = "Immediate supervisor update failed"
                });
            }
        }

        [HttpPost("updateLeaveBalances")]
        public IActionResult updateLeaveBalances() {
            var list = _usersRepository.AllUsers();
            var firstDate = Convert.ToDateTime("1/1/" + DateTime.Now.Year);
            _usersRepository.updateLeaveBalances(firstDate,list.ToList());
            return Ok(new {
                type = "success",
                message = "Leave balances updated",
                // data = list
            });
        }

        [HttpPost("copyUserPermission")]
        public IActionResult copyUserPermission([FromQuery]String permissionFrom, [FromBody]String[] employeeCodeList) {
            var list = _usersRepository.AllUsers();

            var from = list.ToList().Find(e=>e.employeeCode == permissionFrom);
            
            if(from==null) {
                return BadRequest(new {
                    type = "error",
                    message = "From employee not found"
                });
            }

            var updatedNames = new List<String>();
            employeeCodeList.ToList().ForEach(e=>{
                var to = list.ToList().Find(ee=>ee.employeeCode == e);
                if(to!=null) {
                    to.permissions = from.permissions;
                    _usersRepository.UpdatePermission(to.slug, to);
                    updatedNames.Add(to.employeeCode + "-" + to.fullname);
                }
            });
            
            return Ok(new {
                type = "success",
                copiedPermissions = from.permissions,
                updatedEmployees = updatedNames,
                message = "Copy permission successfull"
            });
        }

        [HttpPost("copyFromCloud")]
        public IActionResult copyFromCloud([FromQuery]String slug, [FromQuery]String slugInLocal) {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("token","qweasdzxc");
            var response = client.GetAsync(_urlGetter.get().Item2 + "/api/users/getBySlug/" + slug).Result;
            users item = Newtonsoft.Json.JsonConvert.DeserializeObject<users>(response.Content.ReadAsStringAsync().Result);

            var slugToUse = "";
            if(slugInLocal=="" || slugInLocal == null) {
                slugToUse = slug;
            } else {
                slugToUse = slugInLocal;
            }
            _usersRepository.Update(slugToUse,item);
            _usersRepository.UpdatePermission(slugToUse,item);
            var updatedItem = _usersRepository.getBySlug(slugToUse);
            return Ok(updatedItem);
        }

        [HttpPost("updateRegularEmployees/{branchSlug}")]
        public IActionResult updateRegularEmployees(String branchSlug) {
            var now = DateTime.Now.Date;
            var list = _usersRepository.regularizationByBranchSlug(branchSlug,Convert.ToDateTime("1/1/0001"),now);

            list.ToList().ForEach(l=>{
                _usersRepository.setEmployeeStatus(l.slug, "REGULAR");
                // check if rice subsidy
                var b = _usersRepository.GetBenefits(l.slug);
                var r = b.Where(x=> x.name.ToLower() == "rice subsidy").FirstOrDefault();
                if(r==null) {
                    _usersRepository.InsertBenefit(l.slug, new benefit {
                        slug = "rice-subsidy-12345",
                        name = "RICE SUBSIDY",
                        amount = 1500,
                        effectivityDateFrom = now,
                        effectivityDateTo = now.AddYears(10)
                    });
                }
            });

            if(list.Count() > 0) {
                return Ok(new {
                    type = "success",
                    message = "Following employees has been regularized. " + String.Join(",",list.Select(x=>x.fullname).ToList())
                });
            } else {
                return Ok(new {
                    type = "success",
                    message = "no employee(s) to regularize"
                });
            }
        }
    }

    
}
