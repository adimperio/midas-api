﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.payrollModule.Models
{
    public class tax
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("exemption")]
        public taxExemption exemption { get; set; }
    }

    public class taxExemption
    {
        [BsonElement("status")]
        public List<string> status { get; set; }

        [BsonElement("amount")]
        public double amount { get; set; }
    }
    //public class tax
    //{
    //    [BsonId]
    //    [BsonRepresentation(BsonType.ObjectId)]
    //    public String Id { get; set; }

    //    [BsonElement("status")]
    //    public List<string> status { get; set; }

    //    [BsonElement("limitAmount")]
    //    public double limitAmount { get; set; }

    //    [BsonElement("exemption")]
    //    public List<taxExemption> exemption { get; set; }
    //}

    //public class taxExemption
    //{

    //    [BsonElement("cutoffPeriod")]
    //    public string cutoffPeriod { get; set; }

    //    [BsonElement("heading")]
    //    public List<taxHeading> heading { get; set; }
    //}

    //public class taxHeading
    //{
    //    [BsonElement("number")]
    //    public int number { get; set; }

    //    [BsonElement("exemptionAmount")]
    //    public double exemptionAmount { get; set; }

    //    [BsonElement("percentage")]
    //    public double percentage { get; set; }

    //    [BsonElement("statusAmount")]
    //    public double statusAmount { get; set; }
    //}

}
