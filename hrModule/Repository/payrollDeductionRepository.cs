﻿using Microsoft.Extensions.Options;

using System;
using System.Collections.Generic;
using System.Linq;

using MongoDB.Bson;
using MongoDB.Driver;

using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.hrModule.Models;
using kmbi_core_master.hrModule.Interface;

namespace kmbi_core_master.hrModule.Repository
{
    public class payrollDeductionRepository : dbCon, ipayrollDeductionRepository
    {

        IMongoCollection<payrollDeduction> coll;
        public payrollDeductionRepository(IOptions<dbSettings> settings) : base(settings)
        {
            coll = _db.GetCollection<payrollDeduction>("payrollDeductions");
        }

        //payroll deduction
        public IEnumerable<payrollDeduction> getPayrollDeductions()
        {
            IEnumerable<payrollDeduction> list = coll.Find(Builders<payrollDeduction>.Filter.Empty).ToList();
            return list;
        }

        public payrollDeduction getPayrollDeduction(string slug)
        {
            var filter = Builders<payrollDeduction>.Filter.Eq(e => e.slug, slug);
            IEnumerable<payrollDeduction> list = coll.Find(filter).ToList();
            return list.FirstOrDefault();
        }


        public void insertPayrollDeduction(payrollDeduction pd)
        {
            DateTime now = DateTime.Now;
            pd.createdAt = now;
            pd.updatedAt = now;
            coll.InsertOne(pd);
        }

        public void updatePayrollDeduction(string slug, payrollDeduction pd)
        {
            var filter = Builders<payrollDeduction>.Filter.Eq(e => e.slug, slug);
            var update = Builders<payrollDeduction>.Update
                .Set("employee.code", pd.employee.code)
                .Set("employee.slug", pd.employee.slug)
                .Set("employee.name", pd.employee.name)
                .Set("loanType",pd.loanType)
                .CurrentDate("updatedAt");

            coll.UpdateOne(filter, update);
        }

        public void deletePayrollDeduction(string slug)
        {
            var filter = Builders<payrollDeduction>.Filter.Eq(e => e.slug, slug);
            coll.DeleteOne(filter);
        }

        //loantypes
        public payrollDeductionLoanType getLoanType(string pdSlug, String ltSlug)
        {
            var filter = new BsonDocument {
                {"slug",pdSlug},
                {"loanType.slug",ltSlug}
            };

            var proj = new BsonDocument {
                {"_id",0},
                {"loanType.$",1}
            };

            payrollDeduction item = coll.Find(filter).Project<payrollDeduction>(proj).FirstOrDefault();
            if(item!=null) {
                if(item.loanType.Count() > 0) {
                    return item.loanType.First();
                } else {
                    return null;
                }
                
            } else {
                return null;
            }
        }

        public IEnumerable<payrollDeductionLoanType> getLoanTypes(string pdSlug)
        {
            var filter = new BsonDocument {
                {"slug",pdSlug}
            };

            var proj = new BsonDocument {
                {"_id",0},
                {"loanType",1}
            };

            payrollDeduction list = coll.Find(filter).Project<payrollDeduction>(proj).FirstOrDefault();
            if(list!=null) {
                return list.loanType;
            } else {
                return null;
            }
        }

        public void insertLoanType(string pdSlug, payrollDeductionLoanType loanType)
        {
            var filter = new BsonDocument {
                {"slug",pdSlug}
            };

            var update = Builders<payrollDeduction>.Update
                .AddToSet("loanType", loanType)
                .CurrentDate("updatedAt");

            coll.UpdateOne(filter,update);
        }

        public void updateLoanType(string pdSlug, string ltSlug, payrollDeductionLoanType loanType)
        {
            var filter = new BsonDocument {
                {"slug",pdSlug},
                {"loanType.slug",ltSlug}
            };

            var update = Builders<payrollDeduction>.Update
                .Set("loanType.$.slug", loanType.slug)
                .Set("loanType.$.name", loanType.name)
                .Set("loanType.$.deductionPeriod", loanType.deductionPeriod)
                .Set("loanType.$.amount", loanType.amount)
                .Set("loanType.$.amortization", loanType.amortization)
                .Set("loanType.$.balance", loanType.balance)
                .CurrentDate("updatedAt");

            coll.UpdateOne(filter,update);
        }

        public void deleteLoanType(string pdSlug, string ltSlug)
        {
            var filter = new BsonDocument {
                {"slug",pdSlug}
            };

            var filterLoanType = new BsonDocument {
                {"slug",ltSlug}
            };

            var update = Builders<payrollDeduction>.Update
                .PullFilter(e=>e.loanType,filterLoanType)
                .CurrentDate("updatedAt");

            coll.UpdateOne(filter,update);
        }

        public payrollDeduction getPayrollDeductionsByEmployeeCode(string employeeCode)
        {
            payrollDeduction pd = coll.Find(Builders<payrollDeduction>.Filter.Eq(x=>x.employee.code, employeeCode)).FirstOrDefault();
            return pd;
        }
    }
}
