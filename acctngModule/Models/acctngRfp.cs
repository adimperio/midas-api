﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace kmbi_core_master.acctngModule.Models
{
    public class acctngRfp
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("slug")]
        public String slug { get; set; }

        [BsonElement("number")]
        public string number { get; set; }

        [BsonElement("particulars")]
        public string particulars { get; set; }

        [BsonElement("accounts")]
        public rfpAccounts accounts { get; set; }

        [BsonElement("payee")]
        public rfpPayee payee { get; set; }

        [BsonElement("attachments")]
        public string[] attachments { get; set; }

        [BsonElement("amount")]
        public rfpAmount amount { get; set; }

        [BsonElement("remarks")]
        public string remarks { get; set; }

        [BsonElement("timestamps")]
        public rfpTimestamps timestamps { get; set; }
    }

    public class rfpAccounts
    {
        [BsonElement("credit")]
        public rfpCredit[] credit { get; set; }

        [BsonElement("debit")]
        public rfpDebit[] debit { get; set; }

    }

    public class rfpCredit
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("amount")]
        public double amount { get; set; }
    }

    public class rfpDebit
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("amount")]
        public double amount { get; set; }
    }

    public class rfpPayee
    {
        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("tin")]
        public string tin { get; set; }

        [BsonElement("address")]
        public string address { get; set; }

    }

    public class rfpAmount
    {
        [BsonElement("byNumbers")]
        public double byNumbers { get; set; }

        [BsonElement("byWords")]
        public string byWords { get; set; }
    }

    public class rfpTimestamps
    {
        [BsonElement("issuedAt")]
        public DateTime issuedAt { get; set; }

        [BsonElement("issuedBy")]
        public string issuedBy { get; set; }

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("createdBy")]
        public string createdBy { get; set; }

        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }

        [BsonElement("updatedBy")]
        public string updatedBy { get; set; }
    }

    //public class rfpCreditList
    //{
    //    [BsonId]
    //    [BsonRepresentation(BsonType.ObjectId)]
    //    public String Id { get; set; }

    //    [BsonElement("slug")]
    //    public string slug { get; set; }


    //    [BsonElement("accounts")]
    //    public rfpAccounts accounts { get; set; }

    //}

    //public class rfpDebitList
    //{
    //    [BsonId]
    //    [BsonRepresentation(BsonType.ObjectId)]
    //    public String Id { get; set; }

    //    [BsonElement("slug")]
    //    public string slug { get; set; }

    //    [BsonElement("accounts")]
    //    public rfpAccountsList accounts { get; set; }

    //}
}
