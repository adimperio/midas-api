﻿using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.payrollModule.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.payrollModule.IRepository
{
    public interface iphilhealthRepository
    {
         philhealth getSalary(double salary);
    }

}
