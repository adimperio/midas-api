﻿using kmbi_core_master.coreMaster.Models;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.reports.Models
{

    public class repWeeklySchedule
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string id { get; set; }
        public String referenceId { get; set; }
        public loanloanTypes loanType {get;set;}
        public List<centerInstance> center { get; set; }
        public DateTime disbursementDate {get;set;}
        public List<loanMembers> members { get; set; }
        public List<officers> officers {get;set;}
        public amounts amounts {get;set;}
        public int centerLoanCycle {get;set;}
    }
    
    
}
