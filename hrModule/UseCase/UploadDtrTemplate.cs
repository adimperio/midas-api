using System;
using System.Collections.Generic;
using System.IO;
using kmbi_core_master.hrModule.Interface;
using kmbi_core_master.hrModule.Models;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;
using kmbi_core_master.coreMaster.IRepository;
using System.Linq;

namespace kmbi_core_master.hrModule.UseCase {
    public class UploadDtrTemplate : IUploadDtrTemplate<UploadDtrTemplateRequest, UploadDtrTemplateRespnse>
    {
        public idtrManualRepository _dtrManualRepository { get; }
        public iusersRepository _userRepository { get; }

        public UploadDtrTemplate(idtrManualRepository dtrManualRepository, iusersRepository userRepository) {
            _dtrManualRepository = dtrManualRepository;
            _userRepository = userRepository;
        }

        public UploadDtrTemplateRespnse Handle(UploadDtrTemplateRequest request)
        {
            var template = request.template;
            List<String> errors = new List<String>();
            List<dtrManual> finList = new List<dtrManual>();
            var emp = _userRepository.getBySlug(request.employeeSlug);

            if(emp==null) {
                errors.Add("Employee not found!");
            } else {

                BinaryReader reader = new BinaryReader(template.OpenReadStream());
                byte[] bytes = reader.ReadBytes((int)template.Length);

                List<uploadDtrModel> list = new List<uploadDtrModel>();

                using (MemoryStream ms = new MemoryStream(bytes)) {
                    using (ExcelPackage package = new ExcelPackage(ms)) {
                
                        ExcelWorksheet worksheet = package.Workbook.Worksheets[1];

                        int rowCount = worksheet.Dimension.End.Row;

                        for (int row = 2; row <= rowCount; row++)
                        {
                            string _date = null;
                            if(worksheet.Cells[row, 1].Value!=null)
                                _date = worksheet.Cells[row, 1].Value.ToString().Trim();

                            string _in = null;
                            if(worksheet.Cells[row, 2].Value!=null)
                                _in = worksheet.Cells[row, 2].Value.ToString().Trim();

                            string _out = null;
                            if(worksheet.Cells[row, 3].Value!=null)
                                _out = worksheet.Cells[row, 3].Value.ToString().Trim();

                            string _remarks = null;
                            if(worksheet.Cells[row, 4].Value!=null)
                                _remarks = worksheet.Cells[row, 4].Value.ToString().Trim();




                            list.Add(new uploadDtrModel {
                                Date = _date,
                                TimeIn = _in,
                                TimeOut = _out,
                                Remarks = _remarks
                            });
                        }

                    }
                }

                
                
                int ctr=2;

                if(list.Count == 0) {
                    errors.Add("No data to import");
                } else {
                    list.ForEach(x=>{

                    DateTime _date;
                    var isValidDate = DateTime.TryParse(x.Date, out _date);

                    if(isValidDate) {
                        DateTime? _inAm = null;
                        DateTime? _outPm = null;
                        
                        if(x.TimeIn!=null) {
                            DateTime _dateIn;
                            var isValidDateIn = DateTime.TryParse(x.TimeIn, out _dateIn);
                            if(isValidDateIn)
                                _inAm = Convert.ToDateTime(_date.Date.ToShortDateString() + " " + _dateIn.ToString("HH:mm"));
                            else
                                errors.Add("excel row (" + ctr.ToString() + ") Invalid IN log " + x.TimeIn);
                        }

                        if(x.TimeOut!=null) {
                            DateTime _dateOut;
                            var isValidDateOut = DateTime.TryParse(x.TimeOut, out _dateOut);
                            if(isValidDateOut)
                                _outPm = Convert.ToDateTime(_date.Date.ToShortDateString() + " " + _dateOut.ToString("HH:mm"));
                            else
                                errors.Add("excel row (" + ctr.ToString() + ") Invalid OUT log " + x.TimeOut);
                        }

                        finList.Add(new dtrManual {
                            slug = global.slugify2(10),
                            employeeCode = emp.employeeCode,
                            date = Convert.ToDateTime(x.Date),
                            manual = new manual {
                                inAm = _inAm,
                                outAm = null,
                                inPm = null,
                                outPm = _outPm
                            },
                            remarks = x.Remarks
                        });
                    
                    } else {
                        errors.Add("excel row (" + ctr.ToString() + ") " + x.Date + "is not a valid date");
                    }
                    ctr++;
                });
                }
            }

            var noErrors = false;
            var messages = new List<String>();
            if(errors.Count==0) {
                finList.ForEach(x=>{
                    var existing = _dtrManualRepository.getDtrManualPerEmployeeCodePerDate(emp.employeeCode, x.date);
                    if(existing!=null) {
                        _dtrManualRepository.deleteDtrManual(existing.slug);
                    }

                    _dtrManualRepository.createDtrManual(x);
                });
                noErrors = true;
                var _mes =  finList.Select(x=>x.date.ToShortDateString()).ToList();
                _mes.Insert(0, "Import succesfull for employee " + emp.fullname + " for dates");
                messages = _mes;
            
                return new UploadDtrTemplateRespnse {
                    employeeCode = emp.employeeCode,
                    name = emp.fullname,
                    isSuccesfull = noErrors,
                    messages = messages.ToArray()
                };

            } else {
                noErrors = false;
                messages =  errors;

                return new UploadDtrTemplateRespnse {
                    employeeCode = "",
                    name = "",
                    isSuccesfull = noErrors,
                    messages = messages.ToArray()
                };
            }


        }
    }

    public class UploadDtrTemplateRequest {
        public String employeeSlug {get;set;}
        public IFormFile template {get;set;}
    }

    public class UploadDtrTemplateRespnse {
        public String employeeCode {get;set;}
        public String name {get;set;}
        public bool isSuccesfull {get;set;}
        public String[] messages {get;set;}
    }
}