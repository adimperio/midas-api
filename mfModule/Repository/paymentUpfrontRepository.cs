﻿using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Models;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace kmbi_core_master.coreMaster.Repository
{
    public class paymentUpfrontRepository : dbCon, ipaymentUpfrontRepository
    {
        public paymentUpfrontRepository(IOptions<dbSettings> settings) : base(settings)
        {
        }

        public void add(paymentUpfront b)
        {

            b.createdAt = DateTime.Now;
            b.updatedAt = DateTime.Now;

            _db.GetCollection<paymentUpfront>("paymentUpfront").InsertOneAsync(b);
        }

        public IEnumerable<paymentUpfront> all()
        {
            var c = _db.GetCollection<paymentUpfront>("paymentUpfront").Find(new BsonDocument()).ToListAsync();
            return c.Result;
        }

        public paymentUpfront getSlug(String slug)
        {
            var query = Builders<paymentUpfront>.Filter.Eq(e => e.slug, slug);
            var c = _db.GetCollection<paymentUpfront>("paymentUpfront").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }

        public paymentUpfront getRefId(String refId)
        {
            var query = Builders<paymentUpfront>.Filter.Eq(e => e.loan.referenceId, refId);
            var c = _db.GetCollection<paymentUpfront>("paymentUpfront").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }

        public paymentUpfront getId(String id)
        {
            var query = Builders<paymentUpfront>.Filter.Eq(e => e.id, id);
            var c = _db.GetCollection<paymentUpfront>("paymentUpfront").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }

       
        public void update(String id, paymentUpfront p)
        {
            p.id = id;
            var query = Builders<paymentUpfront>.Filter.Eq(e => e.id, id);
            var update = Builders<paymentUpfront>.Update
                .Set("loan", p.loan)
                .Set("members", p.members)
                .Set("officialReceipt", p.officialReceipt)
                .Set("payee", p.payee)
                .Set("mode", p.mode)
                .Set("remarks", p.remarks)
                .CurrentDate("updatedAt");

            var l = _db.GetCollection<paymentUpfront>("paymentUpfront").UpdateOneAsync(query, update);

        }


        public void updateSlug(String slug, paymentUpfront p)
        {
            p.slug = slug;
            var query = Builders<paymentUpfront>.Filter.Eq(e => e.slug, slug);
            var update = Builders<paymentUpfront>.Update
                .Set("loan", p.loan)
                .Set("members", p.members)
                .Set("officialReceipt", p.officialReceipt)
                .Set("payee", p.payee)
                .Set("mode", p.mode)
                .Set("remarks", p.remarks)
                .CurrentDate("updatedAt");

            var l = _db.GetCollection<paymentUpfront>("paymentUpfront").UpdateOneAsync(query, update);

        }


        public void remove(String id)
        {
            var query = Builders<paymentUpfront>.Filter.Eq(e => e.id, id);
            var result = _db.GetCollection<paymentUpfront>("paymentUpfront").DeleteOneAsync(query);
        }


        public void removeSlug(String slug)
        {
            var query = Builders<paymentUpfront>.Filter.Eq(e => e.slug, slug);
            var result = _db.GetCollection<paymentUpfront>("paymentUpfront").DeleteOneAsync(query);
        }
    }
}
