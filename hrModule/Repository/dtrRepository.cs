﻿using Microsoft.Extensions.Options;

using System;
using System.Collections.Generic;
using System.Linq;

using MongoDB.Bson;
using MongoDB.Driver;

using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.hrModule.Models;
using kmbi_core_master.hrModule.Interface;

namespace kmbi_core_master.hrModule.Repository
{
    public class dtrRepository : dbCon, idtrRepository
    {
        
        public dtrRepository(IOptions<dbSettings> settings) : base(settings) {}

        public void add(dtr b)
        {

            b.createdAt = DateTime.Now;
            b.updatedAt = DateTime.Now;
            
            var coll = _db.GetCollection<dtr>("dtr");
            var q1 = Builders<dtr>.Filter.Eq("date", b.date);            
            var q3 = Builders<dtr>.Filter.Eq("employeeCode", b.employeeCode);
            var query2 = Builders<dtr>.Filter.And(q1, q3);

            coll.DeleteMany(query2);


            _db.GetCollection<dtr>("dtr").InsertOne(b);
        }

        public void add(List<dtr> dtrs)
        {
            if(dtrs!=null) {
                List<dtr> _dtrs = dtrs;
                foreach(dtr dtr in _dtrs)
                {
                    dtr.createdAt = DateTime.Now;
                    dtr.updatedAt = DateTime.Now;

                    var coll = _db.GetCollection<dtr>("dtr");
                    var q1 = Builders<dtr>.Filter.Eq("date", dtr.date);            
                    var q3 = Builders<dtr>.Filter.Eq("employeeCode", dtr.employeeCode);
                    var query2 = Builders<dtr>.Filter.And(q1, q3);

                    var wer = coll.Find(query2).ToList();
                    coll.DeleteMany(query2);
                }
                _db.GetCollection<dtr>("dtr").InsertMany(_dtrs);
            }
        }

        public IEnumerable<dtr> all()
        {
            var c = _db.GetCollection<dtr>("dtr").Find(new BsonDocument()).ToList();
            return c;
        }

        public dtr getSlug(String slug)
        {
            var query = Builders<dtr>.Filter.Eq(e => e.slug, slug);
            var c = _db.GetCollection<dtr>("dtr").Find(query).ToList();

            return c.FirstOrDefault();
        }

       
        public void update(String slug, dtr p)
        {
            p.slug = slug;
            var query = Builders<dtr>.Filter.Eq(e => e.slug, slug);
            var update = Builders<dtr>.Update
                .Set("employeeCode", p.employeeCode)
                .Set("date", p.date)
                .Set("schedule", p.schedule)
                .Set("actual", p.actual)
                .Set("manual", p.manual)
                .Set("late", p.late)
                .Set("undertime", p.undertime)
                .Set("absent", p.absent)
                .Set("overtime", p.overtime)
                .Set("hoursWork", p.hoursWork)
                .Set("nd", p.nd)
                .Set("remarks", p.remarks)
                .CurrentDate("updatedAt");

            var branch = _db.GetCollection<dtr>("dtr").UpdateOne(query, update);

        }



        public void remove(String slug)
        {
            var query = Builders<dtr>.Filter.Eq(e => e.slug, slug);
            var result = _db.GetCollection<dtr>("dtr").DeleteOne(query);
        }

        public List<dtr> getDtrPerEmployeeCodePerDateRange(String employeeCode, DateTime from, DateTime to)
        {
            var coll = _db.GetCollection<dtr>("dtr");
            var q1 = Builders<dtr>.Filter.Gte("date", from.Date);
            var q2 = Builders<dtr>.Filter.Lte("date", to.Date.AddDays(1).AddSeconds(-1));
            var q3 = Builders<dtr>.Filter.Eq("employeeCode", employeeCode);

            var c = coll.Find(Builders<dtr>.Filter.And(q1, q2, q3)).ToList();

            var coll2 = _db.GetCollection<users>("users");
            var q4 = Builders<users>.Filter.Gte(u=>u.employeeCode, employeeCode);

            var e = coll2.Find(q4).FirstOrDefault();

            if(e!=null) {
                c.ForEach(cc=>{
                    cc.jobGrade = e.employment.jobGrade;
                });
            }
            
            return c;
        }

        public List<dtrNameTime> getDtrPerBranchSlugPerDateRange(String branchSlug, DateTime from, DateTime to) {

            var collLoans = _db.GetCollection<users>("users");
            var repLoan = collLoans.Aggregate()
                .Match(new BsonDocument {
                    {"employment.branch",branchSlug}
                })
                .Lookup("dtr", "employeeCode", "employeeCode", "dtr")
                .Unwind("dtr")
                .Project(new BsonDocument
                        {
                            {"_id",0},
                            {"slug","$slug"},
                            {"code","$employeeCode"},
                            {"name","$fullname"},
                            {"name2","$name"},
                            {"extension","$extension"},
                            {"status","$employment.status"},
                            {"position","$employment.position"},
                            {"picture","$picture"},
                            {"remarks", "$dtr.remarks"},
                            {"timeIn",new BsonDocument {
                                    {"$cond",new BsonArray {
                                            {new BsonDocument{
                                                {"$eq",new BsonArray{{"$dtr.manual.inAm"},{BsonNull.Value}}}
                                            }},
                                            {"$dtr.actual.inAm"},
                                            {"$dtr.manual.inAm"}
                                        }
                                    }
                                }
                            },
                            {"timeOut",new BsonDocument {
                                    {"$cond",new BsonArray {
                                            {new BsonDocument{
                                                {"$eq",new BsonArray{{"$dtr.manual.outPm"},{BsonNull.Value}}}
                                            }},
                                            {"$dtr.actual.outPm"},
                                            {"$dtr.manual.outPm"}
                                        }
                                    }
                                }
                            },
                            {"withinRange",new BsonDocument{
                                {"$and",new BsonArray{
                                    {new BsonDocument {
                                        {"$gte",new BsonArray{
                                            {"$dtr.date"},
                                            {from}
                                        }}
                                    }},
                                    {new BsonDocument {
                                        {"$lte",new BsonArray{
                                            {"$dtr.date"},
                                            {to}
                                        }}
                                    }}
                                }}
                            }}
                        })
                .Match(new BsonDocument {
                    {"withinRange",true}
                })
                .Project<dtrNameTime>(new BsonDocument{
                    {"withinRange",0}
                });

            return repLoan.ToList();
        }

        public Int32 getOverlappedAttendances(String employeeCode, DateTime from, DateTime to) {
            var tot = 0.0;
           
            for(DateTime ctr = from; ctr<=to;ctr = ctr.AddDays(1)) {
                if(ctr.DayOfWeek != DayOfWeek.Saturday && ctr.DayOfWeek != DayOfWeek.Sunday) {
                    var hours = 0.0;
                 
                        hours = (to - ctr).TotalHours;
                   
                    if (hours > 4) {
                        tot += (hours - 1) / 8;
                    } else {
                        tot += hours / 8;
                    }
                }
            }

            if(tot % 1 == 0) {
                DateTime _f = from;
                var coll = _db.GetCollection<dtr>("dtr");
                var filter = Builders<dtr>.Filter.Gte("date",Convert.ToDateTime(from.ToString("MM/dd/yyyy") + " 00:00:00"));
                var filter1 = Builders<dtr>.Filter.Lte("date",Convert.ToDateTime(to.ToString("MM/dd/yyyy") + " 23:59:59"));
                
                // var filter2 = Builders<dtr>.Filter.Gte("dates.to",from);
                // var filter3 = Builders<dtr>.Filter.Lte("dates.to",Convert.ToDateTime(to.ToString("MM/dd/yyyy") + " 23:59:59"));

                // var filter4 = Builders<dtr>.Filter.Lte("dates.from",from);
                // var filter5 = Builders<dtr>.Filter.Gte("dates.to",Convert.ToDateTime(to.ToString("MM/dd/yyyy") + " 23:59:59"));

                
                var filter6 = Builders<dtr>.Filter.Eq(l => l.employeeCode, employeeCode);
                
                var filterAnd = Builders<dtr>.Filter.And(filter,filter1);
                // var filterAnd1 = Builders<dtr>.Filter.And(filter2,filter3);
                // var filterAnd2 = Builders<dtr>.Filter.And(filter4,filter5);

                //var filterOr = Builders<dtr>.Filter.Or(filterAnd,filterAnd1,filterAnd2);

                var hours = (to - from).TotalHours;
                if(hours>4) {
                    //whole
                } else {
                    //half
                }
                var filterAnd3 = Builders<dtr>.Filter.And(filter6,filterAnd);
                var list = coll.Find(filterAnd3).ToList();

                Boolean[] truths = {false,false,false};
                list.ForEach((x) => {
                    DateTime? inAm = x.actual.inAm;
                    DateTime? outPm = x.actual.outPm;
                    if(x.manual.inAm!=null) {
                        inAm = x.manual.inAm;
                    }
                    if(x.manual.outPm!=null) {
                        outPm = x.manual.outPm;
                    }


                    
                    if(inAm >= from && inAm <= to) {
                        truths[0]= true;
                    }

                    if(outPm >= from && outPm <= to) {
                        truths[1]= true;
                    }

                    if(inAm >= from && outPm <= to) {
                        truths[2]= true;
                    }


                });
                return truths[0] || truths[1] || truths[2] ? 1 : 0;

            } else {
                return 0;

            }
        }

        public List<dtr> getDtrPerDate(DateTime date)
        {
            var coll = _db.GetCollection<dtr>("dtr");
            var q1 = Builders<dtr>.Filter.Eq("date", date);

            var c = coll.Find(q1).ToList();

            var coll2 = _db.GetCollection<users>("users");
            var e = coll2.Find(new BsonDocument()).ToList();

            e.ForEach(ee=>{
                c.ForEach(cc=>{
                    cc.jobGrade = ee.employment.jobGrade;
                });
            });
            
            return c;
        }

        public List<dtr> getDtrPerDateRange(DateTime from, DateTime to)
        {
            var coll = _db.GetCollection<dtr>("dtr");
            var q1 = Builders<dtr>.Filter.Gte("date", from);
            var q2 = Builders<dtr>.Filter.Lte("date", to);

            var c = coll.Find(Builders<dtr>.Filter.And(q1, q2)).ToList();
            
            var coll2 = _db.GetCollection<users>("users");
            var e = coll2.Find(new BsonDocument()).ToList();

            e.ForEach(ee=>{
                c.ForEach(cc=>{
                    cc.jobGrade = ee.employment.jobGrade;
                });
            });
            
            return c;
        }
        
    }
}
