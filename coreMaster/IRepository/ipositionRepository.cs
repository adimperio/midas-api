﻿using kmbi_core_master.coreMaster.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.coreMaster.IRepository
{
    public interface ipositionRepository
    {
        IEnumerable<positions> getAll();

        positions getSlug(String slug);

        void add(positions b);

        void update(String slug, positions b);

        void delete(String slug);

        IEnumerable<positions> search(string name);
    }
}
