﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;

using kmbi_core_master.coreMaster.Models;

using kmbi_core_master.hrModule.Models;
using kmbi_core_master.hrModule.Interface;
using System.Threading.Tasks;

namespace kmbi_core_master.hrModule.Repository
{
    public class attendanceLogsRepository : dbCon, iattendanceLogsRepository
    {
        IMongoCollection<attendanceLogs> _context;
        IOptions<biometricSettings> _bio;
        public attendanceLogsRepository(IOptions<dbSettings> settings, IOptions<biometricSettings> bio) : base(settings)
        {
            _context = _db.GetCollection<attendanceLogs>("attendanceLogs");
            _bio = bio;
        }

        public IEnumerable<attendanceLogs> getAttendanceLogsPerEmployeePerDateRange(String biometricsCode, DateTime from, DateTime to)
        {
            System.DateTime _from = from.Date;
            System.DateTime _to = to.Date.AddDays(1).AddSeconds(-1);

            var q1 = Builders<attendanceLogs>.Filter.Gte("checkTime", _from);
            var q2 = Builders<attendanceLogs>.Filter.Lte("checkTime", _to);
            var q3 = Builders<attendanceLogs>.Filter.Eq("userId", Int32.Parse(biometricsCode));
            return _context.Find(Builders<attendanceLogs>.Filter.And(q1,q2,q3)).ToList();
        }

        public IEnumerable<attendanceLogs> getAttendanceLogsPerDateRange(DateTime from, DateTime to)
        {
            System.DateTime _from = from.Date;
            System.DateTime _to = to.Date.AddDays(1).AddSeconds(-1);

            var q1 = Builders<attendanceLogs>.Filter.Gte("checkTime", _from);
            var q2 = Builders<attendanceLogs>.Filter.Lte("checkTime", _to);
            return _context.Find(Builders<attendanceLogs>.Filter.And(q1,q2)).ToList();
        }

        public downloadResult download(DateTime from, DateTime to, String employeeCode)
        {
            return privateDownload(from,to,employeeCode);
        }

        public downloadResult download(DateTime from, DateTime to)
        {
            return privateDownload(from,to);
        }

        public downloadResult download(Int32 month, Int32 year, String employeeCode)
        {
            DateTime theDate = DateTime.Parse(month.ToString() + "/1/" + year.ToString());

            downloadResult result = privateDownload(theDate,theDate.AddMonths(1).AddDays(-1),employeeCode);
            return result;
        }

        public downloadResult download(Int32 month, Int32 year)
        {
            DateTime theDate = DateTime.Parse(month.ToString() + "/1/" + year.ToString());

            downloadResult result = privateDownload(theDate,theDate.AddMonths(1).AddDays(-1));
            return result;
        }

        public downloadResult privateDownload(DateTime from, DateTime to, String employeeCode ="")
        {				String cs = String.Format(
                "Data Source={0};Initial Catalog={1}; User ID={2};Password={3};",
                _bio.Value.server,
                _bio.Value.db,
                _bio.Value.uid,
                _bio.Value.pass);

            System.DateTime _from = from.Date;
            System.DateTime _to = to.Date.AddDays(1).AddSeconds(-1);

            SqlParameter pFrom = new SqlParameter{
                ParameterName="@from",
                Value = _from
            };
            SqlParameter pTo = new SqlParameter{
                ParameterName="@to",
                Value = _to
            };
            SqlParameter pEmployeeCode = new SqlParameter{
                ParameterName="@employeeCode",
                Value = employeeCode
            };

            String query = @"
                SELECT 
                    Logid as logId, 
                    Userid as userId, 
                    name, 
                    checkTime, 
                    checkType, 
                    statusText, 
                    clientId, 
                    clientName 
                FROM
                    v_Record
                WHERE
                    checkTime BETWEEN @from AND @to";

            if(employeeCode!="")
            {
                query +=" AND Userid = @employeeCode";
            }
            

            SqlConnection con =new SqlConnection(cs);
            try {
                con.Open();
            } catch(Exception ex) {
                return new downloadResult{
                    startTime = DateTime.Now,
                    endTime = DateTime.Now,
                    duration = 0,
                    logs = new List<log>(),
                    status = "There is some problem downloading attendance",
                    specError = ex.Message
                };
            }
            
            var cmd = new SqlCommand(query,con);
            cmd.Parameters.Add(pFrom);
            cmd.Parameters.Add(pTo);
            cmd.Parameters.Add(pEmployeeCode);
            
            var dr = cmd.ExecuteReader();

            var list2 = new List<attendanceLogs>();

            while (dr.Read())
            {
                list2.Add(new attendanceLogs
                    {
                        logId = Int32.Parse(dr["logId"].ToString()),
                        userId = Int32.Parse(dr["Userid"].ToString()),
                        name = dr["name"].ToString(),
                        checkTime = Convert.ToDateTime(dr["checkTime"].ToString()).ToUniversalTime(),
                        checkType = dr["checkType"].ToString(),
                        statusText = dr["statusText"].ToString(),
                        clientId = Int32.Parse(dr["clientId"].ToString()),
                        clientName = dr["clientName"].ToString()
                    }
                );
            }

            //REMOVE EXISTING
            var coll = _db.GetCollection<attendanceLogs>("attendanceLogs");
            var q1 = Builders<attendanceLogs>.Filter.Gte("checkTime", _from);
            var q2 = Builders<attendanceLogs>.Filter.Lte("checkTime", _to);

            FilterDefinition<attendanceLogs> q3 = Builders<attendanceLogs>.Filter.Empty;
            if(employeeCode!="")
            {
                q3 = Builders<attendanceLogs>.Filter.Eq("userId", Convert.ToInt32(employeeCode));
            }
            
            
            var query2 = Builders<attendanceLogs>.Filter.And(q1, q2, q3);


            var wer = coll.Find(query2).ToList();
            var dd= wer.ToList();
            coll.DeleteMany(query2);

            if (list2.Count > 0)
            {
                coll.InsertMany(list2);
            }
            

            con.Close();

            return new downloadResult{
                startTime = DateTime.Now,
                endTime = DateTime.Now,
                duration = 0,
                logs = (from x in list2 select (new log {name = x.name, employeeCode = x.clientId.ToString(), time = x.checkTime, type =x.checkType})).ToList(),
                status = "Download successfull"
            };
        }
    }
}
