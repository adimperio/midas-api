﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace kmbi_core_master.acctngModule.Models
{
    public class acctngCv
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("slug")]
        public String slug { get; set; }

        [BsonElement("chequeNumber")]
        public string chequeNumber { get; set; }

        [BsonElement("cvNumber")]
        public string cvNumber { get; set; }

        [BsonElement("particulars")]
        public string particulars { get; set; }

        [BsonElement("accounts")]
        public cvAccounts accounts { get; set; }
        
        [BsonElement("branch")]
        public cvBranch branch { get; set; }

        [BsonElement("supplier")]
        public cvSupplier supplier { get; set; }

        [BsonElement("isCancelled")]
        public bool isCancelled { get; set; }

        [BsonElement("reason")]
        public string reason { get; set; }

        [BsonElement("status")]
        public string status { get; set; }

        [BsonElement("isClosed")]
        public bool isClosed { get; set; }

        [BsonElement("amount")]
        public cvAmount amount { get; set; }

        [BsonElement("timestamps")]
        public cvTimestamps timestamps { get; set; }
    }

    public class cvAccounts
    {
        [BsonElement("credit")]
        public cvCredit[] credit { get; set; }

        [BsonElement("debit")]
        public cvDebit[] debit { get; set; }

    }

    public class cvCredit
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("code")]
        public int code { get; set; }

        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("department")]
        public string department { get; set; }

        [BsonElement("budget")]
        public string budget { get; set; }

        [BsonElement("amount")]
        public double amount { get; set; }

        [BsonElement("ewt")]
        public string ewt { get; set; }

        [BsonElement("rate")]
        public int? rate { get; set; }
    }

    public class cvDebit
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("code")]
        public int code { get; set; }

        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("department")]
        public string department { get; set; }

        [BsonElement("budget")]
        public string budget { get; set; }

        [BsonElement("amount")]
        public double amount { get; set; }

        [BsonElement("ewt")]
        public string ewt { get; set; }

        [BsonElement("rate")]
        public int? rate { get; set; }
    }

    public class cvBranch
    {
        [BsonId]
        [BsonElement("id")]
        public string id { get; set; }

        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

    }

    public class cvSupplier
    {
        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("tin")]
        public string tin { get; set; }

        [BsonElement("address")]
        public string address { get; set; }

    }

    public class cvAmount
    {
        [BsonElement("byNumbers")]
        public double byNumbers { get; set; }

        [BsonElement("byWords")]
        public string byWords { get; set; }
    }

    public class cvTimestamps
    {
        [BsonElement("issuedAt")]
        public DateTime issuedAt { get; set; }

        [BsonElement("issuedBy")]
        public string issuedBy { get; set; }

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("createdBy")]
        public string createdBy { get; set; }

        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }

        [BsonElement("updatedBy")]
        public string updatedBy { get; set; }
    }

    //public class rfpCreditList
    //{
    //    [BsonId]
    //    [BsonRepresentation(BsonType.ObjectId)]
    //    public String Id { get; set; }

    //    [BsonElement("slug")]
    //    public string slug { get; set; }


    //    [BsonElement("accounts")]
    //    public rfpAccounts accounts { get; set; }

    //}

    //public class rfpDebitList
    //{
    //    [BsonId]
    //    [BsonRepresentation(BsonType.ObjectId)]
    //    public String Id { get; set; }

    //    [BsonElement("slug")]
    //    public string slug { get; set; }

    //    [BsonElement("accounts")]
    //    public rfpAccountsList accounts { get; set; }

    //}
}
