﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace kmbi_core_master.payrollModule.Models
{
    public class fhgDeductionExtractedForPayroll
    {
        public Double fidelityBond {get;set;}
        public Double fhgLoanPayable {get;set;}
        public Double fhgShareCapital {get;set;}
        public Double fhgSavings {get;set;}
        public Double fhgInsurance {get;set;}
        public Double fhgOthers {get;set;}
    }
}
