﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.coreMaster.Filters;
using Microsoft.AspNetCore.Http;
using MongoDB.Bson;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Net.Http.Headers;
using MongoDB.Driver;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Cors;

using kmbi_core_master.coreMaster.IRepository;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace kmbi_core_master.coreMaster.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/importCSV")]
    [usersAuth]
    public class importCSVController : Controller
    {
        iimportCsvRepository _rep;
        public importCSVController(iimportCsvRepository rep)
        {
            _rep = rep;
        }

        [HttpPost("upload")]
        public IActionResult upload(IFormFile csv, String type="")
        {
            if (csv==null)
            {
                return Ok(new
                {
                    type = "warning",
                    message = "No file submitted."
                });

            }
            else
            {
                string extension = Path.GetExtension(csv.FileName).ToLower();
                if (extension != ".csv")
                {
                    return Ok(new
                    {
                        type = "warning",
                        message = "Supported file extension: csv"
                    });
                }

            }
            
            
            if(type=="clients")
            {
                var res = _rep.importClients(csv, HttpContext.Request.Headers["branchId"].ToString());

                if(res[0].success == 0) {
                    return BadRequest(from x in res select new{type="error", messages = x.message});
                } else {
                    return Ok(from x in res select new{type="success", messages = x.message});
                }
            }
            else if(type=="users")
            {
                return Ok(_rep.importUsers(csv));
            }
            else if(type=="")
            {
                return Ok(new
                    {
                        type = "warning",
                        message = "No type specified."
                    });
            }
            else
            {
                return Ok(new
                    {
                        type = "warning",
                        message = "Invalid type specified."
                    });
            }
            
        }
    }
}
