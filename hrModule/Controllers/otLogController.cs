﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Filters;
using kmbi_core_master.hrModule.Interface;
using kmbi_core_master.hrModule.Models;
using Microsoft.AspNetCore.Cors;

using System;
using System.Threading.Tasks;

namespace kmbi_core_master.coreMaster.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/otLogs")]
    [usersAuth]
    public class otLogsController : Controller
    {
        private readonly iotLogsRepository _Repository;

        public otLogsController(iotLogsRepository Repository) {
            this._Repository = Repository;
        }

        private void logger(String message) {
            //global.log(message, 1, "link here");
        }  

        [HttpGet("{employeeCode}")]
        public IActionResult getOtLogPerEmployeeCodePerDate(String employeeCode, [FromQuery]DateTime? from, [FromQuery]DateTime? to) {
            List<otLog> list = new List<otLog>();
            if(from!=null&&to!=null) {
                list = _Repository.getOtLogPerEmployeeCodePerDateRange(employeeCode,Convert.ToDateTime(from),Convert.ToDateTime(to));
            } else {
                return BadRequest(new {
                    type = "error",
                    message = "Invalid parameter"
                });
            }
            
            return Ok(list);
        }

        [HttpPost]
        public IActionResult createOtLog([FromBody]otLog otLog) {
            if(otLog!=null) {
                DateTime now = DateTime.Now;
                otLog.createdAt = now;
                otLog.updatedAt = now;
                _Repository.createOtLog(otLog);
                logger("OT with slug " + otLog.slug + " created successfully");
                return Ok(new {
                    type = "success",
                    message = "OT create successfully"
                });
            } else {
                return BadRequest(new {
                    type = "error",
                    message = "OT create failed"
                });
            }
        }

        [HttpPut("{slug}")]
        public IActionResult updateOtLog(String slug, [FromBody]otLog otLog) {
            if(otLog!=null) {
                otLog.updatedAt = DateTime.Now;
                _Repository.updateOtLog(slug,otLog);
                logger("OT with slug " + otLog.slug + " updated successfully");
                return Ok(new {
                    type = "success",
                    message = "OT update successfully"
                });
            } else {
                return BadRequest(new {
                    type = "error",
                    message = "OT update failed"
                });                
            }
        }
        
        [HttpPut("replace/{employeeCode}")]
        public IActionResult replaceOtLog(String employeeCode, [FromQuery]DateTime? date, [FromBody]otLog otLog) {
            if(employeeCode!=null && date!=null && otLog!=null) {
                _Repository.replaceOtLogPerEmployeeCodePerDate(employeeCode,Convert.ToDateTime(date),otLog);
                logger("OT with employee code " + employeeCode + ", date " + Convert.ToDateTime(date).ToString("MM/dd/yyyy") + " replaced successfully");
                return Ok(new {
                    type="success",
                    message = "OT replace successfully"
                });
            } else {
                return BadRequest(new {
                    type="error",
                    message = "OT replace failed"
                });
            }
        }

        [HttpDelete("{slug}")]
        public IActionResult deleteOtLog(String slug) {
            if(slug!=null) {
                _Repository.deleteOtLog(slug);
                logger("OT with slug " + slug + " deleted successfully");
                return Ok(new {
                    type = "success",
                    message = "OT delete successfully"
                });
            } else {
                return BadRequest(new {
                    type = "error",
                    message = "OT delete failed"
                });                
            }
        }

    }
}
