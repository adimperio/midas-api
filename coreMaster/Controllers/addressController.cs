﻿using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Filters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Cors;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace kmbi_core_master.coreMaster.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/address")]
    //[exception]
    public class addressController : Controller
    {
        private readonly iaddressRepository _Repository;

        public addressController(iaddressRepository Repository)
        {
            _Repository = Repository;
        }

        [HttpGet("allRegion")]
        public IActionResult allRegion()
        {
            var item = _Repository.allRegion();
            if (item.Count == 0)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        [HttpGet("allProvince")]
        public IActionResult allProvince()
        {
            var item = _Repository.allProvince();
            if (item.Count == 0)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        [HttpGet("region")]
        public IActionResult getRegion([FromQuery]string slug)
        {
            var item = _Repository.getRegion(slug);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        [HttpGet("province")]
        public IActionResult getProvince([FromQuery]string regionSlug)
        {
            var item = _Repository.getProvince(regionSlug);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        [HttpGet("municipality")]
        public IActionResult getMunicipality([FromQuery]string provinceSlug)
        {
            var item = _Repository.getMunicipality(provinceSlug);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        [HttpGet("barangay")]
        public IActionResult getBarangay([FromQuery]string municipalitySlug)
        {
            var item = _Repository.getBarangay(municipalitySlug);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

    }
}
