﻿using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.helpers;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace kmbi_core_master.coreMaster.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/diagnostics")]
    public class diagnosticsController : Controller
    {
        public GetBackendInfo _getBackendInfo { get; }
        public diagnosticsController(GetBackendInfo getBackendInfo) {
            _getBackendInfo = getBackendInfo;
        }

        public string get()
        {

            var result = _getBackendInfo.Handle(new GetBackendInfoRequest {
                key = ""
            });

            var jsonString = (object)null;
           
            if (result.Message.ToLower() == "hello kmbi") {
                jsonString = new {
                    Version = result.Version,
                    CoreLastUpdate = result.AssemblyLastModified,
                    MongoServer = result.MongoServer,
                    MongoServerStatus = result.MongoServerStatus,
                    FrontEnd = result.FronEndLink,
                    BackEnd = result.BackEndLink,
                    Environment = result.Environment,
                    ReportDiretoryAccessStatus = result.ReportDirectoryAccessStatus,
                    ClientImagesAccessStatus = result.ClientImagesAccessStatus,
                    ClientSignaturesAccessStatus = result.ClientSignaturesAccessStatus,
                    SyncServer = result.SyncServer,
                    SyncServerStatus = result.SyncServerStatus,
                    InternetConnection = result.InternetConnection
                };
            } else {
                jsonString = new{
                    message = "error"
                };
            }
            string json = JsonConvert.SerializeObject(jsonString);
            string jsonFormatted = JValue.Parse(json).ToString(Formatting.Indented);
            return jsonFormatted;
        }
    }
}

