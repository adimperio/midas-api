﻿using kmbi_core_master.coreMaster.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.coreMaster.IRepository
{
    public interface icbuLogsRepository
    {
        IEnumerable<cbuLogs> getAll();

        List<cbuLogs> Get(String id);

        List<cbuLogs> getSlug(String slug);

        List<cbuLogs> clientId(String clientId);

        List<cbuLogs> clientIdDates(String clientId, DateTime from, DateTime to);

        void Add(cbuLogs c);

        void Update(String slug, cbuLogs c);
              
        void Remove(String slug);

        cbuLogs checkSlug(String slug);

        List<cbuLogsBytrasactedAt> incremPercentage(DateTime from, DateTime to, string branchSlug);

        cbuLogs getFirstEntry(String clientId);

        Boolean updateFirstEntry(String clientId, Double amount);

        IEnumerable<cbuLogs> getCbuLogsPerBranchPerDateRange(String branchSlug, DateTime from, DateTime to);

        void fixBegCbu();

        IEnumerable<cbuLogs> getBeginningBalance();

        void UpdateCbuBalance(String ClientId, Double Amount);

        void UpdateCbuBalanceBySlug(String ClientSlug, Double Amount);

        void UpdateLoanCycleBySlug(String ClientSlug, int Cycle);
    }
}
