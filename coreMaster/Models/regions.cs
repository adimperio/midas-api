﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace kmbi_core_master.coreMaster.Models
{
    public class regions
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }
        
        [BsonElement("name")]
        [Required(ErrorMessage = "The Name field is required.")]
        public string name { get; set; }
       
        [BsonElement("slug")]
        public string slug { get; set; }
        
        [BsonElement("code")]
        [Required(ErrorMessage = "The Code field is required.")]
        public string code { get; set; }
        
        [BsonElement("head")]
        [Required(ErrorMessage = "The Head field is required.")]
        public string head { get; set; }

        [BsonElement("areas")]
        public IList<areas> areas { get; set; }

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }
    }

    public class areas
    {

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        
        [BsonElement("name")]
        [Required(ErrorMessage = "The Area Name field is required.")]
        public string name { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("code")]
        [Required(ErrorMessage = "The Area Code field is required.")]
        public string code { get; set; }

        [BsonElement("head")]
        [Required(ErrorMessage = "The Area Head field is required.")]
        public string head { get; set; }
    }
}
