﻿using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.spm.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.spm.IRepository
{
    public interface iquestionairesRepository
    {
        IEnumerable<questionaires> getAll();

        questionaires getSlug(String slug);

        void add(questionaires b);

        void update(String slug, questionaires b);

        void delete(String slug);

        IEnumerable<questionaires> search(string name);

        void sortByCode(List<string> id, string slug);
    }
}
