﻿using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Filters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Cors;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace kmbi_core_master.coreMaster.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/logs")]
    //[exception]
    public class logsController : Controller
    {
        private readonly ilogsRepository _Repository;
        public logsController(ilogsRepository logsRepository)
        {
            _Repository = logsRepository;
        }

       
        [HttpPost("insertLog")]
        public void insertLog([FromBody] activity logs)
        {
            _Repository.log(logs);
        }
        
    }
}
