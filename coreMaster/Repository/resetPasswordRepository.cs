﻿using kmbi_core_master.coreMaster.IRepository;
using System;
using System.Linq;
using kmbi_core_master.coreMaster.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace kmbi_core_master.coreMaster.Repository
{
    public class resetPasswordRepository : dbCon, iresetPasswordRepository
    {
        public resetPasswordRepository(IOptions<dbSettings> settings) : base(settings)
        {
        }

        public void add(resetPassword user)
        {
            remove(user.email);
            user.createdAt = DateTime.Now;
            _db.GetCollection<resetPassword>("resetPassword").InsertOneAsync(user);


            //// email newly created user
            //         Email em = new Email();
            //         Email.MailResult res = em.SendViaKmbiSmtp(new Email.MailParam {
            //             to = user.email,
            //             subject = "IT Department Notification",
            //             body = @"Hello, " + user.fullname +
            //             @"<br><br>" +
            //             @"The IT Department has registered your profile and created your account to the system. Before you login and begin working with the application, please confirm your account by clicking on this link:" +
            //             @"<br><br>" +
            //             @"http://localhost:5000/api/users/verify/" + user.verificationToken
            //         });



        }

        public void remove(String email)
        {
            var query = Builders<resetPassword>.Filter.Eq(e => e.email, email);
            var result = _db.GetCollection<resetPassword>("resetPassword").DeleteOneAsync(query);

        }

        //reset Password
        public void updatePassword(String email, users p)
        {

            var filter = Builders<users>.Filter.Eq(e => e.email, p.email);
            var update = Builders<users>.Update
                .Set("password", BCrypt.Net.BCrypt.HashPassword(p.password));
            var result = _db.GetCollection<users>("users").UpdateOneAsync(filter, update);
            remove(p.email);

        }

        public users getCredential(String email)
        {
            // p.email = email;
            var query = Builders<users>.Filter.Eq(e => e.email, email);
            var result = _db.GetCollection<users>("users").Find(query).ToListAsync();
            return result.Result.FirstOrDefault();

        }

        public resetPassword checkEmailToken(String token, string email)
        {
            var q1 = Builders<resetPassword>.Filter.Eq(e => e.token, token);
            var q2 = Builders<resetPassword>.Filter.Eq(e => e.email, email);
            var query = Builders<resetPassword>.Filter.And(q1, q2);
            var result = _db.GetCollection<resetPassword>("resetPassword").Find(query).ToListAsync();
            return result.Result.FirstOrDefault();

        }

    }
}
