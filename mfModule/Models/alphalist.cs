﻿using System;
using System.Collections.Generic;

namespace kmbi_core_master.coreMaster.Models
{
    public class alphalist
    {
        public Int32 year {get;set;}
        public String position {get;set;}
        public List<alphalistEmployee> employees {get;set;}
    }

    public class alphalistEmployee
    {
        public String tin {get;set;}
        public String employeeCode {get;set;}
        public String last {get;set;}
        public String first {get;set;}
        public String middle {get;set;}
        public String status {get;set;}
        public String jobGrade {get;set;}
        public List<alphalistEntry> entries {get;set;}
    }

    public class alphalistEntry
    {
        public String description {get;set;}
        public Double amount {get;set;}
        public Int32 order {get;set;}
        public List<alphalistEntryDetail> details {get;set;}
    }

    public class alphalistEntryDetail
    {
        public Int32 month {get;set;}
        public Double amount {get;set;}
    }
}
