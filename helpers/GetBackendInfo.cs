﻿using Microsoft.AspNetCore.Http;
using kmbi_core_master.helpers;
using Microsoft.Extensions.Options;
using kmbi_core_master.coreMaster.Models;
using System;
using MongoDB.Driver;
using MongoDB.Driver.Core.Clusters;
using Microsoft.AspNetCore.Hosting;
using MongoDB.Bson;
using kmbi_core_master.helpers.interfaces;
using System.IO;
using System.Runtime.InteropServices;
using System.Net;

namespace kmbi_core_master.helpers {

    public class  GetBackendInfoRequest {
        public string key {get;set;}
    }

    public class GetBackendInfoResponse {
        public String Version {get;set;}
        public String Message {get;set;}
        public String MongoServer {get;set;}
        public String MongoServerStatus {get;set;}
        public String AssemblyLastModified {get;set;}
        public String Environment {get;set;}
        public String FronEndLink {get;set;}
        public String BackEndLink {get;set;}
        public String ReportDirectoryAccessStatus {get;set;}
        public String ClientImagesAccessStatus {get;set;}
        public String ClientSignaturesAccessStatus {get;set;}
        public String SyncServer {get;set;}
        public String SyncServerStatus {get;set;}
        public String InternetConnection {get;set;}
    }

    public class GetBackendInfo {

        private IOptions<dbSettings> _mongoSetting;
        private IHostingEnvironment _env;
        private readonly IOptions<localFileRepositorySettings> _localFileRepositorySettings;

        private UrlGetter _urlGetter { get; }
        public IOptions<syncSettings> _syncSetting { get; }

        public GetBackendInfo(IOptions<dbSettings> mongoSetting, IHostingEnvironment env, UrlGetter urlGetter, IOptions<syncSettings> syncSetting, IOptions<localFileRepositorySettings> localFileRepositorySettings) {
            _mongoSetting = mongoSetting;
            _env = env;
            _urlGetter = urlGetter;
            _syncSetting = syncSetting;
            _localFileRepositorySettings = localFileRepositorySettings;
        }

        public GetBackendInfoResponse Handle(GetBackendInfoRequest request) {

            // get assembly last modified;
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            System.IO.FileInfo fileInfo = new System.IO.FileInfo(assembly.Location);
            var assLastModified = fileInfo.LastWriteTime;

            // check mongodb if running
            var connString = _mongoSetting.Value.MongoConnection;
            var client = new MongoClient(connString);
            var database = client.GetDatabase(_mongoSetting.Value.Database);
            bool isMongoRunning = database.RunCommandAsync((Command<BsonDocument>)"{ping:1}").Wait(5000);

            // get frontend and backend links
            var urlGetterResult = _urlGetter.get();
            var fronEndLink = urlGetterResult.Item1;
            var backEndLink = urlGetterResult.Item2;

            // check if has access on core folder
            var ReportDirectoryAccessStatus = "";

            try {
                if (_env.IsEnvironment("Production") || _env.IsEnvironment("Staging") || _env.IsEnvironment("Offline")) {
                    if(RuntimeInformation.IsOSPlatform(OSPlatform.Linux) || RuntimeInformation.IsOSPlatform(OSPlatform.OSX)) {
                        File.WriteAllText(_localFileRepositorySettings.Value.pathLinx + "/wwwroot/kmbidevreporeports/test.txt","test");
                        File.Delete(_localFileRepositorySettings.Value.pathLinx + "/wwwroot/kmbidevreporeports/test.txt");
                    } else {
                        File.WriteAllText(_localFileRepositorySettings.Value.pathWin + "/wwwroot/kmbidevreporeports/test.txt","test");
                        File.Delete(_localFileRepositorySettings.Value.pathWin + "/wwwroot/kmbidevreporeports/test.txt");
                    }
                }
                ReportDirectoryAccessStatus = "Report directory access is ok";
            } catch (Exception e) {
                ReportDirectoryAccessStatus = e.Message;
            }

            // check if has access on client images folder
            var ClientImagesAccessStatus = "";
            try {
                if (_env.IsEnvironment("Production") || _env.IsEnvironment("Staging") || _env.IsEnvironment("Offline")) {
                    if(RuntimeInformation.IsOSPlatform(OSPlatform.Linux) || RuntimeInformation.IsOSPlatform(OSPlatform.OSX)) {
                        File.WriteAllText(_localFileRepositorySettings.Value.pathLinx + "/wwwroot/kmbidevrepo/uploads/clients/pictures/test.txt","test");
                        File.Delete(_localFileRepositorySettings.Value.pathLinx + "/wwwroot/kmbidevrepo/uploads/clients/pictures/test.txt");
                    } else {
                        File.WriteAllText(_localFileRepositorySettings.Value.pathWin + "/wwwroot/kmbidevrepo/uploads/clients/pictures/test.txt","test");
                        File.Delete(_localFileRepositorySettings.Value.pathWin + "/wwwroot/kmbidevrepo/uploads/clients/pictures/test.txt");
                    }
                    
                }
                ClientImagesAccessStatus = "Client Images access is ok";
            } catch (Exception e) {
                ClientImagesAccessStatus = e.Message;
            }

            // check if has access on client signatures folder
            var ClientSignaturesAccessStatus = "";
            try {
                if (_env.IsEnvironment("Production") || _env.IsEnvironment("Staging") || _env.IsEnvironment("Offline")) {
                    if(RuntimeInformation.IsOSPlatform(OSPlatform.Linux) || RuntimeInformation.IsOSPlatform(OSPlatform.OSX)) {
                        File.WriteAllText(_localFileRepositorySettings.Value.pathLinx + "/wwwroot/kmbidevrepo/uploads/clients/signatures/test.txt","test");
                        File.Delete(_localFileRepositorySettings.Value.pathLinx + "/wwwroot/kmbidevrepo/uploads/clients/signatures/test.txt");
                    } else {
                        File.WriteAllText(_localFileRepositorySettings.Value.pathWin + "/wwwroot/kmbidevrepo/uploads/clients/signatures/test.txt","test");
                        File.Delete(_localFileRepositorySettings.Value.pathWin + "/wwwroot/kmbidevrepo/uploads/clients/signatures/test.txt");
                    }
                }
                ClientSignaturesAccessStatus = "Client Signatures access is ok";
            } catch (Exception e) {
                ClientSignaturesAccessStatus = e.Message;
            }

            // sync server info
            var syncConnString = _syncSetting.Value.syncTo;
            //var syncDb = _syncSetting.Value.database;

            bool syncIsMongoRunning = false;


            //try {
            //    var syncClient = new MongoClient(syncConnString);
            //    var syncDatabase = syncClient.GetDatabase(_mongoSetting.Value.Database);
            //    syncIsMongoRunning = syncDatabase.RunCommandAsync((Command<BsonDocument>)"{ping:1}").Wait(5000);
            //} catch {

            //}

            try
            {
                var syncClient = new MongoClient(syncConnString);
                var syncDatabase = syncClient.GetDatabase(_mongoSetting.Value.Database);
                syncIsMongoRunning = syncDatabase.RunCommandAsync((Command<BsonDocument>)"{ping:1}").Wait(5000);
            }
            catch
            {

            }

            // internet connection

            var hasNetConnection = false;
            try
            {
                using (var netClient = new WebClient())
                using (netClient.OpenRead("https://www.google.com")) 
                    hasNetConnection = true;
            } catch {
                
            }

            // get version

            var vers = System.IO.File.ReadAllText("dunamis-version");

            if(request.key == "") {
                return new GetBackendInfoResponse {
                    Version = vers, 
                    Message = "Hello KMBI",
                    AssemblyLastModified = assLastModified.ToString("MM/dd/yyyy HH:mm:ss"),
                    MongoServer = connString.Replace("dev","*****").Replace("ojzyj1p2","*****"),
                    MongoServerStatus = (isMongoRunning) ? "UP" : "DOWN",
                    Environment = _env.EnvironmentName,
                    FronEndLink = fronEndLink,
                    BackEndLink = backEndLink,
                    ReportDirectoryAccessStatus = ReportDirectoryAccessStatus,
                    ClientImagesAccessStatus = ClientImagesAccessStatus,
                    ClientSignaturesAccessStatus = ClientSignaturesAccessStatus,
                    SyncServer = _syncSetting.Value.syncTo.Replace("dbdev","*****").Replace("8yWRKkJuhx3bngxx", "*****").Replace("test","kmbi").Replace("version3-shard", ""),
                    SyncServerStatus = (syncIsMongoRunning) ? "UP" : "DOWN",
                    InternetConnection = (hasNetConnection) ? "UP" : "DOWN"
                };
            } else {
                return new GetBackendInfoResponse {};
            }
        }
    }
}
