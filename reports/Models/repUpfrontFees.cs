﻿using kmbi_core_master.coreMaster.Models;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.reports.Models
{

    public class repUpfrontFees
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string id { get; set; }
        public String referenceId { get; set; }

        public List<branch> branch { get; set; }
        public List<centerInstance> center { get; set; }
        
        public List<loanMembers> members { get; set; }
        
        public String preparedBy {get;set;}
        public String checkedBy {get;set;}
        public String approvedBy {get;set;}


        public Int32 newLoan {get;set;}
        public Int32 reLoan {get;set;}
        public Int32 dependents {get;set;}
        public Double miPay {get;set;}
        public Double miPA {get;set;}

        public Double cbu {get;set;}
        public Double processingFee {get;set;}
        public Double miPayable {get;set;}
        public Double miFee {get;set;}
        public Double dst {get;set;}
        public Double total {get;set;}
    }
}
