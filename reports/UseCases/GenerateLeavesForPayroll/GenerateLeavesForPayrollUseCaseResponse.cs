namespace UseCases.GenerateLeavesForPayroll {
    public class GenerateLeavesForPayrollUseCaseResponse {
        public LeaveItem[] items {get;set;}
        public LeaveItemDet[] dets {get;set;}
    }
}