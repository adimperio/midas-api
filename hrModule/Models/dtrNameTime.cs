﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using kmbi_core_master.coreMaster.Models;

namespace kmbi_core_master.hrModule.Models
{
    public class dtrNameTime
    {
        public String picture {get;set;}
        public String name { get; set; }
        public String slug {get;set;}
        public String code {get;set;}
        public name name2 { get; set; }
        public String extension {get;set;}
        public String status {get;set;}
        public string position {get;set;}
        public DateTime? timeIn { get; set; }        
        public DateTime? timeOut { get; set; }
        public String remarks { get; set; }
    }
}
