﻿using System;
using System.Collections.Generic;

namespace kmbi_core_master.reports.Models
{

    public class repDstModel
    {
        public String branchName { get; set; }
        public List<repDstModelLine> lines { get; set; }        
        public String tin {get;set;}
    }

    public class repDstModelLine
    {
        public Int32 seqNo {get;set;}
        public String centerName {get;set;}
        public DateTime dateOfRelease {get;set;}
        public DateTime dateOfMaturity {get;set;}
        public String atc {get;set;}
        public Double amountRelease {get;set;}
        public Double dstCollected {get;set;}
    }


}
