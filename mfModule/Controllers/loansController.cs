﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Filters;
using System;
using Microsoft.AspNetCore.Cors;
using System.Linq;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using kmbi_core_master.helpers.interfaces;

namespace kmbi_core_master.coreMaster.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/loans")]
    [usersAuth]
    public class loansController : Controller
    {
        private readonly iloanRepository _Repository;
        private readonly iusersRepository _logInfo;
        iupload _upload;
        private string token;


        public loansController(iloanRepository Repositor, iusersRepository logInfo, iupload upload)
        {
            this._Repository = Repositor;
            _logInfo = logInfo;
            _upload = upload;
        }

        public void logger(string message)
        {
            //Insert Log//
            token = HttpContext.Request.Headers["token"];
            var lInfo = _logInfo.getLoginInfo(token);
            string log = message;
            global.createLog(lInfo.id, lInfo.fullname, lInfo.employment.branch, log);
            //Insert Log//
        }

        // GET: 
        [HttpGet]
        public IEnumerable<loans> GetAll()
        {
            var c = _Repository.allLoans();
            return c;

        }

        //GET id
        [HttpGet("{id}")]
        public IActionResult getId(string id)
        {

            var item = _Repository.getId(id);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }



        [HttpGet("branchId/{branchId}")]
        public IActionResult getbranchId(string branchId)
        {

            var item = _Repository.getBranchId(branchId);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }


        [HttpGet("getLoansByBranchId")]
        public IActionResult getLoansByBranchId([FromQuery] string branchId)
        {

            var item = _Repository.getLoansByBranchId(branchId);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        //GET centerSlug
        [HttpGet("centerSlug/{centerSlug}")]
        public IActionResult getCenterSlug(string centerSlug)
        {

            var item = _Repository.getCenterSlug(centerSlug);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        [HttpGet("loanByCenterSlug/{centerSlug}")]
        public IActionResult getLoanByCenter(string centerSlug)
        {

            var item = _Repository.getLoanByCenter(centerSlug);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        [HttpGet("slug/{slug}")]
        public IActionResult getSlug(string slug)
        {

            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        [HttpGet("loanType/{name}")]
        public IActionResult getName(string name)
        {

            var item = _Repository.getName(name);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }


        //GET id
        [HttpGet("clientSlug/{slug}")]
        public IActionResult getClientSlug(string slug)
        {

            var item = _Repository.getClientSlug(slug);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }


        [HttpPost]
        //[errorHandler]
        public IActionResult create([FromBody] loans b)
        {
            try
            {
                _Repository.add(b);
                logger("Insert new loan " + b.loanType.name);

                return Ok(new
                {
                    type = "success",
                    message = b.loanType.name + " created successfully.",
                    slug = b.slug
                });
            }
            catch (Exception x)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = x.Message
                });
            }

        }

        [HttpPost("multiple")]
        //[errorHandler]
        public IActionResult create([FromBody]loans[] loans)
        {
            try
            {
                var statuses = new List<Tuple<String, String, String>>();
                loans.ToList().ForEach(x=>{
                    var loan = _Repository.getSlug(x.slug);
                    if(loan!=null) {
                        //existing
                        _Repository.update(x.Id, x);
                        statuses.Add(new Tuple<String, String, String>("update", loan.referenceId, loan.slug));
                    } else {
                        //not existing
                        _Repository.add(x);
                        var c = _Repository.getSlug(x.slug);
                        if(c!=null) {
                            statuses.Add(new Tuple<String, String, String>("create", c.referenceId, c.slug));
                        } else {
                            statuses.Add(new Tuple<String, String, String>("failed", x.referenceId, x.slug));
                        }

                    }
                });
               

                foreach(var x in loans) {
                    logger("loan sync " + x.loanType.name + " " + x.centerName + " " + x.referenceId + " succesfull");
                }

                return Ok(new
                {
                    type = "success",
                    message = "Loans sycing successful",
                    details = new {
                        create = statuses.Where(x=>x.Item1 == "create").Select(x=>new {slug=x.Item3, referenceId=x.Item2}),
                        update = statuses.Where(x=>x.Item1 == "update").Select(x=>new {slug=x.Item3, name=x.Item2}),
                        failed = statuses.Where(x=>x.Item1 == "failed").Select(x=>new {slug=x.Item3, name=x.Item2})
                    }
                });
            }
            catch (Exception x)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = x.Message
                });
            }

        }

        [HttpPost("updateCbuInterest")]
        //[errorHandler]
        public IActionResult updateCbuInterest([FromBody] List<string> p, [FromQuery] string type, [FromQuery] DateTime transactedAt, [FromQuery] string loanId)
        {
            try
            {

                _Repository.updateCbuInterest(p, type, transactedAt, loanId);
                string msg = "";
                if (type == "increment")
                {
                    msg = "Client CBUs incremented by 1%";
                }
                else
                {
                    msg = "Client CBUs decremented by 1%";
                }

                return Ok(new
                {
                    type = "success",
                    message = msg
                });

            }
            catch (Exception x)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = x.Message
                });
            }
        }


        //Update New Schedule
        [HttpPut("newSchedule")]
        public IActionResult newSchedule([FromQuery] string refId)
        {

            _Repository.getNewSchedule(refId);
            return Ok(new
            {
                type = "success"
            });
        }


        [HttpPut("{id}")]
        public IActionResult update(string id, [FromBody] loans b)
        {
            var item = _Repository.getId(id);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _Repository.update(id, b);

            logger("Modified loan " + b.loanType.name);

            return Ok(new
            {
                type = "success",
                message = b.loanType.name + " updated successfully."
            });
        }


        [HttpPut("changeStatus/id={id}")]
        public IActionResult updateStatus(string id, [FromBody] loans b)
        {
            var item = _Repository.getId(id);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _Repository.updateStatus(id, b);

            logger("Changed loan status to " + b.status);

            return Ok(new
            {
                type = "success",
                message = "Loan status changed to " + b.status
            });
        }


        [HttpPut("changeCenterLoanCycle/slug={slug}")]
        public IActionResult updateCenterLoanCycle(string slug, [FromBody] loans b)
        {
            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _Repository.updateCenterLoanCycle(slug, b);

            logger("Changed center loan cycle to " + b.centerLoanCycle);

            return Ok(new
            {
                type = "success",
                message = "Center Loan Cycle changed to " + b.centerLoanCycle
            });
        }


        [HttpPut("slug/{slug}")]
        public IActionResult slugupdate(string slug, [FromBody] loans b)
        {
            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _Repository.updateSlug(slug, b);

            logger("Modified loan " + b.loanType.name);

            return Ok(new
            {
                type = "success",
                message = b.loanType.name + " updated successfully."
            });
        }



        [HttpDelete("{id}")]
        public IActionResult delete(string id)
        {

            var item = _Repository.getId(id);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.remove(id);

            logger("Delete loan " + item.loanType.name);

            //Insert Deletions
            global.insertDeletion("loans", item.slug, item.branchSlug);

            return Ok(new
            {
                type = "success",
                message = item.loanType.name + " deleted successfully."
            });
        }


        [HttpDelete("slug/{slug}")]
        public IActionResult slugdelete(string slug)
        {

            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.removeSlug(slug);

            logger("Delete loan " + item.loanType.name);

            //Insert Deletions
            global.insertDeletion("loans", slug, item.branchSlug);

            return Ok(new
            {
                type = "success",
                message = item.loanType.name + " deleted successfully."
            });
        }


        [HttpPut("changeHealth")]
        public IActionResult changeHealth([FromQuery] string refId, [FromQuery] string slug, [FromBody] loanMembers p)
        {

            _Repository.changeHealth(refId, slug, p);
            return Ok(new
            {
                type = "success"
            });
        }


        [HttpGet("loansCollectionDate")]
        public IActionResult collectionDate([FromQuery] DateTime collDate, [FromQuery] string branchSlug)
        {

            var item = _Repository.collectionDate(collDate, branchSlug);
            if (item == null || item.Count == 0)
            {
                return new JsonStringResult("[]");
            }

            var c = item[0].members[0].schedule;
            if (c.Count < 1)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        [HttpGet("loansCollectionDateRaw")]
        public IActionResult collectionDateRaw([FromQuery] DateTime collDate, [FromQuery] string branchSlug)
        {

            var item = _Repository.collectionDateRaw(collDate, branchSlug);
            if (item == null || item.Count == 0)
            {
                return new JsonStringResult("[]");
            }

            var c = item[0].members[0].schedule;
            if (c.Count < 1)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        [HttpGet("byStatus")]
        public IActionResult getByStatus([FromQuery] string branchId, [FromQuery] string status)
        {

            var item = _Repository.getByStatus(branchId, status);
            
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        [HttpGet("disbursementDate")]
        public IActionResult disbursementDate([FromQuery] DateTime disbDate, [FromQuery] string branchSlug)
        {

            var item = _Repository.disbDate(disbDate, branchSlug);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        [HttpGet("getDay")]
        public IActionResult dayWeek([FromQuery] int dayOfWeek, [FromQuery] string branchSlug)
        {

            var item = _Repository.dayWeek(dayOfWeek, branchSlug);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        [HttpGet("firstPaymentDate")]
        public IActionResult firstPaymentDate([FromQuery] DateTime fDate, [FromQuery] string branchSlug)
        {

            var item = _Repository.fDate(fDate, branchSlug);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        [HttpGet("lastPaymentDate")]
        public IActionResult lastPaymentDate([FromQuery] DateTime pDate)
        {

            var item = _Repository.lastPDate(pDate);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        [HttpGet("getByCenterAndLoanType")]
        public IActionResult getByCenterLoanType([FromQuery] string centerSlug, [FromQuery] string loanTypeId, [FromQuery] string status)
        {

            var item = _Repository.getByCenterLoanType(centerSlug, loanTypeId, status);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }


        [HttpPost("getByClientSlugs")]
        public IActionResult getByClientSlugs([FromBody] List<string> clientSlugs, [FromQuery] string status)
        {

            var item = _Repository.getByClientSlugs(clientSlugs, status);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        [HttpPost("getLoanBalance")]
        public IActionResult getLoanBalance([FromBody] List<string> clientSlugs, [FromQuery] string status)
        {

            var item = _Repository.getBalances(clientSlugs, status);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }


        [HttpGet("loanPortfolioByCenter")]
        public IActionResult getLoanPortfoliobyCenter([FromQuery] string centerSlug)
        {

            var item = _Repository.getLoanPortfoliobyCenter(centerSlug);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        [HttpGet("loanPortfolioByUnit")]
        public IActionResult getLoanPortfoliobyUnit([FromQuery] string unitSlug)
        {

            var item = _Repository.getLoanPortfoliobyUnit(unitSlug);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        [HttpGet("loanPortfolioByPo")]
        public IActionResult getLoanPortfoliobyPo([FromQuery] string poSlug)
        {

            var item = _Repository.getLoanPortfoliobyPo(poSlug);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        [HttpGet("get")]
        public IActionResult getByTypeBranchStatus([FromQuery] string type, [FromQuery] string branch, [FromQuery] string status)
        {

            var item = _Repository.getByTypeBranchStatus(type, branch, status);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }


        [HttpGet("getLoans")]
        public IActionResult getLoansByBranchTypeStatus([FromQuery] string type, [FromQuery] string branch, [FromQuery] string status)
        {

            var item = _Repository.getLoansByBranchTypeStatus(type, branch, status);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        [HttpGet("matured")]
        public IActionResult getMatured([FromQuery] DateTime date, [FromQuery] string branchSlug)
        {

            var item = _Repository.getMatured(date, branchSlug);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        [HttpPost("for-payment")]
        public IActionResult getAllforPayment([FromBody] List<string> clientSlugs)
        {

            var item = _Repository.getAllforPayment(clientSlugs);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        [HttpGet("loanPaymentsForECR")]
        public IActionResult getloanPaymentsForECR([FromQuery] string loanSlug)
        {

            var item = _Repository.getLoansAndPayments(loanSlug);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        [HttpGet("misReport")]
        public IActionResult getmisIntegration([FromQuery] string branchSlug, [FromQuery] int year, [FromQuery] int month)
        {
            var monthName = "";
            if (month == 1)
                monthName = "JANUARY";
            if (month == 2)
                monthName = "FEBRUARY";
            if (month == 3)
                monthName = "MARCH";
            if (month == 4)
                monthName = "APRIL";
            if (month == 5)
                monthName = "MAY";
            if (month == 6)
                monthName = "JUNE";
            if (month == 7)
                monthName = "JULY";
            if (month == 8)
                monthName = "AUGUST";
            if (month == 9)
                monthName = "SEPTEMBER";
            if (month == 10)
                monthName = "OCTOBER";
            if (month == 11)
                monthName = "NOVEMBER";
            if (month == 12)
                monthName = "DECEMBER";

            var item = _Repository.getMonthlyReport(branchSlug, year, month);
            var filename = "kmbidevreporeports/" + item.FirstOrDefault().branch + "_" + monthName + "_" + year + ".xlsx";
            OfficeOpenXml.ExcelPackage ep = new OfficeOpenXml.ExcelPackage();
            ExcelWorksheet ew = ep.Workbook.Worksheets.Add(item.FirstOrDefault().branch + "_" + monthName + "_" + year + ".xlsx");
            convertRepMiToSheet(item, ew);
            byte[] bytes = ep.GetAsByteArray();
            ep.Dispose();

            var link = _upload.upload(bytes, filename);
            // return Redirect(link);4
            return new ObjectResult(link);
            //if (item == null)
            //{
            //    return new JsonStringResult("[]");
            //}

            //return new ObjectResult(item);
        }

        private void convertRepMiToSheet(IEnumerable<misIntegration> rep, ExcelWorksheet ew)
        {
            // ew.View.FreezePanes(1, 13);
            //ew.Cells["C2"].Value = "Branch & Unit: " + rep.branchName + " - " + rep.unitName;
            //ew.Cells["C3"].Value = "Center Number: " + rep.centerCode;
            //ew.Cells["C4"].Value = "Disbursement Date: " + rep.month;

            
            ew.Cells["A1"].Value = "Branch";
            ew.Cells["B1"].Value = "Center";
            ew.Cells["C1"].Value = "Disbursement Date";
            ew.Cells["D1"].Value = "Maturity Date";
            ew.Cells["E1"].Value = "Cycle";
            ew.Cells["F1"].Value = "Loan Amount";
            ew.Cells["G1"].Value = "Last Name";
            ew.Cells["H1"].Value = "First Name";
            ew.Cells["I1"].Value = "Middle Name";
            ew.Cells["J1"].Value = "Birthdate";
            ew.Cells["K1"].Value = "Gender";
            ew.Cells["L1"].Value = "Type";
            ew.Cells["M1"].Value = "Civil Status";

            

            //ew.Cells["A6:BX7"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //ew.Cells["A6:BX7"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            //ew.Cells["A6:BX7"].Style.WrapText = true;

            //ew.Column(1).Width = 5;
            //ew.Column(2).Width = 15;
            //ew.Column(3).Width = 15;
            //ew.Column(4).Width = 15;
            //ew.Column(5).Width = 30;
            //ew.Column(6).Width = 14;
            //ew.Column(7).Width = 14;
            //ew.Column(8).Width = 14;
            //ew.Column(9).Width = 14;
            //ew.Column(10).Width = 14;
            //ew.Column(11).Width = 14;
            //ew.Column(12).Width = 19;
            //ew.Column(13).Width = 16;
            //ew.Column(14).Width = 30;
            //ew.Column(15).Width = 19;
            //ew.Column(16).Width = 19;

            //for (Int32 _ctr = 0; _ctr <= 16; _ctr++)
            //{
            //    ew.Column(17 + (3 * _ctr)).Width = 30;
            //    ew.Column(18 + (3 * _ctr)).Width = 15;
            //    ew.Column(19 + (3 * _ctr)).Width = 15;
            //}


            //ew.Column(73).Width = 18;
            //ew.Column(74).Width = 18;
            //ew.Column(75).Width = 18;
            //ew.Column(76).Width = 18;



            //lines

            var ctr = 2;

            //var clientCountSum = 0;
            //var spouseCountSum = 0;
            //var childCountSum = 0;
            //var parentCountSum = 0;
            //var siblingCountSum = 0;
            //var premiumForFirstLifeSum = 0.0;
            //var personalAccidentSum = 0.0;
            //var feeRetainedByKmbiSum = 0.0;
            //var paidByClientSum = 0.0;

            foreach (var l in rep)
            {
                ew.Cells["A" + ctr].Value = l.branch;
                ew.Cells["A" + ctr].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ew.Cells["B" + ctr].Value = l.center;
                ew.Cells["B" + ctr].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ew.Cells["C" + ctr].Value = Convert.ToDateTime(l.disbursementDate).ToString("yyyy-MM-dd");
                ew.Cells["C" + ctr].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                //ew.Cells["C" + ctr].Style.Numberformat = "yyyy-MM-dd";
                ew.Cells["D" + ctr].Value = Convert.ToDateTime(l.maturityDate).ToString("yyyy-MM-dd");
                ew.Cells["D" + ctr].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ew.Cells["E" + ctr].Value = l.cycle;
                ew.Cells["E" + ctr].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ew.Cells["F" + ctr].Value = l.loanAmount;
                ew.Cells["F" + ctr].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ew.Cells["G" + ctr].Value = l.lastName;
                ew.Cells["G" + ctr].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ew.Cells["H" + ctr].Value = l.firstName;
                ew.Cells["H" + ctr].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ew.Cells["I" + ctr].Value = l.middleName;
                ew.Cells["I" + ctr].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ew.Cells["J" + ctr].Value = Convert.ToDateTime(l.birthdate).ToString("yyyy-MM-dd");
                ew.Cells["J" + ctr].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ew.Cells["K" + ctr].Value = l.gender;
                ew.Cells["K" + ctr].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ew.Cells["L" + ctr].Value = l.type;
                ew.Cells["L" + ctr].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ew.Cells["M" + ctr].Value = l.civilStatus;
                ew.Cells["M" + ctr].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                ctr++;
            }

            ew.Column(1).AutoFit();
            ew.Column(2).AutoFit();
            ew.Column(3).AutoFit();
            ew.Column(4).AutoFit();
            ew.Column(5).AutoFit();
            ew.Column(6).AutoFit();
            ew.Column(7).AutoFit();
            ew.Column(8).AutoFit();
            ew.Column(9).AutoFit();
            ew.Column(10).AutoFit();
            ew.Column(11).AutoFit();
            ew.Column(12).AutoFit();
            ew.Column(13).AutoFit();



            //ew.Cells["BP" + ctr].Value = clientCountSum;
            //ew.Cells["BQ" + ctr].Value = spouseCountSum;
            //ew.Cells["BR" + ctr].Value = childCountSum;
            //ew.Cells["BS" + ctr].Value = parentCountSum;
            //ew.Cells["BT" + ctr].Value = siblingCountSum;
            //ew.Cells["BU" + ctr].Value = premiumForFirstLifeSum;
            //ew.Cells["BV" + ctr].Value = personalAccidentSum;
            //ew.Cells["BW" + ctr].Value = feeRetainedByKmbiSum;
            //ew.Cells["BX" + ctr].Value = paidByClientSum;

            //ew.Cells["BU8:BX" + ctr.ToString()].Style.Numberformat.Format = "#,##0.00";

            //ctr += 2;

            //ew.Cells["E" + ctr.ToString()].Value = "Prepared by";
            //ew.Cells["F" + ctr.ToString()].Value = "Checked by";
            //ew.Cells["G" + ctr.ToString()].Value = "Approved by";

            //ctr += 1;

            //var sigs = _reporting.GetSignatories("mi-summary", rep.center.branchSlug, rep.center.unitSlug, rep.center.officerSlug);

            //ew.Cells["E" + ctr.ToString()].Value = sigs.preparedBy;
            //ew.Cells["F" + ctr.ToString()].Value = sigs.checkedBy;
            //ew.Cells["G" + ctr.ToString()].Value = sigs.approvedBy;

        }
    }
}
