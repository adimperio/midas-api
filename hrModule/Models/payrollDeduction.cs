using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace kmbi_core_master.coreMaster.Models {

    public class payrollDeduction {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id {get;set;}
        public String slug {get;set;}
        public IEnumerable<payrollDeductionLoanType> loanType {get;set;}
        public payrollDeductionEmployee employee {get;set;}
        public DateTime createdAt {get;set;}
        public DateTime updatedAt {get;set;}
    }

    public class payrollDeductionLoanType {
        public String slug {get;set;}
        public String name {get;set;}
        public Int32 deductionPeriod {get;set;}
        public Double amount {get;set;}
        public payrollDeductionAmortization amortization {get;set;}
        public Double balance {get;set;}
    }

    public class payrollDeductionAmortization {
        public payrollDeductionPeriod period {get;set;}
        public Double amount {get;set;}
    }

    public class payrollDeductionPeriod {
        public DateTime from {get;set;}
        public DateTime to {get;set;}
    }

    public class payrollDeductionEmployee {
        public String code {get;set;}
        public String slug {get;set;}
        public String name {get;set;}
    }

}