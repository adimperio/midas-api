using System;
using System.Collections.Generic;
using kmbi_core_master.coreMaster.Models;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace kmbi_core_master.reports.Models
{
    public class repEcr
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String _id {get;set;}
        public String centerDescription {get;set;}
        public String centerAddress {get;set;}
        public String assignedPA {get;set;}
        public Double loanAmount {get;set;}
        public Double interestRate {get;set;}
        public String interestType {get;set;}
        public String transactionNo {get;set;}
        public Int32 term {get;set;}
        public DateTime loanDate {get;set;}
        public List<loanMembers> members {get;set;}
        public Int32 cycleNo { get; set; }
        public centerInstance center {get;set;}
        public String preparedBy {get;set;}
        public String checkedBy {get;set;}
        public String approvedBy {get;set;}

        public DateTime firstPaymentDate {get;set;}
    }

    public class newRepEcr
    {
        List<ecrDetails> ecrDetails { get; set; }
    }
    public class ecrDetailsOutput
    {
        public string centerDescription { get; set; }
        public string centerAddress { get; set; }
        public string assignedPO { get; set; }
        public Int32 loanCycle { get; set; }
        public double interestRate { get; set; }
        public string interestType { get; set; }

        public DateTime paymentSchedule { get; set; }
        public List<ecrBatches> batches { get; set; }
        public List<ecrClientsOutput> clients { get; set; }
        public String preparedBy { get; set; }
        public String checkedBy { get; set; }
        public String approvedBy { get; set; }
    }

    public class ecrDetails
    {
        public string centerDescription { get; set; }
        public string centerAddress { get; set; }
        public string assignedPO { get; set; }
        public Int32 loanCycle { get; set; }
        public double interestRate { get; set; }
        public string interestType { get; set; }

        public DateTime paymentSchedule { get; set; }
        public List<ecrBatches> batches { get; set; }
        public List<ecrClients> clients { get; set; }
        public String preparedBy { get; set; }
        public String checkedBy { get; set; }
        public String approvedBy { get; set; }
    }

    public class ecrBatches
    {
        public Int32 weekNo { get; set; }
        
        public DateTime loanDate { get; set; }
        public DateTime maturityDate { get; set; }
        public double loanAmount { get; set; }
        public string referenceNo { get; set; }
    }

    public class ecrClientsOutput
    {
        public Int32 cycle { get; set; }
        public string name { get; set; }
        public string slug { get; set; }
        //public ecrBalances balances { get; set; }
        //public ecrCurrentDues dues { get; set; }
        //public ecrPastDues pastDues { get; set; }
        //public ecrTotalDues totalDues { get; set; }
        public double balancePrincipal { get; set; }
        public double balanceInterest { get; set; }
        public double balanceCbu { get; set; }
        public double currentPrincipal { get; set; }
        public double currentInterest { get; set; }
        public double currentCbu { get; set; }
        public double pastPrincipal { get; set; }
        public double pastInterest { get; set; }
        public double pastCbu { get; set; }
        public double totalPrincipal { get; set; }
        public double totalInterest { get; set; }
        public double totalCbu { get; set; }
        public double totalPrincipalInterest { get; set; }
        public double totalTotalDue { get; set; }
    }


    public class ecrClients
    {
        public Int32 cycle { get; set; }
        public string name { get; set; }
        public string slug { get; set; }
        public ecrBalances balances { get; set; }
        public ecrCurrentDues dues { get; set; }
        public ecrPastDues pastDues { get; set; }
        public ecrTotalDues totalDues { get; set; }

    }
    public class ecrBalances
    {
        public double principal { get; set; }
        public double interest { get; set; }
        public double cbu { get; set; }
    }
    public class ecrCurrentDues
    {
        public double principal { get; set; }
        public double interest { get; set; }
        public double cbu { get; set; }
    }
    public class ecrPastDues
    {
        public double principal { get; set; }
        public double interest { get; set; }
        public double cbu { get; set; }
    }
    public class ecrTotalDues
    {
        public double principal { get; set; }
        public double interest { get; set; }
        public double cbu { get; set; }
    }
}