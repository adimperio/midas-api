using System.Collections.Generic;
using Microsoft.Extensions.Options;
using System;
using MongoDB.Driver;
using MongoDB.Bson;
using kmbi_core_master.documentationModule.Models;

namespace kmbi_core_master.documentationModule.Service
{
    public class policyService
    {
        context _context;
        activityService logger;

        public policyService(IOptions<kmbi_core_master.coreMaster.Models.dbSettings> settings)
        {
            _context = new context(settings);
            logger = new activityService(settings);
        }

        public void log(String _log)
        {
            logger.log(_log);
        }

        public IEnumerable<policy> GetPolicies()
        {
            return _context.policies.GetAll();
        }
        public policy GetPolicyBySlug(String slug)
        {
            return _context.policies.GetOne(Builders<policy>.Filter.Eq("slug",slug));
        }

        public policy GetPolicySpecificSection(String policySlug, String sectionSlug)
        {
            var filterPolicy = Builders<policy>.Filter.Eq("slug",policySlug);
            var filterSection = Builders<policy>.Filter.Eq("sections.slug", sectionSlug);
            var filterCombine = Builders<policy>.Filter.And(filterPolicy,filterSection);

            var inc = Builders<policy>.Projection.Include("sections.$");

            var pol = _context.policies.Find(filterCombine,inc);

            if (pol!=null)
            {
                var sec = pol.sections.Find(t => t.slug == sectionSlug);

                if (sec!=null)
                {
                    pol.sections = new List<section> {sec};
                }
                else 
                {
                    pol.sections = new List<section>();
                }
                
            }
            return pol;
        }



        public Boolean InsertPolicy(policy policy)
        {
            Int32 secctr = 0;

            if(policy.sections != null)
            {
                foreach (section s in policy.sections)
                {
                    secctr = secctr + 1;
                    s.Id = ObjectId.GenerateNewId().ToString();
                    s.createdAt = DateTime.Now;
                    s.updatedAt = DateTime.Now;

                    s.order = secctr;
                    secctr += 1;
                }

                policy.createdAt = DateTime.Now;
                policy.updatedAt = DateTime.Now;

                var list = _context.policies.GetAll();
                var ord=0;
                foreach(policy x in list)
                {
                    ord+=1;
                }
                policy.order = ord + 1;

                _context.policies.Insert(policy);

                policy _pol = GetPolicyBySlug(policy.slug);
                if(_pol!=null)
                {
                    log("Insert new policy " + policy.name);
                    return true;
                }
                else
                {
                    return false;
                }
                
            }
            else
            {
                return false;
            }
            
        }

        public Boolean InsertSection(String slug, section section)
        {
            var filterPolicy = Builders<policy>.Filter.Eq("slug",slug);

            section.Id = ObjectId.GenerateNewId().ToString();

            var pol = _context.policies.GetOne(Builders<policy>.Filter.Eq("slug",slug));

            if(pol!=null)
            {
                section.order = pol.sections.Count + 1;
            }
            else
            {
                section.order = 0;
            }
            
            section.createdAt = DateTime.Now;
            section.updatedAt = DateTime.Now;


            var update = Builders<policy>
                .Update
                .CurrentDate("updatedAt")
                .AddToSet("sections", section);

            
            _context.policies.Update(filterPolicy,update);

            policy _pol = GetPolicySpecificSection(slug, section.slug);
            if(_pol.sections.Count > 0)
            {
                log("Insert new section " + section.title);
                return true;
            }
            else
            {
                return false;
            }

        }



        public Boolean UpdatePolicy(String slug, policy policy)
        {
            var filterPolicy = Builders<policy>.Filter.Eq("slug", slug);
            
            var update = Builders<policy>
                .Update
                .Set("name",policy.name)
                .CurrentDate("updatedAt");
            
            var IsSucess = _context.policies.Update(filterPolicy, update);

            if (IsSucess)
            {
                log("Modified policy " + policy.name);
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean UpdateSection(String policySlug, String sectionSlug, section section)
        {
            var filterPolicy = Builders<policy>.Filter.Eq("slug", policySlug);
            var filterSection = Builders<policy>.Filter.Eq("sections.slug", sectionSlug);
            var filterCombine = Builders<policy>.Filter.And(filterPolicy, filterSection);

            var update = Builders<policy>
                .Update
                .Set("sections.$.title",section.title)
                .Set("sections.$.content", section.content);

            var IsSuccess =  _context.policies.Update(filterCombine, update);

            if(IsSuccess)
            {
                log("Modified section " + section.title);
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean DeletePolicy(String slug)
        {
            var IsSucess = _context.policies.Remove(Builders<policy>.Filter.Eq("slug", slug));
            policy pol = GetPolicyBySlug(slug);
            if (IsSucess)
            {
                log("Delete policy " + pol.name);
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean DeleteSection(String policySlug, String sectionSlug)
        {
            var filterPolicy = Builders<policy>.Filter.Eq("slug", policySlug);
            var filterSection = Builders<section>.Filter.Eq("slug", sectionSlug);
            
            var update = Builders<policy>.Update.CurrentDate("updatedAt").PullFilter("sections",Builders<section>.Filter.Eq("slug", sectionSlug));

            policy pol = GetPolicySpecificSection(policySlug, sectionSlug);
            var IsSucess = _context.policies.Update(filterPolicy, update);

            if(IsSucess)
            {
                log("Delete section " + pol.sections[0].title);
                return true;
            }
            else
            {
                return false;
            }
                
        }
    }
}