using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Filters;
using Microsoft.AspNetCore.Cors;
using kmbi_core_master.documentationModule.iRepositories;
using kmbi_core_master.ticketing.IRepository;
using kmbi_core_master.ticketing.Models;
using System;
using kmbi_core_master.trainingModule.IRepository;
using Microsoft.AspNetCore.Http;
using Amazon.S3;
using System.IO;
using Amazon.S3.Model;
using Microsoft.Extensions.Options;
using Amazon;

namespace kmbi_core_master.ticketing.Controllers
{
  [EnableCors("allowCors")]
  [Route("api/issues")]
  public class IssueController : Controller
  {
    private readonly IIssueRepository _Repository;
    private readonly iusersRepository _logInfo;
    private string token;

    private awsSettings _settings;

    private static string _awsAccessKey;
    private static string _awsSecretKey;

    public IssueController(IIssueRepository Repository, iusersRepository logInfo, IOptions<awsSettings> settings)
    {
        this._Repository = Repository;
        _logInfo = logInfo;
        _settings = settings.Value;
        _awsAccessKey = _settings.accessKey;
        _awsSecretKey = _settings.secretKey;
    }

    public void logger(string message)
    {
        //Insert Log//
        token = HttpContext.Request.Headers["token"];
        var lInfo = _logInfo.getLoginInfo(token);
        string log = message;
        global.createLog(lInfo.id, lInfo.fullname, lInfo.employment.branch, log);
        //Insert Log//
    }

    // GET: 
    [HttpGet]
    public IEnumerable<IssueFull> all()
    {
      return _Repository.all();
    }

    //GET Slug
    [HttpGet("{slug}")]
    public IActionResult find(string slug)
    {

      var item = _Repository.find(slug);
      if (item == null) {
        return NotFound();
      }

      return new ObjectResult(item);
    }

    [HttpGet("{slug}/comment")]
    public IActionResult getBySlug(string slug)
    {
      var item = _Repository.getCommentBySlug(slug);
      if (item == null) {
        return NotFound();
      }

      return new ObjectResult(item);
    }

    // POST
    [errorHandler]
    [HttpPost]
    public IActionResult create([FromBody] Issue issue)
    {
      _Repository.create(issue);
      logger( "Insert new issue " + issue.title);
      return Ok(new
      {
        type = "success",
        message = "Issue created successfully.",
        slug = issue.slug
      });
    }

    // // PUT 
    [errorHandler]
    [HttpPut("{slug}")]
    public IActionResult update(string slug, [FromBody] Issue issue)
    {
        var item = _Repository.find(slug);
        if (item == null)
        {
            return BadRequest(new
            {
                type = "error",
                message = "No record found."
            });
        }

        _Repository.update(slug, issue);

        logger("Update issue " + issue.title);

        return Ok(new
        {
            type = "success",
            message = issue.title + " updated successfully."
        });
    }

    [errorHandler]
    [HttpPut("comment/{slug}")]
    public IActionResult updateComment(string slug, [FromBody] Comment comment)
    {
      var item = _Repository.getCommentBySlug(slug);
      if (item == null)
      {
          return BadRequest(new
          {
              type = "error",
              message = "No record found."
          });
      }

      _Repository.updateComment(slug, comment);

      logger("Update comment " + comment.employee);

      return Ok(new
      {
          type = "success",
          message = comment.employee + " updated successfully."
      });
    }

    // // DELETE
    [HttpDelete("{slug}")]
    public IActionResult delete(string slug)
    {

        var item = _Repository.find(slug);
        if (item == null)
        {
            return BadRequest(new
            {
                type = "error",
                message = "No record found."
            });
        }

        _Repository.delete(slug);

        logger("Delete issue " + item.title);

        return Ok(new
        {
            type = "success",
            message = item.title + " deleted successfully."
        });
    }

    [HttpPost("toS3")]
    public IActionResult UploadToS3(IFormFile file)
    {
      var client = new AmazonS3Client(_awsAccessKey, _awsSecretKey, RegionEndpoint.APSoutheast1);
      var _bucketName = "kmbi-ticket";
      string keyName = string.Format("kmbi-ticket/{0}", file.FileName);

      BinaryReader reader = new BinaryReader(file.OpenReadStream());
      Byte[] bytes = reader.ReadBytes((int)file.Length);
      
      Stream stream = new MemoryStream(new MemoryStream(bytes).ToArray());
      PutObjectResponse response = client.PutObjectAsync(new PutObjectRequest
      {
          BucketName = _bucketName,
          Key = keyName,
          InputStream = stream
      }).Result;

      return Ok(new
      {
          message = "Uploaded successfully",
          path = "https://s3-ap-southeast-1.amazonaws.com/" + _bucketName + "/" + keyName,
          type = file.ContentType,
          size = file.Length
      });

    }

  }
}
