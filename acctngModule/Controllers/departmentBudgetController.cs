﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Filters;
using Microsoft.AspNetCore.Cors;
using kmbi_core_master.acctngModule.Interface;
using kmbi_core_master.acctngModule.Models;
using kmbi_core_master.coreMaster;
using kmbi_core_master.coreMaster.IRepository;

namespace kmbi_core_master.acctngModule.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/departmentBudget")]
    [usersAuth]
    public class departmentBudgetController : Controller
    {
        private readonly idepartmentBudgetRepository _Repository;
        private readonly iusersRepository _logInfo;
        private string token;

        public departmentBudgetController(idepartmentBudgetRepository Repositor, iusersRepository logInfo)
        {
            this._Repository = Repositor;
            _logInfo = logInfo;
        }

        public void logger(string message)
        {
            //Insert Log//
            token = HttpContext.Request.Headers["token"];
            var lInfo = _logInfo.getLoginInfo(token);
            string log = message;
            global.createLog(lInfo.id, lInfo.fullname, lInfo.employment.branch, log);
            //Insert Log//
        }

        // GET: 
        [HttpGet]
        public IEnumerable<departmentBudget> GetAll()
        {
            var c = _Repository.all();
            return c;

        }

        //GET  Slug
        [HttpGet("{slug}")]
        public IActionResult getSlug(string slug)
        {

            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        [HttpGet("getbyYear")]
        public IActionResult getbyYear([FromQuery] int year)
        {

            var item = _Repository.getbyYear(year);

            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        [HttpPost]
        public IActionResult deleteInsertByDept([FromQuery] string name, [FromQuery] int year, [FromBody] departmentBudget b)
        {
            

            _Repository.removeByDept(name, year, b);

            logger("Insert new department budget " + b.code);

            return Ok(new
            {
                type = "success",
                message = "Department Budget created successfully.",
                slug = b.slug
            });

        }


        //[HttpPost]
        //public IActionResult create([FromBody] departmentBudget b)
        //{

        //    _Repository.add(b);

        //    logger("Insert new department budget " + b.code);

        //    return Ok(new
        //    {
        //        type = "success",
        //        message = "Department Budget created successfully.",
        //        slug = b.slug
        //    });

        //}

        //// POST Budget
        //[HttpPost("insertBudget/deptBudgetSlug={slug}")]
        //public IActionResult addCredit([FromBody] budBudget u, string slug)
        //{
        //    _Repository.addBudget(u, slug);

        //    logger("Insert new department budget credit " + u.name);

        //    return Ok(new
        //    {
        //        type = "success",
        //        message = u.name + " created successfully.",
        //        id = u.Id
        //    });
        //}

        
        [HttpPut("{slug}")]
        public IActionResult update(string slug, [FromBody] departmentBudget b)
        {
            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _Repository.update(slug, b);

            logger("Modified department budget " + b.code); 

            return Ok(new
            {
                type = "success",
                message = "Department Budget updated successfully."
            });
        }

        //// PUT Budget
        //[errorHandler]
        //[HttpPut("updateBudget/deptBudgetSlug={slug}&budgetId={id}")]
        //public IActionResult updateCredit(string slug, string id, [FromBody] budBudget u)
        //{
        //    var item = _Repository.getId(id);
        //    if (item == null)
        //    {
        //        return BadRequest(new
        //        {
        //            type = "error",
        //            message = "No record found"
        //        });
        //    }
        //    _Repository.updateBudget(slug, id, u);

        //    logger("Modified department budget credit " + u.name);

        //    return Ok(new
        //    {
        //        type = "success",
        //        message = u.name + " updated successfully."
        //    });

        //}

        
        [HttpDelete("{slug}")]
        public IActionResult delete(string slug)
        {

            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.remove(slug);

            logger("Delete department budget " +  item.code);

            return Ok(new
            {
                type = "success",
                message = item.code + " deleted successfully."
            });
        }

        ////DELETE credit
        //[HttpDelete("removeBudget/deptBudgetSlug={slug}&budgetId={id}")]
        //public IActionResult deleteCredit(string slug, string id)
        //{
        //    var item = _Repository.getId(id);
        //    if (item == null)
        //    {
        //        return BadRequest(new
        //        {
        //            type = "error",
        //            message = "No record found."
        //        });
        //    }

        //    _Repository.removeBudget(slug, id);

        //    logger("Delete department budget credit" + item.budget[0].name);

        //    return Ok(new
        //    {
        //        type = "success",
        //        message = item.budget[0].name + " deleted successfully."
        //    });
        //}


    }
}
