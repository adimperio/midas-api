﻿using Microsoft.Extensions.Options;

using System;
using System.Collections.Generic;
using System.Linq;

using MongoDB.Bson;
using MongoDB.Driver;

using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.acctngModule.Models;
using kmbi_core_master.acctngModule.Interface;

namespace kmbi_core_master.acctngModule.Repository
{
    public class budgetRepository : dbCon, ibudgetRepository
    {
        
        public budgetRepository(IOptions<dbSettings> settings) : base(settings) {}

        public void add(budget b)
        {

            b.createdAt = DateTime.Now;
            b.updatedAt = DateTime.Now;
            var coll = _db.GetCollection<ewt>("budget");
            _db.GetCollection<budget>("budget").InsertOneAsync(b);
        }

    
        public IEnumerable<budget> all()
        {
            var c = _db.GetCollection<budget>("budget").Find(new BsonDocument()).ToListAsync();
            return c.Result;
        }

        public budget getSlug(String slug)
        {
            var query = Builders<budget>.Filter.Eq(e => e.slug, slug);
            var c = _db.GetCollection<budget>("budget").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }
       
        public void update(String slug, budget p)
        {
            p.slug = slug;
            var query = Builders<budget>.Filter.Eq(e => e.slug, slug);
            var update = Builders<budget>.Update
                .Set("name", p.name)
                .Set("description", p.description)
                .Set("code", p.code)
                .CurrentDate("updatedAt");

            var branch = _db.GetCollection<budget>("budget").UpdateOneAsync(query, update);

        }
        public void remove(String slug)
        {
            var query = Builders<budget>.Filter.Eq(e => e.slug, slug);
            var result = _db.GetCollection<budget>("budget").DeleteOneAsync(query);
        }
    }
}
