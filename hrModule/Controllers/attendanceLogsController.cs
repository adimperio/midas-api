﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Filters;
using kmbi_core_master.hrModule.Interface;
using kmbi_core_master.hrModule.Models;
using Microsoft.AspNetCore.Cors;

using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using kmbi_core_master.helpers.interfaces;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using kmbi_core_master.coreMaster.Models;
using Microsoft.Extensions.Options;
using System.Data.SqlClient;
using Amazon.S3.Transfer;
using Amazon.S3;
using Amazon;
using System.Data.OleDb;
using OfficeOpenXml;
using System.Data;

namespace kmbi_core_master.coreMaster.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/attendanceLogs")]
    [usersAuth]
    public class attendanceLogsController : Controller
    {
        private awsSettings _settings;

        private static string _awsAccessKey;
        private static string _awsSecretKey;

        private readonly iattendanceLogsRepository _Repository;
        private readonly iupload _upload;
        private IHostingEnvironment hostingEnv;
        IOptions<biometricSettings> _bio;

        private static readonly string _bucketName = "kmbidevrepo";

        public attendanceLogsController(iattendanceLogsRepository Repository, iupload upload, IHostingEnvironment env, IOptions<biometricSettings> bio, IOptions<awsSettings> settings)
        {
            this._Repository = Repository;
            this._upload = upload;
            this.hostingEnv = env;
            _bio = bio;
            _settings = settings.Value;
            _awsAccessKey = _settings.accessKey;
            _awsSecretKey = _settings.secretKey;
        }

        [HttpGetAttribute("get/all")]
        public IActionResult get([FromQueryAttribute]DateTime from, [FromQueryAttribute]DateTime to)
        {
            IEnumerable<attendanceLogs> list = _Repository.getAttendanceLogsPerDateRange(from,to);
            return Ok(list);
        }

        [HttpGetAttribute("get/{employeeCode}")]
        public IActionResult get(String employeeCode, [FromQueryAttribute]DateTime from, [FromQueryAttribute]DateTime to)
        {
            IEnumerable<attendanceLogs> list = _Repository.getAttendanceLogsPerEmployeePerDateRange(employeeCode,from,to);
            return Ok(list);
        }

        [HttpGetAttribute("download/all/today")]
        public IActionResult download()
        {
            downloadResult result = _Repository.download(DateTime.Now,DateTime.Now);
            return Ok(result);
        }

        [HttpGetAttribute("download/all/range")]
        public IActionResult download([FromQueryAttribute]DateTime from, [FromQueryAttribute]DateTime to)
        {   
            downloadResult result = _Repository.download(from,to);
            return Ok(result);
        }

        [HttpGetAttribute("download/all/month")]
        public IActionResult download([FromQueryAttribute]Int32 month, [FromQueryAttribute]Int32 year)
        {
            downloadResult result = _Repository.download(month,year);
            return Ok(result);
        }

        [HttpGetAttribute("download/{employeeCode}/today")]
        public IActionResult download(String employeeCode)
        {
            downloadResult result = _Repository.download(DateTime.Now,DateTime.Now,employeeCode);
            return Ok(result);
        }

        [HttpGetAttribute("download/{employeeCode}/range")]
        public IActionResult download([FromQueryAttribute]DateTime from, [FromQueryAttribute]DateTime to, String employeeCode)
        {   
            downloadResult result = _Repository.download(from,to,employeeCode);
            return Ok(result);
        }

        [HttpGetAttribute("download/{employeeCode}/month")]
        public IActionResult download([FromQueryAttribute]Int32 month, [FromQueryAttribute]Int32 year, String employeeCode)
        {
            downloadResult result = _Repository.download(month,year,employeeCode);
            return Ok(result);
        }

        [HttpGetAttribute("privateDownload")]
        public IActionResult privateDownload([FromQueryAttribute]DateTime from, [FromQueryAttribute]DateTime to, String employeeCode)
        {
            downloadResult result = _Repository.privateDownload(from, to, employeeCode);
            return Ok(result);
        }

        [HttpPost("uploadExcel")]
        public  IActionResult uploadExcel(IFormFile file)
        {
            ////Upload file to local
            //var uploads = Path.Combine(hostingEnv.ContentRootPath, @"wwwroot\kmbidevrepo\");
            //if (file.Length > 0)
            //{
            //    using (var fileStream = new FileStream(Path.Combine(uploads, file.FileName), FileMode.Create))
            //    {
            //        await file.CopyToAsync(fileStream);
            //    }
            //}

            //string filePath = Path.Combine(uploads, file.FileName);
            //TransferUtility fTransfer = new TransferUtility(new AmazonS3Client(_awsAccessKey, _awsSecretKey, RegionEndpoint.APSoutheast1));
            //string keyName = string.Format("csvFiles/{0}", file.FileName);
            //fTransfer.Upload(filePath, _bucketName, keyName);

            String cs = String.Format(
                "Data Source={0};Initial Catalog={1}; User ID={2};Password={3};",
                _bio.Value.server,
                _bio.Value.db,
                _bio.Value.uid,
                _bio.Value.pass);

            SqlConnection con = new SqlConnection(cs);
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }

            BinaryReader reader = new BinaryReader(file.OpenReadStream());
            byte[] bytes = reader.ReadBytes((int)file.Length);
            MemoryStream ms = new MemoryStream(bytes);

            var excel = new ExcelPackage(ms);
            ExcelWorksheet worksheet = excel.Workbook.Worksheets[1];
            int rowCount = worksheet.Dimension.End.Row;
            for (int row = 2; row <= rowCount; row++)
            {
                string _USERID = null;
                if (worksheet.Cells[row, 1].Value != null)
                    _USERID = worksheet.Cells[row, 1].Value.ToString().Trim();

                string _CHECKTIME = null;
                if (worksheet.Cells[row, 2].Value != null)
                    _CHECKTIME = worksheet.Cells[row, 2].Value.ToString().Trim();

                string _CHECKTYPE = null;
                if (worksheet.Cells[row, 3].Value != null)
                    _CHECKTYPE = worksheet.Cells[row, 3].Value.ToString().Trim();


                SqlParameter userid = new SqlParameter
                {
                    ParameterName = "@USERID",
                    Value = _USERID
                };
                SqlParameter checktime = new SqlParameter
                {
                    ParameterName = "@CHECKTIME",
                    Value = _CHECKTIME
                };
                SqlParameter checktype = new SqlParameter
                {
                    ParameterName = "@CHECKTYPE",
                    Value = _CHECKTYPE
                };

                String query = @"
            
                DELETE FROM Checkinout WHERE Userid = @USERID and CheckTime = @CHECKTIME and CheckType = @CHECKTYPE
                
                INSERT INTO Checkinout
                ( 
                    [Userid], 
                    [CheckTime], 
                    [CheckType], 
                    [Sensorid], 
                    [Checked], 
                    [WorkType], 
                    [AttFlag], 
                    [OpenDoorFlag] 
                )
                VALUES
                (
                    @USERID, 
                    @CHECKTIME, 
                    @CHECKTYPE, 
                    3, 
                    1, 
                    0, 
                    1, 
                    0
                )";

                var cmd = new SqlCommand(query, con);
                cmd.Parameters.Add(userid);
                cmd.Parameters.Add(checktime);
                cmd.Parameters.Add(checktype);
                cmd.ExecuteNonQuery();
            }



            return Ok(new
            {
                type = "success",
                message = "Successfully uploaded"
            });
        }


    }
}
