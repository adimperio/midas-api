﻿using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Models;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.AspNetCore.Hosting;
using System.IO;

using System.Drawing;
using kmbi_core_master.reports.Models;
using kmbi_core_master.helpers;
using Microsoft.AspNetCore.DataProtection;
using kmbi_core_master.helpers.interfaces;

using kmbi_core_master.hrModule.Models;

namespace kmbi_core_master.coreMaster.Repository
{
    public class usersRepository: dbCon, iusersRepository
    {
        IMongoCollection<users> _context ;
        private readonly IHostingEnvironment hostingEnv;
        iupload _upload;
        public usersRepository(IOptions<dbSettings> settings, IHostingEnvironment env, IDataProtectionProvider provide, iupload iupload) : base(settings)
        {
            hostingEnv = env;
            _context = _db.GetCollection<users>("users");
            _upload = iupload;
        }

      
        public void Add(users user)
        {
            user.password = BCrypt.Net.BCrypt.HashPassword(user.password);
            user.fullname = user.name.first + " " + user.name.middle + " " + user.name.last + " " + user.extension;
            
            // user.leaves = new leaves();
            // Double remaiSl = 0.0;
            // Double remaiVl = 0.0;
            // if (user.employment.status.ToLower() == "regular") {
            //     remaiSl = countLeaves(user.employeeCode,user.employment.effectivityDateFrom,"Sick Leave");
            //     remaiVl = countLeaves(user.employeeCode,user.employment.effectivityDateFrom,"Vacation Leave");
            //     //remai = 15.0;
            // }
            // user.leaves.types = new List<types> {
            //     new types {slug="sickLeaveSlug", type="Sick Leave", remaining = remaiSl},
            //     new types {slug="vacationLeaveSlug", type="Vacation Leave", remaining = remaiVl}
            // };

            user.createdAt = DateTime.Now;
            user.updatedAt = DateTime.Now;
            user.employment.createdAt = DateTime.Now;
            user.employment.updatedAt = DateTime.Now;
            //encrypt salary
            user.employment.salary = encryptDecrypt.EncryptString(user.employment.salary.ToString());
            //encrypt salary history
            foreach (var item in user.employmentHistory)
            {
                item.salary = encryptDecrypt.EncryptString(item.salary.ToString());
            }

            user.mothersMaidenName.fullname = user.mothersMaidenName.first + " " + user.mothersMaidenName.middle + " " + user.mothersMaidenName.last;
            _db.GetCollection<users>("users").InsertOneAsync(user);
            updateLeaveBalances(user.employment.effectivityDateFrom,new List<users> {user});
        }

        public IEnumerable<users> AllUsers()
        {
            var users = _db.GetCollection<users>("users").Find(new BsonDocument()).ToListAsync().Result;
            //string sal = "";
            foreach (var item in users)
            {
                item.employment.salary = encryptDecrypt.DecryptString(item.employment.salary);
            }
            return users;
        }

        public users Get(String id)
        {
            var query = Builders<users>.Filter.Eq(e => e.Id, id);
            var users = _db.GetCollection<users>("users").Find(query).ToListAsync().Result.FirstOrDefault();
            users.employment.salary = encryptDecrypt.DecryptString(users.employment.salary);
            return users;
        }

        public users getBySlug(String slug)
        {
            var query = Builders<users>.Filter.Eq(e => e.slug, slug);
            var users = _db.GetCollection<users>("users").Find(query).ToListAsync().Result.FirstOrDefault();
            if(users!=null) {
                users.employment.salary = encryptDecrypt.DecryptString(users.employment.salary);
                users.employeeSchedules = fixSkeds(users.employeeSchedules.ToArray()).ToList();
               
                foreach (var item in users.employmentHistory)
                {
                    
                    if (item.salary == "0")
                    {
                        var salary = encryptDecrypt.EncryptString("0");
                        item.salary = encryptDecrypt.DecryptString(salary);
                    }
                    else
                    {
                        item.salary = encryptDecrypt.DecryptString(item.salary);
                    }
                    
                }
            }

            return users;
        }

        public getBySlug getSlug(String slug)
        {
            var query = Builders<getBySlug>.Filter.Eq(e => e.slug, slug);
            var fields = Builders<getBySlug>.Projection
                .Include(u => u.Id)
                .Include(u => u.name)
                .Include(u => u.fullname)
                .Include(u => u.slug)
                .Include(u => u.email)
                .Include(u => u.isVerified);

            var users = _db.GetCollection<getBySlug>("users").Find(query).Project<getBySlug>(fields).ToListAsync();
            return users.Result.FirstOrDefault();
        }

        public getByPermission getPermission(String slug)
        {
            var query = Builders<getByPermission>.Filter.Eq(e => e.slug, slug);
            var fields = Builders<getByPermission>.Projection
                .Include(u => u.permissions)
                .Exclude(u => u.Id);

            var users = _db.GetCollection<getByPermission>("users").Find(query).Project<getByPermission>(fields).ToListAsync();

            return users.Result.FirstOrDefault();
        }

        public void Update(String slug, users p)
        {
            
            var filter = Builders<users>.Filter.Eq(e => e.slug, slug);
            
            if(p.addresses==null) p.addresses = new List<address>();
            if(p.contacts==null) p.contacts = new List<contact>();
            if(p.skills==null) p.skills = new List<skill>();
            
        
            if(p.employment!=null){
                if(p.employment.allowances==null) p.employment.allowances = new List<allowance>();
                if(p.employment.benefits==null) p.employment.benefits = new List<benefit>();
            } else {
                p.employment = new employment {
                    allowances = new List<allowance>(),
                    benefits = new List<benefit>()
                };
            }
            
            if(p.family==null) p.family = new List<familyMember>();
            if(p.trainings==null) p.trainings = new List<training>();
            if(p.educations==null) p.educations = new List<education>();
            if(p.previousEmployments==null) p.previousEmployments = new List<previousEmployment>();
            if(p.employeeRelations==null) p.employeeRelations = new List<employeeRelation>();
            if(p.employeeSchedules==null) p.employeeSchedules = new List<employeeSchedule>();
            if(p.emergencyContactPersons==null) p.emergencyContactPersons = new List<emergencyContactPerson>();
            if(p.languagesSpoken==null) p.languagesSpoken = new List<language>();
            if(p.employmentHistory==null) p.employmentHistory = new List<employmentHistoryItem>();

            p.mothersMaidenName.fullname = p.mothersMaidenName.first + " " + p.mothersMaidenName.middle + " " + p.mothersMaidenName.last;

            foreach (var item in p.employmentHistory)
            {
                item.salary = encryptDecrypt.EncryptString(item.salary.ToString());
            }
            // if(p.leaves==null)
            // {
            //     Double remaiSl = 0.0;
            //     Double remaiVl = 0.0;
            //     if (p.employment.status.ToLower() == "regular") {
            //         remaiSl = countLeaves(p.employeeCode,p.employment.effectivityDateFrom,"Sick Leave");
            //         remaiVl = countLeaves(p.employeeCode,p.employment.effectivityDateFrom,"Vacation Leave");
            //     }
                
            //     p.leaves = new leaves();    
                    
            //     p.leaves.types = new List<types> {
            //         new types {slug="sickLeaveSlug", type="Sick Leave", remaining = remaiSl},
            //         new types {slug="vacationLeaveSlug", type="Vacation Leave", remaining = remaiVl}
            //     };   
            // }

            var update = Builders<users>.Update
                .Set("isPending", p.isPending)
                .Set("name.first", p.name.first)
                .Set("name.middle", p.name.middle)
                .Set("name.last", p.name.last)
                .Set("fullname", p.name.first + ' ' + p.name.middle + ' ' + p.name.last + ' ' + p.extension)
                .Set("employeeCode", p.employeeCode)
                .Set("biometricsCode", p.biometricsCode)
                .Set("gender", p.gender)
                .Set("salutation", p.salutation)
                .Set("extension", p.extension)
                .Set("addresses", p.addresses) // addresses array
                .Set("religion", p.religion)
                .Set("civilStatus", p.civilStatus)
                .Set("contacts", p.contacts) // contacts array
                .Set("height", p.height)
                .Set("weight", p.weight)
                .Set("birthdate", p.birthdate)
                .Set("birthplace", p.birthplace)
                .Set("congregation", p.congregation)
                .Set("church", p.church)
                .Set("skills", p.skills) // skills array
                .Set("employment.position", p.employment.position)
                .Set("employment.group", p.employment.group)
                .Set("employment.department", p.employment.department)
                .Set("employment.division", p.employment.division)
                .Set("employment.section", p.employment.section)
                .Set("employment.region", p.employment.region)
                .Set("employment.area", p.employment.area)
                .Set("employment.branch", p.employment.branch)
                .Set("employment.unit", p.employment.unit)
                .Set("employment.jobGrade", p.employment.jobGrade)
                .Set("employment.jobClassification", p.employment.jobClassification)
                .Set("employment.jobCategory", p.employment.jobCategory)
                .Set("employment.status", p.employment.status)
                .Set("employment.effectivityDateFrom", p.employment.effectivityDateFrom)
                .Set("employment.effectivityDateTo", p.employment.effectivityDateTo)
                .Set("employment.salary", encryptDecrypt.EncryptString(p.employment.salary))
                .Set("employment.allowances", p.employment.allowances) // allowances array
                .Set("employment.benefits",p.employment.benefits) // benefits array
                .Set("employment.isHeadOffice", p.employment.isHeadOffice)
                .Set("family", p.family) // family array
                .Set("trainings", p.trainings) // trainings array
                .Set("educations", p.educations) // educations array
                .Set("previousEmployments", p.previousEmployments) // previousEmployments array
                .Set("sss", p.sss)
                .Set("pagibig", p.pagibig)
                .Set("philhealth", p.philhealth)
                .Set("taxCode", p.taxCode)
                .Set("tin", p.tin)
                .Set("employeeRelations", p.employeeRelations) // employeeRelations array
                .Set("employeeSchedules", p.employeeSchedules) // employeeSchedules array
                .Set("mothersMaidenName", p.mothersMaidenName)
                .Set("nickname", p.nickname)
                .Set("emergencyContactPersons", p.emergencyContactPersons) // emergencyContactPersons array
                .Set("languagesSpoken", p.languagesSpoken) // languagesSpoken array
                .Set("employmentHistory", p.employmentHistory) // employmentHistory array
                .Set("bloodType", p.bloodType)
                .Set("dateHired", p.dateHired)
                .Set("initials", p.initials)
                .Set("leaves", p.leaves)
                .Set("immediateSupervisor", p.immediateSupervisor)
                .Set("email", p.email)
                .CurrentDate("updatedAt");

            var result = _db.GetCollection<users>("users").UpdateOneAsync(filter, update);
            updateLeaveBalances(p.employment.effectivityDateFrom,new List<users> {p});
        }

        public void updateVer(String verToken)
        {
            var filter = Builders<users>.Filter.Eq(e => e.verificationToken, verToken);
            var update = Builders<users>.Update
                .Set("isVerified", 1)
                .CurrentDate("updatedAt");
            var result = _db.GetCollection<users>("users").UpdateOneAsync(filter, update);
        }

        public void UpdatePermission(String slug, users p)
        {

            var filter = Builders<users>.Filter.Eq(e => e.slug, slug);
            var update = Builders<users>.Update
                .Set("permissions", p.permissions)
                .CurrentDate("updatedAt");
            var result = _db.GetCollection<users>("users").UpdateOneAsync(filter, update);

        }

        public void Remove(String slug)
        {
            var query = Builders<users>.Filter.Eq(e => e.slug, slug);
            var result = _db.GetCollection<users>("users").DeleteOneAsync(query);
            
        }

        public IEnumerable<nameSearch> searchName(String fullname)
        {
           
            var query = Builders<nameSearch>.Filter.Regex("fullname", new BsonRegularExpression(fullname, options: "i"));
            var fields = Builders<nameSearch>.Projection
                .Include(u => u.fullname)
                .Include(u => u.position)
                .Include(u => u.slug)
                .Include(u => u.isVerified)
                .Exclude(u => u.Id);

            var users = _db.GetCollection<nameSearch>("users").Find(query).Project<nameSearch>(fields).ToList().AsQueryable();
            return users;
        }

        public IEnumerable<emailSearch> eMail(String email)
        {

            var query = Builders<emailSearch>.Filter.Regex("email", new BsonRegularExpression(email, options: "i"));
            var fields = Builders<emailSearch>.Projection
                .Include(u => u.fullname)
                .Include(u => u.position)
                .Include(u => u.slug)
                .Exclude(u => u.Id);

            var users = _db.GetCollection<emailSearch>("users").Find(query).Project<emailSearch>(fields).ToList().AsQueryable();

            return users;
        }

        public users checkEmail(String email)
        {
            var query = Builders<users>.Filter.Eq(e => e.email, email);
            var users = _db.GetCollection<users>("users").Find(query).ToListAsync();

            return users.Result.FirstOrDefault();
        }

        public users checkSlug(String slug)
        {
            var query = Builders<users>.Filter.Eq(e => e.slug, slug);
            var users = _db.GetCollection<users>("users").Find(query).ToListAsync();

            return users.Result.FirstOrDefault();
        }

        public users checkVerToken(String verToken)
        {
            var query = Builders<users>.Filter.Eq(e => e.verificationToken, verToken);
            var users = _db.GetCollection<users>("users").Find(query).ToListAsync();

            return users.Result.FirstOrDefault();
        }
     
        public bool checkValidUserKey(String token)
        {

            var idList = new List<users>();
            var query = Builders<users>.Filter.Eq(e => e.token, token);
            var users = _db.GetCollection<users>("users").Find(query).ToListAsync();

            idList = users.Result;
            if (idList.Count > 0)
            {
                global.access = idList.FirstOrDefault().permissions.ToList();
                return true;
            }
            else
            {
                return false;
            }

        }

        public List<string> getAccess()
        {
            return global.access.ToList();
        }
        
        public Boolean checkIfSSSIsExisting(String sss)
        {
            var filter = Builders<users>.Filter.Eq(u => u.sss, sss);
            var res = _context.Find(filter).FirstOrDefault();

            if(res!=null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean checkIfPhilhealthIsExisting(String philhealth)
        {
            var filter = Builders<users>.Filter.Eq(u => u.philhealth, philhealth);
            var res = _context.Find(filter).FirstOrDefault();

            if(res!=null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean checkIfPagibigIsExisting(String pagibig)
        {
            var filter = Builders<users>.Filter.Eq(u => u.pagibig, pagibig);
            var res = _context.Find(filter).FirstOrDefault();

            if(res!=null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean checkIfEmployeeCodeIsExisting(String employeeCode)
        {
            var filter = Builders<users>.Filter.Eq(u => u.employeeCode, employeeCode);
            var res = _context.Find(filter).FirstOrDefault();

            if(res!=null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean checkIfTINIsExisting(String tin)
        {
            var filter = Builders<users>.Filter.Eq(u => u.tin, tin);
            var res = _context.Find(filter).FirstOrDefault();

            if(res!=null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean checkIfDocketNumberIsExisting(String docketNo)
        {
            var filter = Builders<users>.Filter.Eq("employeeRelations.docketNumber", docketNo);
            var res = _context.Find(filter).FirstOrDefault();

            if(res!=null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        

//get

        public IEnumerable<address> GetAddresses(String slug)
        {
            
            users users = getBySlug(slug);

            if(users!=null)
            {
                return users.addresses;
            }  
            else
            {
                return null;
            }
        }

        public address GetAddress(String employeeSlug, String addressSlug)
        {
            var filterEmployee = Builders<users>.Filter.Eq("slug",employeeSlug); 
            var filterAddress = Builders<users>.Filter.Eq("addresses.slug",addressSlug);
            var combine = Builders<users>.Filter.And(filterEmployee, filterAddress);         
            var inc = Builders<users>.Projection.Include("addresses.$");
            var emp = _context.Find(combine).Project<users>(inc).FirstOrDefault();

            if(emp!=null)
            {
                return emp.addresses[0];
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<contact> GetContacts(String slug)
        {
            var filterEmployee = Builders<users>.Filter.Eq("slug",slug);
            var inc = Builders<users>.Projection.Include("contacts");

            users users =_context.Find(filterEmployee).Project<users>(inc).FirstOrDefault(); 

            if(users!=null)
            {
                return users.contacts;
            }
            else
            {
                return null;
            }
        }

        public contact GetContact(String employeeSlug, String contactSlug)
        {
            var filterEmployee = Builders<users>.Filter.Eq("slug",employeeSlug);
            var filterAddress = Builders<users>.Filter.Eq("contacts.slug",contactSlug);
            var combine = Builders<users>.Filter.And(filterEmployee, filterAddress);
            var inc = Builders<users>.Projection.Include("contacts.$");
            var _contact = _context.Find(combine).Project<users>(inc).FirstOrDefault();

            if(_contact!=null)
            {
                return _contact.contacts[0];
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<skill> GetSkills(String slug)
        {
            var filterEmployee = Builders<users>.Filter.Eq("slug",slug);
            var inc = Builders<users>.Projection.Include("skills");

            users users =_context.Find(filterEmployee).Project<users>(inc).FirstOrDefault(); 

            if(users!=null)
            {
                return users.skills;
            }
            else
            {
                return null;
            }
        }
        
        public skill GetSkill(String employeeSlug, String slug)
        {
            var filterEmployee = Builders<users>.Filter.Eq("slug",employeeSlug);
            var filterSkill = Builders<users>.Filter.Eq("skills.slug",slug);
            var combine = Builders<users>.Filter.And(filterEmployee, filterSkill);
            var inc = Builders<users>.Projection.Include("skills.$");
            var _skills = _context.Find(filterEmployee).FirstOrDefault();

            if(_skills!=null)
            {
                return _skills.skills[0];
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<allowance> GetAllowances(String slug)
        {
            var filterEmployee = Builders<users>.Filter.Eq("slug",slug);
            var inc = Builders<users>.Projection.Include("employment.allowances");

            users users =_context.Find(filterEmployee).Project<users>(inc).FirstOrDefault(); 

            if(users!=null)
            {
                return users.employment.allowances;
            }
            else
            {
                return null;
            }
        }

        public allowance GetAllowance(String employeeSlug, String allowanceSlug)
        {
            var filterEmployee = Builders<users>.Filter.Eq("slug",employeeSlug);
            var filterAddress = Builders<users>.Filter.Eq("employment.allowances.slug",allowanceSlug);
            var combine = Builders<users>.Filter.And(filterEmployee, filterAddress);
            var inc = Builders<users>.Projection.Include("employment.allowances.$");
            var _contact = _context.Find(combine).Project<users>(inc).FirstOrDefault();

            if(_contact!=null)
            {
                return _contact.employment.allowances[0];
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<benefit> GetBenefits(String slug)
        {
            var filterEmployee = Builders<users>.Filter.Eq("slug",slug);
            var inc = Builders<users>.Projection.Include("employment.benefits");

            users users =_context.Find(filterEmployee).Project<users>(inc).FirstOrDefault(); 

            if(users!=null)
            {
                return users.employment.benefits;
            }
            else
            {
                return null;
            }
        }

        public benefit GetBenefit(String employeeSlug, String benefitSlug)
        {
            var filterEmployee = Builders<users>.Filter.Eq("slug",employeeSlug);
            var filterAddress = Builders<users>.Filter.Eq("employment.benefits.slug",benefitSlug);
            var combine = Builders<users>.Filter.And(filterEmployee, filterAddress);
            var inc = Builders<users>.Projection.Include("employment.benefits.$");
            var _contact = _context.Find(combine).Project<users>(inc).FirstOrDefault();

            if(_contact!=null)
            {
                return _contact.employment.benefits[0];
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<familyMember> GetFamily(String slug)
        {
            var filter = Builders<users>.Filter.Eq("slug", slug);
            var inc = Builders<users>.Projection.Include("family");

            users users = _context.Find(filter).Project<users>(inc).FirstOrDefault();

            if (users!=null)
            {
                return users.family;
            }
            else
            {
                return null;
            }
        }

        public familyMember GetFamilyMember(String employeeSlug, String familyMemberSlug)
        {
            var filterEmployee = Builders<users>.Filter.Eq("slug",employeeSlug);
            var filterFamilyMember = Builders<users>.Filter.Eq("family.slug", familyMemberSlug);
            var filterCombine = Builders<users>.Filter.And(filterEmployee, filterFamilyMember);
            var inc = Builders<users>.Projection.Include("family.$");
            users users = _context.Find(filterCombine).Project<users>(inc).FirstOrDefault();

            if(users!=null)
            {
                return users.family[0];
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<training> GetTrainings(String slug)
        {
            var filterEmployee = Builders<users>.Filter.Eq("slug",slug);
            var inc = Builders<users>.Projection.Include("trainings");
            users users = _context.Find(filterEmployee).Project<users>(inc).FirstOrDefault();

            if(users!=null)
            {
                return users.trainings;
            }
            else
            {
                return null;
            }
        }

        public training GetTraining(String employeeSlug, String trainingSlug)
        {
            var filterEmployee = Builders<users>.Filter.Eq("slug",employeeSlug);
            var filterTraning = Builders<users>.Filter.Eq("trainings.slug",trainingSlug);
            var filterCombine = Builders<users>.Filter.And(filterEmployee, filterTraning);
            var inc = Builders<users>.Projection.Include("trainings.$");

            users users = _context.Find(filterCombine).Project<users>(inc).FirstOrDefault();

            if(users!=null)
            {
                return users.trainings[0];
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<education> GetEducations(String slug)
        {
            var filterEmployee = Builders<users>.Filter.Eq("slug",slug);
            var inc = Builders<users>.Projection.Include("educations");
            users users = _context.Find(filterEmployee).Project<users>(inc).FirstOrDefault();

            if(users!=null)
            {
                return users.educations;
            }
            else
            {
                return null;
            }
        }

        public education GetEducation(String employeeSlug, String educationSlug)
        {
            var filterEmployee = Builders<users>.Filter.Eq("slug",employeeSlug);
            var filterEducation = Builders<users>.Filter.Eq("educations.slug",educationSlug);
            var filterCombine = Builders<users>.Filter.And(filterEmployee, filterEducation);
            var inc = Builders<users>.Projection.Include("educations.$");

            users users = _context.Find(filterCombine).Project<users>(inc).FirstOrDefault();

            if(users!=null)
            {
                return users.educations[0];
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<previousEmployment> GetPreviousEmployments(String slug)
        {
            var filterEmployee = Builders<users>.Filter.Eq("slug",slug);
            var inc = Builders<users>.Projection.Include("previousEmployments");
            users users = _context.Find(filterEmployee).Project<users>(inc).FirstOrDefault();

            if(users!=null)
            {
                return users.previousEmployments;
            }
            else
            {
                return null;
            }
        }

        public previousEmployment GetPreviousEmployment(String employeeSlug, String previousEmploymentSlug)
        {
            var filterEmployee = Builders<users>.Filter.Eq("slug",employeeSlug);
            var filterHistory = Builders<users>.Filter.Eq("previousEmployments.slug",previousEmploymentSlug);
            var filterCombine = Builders<users>.Filter.And(filterEmployee, filterHistory);
            var inc = Builders<users>.Projection.Include("previousEmployments.$");

            users users = _context.Find(filterCombine).Project<users>(inc).FirstOrDefault();

            if(users!=null)
            {
                return users.previousEmployments[0];
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<employeeRelation> GetEmployeeRelations(String employeeSlug)
        {
            var filterEmployee = Builders<users>.Filter.Eq("slug",employeeSlug);
            var inc = Builders<users>.Projection.Include("employeeRelations");
            users users = _context.Find(filterEmployee).Project<users>(inc).FirstOrDefault();

            if(users!=null)
            {
                return users.employeeRelations;
            }
            else
            {
                return null;
            }
        }

        public employeeRelation GetEmployeeRelation(String employeeSlug, String employeeRelationSlug)
        {
            var filterEmployee = Builders<users>.Filter.Eq("slug",employeeSlug);
            var filterRelation = Builders<users>.Filter.Eq("employeeRelations.slug",employeeRelationSlug);
            var filterCombine = Builders<users>.Filter.And(filterEmployee, filterRelation);
            var inc = Builders<users>.Projection.Include("employeeRelations.$");

            users users = _context.Find(filterCombine).Project<users>(inc).FirstOrDefault();

            if(users!=null)
            {
                return users.employeeRelations[0];
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<employeeSchedule> GetEmployeeSchedules(String employeeSlug)
        {
            var filter = Builders<users>.Filter.Eq("slug",employeeSlug);
            var inc = Builders<users>.Projection.Include("employeeSchedules");
            users users = _context.Find(filter).Project<users>(inc).FirstOrDefault();

            if(users!=null)
            {
                return users.employeeSchedules;
            }
            else
            {
                return null;
            }
        }

        public employeeSchedule GetEmployeeSchedule(String employeeSlug, String employeeScheduleSlug)
        {
            var filter1 = Builders<users>.Filter.Eq("slug",employeeSlug);
            var filter2 = Builders<users>.Filter.Eq("employeeSchedules.slug",employeeScheduleSlug);
            var filter3 = Builders<users>.Filter.And(filter1, filter2);
            var inc = Builders<users>.Projection.Include("employeeSchedules.$");

            users users = _context.Find(filter3).Project<users>(inc).FirstOrDefault();

            if(users!=null)
            {
                return users.employeeSchedules[0];
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<emergencyContactPerson> GetEmergencyContactPersons(String employeeSlug)
        {
            var filter = Builders<users>.Filter.Eq("slug",employeeSlug);
            var inc = Builders<users>.Projection.Include("emergencyContactPersons");
            users users = _context.Find(filter).Project<users>(inc).FirstOrDefault();

            if(users!=null)
            {
                return users.emergencyContactPersons;
            }
            else
            {
                return null;
            }
        }

        public emergencyContactPerson GetEmergencyContactPerson(String employeeSlug, String emergencyContactPersonSlug)
        {
            var filter1 = Builders<users>.Filter.Eq("slug",employeeSlug);
            var filter2 = Builders<users>.Filter.Eq("emergencyContactPersons.slug",emergencyContactPersonSlug);
            var filter3 = Builders<users>.Filter.And(filter1, filter2);
            var inc = Builders<users>.Projection.Include("emergencyContactPersons.$");

            users users = _context.Find(filter3).Project<users>(inc).FirstOrDefault();

            if(users!=null)
            {
                return users.emergencyContactPersons[0];
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<language> GetLanguagesSpoken(String employeeSlug)
        {
            var filter = Builders<users>.Filter.Eq("slug",employeeSlug);
            var inc = Builders<users>.Projection.Include("languagesSpoken");
            users users = _context.Find(filter).Project<users>(inc).FirstOrDefault();

            if(users!=null)
            {
                return users.languagesSpoken;
            }
            else
            {
                return null;
            }
        }

        public language GetLanguageSpoken(String employeeSlug, String languageSpokenSlug)
        {
            var filter1 = Builders<users>.Filter.Eq("slug",employeeSlug);
            var filter2 = Builders<users>.Filter.Eq("languagesSpoken.slug",languageSpokenSlug);
            var filter3 = Builders<users>.Filter.And(filter1, filter2);
            var inc = Builders<users>.Projection.Include("languagesSpoken.$");

            users users = _context.Find(filter3).Project<users>(inc).FirstOrDefault();

            if(users!=null)
            {
                return users.languagesSpoken[0];
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<employmentHistoryItem> GetEmploymentHistory(String employeeSlug)
        {
            var filter = Builders<users>.Filter.Eq("slug",employeeSlug);
            var inc = Builders<users>.Projection.Include("employmentHistory");
            users users = _context.Find(filter).Project<users>(inc).FirstOrDefault();

            if(users!=null)
            {
                return users.employmentHistory;
            }
            else
            {
                return null;
            }
        }

        public employmentHistoryItem GetEmploymentHistoryItem(String employeeSlug, String employmentHistoryItemSlug)
        {
            var filter1 = Builders<users>.Filter.Eq("slug",employeeSlug);
            var filter2 = Builders<users>.Filter.Eq("employmentHistory.slug",employmentHistoryItemSlug);
            var filter3 = Builders<users>.Filter.And(filter1, filter2);
            var inc = Builders<users>.Projection.Include("employmentHistory.$.position");

            users users = _context.Find(filter3).Project<users>(inc).FirstOrDefault();

            if(users!=null)
            {
                return users.employmentHistory[0];
            }
            else
            {
                return null;
            }
        }
//insert

        public Boolean InsertAddress(String employeeSlug, address address)
        {

            address.createdAt = DateTime.Now;
            address.updatedAt = DateTime.Now;
            
            var filterEmployee = Builders<users>.Filter.Eq("slug",employeeSlug);
            var update = Builders<users>
                .Update
                .AddToSet("addresses",address)
                .CurrentDate("updatedAt");

            _context.UpdateOne(filterEmployee, update);

            address adds = GetAddress(employeeSlug, address.slug);
            
            if(adds!=null)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public Boolean InsertContact(String employeeSlug, contact contact)
        {
            contact.createdAt = DateTime.Now;
            contact.updatedAt = DateTime.Now;

            var filterEmployee = Builders<users>.Filter.Eq("slug",employeeSlug);
            var update = Builders<users>
                .Update
                .AddToSet("contacts",contact)
                .CurrentDate("updatedAt");

            _context.UpdateOne(filterEmployee, update);

            contact _contact = GetContact(employeeSlug, contact.slug);
            
            if(_contact!=null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean InsertSkill(String employeeSlug, skill skill)
        {
            skill.createdAt = DateTime.Now;
            skill.updatedAt = DateTime.Now;
            var filterEmployee = Builders<users>.Filter.Eq("slug",employeeSlug);
            var update = Builders<users>
                .Update
                .AddToSet("skills",skill)
                .CurrentDate("updatedAt");

            _context.UpdateOne(filterEmployee, update);

            skill _skill = GetSkill(employeeSlug, skill.slug);
            
            if(_skill!=null)
            {
                
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean InsertAllowance(String employeeSlug, allowance allowance)
        {
            allowance.createdAt = DateTime.Now;
            allowance.updatedAt = DateTime.Now;

            var filterEmployee = Builders<users>.Filter.Eq("slug",employeeSlug);
            var update = Builders<users>
                .Update
                .AddToSet("employment.allowances",allowance)
                .CurrentDate("updatedAt");

            _context.UpdateOne(filterEmployee, update);

            allowance _allowance = GetAllowance(employeeSlug, allowance.slug);
            
            if(_allowance!=null)
            {
                
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean InsertBenefit(String employeeSlug, benefit benefit)
        {
            benefit.createdAt = DateTime.Now;
            benefit.updatedAt = DateTime.Now;

            var filterEmployee = Builders<users>.Filter.Eq("slug",employeeSlug);
            var update = Builders<users>
                .Update
                .AddToSet("employment.benefits",benefit)
                .CurrentDate("updatedAt");

            _context.UpdateOne(filterEmployee, update);

            benefit _benefit = GetBenefit(employeeSlug, benefit.slug);
            
            if(_benefit!=null)
            {
                
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean InsertFamily(String slug, familyMember familyMember)
        {
            familyMember.createdAt = DateTime.Now;
            familyMember.updatedAt = DateTime.Now;

            var filterEmployee = Builders<users>.Filter.Eq("slug", slug);
            var update = Builders<users>
                .Update
                .AddToSet("family", familyMember)
                .CurrentDate("updatedAt");

            var isSuccess = true;
            _context.UpdateOne(filterEmployee, update);

            if (isSuccess)
            {
                
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean InsertTraining(String slug, training training)
        {
            training.createdAt = DateTime.Now;
            training.updatedAt = DateTime.Now;
            var filterEmployee = Builders<users>.Filter.Eq("slug",slug);
            var update = Builders<users>
                .Update
                .AddToSet("trainings",training)
                .CurrentDate("updatedAt");
            
            var isSuccess = true;
             _context.UpdateOne(filterEmployee,update);

            if(isSuccess)
            {
                
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean InsertEducation(String slug, education education)
        {
            education.createdAt = DateTime.Now;
            education.updatedAt = DateTime.Now;
            var filterEmployee = Builders<users>.Filter.Eq("slug",slug);
            var update = Builders<users>
                .Update
                .AddToSet("educations",education)
                .CurrentDate("updatedAt");
            
            var isSuccess = true;
            _context.UpdateOne(filterEmployee,update);

            if(isSuccess)
            {
                
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean InsertPreviousEmployment(String slug, previousEmployment previousEmployment)
        {
            previousEmployment.createdAt = DateTime.Now;
            previousEmployment.updatedAt = DateTime.Now;
            var filterEmployee = Builders<users>.Filter.Eq("slug",slug);
            var update = Builders<users>
                .Update
                .AddToSet("previousEmployments",previousEmployment)
                .CurrentDate("updatedAt");
            
            var isSuccess = true;
             _context.UpdateOne(filterEmployee,update);

            if(isSuccess)
            {
                
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean InsertEmployeeRelation(String slug, employeeRelation employeeRelation)
        {
            employeeRelation.createdAt = DateTime.Now;
            employeeRelation.updatedAt = DateTime.Now;
            var filter = Builders<users>.Filter.Eq("slug",slug);
            var update = Builders<users>
                .Update
                .AddToSet("employeeRelations",employeeRelation)
                .CurrentDate("updatedAt");
            
            var isSuccess = true;
            _context.UpdateOne(filter,update);

            if(isSuccess)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean InsertEmployeeSchedule(String slug, employeeSchedule employeeSchedule)
        {
            employeeSchedule.createdAt = DateTime.Now;
            employeeSchedule.updatedAt = DateTime.Now;
            var filter = Builders<users>.Filter.Eq("slug",slug);
            var update = Builders<users>
                .Update
                .AddToSet("employeeSchedules",employeeSchedule)
                .CurrentDate("updatedAt");
            
            var isSuccess = true;
            _context.UpdateOne(filter,update);

            if(isSuccess)
            {
                return true;
            }
            else
            {
                return false;
            }
        } 

        public Boolean InsertEmergencyContactPerson(String slug, emergencyContactPerson emergencyContactPerson)
        {
            emergencyContactPerson.createdAt = DateTime.Now;
            emergencyContactPerson.updatedAt = DateTime.Now;
            var filter = Builders<users>.Filter.Eq("slug",slug);
            var update = Builders<users>
                .Update
                .AddToSet("emergencyContactPersons",emergencyContactPerson)
                .CurrentDate("updatedAt");
            
            var isSuccess = true;
            _context.UpdateOne(filter,update);

            if(isSuccess)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean InsertLanguageSpoken(String slug, language language)
        {
            language.createdAt = DateTime.Now;
            language.updatedAt = DateTime.Now;
            var filter = Builders<users>.Filter.Eq("slug",slug);
            var update = Builders<users>
                .Update
                .AddToSet("languagesSpoken",language)
                .CurrentDate("updatedAt");
            
            var isSuccess = true;
            _context.UpdateOne(filter,update);

            if(isSuccess)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean InsertEmploymentHistoryItem(String slug, employmentHistoryItem employmentHistoryItem)
        {
            employmentHistoryItem.createdAt = DateTime.Now;
            employmentHistoryItem.updatedAt = DateTime.Now;
            employmentHistoryItem.salary = encryptDecrypt.EncryptString(employmentHistoryItem.salary);
            var filter = Builders<users>.Filter.Eq("slug",slug);
            var update = Builders<users>
                .Update
                .AddToSet("employmentHistory",employmentHistoryItem)
                .CurrentDate("updatedAt");
            
            var isSuccess = true;
            _context.UpdateOne(filter,update);

            if(isSuccess)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        // update

        public Boolean UpdateAddress(String employeeSlug, String addressSlug, address address)
        {
            var filterEmployee = Builders<users>.Filter.Eq("slug", employeeSlug);
            var filterAddress = Builders<users>.Filter.Eq("addresses.slug", addressSlug);
            var filterCombine = Builders<users>.Filter.And(filterEmployee, filterAddress);

            var update = Builders<users>
                .Update
                .Set("addresses.$.type", address.type)
                .Set("addresses.$.location", address.location)
                .CurrentDate("addresses.$.updatedAt")
                .CurrentDate("updatedAt");
                
            var isSuccess = true;
            _context.UpdateOne(filterCombine, update); 
            
            if(isSuccess)
            {
                
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean UpdateContact(String employeeSlug, String contactSlug, contact contact)
        {
            var filterEmployee = Builders<users>.Filter.Eq("slug", employeeSlug);
            var filterContact = Builders<users>.Filter.Eq("contacts.slug", contactSlug);
            var filterCombine = Builders<users>.Filter.And(filterEmployee, filterContact);

            var update = Builders<users>
                .Update
                .Set("contacts.$.mobile", contact.mobile)
                .Set("contacts.$.landline", contact.landline)
                .CurrentDate("contacts.$.updatedAt")
                .CurrentDate("updatedAt");
                
            var isSuccess = true;
            _context.UpdateOne(filterCombine, update); 
            
            if(isSuccess)
            {
                
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean UpdateSkill(String employeeSlug, String skillSlug, skill skill)
        {
            var filterEmployee = Builders<users>.Filter.Eq("slug", employeeSlug);
            var filterSkill = Builders<users>.Filter.Eq("skills.slug", skillSlug);
            var filterCombine = Builders<users>.Filter.And(filterEmployee, filterSkill);

            var update = Builders<users>
                .Update
                .Set("skills.$.type", skill.type)
                .CurrentDate("skills.$.updatedAt")
                .CurrentDate("updatedAt");
            var isSuccess = true;
            _context.UpdateOne(filterCombine, update);

            if (isSuccess)
            {
                
                return true;
            }
            else
            {
                return false;
            }
        }


        public Boolean UpdateAllowance(String employeeSlug, String allowanceSlug, allowance allowance)
        {
            var filterEmployee = Builders<users>.Filter.Eq("slug", employeeSlug);
            var filterAllowance = Builders<users>.Filter.Eq("employment.allowances.slug", allowanceSlug);
            var filterCombine = Builders<users>.Filter.And(filterEmployee, filterAllowance);

            var update = Builders<users>
                .Update
                .Set("employment.allowances.$.name", allowance.name)
                .Set("employment.allowances.$.amount", allowance.amount)
                .Set("employment.allowances.$.temporary", allowance.temporary)
                .Set("employment.allowances.$.effectivityDateFrom", allowance.effectivityDateFrom)
                .Set("employment.allowances.$.effectivityDateTo", allowance.effectivityDateTo)
                .CurrentDate("employment.allowances.$.updatedAt")
                .CurrentDate("updatedAt");
                
            var isSuccess = true;
            _context.UpdateOne(filterCombine, update); 
            
            if(isSuccess)
            {
                
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean UpdateBenefit(String employeeSlug, String benefitSlug, benefit benefit)
        {
            var filterEmployee = Builders<users>.Filter.Eq("slug", employeeSlug);
            var filterBenefit = Builders<users>.Filter.Eq("employment.benefits.slug", benefitSlug);
            var filterCombine = Builders<users>.Filter.And(filterEmployee, filterBenefit);

            var update = Builders<users>
                .Update
                .Set("employment.benefits.$.name", benefit.name)
                .Set("employment.benefits.$.amount", benefit.amount)
                .Set("employment.benefits.$.effectivityDateFrom", benefit.effectivityDateFrom)
                .Set("employment.benefits.$.effectivityDateTo", benefit.effectivityDateTo)
                .CurrentDate("employment.benefits.$.updatedAt")
                .CurrentDate("updatedAt");
                
            var isSuccess = true;
            _context.UpdateOne(filterCombine, update); 
            
            if(isSuccess)
            {
                
                return true;
            }
            else
            {
                return false;
            }
        }
        
        public Boolean UpdateFamilyMember(String employeeSlug, string familyMemberSlug, familyMember familyMember)
        {
            var filterEmployee = Builders<users>.Filter.Eq("slug", employeeSlug);
            var filterFamily = Builders<users>.Filter.Eq("family.slug", familyMemberSlug);
            var filterCombine = Builders<users>.Filter.And(filterEmployee, filterFamily);
            //var res = _context.Find(filterCombine).ToList();
            var update = Builders<users>
                .Update
                .Set("family.$.last", familyMember.last)
                .Set("family.$.first", familyMember.first)
                .Set("family.$.middle", familyMember.middle)
                .Set("family.$.relationship", familyMember.relationship)
                .Set("family.$.birthdate", familyMember.birthdate)
                .Set("family.$.occupation", familyMember.occupation)
                .Set("family.$.contact", familyMember.contact)
                .Set("family.$.dependents", familyMember.dependents)
                .Set("family.$.highestEducationalAttainment", familyMember.highestEducationalAttainment)
                .CurrentDate("family.$.updatedAt")
                .CurrentDate("updatedAt");

            var isSuccess = true;
             _context.UpdateOne(filterCombine, update);

            if(isSuccess)
            {
                
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean UpdateTraining(string employeeSlug, string trainingSlug, training training)
        {
            var filterEmployee = Builders<users>.Filter.Eq("slug", employeeSlug);
            var filterTraining = Builders<users>.Filter.Eq("trainings.slug", trainingSlug);
            var filterCombine = Builders<users>.Filter.And(filterEmployee, filterTraining);
            
            var update = Builders<users>
                .Update
                .Set("trainings.$.title", training.title)
                .Set("trainings.$.dateFrom", training.dateFrom)
                .Set("trainings.$.dateTo", training.dateTo)
                .Set("trainings.$.sponsor", training.sponsor)
                .Set("trainings.$.venue", training.venue)
                .Set("trainings.$.isResourceSpeaker", training.isResourceSpeaker)
                .Set("trainings.$.cost", training.cost)
                .Set("trainings.$.personalCost", training.personalCost)
                .Set("trainings.$.noOfHours", training.noOfHours)
                .Set("trainings.$.holdingPeriod", training.holdingPeriod)
                .Set("trainings.$.holdingUntil", training.holdingUntil)
                .Set("trainings.$.documents", training.documents)
                .CurrentDate("trainings.$.updatedAt")
                .CurrentDate("updatedAt");

            var isSuccess = true;
            _context.UpdateOne(filterCombine, update);

            if(isSuccess)
            {
                
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean UpdateEducation(string employeeSlug, string educationSlug, education education)
        {
            var filterEmployee = Builders<users>.Filter.Eq("slug", employeeSlug);
            var filterEducation = Builders<users>.Filter.Eq("educations.slug", educationSlug);
            var filterCombine = Builders<users>.Filter.And(filterEmployee, filterEducation);
            
            var update = Builders<users>
                .Update
                .Set("educations.$.educationType", education.educationType)
                .Set("educations.$.school", education.school)
                .Set("educations.$.address", education.address)
                .Set("educations.$.inclusiveDateFrom", education.inclusiveDateFrom)
                .Set("educations.$.inclusiveDateTo", education.inclusiveDateTo)
                .Set("educations.$.degree", education.degree)
                .Set("educations.$.major", education.major)
                .Set("educations.$.award", education.award)
                .Set("educations.$.notYetGraduated", education.notYetGraduated)
                .Set("educations.$.noOfUnits", education.noOfUnits)
                .Set("educations.$.yearLevel", education.yearLevel)
                .Set("educations.$.documents", education.documents)
                .CurrentDate("educations.$.updatedAt")
                .CurrentDate("updatedAt");

            var isSuccess = true;
            _context.UpdateOne(filterCombine, update);

            if(isSuccess)
            {
                
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean UpdatePreviousEmployment(string employeeSlug, string previousEmploymentSlug, previousEmployment previousEmployment)
        {
            var filterEmployee = Builders<users>.Filter.Eq("slug", employeeSlug);
            var filterPreviousEmployment = Builders<users>.Filter.Eq("previousEmployments.slug", previousEmploymentSlug);
            var filterCombine = Builders<users>.Filter.And(filterEmployee, filterPreviousEmployment);
            
            var update = Builders<users>
                .Update
                .Set("previousEmployments.$.company", previousEmployment.company)
                .Set("previousEmployments.$.address", previousEmployment.address)
                .Set("previousEmployments.$.telNo", previousEmployment.telNo)
                .Set("previousEmployments.$.inclusiveDateFrom", previousEmployment.inclusiveDateFrom)
                .Set("previousEmployments.$.inclusiveDateTo", previousEmployment.inclusiveDateTo)
                .Set("previousEmployments.$.position", previousEmployment.position)
                .Set("previousEmployments.$.salaryStart", previousEmployment.salaryStart)
                .Set("previousEmployments.$.salaryEnd", previousEmployment.salaryEnd)
                .Set("previousEmployments.$.reasonOfSeverance", previousEmployment.reasonOfSeverance)
                .Set("previousEmployments.$.immediateSupervisor", previousEmployment.immediateSupervisor)
                .Set("previousEmployments.$.statusOfEmployment", previousEmployment.statusOfEmployment)
                .Set("previousEmployments.$.jobFunction", previousEmployment.jobFunction)
                .Set("previousEmployments.$.documents", previousEmployment.documents)
                .CurrentDate("previousEmployments.$.updatedAt")
                .CurrentDate("updatedAt");

            var isSuccess = true;
            _context.UpdateOne(filterCombine, update);

            if(isSuccess)
            {
                
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean UpdateEmployeeRelation(String employeeSlug, String employeeRelationSlug, employeeRelation employeeRelation)
        {
            var filter1 = Builders<users>.Filter.Eq("slug", employeeSlug);
            var filter2 = Builders<users>.Filter.Eq("employeeRelations.slug", employeeRelationSlug);
            var filter3 = Builders<users>.Filter.And(filter1, filter2);
            
            var update = Builders<users>
                .Update
                .Set("employeeRelations.$.docketNumber", employeeRelation.docketNumber)
                .Set("employeeRelations.$.cocViolation", employeeRelation.cocViolation)
                .Set("employeeRelations.$.natureOfOffense", employeeRelation.natureOfOffense)
                .Set("employeeRelations.$.dateOfOffense", employeeRelation.dateOfOffense)
                .Set("employeeRelations.$.dateOfResolution", employeeRelation.dateOfResolution)
                .Set("employeeRelations.$.sanction", employeeRelation.sanction)
                .Set("employeeRelations.$.caseStatus", employeeRelation.caseStatus)
                .Set("employeeRelations.$.documents", employeeRelation.documents)
                .CurrentDate("employeeRelations.$.updatedAt")
                .CurrentDate("updatedAt");

            var isSuccess = true;
            _context.UpdateOne(filter3, update);
            return isSuccess;
        } 

        public Boolean UpdateEmployeeSchedule(String employeeSlug, String employeeScheduleSlug, employeeSchedule employeeSchedule)
        {
            var filter1 = Builders<users>.Filter.Eq("slug", employeeSlug);
            var filter2 = Builders<users>.Filter.Eq("employeeSchedules.slug", employeeScheduleSlug);
            var filter3 = Builders<users>.Filter.And(filter1, filter2);
            
            var update = Builders<users>
                .Update
                .Set("employeeSchedules.$.timeInAm", employeeSchedule.timeInAm)
                .Set("employeeSchedules.$.timeOutAm", employeeSchedule.timeOutAm)
                .Set("employeeSchedules.$.timeInPm", employeeSchedule.timeInPm)
                .Set("employeeSchedules.$.timeOutPm", employeeSchedule.timeOutPm)
                .Set("employeeSchedules.$.effectivityDateFrom", employeeSchedule.effectivityDateFrom)
                .Set("employeeSchedules.$.effectivityDateTo", employeeSchedule.effectivityDateTo)
                .Set("employeeSchedules.$.isWorkFromHome", employeeSchedule.isWorkFromHome)
                .Set("employeeSchedules.$.isWorkFromBranch", employeeSchedule.isWorkFromBranch)
                .Set("employeeSchedules.$.days", employeeSchedule.days)
                .Set("employeeSchedules.$.branchSlug", employeeSchedule.branchSlug)
                .CurrentDate("employeeSchedules.$.updatedAt")
                .CurrentDate("updatedAt");

            var isSuccess = true;
            _context.UpdateOne(filter3, update);
            return isSuccess;
        }   

        public Boolean UpdateEmergencyContactPerson(String employeeSlug, String emergencyContactPersonSlug, emergencyContactPerson emergencyContactPerson)
        {
            var filter1 = Builders<users>.Filter.Eq("slug", employeeSlug);
            var filter2 = Builders<users>.Filter.Eq("emergencyContactPersons.slug", emergencyContactPersonSlug);
            var filter3 = Builders<users>.Filter.And(filter1, filter2);
            
            var update = Builders<users>
                .Update
                .Set("emergencyContactPersons.$.contactPerson", emergencyContactPerson.contactPerson)
                .Set("emergencyContactPersons.$.contactNumber", emergencyContactPerson.contactNumber)
                .Set("emergencyContactPersons.$.address", emergencyContactPerson.address)
                .Set("emergencyContactPersons.$.relationship", emergencyContactPerson.relationship)
                .CurrentDate("emergencyContactPersons.$.updatedAt")
                .CurrentDate("updatedAt");

            var isSuccess = true;
            _context.UpdateOne(filter3, update);
            return isSuccess;
        }

        public Boolean UpdateLanguageSpoken(String employeeSlug, String languageSpokenSlug, language language)
        {
            var filter1 = Builders<users>.Filter.Eq("slug", employeeSlug);
            var filter2 = Builders<users>.Filter.Eq("languagesSpoken.slug", languageSpokenSlug);
            var filter3 = Builders<users>.Filter.And(filter1, filter2);
            
            var update = Builders<users>
                .Update
                .Set("languagesSpoken.$.name", language.name)
                .CurrentDate("languagesSpoken.$.updatedAt")
                .CurrentDate("updatedAt");

            var isSuccess = true;
            _context.UpdateOne(filter3, update);
            return isSuccess;
        }

        public Boolean UpdateEmploymentHistoryItem(String employeeSlug, String employmentHistoryItemSlug, employmentHistoryItem employmentHistoryItem)
        {
            var filter1 = Builders<users>.Filter.Eq("slug", employeeSlug);
            var filter2 = Builders<users>.Filter.Eq("employmentHistory.slug", employmentHistoryItemSlug);
            var filter3 = Builders<users>.Filter.And(filter1, filter2);
            
            var update = Builders<users>
                .Update
                .Set("employmentHistory.$.position", employmentHistoryItem.position)
                .Set("employmentHistory.$.group", employmentHistoryItem.group)
                .Set("employmentHistory.$.department", employmentHistoryItem.department)
                .Set("employmentHistory.$.division", employmentHistoryItem.division)
                .Set("employmentHistory.$.section", employmentHistoryItem.section)
                .Set("employmentHistory.$.region", employmentHistoryItem.region)
                .Set("employmentHistory.$.area", employmentHistoryItem.area)
                .Set("employmentHistory.$.branch", employmentHistoryItem.branch)
                .Set("employmentHistory.$.unit", employmentHistoryItem.unit)
                .Set("employmentHistory.$.jobGrade", employmentHistoryItem.jobGrade)
                .Set("employmentHistory.$.jobClassification", employmentHistoryItem.jobClassification)
                .Set("employmentHistory.$.jobCategory", employmentHistoryItem.jobCategory)
                .Set("employmentHistory.$.status", employmentHistoryItem.status)
                .Set("employmentHistory.$.effectivityDateFrom", employmentHistoryItem.effectivityDateFrom)
                .Set("employmentHistory.$.effectivityDateTo", employmentHistoryItem.effectivityDateTo)
                .Set("employmentHistory.$.salary", encryptDecrypt.EncryptString(employmentHistoryItem.salary))
                .Set("employmentHistory.$.allowances", employmentHistoryItem.allowances) // allowances array
                .Set("employmentHistory.$.benefits",employmentHistoryItem.benefits) // benefits array
                .Set("employmentHistory.$.isHeadOffice", employmentHistoryItem.isHeadOffice)
                .CurrentDate("employmentHistory.$.updatedAt")
                .CurrentDate("updatedAt");

            var isSuccess = true;
            _context.UpdateOne(filter3, update);
            return isSuccess;
        }

        //delete

        public Boolean DeleteAddress(String employeeSlug, String addressSlug)
        {

            var filterEmployee = Builders<users>.Filter.Eq("slug", employeeSlug);
            var filterAddress = Builders<address>.Filter.Eq("slug", addressSlug);

            var update = Builders<users>
                .Update
                .PullFilter("addresses",filterAddress)
                .CurrentDate("updatedAt");

            var isSuccess = true;
            _context.UpdateOne(filterEmployee,update);

            if(isSuccess)
            {
                
                return true;
            }
            else
            {
                return false;
            } 
        }

        public Boolean DeleteContact(String employeeSlug, String contactSlug)
        {

            var filterEmployee = Builders<users>.Filter.Eq("slug", employeeSlug);
            var filterContact = Builders<contact>.Filter.Eq("slug", contactSlug);

            var update = Builders<users>
                .Update
                .PullFilter("contacts",filterContact)
                .CurrentDate("updatedAt");

            var isSuccess = true;
             _context.UpdateOne(filterEmployee,update);

            if(isSuccess)
            {
                
                return true;
            }
            else
            {
                return false;
            } 
        }

        public Boolean DeleteSkill(String employeeSlug, String skillSlug)
        {

            var filterEmployee = Builders<users>.Filter.Eq("slug", employeeSlug);
            var filterSkill = Builders<skill>.Filter.Eq("slug", skillSlug);
            var u = _context.Find(filterEmployee).FirstOrDefault();
            
            var update = Builders<users>
                .Update
                .PullFilter("skills",filterSkill)
                .CurrentDate("updatedAt");

            var isSuccess = true;
            var res = _context.UpdateOne(filterEmployee,update);

            if(isSuccess)
            {
                
                return true;
            }
            else
            {
                return false;
            } 
        }

        public Boolean DeleteAllowance(String employeeSlug, String allowanceSlug)
        {
            var filterEmployee = Builders<users>.Filter.Eq("slug", employeeSlug);
            var filterAllowance = Builders<allowance>.Filter.Eq("slug", allowanceSlug);
            var update = Builders<users>
                .Update
                .PullFilter("employment.allowances", filterAllowance)
                .CurrentDate("employment.updatedAt");

             var isSuccess = true;
              _context.UpdateOne(filterEmployee, update);
            
            if(isSuccess)
            {
                
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean DeleteBenefit(String employeeSlug, String benefitSlug)
        {
            var filterEmployee = Builders<users>.Filter.Eq("slug", employeeSlug);
            var filterBenefit = Builders<benefit>.Filter.Eq("slug", benefitSlug);
            var update = Builders<users>
                .Update
                .PullFilter("employment.benefits", filterBenefit)
                .CurrentDate("employment.updatedAt");

             var isSuccess = true;
              _context.UpdateOne(filterEmployee, update);
            
            if(isSuccess)
            {
                
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean DeleteFamilyMember(String employeeSlug, string familyMemberSlug)
        {
            var filterEmployee = Builders<users>.Filter.Eq("slug", employeeSlug);
            var filterFamily = Builders<familyMember>.Filter.Eq("slug", familyMemberSlug);
            var update = Builders<users>
                .Update
                .PullFilter("family", filterFamily)
                .CurrentDate("updatedAt");

             var isSuccess = true;
              _context.UpdateOne(filterEmployee, update);
            
            if(isSuccess)
            {
                
                return true;
            }
            else
            {
                return false;
            }
        }

        public Boolean DeleteTraining(String employeeSlug, string trainingSlug)
        {
            var filterEmployee = Builders<users>.Filter.Eq("slug", employeeSlug);
            var filterTraining = Builders<training>.Filter.Eq("slug", trainingSlug);
            var update = Builders<users>
                .Update
                .PullFilter("trainings", filterTraining)
                .CurrentDate("updatedAt");
                
            var isSuccess = true;
            _context.UpdateOne(filterEmployee, update);

            if(isSuccess)
            {
                
                return true;
            }
            else
            {
                return false;
            }

        }

        public Boolean DeleteEducation(String employeeSlug, string educationSlug)
        {
            var filterEmployee = Builders<users>.Filter.Eq("slug", employeeSlug);
            var filterEducation = Builders<education>.Filter.Eq("slug", educationSlug);
            var update = Builders<users>
                .Update
                .PullFilter("educations", filterEducation)
                .CurrentDate("updatedAt");
                
            var isSuccess = true;
            _context.UpdateOne(filterEmployee, update);

            if(isSuccess)
            {
                
                return true;
            }
            else
            {
                return false;
            }

        }

        public Boolean DeletePreviousEmployment(String employeeSlug, string previousEmploymentSlug)
        {
            var filterEmployee = Builders<users>.Filter.Eq("slug", employeeSlug);
            var filterPreviousEmployment = Builders<previousEmployment>.Filter.Eq("slug", previousEmploymentSlug);
            var update = Builders<users>
                .Update
                .PullFilter("previousEmployments", filterPreviousEmployment)
                .CurrentDate("updatedAt");
                
            var isSuccess = true;
             _context.UpdateOne(filterEmployee, update);

            if(isSuccess)
            {
                
                return true;
            }
            else
            {
                return false;
            }

        }

        public Boolean DeleteEmployeeRelation(String employeeSlug, String employeeRelationSlug)
        {
            var filter1 = Builders<users>.Filter.Eq("slug", employeeSlug);
            var filter2 = Builders<employeeRelation>.Filter.Eq("slug", employeeRelationSlug);
            var update = Builders<users>
                .Update
                .PullFilter("employeeRelations", filter2)
                .CurrentDate("updatedAt");
                
            var isSuccess = true;
            _context.UpdateOne(filter1, update);

            return isSuccess;
        }

        public Boolean DeleteEmployeeSchedule(String employeeSlug, String employeeScheduleSlug)
        {
            var filter1 = Builders<users>.Filter.Eq("slug", employeeSlug);
            var filter2 = Builders<employeeSchedule>.Filter.Eq("slug", employeeScheduleSlug);
            var update = Builders<users>
                .Update
                .PullFilter("employeeSchedules", filter2)
                .CurrentDate("updatedAt");
                
            var isSuccess = true;
            _context.UpdateOne(filter1, update);

            return isSuccess;
        } 

        public Boolean DeleteEmergencyContactPerson(String employeeSlug, String emergencyContactPersonSlug)
        {
            var filter1 = Builders<users>.Filter.Eq("slug", employeeSlug);
            var filter2 = Builders<emergencyContactPerson>.Filter.Eq("slug", emergencyContactPersonSlug);
            var update = Builders<users>
                .Update
                .PullFilter("emergencyContactPersons", filter2)
                .CurrentDate("updatedAt");
                
            var isSuccess = true;
            _context.UpdateOne(filter1, update);

            return isSuccess;
        }

        public Boolean DeleteLanguageSpoken(String employeeSlug, String languageSlug)
        {
            var filter1 = Builders<users>.Filter.Eq("slug", employeeSlug);
            var filter2 = Builders<language>.Filter.Eq("slug", languageSlug);
            var update = Builders<users>
                .Update
                .PullFilter("languagesSpoken", filter2)
                .CurrentDate("updatedAt");
                
            var isSuccess = true;
            _context.UpdateOne(filter1, update);

            return isSuccess;
        }

        public Boolean DeleteEmploymentHistoryItem(String employeeSlug, String employmentHistoryItemSlug)
        {
            var filter1 = Builders<users>.Filter.Eq("slug", employeeSlug);
            var filter2 = Builders<employmentHistoryItem>.Filter.Eq("slug", employmentHistoryItemSlug);
            var update = Builders<users>
                .Update
                .PullFilter("employmentHistory", filter2)
                .CurrentDate("updatedAt");
                
            var isSuccess = true;
            _context.UpdateOne(filter1, update);

            return isSuccess;
        }
        //upload

        public Boolean UploadPicture(String slug, Byte[] bytes)
        {
            users users = getBySlug(slug);
            var _bytes = bytes;
            if(users!=null)
            {

                var rndString = global.RandomLetter(3) + global.RandomNumber(3);
                String corePath256fn = slug + "-" + rndString + "-256.png";
                String corePath64fn= slug + "-" + rndString + "-64.png";

                MemoryStream ms = new MemoryStream(_bytes);

                Image resized256Image = ResizeImage(Image.FromStream(ms), 256, 256);
                MemoryStream ms2 = new MemoryStream();
                resized256Image.Save(ms2,System.Drawing.Imaging.ImageFormat.Jpeg);
                
                Image resized64Image = ResizeImage(Image.FromStream(ms), 64, 64);
                MemoryStream ms3 = new MemoryStream();
                resized64Image.Save(ms3,System.Drawing.Imaging.ImageFormat.Jpeg);

                String link64 = _upload.upload(ms3.ToArray(),"kmbidevrepo/uploads/employees/pictures/" + corePath64fn);
                String link256 = _upload.upload(ms2.ToArray(),"kmbidevrepo/uploads/employees/pictures/" + corePath256fn);

                if(link256!="")
                {
                    //update employee
                    var isSuccess = true;
                    _context.UpdateOne(
                    Builders<users>
                        .Filter
                        .Eq("slug", slug),
                    Builders<users>
                        .Update
                        .Set("picture", link256)
                        .CurrentDate("updatedAt")
                    );

                    if(isSuccess)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }            
        }

        //upload document per tab
        public Boolean UploadEducationDocument(String employeeSlug, String educationSlug, String documentSlug, String Filename, Byte[] bytes)
        {
            education edu = GetEducation(employeeSlug, educationSlug);
            if(edu!=null)
            {
                String link = _upload.upload(new MemoryStream(bytes).ToArray(),"kmbidevrepo/uploads/employees/documents/" + Filename);
                if(link!="")
                {
                    var doc = edu.documents.Find(f => f.slug ==documentSlug);
                    doc.filename = Filename;
                    doc.path = link;
                    return UpdateEducation(employeeSlug,educationSlug,edu);
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public String UploadDocument(String Filename, Byte[] bytes)
        {
            String link = _upload.upload(new MemoryStream(bytes).ToArray(),"kmbidevrepo/uploads/employees/documents/" + Filename);
            return link;
        }

        public Boolean UploadTrainingDocument(String employeeSlug, String trainingSlug, String documentSlug, String Filename, Byte[] bytes)
        {
            training tra = GetTraining(employeeSlug, trainingSlug);
            if(tra!=null)
            {
                String link = _upload.upload(new MemoryStream(bytes).ToArray(),"kmbidevrepo/uploads/employees/documents/" + Filename);
                if(link!="")
                {
                    var doc = tra.documents.Find(f => f.slug ==documentSlug);
                    doc.filename = Filename;
                    doc.path = link;
                    return UpdateTraining(employeeSlug,trainingSlug,tra);
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public Boolean UploadPreviousEmploymentDocument(String employeeSlug, String previousEmploymentSlug, String documentSlug, String Filename, Byte[] bytes)
        {
            previousEmployment pre = GetPreviousEmployment(employeeSlug, previousEmploymentSlug);
            if(pre!=null)
            {
                String link = _upload.upload(new MemoryStream(bytes).ToArray(),"kmbidevrepo/uploads/employees/documents/" + Filename);
                if(link!="")
                {
                    var doc = pre.documents.Find(f => f.slug ==documentSlug);
                    doc.filename = Filename;
                    doc.path = link;
                    return UpdatePreviousEmployment(employeeSlug,previousEmploymentSlug,pre);
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public Boolean UploadEmployeeRelationDocument(String employeeSlug, String employeeRelationSlug, String documentSlug, String Filename, Byte[] bytes)
        {
            employeeRelation empr =GetEmployeeRelation(employeeSlug, employeeRelationSlug);
            if(empr!=null)
            {
                
                String link = _upload.upload(new MemoryStream(bytes).ToArray(),"kmbidevrepo/uploads/employees/documents/" + Filename);
                if(link!="")
                {
                    var doc = empr.documents.Find(f => f.slug ==documentSlug);
                    doc.filename = Filename;
                    doc.path = link;
                    return UpdateEmployeeRelation(employeeSlug,employeeRelationSlug,empr);
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }



        public Image ResizeImage(Image originalImage, int width, int height)
        {
            //padd image
            int largestDimension = Math.Max(originalImage.Height, originalImage.Width);
            Size squareSize = new Size(largestDimension, largestDimension);
            Bitmap squareImage = new Bitmap(squareSize.Width, squareSize.Height);
            using (Graphics graphics = Graphics.FromImage(squareImage))
            {
                graphics.FillRectangle(Brushes.White, 0, 0, squareSize.Width, squareSize.Height);
                graphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

                graphics.DrawImage(originalImage, (squareSize.Width / 2) - (originalImage.Width / 2), (squareSize.Height / 2) - (originalImage.Height / 2), originalImage.Width, originalImage.Height);
            }
            //scale image
            int maxWidth = width;
            int maxHeight = height;
            var ratioX = (double)maxWidth / squareImage.Width;
            var ratioY = (double)maxHeight / squareImage.Height;
            var ratio = Math.Min(ratioX, ratioY);

            var newWidth = (int)(squareImage.Width * ratio);
            var newHeight = (int)(squareImage.Height * ratio);

            var scaledImage = new Bitmap(newWidth, newHeight);

            using (var graphics = Graphics.FromImage(scaledImage))
                graphics.DrawImage(squareImage, 0, 0, newWidth, newHeight);

            return scaledImage;
        }

        //search
        public IEnumerable<kmbi_core_master.coreMaster.Models.users> Search(String searchString)
        {
            IEnumerable<kmbi_core_master.coreMaster.Models.users> list = null;
            if(searchString!=null)
            {
                var search1 = Builders<kmbi_core_master.coreMaster.Models.users>.Filter.Regex("fullname", new BsonRegularExpression(searchString,"i"));
                var search2 = Builders<kmbi_core_master.coreMaster.Models.users>.Filter.Regex("employeeCode", new BsonRegularExpression(searchString,"i"));
                var search3 = Builders<kmbi_core_master.coreMaster.Models.users>.Filter.Regex("employment.department", new BsonRegularExpression(searchString,"i"));
                var search4 = Builders<kmbi_core_master.coreMaster.Models.users>.Filter.Regex("employment.branch", new BsonRegularExpression(searchString,"i"));
                var search5 = Builders<kmbi_core_master.coreMaster.Models.users>.Filter.Regex("nickname", new BsonRegularExpression(searchString,"i"));
                list = _context.Find(Builders<users>.Filter.Or(search1,search2,search3,search4,search5)).ToList();
            }
            else
            {
                list = new List<kmbi_core_master.coreMaster.Models.users>();
            }
            
            return list;
        }

        public IEnumerable<users> GetListOfPendingUsers(){

            var filtera1 = Builders<users>.Filter.Exists("permissions.0",false);
            var filtera2 = Builders<users>.Filter.Eq("isVerified",1);
            var filterAnda1 = Builders<users>.Filter.And(filtera1,filtera2);

            var filterb1 = Builders<users>.Filter.Exists("permissions.0",true);
            var filterb2 = Builders<users>.Filter.Eq("isVerified",0);
            var filterbAnd1 = Builders<users>.Filter.And(filterb1,filterb2);

            var filterc1 = Builders<users>.Filter.Exists("permissions.0",false);
            var filterc2 = Builders<users>.Filter.Eq("isVerified",0);
            var filtercAnd1 = Builders<users>.Filter.And(filterc1,filterc2);

            var filterOr1 = Builders<users>.Filter.Or(filterAnda1,filterbAnd1, filtercAnd1);
            var u = _context.Find(filterOr1);
            List<users> l = u.ToList();
            return l;
        }

        public IEnumerable<possibleRelative> GetListOfPossibleRelatives(String last, String middle, String mothersMaidenNameMiddle, String fathersMiddleName, String relationship)
        {
            String _test = fathersMiddleName;
            if(relationship.ToLower()!="father")
            {
                _test = "";
            }

            var filter = new BsonDocument {
                                {"input", "$family"},
                                {"as", "fam" },
                                {"cond", new BsonDocument {
                                    { "$eq",  new BsonArray { "$$fam.relationship", "father" } } }
                                }
                            };

            var concatFamily = new BsonArray {
                                {"$family.first"},
                                {" "},
                                {"$family.middle"},
                                {" "},
                                {"$family.last"}
                            };

            var concatMaidenName = new BsonArray {
                                {"$mothersMaidenName.first"},
                                {" "},
                                {"$mothersMaidenName.middle"},
                                {" "},
                                {"$mothersMaidenName.last"}
                            };

            var q = _context.Aggregate()
                .Project<possibleRelativeProjected>(new BsonDocument
                        {
                            {"_id",1},
                            {"slug",1},
                            {"name",1},
                            {"employeeCode",1},
                            {"fullname", 1},
                            {"picture",1},
                            {"position",1},
                            {"department",1},
                            {"branch",1},
                            {"employment",1},
                            {"family", new BsonDocument { { "$filter", filter} }},
                            {"mothersMaidenName",1}
                        })
                .Unwind<possibleRelativeProjected, possibleRelativeUnwinded>(x => x.family, new AggregateUnwindOptions<possibleRelativeUnwinded> { PreserveNullAndEmptyArrays = true })
                .Match(
                    Builders<possibleRelativeUnwinded>.Filter.Or(
                        Builders<possibleRelativeUnwinded>.Filter.Regex("name.last", new BsonRegularExpression("^" + last + "$", options: "i")),
                        Builders<possibleRelativeUnwinded>.Filter.Regex("name.middle", new BsonRegularExpression("^" + last + "$", options: "i")),
                        Builders<possibleRelativeUnwinded>.Filter.Regex("mothersMaidenName.middle", new BsonRegularExpression("^" + last + "$", options: "i")),
                        Builders<possibleRelativeUnwinded>.Filter.Regex("family.middle", new BsonRegularExpression("^" + last + "$", options: "i")),
                        Builders<possibleRelativeUnwinded>.Filter.Regex("name.last", new BsonRegularExpression("^" + middle + "$", options: "i")),
                        Builders<possibleRelativeUnwinded>.Filter.Regex("name.middle", new BsonRegularExpression("^" + middle + "$", options: "i")),
                        Builders<possibleRelativeUnwinded>.Filter.Regex("mothersMaidenName.middle", new BsonRegularExpression("^" + middle + "$", options: "i")),
                        Builders<possibleRelativeUnwinded>.Filter.Regex("family.middle", new BsonRegularExpression("^" + middle + "$", options: "i")),
                        Builders<possibleRelativeUnwinded>.Filter.Regex("name.last", new BsonRegularExpression("^" + mothersMaidenNameMiddle + "$", options: "i")),
                        Builders<possibleRelativeUnwinded>.Filter.Regex("name.middle", new BsonRegularExpression("^" + mothersMaidenNameMiddle + "$", options: "i")),
                        Builders<possibleRelativeUnwinded>.Filter.Regex("mothersMaidenName.middle", new BsonRegularExpression("^" + mothersMaidenNameMiddle + "$", options: "i")),
                        Builders<possibleRelativeUnwinded>.Filter.Regex("family.middle", new BsonRegularExpression("^" + mothersMaidenNameMiddle + "$", options: "i")),
                        Builders<possibleRelativeUnwinded>.Filter.Regex("name.last", new BsonRegularExpression("^" + fathersMiddleName + "$", options: "i")),
                        Builders<possibleRelativeUnwinded>.Filter.Regex("name.middle", new BsonRegularExpression("^" + fathersMiddleName + "$", options: "i")),
                        Builders<possibleRelativeUnwinded>.Filter.Regex("mothersMaidenName.middle", new BsonRegularExpression("^" + fathersMiddleName + "$", options: "i")),
                        Builders<possibleRelativeUnwinded>.Filter.Regex("family.middle", new BsonRegularExpression("^" + _test + "$", options: "i"))
                    )
                )
                .Project<possibleRelative>(new BsonDocument
                        {
                            {"_id",1},
                            {"slug",1},
                            {"employeeCode",1},
                            {"fullname", 1},
                            {"picture",1},
                            {"position","$employment.position"},
                            {"department","$employment.department"},
                            {"branch","$employment.branch"},
                            {"father", new BsonDocument { { "$concat", concatFamily } }},
                            {"mothersMaidenName", new BsonDocument { { "$concat", concatMaidenName } }}
                        })
                .ToList();

            
            return q;

            // var q = _context.Aggregate()
            //     .Match(
            //         Builders<users>.Filter.Or(
            //             Builders<users>.Filter.Regex("name.last", new BsonRegularExpression("^" + last + "$", options: "i")),
            //             Builders<users>.Filter.Regex("name.middle", new BsonRegularExpression("^" + middle + "$", options: "i")),
            //             Builders<users>.Filter.Regex("mothersMaidenName.middle", new BsonRegularExpression("^" + mothersMaidenNameMiddle + "$", options: "i")),
            //             Builders<users>.Filter.Regex("family.middle", new BsonRegularExpression("^" + fathersMiddleName + "$", options: "i"))
            //         )
            //     )
            //     .Project<possibleRelativeProjected>(Builders<users>.Projection.Combine(
            //         Builders<users>.Projection.Exclude("_id"),
            //         Builders<users>.Projection.Include("slug"),
            //         Builders<users>.Projection.Include("employeeCode"),
            //         Builders<users>.Projection.Include("fullname"),
            //         Builders<users>.Projection.Include("family"),
            //         Builders<users>.Projection.Include("mothersMaidenName")
            //     ))
            //     .Unwind<possibleRelativeProjected,possibleRelativeUnwinded>(f => f.family)
            //     .Project<possibleRelative>(Builders<possibleRelativeUnwinded>.Projection.Combine(
            //         Builders<possibleRelativeUnwinded>.Projection.Include("slug"),
            //         Builders<possibleRelativeUnwinded>.Projection.Include("employeeCode"),
            //         Builders<possibleRelativeUnwinded>.Projection.Include("fullname"),
            //         Builders<possibleRelativeUnwinded>.Projection.Include("slug")
            //     )).ToList();
                
            //     return q;
        }

        // public documentPerOwnerSlug[] GetListOfDocumentsPerOwnerSlug (String ownerSlug)
        // {
        //     // var q = _context.Aggregate()
        //     //     .Match(Builders<users>.Filter.Eq("documents.ownerSlug",ownerSlug))
        //     //     .Unwind<users>(p => p.documents)
        //     //     .Project<documentPerOwnerSlug>(new BsonDocument
        //     //             {
        //     //                 {"_id",0},
        //     //                 {"slug","$documents.slug"},
        //     //                 {"name","$documents.name"},
        //     //                 {"filename", "$documents.filename"},
        //     //                 {"path","$documents.path"},
        //     //                 {"description","$documents.description"}
        //     //             });
        //     // documentPerOwnerSlug[] list = q.ToList().ToArray();
        
        //     // return list;

        //     return null;
        // }

        public IEnumerable<users> AllProbationaryUsers()
        {
            var query = Builders<users>.Filter.Eq("employment.status","Probationary");
            var users = _db.GetCollection<users>("users").Find(query).ToListAsync().Result;

            foreach (var item in users)
            {
                item.employment.salary = encryptDecrypt.DecryptString(item.employment.salary);
            }
            return users;
        }
        
        public void updatePendingStatus(String slug, Boolean status){
            var query = Builders<users>.Filter.Eq("slug",slug);
            var update = Builders<users>.Update.Set("isPending",status);
            _context.UpdateOne(query,update);
        }

        public IEnumerable<types> getTypesByEmployeeSlug(String employeeSlug) {
            var filter = Builders<users>.Filter.Eq("slug", employeeSlug);
            var res = _db.GetCollection<users>("users").Find(filter).FirstOrDefault();
            if(res==null) {
                return null;
            } else {
                return res.leaves.types;
            }
        }

        public types getTypesByEmployeeSlugTypeSlug(String employeeSlug, String typeSlug) {
            var filter = Builders<users>.Filter.Eq("slug", employeeSlug);
            var filter1 = Builders<users>.Filter.Eq("leaves.types.slug", typeSlug);
            var filterAnd = Builders<users>.Filter.And(filter,filter1);
            var project = Builders<users>.Projection.Include("leaves.types.$");
            var res = _db.GetCollection<users>("users").Find(filterAnd).Project<users>(project).FirstOrDefault();
            types res2=new types();
            foreach(types t in res.leaves.types){
                res2 = t;
            }

            return res2;
        }

        public Boolean createTypesByEmployeeSlug(String employeeSlug, types types) {
            
            var filter = Builders<users>.Filter.Eq("slug", employeeSlug);
            var update = Builders<users>
                .Update
                .AddToSet("leaves.types", types)
                .CurrentDate("updatedAt");

            var res = _db.GetCollection<users>("users").UpdateOne(filter,update);
            return res.MatchedCount > 0;
        }

        public Boolean updateTypesByEmployeeSlugTypeSlug(String employeeSlug, String typeSlug, types types) {
            var filter = Builders<users>.Filter.Eq("slug", employeeSlug);
            var filter1 = Builders<users>.Filter.Eq("leaves.types.slug", typeSlug);
            var filterAnd = Builders<users>.Filter.And(filter,filter1);
            var update = Builders<users>
                .Update
                .Set("leaves.types.$.type", types.type)
                .Set("leaves.types.$.remaining", types.remaining)
                .Set("leaves.types.$.earning", types.earning)
                .CurrentDate("updatedAt");

            var res = _db.GetCollection<users>("users").UpdateOne(filterAnd,update);
            return res.MatchedCount > 0;
        }

        public Boolean deleteTypesByEmployeeSlugTypeSlug(String employeeSlug, String typeSlug){
            var filter = Builders<users>.Filter.Eq("slug", employeeSlug);
            var filter1 = Builders<types>.Filter.Eq("slug", typeSlug);
            var update = Builders<users>
                .Update
                .PullFilter("leaves.types", filter1)
                .CurrentDate("updatedAt");

            var res = _db.GetCollection<users>("users").UpdateOne(filter,update);
            return res.MatchedCount > 0;
        }

        public IEnumerable<users> directoryByBranchSlug(String branchSlug, String searchString) {
            IEnumerable<kmbi_core_master.coreMaster.Models.users> list = null;
            
            if(searchString=="all") {
                searchString = "";
            }

            if(branchSlug!=null && searchString!=null)
            {
                var filter1 = Builders<kmbi_core_master.coreMaster.Models.users>.Filter.Eq("employment.branch", branchSlug);
                var search1 = Builders<kmbi_core_master.coreMaster.Models.users>.Filter.Regex("fullname", new BsonRegularExpression(searchString,"i"));
                list = _context.Find(Builders<users>.Filter.And(filter1,search1)).ToList();
            }
            else
            {
                list = new List<kmbi_core_master.coreMaster.Models.users>();
            }
            
            return list;            
        }

        public IEnumerable<users> birthdatesByBranchSlug(String branchSlug, Int32 month)
        {

            var coll = _db.GetCollection<users>("users");
            
            var rep = coll.Aggregate()
                .Match(new BsonDocument{
                    {"employment.branch",branchSlug}
                })
                .Project<users>(new BsonDocument {
                    {"fullname",1},
                    {"name.last",1},
                    {"name.first",1},
                    {"name.middle",1},
                    {"extension",1},
                    {"slug",1},
                    {"birthdate",1},
                    {"employment",1}
                }).ToList();

            return from x in rep where x.birthdate.Month == month select x;
        }

        public IEnumerable<users> regularizationByBranchSlug(String branchSlug, DateTime dateFrom, DateTime dateTo) {

            var coll = _db.GetCollection<users>("users");
            
            var rep = coll.Aggregate()
                .Match(new BsonDocument{
                    {"employment.branch",branchSlug},
                    {"employment.status",new BsonRegularExpression("probationary","i")}
                })
                .Project<users>(new BsonDocument {
                    {"fullname",1},
                    {"name",1},
                    {"slug",1},
                    {"employment.branch",1},
                    {"employment.status",1},
                    {"employment.effectivityDateFrom",1}
                }).ToList();;

            

            var list = from x in rep where (x.employment.effectivityDateFrom.AddMonths(6).AddDays(1) >= dateFrom) && (x.employment.effectivityDateFrom.AddMonths(6).AddDays(1)) <= dateTo select x;
            return list;
        }

        

        public IEnumerable<users> GetListOfPendingUsers(String branchSlug){

            var filtera1 = Builders<users>.Filter.Exists("permissions.0",false);
            var filtera2 = Builders<users>.Filter.Eq("isVerified",1);
            var filterAnda1 = Builders<users>.Filter.And(filtera1,filtera2);

            var filterb1 = Builders<users>.Filter.Exists("permissions.0",true);
            var filterb2 = Builders<users>.Filter.Eq("isVerified",0);
            var filterbAnd1 = Builders<users>.Filter.And(filterb1,filterb2);

            var filterc1 = Builders<users>.Filter.Exists("permissions.0",false);
            var filterc2 = Builders<users>.Filter.Eq("isVerified",0);
            var filtercAnd1 = Builders<users>.Filter.And(filterc1,filterc2);

            var filterOr1 = Builders<users>.Filter.Or(filterAnda1,filterbAnd1, filtercAnd1);

            var filterBranch = Builders<users>.Filter.Eq("employment.branch", branchSlug);

            var filterFinal = Builders<users>.Filter.And(filterOr1,filterBranch);
            var u = _context.Find(filterFinal);
            List<users> l = u.ToList();
            return l;
        }

        public IEnumerable<users> getListOfServiceAwardees(DateTime from, DateTime to, String branchSlug, Int32 lengthOfService) {

            var coll = _db.GetCollection<users>("users");
            var filter = Builders<users>.Filter.Eq(u => u.employment.branch, branchSlug);
            var rep = coll.Find(filter).ToList();
            var list = from x in rep where DateTime.Now.Year - x.dateHired.Year >= lengthOfService select x;

            return from x in list where x.dateHired.AddYears(lengthOfService) >= @from && x.dateHired.AddYears(lengthOfService) <=to select x;
        }

        public IEnumerable<users> getListOfServiceAwardees(DateTime from, DateTime to, Int32 lengthOfService) {
            var coll = _db.GetCollection<users>("users");
            var rep = coll.Find(Builders<users>.Filter.Empty).ToList();
            var list = from x in rep where DateTime.Now.Year - x.dateHired.Year >= lengthOfService select x;

            return from x in list where x.dateHired.AddYears(lengthOfService) >= @from && x.dateHired.AddYears(lengthOfService) <=to select x;
        }

        public IEnumerable<users> universalSearch(universalSearch universalSearch) {
            var u = universalSearch;
            var collBranch = _db.GetCollection<branch>("branch");
            List<branch> branchList = collBranch.Find(Builders<branch>.Filter.Empty).ToList();
            var coll = _db.GetCollection<users>("users");
            
            List<FilterDefinition<users>> list = new List<FilterDefinition<users>>();

            // filters that can be done on mongodb

            // essentials

            if(u.name!=null) {list.Add(Builders<users>.Filter.Regex("fullname", new BsonRegularExpression(u.name, options: "i")));}
            if(u.email!=null) {list.Add(Builders<users>.Filter.Regex("email", new BsonRegularExpression(u.email, options: "i")));}
            if(u.branchSlug!=null) {list.Add(Builders<users>.Filter.Regex("employment.branch", new BsonRegularExpression(u.branchSlug, options: "i")));}
            if(u.address!=null) {list.Add(Builders<users>.Filter.Regex("addresses.location", new BsonRegularExpression(u.address, options: "i")));}

           if(u.ageFrom!=null && u.ageTo!=null) { 

                // var bDayOfAgeFrom = DateTime.Now.Date.AddYears((Int32)(u.ageFrom) * -1);
                // var bDayOfAgeTo = DateTime.Now.Date.AddYears((Int32)u.ageTo * -1);

                var bDayOfAgeFrom = DateTime.Now.Date.AddYears((Int32)(u.ageTo + 1) * -1);
                var bDayOfAgeTo = DateTime.Now.Date.AddYears((Int32)(u.ageFrom) * -1).AddSeconds(-1);

                list.Add(Builders<users>.Filter.And(
                    Builders<users>.Filter.Gte("birthdate",bDayOfAgeFrom),
                    Builders<users>.Filter.Lte("birthdate", bDayOfAgeTo)
                    )
                 );
            }
            // others

            if(u.civilStatus!=null) {list.Add(Builders<users>.Filter.Regex("civilStatus", new BsonRegularExpression(u.civilStatus, options: "i")));}
            if(u.religion!=null) {list.Add(Builders<users>.Filter.Regex("religion", new BsonRegularExpression(u.religion, options: "i")));}
            if(u.sss!=null) {list.Add(Builders<users>.Filter.Regex("sss", new BsonRegularExpression(u.sss, options: "i")));}
            if(u.pagibig!=null) {list.Add(Builders<users>.Filter.Regex("pagibig", new BsonRegularExpression(u.pagibig, options: "i")));}
            if(u.philhealth!=null) {list.Add(Builders<users>.Filter.Regex("philhealth", new BsonRegularExpression(u.philhealth, options: "i")));}
            if(u.tin!=null) {list.Add(Builders<users>.Filter.Regex("tin", new BsonRegularExpression(u.tin, options: "i")));}
            if(u.taxCode!=null) {list.Add(Builders<users>.Filter.Regex("taxCode", new BsonRegularExpression(u.taxCode, options: "i")));}
            if(u.gender!=null) {list.Add(Builders<users>.Filter.Regex("gender", new BsonRegularExpression(u.gender, options: "i")));}
            if(u.salutation!=null) {list.Add(Builders<users>.Filter.Regex("salutation", new BsonRegularExpression(u.salutation, options: "i")));}
            if(u.extension!=null) {list.Add(Builders<users>.Filter.Regex("extension", new BsonRegularExpression(u.extension, options: "i")));}
            if(u.mobile!=null) {list.Add(Builders<users>.Filter.Regex("contacts.mobile", new BsonRegularExpression(u.mobile, options: "i")));}
            if(u.landline!=null) {list.Add(Builders<users>.Filter.Regex("contacts.landline", new BsonRegularExpression(u.landline, options: "i")));}
            if(u.birthdateFrom!=null && u.birthdateTo!=null) {
                list.Add(Builders<users>.Filter.And(
                    Builders<users>.Filter.Gte("birthdate",u.birthdateFrom),
                    Builders<users>.Filter.Lte("birthdate", u.birthdateTo)
                    )
                );
            }
            if(u.skill!=null) {list.Add(Builders<users>.Filter.Regex("skills.type", new BsonRegularExpression(u.skill, options: "i")));}
            if(u.position!=null) {list.Add(Builders<users>.Filter.Regex("employment.position", new BsonRegularExpression(u.position, options: "i")));}
            if(u.department!=null) {list.Add(Builders<users>.Filter.Regex("employment.department", new BsonRegularExpression(u.department, options: "i")));}
            if(u.jobGrade!=null) {list.Add(Builders<users>.Filter.Regex("employment.jobGrade", new BsonRegularExpression(u.jobGrade, options: "i")));}
            if(u.jobClassification!=null) {list.Add(Builders<users>.Filter.Regex("employment.jobClassification", new BsonRegularExpression(u.jobClassification, options: "i")));}
            if(u.jobCategory!=null) {list.Add(Builders<users>.Filter.Regex("employment.jobCategory", new BsonRegularExpression(u.jobCategory, options: "i")));}
            if(u.status!=null) {list.Add(Builders<users>.Filter.Regex("employment.status", new BsonRegularExpression(u.status, options: "i")));}

            if(u.allowanceFrom!=null && u.allowanceTo!=null) {
                list.Add(Builders<users>.Filter.And(
                    Builders<users>.Filter.Gte("employment.allowance.amount",u.allowanceFrom),
                    Builders<users>.Filter.Lte("employment.allowance.amount", u.allowanceTo)
                    )
                );
            }

            if(u.benefitsFrom!=null && u.benefitsTo!=null) {
                list.Add(Builders<users>.Filter.And(
                    Builders<users>.Filter.Gte("benefits.amount",u.benefitsFrom),
                    Builders<users>.Filter.Lte("benefits.amount", u.benefitsTo)
                    )
                );
            }
            //var a = encryptDecrypt.DecryptString("dUCyeB2XBXx14XXpTqT4limrvzFS6F5L6Zy3AtQiCWY=");

            if(u.nickname!=null) {list.Add(Builders<users>.Filter.Regex("nickname", new BsonRegularExpression(u.nickname, options: "i")));}
            
            List<users> finalList = new List<users>();

            if(list.Count>0) {
                finalList = coll.Aggregate().Match(Builders<users>.Filter.And(list)).ToList();
            } else {

                // finalList = coll.Aggregate().Match(Builders<users>.Filter.Empty).ToList();
                // if(u.salaryFrom!=null && u.salaryTo!=null) {
                //     finalList = (from x in finalList where 
                //         decimal.Parse(encryptDecrypt.DecryptString(x.employment.salary)) >= (Decimal)u.salaryFrom &&
                //         decimal.Parse(encryptDecrypt.DecryptString(x.employment.salary)) <= (Decimal)u.salaryTo
                //         select x).ToList();
                // }    
            }

            
            
            finalList.ForEach((x) => {
                var b  = branchList.Where(r=>r.code == x.employment.branch).FirstOrDefault();
                if(b!=null) {
                    x.employment.branch = b.name;
                } else {
                    x.employment.branch  = "";
                }
            });
            return finalList;
        }

        public Boolean setEmployeeStatus(String employeeSlug, String status) {
            var coll = _db.GetCollection<users>("users");
            var filter = Builders<users>.Filter.Eq(x=>x.slug,employeeSlug);
            var update = Builders<users>.Update.Set(x=>x.employment.status,status);
            var result = coll.UpdateOne(filter,update);
            return true;
        }

        public IEnumerable<users> getSubordinates (String isSlug) {
            var coll = _db.GetCollection<users>("users");
            var filter = Builders<users>.Filter.Eq(x=>x.immediateSupervisor,isSlug);
            var list = coll.Find(filter).ToList();
            return list;
        }

        public Boolean updateSubordinatesIs(String isSlug, List<String> subordinatesSlug) {
            var coll = _db.GetCollection<users>("users");
            var filter = Builders<users>.Filter.In(x=>x.slug,subordinatesSlug);
            var update = Builders<users>.Update.Set(x=>x.immediateSupervisor,isSlug);
            var result = coll.UpdateMany(filter,update);
            return true;
        }

        public Double countLeaves(String employeeCode, DateTime date, String leaveType) {
            //var nextMonth = date.AddMonths(1);
            var thisYearLastMonth = DateTime.Parse("12/31/" + DateTime.Now.Year);
            
            var count = 0;
            for(var dtCtr=date;dtCtr<=thisYearLastMonth;dtCtr = dtCtr.AddMonths(1))
            {
                count++;
                if(count>=12) {
                    dtCtr= thisYearLastMonth.AddDays(1);
                }
            }

            
            var filterEmpCode = Builders<leaveLog>.Filter.Eq("employee.code",employeeCode);
            var filterLeaveType = Builders<leaveLog>.Filter.Eq("type",leaveType);

            var filterFrom = Builders<leaveLog>.Filter.Gt("dates.from",DateTime.Parse("1/1/" + DateTime.Now.Year));
            var filterTo = Builders<leaveLog>.Filter.Lt("dates.to",DateTime.Parse("12/31/" + DateTime.Now.Year));
            
            var leaveLogs = _db.GetCollection<leaveLog>("leaveLogs");
            var list = leaveLogs.Find(Builders<leaveLog>.Filter.And(filterEmpCode, filterLeaveType, filterFrom, filterTo)).ToList();

            var totalDays = 0.0;

            list.ForEach((x) => {
                if(x.withPay!=null) {
                    if((bool)x.withPay) {

                        totalDays += (x.dates.to.Date - x.dates.from.Date).TotalDays + 1;
                        
                        for(DateTime ctr = x.dates.from.Date; ctr<=x.dates.to.Date;ctr = ctr.AddDays(1))
                            totalDays -= (ctr.DayOfWeek == DayOfWeek.Saturday && ctr.DayOfWeek == DayOfWeek.Sunday) ? 1 : 0;

                        if(x.dates.from.Hour > 12)
                            totalDays -= .5;

                        if(x.dates.to.Hour <= 12)
                            totalDays -= .5;
                    }
                }
            });

            return (count * 1.25) - totalDays;
        }

        public void updateLeaveBalances(DateTime effectivityDate, List<users> users)
        {
            users.ToList().ForEach((x) => {

                var remaiMat = countLeaves(x.employeeCode,effectivityDate,"Maternity");
                var remaiPat = countLeaves(x.employeeCode,effectivityDate,"Paternity");
                var remaiSl = countLeaves(x.employeeCode,effectivityDate,"Sick Leave");
                var remaiSingPar = countLeaves(x.employeeCode,effectivityDate,"Single Parent");
                var remaiTrav = countLeaves(x.employeeCode,effectivityDate,"Travel");
                var remaiVl = countLeaves(x.employeeCode,effectivityDate,"Vacation Leave");

                x.leaves = new leaves {
                    types = new List<types> {
                        new types {slug="maternityLeaveSlug", type="Maternity", remaining = remaiMat},
                        new types {slug="paternityLeaveSlug", type="Paternity", remaining = remaiPat},
                        new types {slug="sickLeaveSlug", type="Sick Leave", remaining = remaiSl, earning = 15},
                        new types {slug="singleParentLeaveSlug", type="Single Parent", remaining = remaiSingPar},
                        new types {slug="travelLeaveSlug", type="Travel", remaining = remaiTrav},
                        new types {slug="vacationLeaveSlug", type="Vacation Leave", remaining = remaiVl, earning = 15}
                    }
                };
                var coll = _db.GetCollection<users>("users");
                coll.UpdateOne(Builders<users>.Filter.Eq(y=>y.slug, x.slug),
                    Builders<users>.Update.Set(y=>y.leaves,x.leaves));
            });
        }

        public loginInfo getLoginInfo(String token)
        {
            var query = Builders<loginInfo>.Filter.Eq(e => e.token, token);
            var fields = Builders<loginInfo>.Projection
                .Include(u => u.id)
                .Include(u => u.fullname)
                .Include(u => u.employment.branch);
            var info = _db.GetCollection<loginInfo>("users").Find(query).Project<loginInfo>(fields).FirstOrDefault();
            return info;

        }

        public employeeSchedule[] fixSkeds(employeeSchedule[] skeds) {
            var _skeds = skeds.ToList().OrderByDescending(x=>x.effectivityDateFrom).ToList();
            DateTime? firstDate = null;
            _skeds.ForEach(x=>{
                // x.effectivityDateTo = new DateTime(DateTime.Now.Year,12,31);
                // if(firstDate==null)
                //     // x.effectivityDateTo = new DateTime(DateTime.Now.Year,12,31);
                // else
                //     x.effectivityDateTo = Convert.ToDateTime(firstDate).AddDays(-1);
                
                firstDate = x.effectivityDateFrom;
            });

            return _skeds.ToArray();
        }
    }
}
