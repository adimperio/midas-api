using kmbi_core_master.documentationModule.Models;
using kmbi_core_master.coreMaster.Repository;

using kmbi_core_master.coreMaster.Models;
using MongoDB.Driver;
using Microsoft.Extensions.Options;

namespace kmbi_core_master.documentationModule
{
    public class context
    {
        public context(IOptions<dbSettings> settings)
        {
            dbSettings sets = settings.Value;
            var client = new MongoClient(sets.MongoConnection);
            var db = client.GetDatabase(sets.Database);
            
            tutorials = new repository<tutorial>(db);
            policies = new repository<policy>(db);
            comments = new repository<comment>(db);
            tickets = new repository<ticket>(db);
            activities = new activityRepository(settings);
            holidays = new repository<holiday>(db);
            loans = new repository<loans>(db);
            branches = new repository<branch>(db);
        }

        public repository<tutorial> tutorials {get; private set;}
        public repository<policy> policies {get; private set;}
        public repository<comment> comments {get;private set;}
        public repository<ticket> tickets {get; private set;}
        public activityRepository activities {get;private set;}
        public repository<holiday> holidays {get;private set;}
        public repository<loans> loans {get;private set;}
        public repository<branch> branches {get;private set;}
    }
}