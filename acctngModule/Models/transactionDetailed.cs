using System;

namespace kmbi_core_master.acctngModule.Models
{
    public class  transactionDetailed
    {
        public String slug {get;set;}
        public String book {get;set;}
        public String number {get;set;}
        public String particulars {get;set;}
        public String accountType {get;set;}
        public Int32 motherCode {get;set;}
        public Int32 code {get;set;}
        public String codeElement {get;set;}
        public String name {get;set;}
        public Double amount {get;set;}
        public DateTime issuedAt {get;set;}
    }

    public class transactionDetailedIs : transactionDetailed {
        public String isItem {get;set;}
    }
}