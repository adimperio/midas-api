﻿using Microsoft.Extensions.Options;

using System;
using System.Collections.Generic;
using System.Linq;

using MongoDB.Bson;
using MongoDB.Driver;

using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.acctngModule.Models;
using kmbi_core_master.acctngModule.Interface;

namespace kmbi_core_master.acctngModule.Repository
{
    public class cashReceiptRepository : dbCon, icashReceiptRepository
    {
        
        public cashReceiptRepository(IOptions<dbSettings> settings) : base(settings) {}

        public void add(cashReceipt b)
        {

            b.timestamps.createdAt = DateTime.Now;
            b.timestamps.updatedAt = DateTime.Now;
            for (int i = 0; i <= b.accounts.credit.Count() - 1; i++)
            {
                b.accounts.credit[i].Id = ObjectId.GenerateNewId().ToString();
            }

            for (int i = 0; i <= b.accounts.debit.Count() - 1; i++)
            {
                b.accounts.debit[i].Id = ObjectId.GenerateNewId().ToString();
            }
            var coll = _db.GetCollection<cashReceipt>("cashReceipt");
            _db.GetCollection<cashReceipt>("cashReceipt").InsertOneAsync(b);
        }
        

        public IEnumerable<cashReceipt> all()
        {
            var c = _db.GetCollection<cashReceipt>("cashReceipt").Find(new BsonDocument()).ToListAsync();
            return c.Result;
        }

        public IEnumerable<cashReceipt> all(DateTime from, DateTime to)
        {
            var filterFrom = Builders<cashReceipt>.Filter.Gte(cr=>cr.timestamps.issuedAt, from);
            var filterTo = Builders<cashReceipt>.Filter.Lte(cr=>cr.timestamps.issuedAt, to);
            var c = _db.GetCollection<cashReceipt>("cashReceipt").Find(Builders<cashReceipt>.Filter.And(filterFrom, filterTo)).ToListAsync();
            return c.Result;
        }

        public cashReceipt getSlug(String slug)
        {
            var query = Builders<cashReceipt>.Filter.Eq(e => e.slug, slug);
            var c = _db.GetCollection<cashReceipt>("cashReceipt").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }
       
        public void update(String slug, cashReceipt p)
        {
            p.slug = slug;
            var query = Builders<cashReceipt>.Filter.Eq(e => e.slug, slug);
            var update = Builders<cashReceipt>.Update
                .Set("number", p.number)
                .Set("dateDeposited", p.dateDeposited)
                .Set("particulars", p.particulars)
                .Set("branch", p.branch)
                .Set("accounts", p.accounts)
                .Set("payor", p.payor)
                .Set("amount", p.amount)
                .Set("isCancelled", p.isCancelled)
                .Set("isClosed", p.isClosed)
                .Set("reason", p.reason)
                .Set("status", p.status)
                .Set("timestamps.issuedAt", p.timestamps.issuedAt)
                .Set("timestamps.issuedBy", p.timestamps.issuedBy)
                .Set("timestamps.createdBy", p.timestamps.createdBy)
                .Set("timestamps.updatedBy", p.timestamps.updatedBy)
                .CurrentDate("timestamps.updatedAt");

            var cr = _db.GetCollection<cashReceipt>("cashReceipt").UpdateOneAsync(query, update);

        }
        public void remove(String slug)
        {
            var query = Builders<cashReceipt>.Filter.Eq(e => e.slug, slug);
            var result = _db.GetCollection<cashReceipt>("cashReceipt").DeleteOneAsync(query);
        }

        public List<cashReceipt> issuedAt(DateTime from, DateTime to)
        {
            
            var fdate = Builders<cashReceipt>.Filter.Gte(e => e.timestamps.issuedAt, from);
            var tdate = Builders<cashReceipt>.Filter.Lte(e => e.timestamps.issuedAt, to);
            var sort = Builders<cashReceipt>.Sort.Descending(e => e.timestamps.issuedAt);
            var query = Builders<cashReceipt>.Filter.And(fdate, tdate);

            var c = _db.GetCollection<cashReceipt>("cashReceipt").Find(query).Sort(sort).ToListAsync();
            return c.Result;

        }

        //CREDIT
        public crAccounts getCreditId(string creditId)
        {

            var query = Builders<crAccounts>.Filter.ElemMatch(x => x.credit, x => x.Id == creditId);
            var fields = Builders<crAccounts>.Projection
                .Include(u => u.credit)
                .ElemMatch(x => x.credit, x => x.Id == creditId);

            var unit = _db.GetCollection<crAccounts>("cashReceipt").Find(query).Project<crAccounts>(fields).ToListAsync();
            return unit.Result.FirstOrDefault();
        }

        public void addCredit(crCredit u, string crSlug)
        {
            var query = Builders<cashReceipt>.Filter.Eq(e => e.slug, crSlug);
            var update = Builders<cashReceipt>.Update.Push(e => e.accounts.credit, u);
            u.Id = ObjectId.GenerateNewId().ToString();
            _db.GetCollection<cashReceipt>("cashReceipt").FindOneAndUpdateAsync(query, update);
        }

        public void updateCredit(string crSlug, string creditId, crCredit u)
        {

            var query = Builders<cashReceipt>.Filter.And(Builders<cashReceipt>.Filter.Eq(x => x.slug, crSlug),
            Builders<cashReceipt>.Filter.ElemMatch(x => x.accounts.credit, x => x.Id == creditId));

            var update = Builders<cashReceipt>.Update
                .Set("credit.$.name", u.name)
                .Set("credit.$.code", u.code)
                .Set("credit.$.amount", u.amount);
            var result = _db.GetCollection<cashReceipt>("cashReceipt").UpdateOneAsync(query, update);
        }

        public void removeCredit(String crSlug, string creditId)
        {
            var pull = Builders<cashReceipt>.Update.PullFilter(x => x.accounts.credit, a => a.Id == creditId);

            var filter1 = Builders<cashReceipt>.Filter.And(Builders<cashReceipt>.Filter.Eq(a => a.slug, crSlug),
                          Builders<cashReceipt>.Filter.ElemMatch(q => q.accounts.credit, t => t.Id == creditId));

            var result = _db.GetCollection<cashReceipt>("cashReceipt").UpdateOneAsync(filter1, pull);
        }


        //DEBIT
        public cashReceipt getDebitId(string debitId)
        {

            var query = Builders<cashReceipt>.Filter.ElemMatch(x => x.accounts.debit, x => x.Id == debitId);
            var fields = Builders<cashReceipt>.Projection
                .Include(u => u.Id)
                .Include(u => u.slug)
                .ElemMatch(x => x.accounts.debit, x => x.Id == debitId);

            var unit = _db.GetCollection<cashReceipt>("cashReceipt").Find(query).Project<cashReceipt>(fields).ToListAsync();
            return unit.Result.FirstOrDefault();
        }

        public void addDebit(crDebit u, string crSlug)
        {
            var query = Builders<cashReceipt>.Filter.Eq(e => e.slug, crSlug);
            var update = Builders<cashReceipt>.Update.Push(e => e.accounts.debit, u);
            u.Id = ObjectId.GenerateNewId().ToString();
            _db.GetCollection<cashReceipt>("cashReceipt").FindOneAndUpdateAsync(query, update);
        }

        public void updateDebit(string crSlug, string debitId, crDebit u)
        {

            var query = Builders<cashReceipt>.Filter.And(Builders<cashReceipt>.Filter.Eq(x => x.slug, crSlug),
            Builders<cashReceipt>.Filter.ElemMatch(x => x.accounts.debit, x => x.Id == debitId));

            var update = Builders<cashReceipt>.Update
                .Set("debit.$.name", u.name)
                .Set("debit.$.code", u.code)
                .Set("debit.$.amount", u.amount);
            var result = _db.GetCollection<cashReceipt>("cashReceipt").UpdateOneAsync(query, update);
        }

        public void removeDebit(String crSlug, string debitId)
        {
            var pull = Builders<cashReceipt>.Update.PullFilter(x => x.accounts.debit, a => a.Id == debitId);

            var filter1 = Builders<cashReceipt>.Filter.And(Builders<cashReceipt>.Filter.Eq(a => a.slug, crSlug),
                          Builders<cashReceipt>.Filter.ElemMatch(q => q.accounts.debit, t => t.Id == debitId));

            var result = _db.GetCollection<cashReceipt>("cashReceipt").UpdateOneAsync(filter1, pull);
        }

        public cashReceipt checkOr(String orNo)
        {
            var query = Builders<cashReceipt>.Filter.Eq(e => e.number, orNo);
            var c = _db.GetCollection<cashReceipt>("cashReceipt").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }


        public void updateIsCancelled(String slug, cashReceipt p)
        {
            p.slug = slug;
            var query = Builders<cashReceipt>.Filter.Eq(e => e.slug, slug);
            var update = Builders<cashReceipt>.Update
                .Set("isCancelled", true)
                .Set("reason", p.reason)
                .Set("timestamps.updatedBy", p.timestamps.updatedBy)
                .CurrentDate("timestamps.updatedAt");

            var cr = _db.GetCollection<cashReceipt>("cashReceipt").UpdateOneAsync(query, update);

        }

        public void updateIsClosed(DateTime from, DateTime to, cashReceipt p)
        {
            var fdate = Builders<cashReceipt>.Filter.Gte(e => e.timestamps.issuedAt, from);
            var tdate = Builders<cashReceipt>.Filter.Lte(e => e.timestamps.issuedAt, to);
            //var c = _db.GetCollection<cashReceipt>("cashReceipt").Find(query).Sort(sort).ToListAsync();
            //return c.Result;
            var query = Builders<cashReceipt>.Filter.And(fdate, tdate);
            var update = Builders<cashReceipt>.Update
                .Set("isClosed", true)
                .Set("timestamps.updatedBy", p.timestamps.updatedBy)
                .CurrentDate("timestamps.updatedAt");

            var cr = _db.GetCollection<cashReceipt>("cashReceipt").UpdateOneAsync(query, update);

        }
    }
}
