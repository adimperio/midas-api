﻿using kmbi_core_master.coreMaster.Models;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.reports.Models
{

    public class repPromisorryNote
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string id { get; set; }
        public String referenceId { get; set; }
        public DateTime disbursementDate { get; set; }
        public DateTime firstPaymentDate { get; set; }

        public loanloanTypes loanType { get; set; }
        public amounts amounts { get; set; }
        
        public IList<loanMembers> members { get; set; }
        public List<centerInstance> center { get; set; }

        public Int32 version {get;set;}
        public double weeklyAmount { get; set; }
        public DateTime maturityDate { get; set; }
    }

    

}
