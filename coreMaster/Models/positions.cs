﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace kmbi_core_master.coreMaster.Models
{
    public class positions
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("name")]
        public string name { get; set; }
       
        [BsonElement("department")]
        public department department { get; set; }

        [BsonElement("job")]
        public job job { get; set; }

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }
    }

    public class department
    {
        [BsonElement("id")]
        public string id { get; set; }
        
        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }
    }

    public class job
    {
        [BsonElement("grade")]
        public int grade { get; set; }

        [BsonElement("classification")]
        public string classification { get; set; }

        [BsonElement("category")]
        public string category { get; set; }
    }
}
