﻿using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Models;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace kmbi_core_master.coreMaster.Repository
{
    public class cbuLogsRepository : dbCon, icbuLogsRepository
    {
       
        public cbuLogsRepository(IOptions<dbSettings> settings) : base(settings)
        {
        }

        public void Add(cbuLogs c)
        {
            c.createdAt = DateTime.Now;
            c.updatedAt = DateTime.Now;
            
            _db.GetCollection<cbuLogs>("cbuLogs").InsertOneAsync(c);
        }

       
        public IEnumerable<cbuLogs> getAll()
        {
            var c = _db.GetCollection<cbuLogs>("cbuLogs").Find(new BsonDocument()).ToListAsync();
            return c.Result;
        }

        public List<cbuLogs> Get(String id)
        {
            var query = Builders<cbuLogs>.Filter.Eq(e => e.Id, id);
            var c = _db.GetCollection<cbuLogs>("cbuLogs").Find(query).ToListAsync();
            
            return c.Result;
        }

        public List<cbuLogs> clientId(String clientId)
        {
            var query = Builders<cbuLogs>.Filter.Eq(e => e.clientId, clientId);
            var c = _db.GetCollection<cbuLogs>("cbuLogs").Find(query).ToListAsync();

            return c.Result;
        }


        public List<cbuLogs> clientIdDates(String clientId, DateTime from, DateTime to)
        {
            var id = Builders<cbuLogs>.Filter.Eq(e => e.clientId, clientId);
            var fdate = Builders<cbuLogs>.Filter.Gte(e => e.createdAt, from);
            var tdate = Builders<cbuLogs>.Filter.Lte(e => e.createdAt, to);
            var sort = Builders<cbuLogs>.Sort.Descending(e => e.createdAt);
            var query = Builders<cbuLogs>.Filter.And(id, fdate, tdate);

            var c = _db.GetCollection<cbuLogs>("cbuLogs").Find(query).Sort(sort).ToListAsync();
            return c.Result;

        }


        public List<cbuLogs> getSlug(String slug)
        {
            var query = Builders<cbuLogs>.Filter.Eq(e => e.slug, slug);
            var fields = Builders<cbuLogs>.Projection
                .Include(u => u.Id)
                .Include(u => u.type)
                .Include(u => u.slug)
                .Include(u => u.clientId)
                .Include(u => u.amount)
                .Include(u => u.cbuBalance)
                .Include(u => u.loan)
                .Include(u => u.transactedAt)
                .Include(u => u.createdAt)
                .Include(u => u.updatedAt);

            var c = _db.GetCollection<cbuLogs>("cbuLogs").Find(query).Project<cbuLogs>(fields).ToListAsync();

            return c.Result;
        }




        public void Update(String slug, cbuLogs p)
        {
            
            var filter = Builders<cbuLogs>.Filter.Eq(e => e.slug, slug);
            var update = Builders<cbuLogs>.Update
                .Set("clientId", p.clientId)
                .Set("type", p.type)
                .Set("amount", p.amount)
                .Set("cbuBalance", p.cbuBalance)
                .Set("loan", p.loan)
                .Set("transactedAt", p.transactedAt)
                .CurrentDate("updatedAt");
            var result = _db.GetCollection<cbuLogs>("cbuLogs").UpdateOneAsync(filter, update);

        }

       
        public void Remove(String slug)
        {
            var query = Builders<cbuLogs>.Filter.Eq(e => e.slug, slug);
            var result = _db.GetCollection<cbuLogs>("cbuLogs").DeleteOneAsync(query);
            
        }


        public cbuLogs checkSlug(String slug)
        {
            var query = Builders<cbuLogs>.Filter.Eq(e => e.slug, slug);
            var c = _db.GetCollection<cbuLogs>("cbuLogs").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }

        public List<cbuLogsBytrasactedAt> incremPercentage(DateTime from, DateTime to, string branchSlug)
        {
            var dtTo = to.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
            //var q1 = Builders<cbuLogs>.Filter.Gte(x => x.transactedAt, from);
            //var q2 = Builders<cbuLogs>.Filter.Lte(x => x.transactedAt, dtTo);
            //var q3 = Builders<cbuLogs>.Filter.Eq(x => x.type, "increment percentage");
            //var query = Builders<cbuLogs>.Filter.And(q1, q2, q3);
            //var c = _db.GetCollection<cbuLogs>("cbuLogs").Find(query).ToListAsync();


            //return c.Result;
            
            var coll = _db.GetCollection<cbuLogs>("cbuLogs");
            var rep = coll.Aggregate()
                .Match(e => e.transactedAt >= from)
                .Match(e => e.transactedAt <= dtTo)
                .Match(e => e.type == "increment percentage")
                .Lookup("clients", "slug", "slug", "clients")
                .Match(new BsonDocument { { "clients.branchSlug", branchSlug } })
                .Project<cbuLogs>(new BsonDocument
                {
                    {"Id", 1},
                    {"type",1},
                    {"slug",1},
                    {"clientId", 1},
                    {"amount", 1},
                    {"cbuBalance",1},
                    {"loan",1},
                    {"transactedAt",1},
                    {"createdAt",1},
                    {"updatedAt",1}
                })
                .ToListAsync();

            List<cbuLogsBytrasactedAt> cbuLogs = new List<cbuLogsBytrasactedAt>();
            
            foreach (var item in rep.Result)
            {
                if (item.loan != null)
                {
                    var q1 = Builders<loans>.Filter.Gte(x => x.Id, item.loan.id);
                    var c = _db.GetCollection<loans>("loans").Find(q1).ToListAsync().Result.FirstOrDefault();
                    var clientCount = c.members.Count();

                    cbuLogs.Add(new cbuLogsBytrasactedAt
                    {
                        Id = item.Id,
                        type = item.type,
                        slug = item.slug,
                        clientId = item.clientId,
                        amount = item.amount,
                        cbuBalance = item.cbuBalance,
                        loan = new cbuLoanBytrasactedAt
                        {
                            id = item.loan.id,
                            termNo = item.loan.termNo,
                            loanType = c.loanType.name,
                            centerName = c.centerName,
                            noClients = clientCount
                        },
                        transactedAt = item.transactedAt,
                        createdAt = item.createdAt,
                        updatedAt = item.createdAt
                    });
                }
                else
                {
                    cbuLogs.Add(new cbuLogsBytrasactedAt
                    {
                        Id = item.Id,
                        type = item.type,
                        slug = item.slug,
                        clientId = item.clientId,
                        amount = item.amount,
                        cbuBalance = item.cbuBalance,
                        loan = null,
                        transactedAt = item.transactedAt,
                        createdAt = item.createdAt,
                        updatedAt = item.createdAt
                    });
                }
                
            }

            

            return cbuLogs;
        }

        public cbuLogs getFirstEntry(String clientId) {
            var coll = _db.GetCollection<cbuLogs>("cbuLogs");
            var filter = Builders<cbuLogs>.Filter.Eq("clientId",clientId);
            var sort = Builders<cbuLogs>.Sort.Ascending(c=>c.createdAt);
            cbuLogs firstEntry = coll.Find(filter).Sort(sort).Limit(1).FirstOrDefault();
            return firstEntry;
        }

        public Boolean updateFirstEntry(String clientId, Double amount) {
            // cbulogs collection
            var collCbuLogs = _db.GetCollection<cbuLogs>("cbuLogs");

            // update first entry
            var filterCbuLogs = Builders<cbuLogs>.Filter.Eq("clientId",clientId);
            //var filterBalance = Builders<cbuLogs>.Filter.Gt(c=>c.cbuBalance,0.0);
            //var filterAnd = Builders<cbuLogs>.Filter.And(filterCbuLogs, filterBalance);
            var sortCbuLogs = Builders<cbuLogs>.Sort.Ascending(c=>c.createdAt);
            cbuLogs firstEntry = collCbuLogs.Find(filterCbuLogs).Sort(sortCbuLogs).FirstOrDefault();
            
            if(firstEntry!=null) {
                var filter = Builders<cbuLogs>.Filter.Eq(c=>c.Id,firstEntry.Id);
                var updateFirstEntry = Builders<cbuLogs>.Update
                        .Set(locC=>locC.amount, amount)
                        .Set(locC=>locC.cbuBalance, amount)
                        .CurrentDate("updatedAt");

                collCbuLogs.UpdateOne(filter, updateFirstEntry);
            }


            // reupdate list
            List<cbuLogs> list = collCbuLogs.Find(filterCbuLogs).Sort(sortCbuLogs).ToList();

            if(list!=null) {
                var bal = 0.0;
                list.ForEach(c=>{
                    bal+=c.amount;
                    c.cbuBalance = amount;

                    var filter2 = Builders<cbuLogs>.Filter.Eq(locC=>locC.Id, c.Id);
                    var update = Builders<cbuLogs>.Update
                        .Set(locC=>locC.cbuBalance, bal)
                        .CurrentDate("updatedAt");

                    collCbuLogs.UpdateMany(filter2, update);
                });
                
                var coll2 = _db.GetCollection<clients>("clients");
                var filter3 = Builders<clients>.Filter.Eq("Id", clientId);
                var update2 = Builders<clients>.Update
                        .Set(locC=>locC.cbuBalance, bal)
                        .CurrentDate("updatedAt");

                coll2.UpdateOne(filter3, update2);

                return true;
            }
            return false;
        }

        public IEnumerable<cbuLogs> getCbuLogsPerBranchPerDateRange(String branchSlug, DateTime from, DateTime to)
        {
            var clients = _db.GetCollection<clients>("clients").Find(Builders<clients>.Filter.Eq("branchSlug", branchSlug)).ToList();

            var cbuLogsResult = _db.GetCollection<cbuLogs>("cbuLogs").Find(
                Builders<cbuLogs>.Filter.And(
                    Builders<cbuLogs>.Filter.Gte(c=>c.createdAt, from.Date),
                    Builders<cbuLogs>.Filter.Lte(c=>c.createdAt, to.Date.AddHours(23).AddSeconds(59)),
                    Builders<cbuLogs>.Filter.In(c=> c.clientId, clients.ToList().Select(c=>c.Id))
                )
            ).ToList();
            // var cbuLogsResult = _db.GetCollection<cbuLogs>("cbuLogs").Aggregate()
            //        .Match(e => e.createdAt >= from.Date)
            //        .Match(e => e.createdAt <= to.Date.AddHours(23).AddSeconds(59))
            //        .Lookup("clients", "slug", "slug", "logs")
            //     //    .Match(new BsonDocument { { "logs.branchSlug", branchSlug } })
            //        .Project<cbuLogs>(new BsonDocument
            //                {
                            
            //                 {"type",1},
            //                 {"slug",1},
            //                 {"clientId",1},
            //                 {"amount",1},
            //                 {"cbuBalance",1},
            //                 {"loan",1},
            //                 {"transactedAt",1},
            //                 {"createdAt",1},
            //                 {"updatedAt",1}
            //         })
            //        .ToList();

            return cbuLogsResult;
        }

        public void fixBegCbu() {
            //from cbuLogs get clients with beg cbu
            var colCbuLogs = _db.GetCollection<cbuLogs>("cbuLogs");
            var cbuLogsWithBegCbu = colCbuLogs.Find(Builders<cbuLogs>.Filter.Eq(cbu=>cbu.type,"beginning balance")).ToList();
            //from clients get clients not in list of client with beg cbu
            var colClients = _db.GetCollection<clients>("clients");
            var clientWithoutBegCbu = colClients.Find(Builders<clients>.Filter.Nin(cli=>cli.Id,(from x in cbuLogsWithBegCbu select x.clientId).ToList())).ToList();
            //create cbu with client info
            List<cbuLogs> cbuLogss = new List<cbuLogs>();
            foreach(clients c in clientWithoutBegCbu.ToList()) {
                cbuLogss.Add(
                    new cbuLogs { 
                        Id = ObjectId.GenerateNewId().ToString(), 
                        type = "beginning balance", 
                        slug = c.slug + '-' + global.slugify(8), 
                        clientId = c.Id, 
                        amount = c.cbuBalance, 
                        cbuBalance =+ c.cbuBalance, 
                        createdAt = c.createdAt, 
                        updatedAt = c.createdAt 
                    }
                );
            }
            //insert cbu
            if(cbuLogss.Count>0)
                colCbuLogs.InsertMany(cbuLogss);
        }

        public IEnumerable<cbuLogs> getBeginningBalance() {
            return _db.GetCollection<cbuLogs>("cbuLogs").Find(Builders<cbuLogs>.Filter.Eq(x=>x.type,null)).ToList();
        }

        public void UpdateCbuBalance(String ClientId, Double Amount) {
            var client = _db.GetCollection<clients>("clients").Find(c=>c.Id == ClientId).FirstOrDefault();
            var now = DateTime.Now;
            if(client!=null) {
                // update client.cbuBalance            
                var filter = Builders<clients>.Filter.Eq(c=> c.Id, ClientId);
                var update = Builders<clients>.Update
                    .Set("cbuBalance", Amount)
                    .CurrentDate("updatedAt");
                var result = _db.GetCollection<clients>("clients").UpdateOneAsync(filter, update).Result;
                // insert entry for cbulogs
                cbuLogs log = new cbuLogs
                {
                    Id = ObjectId.GenerateNewId().ToString(),
                    type = "edit cbu balance",
                    slug = client.slug + '-' + global.slugify(8),
                    clientId = client.Id,
                    amount = Amount,
                    cbuBalance = Amount,
                    createdAt = now,
                    updatedAt = now
                };
                _db.GetCollection<cbuLogs>("cbuLogs").InsertOne(log);
            }
        }


        public void UpdateCbuBalanceBySlug(String ClientSlug, Double Amount) {
            var client = _db.GetCollection<clients>("clients").Find(c=>c.slug == ClientSlug).FirstOrDefault();
            var now = DateTime.Now;
            if(client!=null) {
                // update client.cbuBalance            
                var filter = Builders<clients>.Filter.Eq(c=> c.slug, ClientSlug);
                var update = Builders<clients>.Update
                    .Set("cbuBalance", Amount)
                    .CurrentDate("updatedAt");
                var result = _db.GetCollection<clients>("clients").UpdateOneAsync(filter, update).Result;
                // insert entry for cbulogs
                cbuLogs log = new cbuLogs
                {
                    Id = ObjectId.GenerateNewId().ToString(),
                    type = "edit cbu balance",
                    slug = client.slug + '-' + global.slugify(8),
                    clientId = client.Id,
                    amount = Amount,
                    cbuBalance = Amount,
                    createdAt = now,
                    updatedAt = now
                };
                _db.GetCollection<cbuLogs>("cbuLogs").InsertOne(log);
            }
        }

        public void UpdateLoanCycleBySlug(String ClientSlug, int Cycle) {
            var client = _db.GetCollection<clients>("clients").Find(c=>c.slug == ClientSlug).FirstOrDefault();
            var now = DateTime.Now;
            if(client!=null) {
                // update client.cbuBalance            
                var filter = Builders<clients>.Filter.Eq(c=> c.slug, ClientSlug);
                var update = Builders<clients>.Update
                    .Set("loanCycle", Cycle)
                    .CurrentDate("updatedAt");
                var result = _db.GetCollection<clients>("clients").UpdateOneAsync(filter, update).Result;
            }
        }
    }
}
