using System;
using kmbi_core_master.coreMaster.Models;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using kmbi_core_master.acctngModule.Models;

namespace kmbi_core_master.reports.Models
{

    public class repIncomeStatement {
        public DateTime asOf {get;set;}
        public Int32 currentYear {get;set;}
        public Int32 previousYear {get;set;}
        public repIncomeStatementRevenue revenue {get;set;}
        public repIncomeStatementExpenses expenses {get;set;}
        public repIncomeStatementLineItem netIncomeLossBeforeTax {get;set;}
        public repIncomeStatementLineItem incomeTaxExpense {get;set;}
        public repIncomeStatementLineItem netIncomeLoss {get;set;}
        public List<transactionDetailedIs> details {get;set;}
    }

    public class repIncomeStatementRevenue {
        public repIncomeStatementLineItem interestIncomeOnLoansReceivable {get;set;}
        public repIncomeStatementLineItem serviceIncome {get;set;}
        public repIncomeStatementLineItem interestOnDepositAndPlacements {get;set;}
        public repIncomeStatementLineItem forexGainOrLoss {get;set;}
        public repIncomeStatementLineItem otherIncome {get;set;}
        public repIncomeStatementLineItem totalGrossIncome {get;set;}
        public repIncomeStatementLineItem interestExpenseOnClientsCbu {get;set;}
        public repIncomeStatementLineItem totalFinancialCost {get;set;}
        public repIncomeStatementLineItem badDebtsExpense {get;set;}
        public repIncomeStatementLineItem netMargin {get;set;}
    }

    public class repIncomeStatementExpenses {
        public repIncomeStatementLineItem personnelCosts {get;set;}
        public repIncomeStatementLineItem outstation {get;set;}
        public repIncomeStatementLineItem fringeBenefit {get;set;}
        public repIncomeStatementLineItem retirement {get;set;}
        public repIncomeStatementLineItem outsideServices {get;set;}
        public repIncomeStatementLineItem professionalFees {get;set;}
        public repIncomeStatementLineItem meetingsTrainingsAndConferences {get;set;}
        public repIncomeStatementLineItem transformationExpense {get;set;}
        public repIncomeStatementLineItem litigation {get;set;}
        public repIncomeStatementLineItem supplies {get;set;}
        public repIncomeStatementLineItem utilities {get;set;}
        public repIncomeStatementLineItem communication {get;set;}
        public repIncomeStatementLineItem courierCharges {get;set;}
        public repIncomeStatementLineItem rental {get;set;}
        public repIncomeStatementLineItem insurance {get;set;}
        public repIncomeStatementLineItem transporationAndTravel {get;set;}
        public repIncomeStatementLineItem taxesAndLicenses {get;set;}
        public repIncomeStatementLineItem repairsAndMaintenance {get;set;}
        public repIncomeStatementLineItem depreciationAndAmortization {get;set;}
        public repIncomeStatementLineItem lossOnAssetWriteOff {get;set;}
        public repIncomeStatementLineItem others {get;set;}
        public repIncomeStatementLineItem totalExpenses {get;set;}
    }

    public class repIncomeStatementLineItem {
        public String name {get;set;}
        public String note {get;set;}
        public Decimal currentYear {get;set;}
        public Decimal previousYear {get;set;}
        public Decimal increaseDecrease {get;set;}
        public Decimal percentage {get;set;}
    }
}