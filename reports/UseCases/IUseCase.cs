namespace kmbi_core_master.reports.UseCases {
    public interface IUseCase<Request, Response> {
        Response Handle(Request request);
    }
}