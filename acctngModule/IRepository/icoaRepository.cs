﻿using kmbi_core_master.acctngModule.Models;
using kmbi_core_master.hrModule.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.acctngModule.Interface
{
    public interface icoaRepository
    {
        IEnumerable<coa> all();

        coa getSlug(String slug);

        void add(coa b);

        void update(String slug, coa b);

        void remove(String slug);

        coa checkParentCode(int code);

        coa checkSubCode(int code);
    }
}
