﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace kmbi_core_master.spm.Models
{

    public class ppi
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string slug { get; set; }
        public ppiClient client { get; set; }
        public ppiLoan loan { get; set; }
        public ppiQuestionaire questionaire { get; set; }
        public List<ppiQuestions> questions { get; set; }
        public DateTime createdAt { get; set; }
        public Int32 score { get; set; }
        public DateTime updatedAt { get; set; }
    }

    public class ppiClient
    {
        public string id { get; set; }
        public string slug { get; set; }
        public string name { get; set; }
    }

    public class ppiLoan
    {
        public string id { get; set; }
        public string slug { get; set; }
        public string name { get; set; }
    }

    public class ppiQuestionaire
    {
        public string id { get; set; }
        public string slug { get; set; }
        public string name { get; set; }
        public string version { get; set; }
    }

    public class ppiQuestions
    {
        public string question { get; set; }
        public string type { get; set; }
        public string columns { get; set; }
        public string series { get; set; }
        public ppiAnswers answer { get; set; }
    }

    public class ppiAnswers
    {
        public string choice { get; set; }
        public string value { get; set; }
    }

    public class syncPpi
    {
        
        public ObjectId Id { get; set; }
        public string slug { get; set; }
        public ppiClient client { get; set; }
        public ppiLoan loan { get; set; }
        public ppiQuestionaire questionaire { get; set; }
        public List<ppiQuestions> questions { get; set; }
        public DateTime createdAt { get; set; }
        public Int32 score { get; set; }
        public DateTime updatedAt { get; set; }
    }

    public class ppiScore
    {
        public double foodScore { get; set; }
        public double nationalScore { get; set; }
        public double onePerDayScore { get; set; }
        public double twoPerDayScore { get; set; }
    }

    public class ppiExtraction
    {
        //public string Id { get; set; }
        public string clientSlug { get; set; }
        public string firstName { get; set; }
        public string middleName { get; set; }
        public string lastName { get; set; }
        public Int32 loanCycle { get; set; }
        public string branch { get; set; }
        public string center { get; set; }
        public Int32 score { get; set; }
    }

    public class ClientProfiling
    {
        public string area { get; set; }
        public string branch { get; set; }
        public string name { get; set; }
        public int cycle { get; set; }
        public int age { get; set; }
        public string civilStatus { get; set; }
        public string business { get; set; }
        public string code { get; set; }
    }


}
