﻿using kmbi_core_master.hrModule.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.hrModule.Interface
{
    public interface isalaryUpdateRepository
    {
        List<salaryUpdate> getAll();
        salaryUpdate getBySlug(string slug);
        List<salaryUpdate> getByApproved(int approved);
        void create(salaryUpdate salaryUpdate);
        void update(string slug, salaryUpdate salaryUpdate);
        void delete(string slug);
        List<salaryUpdate> search(String name);
    }
}
