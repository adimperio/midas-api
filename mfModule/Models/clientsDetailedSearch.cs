﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
namespace kmbi_core_master.coreMaster.Models
{
    public class clientsDetailedSearch
    {
        public String id { get; set; }
        public String slug { get; set; }
        public clientFullname name { get; set; }
        public DateTime? birthdate { get; set; }
        public Double cbuAmount { get; set; }
        public Double loanAmount { get; set; }
        public String classification { get; set; }
        public Int32 loanCycleNo { get; set; }
    }

    public class repClientDetailedSearch
    {
        public String id { get; set; }
        public string branchName { get; set; }
        public String slug { get; set; }
        public clientFullname name { get; set; }
        public DateTime? birthdate { get; set; }
        public Double cbuAmount { get; set; }
        public Double loanAmount { get; set; }
        public String classification { get; set; }
        public Int32 loanCycleNo { get; set; }
        public string field { get; set; }
        public double from { get; set; }
        public double to { get; set; }
    }

    public class repClientsDet
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String id { get; set; }
        public string slug { get; set; }
        public clientFullname name { get; set; }
        public DateTime? birthdate { get; set; }
        public Double cbuBalance { get; set; }
        public Double loanAmount { get; set; }
        public String classification { get; set; }
        public Int32 loanCycle { get; set; }
        public reCbranch branch { get; set; }
    }
    public class reCbranch
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String id { get; set; }
        public String name { get; set; }
    }
}