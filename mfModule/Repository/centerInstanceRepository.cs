﻿using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Models;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace kmbi_core_master.coreMaster.Repository
{
    public class centerInstanceRepository : dbCon, icenterInstanceRepository
    {
        public centerInstanceRepository(IOptions<dbSettings> settings) : base(settings)
        {
        }

        public void add(centerInstance b)
        {

            b.createdAt = DateTime.Now;
            b.updatedAt = DateTime.Now;
            
            _db.GetCollection<centerInstance>("centerInstance").InsertOneAsync(b);
        }

        public IEnumerable<centerInstance> allCenters()
        {
            var c = _db.GetCollection<centerInstance>("centerInstance").Find(new BsonDocument()).ToListAsync();
            return c.Result;
        }

        public centerInstance getSlug(String slug)
        {
            var query = Builders<centerInstance>.Filter.Eq(e => e.slug, slug);
            var c = _db.GetCollection<centerInstance>("centerInstance").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }

        public IEnumerable<centerInstance> getOfficer(String officerId)
        {
            var query = Builders<centerInstance>.Filter.Eq(e => e.officerId, officerId);
            var c = _db.GetCollection<centerInstance>("centerInstance").Find(query).ToListAsync();

            return c.Result;
        }

        public IEnumerable<centerInstance> getOfficerSlug(String officerSlug)
        {
            var query = Builders<centerInstance>.Filter.Eq(e => e.officerSlug, officerSlug);
            var c = _db.GetCollection<centerInstance>("centerInstance").Find(query).ToListAsync();

            return c.Result;
        }

        public IEnumerable<centerInstance> searchCode(String code, string branchId)
        {
            var query1 = Builders<centerInstance>.Filter.Eq(e => e.branchId, branchId);
            var query2 = Builders<centerInstance>.Filter.Regex("code", new BsonRegularExpression(code, options: "i"));
            var query = Builders<centerInstance>.Filter.And(query1, query2);

            var fields = Builders<centerInstance>.Projection
                .Include(u => u.Id)
                .Include(u => u.referenceId)
                .Include(u => u.centerId)
                .Include(u => u.branchId)
                .Include(u => u.branchSlug)
                .Include(u => u.unitId)
                .Include(u => u.unitSlug)
                .Include(u => u.officerId)
                .Include(u => u.officerSlug)
                .Include(u => u.address)
                .Include(u => u.code)
                .Include(u => u.slug)
                .Include(u => u.loanCycle)
                .Include(u => u.status);

            var c = _db.GetCollection<centerInstance>("centerInstance").Find(query).Project<centerInstance>(fields).ToList().AsQueryable();
            return c;
        }

        public IEnumerable<centerInstance> getByBranch(string branchSlug)
        {
            var query = Builders<centerInstance>.Filter.Eq(e => e.branchSlug, branchSlug);

            var fields = Builders<centerInstance>.Projection
                .Include(u => u.Id)
                .Include(u => u.referenceId)
                .Include(u => u.centerId)
                .Include(u => u.branchId)
                .Include(u => u.branchSlug)
                .Include(u => u.unitId)
                .Include(u => u.unitSlug)
                .Include(u => u.officerId)
                .Include(u => u.officerSlug)
                .Include(u => u.address)
                .Include(u => u.code)
                .Include(u => u.slug)
                .Include(u => u.loanCycle)
                .Include(u => u.status);

            var sort = Builders<centerInstance>.Sort.Ascending(e => e.code);
            var c = _db.GetCollection<centerInstance>("centerInstance").Find(query).Project<centerInstance>(fields).Sort(sort).ToList().AsQueryable();
            return c;
        }

        public void update(String slug, centerInstance p)
        {
            p.slug = slug;
            var query = Builders<centerInstance>.Filter.Eq(e => e.slug, slug);
            var update = Builders<centerInstance>.Update
                .Set("address", p.address)
                .Set("code", p.code)
                .Set("loanCycle", p.loanCycle)
                .Set("status", p.status)
                .CurrentDate("updatedAt");

            var branch = _db.GetCollection<centerInstance>("centerInstance").UpdateOneAsync(query, update);

        }


        public void remove(String slug)
        {
            var query = Builders<centerInstance>.Filter.Eq(e => e.slug, slug);
            var result = _db.GetCollection<centerInstance>("centerInstance").DeleteOneAsync(query);
        }

        public IEnumerable<centerInstance> getByPOEmailAddress(string email)
        {
            // //get branchSlug
            // var qUsers = Builders<users>.Filter.Eq(e => e.email, email);
            // var branchSlug = _db.GetCollection<users>("users").Find(qUsers).FirstOrDefault().employment.branch;

            // //get unitID
            // var unitName = email.Substring(5, 1).ToUpper();
            // var qUnit1 = Builders<branch>.Filter.Eq(e => e.slug, branchSlug);
            // var units = _db.GetCollection<branch>("branch").Find(qUnit1).FirstOrDefault();
            
            // var unitId = "";

            // foreach (var unit in units.units)
            // {
            //     if (unit.name == unitName)
            //     {
            //         unitId = unit.Id;
            //     }
            // }

            //get officers
            // var designation = email.Substring(0, 2).ToUpper() + email.Substring(3, 1);
            // var qOff1 = Builders<officers>.Filter.Eq(e => e.designation, designation);
            // var qOff2 = Builders<officers>.Filter.Eq(e => e.unitId, unitId);
            // var qOff = Builders<officers>.Filter.And(qOff1, qOff2);
            // var officerSlug = _db.GetCollection<officers>("officers").Find(qOff).FirstOrDefault().slug;

            // //get centerInstance
            // var cInstance = Builders<centerInstance>.Filter.Eq(e => e.officerSlug, officerSlug);
            // var cInstanceResult = _db.GetCollection<centerInstance>("centerInstance").Find(cInstance).ToListAsync();

            // return cInstanceResult.Result;

            // rdc 01/14/2019

            List<centerInstance> centerInstanceList = new List<centerInstance>();

            var user = _db.GetCollection<users>("users").Find(x=>x.email == email).FirstOrDefault();
            var branch = _db.GetCollection<branch>("branch").Find(x=>x.slug == user.employment.branch).FirstOrDefault();
            

            var proceed = !(user==null || branch==null);

            if(proceed) {
                var designation = email.Split(".")[0].ToLower();
           
                switch(designation) {
                    case "po": {
                        
                        var unitNo = email.Split(".")[1].ToLower();
                        var unitLett = email.Split(".")[2].ToLower();
                        var units = branch.units.Where(x=>x.name.ToLower() == unitLett.ToLower()).Select(x=>x.Id);
                        var officer = _db.GetCollection<officers>("officers").Find(x =>
                            x.designation == "PO" + unitNo.ToString() && units.Contains(x.unitId)
                        ).FirstOrDefault();
                        var officerSlug = "";
                        if(officer!=null) {
                            officerSlug = officer.slug;
                            centerInstanceList.AddRange(_db.GetCollection<centerInstance>("centerInstance").Find(x=>x.officerSlug == officerSlug).ToList());
                        }                        
                        break;
                    }
                    case "puh": {

                        var unitLetter = email.Split(".")[1].ToLower();
                        var units = branch.units.Where(x=>x.name.ToLower() == unitLetter).Select(x=>x.Id);

                        var officersSlug = _db.GetCollection<officers>("officers").Find(x =>
                            units.Contains(x.unitId)
                        ).ToList().Select(x=>x.slug);
                        
                        centerInstanceList.AddRange(_db.GetCollection<centerInstance>("centerInstance").Find(x=>officersSlug.Contains(x.officerSlug)).ToList());

                        break;
                    }
                    default: {
                        var units = branch.units.Select(x=>x.Id);

                        var officersSlug = _db.GetCollection<officers>("officers").Find(x =>
                            units.Contains(x.unitId)
                        ).ToList().Select(x=>x.slug);
                        
                        centerInstanceList.AddRange(_db.GetCollection<centerInstance>("centerInstance").Find(x=>officersSlug.Contains(x.officerSlug)).ToList());

                        break;
                    }
                }
            }


            return centerInstanceList;

            
        }
    }
}
