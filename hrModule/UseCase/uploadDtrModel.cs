using System;

namespace kmbi_core_master.hrModule.UseCase {
    public class uploadDtrModel {
        public String Date {get;set;}
        public String TimeIn {get;set;}
        public String TimeOut {get;set;}
        public String Remarks {get;set;}
    }
}