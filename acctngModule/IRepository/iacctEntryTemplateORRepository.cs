﻿using kmbi_core_master.acctngModule.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.acctngModule.Interface
{
    public interface iacctEntryTemplateORRepository
    {
        IEnumerable<acctEntryTemplateOR> all();

        acctEntryTemplateOR getSlug(String slug);

        void add(acctEntryTemplateOR b);

        void update(String slug, acctEntryTemplateOR b);

        void remove(String slug);
    }
}
