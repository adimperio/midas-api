﻿using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Models;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace kmbi_core_master.coreMaster.Repository
{
    public class officersRepository : dbCon, iofficersRepository
    {
        public officersRepository(IOptions<dbSettings> settings) : base(settings)
        {
        }

        public void addOfficer(officers b)
        {

            b.createdAt = DateTime.Now;
            b.updatedAt = DateTime.Now;
            for (int i = 0; i <= b.centers.Count - 1; i++)
            {
                b.centers[i].Id = ObjectId.GenerateNewId().ToString();
                b.centers[i].createdAt = DateTime.Now;
                b.centers[i].updatedAt = DateTime.Now;

            }
            _db.GetCollection<officers>("officers").InsertOneAsync(b);
        }

        public void addCenter(centers u, string officerSlug)
        {
            var query = Builders<officers>.Filter.Eq(e => e.slug, officerSlug);
            var update = Builders<officers>.Update.Push(e => e.centers, u);
            u.Id = ObjectId.GenerateNewId().ToString();
            u.createdAt = DateTime.Now;
            u.updatedAt = DateTime.Now;
            _db.GetCollection<officers>("officers").FindOneAndUpdateAsync(query, update);
        }



        public IEnumerable<officersCenterList> allOfficers()
        {

            var coll = _db.GetCollection<officersCenterList>("officers");
            var officers = coll.Aggregate()
                .Lookup("centerInstance", "slug", "officerSlug", "centerInstances")
                .Project<officersCenterList>(new BsonDocument
                        {
                            {"id", 1},
                            {"unitId", 1},
                            {"name", 1},
                            {"slug", 1},
                            {"designation", 1},
                            {"centers", 1},
                            {"centerInstances", 1}
                        })
                .ToListAsync();

            return officers.Result;


        }

        public centerList getCenterSlug(string centerSlug)
        {
            var query = Builders<centerList>.Filter.ElemMatch(x => x.centers, x => x.slug == centerSlug);
            var fields = Builders<centerList>.Projection
                .Include(u => u.slug)
                .ElemMatch(x => x.centers, x => x.slug == centerSlug);

            var center = _db.GetCollection<centerList>("officers").Find(query).Project<centerList>(fields).ToListAsync();
            return center.Result.FirstOrDefault();
        }

        public officersCenterList getOfficerSlug(String slug)
        {
            //var query = Builders<officers>.Filter.Eq(e => e.slug, slug);
            //var officer = _db.GetCollection<officers>("officers").Find(query).ToListAsync();

            var coll = _db.GetCollection<officersCenterList>("officers");
            var coll1 = _db.GetCollection<centerInstance>("centerInstance");

            //var Id = global.GetMemberName((officersCenterList c) => c.Id);
            //var officerId = global.GetMemberName((centerIns c) => c.officerId);

            var officers = coll.Aggregate()
                .Match(e => e.slug == slug)
                .Lookup("centerInstance", "slug", "officerSlug", "centerInstances")
                .Project<officersCenterList>(new BsonDocument
                        {
                            {"unitId", 1},
                            {"name", 1},
                            {"slug", 1},
                            {"designation", 1},
                            {"centers", 1},
                            {"centerInstances", 1}
                        })
                .ToListAsync();

            //return officers.Result;

            return officers.Result.FirstOrDefault();
        }


        public IEnumerable<officersCenterList> getOfficerunitId(String unitId)
        {
            //var query = Builders<officers>.Filter.Eq(e => e.unitId, unitId);
            //var officer = _db.GetCollection<officers>("officers").Find(query).ToListAsync();

            //return officer.Result;

            var coll = _db.GetCollection<officersCenterList>("officers");
            var coll1 = _db.GetCollection<centerInstance>("centerInstance");

            //var Id = global.GetMemberName((officersCenterList c) => c.Id);
            //var officerId = global.GetMemberName((centerIns c) => c.officerId);

            var officers = coll.Aggregate()
                .Match(e => e.unitId == unitId)
                .Lookup("centerInstance", "slug", "officerSlug", "centerInstances")
                .Project<officersCenterList>(new BsonDocument
                        {
                            {"id", 1},
                            {"unitId", 1},
                            {"name", 1},
                            {"slug", 1},
                            {"designation", 1},
                            {"centers", 1},
                            {"centerInstances", 1}
                        })
                .ToListAsync();

            return officers.Result;
        }

        public centerList getCenterCode(string centerCode)
        {
            var query = Builders<centerList>.Filter.ElemMatch(x => x.centers, x => x.code == centerCode);
            var fields = Builders<centerList>.Projection
                .Include(u => u.slug)
                .ElemMatch(x => x.centers, x => x.slug == centerCode);

            var center = _db.GetCollection<centerList>("officers").Find(query).Project<centerList>(fields).ToListAsync();
            return center.Result.FirstOrDefault();
        }


        public void updateOfficer(String slug, officers p)
        {
            p.slug = slug;
            var query = Builders<officers>.Filter.Eq(e => e.slug, slug);
            var update = Builders<officers>.Update
                .Set("name", p.name)
                .Set("designation", p.designation)
                .CurrentDate("updatedAt");

            var branch = _db.GetCollection<officers>("officers").UpdateOneAsync(query, update);

        }

        public void updateCenter(string officerSlug, string centerSlug, centers u)
        {

            var query = Builders<officers>.Filter.And(Builders<officers>.Filter.Eq(x => x.slug, officerSlug),
            Builders<officers>.Filter.ElemMatch(x => x.centers, x => x.slug == centerSlug));

            var update = Builders<officers>.Update
                .Set("centers.$.code", u.code)
                .Set("centers.$.address", u.address)
                .CurrentDate("updatedAt");

            var result = _db.GetCollection<officers>("officers").UpdateOneAsync(query, update);
        }


        public void removeOfficer(String slug)
        {
            var query = Builders<officers>.Filter.Eq(e => e.slug, slug);
            var result = _db.GetCollection<officers>("officers").DeleteOneAsync(query);
        }

        public void removeCenter(String officerSlug, string centerSlug)
        {

            var pull = Builders<officers>.Update.PullFilter(x => x.centers, a => a.slug == centerSlug);

            var filter1 = Builders<officers>.Filter.And(Builders<officers>.Filter.Eq(a => a.slug, officerSlug),
                          Builders<officers>.Filter.ElemMatch(q => q.centers, t => t.slug == centerSlug));

            var result = _db.GetCollection<officers>("officers").UpdateOneAsync(filter1, pull);
        }

        public IEnumerable<officers> searchOfficer(String name)
        {

            var query = Builders<officers>.Filter.Regex("name", new BsonRegularExpression(name, options: "i"));
            var officer = _db.GetCollection<officers>("officers").Find(query).ToListAsync();

            return officer.Result;
        }


        public IEnumerable<centerList> searchCenter(String code)
        {
            var filter = Builders<centerList>.Filter.ElemMatch(x => x.centers, x => Regex.IsMatch(x.code, code, RegexOptions.IgnoreCase));

            var fields = Builders<centerList>.Projection
                .Include(u => u.slug)
                .ElemMatch(x => x.centers, x => Regex.IsMatch(x.code, code, RegexOptions.IgnoreCase));

            var center = _db.GetCollection<centerList>("officers").Find(filter).Project<centerList>(fields).ToListAsync();
            return center.Result;
        }

        public officers checkOfficerSlug(String slug)
        {
            var query = Builders<officers>.Filter.Eq(e => e.slug, slug);
            var officer = _db.GetCollection<officers>("officers").Find(query).ToListAsync();

            return officer.Result.FirstOrDefault();
        }

        public IEnumerable<officers> getByUnit(string id)
        {
            var query = Builders<officers>.Filter.Eq(e => e.unitId, id);
            var officer = _db.GetCollection<officers>("officers").Find(query).ToListAsync();

            return officer.Result;
        }


    }
}
