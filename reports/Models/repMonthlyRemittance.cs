using System;
using System.Collections.Generic;

namespace kmbi_core_master.reports.Models
{
    public class repMonthlyRemittance
    {
        public Int32 month {get;set;}
        public Int32 year {get;set;}
        public List<repMonthlyRemittanceDetail> details {get;set;}
    }

    public class repMonthlyRemittanceDetail
    {
        public String fullname {get;set;}
        public String taxCode {get;set;}
        public Double monthlyRate {get;set;}
        public Double grossCompensation {get;set;}
        public Double thirteenthMonthAccrual {get;set;}
        public Double witholdingTax {get;set;}
        public Double sssEe {get;set;}
        public Double sssEr {get;set;}
        public Double ec {get;set;}
        public Double sssTotal {get;set;}
        public Double phicEe {get;set;}
        public Double phicEr {get;set;}
        public Double phicTotal {get;set;}
        public Double hdmfEe {get;set;}
        public Double hdmfEr {get;set;}
        public Double hdmfTotal {get;set;}
        public Double hdmfHousingLoan {get;set;}
        public Double hdmfMpl {get;set;}
        public Double hdmfCalamity {get;set;}
        public Double sssLoan {get;set;}
        public Double mp2 {get;set;}
    }



}