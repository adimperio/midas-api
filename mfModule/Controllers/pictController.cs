﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Cors;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace kmbi_core_master.mfModule.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/picts")]
    public class pictController : Controller
    {

        private IHostingEnvironment hostingEnv;

        public pictController(IHostingEnvironment env)
        {
            this.hostingEnv = env;
        }

        // POST api/values
        [HttpPost]
        public  IActionResult upload(IFormFile pict)
        {
            //Upload hoto
            string extension = Path.GetExtension(pict.FileName).ToLower();
            var filename = ContentDispositionHeaderValue
                                 .Parse(pict.ContentDisposition)
                                 .FileName.Trim();

            //copy csv file to server
            var uploads = Path.Combine(hostingEnv.ContentRootPath, @"uploads\clientPicts");
            var newFilename = "hello" + extension;
            var newPath = uploads + @"\" + newFilename;

            if (pict.Length > 0)
            {
                using (var fileStream = new FileStream(Path.Combine(uploads, newFilename), FileMode.Create, FileAccess.ReadWrite))
                {
                     pict.CopyToAsync(fileStream);
                }
            }

            return Ok(new {
                path = newPath
            });
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        
    }
}
