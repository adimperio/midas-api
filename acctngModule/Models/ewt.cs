﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace kmbi_core_master.acctngModule.Models
{
    public class ewt    
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }
       
        [BsonElement("slug")]
        public String slug { get; set; }

        [BsonElement("name")]
        public String name { get; set; }

        [BsonElement("description")]
        public String description { get; set; }

        [BsonElement("code")]
        public String code { get; set; }

        [BsonElement("taxRate")]
        public double taxRate { get; set; }

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }
    }
   
}
