﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace kmbi_core_master.acctngModule.Models
{
    public class acctngGl
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("slug")]
        public String slug { get; set; }

        [BsonElement("refNumber")]
        public string refNumber { get; set; }

        [BsonElement("particulars")]
        public string particulars { get; set; }

        [BsonElement("crb")]
        public crb crb { get; set; }

        [BsonElement("cdb")]
        public cdb cdb { get; set; }

        [BsonElement("gjb")]
        public gjb gjb { get; set; }
       
        [BsonElement("branch")]
        public glBranch branch { get; set; }

        [BsonElement("accountEntity")]
        public glAccountEntity accountEntity { get; set; }

        [BsonElement("amount")]
        public glAmount amount { get; set; }

        [BsonElement("isCancelled")]
        public bool isCancelled { get; set; }

        [BsonElement("isClosed")]
        public bool isClosed { get; set; }

        [BsonElement("reason")]
        public string reason { get; set; }

        [BsonElement("status")]
        public string status { get; set; }

        [BsonElement("timestamps")]
        public glTimestamps timestamps { get; set; }
    }

    public class crb
    {
        [BsonElement("credit")]
        public crbCredit[] credit { get; set; }

        [BsonElement("debit")]
        public crbDebit[] debit { get; set; }

    }

    public class crbCredit
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("code")]
        public int code { get; set; }

        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("amount")]
        public double amount { get; set; }
    }

    public class crbDebit
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("code")]
        public int code { get; set; }

        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("amount")]
        public double amount { get; set; }
    }

    public class cdb
    {
        [BsonElement("credit")]
        public cdbCredit[] credit { get; set; }

        [BsonElement("debit")]
        public cdbDebit[] debit { get; set; }

    }

    public class cdbCredit
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("code")]
        public int code { get; set; }

        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("amount")]
        public double amount { get; set; }

        [BsonElement("ewt")]
        public string ewt { get; set; }

        [BsonElement("rate")]
        public int rate { get; set; }
    }

    public class cdbDebit
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("code")]
        public int code { get; set; }

        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("amount")]
        public double amount { get; set; }

        [BsonElement("ewt")]
        public string ewt { get; set; }

        [BsonElement("rate")]
        public int rate { get; set; }
    }

    public class gjb
    {
        [BsonElement("credit")]
        public gjbCredit[] credit { get; set; }

        [BsonElement("debit")]
        public gjbDebit[] debit { get; set; }

    }

    public class gjbCredit
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("code")]
        public int code { get; set; }

        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("particular")]
        public string particular { get; set; }

        [BsonElement("amount")]
        public double amount { get; set; }
    }

    public class gjbDebit
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("code")]
        public int code { get; set; }

        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("particular")]
        public string particular { get; set; }

        [BsonElement("amount")]
        public double amount { get; set; }
    }

    public class glBranch
    {
        [BsonId]
        [BsonElement("id")]
        public string id { get; set; }

        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

    }

    public class glAccountEntity
    {
        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("tin")]
        public string tin { get; set; }

        [BsonElement("address")]
        public string address { get; set; }

    }

    public class glAmount
    {
        [BsonElement("byNumbers")]
        public double byNumbers { get; set; }

        [BsonElement("byWords")]
        public string byWords { get; set; }
    }

    public class glTimestamps
    {
        [BsonElement("issuedAt")]
        public DateTime issuedAt { get; set; }

        [BsonElement("issuedBy")]
        public string issuedBy { get; set; }

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("createdBy")]
        public string createdBy { get; set; }

        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }

        [BsonElement("updatedBy")]
        public string updatedBy { get; set; }
    }

}
