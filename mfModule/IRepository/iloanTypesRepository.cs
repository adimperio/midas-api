﻿using kmbi_core_master.coreMaster.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.coreMaster.IRepository
{
    public interface iloanTypesRepository
    {
        IEnumerable<loanTypes> allLoanTypes();

        loanTypes Get(String id);

        loanTypes getSlug(String slug);
               
        void Add(loanTypes c);

        void Update(String slug, loanTypes c);
              
        void Remove(String slug);
        
        loanTypes checkSlug(String slug);

        IEnumerable<loanTypes> searchLoanTypes(string name);

    }
}
