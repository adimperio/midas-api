﻿using kmbi_core_master.coreMaster.Controllers;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Models;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using kmbi_core_master.helpers;

namespace kmbi_core_master.coreMaster.Repository
{
    public class logsRepository : dbCon, ilogsRepository
    {
        public logsRepository(IOptions<dbSettings> settings) : base(settings)
        {
        }

        public void log(activity logs)
        {
            //activity act = new activity()
            //{
            //    Id = ObjectId.GenerateNewId().ToString(),
            //    userId = userId,
            //    fullname = fullname,
            //    branchId = branchId,
            //    log = log,
            //    level = 1,
            //    link = "",
            //    createdAt = DateTime.Now
            //};
            logs.Id = ObjectId.GenerateNewId().ToString();
            logs.createdAt = DateTime.Now;
            _db.GetCollection<activity>("activity").InsertOneAsync(logs).ToString();

        }

        //public List<oldAmort> getAmort()
        //{
        //    var a = _db.GetCollection<oldAmort>("amortization").Find(new BsonDocument()).ToListAsync();
        //    return a.Result;
        //}


        //public List<amort> getNewAmort()
        //{
        //    var a = _db.GetCollection<amort>("amortization").Find(new BsonDocument()).ToListAsync().Result;

        //    List<amort> s = new List<amort>();
        //    List<sched> z = new List<sched>();
        //    foreach (var i in a)
        //    {


        //        //z.Add(new sched
        //        //{
        //        //    weekNo = i.weekNo,
        //        //    pricipal = i.principal,
        //        //    interest = i.interest
        //        //});

        //        s.Add(new amort
        //        {
        //            Id = new ObjectId().ToString(),
        //            slug = global.slugify(8),
        //            loanType = "Group Loan",
        //            amount = i.loanAmount,
        //            term = i.repaymentNo,
        //            schedule = z

        //            //schedule = new sched
        //            //{
        //            //    weekNo = 
        //            //}
        //        });

        //    }

        //    return s;
        //}

        //public List<amort> updateSlug()
        //{
        //    var list = _db.GetCollection<amort>("amortization_copy").Find(new BsonDocument()).ToListAsync().Result;

        //    List<amort> amort = new List<amort>();
        //    for (int i = 0; i <= list.Count - 1; i++)
        //    {
        //        //list[i].slug = global.slugify(8);
        //        amort.Add(new amort {
        //            //Id = list[i].Id,
        //            amount = list[i].amount,
        //            schedule = list[i].schedule,
        //            slug = global.slugify(16),
        //            loanType = list[i].loanType,
        //            term = list[i].term
        //        });

        //        //var update = Builders<amort>.Update
        //        //.Set("slug", global.slugify(8))
        //        //.CurrentDate("updatedAt");

        //        //var d = _db.GetCollection<questionaires>("questionaires").UpdateOneAsync(qryGetSlug, update);

        //    }

            
        //    return amort;
        //    //var update = Builders<amort>.Update
        //    //    .Set("slug", amort)
        //    //    .CurrentDate("updatedAt");

        //    //var d = _db.GetCollection<questionaires>("questionaires").UpdateOneAsync(qryGetSlug, update);
        //}
    }
}
