﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Filters;
using kmbi_core_master.hrModule.Interface;
using kmbi_core_master.hrModule.Models;
using Microsoft.AspNetCore.Cors;
using System;
namespace kmbi_core_master.coreMaster.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/dtrManual")]
    [usersAuth]
    public class dtrManualController : Controller
    {
        private readonly idtrManualRepository _Repository;

        public dtrManualController(idtrManualRepository Repository)
        {
            this._Repository = Repository;
        }

        private void logger(String message)
        {
            //global.log(message, 1, "link here");
        }  
 
        [HttpGetAttribute()]
        public IActionResult getDtrManualPerEmployeeCodePerDate([FromQueryAttribute]String employeeCode, [FromQueryAttribute]DateTime date)
        {
            dtrManual dtrManual = _Repository.getDtrManualPerEmployeeCodePerDate(employeeCode,date);
            if (dtrManual!=null) {
                return Ok(dtrManual);
            } else {
                return Ok(new {});
            };
        }

        [HttpGetAttribute("list")]
        public IActionResult getDtrManualPerEmployeeCodePerDateRange([FromQueryAttribute]String employeeCode, [FromQueryAttribute]DateTime from, [FromQueryAttribute]DateTime to)
        {
            List<dtrManual> dtrManuals = _Repository.getDtrManualPerEmployeeCodePerDateRange(employeeCode,from,to);
            if (dtrManuals!=null) {
                return Ok(dtrManuals);
            } else {
                return Ok(new List<dtrManual>());
            };
        }

        [HttpPostAttribute("insert")]
        public IActionResult createDtrManual([FromBodyAttribute]dtrManual dtrManual)
        {
            Boolean res = _Repository.createDtrManual(dtrManual);

            if (res) {
                logger("Insert Dtr Manual " + dtrManual.employeeCode);
                return Ok(new {type="success", message="DTR updated succesfully"});
            } else {
                return Ok(new {type="failed", message="Dtr update failed"});
            };
        }

        [HttpPutAttribute("update/{slug}")]
        public IActionResult updateDtrManual(String slug, [FromBodyAttribute]dtrManual dtrManual)
        {
            Boolean res = _Repository.updateDtrManual(slug,dtrManual);

            if (res) {
                logger("Update Dtr Manual " + dtrManual.employeeCode);
               return Ok(new {type="success", message="DTR updated succesfully"});
            } else {
                return Ok(new {type="failed", message="Dtr update failed"});
            };
        }

        [HttpDeleteAttribute("delete/{slug}")]
        public IActionResult deleteDtrManual(String slug)
        {
            Boolean res = _Repository.deleteDtrManual(slug);

            if (res) {
                logger("Update Dtr Manual " + slug);
               return Ok(new {type="success", message="DTR updated succesfully"});
            } else {
                return Ok(new {type="failed", message="Dtr update failed"});
            };
        }

        [HttpPostAttribute("replace")]
        public IActionResult replaceDtrManualPerEmployeeCodePerDate([FromQueryAttribute]String employeeCode, [FromQueryAttribute]DateTime date, [FromBodyAttribute]dtrManual dtrManual)
        {
            Boolean res = _Repository.replaceDtrManualPerEmployeeCodePerDate(employeeCode, date, dtrManual);

            if (res) {
                logger("Update Dtr Manual " + employeeCode);
               return Ok(new {type="success", message="DTR updated succesfully"});
            } else {
                return Ok(new {type="failed", message="Dtr update failed"});
            };
        }
    }
}
