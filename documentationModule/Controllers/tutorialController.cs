using System;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Microsoft.Extensions.Options;
using System.Linq;

using kmbi_core_master.documentationModule.Models;
using kmbi_core_master.coreMaster.Filters;
using kmbi_core_master.coreMaster.Models;

using kmbi_core_master.documentationModule.Service;
using Microsoft.AspNetCore.Cors;
using kmbi_core_master.documentationModule.iRepositories;

namespace kmbi_core_master.documentationModule.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/tutorial")]
    [errorHandler]
    public class tutorialController : Controller
    {
        private readonly itutorialRepository _tutorialRepo;
        public tutorialController(itutorialRepository tutorialRepo) 
        {
            _tutorialRepo = tutorialRepo;
        }

        private void logger(String message)
        {
            //global.log(message, 1, "link here");
        }  

        [HttpGet]
        public IActionResult Get()
        {
            var list = _tutorialRepo.GetTutorials();
            
            if(list != null)
            {
                return Ok(from y in list select new {_id = y.Id, name = y.name, slug = y.slug, order = y.order});
            }
            else
            {
                return Ok(new List<tutorial>());
            }
            
        }

        [HttpGet("{slug}")]
        public IActionResult Get(String slug)
        {
            var tut = _tutorialRepo.GetTutorialBySlug(slug);
            
            if (tut != null)
            {
                return Ok(tut);
            }
            else
            {
                return Ok(new {});
            }
        }

        [HttpGet("{tutorialSlug}/{topicSlug}")]
        public IActionResult Get(String tutorialSlug, String topicSlug)
        {
            var top = _tutorialRepo.GetTutorialSpecificTopic(tutorialSlug, topicSlug);
            
            if(top != null)
            {
                return Ok(top);
            }
            else
            {
                return Ok(new {});
            }
            
        }

        [HttpPost()]
        public IActionResult Post([FromBody]tutorial tutorial)
        {
            var res = _tutorialRepo.InsertTutorial(tutorial);
            if(res)
            {
                logger("Insert tutorial " + tutorial.name);
                return Ok(new {
                    type="success",
                    message= tutorial.name + " created successfully.",
                    slug = tutorial.slug
                });
            }
            else
            {
                return BadRequest(new {
                    type="error",
                    message= "Tutorial create failed."
                });
            }
        }

        [HttpPost("{slug}")]
        public IActionResult Post(String slug, [FromBody]topic topic)
        {
            var res = _tutorialRepo.InsertTopic(slug,topic);

            if(res)
            {
                logger("Insert topic " + topic.title);
                return Ok(new {
                    type="success",
                    message= topic.title + " created successfully.",
                    slug= topic.slug
                });
            }
            else
            {
                return BadRequest(new {
                    type="error",
                    message= "Topic create failed."
                });
            }
        }

        [HttpPut("{slug}")]
        public IActionResult Put(String slug, [FromBody]tutorial tutorial)
        {
            var res = _tutorialRepo.UpdateTutorial(slug, tutorial);
           
            if (res)
            {
                logger("Modified tutorial " + tutorial.name);
                return Ok(new {
                    type= "success",
                    message = tutorial.name + " updated successfully."
                });
            }
            else
            {
                return BadRequest(new {
                    type="error",
                    message= "Tutorial update failed." 
                });
            }
        }

        [HttpPut("{tutorialSlug}/{topicSlug}")]
        public IActionResult Put(String tutorialSlug, String topicSlug, [FromBody]topic topic)
        {
            var res = _tutorialRepo.UpdateTopic(tutorialSlug, topicSlug, topic);

            if(res)
            {
                logger("Modified topic " + topic.title);
                return Ok(new {
                    type="success",
                    message= topic.title + " updated successfully."
                });
            }
            else
            {
                return BadRequest(new {
                    type="error",
                    message= "Topic update failed."
                });
            }
        }

        [HttpDelete("{slug}")]
        public IActionResult Delete(String slug)
        {
            var tut = _tutorialRepo.GetTutorialBySlug(slug);
            var res = _tutorialRepo.DeleteTutorial(slug);

            if(res)
            {
                logger("Delete tutorial " + tut.name);
                return Ok(new {
                    type="success",
                    message= tut.name + " deleted successfully."
                });
            }
            else
            {
                 return BadRequest(new {
                    type="error",
                    message= "Tutorial delete failed."
                });
            }
        }

        [HttpDelete("{tutorialSlug}/{topicSlug}")]
        public IActionResult Delete(String tutorialSlug, String topicSlug)
        {
            topic top = _tutorialRepo.GetTutorialSpecificTopic(tutorialSlug, topicSlug);
            var res = _tutorialRepo.DeleteTopic(tutorialSlug,topicSlug);

            if(res)
            {
                logger("Delete topic " + top.title);
                return Ok(new {
                    type="success",
                    message= top.title + " deleted successfully."
                });
            }
            else
            {
                return BadRequest(new {
                    type="error",
                    message= "Topic delete failed."
                });
            }
        }

        
    }
}