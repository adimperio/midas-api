using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace kmbi_core_master.documentationModule.Models
{
    public class holiday
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }
        [BsonElement("name")]
        public String name {get;set;}
        [BsonElement("slug")]
        public String slug {get;set;}
        [BsonElementAttribute("date")]
        public DateTime date {get;set;}
        [BsonElement("type")]
        public String type {get;set;}
        [BsonElement("createdAt")]
        public DateTime createdAt {get;set;}
        [BsonElement("updatedAt")]
        public DateTime updatedAt {get;set;}
    }
}