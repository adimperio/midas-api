using System;
using System.Collections.Generic;
using kmbi_core_master.coreMaster.Models;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace kmbi_core_master.reports.Models
{
    public class possibleRelativeProjected
    {
        public String slug {get;set;}
        public String employeeCode {get;set;}
        public String fullname {get;set;}
        public String picture {get;set;}
        public employment employment {get;set;}
        public List<familyMember> family {get;set;}
        public mothersMaidenName mothersMaidenName {get;set;}
    }

    public class possibleRelativeUnwinded
    {
        public String slug {get;set;}
        public String employeeCode {get;set;}
        public String fullname {get;set;}
        public String picture {get;set;}
        public employment employment {get;set;}
        public familyMember family {get;set;}
        public mothersMaidenName mothersMaidenName {get;set;}
    }

    public class possibleRelative
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }
        public String slug {get;set;}
        public String employeeCode {get;set;}
        public String fullname {get;set;}
        public String picture {get;set;}
        public String position {get;set;}
        public String department {get;set;}
        public String branch {get;set;}
        public String father { get; set;}
        public String mothersMaidenName {get;set;}
    }
}