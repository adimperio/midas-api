﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Filters;
using Microsoft.AspNetCore.Cors;
using kmbi_core_master.spm.IRepository;
using kmbi_core_master.spm.Models;
using kmbi_core_master.coreMaster;
using System;
using kmbi_core_master.trainingModule.IRepository;
using kmbi_core_master_v2.trainingModule.Models;

namespace kmbi_core_master.trainingModule.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/trainingInstances")]
    [usersAuth]
    public class trainingInstancesController : Controller
    {
        private readonly itrainingInstancesRepository _Repository;
        private readonly iusersRepository _logInfo;
        private string token;

        public trainingInstancesController(itrainingInstancesRepository Repositor, iusersRepository logInfo)
        {
            this._Repository = Repositor;
            _logInfo = logInfo;
        }

        public void logger(string message)
        {
            //Insert Log//
            token = HttpContext.Request.Headers["token"];
            var lInfo = _logInfo.getLoginInfo(token);
            string log = message;
            global.createLog(lInfo.id, lInfo.fullname, lInfo.employment.branch, log);
            //Insert Log//
        }

        // GET: 
        [HttpGet]
        public IEnumerable<trainingInstances> GetAll()
        {
            var c = _Repository.getAll();
            return c;

        }

        [HttpGet("{slug}")]
        public IActionResult getSlug(string slug)
        {

            var item = _Repository.getBySlug(slug);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

       

        [HttpPost]
        [errorHandler]
        public IActionResult create([FromBody] trainingInstances b)
        {

            _Repository.add(b);

            logger( "Insert new training Instance " + b.name);

            return Ok(new
            {
                type = "success",
                message = b.name + " created successfully.",
                slug = b.slug,
                instance = b
            });

        }



        [HttpPut("{slug}")]
        public IActionResult slugupdate(string slug, [FromBody] trainingInstances b)
        {
            var item = _Repository.getBySlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _Repository.update(slug, b);

           logger("Modified training Instance " + b.name);

            return Ok(new
            {
                type = "success",
                message = b.name + " updated successfully.",
                instance = b
            });
        }




        [HttpDelete("{slug}")]
        public IActionResult slugdelete(string slug)
        {

            var item = _Repository.getBySlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.remove(slug);

            logger("Delete training " + item.name);

            return Ok(new
            {
                type = "success",
                message = item.name + " deleted successfully."
            });
        }

        [HttpGet("trainingsperyear")]
        public IActionResult TrainingsPerYear([FromQuery]Int32 Year = 0) {
            if(Year == 0)
                Year = DateTime.Now.Year;
            var items = _Repository.getByYear(Year);

            return Ok(new {trainings =items});
        }

       
    }
}