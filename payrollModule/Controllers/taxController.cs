﻿

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

using kmbi_core_master.coreMaster;
using kmbi_core_master.coreMaster.Filters;
using kmbi_core_master.payrollModule.IRepository;
using kmbi_core_master.payrollModule.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace kmbi_core_master.payrollModule.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/tax")]
    [usersAuth]
    public class taxController : Controller
    {
        private readonly itaxRepository _Repository;

        public taxController(itaxRepository Repository)
        {
            this._Repository = Repository;
        }

        

        [HttpGet]
        public IActionResult Get([FromQuery] string taxCode)
        {
            var item = _Repository.getExemption(taxCode);
            if (item == null)
            {
               
                return Ok(new { });
            }

            return Ok(new {
                exemptionAmount = item.exemption.amount
            });
        }
       
    }

    
}
