﻿

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

using kmbi_core_master.coreMaster;
using kmbi_core_master.coreMaster.Filters;
using kmbi_core_master.payrollModule.IRepository;
using kmbi_core_master.payrollModule.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace kmbi_core_master.payrollModule.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/philhealth")]
    [usersAuth]
    public class philhealthController : Controller
    {
        private readonly iphilhealthRepository _Repository;

        public philhealthController(iphilhealthRepository Repository)
        {
            this._Repository = Repository;
        }

        

        // GET id
        [HttpGet]
        public IActionResult Get([FromQuery] double salary)
        {
            var item = _Repository.getSalary(salary);
            if (item == null)
            {
               
                return Ok(new { });
            }

            return Ok(new {
                ee = item.contribution.ee,
                er = item.contribution.er,
                total = item.total
            });
        }
       
    }

    
}
