﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Filters;
using Microsoft.AspNetCore.Cors;
using kmbi_core_master.documentationModule.iRepositories;
using kmbi_core_master.helpers.interfaces;

namespace kmbi_core_master.coreMaster.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/departments")]
    public class departmentController : Controller
    {
        private readonly idepartmentRepository _Repository;
        private readonly iusersRepository _logInfo;
        private string token;

        public departmentController(idepartmentRepository Repository, iusersRepository logInfo)
        {
            this._Repository = Repository;
            _logInfo = logInfo;
        }

        public void logger(string message)
        {
            //Insert Log//
            token = HttpContext.Request.Headers["token"];
            var lInfo = _logInfo.getLoginInfo(token);
            string log = message;
            global.createLog(lInfo.id, lInfo.fullname, lInfo.employment.branch, log);
            //Insert Log//
        }

        // GET: 
        [HttpGet]
        public IEnumerable<departments> GetAll()
        {
            var d = _Repository.getAll();
            return d;

        }

        //GET  Slug
        [HttpGet("{slug}")]
        public IActionResult getSlug(string slug)
        {

            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

      
        [HttpGet("search")]
        public IActionResult search([FromQuery]string name = "")
        {
            if(name!=null) {
                var item = _Repository.search(name);
                if(item!=null) {
                    return Ok(item);
                } else {
                    return new JsonStringResult("[]");    
                }
            } else {
                return new JsonStringResult("[]");
            }
        }

    
        [HttpPost]
        public IActionResult create([FromBody] departments b)
        {
            

            _Repository.add(b);

            logger("Insert new department " + b.name);

            return Ok(new
            {
                type = "success",
                message = b.name + " created successfully.",
                slug = b.slug
            });


        }

        
        // PUT branch
        [errorHandler]
        [HttpPut("{slug}")]
        public IActionResult update(string slug, [FromBody] departments b)
        {
            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.update(slug, b);

           logger( "Update department " + b.name);

            return Ok(new
            {
                type = "success",
                message = b.name + " updated successfully."
            });
        }

        
        [HttpDelete("{slug}")]
        public IActionResult delete(string slug)
        {

            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.delete(slug);

           logger( "Delete department " + item.name);
            return Ok(new
            {
                type = "success",
                message = item.name + " deleted successfully."
            });
        }

        
    }
}
