﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace kmbi_core_master.coreMaster.Models
{
    public class activity
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("userId")]
        public string userId { get; set; }

        [BsonElement("fullname")]
        public string fullname { get; set; }

        [BsonElement("picture")]
        public string picture { get; set; }

        [BsonElement("branchId")]
        public string branchId { get; set; }

        [BsonElement("log")]
        public string log { get; set; }

        [BsonElement("level")]
        public int level { get; set; }

        [BsonElement("link")]
        public string link { get; set; }

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }
    }
}
