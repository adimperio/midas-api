﻿using kmbi_core_master.adminModule.Inv.IRepository;
using kmbi_core_master.adminModule.Inv.Models;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Models;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace kmbi_core_master.adminModule.Inv.Repository
{
    public class itemsRepository : dbCon, iitemsRepository
    {
        public itemsRepository(IOptions<dbSettings> settings) : base(settings)
        {
        }

        public void Add(items i)
        {

            i.createdAt = DateTime.Now;
            i.updatedAt = DateTime.Now;

            _db.GetCollection<items>("items").InsertOneAsync(i);
        }

        public IEnumerable<items> getAll()
        {
            var c = _db.GetCollection<items>("items").Find(new BsonDocument()).ToListAsync();
            return c.Result;
        }

        public items getBySlug(String slug)
        {
            var query = Builders<items>.Filter.Eq(e => e.slug, slug);
            var c = _db.GetCollection<items>("items").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }

       
        public void update(String slug, items p)
        {
            var query = Builders<items>.Filter.Eq(e => e.slug, slug);
            var update = Builders<items>.Update
                .Set("name", p.name)
                .Set("code", p.code)
                .Set("description", p.description)
                .Set("category", p.category)
                .Set("updatedBy", p.updatedBy)
                .CurrentDate("updatedAt");

            var l = _db.GetCollection<items>("items").UpdateOneAsync(query, update);

        }
       

        public void remove(String slug)
        {
            var query = Builders<items>.Filter.Eq(e => e.slug, slug);
            var result = _db.GetCollection<items>("items").DeleteOneAsync(query);
        }
    }
}
