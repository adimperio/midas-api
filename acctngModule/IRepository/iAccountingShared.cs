using System;
using System.Collections.Generic;
using kmbi_core_master.acctngModule.Models;
using kmbi_core_master.coreMaster.Models;

namespace kmbi_core_master.acctngModule.Interface
{
    public interface iAccountingShared
    {
        IEnumerable<dynamic> dailyBalances(DateTime from, DateTime to);
        IEnumerable<dynamic> transactionList(DateTime from, DateTime to);
        List<transactionDetailed> transactionDetailedList(DateTime from, DateTime to, Int32 accountCode = 0, Boolean includeSubsidiaryIfThereIs = false);
        IEnumerable<dynamic> generalLedger(Int32? account, Int32? year);
        IEnumerable<dynamic> budgetSummary(Int32 year);
        IEnumerable<dynamic> budgetPerTransaction(Int32 year);
    }
}
