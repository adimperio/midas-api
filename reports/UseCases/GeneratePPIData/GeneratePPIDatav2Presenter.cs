

using System;
using System.Linq;
using OfficeOpenXml;

namespace kmbi_core_master.reports.UseCases.GeneratePPIData {
    public class GeneratePPIDatav2Presenter : IPresent<GeneratePPIDataResponse, GeneratePPIDatav2PresenterResponse> {
        
        public GeneratePPIDatav2PresenterResponse Present(GeneratePPIDataResponse request) {

            using (OfficeOpenXml.ExcelPackage ep = new OfficeOpenXml.ExcelPackage())
            {
                ExcelWorksheet ew = ep.Workbook.Worksheets.Add("Data");
                
                ew.Cells[1,1].Value = "PPI Data";
                ew.Cells[2,1].Value = "As of " + request.AsOf.ToShortDateString();

                ew.Cells[3,1].Value = "Name";
                ew.Cells[3,2].Value = "Branch";
                ew.Cells[3,3].Value = "Score";
                ew.Cells[3,4].Value = "National Poverty Line";
                ew.Cells[3,5].Value = "Food Poverty Line";
                ew.Cells[3,6].Value = "150 % National";
                ew.Cells[3,7].Value = "200 % National";
                ew.Cells[3,8].Value = "$1.90/day 2011";
                ew.Cells[3,9].Value = "$3.20/day 2011";
                ew.Cells[3,10].Value = "$5.50/day 2011";
                ew.Cells[3,11].Value = "$8.00/day 2011";
                ew.Cells[3,12].Value = "$11.00/day 2011";
                ew.Cells[3,13].Value = "$15.00/day 2011";
                ew.Cells[3,14].Value = "$1.25/day 2005";
                ew.Cells[3,15].Value = "$2.50/day 2005";
                ew.Cells[3,16].Value = "$5.00/day 2005";
                ew.Cells[3,17].Value = "Bottom 20th Percentile";
                ew.Cells[3,18].Value = "Bottom 40th Percentile";
                ew.Cells[3,19].Value = "Bottom 60th Percentile";
                ew.Cells[3,20].Value = "Bottom 80th Percentile";

                var ctr = 4;

                request.Details.ToList().ForEach(item=>{
                    ew.Cells[ctr,1].Value = item.Name;
                    ew.Cells[ctr,2].Value = item.Branch;
                    ew.Cells[ctr,3].Value = item.Score;
                    ew.Cells[ctr,4].Value = item.Likelihood.NationalPovertyLine;
                    ew.Cells[ctr,5].Value = item.Likelihood.FoodPovertyLine.ToString() + "%";
                    ew.Cells[ctr,6].Value = item.Likelihood._150National.ToString() + "%";
                    ew.Cells[ctr,7].Value = item.Likelihood._200National.ToString() + "%";
                    ew.Cells[ctr,8].Value = item.Likelihood._1dot90perDay2011.ToString() + "%";
                    ew.Cells[ctr,9].Value = item.Likelihood._3dot20perDay2011.ToString() + "%";
                    ew.Cells[ctr,10].Value = item.Likelihood._5dot50perDay2011.ToString() + "%";
                    ew.Cells[ctr,11].Value = item.Likelihood._8dot00perDay2011.ToString() + "%";
                    ew.Cells[ctr,12].Value = item.Likelihood._11dot00perDay2011.ToString() + "%";
                    ew.Cells[ctr,13].Value = item.Likelihood._15dot00perDay2011.ToString() + "%";
                    ew.Cells[ctr,14].Value = item.Likelihood._1dot25perDay2005.ToString() + "%";
                    ew.Cells[ctr,15].Value = item.Likelihood._2dot50perDay2005.ToString() + "%";
                    ew.Cells[ctr,16].Value = item.Likelihood._5dot00perDay2005.ToString() + "%";
                    ew.Cells[ctr,17].Value = item.Likelihood.Bottom20thPercentile.ToString() + "%";
                    ew.Cells[ctr,18].Value = item.Likelihood.Bottom40thPercentile.ToString() + "%";
                    ew.Cells[ctr,19].Value = item.Likelihood.Bottom60thPercentile.ToString() + "%";
                    ew.Cells[ctr,20].Value = item.Likelihood.Bottom80thPercentile.ToString() + "%";
                    ctr++;
                });

                var distinctBranch = request.Details.Select(x=>x.Branch).Distinct();

                distinctBranch.ToList().ForEach(x=>{
                    ExcelWorksheet ewPerBranch = ep.Workbook.Worksheets.Add(x.ToUpper());
                    
                    ewPerBranch.Cells[1,1].Value = x.ToUpper();

                    ewPerBranch.Cells[2,1].Value = "Name";
                    ewPerBranch.Cells[2,2].Value = "Score";
                    ewPerBranch.Cells[2,3].Value = "National Poverty Line";
                    ewPerBranch.Cells[2,4].Value = "Food Poverty Line";
                    ewPerBranch.Cells[2,5].Value = "150 % National";
                    ewPerBranch.Cells[2,6].Value = "200 % National";
                    ewPerBranch.Cells[2,7].Value = "$1.90/day 2011";
                    ewPerBranch.Cells[2,8].Value = "$3.20/day 2011";
                    ewPerBranch.Cells[2,9].Value = "$5.50/day 2011";
                    ewPerBranch.Cells[2,10].Value = "$8.00/day 2011";
                    ewPerBranch.Cells[2,11].Value = "$11.00/day 2011";
                    ewPerBranch.Cells[2,12].Value = "$15.00/day 2011";
                    ewPerBranch.Cells[2,13].Value = "$1.25/day 2005";
                    ewPerBranch.Cells[2,14].Value = "$2.50/day 2005";
                    ewPerBranch.Cells[2,15].Value = "$5.00/day 2005";
                    ewPerBranch.Cells[2,16].Value = "Bottom 20th Percentile";
                    ewPerBranch.Cells[2,17].Value = "Bottom 40th Percentile";
                    ewPerBranch.Cells[2,18].Value = "Bottom 60th Percentile";
                    ewPerBranch.Cells[2,19].Value = "Bottom 80th Percentile";

                    var ewPerBranchCtr = 3;

                    request.Details.Where(item=>item.Branch == x).ToList().ForEach(item=>{
                        ewPerBranch.Cells[ewPerBranchCtr,1].Value = item.Name;
                        ewPerBranch.Cells[ewPerBranchCtr,2].Value = item.Score;
                        ewPerBranch.Cells[ewPerBranchCtr,3].Value = item.Likelihood.NationalPovertyLine.ToString() + "%";
                        ewPerBranch.Cells[ewPerBranchCtr,4].Value = item.Likelihood.FoodPovertyLine.ToString() + "%";
                        ewPerBranch.Cells[ewPerBranchCtr,5].Value = item.Likelihood._150National.ToString() + "%";
                        ewPerBranch.Cells[ewPerBranchCtr,6].Value = item.Likelihood._200National.ToString() + "%";
                        ewPerBranch.Cells[ewPerBranchCtr,7].Value = item.Likelihood._1dot90perDay2011.ToString() + "%";
                        ewPerBranch.Cells[ewPerBranchCtr,8].Value = item.Likelihood._3dot20perDay2011.ToString() + "%";
                        ewPerBranch.Cells[ewPerBranchCtr,9].Value = item.Likelihood._5dot50perDay2011.ToString() + "%";
                        ewPerBranch.Cells[ewPerBranchCtr,10].Value = item.Likelihood._8dot00perDay2011.ToString() + "%";
                        ewPerBranch.Cells[ewPerBranchCtr,11].Value = item.Likelihood._11dot00perDay2011.ToString() + "%";
                        ewPerBranch.Cells[ewPerBranchCtr,12].Value = item.Likelihood._15dot00perDay2011.ToString() + "%";
                        ewPerBranch.Cells[ewPerBranchCtr,13].Value = item.Likelihood._1dot25perDay2005.ToString() + "%";
                        ewPerBranch.Cells[ewPerBranchCtr,14].Value = item.Likelihood._2dot50perDay2005.ToString() + "%";
                        ewPerBranch.Cells[ewPerBranchCtr,15].Value = item.Likelihood._5dot00perDay2005.ToString() + "%";
                        ewPerBranch.Cells[ewPerBranchCtr,16].Value = item.Likelihood.Bottom20thPercentile.ToString() + "%";
                        ewPerBranch.Cells[ewPerBranchCtr,17].Value = item.Likelihood.Bottom40thPercentile.ToString() + "%";
                        ewPerBranch.Cells[ewPerBranchCtr,18].Value = item.Likelihood.Bottom60thPercentile.ToString() + "%";
                        ewPerBranch.Cells[ewPerBranchCtr,19].Value = item.Likelihood.Bottom80thPercentile.ToString() + "%";
                        ewPerBranchCtr++;
                    });  
                

                });

                ExcelWorksheet ewSummary = ep.Workbook.Worksheets.Add("Summary");

                var ewSummaryCtr = 2;
                double totalCli = 0;
                double totalPer = 0;

                distinctBranch.ToList().ForEach(x=>{
                    double count = request.Details.Where(item=>item.Branch == x).Count();
                    double nplSum  = request.Details.Where(item=>item.Branch == x).Sum(item=>item.Likelihood.NationalPovertyLine);

                    var ave = Math.Round(nplSum / count,2);
                    totalCli += count;
                    totalPer += ave;

                    ewSummary.Cells[ewSummaryCtr,1].Value = x.ToUpper();
                    ewSummary.Cells[ewSummaryCtr,2].Value = count;
                    ewSummary.Cells[ewSummaryCtr,3].Value = ave.ToString() + "%";
                    var a = Math.Round(count * (ave / 100),2);
                    ewSummary.Cells[ewSummaryCtr,4].Value = a;
                    ewSummaryCtr++;
                });

                var fin = Math.Round(totalPer / totalCli,2);
                ewSummary.Cells[ewSummaryCtr,2].Value = totalCli;
                ewSummary.Cells[ewSummaryCtr,4].Value = fin.ToString() + "%";


                ewSummaryCtr+=2;

                ewSummary.Cells[ewSummaryCtr,1].Value = string.Format("{0} of the total of {1} client(s) live below the National Poverty Line",fin.ToString() + "%", totalCli);

                ewSummaryCtr+=2;

                ewSummary.Cells[ewSummaryCtr,1].Value = "PPI Segmentation";
                ewSummary.Cells[ewSummaryCtr+1,1].Value = "0.1% --> 19%";
                ewSummary.Cells[ewSummaryCtr+1,2].Value = "least likely to be poor";
                ewSummary.Cells[ewSummaryCtr+2,1].Value = "20% --> 40%";
                ewSummary.Cells[ewSummaryCtr+2,2].Value = "intermediate likelihood";
                ewSummary.Cells[ewSummaryCtr+3,1].Value = "41% --> up";
                ewSummary.Cells[ewSummaryCtr+3,2].Value = "most likely to b poor";
                

                byte[] bytes = ep.GetAsByteArray();
                ep.Dispose();

                return new GeneratePPIDatav2PresenterResponse {
                    data = request,
                    bytes = bytes
                };
            }

        }
    }
}