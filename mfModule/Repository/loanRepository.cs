﻿using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.documentationModule.Models;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace kmbi_core_master.coreMaster.Repository
{
    public class loanRepository : dbCon, iloanRepository
    {
        public loanRepository(IOptions<dbSettings> settings) : base(settings)
        {
        }

        public void add(loans b)
        {

            b.createdAt = DateTime.Now;
            b.updatedAt = DateTime.Now;

            _db.GetCollection<loans>("loans").InsertOneAsync(b);

            //for (int i = 0; i <= b.members.Count - 1; i++)
            //{
            //    cbuLogs log = new cbuLogs
            //    {
            //        Id = ObjectId.GenerateNewId().ToString(),
            //        type = "upfront fees",
            //        slug = b.members[i].slug + '-' + global.slugify(8),
            //        clientId = b.members[i].memberId,
            //        amount = b.members[i].upfrontFees.CBUAmount,
            //        cbuBalance = b.members[i].upfrontFees.CBUAmount,
            //        createdAt = DateTime.Now,
            //        updatedAt = DateTime.Now
            //    };
            //    _db.GetCollection<cbuLogs>("cbuLogs").InsertOneAsync(log);
            //}

            
        }

        public IEnumerable<loans> allLoans()
        {
            var c = _db.GetCollection<loans>("loans").Find(new BsonDocument()).ToListAsync();
            return c.Result;
        }

        public loans getSlug(String slug)
        {
            var query = Builders<loans>.Filter.Eq(e => e.slug, slug);
            var c = _db.GetCollection<loans>("loans").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }

        public List<loans> getClientSlug(String slug)
        {
            var query = Builders<loans>.Filter.ElemMatch(x => x.members, x => x.slug == slug);
            //var c = _db.GetCollection<loans>("loans").Find(query).ToListAsync();
            var fields = Builders<loans>.Projection
                .Include(u => u.Id)
                .Include(u => u.slug)
                .Include(u => u.referenceId)
                .Include(u => u.centerId)
                .Include(u => u.branchId)
                .Include(u => u.officerId)
                .Include(u => u.officerName)
                .Include(u => u.unitId)
                .Include(u => u.unitCode)
                .Include(u => u.centerSlug)
                .Include(u => u.centerName)
                .Include(u => u.branchSlug)
                .Include(u => u.branchName)
                .Include(u => u.officerSlug)
                .Include(u => u.unitSlug)
                .Include(u => u.providerId)
                .Include(u => u.centerLoanCycle)
                .Include(u => u.disbursementDate)
                .Include(u => u.firstPaymentDate)
                .Include(u => u.loanType)
                .Include(u => u.amounts)
                .Include(u => u.status)
                .Include(u => u.createdAt)
                .ElemMatch(x => x.members, x => x.slug == slug);
            var c = _db.GetCollection<loans>("loans").Find(query).Project<loans>(fields).ToListAsync();
            return c.Result;
        }

        public loans getId(String id)
        {
            var query = Builders<loans>.Filter.Eq(e => e.Id, id);
            var c = _db.GetCollection<loans>("loans").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }

        public IEnumerable<getbyBranchloans> getBranchId(String branchId)
        {
           
            var coll = _db.GetCollection<getbyBranchloans>("loans");
            var rep = coll.Aggregate()
                .Match(e => e.branchId == branchId)
                .Project<getbyBranchloans>(new BsonDocument
                {
                    {"Id", 1},
                    {"slug",1},
                    {"referenceId",1},
                    {"centerId", 1},
                    {"branchId", 1},
                    {"officerId",1},
                    {"officerName",1},
                    {"unitId",1},
                    {"unitCode",1},
                    {"centerSlug",1},
                    {"centerName",1},
                    {"branchSlug",1},
                    {"branchName",1},
                    {"officerSlug",1},
                    {"unitSlug",1},
                    {"providerId",1},
                    {"centerLoanCycle",1},
                    {"disbursementDate",1},
                    {"firstPaymentDate",1},
                    {"lastPaymentDate",1},
                    {"loanType",1},
                    {"amounts",1},
                    {"status",1},
                    {"createdAt",1},
                    {"totalMembers", new BsonDocument { { "$size","$members" } } }
                })
                .ToListAsync();
                return rep.Result;
        }

        public IEnumerable<getLoansByBranch> getLoansByBranchId(String branchId)
        {

            var coll = _db.GetCollection<getLoansByBranch>("loans");
            var rep = coll.Aggregate()
                .Match(e => e.branchId == branchId)
                .Project<getLoansByBranch>(new BsonDocument
                {
                    {"Id", 1},
                    {"slug",1},
                    {"referenceId",1},
                    {"centerSlug", 1},
                    {"amounts", 1},
                    {"centerLoanCycle",1},
                    {"disbursementDate",1},
                    {"createdAt",1},
                    {"status",1},
                    {"totalMembers", new BsonDocument { { "$size","$members" } } }
                })
                .ToListAsync();
            return rep.Result;
        }

        public IEnumerable<getbyBranchloans> getCenterSlug(String centerSlug)
        {
            //var query = Builders<loans>.Filter.Eq(e => e.centerSlug, centerSlug);
            //var c = _db.GetCollection<loans>("loans").Find(query).ToListAsync();

            //return c.Result;
            var coll = _db.GetCollection<getbyBranchloans>("loans");
            var rep = coll.Aggregate()
                .Match(e => e.centerSlug == centerSlug)
                .Project<getbyBranchloans>(new BsonDocument
                {
                    {"Id", 1},
                    {"slug",1},
                    {"referenceId",1},
                    {"centerId", 1},
                    {"branchId", 1},
                    {"officerId",1},
                    {"officerName",1},
                    {"unitId",1},
                    {"unitCode",1},
                    {"centerSlug",1},
                    {"centerName",1},
                    {"branchSlug",1},
                    {"branchName",1},
                    {"officerSlug",1},
                    {"unitSlug",1},
                    {"providerId",1},
                    {"centerLoanCycle",1},
                    {"disbursementDate",1},
                    {"firstPaymentDate",1},
                    {"lastPaymentDate",1},
                    {"loanType",1},
                    {"amounts",1},
                    {"status",1},
                    {"createdAt",1},
                    {"totalMembers", new BsonDocument { { "$size","$members" } } }
                })
                .ToListAsync();
            return rep.Result;
        }

        public IEnumerable<getbyBranchCenterloans> getLoanByCenter(String centerSlug)
        {
            //var query = Builders<loans>.Filter.Eq(e => e.centerSlug, centerSlug);
            //var c = _db.GetCollection<loans>("loans").Find(query).ToListAsync();

            //return c.Result;
            var coll = _db.GetCollection<getbyBranchCenterloans>("loans");
            var rep = coll.Aggregate()
                .Match(e => e.centerSlug == centerSlug)
                .Project<getbyBranchCenterloans>(new BsonDocument
                {
                    {"Id", 1},
                    {"slug",1},
                    {"referenceId",1},
                    {"centerId", 1},
                    {"branchId", 1},
                    {"officerId",1},
                    {"officerName",1},
                    {"unitId",1},
                    {"unitCode",1},
                    {"centerSlug",1},
                    {"centerName",1},
                    {"branchSlug",1},
                    {"branchName",1},
                    {"officerSlug",1},
                    {"unitSlug",1},
                    {"providerId",1},
                    {"centerLoanCycle",1},
                    {"disbursementDate",1},
                    {"firstPaymentDate",1},
                    {"lastPaymentDate",1},
                    {"loanType",1},
                    {"amounts",1},
                    {"status",1},
                    {"createdAt",1},
                    {"members",1},
                    {"totalMembers", new BsonDocument { { "$size","$members" } } }
                })
                .ToListAsync();
            return rep.Result;
        }

        public getloanTypes getName(String name)
        {
            var query = Builders<getloanTypes>.Filter.Eq(e => e.name, name);
            var fields = Builders<getloanTypes>.Projection
                .Include(u => u.Id)
                .Include(u => u.name)
                .Include(u => u.interestRate)
                .Include(u => u.frequency)
                .Include(u => u.maxClients)
                .Include(u => u.minClients)
                .Include(u => u.interestType)
                .Include(u => u.processingFee)
                .Include(u => u.initialCBUpercentage)
                .Include(u => u.initialCBU)
                .Include(u => u.cbuAmountPerTerm)
                .Include(u => u.allowDownpayment)
                .Include(u => u.modesOfPayment);

            var c = _db.GetCollection<getloanTypes>("loanTypes").Find(query).Project<getloanTypes>(fields).ToListAsync();

            return c.Result.FirstOrDefault();
        }


        public void update(String id, loans p)
        {
            p.Id = id;
            var query = Builders<loans>.Filter.Eq(e => e.Id, id);
            var update = Builders<loans>.Update
                .Set("centerId", p.centerId)
                .Set("branchId", p.branchId)
                .Set("officerId", p.officerId)
                .Set("officerName", p.officerName)
                .Set("unitId", p.unitId)
                .Set("unitCode", p.unitCode)
                .Set("centerSlug", p.centerSlug)
                .Set("centerName", p.centerName)
                .Set("branchSlug", p.branchSlug)
                .Set("branchName", p.branchName)
                .Set("officerSlug", p.officerSlug)
                .Set("unitSlug", p.unitSlug)
                .Set("providerId", p.providerId)
                .Set("loanType", p.loanType)
                .Set("centerLoanCycle", p.centerLoanCycle)
                .Set("disbursementDate", p.disbursementDate)
                .Set("firstPaymentDate", p.firstPaymentDate)
                .Set("lastPaymentDate", p.lastPaymentDate)
                .Set("amounts", p.amounts)
                .Set("status", p.status)
                .Set("members", p.members)
                .Set("previousMembers", p.previousMembers)
                // .Set("approvalStatus", p.approvalStatus)
                // .Set("regionSlug", p.regionSlug)
                // .Set("areaSlug" , p.areaSlug)
                .CurrentDate("updatedAt");

            var l = _db.GetCollection<loans>("loans").UpdateOneAsync(query, update);

        }

        public void updateStatus(String id, loans p)
        {
            p.Id = id;
            var query = Builders<loans>.Filter.Eq(e => e.Id, id);
            var update = Builders<loans>.Update
                .Set("status", p.status)
                .CurrentDate("updatedAt");

            var l = _db.GetCollection<loans>("loans").UpdateOneAsync(query, update);

        }

        public void updateCenterLoanCycle(String slug, loans p)
        {
            p.slug = slug ;
            var query = Builders<loans>.Filter.Eq(e => e.slug, slug);
            var update = Builders<loans>.Update
                .Set("centerLoanCycle", p.centerLoanCycle)
                .CurrentDate("updatedAt");

            var l = _db.GetCollection<loans>("loans").UpdateOneAsync(query, update);

        }

        public void updateSlug(String slug, loans p)
        {
           // p.slug = slug;
            var query = Builders<loans>.Filter.Eq(e => e.slug, slug);
            var update = Builders<loans>.Update
                .Set("centerId", p.centerId)
                .Set("branchId", p.branchId)
                .Set("officerId", p.officerId)
                .Set("officerName", p.officerName)
                .Set("unitId", p.unitId)
                .Set("unitCode", p.unitCode)
                .Set("centerSlug", p.centerSlug)
                .Set("centerName", p.centerName)
                .Set("branchSlug", p.branchSlug)
                .Set("branchName", p.branchName)
                .Set("officerSlug", p.officerSlug)
                .Set("providerId", p.providerId)
                .Set("loanType", p.loanType)
                .Set("centerLoanCycle", p.centerLoanCycle)
                .Set("disbursementDate", p.disbursementDate)
                .Set("firstPaymentDate", p.firstPaymentDate)
                .Set("lastPaymentDate", p.lastPaymentDate)
                .Set("amounts", p.amounts)
                .Set("status", p.status)
                .Set("members", p.members)
                .Set("previousMembers", p.previousMembers)
                .Set("approvalStatus", p.approvalStatus)
                .Set("regionSlug", p.regionSlug)
                .Set("areaSlug", p.areaSlug)
                .CurrentDate("updatedAt");

            var l = _db.GetCollection<loans>("loans").UpdateOneAsync(query, update);

        }


        public void remove(String id)
        {
            var query = Builders<loans>.Filter.Eq(e => e.Id, id);
            var result = _db.GetCollection<loans>("loans").DeleteOneAsync(query);
        }


        public void removeSlug(String slug)
        {
            var query = Builders<loans>.Filter.Eq(e => e.slug, slug);
            var result = _db.GetCollection<loans>("loans").DeleteOneAsync(query);
        }

        

        public void getNewSchedule(string refId)
        {
            
            int cnt = 0;
            IEnumerable<holiday> list = _db.GetCollection<holiday>("holiday").Find(new BsonDocument()).ToListAsync().Result;
            IEnumerable<DateTime> holidays = from x in list select x.date;

            var query = Builders<loans>.Filter.Eq(e => e.referenceId, refId);
            var clients = _db.GetCollection<loans>("loans").Find(query).ToListAsync().Result.FirstOrDefault();
           

            DateTime disbursementDate = clients.disbursementDate.Value;
            int term = clients.loanType.term;

            // New Schedule
            for (int i = 0; i <= term - 1; i++)
            {
                cnt += 1;
                disbursementDate = disbursementDate.AddDays(7);
                while (holidays.Contains(disbursementDate))
                {
                    disbursementDate = disbursementDate.AddDays(7);
                }


                //Update each member
                for (int r = 0; r <= clients.members.Count - 1; r++)
                {
                    var update = Builders<loans>.Update
                       .Set("members." + r + ".schedule." + i + ".collectionDate", disbursementDate)
                       .Set("lastPaymentDate", disbursementDate)
                       .CurrentDate("updatedAt");

                    var l = _db.GetCollection<loans>("loans").UpdateOneAsync(query, update);
                    
                }
            }
        }


        public void updateCbuInterest(List<string> p, string type, DateTime transactedAt, string loanId)
        {
            
            IEnumerable<string> slugs = from x in p select x;

            if (type == "increment")
            { 
                for (int i = 0; i <= p.Count - 1; i++)
                {
                    var query = Builders<clients>.Filter.Eq(e => e.slug, p[i]);
                    var result = _db.GetCollection<clients>("clients").Find(query).ToListAsync().Result.FirstOrDefault();
                    var cBal = result.cbuBalance;
                    var update = Builders<clients>.Update
                        .Set("cbuBalance", Convert.ToDouble(string.Format("{0:n2}", result.cbuBalance * 1.01)))
                        .CurrentDate("updatedAt");
                    var l = _db.GetCollection<clients>("clients").UpdateOneAsync(query, update);

                    cbuLogs c = new cbuLogs
                    {
                        Id = ObjectId.GenerateNewId().ToString(),
                        type = "increment percentage",
                        slug = result.slug ,
                        clientId = result.Id,
                        amount = result.cbuBalance * 0.01,
                        cbuBalance = result.cbuBalance * 1.01,
                        createdAt = DateTime.Now,
                        updatedAt = DateTime.Now,
                        transactedAt = transactedAt,
                        loan = new cbuLoan {
                            id = loanId,
                            termNo = null
                        }
                    };
                    _db.GetCollection<cbuLogs>("cbuLogs").InsertOneAsync(c);

                }
            }

            if (type == "decrement")
            {
                for (int i = 0; i <= p.Count - 1; i++)
                {
                    var query = Builders<clients>.Filter.Eq(e => e.slug, p[i]);
                    var result = _db.GetCollection<clients>("clients").Find(query).ToListAsync().Result.FirstOrDefault();
                    var cBal = result.cbuBalance;
                    var update = Builders<clients>.Update
                        .Set("cbuBalance", Convert.ToDouble(string.Format("{0:n2}", result.cbuBalance / 1.01)))
                        .CurrentDate("updatedAt");
                    var l = _db.GetCollection<clients>("clients").UpdateOneAsync(query, update);

                    cbuLogs c = new cbuLogs
                    {
                        Id = ObjectId.GenerateNewId().ToString(),
                        type = "decrement percentage",
                        slug = result.slug ,
                        clientId = result.Id,
                        amount = result.cbuBalance / 0.01,
                        cbuBalance = result.cbuBalance / 1.01,
                        createdAt = DateTime.Now,
                        updatedAt = DateTime.Now,
                        transactedAt = transactedAt,
                        loan = new cbuLoan
                        {
                            id = loanId,
                            termNo = null
                        }
                    };
                    _db.GetCollection<cbuLogs>("cbuLogs").InsertOneAsync(c);
                }
            }

        }

        public void changeHealth(string refId, String memberSlug, loanMembers p)
        {
            var q1 = Builders<loans>.Filter.Eq(e => e.referenceId, refId);
            var q2 = Builders<loans>.Filter.ElemMatch(x => x.members, x => x.slug == memberSlug);
            var query = Builders<loans>.Filter.And(q1, q2);
            //var clients = _db.GetCollection<loans>("loans").Find(query).ToListAsync().Result.FirstOrDefault();
            var update = Builders<loans>.Update
               .Set("members.$.health", p.health)
               .CurrentDate("updatedAt");

            var l = _db.GetCollection<loans>("loans").UpdateOneAsync(query, update);
        }


        public List<loanSched> collectionDate(DateTime collDate, string branchSlug)
        {
            
            List<loanSched> newLoans = new List<loanSched>();
            
            

            var stat = Builders<loans>.Filter.Eq(x => x.status, "for-payment");
            var bSlug = Builders<loans>.Filter.Eq(x => x.branchSlug, branchSlug);
            var sb = Builders<loans>.Filter.And(stat, bSlug);
            var c = _db.GetCollection<loans>("loans").Find(sb).ToListAsync().Result;

           // var res = _db.GetCollection<loans>("loans").Find(new BsonDocument()).ToListAsync();

            //var memCnt = 0;
            

            for (int i = 0; i <= c.Count-1; i++)
            {
                List<loanMembersSched> newMembers = new List<loanMembersSched>();
                foreach (loanMembers lMem in c[i].members)
                {
                    List<schedule> newSchedule = new List<schedule>();
                    foreach (schedule sched in lMem.schedule)
                    {
                        
                        var dtFrom = collDate;
                        var dtTo = collDate.Date.AddHours(23).AddMinutes(59).AddSeconds(59);

                        if (sched.collectionDate >= dtFrom && sched.collectionDate <= dtTo)
                        {
                            newSchedule.Add(new schedule
                            {
                                weekNo = sched.weekNo,
                                collectionDate = sched.collectionDate,
                                interestRate = sched.interestRate,
                                paidAt = sched.paidAt,
                                principalBalance = sched.principalBalance,
                                interestBalance = sched.interestBalance,
                                cbuBalance = sched.cbuBalance,
                                currentPrincipalDue = sched.currentPrincipalDue,
                                currentInterestDue = sched.currentInterestDue,
                                currentCbuDue = sched.currentCbuDue,
                                pastPrincipalDue = sched.pastPrincipalDue,
                                pastInterestDue = sched.pastInterestDue,
                                pastCbuDue = sched.pastCbuDue
                            });
                        }
                    }

                    if (newSchedule.Count > 0) {
                        newMembers.Add(new loanMembersSched
                        {
                            
                            slug = lMem.slug,
                            fullname = lMem.fullname,
                            schedule = newSchedule
                        });
                    }
                }

                if (newMembers.Count > 0) {
                    newLoans.Add(new loanSched
                    {
                        Id = c[i].Id,
                        slug = c[i].slug,
                        referenceId = c[i].Id,
                        branchSlug = c[i].branchSlug,
                        branchName = c[i].branchName,
                        status = c[i].status,
                        members = newMembers
                    });
                }


            }
            return newLoans;
            //return res.Result;
           
        }


        public List<loans> disbDate(DateTime disbDate, string branchSlug)
        {
            var dtTo = disbDate.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
            var dtFrom = disbDate;
            var q1 = Builders<loans>.Filter.Gte(x => x.disbursementDate, dtFrom);
            var q2 = Builders<loans>.Filter.Lte(x => x.disbursementDate, dtTo);
            var q3 = Builders<loans>.Filter.Eq(x => x.branchSlug, branchSlug);
            var q4 = Builders<loans>.Filter.Eq(x => x.status, "for-payment");
            var query = Builders<loans>.Filter.And(q1,q2, q3,q4);
            var c = _db.GetCollection<loans>("loans").Find(query).ToListAsync();

            
            return c.Result;
        }


        public List<loans> dayWeek(int dayOfWeek, string branchSlug)
        {
           
          
            var q2 = Builders<loans>.Filter.Eq(x => x.loanType.dayOfWeek, dayOfWeek);
            var q3 = Builders<loans>.Filter.Eq(x => x.branchSlug, branchSlug);
            var query = Builders<loans>.Filter.And( q2, q3);
            var c = _db.GetCollection<loans>("loans").Find(query).ToListAsync();


            return c.Result;
        }


        public List<loans> fDate(DateTime fDate, string branchSlug)
        {
            //var dtTo = fdate.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
            //var dtFrom = disbDate;
            var q1 = Builders<loans>.Filter.Lte(x => x.firstPaymentDate, fDate);
            var q2 = Builders<loans>.Filter.Eq(x => x.branchSlug, branchSlug);
            var q3 = Builders<loans>.Filter.Eq(x => x.status, "for-payment");
            var query = Builders<loans>.Filter.And(q1, q2, q3);
            var c = _db.GetCollection<loans>("loans").Find(query).ToListAsync();


            return c.Result;
        }

        public List<loans> lastPDate(DateTime pDate)
        {
            var dtTo = pDate.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
            var q1 = Builders<loans>.Filter.Gte(x => x.lastPaymentDate, pDate);
            var q2 = Builders<loans>.Filter.Lte(x => x.lastPaymentDate, dtTo);
            var query = Builders<loans>.Filter.And(q1, q2);
            var c = _db.GetCollection<loans>("loans").Find(query).ToListAsync();

            return c.Result;
        }

        public List<loans> getByStatus(string branchId, string status)
        {
            var query1 = Builders<loans>.Filter.Eq(e => e.branchId, branchId);
            var query2 = Builders<loans>.Filter.Eq(e => e.status, status);
            var query = Builders<loans>.Filter.And(query1, query2);
            var c = _db.GetCollection<loans>("loans").Find(query).ToListAsync();

            return c.Result;
        }


        public List<loans> collectionDateRaw(DateTime collDate, string branchSlug)
        {

            List<loans> newLoans = new List<loans>();



            var stat = Builders<loans>.Filter.Eq(x => x.status, "for-payment");
            var bSlug = Builders<loans>.Filter.Eq(x => x.branchSlug, branchSlug);
            var sb = Builders<loans>.Filter.And(stat, bSlug);
            var c = _db.GetCollection<loans>("loans").Find(sb).ToListAsync().Result;

            // var res = _db.GetCollection<loans>("loans").Find(new BsonDocument()).ToListAsync();

            //var memCnt = 0;


            for (int i = 0; i <= c.Count - 1; i++)
            {
                List<loanMembers> newMembers = new List<loanMembers>();
                foreach (loanMembers lMem in c[i].members)
                {
                    List<schedule> newSchedule = new List<schedule>();
                    foreach (schedule sched in lMem.schedule)
                    {

                        var dtFrom = collDate;
                        var dtTo = collDate.Date.AddHours(23).AddMinutes(59).AddSeconds(59);

                        if (sched.collectionDate >= dtFrom && sched.collectionDate <= dtTo)
                        {
                            newSchedule.Add(new schedule
                            {
                                weekNo = sched.weekNo,
                                collectionDate = sched.collectionDate,
                                interestRate = sched.interestRate,
                                paidAt = sched.paidAt,
                                principalBalance = sched.principalBalance,
                                interestBalance = sched.interestBalance,
                                cbuBalance = sched.cbuBalance,
                                currentPrincipalDue = sched.currentPrincipalDue,
                                currentInterestDue = sched.currentInterestDue,
                                currentCbuDue = sched.currentCbuDue,
                                pastPrincipalDue = sched.pastPrincipalDue,
                                pastInterestDue = sched.pastInterestDue,
                                pastCbuDue = sched.pastCbuDue
                            });
                        }
                    }

                    if (newSchedule.Count > 0) {
                        newMembers.Add(new loanMembers
                        {
                            
                            slug = lMem.slug,
                            name = lMem.name,
                            fullname = lMem.fullname,
                            birthdate = lMem.birthdate,
                            civilStatus = lMem.civilStatus,
                            memberId = lMem.memberId,
                            address = lMem.address,
                            position =lMem.position,
                            businessName = lMem.businessName,
                            rfpNumber = lMem.rfpNumber,
                            totalAmount = lMem.totalAmount,
                            principalAmount = lMem.principalAmount,
                            schedule = newSchedule,
                            upfrontFees = lMem.upfrontFees,
                            health = lMem.health,
                            issuedId = lMem.issuedId,
                            dependents= lMem.dependents,
                            beneficiary = lMem.beneficiary,
                            spouse = lMem.spouse,
                            creditLimitAmount = lMem.creditLimitAmount,
                            loanCycle = lMem.loanCycle,
                            purpose = lMem.purpose
                        });
                    }
                }


                if (newMembers.Count > 0) {
                    newLoans.Add(new loans
                    {
                        Id = c[i].Id,
                        slug = c[i].slug,
                        referenceId = c[i].referenceId,
                        centerId = c[i].centerId,
                        branchId= c[i].branchId,
                        officerId = c[i].officerId,
                        officerName = c[i].officerName,
                        unitId = c[i].unitId,
                        unitCode = c[i].unitCode,
                        centerSlug =c[i].centerSlug,
                        centerName=c[i].centerName,
                        branchSlug = c[i].branchSlug,
                        branchName = c[i].branchName,
                        officerSlug = c[i].officerSlug,
                        unitSlug = c[i].unitSlug,
                        providerId = c[i].providerId,
                        centerLoanCycle = c[i].centerLoanCycle,
                        disbursementDate = c[i].disbursementDate,
                        firstPaymentDate = c[i].firstPaymentDate,
                        lastPaymentDate = c[i].lastPaymentDate,
                        loanType = c[i].loanType,
                        amounts = c[i].amounts,
                        status = c[i].status,
                        members = newMembers,
                        previousMembers = c[i].previousMembers,
                        createdAt = c[i].createdAt,
                        updatedAt = c[i].updatedAt
                    });
                }


            }
            return newLoans;
            //return res.Result;

        }

        public List<loans> getByCenterLoanType(String centerSlug, string loanTypeId, string status)
        {
          
            var q1 = Builders<loans>.Filter.Eq(x => x.centerSlug, centerSlug);
            var q2 = Builders<loans>.Filter.Eq(x => x.loanType.id, loanTypeId);
            var q3 = Builders<loans>.Filter.Eq(x => x.status, status);
            var query = Builders<loans>.Filter.And(q1, q2, q3);
            var c = _db.GetCollection<loans>("loans").Find(query).ToListAsync();

            return c.Result;
        }

        //public List<List<loans>> getByClientSlugs(List<string> clientSlugs, string status)
        //{
        //    //var c = _db.GetCollection<loans>("loans").Find(new BsonDocument()).ToListAsync();

        //    List<List<loans>> newArrayLoans = new List<List<loans>>();
        //    List<arrayLoans> newArrayLoansQ = new List<arrayLoans>();

        //    for (int i = 0; i <= clientSlugs.Count - 1; i++)
        //    {
        //        var query1 = Builders<loans>.Filter.Eq(e => e.status, status);
        //        var query2 = Builders<loans>.Filter.ElemMatch(e => e.members, x => x.slug == clientSlugs[i].ToString());
        //        var query = Builders<loans>.Filter.And(query1, query2);
        //        var result = _db.GetCollection<loans>("loans").Find(query).ToListAsync().Result;

        //        List<loans> newLoans = new List<loans>();
        //        foreach (var item in result)
        //        {
        //            newLoans.Add(new loans
        //            {
        //                Id = item.Id,
        //                slug = item.slug,
        //                referenceId = item.referenceId,
        //                centerId = item.centerId,
        //                branchId = item.branchId,
        //                officerId = item.officerId,
        //                officerName = item.officerName,
        //                unitId = item.unitId,
        //                unitCode = item.unitCode,
        //                centerSlug = item.centerSlug,
        //                centerName = item.centerName,
        //                branchSlug = item.branchSlug,
        //                branchName = item.branchName,
        //                officerSlug = item.officerSlug,
        //                unitSlug = item.unitSlug,
        //                providerId = item.providerId,
        //                centerLoanCycle = item.centerLoanCycle,
        //                disbursementDate = item.disbursementDate,
        //                firstPaymentDate = item.firstPaymentDate,
        //                lastPaymentDate = item.lastPaymentDate,
        //                loanType = item.loanType,
        //                amounts = item.amounts,
        //                status = item.status,
        //                members = item.members,
        //                previousMembers = item.previousMembers
        //            });
        //        }
        //        newArrayLoans.Add(newLoans);
        //    }
        //    return newArrayLoans;
        //}

        public List<arrayLoans> getByClientSlugs(List<string> clientSlugs, string status)
        {
            //var c = _db.GetCollection<loans>("loans").Find(new BsonDocument()).ToListAsync();

           
            List<arrayLoans> newArrayLoans = new List<arrayLoans>();

            for (int i = 0; i <= clientSlugs.Count - 1; i++)
            {
                var query1 = Builders<loans>.Filter.Eq(e => e.status, status);
                var query2 = Builders<loans>.Filter.ElemMatch(e => e.members, x => x.slug == clientSlugs[i].ToString());
                var query = Builders<loans>.Filter.And(query1, query2);
                var result = _db.GetCollection<loans>("loans").Find(query).ToListAsync().Result;

                List<loans> newLoans = new List<loans>();
                foreach (var item in result)
                {
                    newLoans.Add(new loans
                    {
                        Id = item.Id,
                        slug = item.slug,
                        referenceId = item.referenceId,
                        centerId = item.centerId,
                        branchId = item.branchId,
                        officerId = item.officerId,
                        officerName = item.officerName,
                        unitId = item.unitId,
                        unitCode = item.unitCode,
                        centerSlug = item.centerSlug,
                        centerName = item.centerName,
                        branchSlug = item.branchSlug,
                        branchName = item.branchName,
                        officerSlug = item.officerSlug,
                        unitSlug = item.unitSlug,
                        providerId = item.providerId,
                        centerLoanCycle = item.centerLoanCycle,
                        disbursementDate = item.disbursementDate,
                        firstPaymentDate = item.firstPaymentDate,
                        lastPaymentDate = item.lastPaymentDate,
                        loanType = item.loanType,
                        amounts = item.amounts,
                        status = item.status,
                        members = item.members,
                        previousMembers = item.previousMembers
                    });
                }
                newArrayLoans.Add(new arrayLoans {
                    slug = clientSlugs[i].ToString(),
                    loans = newLoans
                });
            }
            return newArrayLoans;
        }

        public List<loanBalances> getBalances(List<string> clientSlugs, string status)
        {
            List<loanBalances> newArrayLoans = new List<loanBalances>();

            for (int i = 0; i <= clientSlugs.Count - 1; i++)
            {

                var coll = _db.GetCollection<loanPayRep>("paymentLoans");
                var iResult = coll.Aggregate()
                    .Match(new BsonDocument { { "loans.status", status } })
                    .Match(new BsonDocument { { "members.slug", clientSlugs[i].ToString() } })
                    .Lookup("loans", "loan.referenceId", "referenceId", "loans")
                    .Unwind("members")
                    .Unwind("loans")
                    .Unwind("loans.members")
                    .Match(new BsonDocument { { "loans.members.slug", clientSlugs[i].ToString() } })
                    .Project<loanPayRep>(new BsonDocument
                    {
                        {"loans.referenceId", 1},
                        {"loans.status",1},
                        {"loans.members",1},
                        {"loans.centerName",1},
                        {"members.slug", 1},
                        {"prinPayment", new BsonDocument{ { "$sum", "$members.amounts.principal" } } },
                        {"intPayment", new BsonDocument{ { "$sum", "$members.amounts.interest" } } },
                        {"cbuPayment", new BsonDocument{ { "$sum", "$members.amounts.cbu" } } }
                    })
                    .ToListAsync().Result;

                var totPrin = 0.0;
                var totInt = 0.0;
                var totCbu = 0.0;
                foreach (var item in iResult)
                {
                    totPrin += item.prinPayment;
                    totInt += item.intPayment;
                    totCbu += item.cbuPayment;
                }

                var coll1 = _db.GetCollection<loans>("loans");
                var iResult1 = coll1.Aggregate()
                    .Match(new BsonDocument { { "status", status } })
                    .Match(new BsonDocument { { "members.slug", clientSlugs[i].ToString() } })
                    .Unwind("members")
                    .Project<loanPayLoans>(new BsonDocument
                    {
                        {"referenceId", 1},
                        {"status",1},
                        {"centerName",1},
                        {"members",1}
                    })
                    .ToListAsync().Result.LastOrDefault();

                string refId = "";
                double totalPrincipalPayment = 0;
                double totalInterestPayment = 0;
                double totalCbuPayment = 0;
                double totalLoan = 0;
                double totalPrincipal = 0;
                double totalInterest = 0;
                double totalCbu = 0;


                //foreach (var item in iResult)
                //{
                //    refId = item.loans.referenceId ;
                //    totalPrincipalPayment += item.prinPayment;
                //    totalInterestPayment += item.intPayment;
                //    totalCbuPayment += item.cbuPayment;
                //    totalLoan = item.loans.members.totalAmount;
                //    totalPrincipal = item.loans.members.principalAmount;
                //    totalInterest = totalLoan - item.loans.members.principalAmount;
                //    //totalCbu += item.loans.members.schedule.FirstOrDefault().currentCbuDue;
                //    foreach (var cbu in item.loans.members.schedule)
                //    {
                //        totalCbu += cbu.currentCbuDue;
                //    }

                //}

               
                    
               

                double q = 0;
                if (iResult == null)
                {
                    if (iResult1 == null)
                    {
                        newArrayLoans.Add(new loanBalances
                        {
                            slug = clientSlugs[i].ToString(),
                            referenceId = "",
                            centerName = "",
                            amounts = new loanRemaining
                            {
                                principal = 0,
                                interest = 0,
                                cbu = 0,
                                total = 0
                            }
                        });
                    }
                    else
                    {
                        q = 0;
                        double principal = iResult1.members.principalAmount;
                        double cbu = 0;
                        foreach (var item in iResult1.members.schedule)
                        {
                            cbu += item.currentCbuDue;
                        }
                        double interest = Math.Round(principal * 0.2, 2);
                        var total = principal + interest + cbu;
                        newArrayLoans.Add(new loanBalances
                        {
                            slug = clientSlugs[i].ToString(),
                            referenceId = iResult1.referenceId,
                            centerName = iResult1.centerName,
                            amounts = new loanRemaining
                            {
                                principal = principal,
                                interest = interest ,
                                cbu = cbu,
                                total = total
                            }
                        });
                    }
                    
                }
                else
                {
                    if (iResult1 == null)
                    {
                        newArrayLoans.Add(new loanBalances
                        {
                            slug = clientSlugs[i].ToString(),
                            referenceId = "",
                            centerName = "",
                            amounts = new loanRemaining
                            {
                                principal = 0,
                                interest = 0,
                                cbu = 0,
                                total = 0
                            }
                        });
                    } else {

                        refId = iResult1.referenceId;
                        //totalPrincipalPayment += iResult.prinPayment;
                        //totalInterestPayment += iResult.intPayment;
                        //totalCbuPayment += iResult.cbuPayment;
                        //totalLoan = iResult.loans.members.totalAmount;
                        //totalPrincipal = iResult.loans.members.principalAmount;
                        //totalInterest = totalLoan - iResult.loans.members.principalAmount;

                        totalPrincipalPayment = totPrin;
                        totalInterestPayment = totInt;
                        totalCbuPayment += totCbu;
                        totalLoan = iResult1.members.totalAmount;
                        totalPrincipal = iResult1.members.principalAmount;
                        
                        //totalCbu += item.loans.members.schedule.FirstOrDefault().currentCbuDue;
                        foreach (var cbu in iResult1.members.schedule)
                        {
                            totalCbu += cbu.currentCbuDue;
                        }

                        totalInterest = totalPrincipal * 0.2;

                        q = Math.Round((totalCbu) - totalCbuPayment, 2);
                        newArrayLoans.Add(new loanBalances
                        {
                            slug = clientSlugs[i].ToString(),
                            referenceId = refId,
                            centerName = iResult1.centerName,
                            amounts = new loanRemaining
                            {
                                principal = Math.Round(totalPrincipal - totalPrincipalPayment, 2),
                                interest = Math.Round(totalInterest - totalInterestPayment, 2),
                                cbu = q,
                                total = Math.Round((totalPrincipal - totalPrincipalPayment) + (totalInterest - totalInterestPayment) + q, 2)
                            }
                        });
                    }
                }
               
                
            }
            return newArrayLoans;
        }

        public loanPortfolio getLoanPortfoliobyCenter(string centerSlug)
        {
            var query1 = Builders<loans>.Filter.Eq(e => e.status, "for-payment");
            var query2 = Builders<loans>.Filter.Eq(e => e.centerSlug, centerSlug);
            var query = Builders<loans>.Filter.And(query1, query2);
            var result = _db.GetCollection<loans>("loans").Find(query).ToListAsync().Result;

            double totPrincipal = 0;
            double totInterest = 0;
            double totCbu = 0;
            foreach (var item in result)
            {
                
                totPrincipal += item.amounts.principalAmount;
                totInterest += item.amounts.interestAmount;
                totCbu += item.amounts.cbuAmount;
            }

            loanPortfolio loanPortfolio = new loanPortfolio();
            loanPortfolio.principal = totPrincipal;
            loanPortfolio.interest = totInterest;
            loanPortfolio.cbu  = totCbu;

            return loanPortfolio;
        }

        public loanPortfolio getLoanPortfoliobyUnit(string unitSlug)
        {
            var query1 = Builders<loans>.Filter.Eq(e => e.status, "for-payment");
            var query2 = Builders<loans>.Filter.Eq(e => e.unitSlug, unitSlug);
            var query = Builders<loans>.Filter.And(query1, query2);
            var result = _db.GetCollection<loans>("loans").Find(query).ToListAsync().Result;

            double totPrincipal = 0;
            double totInterest = 0;
            double totCbu = 0;
            foreach (var item in result)
            {

                totPrincipal += item.amounts.principalAmount;
                totInterest += item.amounts.interestAmount;
                totCbu += item.amounts.cbuAmount;
            }

            loanPortfolio loanPortfolio = new loanPortfolio();
            loanPortfolio.principal = totPrincipal;
            loanPortfolio.interest = totInterest;
            loanPortfolio.cbu = totCbu;

            return loanPortfolio;
        }

        public loanPortfolio getLoanPortfoliobyPo(string poSlug)
        {
            var query1 = Builders<loans>.Filter.Eq(e => e.status, "for-payment");
            var query2 = Builders<loans>.Filter.Eq(e => e.officerSlug, poSlug);
            var query = Builders<loans>.Filter.And(query1, query2);
            var result = _db.GetCollection<loans>("loans").Find(query).ToListAsync().Result;

            double totPrincipal = 0;
            double totInterest = 0;
            double totCbu = 0;
            foreach (var item in result)
            {

                totPrincipal += item.amounts.principalAmount;
                totInterest += item.amounts.interestAmount;
                totCbu += item.amounts.cbuAmount;
            }

            loanPortfolio loanPortfolio = new loanPortfolio();
            loanPortfolio.principal = totPrincipal;
            loanPortfolio.interest = totInterest;
            loanPortfolio.cbu = totCbu;

            return loanPortfolio;
        }

        public List<loans> getByTypeBranchStatus(string type, string branch, string status)
        {
            var query1 = Builders<loans>.Filter.Eq(e => e.status, status);
            var query2 = Builders<loans>.Filter.Eq(e => e.branchSlug , branch);
            var query3 = Builders<loans>.Filter.Eq(e => e.loanType.id, type);
            var query = Builders<loans>.Filter.And(query1, query2, query3);
            var result = _db.GetCollection<loans>("loans").Find(query).ToListAsync().Result;

            return result;
        }


        public List<loansByTypeBranchStatus> getLoansByBranchTypeStatus(string type, string branch, string status)
        {
           var coll = _db.GetCollection<loansByTypeBranchStatus>("loans");
            var rep = coll.Aggregate()
                .Match(e => e.status == status)
                .Match(e => e.branchSlug == branch)
                .Match(e => e.loanType.id == type)
                .Project<loansByTypeBranchStatus>(new BsonDocument
                {
                    {"Id", 1},
                    {"slug",1},
                    {"referenceId",1},
                    {"centerName", 1},
                    {"branchSlug", 1},
                    {"unitSlug",1},
                    //{"loanType",1},
                    {"amounts",1},
                    {"status",1},
                    {"members",1}
                })
                .ToListAsync();
            return rep.Result;
        }

        public List<loans> getMatured(DateTime date, string branchSlug)
        {
            //var c = _db.GetCollection<loans>("loans").Find(new BsonDocument()).ToListAsync().Result;
            var bSlug = Builders<loans>.Filter.Eq(x => x.branchSlug, branchSlug);
            var status = Builders<loans>.Filter.Eq(x => x.status, "for-payment");
            var c = _db.GetCollection<loans>("loans").Find(Builders<loans>.Filter.And(bSlug,status)).ToListAsync().Result;

            List<loans> newLoans = new List<loans>();
            //for(int i = 0; i <= c.Count - 1; i++)
            //{
            //    var schedCount = c[i].members[0].schedule.Count()-1;
            //    var query = Builders<loans>.Filter.Lt(e => e.members[0].schedule[schedCount].collectionDate, date);
            //    var result = _db.GetCollection<loans>("loans").Find(query).ToListAsync().Result.FirstOrDefault();

            //    newLoan.Add(new loans {
            //        Id = result.Id,
            //        slug = result.slug,
            //        referenceId = result.referenceId,
            //        centerId = result.centerId,
            //        branchId = result.branchId,
            //        officerId = result.officerId,
            //        officerName = result.officerName,
            //        unitId = result.unitId,
            //        unitCode = result.unitCode,
            //        centerSlug = result.centerSlug,
            //        centerName = result.centerName,
            //        branchSlug = result.branchSlug,
            //        branchName = result.branchName,
            //        officerSlug = result.officerSlug,
            //        unitSlug = result.unitSlug,
            //        providerId = result.providerId,
            //        centerLoanCycle = result.centerLoanCycle,
            //        disbursementDate = result.disbursementDate,
            //        firstPaymentDate = result.firstPaymentDate,
            //        lastPaymentDate = result.lastPaymentDate,
            //        loanType = result.loanType,
            //        amounts = result.amounts,
            //        status = result.status,
            //        members = result.members,
            //        previousMembers = result.previousMembers
            //    });
            //}


            //return newLoan;

            for (int i = 0; i <= c.Count - 1; i++)
            {
                List<loanMembers> newMembers = new List<loanMembers>();
                foreach (loanMembers lMem in c[i].members)
                {
                    List<schedule> newSchedule = new List<schedule>();
                    foreach (schedule sched in lMem.schedule)
                    {
                        var lastSched = lMem.schedule.Count - 1;
                        var dtFrom = date;
                        var dtTo = date.Date.AddHours(23).AddMinutes(59).AddSeconds(59);

                        if (lMem.schedule[lastSched].collectionDate < dtFrom)
                        {
                            newSchedule.Add(new schedule
                            {
                                weekNo = sched.weekNo,
                                collectionDate = sched.collectionDate,
                                interestRate = sched.interestRate,
                                paidAt = sched.paidAt,
                                principalBalance = sched.principalBalance,
                                interestBalance = sched.interestBalance,
                                cbuBalance = sched.cbuBalance,
                                currentPrincipalDue = sched.currentPrincipalDue,
                                currentInterestDue = sched.currentInterestDue,
                                currentCbuDue = sched.currentCbuDue,
                                pastPrincipalDue = sched.pastPrincipalDue,
                                pastInterestDue = sched.pastInterestDue,
                                pastCbuDue = sched.pastCbuDue,
                                payments = sched.payments,
                                remarks = sched.remarks
                            });
                        }
                    }

                    if (newSchedule.Count > 0)
                    {
                        newMembers.Add(new loanMembers
                        {

                            slug = lMem.slug,
                            name = lMem.name,
                            fullname = lMem.fullname,
                            birthdate = lMem.birthdate,
                            civilStatus = lMem.civilStatus,
                            memberId = lMem.memberId,
                            address = lMem.address,
                            position = lMem.position,
                            businessName = lMem.businessName,
                            rfpNumber = lMem.rfpNumber,
                            totalAmount = lMem.totalAmount,
                            principalAmount = lMem.principalAmount,
                            schedule = newSchedule,
                            upfrontFees = lMem.upfrontFees,
                            health = lMem.health,
                            issuedId = lMem.issuedId,
                            dependents = lMem.dependents,
                            beneficiary = lMem.beneficiary,
                            spouse = lMem.spouse,
                            creditLimitAmount = lMem.creditLimitAmount,
                            loanCycle = lMem.loanCycle,
                            purpose = lMem.purpose,
                        });
                    }
                }

                if (newMembers.Count > 0)
                {
                    newLoans.Add(new loans
                    {
                        Id = c[i].Id,
                        slug = c[i].slug,
                        referenceId = c[i].referenceId,
                        centerId = c[i].centerId,
                        branchId = c[i].branchId,
                        officerId = c[i].officerId,
                        officerName = c[i].officerName,
                        unitId = c[i].unitId,
                        unitCode = c[i].unitCode,
                        centerSlug = c[i].centerSlug,
                        centerName = c[i].centerName,
                        branchSlug = c[i].branchSlug,
                        branchName = c[i].branchName,
                        officerSlug = c[i].officerSlug,
                        unitSlug = c[i].unitSlug,
                        providerId = c[i].providerId,
                        centerLoanCycle = c[i].centerLoanCycle,
                        disbursementDate = c[i].disbursementDate,
                        firstPaymentDate = c[i].firstPaymentDate,
                        lastPaymentDate = c[i].lastPaymentDate,
                        loanType = c[i].loanType,
                        amounts = c[i].amounts,
                        status = c[i].status,
                        members = newMembers,
                        previousMembers = c[i].previousMembers,
                        createdAt = c[i].createdAt,
                        updatedAt = c[i].updatedAt
                    });
                }


            }
            return newLoans;
        }

        public List<loans> getAllforPayment(List<string> clientSlugs)
        {
            List<loans> newLoans = new List<loans>();

            for (int i = 0; i <= clientSlugs.Count - 1; i++)
            {
                var query1 = Builders<loans>.Filter.Eq(e => e.status, "for-payment");
                var query2 = Builders<loans>.Filter.ElemMatch(e => e.members, x => x.slug == clientSlugs[i].ToString());
                var query = Builders<loans>.Filter.And(query1, query2);
                var result = _db.GetCollection<loans>("loans").Find(query).ToListAsync().Result;

                
                foreach (var item in result)
                {
                    newLoans.Add(new loans
                    {
                        Id = item.Id,
                        slug = item.slug,
                        referenceId = item.referenceId,
                        centerId = item.centerId,
                        branchId = item.branchId,
                        officerId = item.officerId,
                        officerName = item.officerName,
                        unitId = item.unitId,
                        unitCode = item.unitCode,
                        centerSlug = item.centerSlug,
                        centerName = item.centerName,
                        branchSlug = item.branchSlug,
                        branchName = item.branchName,
                        officerSlug = item.officerSlug,
                        unitSlug = item.unitSlug,
                        providerId = item.providerId,
                        centerLoanCycle = item.centerLoanCycle,
                        disbursementDate = item.disbursementDate,
                        firstPaymentDate = item.firstPaymentDate,
                        lastPaymentDate = item.lastPaymentDate,
                        loanType = item.loanType,
                        amounts = item.amounts,
                        status = item.status,
                        members = item.members,
                        previousMembers = item.previousMembers
                    });
                }
                //newArrayLoans.Add(new loans
                //{
                //    slug = clientSlugs[i].ToString(),
                //    loans = newLoans
                //});
            }
            return newLoans;
        }

        public void add(loans[] loans) {
            var _loans = loans;

            _loans.ToList().ForEach(l=>{
                l.createdAt = DateTime.Now;
                l.updatedAt = DateTime.Now;
            });

            _db.GetCollection<loans>("loans").InsertMany(_loans);
        }

        public List<loans> getActiveLoans() {
            var list = new List<loans>();
            
            list = _db.GetCollection<loans>("loans").Find(x=>x.status=="for-payment").ToList();

            return list;
        }

        public List<loans> getActiveLoans(DateTime from, DateTime to) {
            var list = new List<loans>();
            var filter1 = Builders<loans>.Filter.Gte(x=>x.disbursementDate, from.Date);
            var filter2 = Builders<loans>.Filter.Lte(x=>x.disbursementDate, to.Date.AddDays(1).AddSeconds(-1));
            var filter3 = Builders<loans>.Filter.Eq(x=>x.status, "for-payment");
            
            list = _db.GetCollection<loans>("loans").Find(Builders<loans>.Filter.And(filter1, filter2, filter3)).ToList();

            return list;
        }

       

        public List<loansECR> getLoansAndPayments(string loanSlug)
        {

            //var filter = Builders<loans>.Filter.Eq(e => e.slug, slug);
            //var update = Builders<loans>.Update
            //    .Set("approvalStatus", "approved")
            //    .CurrentDate("updatedAt");
            //var result = _db.GetCollection<loans>("loans").UpdateOneAsync(filter, update);
            List<loansECR> lfe = new List<loansECR>();

            //GET center
            var center = Builders<loans>.Filter.Eq(x => x.slug, loanSlug);
            var resultCenter = _db.GetCollection<loans>("loans").Find(center).FirstOrDefault();
            var centerSlug = "";
            if (resultCenter == null)
            {
                centerSlug = "";
            }
            else
            {
                centerSlug = resultCenter.centerSlug;
            }

            //GET Loans
            var q1 = Builders<loans>.Filter.Eq(x => x.centerSlug, centerSlug);
            var q2 = Builders<loans>.Filter.Eq(x => x.status, "for-payment");
            var query = Builders<loans>.Filter.And(q1,q2);
            var resultLoan = _db.GetCollection<loans>("loans").Find(query).ToList();

            //GET PAYMENTS
            //List<paymentLoans> resultPayment = new List<paymentLoans>();
            foreach (var item in resultLoan)
            {
                var payments = Builders<paymentLoans>.Filter.Eq(x => x.loan.referenceId, item.referenceId);
                var resultPayment = _db.GetCollection<paymentLoans>("paymentLoans").Find(payments).ToList();

                lfe.Add(new loansECR
                {
                    loans = item,
                    paymentLoans = resultPayment
                });

            }
            
            return lfe;

        }

        //}

        public IEnumerable<misIntegration> getMonthlyReport(string branchSlug, int year, int month)
        {

            // get Loans
            var query = Builders<loans>.Filter.Eq(e => e.branchSlug, branchSlug);
            //var query2 = Builders<loans>.Filter.Eq(e => Convert.ToDateTime(e.disbursementDate.Value).Year, year);
            //var query3 = Builders<loans>.Filter.Eq(e => Convert.ToDateTime(e.disbursementDate.Value).Month, month);
            //var query = Builders<loans>.Filter.And(query1, query2, query3);
            var result = _db.GetCollection<loans>("loans").Find(query).ToListAsync().Result;

            //get branch
            var queryBranch = Builders<branch>.Filter.Eq(e => e.slug, branchSlug);
            var branchResult = _db.GetCollection<branch>("branch").Find(queryBranch).FirstOrDefault();

            List<misIntegration> mis = new List<misIntegration>();
            List<misIntegration> misDep = new List<misIntegration>();

            foreach (var item in result)
            {
                DateTime disbursementDate = item.disbursementDate.Value;
                int disYear = disbursementDate.Year;
                int disMonth = disbursementDate.Month;

                if (disbursementDate.Year == year && disbursementDate.Month == month)
                {
                    //get center
                    var queryCenter = Builders<centerInstance>.Filter.Eq(e => e.slug, item.centerSlug);
                    var centerResult = _db.GetCollection<centerInstance>("centerInstance").Find(queryCenter).FirstOrDefault();
                    var centerCode = "";
                    if (centerResult == null)
                    {
                        centerCode = "";
                    }
                    else
                    {
                        centerCode = centerResult.code;
                    }
                    //get clients
                    foreach (var c in item.members)
                    {
                        var queryClient = Builders<clients>.Filter.Eq(e => e.slug, c.slug);
                        var clientResult = _db.GetCollection<clients>("clients").Find(queryClient).FirstOrDefault();
                        var gender = "";
                        if (clientResult == null)
                        {
                            gender = "";
                        }
                        else
                        {
                            gender = clientResult.gender;
                        }

                        mis.Add(new misIntegration
                        {
                            //slug = item.slug,
                            branch = branchResult.name,
                            center = centerCode,
                            disbursementDate = item.disbursementDate.Value,
                            maturityDate = item.lastPaymentDate.Value,
                            cycle = c.loanCycle,
                            loanAmount = c.principalAmount,
                            lastName = c.name.last,
                            firstName = c.name.first,
                            middleName = c.name.middle,
                            birthdate = c.birthdate,
                            gender = gender,
                            type = "Principal",
                            civilStatus = c.civilStatus
                        });

                        foreach (var d in c.dependents)
                        {
                            mis.Add(new misIntegration
                            {
                               // slug = item.slug,
                                branch = branchResult.name,
                                center = centerCode,
                                disbursementDate = item.disbursementDate.Value,
                                maturityDate = item.lastPaymentDate.Value,
                                cycle = 0,
                                loanAmount = 0,
                                lastName =  "",
                                firstName = d.name,
                                middleName = "",
                                birthdate = d.birthdate.Value,
                                gender = "",
                                type = d.type,
                                civilStatus = ""
                            });
                        }

                        
                    }

                }
                

                
            }

            //var newList = mis.Concat(misDep);

            return mis;
        }

     
    }
}
