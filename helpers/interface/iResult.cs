using System;


namespace kmbi_core_master.helpers.interfaces
{
    public interface iResult
    {
        String code {get;set;}
        String description {get;set;}
        byte[] bytes {get;set;}
        String raw {get;set;}
    }
}