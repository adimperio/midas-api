﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using kmbi_core_master.helpers.interfaces;
using kmbi_core_master.reports.IRepository;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Stimulsoft.Report;
using Stimulsoft.Report.Export;
using Stimulsoft.Report.Mvc;

namespace kmbi_core_master_2._0.reports.Stimulsoft.Controllers
{
    [Produces("application/json")]
    [Route("api/sti")]
    public class stiController : Controller
    {
        private IHostingEnvironment _env;
        ireporting _reporting;
        iupload _upload;
        public stiController(ireporting reporting, iupload upload, IHostingEnvironment env)
        {
            
            _reporting = reporting;
            _upload = upload;
            _env = env;
        }

        public string getDataPath()
        {
            return "./reports/Stimulsoft/Data/";
        }

        [HttpGetAttribute("sola")]
        public IActionResult GetsummaryOfLoanApplication([FromQuery]String refId = "")
        {
            //Data Source
            var rep = _reporting.getsummaryOfLoanApplication(refId);
            string data = JsonConvert.SerializeObject(rep);
            string path = getDataPath();
            System.IO.File.WriteAllText(path + "sola.json", data);

            //Report Source
            string reportPath = "./reports/Stimulsoft/Reports/sola.mrt";

            //Render Report
            var report = new StiReport();
            report.Load(reportPath);
            report.Render();
            return StiNetCoreReportResponse.PrintAsPdf(report);
          
        }

        [HttpGetAttribute("rfp")]
        public IActionResult Getrfp([FromQueryAttribute]String refId = "")
        {

            var rep = _reporting.getrfpcv(refId);
            string data = JsonConvert.SerializeObject(rep);
            string path = getDataPath();
            System.IO.File.WriteAllText(path + "rfp.json", data);

            //Report Source
            string reportPath = "./reports/Stimulsoft/Reports/rfp.mrt";

            //Render Report
            var report = new StiReport();
            report.Load(reportPath);
            report.Render();
            return StiNetCoreReportResponse.PrintAsPdf(report);
        }

        //[HttpGetAttribute("rfp")]
        //public IActionResult Getrfp([FromQueryAttribute]String refId = "")
        //{

        //    var rep = _reporting.getrfpcv(refId);
        //    string data = JsonConvert.SerializeObject(rep);
        //    string path = getDataPath();
        //    System.IO.File.WriteAllText(path + "rfp.json", data);

        //    //Report Source
        //    string reportPath = "./reports/Stimulsoft/Reports/rfp.mrt";

        //    //Render Report
        //    var report = new StiReport();
        //    report.Load(reportPath);
        //    report.Render();
        //    return StiNetCoreReportResponse.PrintAsPdf(report);
        //}

        [HttpGetAttribute("cv")]
        public IActionResult Getcv([FromQueryAttribute]String refId = "")
        {

            var rep = _reporting.getrfpcv(refId);
            string data = JsonConvert.SerializeObject(rep);
            string path = getDataPath();
            System.IO.File.WriteAllText(path + "cv.json", data);
            var link = "";//_upload.upload(res.bytes, "kmbidevreporeports/cv" + "-" + rep.center[0].code + "-" + rep.referenceId + ".pdf");
            return Redirect(link);

        }

    }
}