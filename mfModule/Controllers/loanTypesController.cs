﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Filters;
using Microsoft.AspNetCore.Cors;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace kmbi_core_master.coreMaster.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/loanTypes")]
    [usersAuth]
    public class loanTypesController : Controller
    {
        private readonly iloanTypesRepository _loanTypesRepository;
        private readonly iusersRepository _logInfo;
        private string token;

        public loanTypesController(iloanTypesRepository loanTypesRepository, iusersRepository logInfo)
        {
            this._loanTypesRepository = loanTypesRepository;
            _logInfo = logInfo;
        }

        public void logger(string message)
        {
            //Insert Log//
            token = HttpContext.Request.Headers["token"];
            var lInfo = _logInfo.getLoginInfo(token);
            string log = message;
            global.createLog(lInfo.id, lInfo.fullname, lInfo.employment.branch, log);
            //Insert Log//
        }

        // GET: 
        [HttpGet]
        public IEnumerable<loanTypes> GetAll()
        {
            var lTypes = _loanTypesRepository.allLoanTypes();
            return lTypes;

        }

        // GET id
        [HttpGet("{id:length(24)}")]
        public IActionResult Get(string id)
        {
            var item = _loanTypesRepository.Get(id);
            if (item == null)
            {
               
                return Ok(new { });
            }

            return new ObjectResult(item);
        }

        
        [HttpGet("{slug}")]
        public IActionResult getSlug(string slug)
        {

            var item = _loanTypesRepository.getSlug(slug);
            if (item == null)
            {
                return Ok(new { });
            }
           
            return new ObjectResult(item);
        }

       

        // SEARCH name
        [HttpGet("search")]
        public IActionResult searchName([FromQuery]string name)
        {

            var item = _loanTypesRepository.searchLoanTypes(name);
            if (item.Count() == 0)
            {
                //return new JsonStringResult("{\"type\":\"info\",\"message\":\"No record found.\"}");
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }



        // GET check unique slug
        [HttpGet("checkSlug")]
        public IActionResult checkSlug([FromQuery]string slug)
        {

            var item = _loanTypesRepository.checkSlug(slug);
            if (item == null)
            {
                return Ok(new
                {
                    available = 1
                });
            }

            return Ok(new
            {
                available = 0
            });
        }


       
        // POST 
        [errorHandler]
        [HttpPost]
        //[usersAccess(access: "user-add")]
        public IActionResult createLoanTypes([FromBody] loanTypes lTypes)
        {
            
            _loanTypesRepository.Add(lTypes);

            logger("Insert new loan type " + lTypes.name);
           
            return Ok(new
            {
                type = "success",
                message = lTypes.name + " created successfully.",
                slug = lTypes.slug
            });

        }

        
        // PUT 
        [errorHandler]
        [HttpPut("{slug}")]
        public IActionResult Put(string slug, [FromBody] loanTypes lTypes)
        {
            var item = _loanTypesRepository.checkSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _loanTypesRepository.Update(slug, lTypes);

            logger("Modified loan type " + lTypes.name);

            return Ok(new
            {
                type = "success",
                message = lTypes.name + " updated successfully."
            });
        }


        
        // DELETE

        [HttpDelete("{slug}")]
        public IActionResult deleteLoanTypes(string slug)
        {

            var item = _loanTypesRepository.checkSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _loanTypesRepository.Remove(slug);

            logger("Remove loan type " + item.name);

            return Ok(new
            {
                type = "success",
                message = item.name + " deleted successfully.",
                slug = item.slug
            });
        }

        

    }

    
}
