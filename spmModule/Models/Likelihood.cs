using System;

namespace kmbi_core_master.spm.Models {
    public class Likelihood {
        public Int32 Score {get;set;}
        public Double NationalPovertyLine {get;set;}
        public Double FoodPovertyLine {get;set;}
        public Double _150National {get;set;}
        public Double _200National {get;set;}
        public Double _1dot90perDay2011 {get;set;}
        public Double _3dot20perDay2011 {get;set;}
        public Double _5dot50perDay2011 {get;set;}
        public Double _8dot00perDay2011 {get;set;}
        public Double _11dot00perDay2011 {get;set;}
        public Double _15dot00perDay2011 {get;set;}
        public Double _1dot25perDay2005 {get;set;}
        public Double _2dot50perDay2005 {get;set;}
        public Double _5dot00perDay2005 {get;set;}
        public Double Bottom20thPercentile {get;set;}
        public Double Bottom40thPercentile {get;set;}
        public Double Bottom60thPercentile {get;set;}
        public Double Bottom80thPercentile {get;set;}
    }
}