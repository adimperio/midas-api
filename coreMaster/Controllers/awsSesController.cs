﻿using Amazon;
using Amazon.CloudWatch;
using Amazon.CloudWatch.Model;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Amazon.SimpleEmail;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Amazon.SimpleEmail.Model;
using kmbi_core_master.coreMaster.Models;
using Microsoft.Extensions.Options;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace kmbi_core_master.coreMaster.Controllers
{
    [Route("api/awsSes")]
    public class awsSesController : Controller
    {
        //private IHostingEnvironment hostingEnv;

        private awsSettings _settings;

        private static string _awsAccessKey;
        private static string _awsSecretKey;

        static readonly string senderAddress = "no-reply@kmbi.org.ph";
        // Set the receiver's email address here.
        static readonly string receiverAddress = "rdcanto@kmbi.org.ph";

        public awsSesController(IOptions<awsSettings> settings)
        {
            _settings = settings.Value;
            _awsAccessKey = _settings.accessKey;
            _awsSecretKey = _settings.secretKey;
        }

       
        [HttpPost]
        public IActionResult sendEmailSES()
        {
            
            using (var client = new AmazonSimpleEmailServiceClient(_awsAccessKey,_awsSecretKey, RegionEndpoint.USWest2))
            {
                var sendRequest = new SendEmailRequest
                {
                    Source = senderAddress,
                    Destination = new Destination { ToAddresses = new List<string> { receiverAddress } },
                    Message = new Message
                    {
                        Subject = new Content("Sample Mail using SES"),
                        Body = new Body { Text = new Content("Sample message content.") }
                    }
                };
                try
                {
                    //Console.WriteLine("Sending email using AWS SES...");
                    var response = client.SendEmailAsync(sendRequest);
                    return Ok(new
                    {
                        message = "The email was sent successfully."
                    });
                    //Console.WriteLine("The email was sent successfully.");
                }
                catch (Exception ex)
                {
                   
                    return BadRequest(new
                    {
                        message = "Error message: " + ex.Message
                    });

                }
            }

        }


    }
}



