using Microsoft.AspNetCore.Mvc;
using System;
using Microsoft.AspNetCore.Hosting;
using kmbi_core_master.helpers.interfaces;

using OfficeOpenXml;
using OfficeOpenXml.Style;
using kmbi_core_master.reports.Models;

using kmbi_core_master.reports.IRepository;

using Microsoft.AspNetCore.Cors;
using System.Collections.Generic;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.hrModule.Models;
using kmbi_core_master.mfModule.Models;
using System.Linq;

using kmbi_core_master.documentationModule.Service;
using kmbi_core_master.documentationModule.Models;
using Microsoft.Extensions.Options;
using kmbi_core_master.documentationModule.iRepositories;
using System.Runtime.InteropServices;
using System.Dynamic;
using kmbi_core_master.acctngModule.Interface;
using System.Drawing;
using kmbi_core_master.acctngModule.Models;
using kmbi_core_master.payrollModule.IRepository;
using Stimulsoft.Report;
using Stimulsoft.Report.Mvc;
using Newtonsoft.Json;
using Stimulsoft.Report.Dictionary;
using System.Data;
using Microsoft.AspNetCore.Mvc.Filters;
using kmbi_core_master.coreMaster.Filters;
using kmbi_core_master.coreMaster.Repository;
using kmbi_core_master.reports.UseCases;
using kmbi_core_master.reports.UseCases.GenerateLrCbuSchedule;
using kmbi_core_master.reports.UseCases.GenerateResignedSummary;
using kmbi_core_master.reports.UseCases.GeneratePPIData;
using kmbi_core_master.reports.UseCases.GenerateTINIssuanceRepot;
using UseCases.GenerateLeavesForPayroll;
using UseCases.GenerateDtrForPayroll;
using UseCases.GenerateOtForPayroll;
using kmbi_core_master.coreMaster;
using System.IO;
using Stimulsoft.Report.Components;
using System.Text;

namespace kmbi_core_master.reports.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/reports")]
    public class reportsController : Controller
    {
     

        private icutOffRepository _cutoff;

        IHostingEnvironment _env;
        iReport _rep;
        iupload _upload;
        ireporting _reporting;
        iusersRepository _users;
        ibranchRepository _branch;
        private icoaRepository _coa;
        private icashReceiptRepository _cr;
        private iacctngCvRepository _acv;
        private iacctngJvRepository _ajv;
        String root;
        String relativeTemplatePath;
        private iAccountingShared _acc;
        private ipayrollRepository _payroll;
        private iloanRepository _loanRep;
        private iclientsRepository _clientRepository { get; }
        public IUseCase<GenerateLrCbuScheduleRequest, GenerateLrCbuScheduleResponse> _generateLrCbuSchedule { get; }
        public IPresent<GenerateLrCbuScheduleResponse, GenerateLrCbuShedulePresenterResponse> _presentGenerateLrCbuSchedule { get; }
        public IUseCase<GenerateResignedSummaryRequest, GenerateResignedSummaryResponse> _generateResignedSummary { get; }
        public IPresent<GenerateResignedSummaryResponse, GenerateResignedSummaryPresenterResponse> _presentGenerateResignedSummary { get; }
        public IUseCase<GeneratePPIDataRequest, GeneratePPIDataResponse> _generatePPIData { get; }
        public IPresent<GeneratePPIDataResponse, GeneratePPIDataPresenterResponse> _presentGeneratePPIData { get; }
        public IPresent<GeneratePPIDataResponse, GeneratePPIDatav2PresenterResponse> _presentGeneratePPIDatav2 { get; }
        public IUseCase<GenerateDtrForPayrollUseCaseRequest, GenerateDtrForPayrollUseCaseResponse> _generateDtrForPayroll { get; }
        public IUseCase<GenerateLeavesForPayrollUseCaseRequest, GenerateLeavesForPayrollUseCaseResponse> _generateLeavesForPayroll { get; }
        public IUseCase<GenerateOtForPayrollUseCaseRequest, GenerateOtForPayrollUseCaseResponse> _generateOtForPayroll { get; }
        public IOptions<reportSetting> _reportSetting { get; }
        public IUseCase<GenerateTINIssuanceReportRequest, GenerateTINIssuanceReportResponse> _generateTinIssuanceReport { get; }
        public IPresent<GenerateTINIssuanceReportResponse, GenerateTINIssuanceReportPresenterResponse> _presentTinIssuanceReport { get; }

        public reportsController(
            icutOffRepository cutoff,
            IHostingEnvironment env,
            iReport rep,
            ireporting reporting,
            iupload upload,
            iusersRepository users,
            ibranchRepository branch,
            icashReceiptRepository cr, icoaRepository coa, iacctngCvRepository acv, iacctngJvRepository ajv,
            iAccountingShared acc,
            ipayrollRepository payroll,
            iloanRepository loanRep,
            iclientsRepository clientRepository,
            IOptions<reportSetting> reportSetting,
            IUseCase<GenerateLrCbuScheduleRequest, GenerateLrCbuScheduleResponse> generateLrCbuSchedule,
            IPresent<GenerateLrCbuScheduleResponse, GenerateLrCbuShedulePresenterResponse> presentGenerateLrCbuSchedule,
            IUseCase<GenerateResignedSummaryRequest, GenerateResignedSummaryResponse> generateResignedSummary,
            IPresent<GenerateResignedSummaryResponse, GenerateResignedSummaryPresenterResponse> presentGenerateResignedSummary,
            IUseCase<GeneratePPIDataRequest, GeneratePPIDataResponse> generatePPIData,
            IPresent<GeneratePPIDataResponse, GeneratePPIDataPresenterResponse> presentGeneratePPIData,
            IUseCase<GenerateTINIssuanceReportRequest, GenerateTINIssuanceReportResponse> generateTinIssuanceReport,
            IPresent<GenerateTINIssuanceReportResponse, GenerateTINIssuanceReportPresenterResponse> presentTinIssuanceReport,
            IPresent<GeneratePPIDataResponse, GeneratePPIDatav2PresenterResponse> presentGeneratePPIDatav2,
            IUseCase<GenerateDtrForPayrollUseCaseRequest, GenerateDtrForPayrollUseCaseResponse> generateDtrForPayroll,
            IUseCase<GenerateLeavesForPayrollUseCaseRequest, GenerateLeavesForPayrollUseCaseResponse> generateLeavesForPayroll,
            IUseCase<GenerateOtForPayrollUseCaseRequest, GenerateOtForPayrollUseCaseResponse> generateOtForPayroll
        )
        {
            _cutoff = cutoff;
            _env = env;
            _rep = rep;
            _upload = upload;
            _reporting = reporting;
            _users = users;
            _branch = branch;
            
            _coa = coa;
            _cr = cr;
            _acv = acv;
            _ajv = ajv;

            _acc = acc;

            root = _upload.getRoot();

            relativeTemplatePath = "/wwwroot/templates/";
            _rep.templatePath = env.ContentRootPath + relativeTemplatePath;
            if(env.IsProduction() || env.IsStaging() || env.IsEnvironment("Offline"))
            {
                if(RuntimeInformation.IsOSPlatform(OSPlatform.Linux) || RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
                    _rep.templatePath ="/var/www/aspnetcore/" + relativeTemplatePath;
            }

            _payroll = payroll;
            _loanRep = loanRep;
            _clientRepository = clientRepository;
            _generateLrCbuSchedule = generateLrCbuSchedule;
            _presentGenerateLrCbuSchedule = presentGenerateLrCbuSchedule;
            _generateResignedSummary = generateResignedSummary;
            _presentGenerateResignedSummary = presentGenerateResignedSummary;
            _generatePPIData = generatePPIData;
            _presentGeneratePPIData = presentGeneratePPIData;
            _generateTinIssuanceReport = generateTinIssuanceReport;
            _presentTinIssuanceReport = presentTinIssuanceReport;
            _presentGeneratePPIDatav2 = presentGeneratePPIDatav2;
            _generateDtrForPayroll = generateDtrForPayroll;
            _generateLeavesForPayroll = generateLeavesForPayroll;
            _generateOtForPayroll = generateOtForPayroll;
            _reportSetting = reportSetting;
        }

        public string getDataPath(string path)
        {
            var rootPath = _env.ContentRootPath + "/reports/Stimulsoft/Data/" + path + ".json";
            if (_env.IsProduction() || _env.IsStaging() || _env.IsEnvironment("Offline"))
            {
                if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux) || RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
                    rootPath = "/var/www/aspnetcore/reports/Stimulsoft/Data/" + path + ".json";
            }
            return rootPath;
        }

        public string getReportPath(string path)
        {
            var rootPath = _env.ContentRootPath + "/reports/Stimulsoft/Reports/" + path + ".mrt";
            if (_env.IsProduction() || _env.IsStaging() || _env.IsEnvironment("Offline"))
            {
                if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux) || RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
                    rootPath = "/var/www/aspnetcore/reports/Stimulsoft/Reports/" + path + ".mrt";
            }
            return rootPath;
        }

        private IActionResult generateReport(string repName, string data, string repDesc)
        {
            string dataPath = getDataPath(repName);
            System.IO.File.WriteAllText(dataPath, data);

            //Report Source
            string reportPath = getReportPath(repName);

            //Render Report
            var report = new StiReport();
            report.Load(reportPath);
            report.ReportName = repDesc;
            report.ReportAlias = repDesc;
            
            //report.Render();

            StiJsonDatabase db = new StiJsonDatabase(repName, dataPath);
            // db.PathData = path;

            report.Dictionary.Databases.Clear();
            report.Dictionary.Databases.Add(db);
            report.Dictionary.Connect(false);
            report.Dictionary.Synchronize();
            return StiNetCoreReportResponse.ResponseAsPdf(report);

        }

        private StiReport GetReport(string name)
        {
            string dataPath = getDataPath(name);
            string reportPath = getReportPath(name);
            var report = new StiReport();
            report.Load(_env.ContentRootPath + "/reports/Stimulsoft/Reports/" + name + ".mrt");
            //report.Load(reportPath);
            //report.ReportName = "ECR";
            //report.ReportAlias = "ECR";
            //report.RegData(dataSet);
            StiJsonDatabase db = new StiJsonDatabase(name, dataPath);
            // db.PathData = path;

            report.Dictionary.Databases.Clear();
            report.Dictionary.Databases.Add(db);
            report.Dictionary.Connect(false);
            report.Dictionary.Synchronize();

            report.Render(false);
            return report;
        }

        private void saveReport(string repName, string data, string repDesc)
        {
            string dataPath = getDataPath(repName);
            System.IO.File.WriteAllText(dataPath, data);

            //Report Source
            string reportPath = getReportPath(repName);

            StiReport report1 = GetReport(repName);

            //Render Report
            var report = new StiReport();
            report.Load(reportPath);
            report.ReportName = repDesc;
            report.ReportAlias = repDesc;

            report.Render(false);
            report.RenderedPages.Clear();
            //var rootPath = _env.ContentRootPath + "/wwwroot/kmbidevrepo/" + repName + ".mrt";
            foreach (StiPage page in report1.RenderedPages)
            {
                report.RenderedPages.Add(page);
            }


            //StiJsonDatabase db = new StiJsonDatabase(repName, dataPath);
            //// db.PathData = path;

            //report.Dictionary.Databases.Clear();
            //report.Dictionary.Databases.Add(db);
            //report.Dictionary.Connect(false);
            //report.Dictionary.Synchronize();

            var path = Directory.GetCurrentDirectory() + @"\wwwroot\kmbidevrepo\";

            report.ExportDocument(StiExportFormat.Pdf, path + repName +".pdf");
            //return StiNetCoreReportResponse. (report);

        }

        private IActionResult generateReportHtml(string repName, string data, string repDesc)
        {
            string dataPath = getDataPath(repName);
            System.IO.File.WriteAllText(dataPath, data);

            //Report Source
            string reportPath = getReportPath(repName);

            //Render Report
            var report = new StiReport();
            report.Load(reportPath);
            report.ReportName = repDesc;
            report.ReportAlias = repDesc;

            //report.Render();

            StiJsonDatabase db = new StiJsonDatabase(repName, dataPath);
            // db.PathData = path;

            report.Dictionary.Databases.Clear();
            report.Dictionary.Databases.Add(db);
            report.Dictionary.Connect(false);
            report.Dictionary.Synchronize();
            return StiNetCoreReportResponse.PrintAsHtml(report);

        }

        private IActionResult generateReportExcel(string repName, string data, string repDesc)
        {
            string dataPath = getDataPath(repName);
            System.IO.File.WriteAllText(dataPath, data);

            //Report Source
            string reportPath = getReportPath(repName);

            //Render Report
            var report = new StiReport();
            report.Load(reportPath);
            report.ReportName = repDesc;
            report.ReportAlias = repDesc;

            //report.Render();

            StiJsonDatabase db = new StiJsonDatabase(repName, dataPath);
            // db.PathData = path;

            report.Dictionary.Databases.Clear();
            report.Dictionary.Databases.Add(db);
            report.Dictionary.Connect(false);
            report.Dictionary.Synchronize();
            return StiNetCoreReportResponse.ResponseAsExcel2007(report);

        }

        [HttpGetAttribute("ecr")]
        public IActionResult Getecr([FromQueryAttribute]String refId="", [FromQueryAttribute]Int32 weekNo=1)
        {
            var __rep = _reporting.getEcr(refId,weekNo);

            var list = _loanRep.getByCenterLoanType(__rep.center.slug,"5897e52464443e05cac895cc","for-payment");

            var modelList = new List<repEcrModel>();
            list.ForEach(x=>{
                var sked = x.members[0].schedule.Where(y=>y.collectionDate == __rep.paymentSchedule).FirstOrDefault();
                if(sked!=null)
                    modelList.Add(_reporting.getEcr(x.referenceId,sked.weekNo));
            });

            var filnam = "";
            iResult res;
            
            var version = _reportSetting.Value.ecrBatchingVersion;

            if(modelList.Count == 1)
                version = 1;

            switch (version)
            {
                case 1: {
                    _rep.templateFilename = "ecr.cshtml";
                    _rep.model = modelList;
                    filnam = refId;
                    break;
                }
                case 2: {
                    var _model = _reporting.getEcrBatching(refId, weekNo);
                    _rep.model = _model;
                    _rep.templateFilename = "ecrBatching.cshtml";
                    filnam = String.Join(",", modelList.Select(x=>x.transactionNo));
                    break;
                }
                default: {
                    break;
                }
            }

      
            _rep.footerText = "System Generated Report - Microfinance System - " + DateTime.Now;
            _rep.title = "ECR";
            _rep.isHalf = false;
            _rep.orientation = "landscape";
            _rep.pageType ="legal";
            
            res = _rep.generatePDF();

                
       
            var link = _upload.upload(res.bytes,@"kmbidevreporeports/ecr-" + filnam + ".pdf");
            
            return Redirect(link);
        }

        [HttpGetAttribute("prospectiveClients")]
        public IActionResult GetprospectiveClients([FromQueryAttribute]String refId = "")
        {
            var rep = _reporting.getProspectiveClients(refId);

            _rep.templateFilename = "prospectiveClients.cshtml";
            _rep.model = rep;
            _rep.footerText = "System Generated Report - Microfinance System - " + DateTime.Now;
            _rep.title = "Prospective Clients";
            _rep.isHalf = false;
            _rep.orientation = "portrait";
            _rep.pageType ="legal";
            dynamic vb = new System.Dynamic.ExpandoObject();
            vb.root = root;
            _rep.viewBag = vb;

            iResult res = _rep.generatePDF();

            var link = _upload.upload(res.bytes,"kmbidevreporeports/prospectiveClients.pdf");
            return Redirect(link);
        }

        [HttpGetAttribute("summaryOfLoanApplication")]
        public IActionResult GetsummaryOfLoanApplication([FromQueryAttribute]String refId = "")
        {
            var rep = _reporting.getsummaryOfLoanApplicationv2(refId);
            
            var sigs = _reporting.GetSignatories("summary-of-loan-application",rep.center[0].branchSlug,rep.center[0].unitSlug,rep.center[0].officerSlug);

            rep.preparedBy = sigs.preparedBy;
            rep.checkedBy = sigs.checkedBy;
            rep.approvedBy = sigs.approvedBy;

            var data = JsonConvert.SerializeObject(rep);
            var repDesc = "Summary of Loan Application";
            return generateReport("sola", data, repDesc);
        }

        [HttpGetAttribute("upfrontFeeSchedule")]
        public IActionResult GetupfrontFeeSchedule([FromQueryAttribute]String refId = "")
        {
            
            var rep = _reporting.getupfrontFeeSchedule(refId);
            var sigs = _reporting.GetSignatories("upfront-fees",rep.center[0].branchSlug,rep.center[0].unitSlug,rep.center[0].officerSlug);

            rep.preparedBy = sigs.preparedBy;
            rep.checkedBy = sigs.checkedBy;
            rep.approvedBy = sigs.approvedBy;

            var data = JsonConvert.SerializeObject(rep);
            var repDesc = "Upfront Fees";
            return generateReport("upfrontFees", data, repDesc);
        }

        [HttpGetAttribute("weeklyPaymentSchedule")]
        public IActionResult GetweeklyPaymentSchedule( [FromQueryAttribute]String refId = "")
        {

            var rep = _reporting.getweeklyPaymentSchedule(refId);

            _rep.templateFilename = "weeklyPaymentSchedule.cshtml";
            _rep.model = rep;
            _rep.orientation = "portrait";
            _rep.footerText = "System Generated Report - Microfinance System - " + DateTime.Now;
            _rep.title = "Weekly Payment Schedule";
            _rep.pageType ="legal";
            _rep.isHalf = false;
            dynamic vb = new System.Dynamic.ExpandoObject();
            vb.root = root;
            _rep.viewBag = vb;
            iResult res = _rep.generatePDF();

            var link = _upload.upload(res.bytes,"kmbidevreporeports/" + rep.center[0].code + "-" + refId + ".pdf");
            return Redirect(link);
        }

        [HttpGetAttribute("documentaryStampTaxSummary")]
        public IActionResult GetdocumentaryStampTaxSummary([FromQueryAttribute]String branchId = "", [FromQueryAttribute]String month = "", [FromQueryAttribute]int? year = null)
        {
            var rep = _reporting.getDst(branchId, month, year);
            
            if(rep!=null) {
                _rep.templateFilename = "documentaryStampTaxSummary.cshtml";
                _rep.model = rep;
                _rep.footerText = "";
                _rep.title = "Documentary Stamp Tax (DST Summary)";
                _rep.isHalf = false;
                _rep.orientation = "portrait";
                _rep.pageType ="legal";

                dynamic vb = new System.Dynamic.ExpandoObject();
                vb.monthyear = month.ToUpper() + " " + year.ToString();
                _rep.viewBag = vb;

                iResult res = _rep.generatePDF();
                
                var link = _upload.upload(res.bytes,"kmbidevreporeports/documentaryStampTaxSummary" + "-" + branchId + "-" + month + year + ".pdf");
                return Redirect(link);
            } else {
                DateTime d;
                if(DateTime.TryParse(month + "/1/" + year, out d)) {
                    // return Ok("No Active loans for month of " + d.ToString("MMMM yyyy"));
                    return Ok("No disbursement for this month");
                } else {
                    return Ok("Invalid Parameters");
                }
            }

        }

        [HttpGetAttribute("rfp")]
        public IActionResult Getrfp([FromQueryAttribute]String refId = "")
        {
            var rep = _reporting.getrfpcv(refId);

            var sigs = _reporting.GetSignatories("rfp",rep.branch[0].slug,rep.center[0].unitSlug,rep.center[0].officerSlug);

            rep.preparedBy = sigs.preparedBy;
            rep.checkedBy = sigs.checkedBy;
            rep.approvedBy = sigs.approvedBy;

            var data = JsonConvert.SerializeObject(rep);
            var repDesc = "Request for Payment";
            return generateReport("rfp", data, repDesc);
        }

        [HttpGetAttribute("cv")]
        public IActionResult Getcv([FromQueryAttribute]String refId = "")
        {
            var rep = _reporting.getrfpcv(refId);

            var sigs = _reporting.GetSignatories("check-voucher",rep.branch[0].slug,rep.center[0].unitSlug,rep.center[0].officerSlug);

            rep.preparedBy = sigs.preparedBy;
            rep.checkedBy = sigs.checkedBy;
            rep.approvedBy = sigs.approvedBy;

            var data = JsonConvert.SerializeObject(rep);
            var repDesc = "Check Voucher";
            return generateReport("cv", data, repDesc);
        }

        [HttpGetAttribute("promisoryNote")]
        public IActionResult GetpromisoryNote([FromQueryAttribute]String refId = "")
        {
            var rep = _reporting.getpromisoryNote(refId);
            switch(rep.version) {
                case 1: {        
                    _rep.templateFilename = "promisoryNote.cshtml";
                    _rep.model = rep;
                    _rep.orientation = "portrait";
                    _rep.footerText = "System Generated Report - Microfinance System - " + DateTime.Now;
                    _rep.title = "Promissory Note";
                    _rep.isHalf = false;
                    _rep.pageType = "letter";
                    dynamic vb = new System.Dynamic.ExpandoObject();
                    vb.root = root;
                    _rep.viewBag = vb;
                    iResult res = _rep.generatePDF();

                    var link = _upload.upload(res.bytes, "kmbidevreporeports/promisorryNote" + " - " + rep.center[0].code + " - " + refId + ".pdf");
                    return Redirect(link);
                }
                case 2: {
                    var data = JsonConvert.SerializeObject(rep);
                    var repDesc = "Promissory Note";
                    return generateReport("promissoryNote", data, repDesc);
                }
                default: {
                    return Ok(new {
                        type = "failed",
                        message = "no version specified"
                    });
                }
            }
            
        }

        [HttpGetAttribute("disclosureStatement")]
        public IActionResult GetdisclosureStatement([FromQueryAttribute]String refId = "")
        {
            var rep = _reporting.getDisclosure(refId);

                _rep.templateFilename = "disclosureStatement.cshtml";
                _rep.model = rep;
                _rep.footerText = "System Generated Report - Microfinance System - " + DateTime.Now;
                _rep.title = "Disclosure Statement";
                _rep.isHalf = false;
                _rep.orientation = "portrait";
                _rep.pageType ="legal";
                iResult res = _rep.generatePDF();

            var link = _upload.upload(res.bytes,"kmbidevreporeports/disclosureStatement" + " - " + rep.center[0].code + " - " + refId + ".pdf");
            return Redirect(link);

        }

        [HttpGetAttribute("disclosureStatementSummary")]
        public IActionResult GetdisclosureStatementSummary([FromQueryAttribute]String refId = "")
        {
            var rep = _reporting.getDisclosureSummary(refId);
            
            var sigs = _reporting.GetSignatories("disclosure-summary",rep.center[0].branchSlug,rep.center[0].unitSlug,rep.center[0].officerSlug);

            rep.preparedBy = sigs.preparedBy;
            rep.checkedBy = sigs.checkedBy;
            rep.approvedBy = sigs.approvedBy;
            

            var data = JsonConvert.SerializeObject(rep);
            var repDesc = "Disclosure Statement Summary";
            return generateReport("disclosure", data, repDesc);

        }

        private void convertRepMiToSheet(repMiModel rep, ExcelWorksheet ew)
        {
            ew.View.FreezePanes(8, 5);
            ew.Cells["C2"].Value = "Branch & Unit: " + rep.branchName + " - " + rep.unitName;
            ew.Cells["C3"].Value = "Center Number: " + rep.centerCode;
            ew.Cells["C4"].Value = "Disbursement Date: " + rep.month;

            ew.Cells["A6:A7"].Merge = true;
            ew.Cells["A6:A7"].Value = "";

            ew.Cells["B6:B7"].Merge = true;
            ew.Cells["B6:B7"].Value = "Last Name";

            ew.Cells["C6:C7"].Merge = true;
            ew.Cells["C6:C7"].Value = "First Name";

            ew.Cells["D6:D7"].Merge = true;
            ew.Cells["D6:D7"].Value = "Middle Name";

            ew.Cells["E6:E7"].Merge = true;
            ew.Cells["E6:E7"].Value = "Principal Beneficiary";

            ew.Cells["F6:F7"].Merge = true;
            ew.Cells["F6:F7"].Value = "Principal Beneficiary Date of Birth";

            ew.Cells["G6:G7"].Merge = true;
            ew.Cells["G6:G7"].Value = "Principal Beneficiary Age";

            ew.Cells["H6:H7"].Merge = true;
            ew.Cells["H6:H7"].Value = "Date Of Loan Release";

            ew.Cells["I6:I7"].Merge = true;
            ew.Cells["I6:I7"].Value = "Date of Loan Maturity";

            ew.Cells["J6:J7"].Merge = true;
            ew.Cells["J6:J7"].Value = "Loan Cycle";

            ew.Cells["K6:K7"].Merge = true;
            ew.Cells["K6:K7"].Value = "Member Date of Birth";

            ew.Cells["L6:L7"].Merge = true;
            ew.Cells["L6:L7"].Value = "Age as of Enrollment";

            ew.Cells["M6:M7"].Merge = true;
            ew.Cells["M6:M7"].Value = "Marital Status";

            ew.Cells["N6:N7"].Merge = true;
            ew.Cells["N6:N7"].Value = "Spouse Name";

            ew.Cells["O6:O7"].Merge = true;
            ew.Cells["O6:O7"].Value = "Spouse Date of Birth";

            ew.Cells["P6:P7"].Merge = true;
            ew.Cells["P6:P7"].Value = "Age as of Enrollment";

            ew.Cells["Q6:Q7"].Merge = true;
            ew.Cells["Q6:Q7"].Value = "Child 1 Name";

            ew.Cells["R6:R7"].Merge = true;
            ew.Cells["R6:R7"].Value = "Child 1 Date of Birth";

            ew.Cells["S6:S7"].Merge = true;
            ew.Cells["S6:S7"].Value = "Age as of Enrollment";

            ew.Cells["T6:T7"].Merge = true;
            ew.Cells["T6:T7"].Value = "Child 2 Name";

            ew.Cells["U6:U7"].Merge = true;
            ew.Cells["U6:U7"].Value = "Child 2 Date of Birth";

            ew.Cells["V6:V7"].Merge = true;
            ew.Cells["V6:V7"].Value = "Age as of Enrollment";

            ew.Cells["W6:W7"].Merge = true;
            ew.Cells["W6:W7"].Value = "Child 3 Name";

            ew.Cells["X6:X7"].Merge = true;
            ew.Cells["X6:X7"].Value = "Child 3 Date of Birth";

            ew.Cells["Y6:Y7"].Merge = true;
            ew.Cells["Y6:Y7"].Value = "Age as of Enrollment";

            ew.Cells["Z6:Z7"].Merge = true;
            ew.Cells["Z6:Z7"].Value = "Child 4 Name";

            ew.Cells["AA6:AA7"].Merge = true;
            ew.Cells["AA6:AA7"].Value = "Child 4 Date of Birth";

            ew.Cells["AB6:AB7"].Merge = true;
            ew.Cells["AB6:AB7"].Value = "Age as of Enrollment";

            ew.Cells["AC6:AC7"].Merge = true;
            ew.Cells["AC6:AC7"].Value = "Child 5 Name";

            ew.Cells["AD6:AD7"].Merge = true;
            ew.Cells["AD6:AD7"].Value = "Child 5 Date of Birth";

            ew.Cells["AE6:AE7"].Merge = true;
            ew.Cells["AE6:AE7"].Value = "Age as of Enrollment";

            ew.Cells["AF6:AF7"].Merge = true;
            ew.Cells["AF6:AF7"].Value = "Child 6 Name";

            ew.Cells["AG6:AG7"].Merge = true;
            ew.Cells["AG6:AG7"].Value = "Child 6 Date of Birth";

            ew.Cells["AH6:AH7"].Merge = true;
            ew.Cells["AH6:AH7"].Value = "Age as of Enrollment";

            ew.Cells["AI6:AI7"].Merge = true;
            ew.Cells["AI6:AI7"].Value = "Child 7 Name";

            ew.Cells["AJ6:AJ7"].Merge = true;
            ew.Cells["AJ6:AJ7"].Value = "Child 7 Date of Birth";

            ew.Cells["AK6:AK7"].Merge = true;
            ew.Cells["AK6:AK7"].Value = "Age as of Enrollment";

            ew.Cells["AL6:AL7"].Merge = true;
            ew.Cells["AL6:AL7"].Value = "Child 8 Name";

            ew.Cells["AM6:AM7"].Merge = true;
            ew.Cells["AM6:AM7"].Value = "Child 8 Date of Birth";

            ew.Cells["AN6:AN7"].Merge = true;
            ew.Cells["AN6:AN7"].Value = "Age as of Enrollment";

            ew.Cells["AO6:AO7"].Merge = true;
            ew.Cells["AO6:AO7"].Value = "Child 9 Name";

            ew.Cells["AP6:AP7"].Merge = true;
            ew.Cells["AP6:AP7"].Value = "Child 9 Date of Birth";

            ew.Cells["AQ6:AQ7"].Merge = true;
            ew.Cells["AQ6:AQ7"].Value = "Age as of Enrollment";

            ew.Cells["AR6:AR7"].Merge = true;
            ew.Cells["AR6:AR7"].Value = "Child 10 Name";

            ew.Cells["AS6:AS7"].Merge = true;
            ew.Cells["AS6:AS7"].Value = "Child 10 Date of Birth";

            ew.Cells["AT6:AT7"].Merge = true;
            ew.Cells["AT6:AT7"].Value = "Age as of Enrollment";

            ew.Cells["AU6:AU7"].Merge = true;
            ew.Cells["AU6:AU7"].Value = "Parent 1 Name";

            ew.Cells["AV6:AV7"].Merge = true;
            ew.Cells["AV6:AV7"].Value = "Parent 1 Date of Birth";

            ew.Cells["AW6:AW7"].Merge = true;
            ew.Cells["AW6:AW7"].Value = "Age as of Enrollment";

            ew.Cells["AX6:AX7"].Merge = true;
            ew.Cells["AX6:AX7"].Value = "Parent 2 Name";

            ew.Cells["AY6:AY7"].Merge = true;
            ew.Cells["AY6:AY7"].Value = "Parent 2 Date of Birth";

            ew.Cells["AZ6:AZ7"].Merge = true;
            ew.Cells["AZ6:AZ7"].Value = "Age as of Enrollment";

            ew.Cells["BA6:BA7"].Merge = true;
            ew.Cells["BA6:BA7"].Value = "Sibling 1 Name";

            ew.Cells["BB6:BB7"].Merge = true;
            ew.Cells["BB6:BB7"].Value = "Sibling 1 Date of Birth";

            ew.Cells["BC6:BC7"].Merge = true;
            ew.Cells["BC6:BC7"].Value = "Age as of Enrollment";

            ew.Cells["BD6:BD7"].Merge = true;
            ew.Cells["BD6:BD7"].Value = "Sibling 2 Name";

            ew.Cells["BE6:BE7"].Merge = true;
            ew.Cells["BE6:BE7"].Value = "Sibling 2 Date of Birth";

            ew.Cells["BF6:BF7"].Merge = true;
            ew.Cells["BF6:BF7"].Value = "Age as of Enrollment";

            ew.Cells["BG6:BG7"].Merge = true;
            ew.Cells["BG6:BG7"].Value = "Sibling 3 Name";

            ew.Cells["BH6:BH7"].Merge = true;
            ew.Cells["BH6:BH7"].Value = "Sibling 3 Date of Birth";

            ew.Cells["BI6:BI7"].Merge = true;
            ew.Cells["BI6:BI7"].Value = "Age as of Enrollment";

            ew.Cells["BJ6:BJ7"].Merge = true;
            ew.Cells["BJ6:BJ7"].Value = "Sibling 4 Name";

            ew.Cells["BK6:BK7"].Merge = true;
            ew.Cells["BK6:BK7"].Value = "Sibling 4 Date of Birth";

            ew.Cells["BL6:BL7"].Merge = true;
            ew.Cells["BL6:BL7"].Value = "Age as of Enrollment";

            ew.Cells["BM6:BM7"].Merge = true;
            ew.Cells["BM6:BM7"].Value = "Sibling 5 Name";

            ew.Cells["BN6:BN7"].Merge = true;
            ew.Cells["BN6:BN7"].Value = "Sibling 5 Date of Birth";

            ew.Cells["BO6:BO7"].Merge = true;
            ew.Cells["BO6:BO7"].Value = "Age as of Enrollment";

            ew.Cells["BP6:BP7"].Merge = true;
            ew.Cells["BP6:BP7"].Value = "Client Count";

            ew.Cells["BQ6:BQ7"].Merge = true;
            ew.Cells["BQ6:BQ7"].Value = "Spouse Count";

            ew.Cells["BR6:BR7"].Merge = true;
            ew.Cells["BR6:BR7"].Value = "Child Count";

            ew.Cells["BS6:BS7"].Merge = true;
            ew.Cells["BS6:BS7"].Value = "Parent Count";

            ew.Cells["BT6:BT7"].Merge = true;
            ew.Cells["BT6:BT7"].Value = "Sibling Count";

            ew.Cells["BU6:BU7"].Merge = true;
            ew.Cells["BU6:BU7"].Value = "Premium for Firstlife";

            ew.Cells["BV6:BV7"].Merge = true;
            ew.Cells["BV6:BV7"].Value = "Personal Accident Insurance";

            ew.Cells["BW6:BW7"].Merge = true;
            ew.Cells["BW6:BW7"].Value = "Fee Retained by KMBI";

            ew.Cells["BX6:BX7"].Merge = true;
            ew.Cells["BX6:BX7"].Value = "Paid by Client";

            ew.Cells["A6:BX7"].Style.VerticalAlignment =  OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            ew.Cells["A6:BX7"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            ew.Cells["A6:BX7"].Style.WrapText = true;

            ew.Column(1).Width = 5;
            ew.Column(2).Width = 15;
            ew.Column(3).Width = 15;
            ew.Column(4).Width = 15;
            ew.Column(5).Width = 30;
            ew.Column(6).Width = 14;
            ew.Column(7).Width = 14;
            ew.Column(8).Width = 14;
            ew.Column(9).Width = 14;
            ew.Column(10).Width = 14;
            ew.Column(11).Width = 14;
            ew.Column(12).Width = 19;
            ew.Column(13).Width = 16;
            ew.Column(14).Width = 30;
            ew.Column(15).Width = 19;
            ew.Column(16).Width = 19;
            
            for(Int32 _ctr=0;_ctr<=16;_ctr++) {
                ew.Column(17 + (3 * _ctr)).Width = 30;
                ew.Column(18 + (3 * _ctr)).Width = 15;
                ew.Column(19 + (3 * _ctr)).Width = 15;
            }
            
            
            ew.Column(73).Width = 18;
            ew.Column(74).Width = 18;
            ew.Column(75).Width = 18;
            ew.Column(76).Width = 18;



            //lines

            var ctr = 8;

            var clientCountSum = 0;
            var spouseCountSum = 0;
            var childCountSum = 0;
            var parentCountSum = 0;
            var siblingCountSum = 0;
            var premiumForFirstLifeSum = 0.0;
            var personalAccidentSum = 0.0;
            var feeRetainedByKmbiSum = 0.0;
            var paidByClientSum = 0.0;

            foreach(repMiModelLine l in rep.lines)
            {
                ew.Cells["A" + ctr].Value = ctr - 7;
                ew.Cells["B" + ctr].Value = l.lastname;
                ew.Cells["C" + ctr].Value = l.firstname;
                ew.Cells["D" + ctr].Value = l.middlename;
                ew.Cells["E" + ctr].Value = l.principalBeneficiary;
                ew.Cells["F" + ctr].Value = l.principalBeneficiaryBirthdate!=Convert.ToDateTime("01/01/0001 12:00 AM") ? String.Format("{0:d}",@l.principalBeneficiaryBirthdate) : "";
                ew.Cells["G" + ctr].Value = l.principalBeneficiaryAge == 0 ? "" : l.principalBeneficiaryAge.ToString();
                ew.Cells["H" + ctr].Value = l.dateOfLoanRelease!=Convert.ToDateTime("01/01/0001 12:00 AM") ? String.Format("{0:d}",@l.dateOfLoanRelease) : "";
                ew.Cells["I" + ctr].Value = l.dateOfLoanMaturity!=Convert.ToDateTime("01/01/0001 12:00 AM") ? String.Format("{0:d}",@l.dateOfLoanMaturity) : "";
                ew.Cells["J" + ctr].Value = l.LoanCycle;
                ew.Cells["K" + ctr].Value = l.memberDateOfBirth!=Convert.ToDateTime("01/01/0001 12:00 AM") ? String.Format("{0:d}",@l.memberDateOfBirth) : "";
                ew.Cells["L" + ctr].Value = l.age == 0 ? "" : l.age.ToString();
                ew.Cells["M" + ctr].Value = l.maritalStatus;
                ew.Cells["N" + ctr].Value = l.spouseName;
                ew.Cells["O" + ctr].Value = l.spouseDateOfBirth!=new DateTime(1,1,1) ? @l.spouseDateOfBirth.ToString("MM/dd/yyyy") : "";
                ew.Cells["P" + ctr].Value = l.spouseAge == 0 ? "" : l.spouseAge.ToString();
                ew.Cells["Q" + ctr].Value = l.child1Name;
                ew.Cells["R" + ctr].Value = l.child1DateOfBirth!=Convert.ToDateTime("01/01/0001 12:00 AM") ? String.Format("{0:d}",@l.child1DateOfBirth) : "";
                ew.Cells["S" + ctr].Value = l.child1Age == 0 ? "" : l.child1Age.ToString();
                ew.Cells["T" + ctr].Value = l.child2Name;
                ew.Cells["U" + ctr].Value = l.child2DateOfBirth!=Convert.ToDateTime("01/01/0001 12:00 AM") ? String.Format("{0:d}",@l.child2DateOfBirth) : "";
                ew.Cells["V" + ctr].Value = l.child2Age == 0 ? "" : l.child2Age.ToString();
                ew.Cells["W" + ctr].Value = l.child3Name;
                ew.Cells["X" + ctr].Value = l.child3DateOfBirth!=Convert.ToDateTime("01/01/0001 12:00 AM") ? String.Format("{0:d}",@l.child3DateOfBirth) : "";
                ew.Cells["Y" + ctr].Value = l.child3Age == 0 ? "" : l.child3Age.ToString();
                ew.Cells["Z" + ctr].Value = l.child4Name;
                ew.Cells["AA" + ctr].Value = l.child4DateOfBirth!=Convert.ToDateTime("01/01/0001 12:00 AM") ? String.Format("{0:d}",@l.child4DateOfBirth) : "";
                ew.Cells["AB" + ctr].Value = l.child4Age == 0 ? "" : l.child4Age.ToString();
                ew.Cells["AC" + ctr].Value = l.child5Name;
                ew.Cells["AD" + ctr].Value = l.child5DateOfBirth!=Convert.ToDateTime("01/01/0001 12:00 AM") ? String.Format("{0:d}",@l.child5DateOfBirth) : "";
                ew.Cells["AE" + ctr].Value = l.child5Age == 0 ? "" : l.child5Age.ToString();
                ew.Cells["AF" + ctr].Value = l.child6Name;
                ew.Cells["AG" + ctr].Value = l.child6DateOfBirth!=Convert.ToDateTime("01/01/0001 12:00 AM") ? String.Format("{0:d}",@l.child6DateOfBirth) : "";
                ew.Cells["AH" + ctr].Value = l.child6Age == 0 ? "" : l.child6Age.ToString();
                ew.Cells["AI" + ctr].Value = l.child7Name;
                ew.Cells["AJ" + ctr].Value = l.child7DateOfBirth!=Convert.ToDateTime("01/01/0001 12:00 AM") ? String.Format("{0:d}",@l.child7DateOfBirth) : "";
                ew.Cells["AK" + ctr].Value = l.child7Age == 0 ? "" : l.child7Age.ToString();
                ew.Cells["AL" + ctr].Value = l.child8Name;
                ew.Cells["AM" + ctr].Value = l.child8DateOfBirth!=Convert.ToDateTime("01/01/0001 12:00 AM") ? String.Format("{0:d}",@l.child8DateOfBirth) : "";
                ew.Cells["AN" + ctr].Value = l.child8Age == 0 ? "" : l.child8Age.ToString();
                ew.Cells["AO" + ctr].Value = l.child9Name;
                ew.Cells["AP" + ctr].Value = l.child9DateOfBirth!=Convert.ToDateTime("01/01/0001 12:00 AM") ? String.Format("{0:d}",@l.child9DateOfBirth) : "";
                ew.Cells["AQ" + ctr].Value = l.child9Age == 0 ? "" : l.child9Age.ToString();
                ew.Cells["AR" + ctr].Value = l.child10Name;
                ew.Cells["AS" + ctr].Value = l.child10DateOfBirth!=Convert.ToDateTime("01/01/0001 12:00 AM") ? String.Format("{0:d}",@l.child10DateOfBirth) : "";
                ew.Cells["AT" + ctr].Value = l.child10Age == 0 ? "" : l.child10Age.ToString();
                ew.Cells["AU" + ctr].Value = l.parent1Name;
                ew.Cells["AV" + ctr].Value = l.parent1DateOfBirth!=Convert.ToDateTime("01/01/0001 12:00 AM") ? String.Format("{0:d}",@l.parent1DateOfBirth) : "";
                ew.Cells["AW" + ctr].Value = l.parent1Age == 0 ? "" : l.parent1Age.ToString();
                ew.Cells["AX" + ctr].Value = l.parent2Name;
                ew.Cells["AY" + ctr].Value = l.parent2DateOfBirth!=Convert.ToDateTime("01/01/0001 12:00 AM") ? String.Format("{0:d}",@l.parent2DateOfBirth) : "";
                ew.Cells["AZ" + ctr].Value = l.parent2Age == 0 ? "" : l.parent2Age.ToString();
                ew.Cells["BA" + ctr].Value = l.sibling1Name;
                ew.Cells["BB" + ctr].Value = l.sibling1DateOfBirth!=Convert.ToDateTime("01/01/0001 12:00 AM") ? String.Format("{0:d}",@l.sibling1DateOfBirth) : "";
                ew.Cells["BC" + ctr].Value = l.sibling1Age == 0 ? "" : l.sibling1Age.ToString();
                ew.Cells["BD" + ctr].Value = l.sibling2Name;
                ew.Cells["BE" + ctr].Value = l.sibling2DateOfBirth!=Convert.ToDateTime("01/01/0001 12:00 AM") ? String.Format("{0:d}",@l.sibling2DateOfBirth) : "";
                ew.Cells["BF" + ctr].Value = l.sibling2Age == 0 ? "" : l.sibling2Age.ToString();
                ew.Cells["BG" + ctr].Value = l.sibling3Name;
                ew.Cells["BH" + ctr].Value = l.sibling3DateOfBirth!=Convert.ToDateTime("01/01/0001 12:00 AM") ? String.Format("{0:d}",@l.sibling3DateOfBirth) : "";
                ew.Cells["BI" + ctr].Value = l.sibling3Age == 0 ? "" : l.sibling3Age.ToString();
                ew.Cells["BJ" + ctr].Value = l.sibling4Name;
                ew.Cells["BK" + ctr].Value = l.sibling4DateOfBirth!=Convert.ToDateTime("01/01/0001 12:00 AM") ? String.Format("{0:d}",@l.sibling4DateOfBirth) : "";
                ew.Cells["BL" + ctr].Value = l.sibling4Age == 0 ? "" : l.sibling4Age.ToString();
                ew.Cells["BM" + ctr].Value = l.sibling5Name;
                ew.Cells["BN" + ctr].Value = l.sibling5DateOfBirth!=Convert.ToDateTime("01/01/0001 12:00 AM") ? String.Format("{0:d}",@l.sibling5DateOfBirth) : "";
                ew.Cells["BO" + ctr].Value = l.sibling5Age == 0 ? "" : l.sibling5Age.ToString();
                ew.Cells["BP" + ctr].Value = l.clientCount;
                ew.Cells["BQ" + ctr].Value = l.spouseCount;
                ew.Cells["BR" + ctr].Value = l.childCount;
                ew.Cells["BS" + ctr].Value = l.parentCount;
                ew.Cells["BT" + ctr].Value = l.siblingCount;
                ew.Cells["BU" + ctr].Value = l.premiumForFirstLife;
                ew.Cells["BV" + ctr].Value = l.personalAccidentInsurance;
                ew.Cells["BW" + ctr].Value = l.feeRetainedByKmbi;
                ew.Cells["BX" + ctr].Value = l.paidByClient;

                clientCountSum += l.clientCount;
                spouseCountSum += l.spouseCount;
                childCountSum += l.childCount;
                parentCountSum += l.parentCount;
                siblingCountSum += l.siblingCount;
                premiumForFirstLifeSum += l.premiumForFirstLife;
                personalAccidentSum += l.personalAccidentInsurance;
                feeRetainedByKmbiSum += l.feeRetainedByKmbi;
                paidByClientSum += l.paidByClient;
                ctr++;
            }
            
            ew.Cells["BP" + ctr].Value = clientCountSum;
            ew.Cells["BQ" + ctr].Value = spouseCountSum;
            ew.Cells["BR" + ctr].Value = childCountSum;
            ew.Cells["BS" + ctr].Value = parentCountSum;
            ew.Cells["BT" + ctr].Value = siblingCountSum;
            ew.Cells["BU" + ctr].Value = premiumForFirstLifeSum;
            ew.Cells["BV" + ctr].Value = personalAccidentSum;
            ew.Cells["BW" + ctr].Value = feeRetainedByKmbiSum;
            ew.Cells["BX" + ctr].Value = paidByClientSum;

            ew.Cells["BU8:BX" + ctr.ToString()].Style.Numberformat.Format = "#,##0.00";
            
            ctr+=2;
            
            ew.Cells["E" + ctr.ToString()].Value = "Prepared by";
            ew.Cells["F" + ctr.ToString()].Value = "Checked by";
            ew.Cells["G" + ctr.ToString()].Value = "Approved by";

            ctr += 1;

            var sigs = _reporting.GetSignatories("mi-summary",rep.center.branchSlug,rep.center.unitSlug,rep.center.officerSlug);
                        
            ew.Cells["E" + ctr.ToString()].Value = sigs.preparedBy;
            ew.Cells["F" + ctr.ToString()].Value = sigs.checkedBy;
            ew.Cells["G" + ctr.ToString()].Value = sigs.approvedBy;

        }

        [HttpGetAttribute("mi")]
        public IActionResult GetMi([FromQueryAttribute]String refId="", [FromQueryAttribute]Int32 month=0, [FromQueryAttribute]Int32 year=0, [FromQueryAttribute]String branchSlug="")
        {

            if(refId!="" && month + year == 0 && branchSlug=="")
            {
                var rep = _reporting.getMiPerReferenceId(refId);
                var filename = "kmbidevreporeports/mi-" + refId + "-" + rep.centerCode + "-" + DateTime.Now.ToString("yyyyMMddHHmmssffff") + ".xlsx";
                OfficeOpenXml.ExcelPackage ep = new OfficeOpenXml.ExcelPackage();
                ExcelWorksheet ew = ep.Workbook.Worksheets.Add(refId);
                convertRepMiToSheet(rep,ew);
                byte[] bytes = ep.GetAsByteArray();
                ep.Dispose();
                var link = _upload.upload(bytes,filename);
                return Redirect(link);
            }
            else if(refId=="" && month > 0 && year > 0 && branchSlug != "")
            {
                List<repMiModel> rep2 = _reporting.getMiPerMonthYearPerBranch(month, year, branchSlug);
                var filename = "kmbidevreporeports/mi-" + month.ToString() + "-" + year.ToString() + "-" + branchSlug + "-" + DateTime.Now.ToString("yyyyMMddHHmmssffff") + ".xlsx";
                byte[] bytes;
                if (rep2.Count > 0)
                {
                    using (OfficeOpenXml.ExcelPackage ep = new OfficeOpenXml.ExcelPackage())
                    {
                        int ctr = 1;
                        foreach(repMiModel rm in rep2)
                        {
                            //ExcelWorksheet ew = ep.Workbook.Worksheets.Add(rm.centerCode);
                            ExcelWorksheet ew = ep.Workbook.Worksheets.Add(ctr.ToString());
                            convertRepMiToSheet(rm,ew);
                            ctr++;
                        }

                        ExcelWorksheet sum = ep.Workbook.Worksheets.Add("MI Summary");
                        sum.View.FreezePanes(10, 3);

                        sum.Cells["C1"].Value = "SUMMARY OF PREMIUMS";

                        sum.Cells["C3"].Value = "Branch:";
                        sum.Cells["C4"].Value = "For the month of:";

                        sum.Cells["B9"].Value = "Date of Loan Release";
                        sum.Cells["C9"].Value = "Date of Loan Maturity";
                        sum.Cells["D9"].Value = "Center Number";
                        sum.Cells["E9"].Value = "Client Count";
                        sum.Cells["F9"].Value = "Spouse Count";
                        sum.Cells["G9"].Value = "Childrent Count";
                        sum.Cells["H9"].Value = "Parent Count";
                        sum.Cells["I9"].Value = "Sibling Count";
                        sum.Cells["J9"].Value = "Premiums for First Life";
                        sum.Cells["K9"].Value = "Personal Accident Insurances";
                        sum.Cells["L9"].Value = "Fees Retained by KMBI";
                        sum.Cells["M9"].Value = "Premiums Paid by Client";


                        sum.Cells["B9:N9"].Style.VerticalAlignment =  OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                        sum.Cells["B9:N9"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        sum.Cells["B9:N9"].Style.WrapText = true;

                        sum.Column(1).Width = 5;
                        sum.Column(2).Width = 13;
                        sum.Column(3).Width = 27;
                        sum.Column(4).Width = 18;
                        sum.Column(5).Width = 10;
                        sum.Column(6).Width = 10;
                        sum.Column(7).Width = 9;
                        sum.Column(8).Width = 9;
                        sum.Column(9).Width = 9;
                        sum.Column(10).Width = 15;
                        sum.Column(11).Width = 15;
                        sum.Column(12).Width = 15;
                        sum.Column(13).Width = 15;


                        Int32 sumctr = 10;
                        Int32 sumclientNo = 1;

                        String sumbranch = "";
                        String sumcenter = "";


                        Int32 ggtotalClientCount  = 0;
                        Int32 ggtotalSpouseCount  = 0;
                        Int32 ggtotalChildCount  = 0;
                        Int32 ggtotalParentCount  = 0;
                        Int32 ggtotalSiblingCount  = 0;
                        Double ggtotalPremiumForFirstlife  = 0;
                        Double ggtotalPersonalAccidentInsurance  = 0;
                        Double ggtotalFeeRetainedByKMBI  = 0;
                        Double ggtotalPaidByClient  = 0;

                        DateTime dateOfLoanRelease  = new DateTime();
                        DateTime dateOfLoanMaturity  = new DateTime();


                        foreach(repMiModel t in rep2)
                        {
                            Int32 gtotalClientCount  = 0;
                            Int32 gtotalSpouseCount  = 0;
                            Int32 gtotalChildCount  = 0;
                            Int32 gtotalParentCount  = 0;
                            Int32 gtotalSiblingCount  = 0;
                            Double gtotalPremiumForFirstlife  = 0;
                            Double gtotalPersonalAccidentInsurance  = 0;
                            Double gtotalFeeRetainedByKMBI  = 0;
                            Double gtotalPaidByClient  = 0;

                            foreach(repMiModelLine e in t.lines)
                            {
                                sumbranch = t.branchName;
                                sumcenter = t.centerCode;

                                dateOfLoanRelease = e.dateOfLoanRelease;
                                dateOfLoanMaturity = e.dateOfLoanMaturity;


                                gtotalClientCount += e.clientCount;
                                gtotalSpouseCount += e.spouseCount;
                                gtotalChildCount += e.childCount;
                                gtotalParentCount += e.parentCount;
                                gtotalSiblingCount += e.siblingCount;
                                gtotalPremiumForFirstlife += e.premiumForFirstLife;
                                gtotalPersonalAccidentInsurance += e.personalAccidentInsurance;
                                gtotalFeeRetainedByKMBI += e.feeRetainedByKmbi;
                                gtotalPaidByClient += e.paidByClient;

                                ggtotalClientCount += e.clientCount;
                                ggtotalSpouseCount += e.spouseCount;
                                ggtotalChildCount += e.childCount;
                                ggtotalParentCount += e.parentCount;
                                ggtotalSiblingCount += e.siblingCount;
                                ggtotalPremiumForFirstlife += e.premiumForFirstLife;
                                ggtotalPersonalAccidentInsurance += e.personalAccidentInsurance;
                                ggtotalFeeRetainedByKMBI += e.feeRetainedByKmbi;
                                ggtotalPaidByClient += e.paidByClient;
                            }

                            sum.Cells["D3"].Value = sumbranch;
                            sum.Cells["D4"].Value = DateTime.Parse(month + "/1/" + year).ToString("MMMM") + " " + year;

                            sum.Cells["A" + sumctr].Value = sumclientNo;
                            sum.Cells["B" + sumctr].Value = dateOfLoanRelease;
                            sum.Cells["C" + sumctr].Value = dateOfLoanMaturity;
                            sum.Cells["D" + sumctr].Value = sumcenter;

                            sum.Cells["E" + sumctr].Value = gtotalClientCount;
                            sum.Cells["F" + sumctr].Value = gtotalSpouseCount;
                            sum.Cells["G" + sumctr].Value = gtotalChildCount;
                            sum.Cells["H" + sumctr].Value = gtotalParentCount;
                            sum.Cells["I" + sumctr].Value = gtotalSiblingCount;
                            sum.Cells["J" + sumctr].Value = gtotalPremiumForFirstlife;
                            sum.Cells["K" + sumctr].Value = gtotalPersonalAccidentInsurance;
                            sum.Cells["L" + sumctr].Value = gtotalFeeRetainedByKMBI;
                            sum.Cells["M" + sumctr].Value = gtotalPaidByClient;

                            sumctr += 1;
                            sumclientNo += 1;

                        }

                        sum.Cells["B" + sumctr.ToString() + ":D" + sumctr.ToString()].Merge = true;
                        sum.Cells["B" + sumctr.ToString() + ":D" + sumctr.ToString()].Value = "TOTAL";
                        sum.Cells["B" + sumctr.ToString() + ":D" + sumctr.ToString()].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                        sum.Cells["E" + sumctr.ToString()].Value = ggtotalClientCount;
                        sum.Cells["F" + sumctr.ToString()].Value = ggtotalSpouseCount;
                        sum.Cells["G" + sumctr.ToString()].Value = ggtotalChildCount;
                        sum.Cells["H" + sumctr.ToString()].Value = ggtotalParentCount;
                        sum.Cells["I" + sumctr.ToString()].Value = ggtotalSiblingCount;
                        sum.Cells["J" + sumctr.ToString()].Value = ggtotalPremiumForFirstlife;
                        sum.Cells["K" + sumctr.ToString()].Value = ggtotalPersonalAccidentInsurance;
                        sum.Cells["L" + sumctr.ToString()].Value = ggtotalFeeRetainedByKMBI;
                        sum.Cells["M" + sumctr.ToString()].Value = ggtotalPaidByClient;


                        sum.Cells["B10:C" + sumctr.ToString()].Style.Numberformat.Format = "mm/d/yyyy";
                        sum.Cells["E10:I" + sumctr.ToString()].Style.Numberformat.Format = "0";
                        sum.Cells["J10:M" + sumctr.ToString()].Style.Numberformat.Format = "#,##0.00";

                        sumctr += 2;

                        sum.Cells["E" + sumctr.ToString()].Value = "Prepared by";
                        sum.Cells["F" + sumctr.ToString()].Value = "Checked by";
                        sum.Cells["G" + sumctr.ToString()].Value = "Approved by";

                        sumctr += 1;

                        
                        var sigs = _reporting.GetSignatories("mi-summary",rep2[0].center.branchSlug,rep2[0].center.unitSlug,rep2[0].center.officerSlug);
                        
                        sum.Cells["E" + sumctr.ToString()].Value = sigs.preparedBy;
                        sum.Cells["F" + sumctr.ToString()].Value = sigs.checkedBy;
                        sum.Cells["G" + sumctr.ToString()].Value = sigs.approvedBy;


                        bytes = ep.GetAsByteArray();
                        ep.Dispose();
                    }
                    var link = _upload.upload(bytes,filename);
                    return Redirect(link);
                }
                else
                {
                    // return Ok("No Active loans for month of " + Convert.ToDateTime(month.ToString() + "/1/" + year.ToString()).ToString("MMMM yyyy"));
                    return Ok("No disbursement for this month");
                }

            }
            else
            {
                return Ok("Invalid Parameter");
            }
        }

        [HttpGetAttribute("cic")]
        public IActionResult GetCic()
        {
            List<repCic> list = _reporting.getCic();
            var filename = "cic.xlsx";
            byte[] bytes;

            using (OfficeOpenXml.ExcelPackage ep = new OfficeOpenXml.ExcelPackage())
            {
                ExcelWorksheet ew = ep.Workbook.Worksheets.Add("Sample");

                ew.Cells["A1"].Value = "Record Type";
                ew.Cells["B1"].Value = "Provider Code";
                ew.Cells["C1"].Value = "File Reference Date";
                ew.Cells["D1"].Value = "Version";
                ew.Cells["E1"].Value = "Submission Type";
                ew.Cells["F1"].Value = "Provider Comments";

                ew.Cells["A2"].Value = "HD";
                ew.Cells["B2"].Value = "";
                ew.Cells["C2"].Value = "";
                ew.Cells["D2"].Value = "1.0";
                ew.Cells["E2"].Value = "0";
                ew.Cells["F2"].Value = "";

                ew.Cells["A3"].Value = "Record Type";
                ew.Cells["B3"].Value = "Provider Code";
                ew.Cells["C3"].Value = "Branch Code";
                ew.Cells["D3"].Value = "Subject Reference Date";
                ew.Cells["E3"].Value = "Provider Subject Code";
                ew.Cells["F3"].Value = "Title";
                ew.Cells["G3"].Value = "First Name";
                ew.Cells["H3"].Value = "Last Name";
                ew.Cells["I3"].Value = "Middle Name";

                ew.Cells["J3"].Value = "Suffix";
                ew.Cells["K3"].Value = "Nickname";


                ew.Cells["L3"].Value = "Previous Last Name";
                ew.Cells["M3"].Value = "Gender";
                ew.Cells["N3"].Value = "Date of Birth";
                ew.Cells["O3"].Value = "Place of Birth";
                ew.Cells["P3"].Value = "Country of Birth";
                ew.Cells["Q3"].Value = "Nationality";
                ew.Cells["R3"].Value = "Resident";
                ew.Cells["S3"].Value = "Civil Status";
                ew.Cells["T3"].Value = "Number of Dependents";
                ew.Cells["U3"].Value = "Car/s Owned";
                ew.Cells["V3"].Value = "Spouse First Name";
                ew.Cells["W3"].Value = "Spouse Last Name";
                ew.Cells["X3"].Value = "Spouse Middle Name";
                ew.Cells["Y3"].Value = "Mother's Maiden First Name";
                ew.Cells["Z3"].Value = "Mother's Maiden Last Name";
                ew.Cells["AA3"].Value = "Mother's Maiden Middle Name";
                ew.Cells["AB3"].Value = "Father's Maiden First Name";
                ew.Cells["AC3"].Value = "Father's Maiden Last Name";
                ew.Cells["AD3"].Value = "Father's Maiden Middle Name";
                ew.Cells["AE3"].Value = "Father's Suffix";
                ew.Cells["AF3"].Value = "Address 1 Address Type";
                ew.Cells["AG3"].Value = "Address 1 FullAddress";
                ew.Cells["AH3"].Value = "Address 1 StreetNo";
                ew.Cells["AI3"].Value = "Address 1 PostalCode";
                ew.Cells["AJ3"].Value = "Address 1 Subdivision";
                ew.Cells["AK3"].Value = "Address 1 Barangay";
                ew.Cells["AL3"].Value = "Address 1 City";
                ew.Cells["AM3"].Value = "Address 1 Province";
                ew.Cells["AN3"].Value = "Address 1 Country";
                ew.Cells["AO3"].Value = "Address 1 HouseOwner/Lessee";
                ew.Cells["AP3"].Value = "Address 1 Occupied Since";
                ew.Cells["AQ3"].Value = "Address 2 Address Type";
                ew.Cells["AR3"].Value = "Address 2 FullAddress";
                ew.Cells["AS3"].Value = "Address 2 StreetNo";
                ew.Cells["AT3"].Value = "Address 2 PostalCode";
                ew.Cells["AU3"].Value = "Address 2 Subdivision";
                ew.Cells["AV3"].Value = "Address 2 Barangay";
                ew.Cells["AW3"].Value = "Address 2 City";
                ew.Cells["AX3"].Value = "Address 2 Province";
                ew.Cells["AY3"].Value = "Address 2 Country";
                ew.Cells["AZ3"].Value = "Address 2 HouseOwner/Lessee";
                ew.Cells["BA3"].Value = "Address 2 Occupied Since";
                ew.Cells["BB3"].Value = "Identification 1:Type";
                ew.Cells["BC3"].Value = "Identification 1:Number";
                ew.Cells["BD3"].Value = "Identification 2:Type";
                ew.Cells["BE3"].Value = "Identification 2:Number";
                ew.Cells["BF3"].Value = "Identification 3:Type";
                ew.Cells["BG3"].Value = "Identification 3:Number";
                ew.Cells["BH3"].Value = "ID 1:Type";
                ew.Cells["BI3"].Value = "ID 1:Number";
                ew.Cells["BJ3"].Value = "ID 1:IssueDate";
                ew.Cells["BK3"].Value = "ID 1:IssueCountry";
                ew.Cells["BL3"].Value = "ID 1:ExpiryDate";
                ew.Cells["BM3"].Value = "ID 1:Issued By";
                ew.Cells["BN3"].Value = "ID 1:IssueDate";
                ew.Cells["BO3"].Value = "ID 2:Type";
                ew.Cells["BP3"].Value = "ID 2:Number";
                ew.Cells["BQ3"].Value = "ID 2:IssueDate";
                ew.Cells["BR3"].Value = "ID 2:IssueCountry";
                ew.Cells["BS3"].Value = "ID 2:ExpiryDate";
                ew.Cells["BT3"].Value = "ID 2:Issued By";
                ew.Cells["BU3"].Value = "ID 2:IssueDate";
                ew.Cells["BV3"].Value = "ID 3:Type";
                ew.Cells["BW3"].Value = "ID 3:Number";
                ew.Cells["BX3"].Value = "ID 3:IssueDate";
                ew.Cells["BY3"].Value = "ID 3:IssueCountry";
                ew.Cells["BZ3"].Value = "ID 3:ExpiryDate";
                ew.Cells["BX3"].Value = "ID 3:Issued By";
                ew.Cells["BY3"].Value = "ID 3:IssueDate";
                ew.Cells["BZ3"].Value = "Contact 1:Type";
                ew.Cells["CA3"].Value = "Contact 1:Value";
                ew.Cells["CB3"].Value = "Contact 2:Type";
                ew.Cells["CC3"].Value = "Contact 2:Value";
                ew.Cells["CD3"].Value = "Employment: Trade Name";
                ew.Cells["CE3"].Value = "Employment: TIN";
                ew.Cells["CF3"].Value = "Employment: Phone Number";
                ew.Cells["CG3"].Value = "Employment: PSIC";
                ew.Cells["CH3"].Value = "Employment: GrossIncome";
                ew.Cells["CI3"].Value = "Employment: Annual/Monthly Indicator";
                ew.Cells["CJ3"].Value = "Employment: Currency";
                ew.Cells["CK3"].Value = "Employment: OccupationStatus";
                ew.Cells["CL3"].Value = "Employment: DateHiredFrom";
                ew.Cells["CM3"].Value = "Employment: DateHiredTo";
                ew.Cells["CN3"].Value = "Employment: Occupation";
                ew.Cells["CO3"].Value = "Sole Trader: TradeName";
                ew.Cells["CP3"].Value = "Sole Trader 1: AddressType";
                ew.Cells["CQ3"].Value = "Sole Trader 1: FullAddress";
                ew.Cells["CR3"].Value = "Sole Trader 1: StreetNo";
                ew.Cells["CS3"].Value = "Sole Trader 1: PostalCode";
                ew.Cells["CT3"].Value = "Sole Trader 1: Subdivision";
                ew.Cells["CU3"].Value = "Sole Trader 1 Barangay";
                ew.Cells["CV3"].Value = "Sole Trader 1 City";
                ew.Cells["CW3"].Value = "Sole Trader 1 Province";
                ew.Cells["CX3"].Value = "Sole Trader 1 Country";
                ew.Cells["CY3"].Value = "Sole Trader 1 HouseOwner/Lessee";
                ew.Cells["CZ3"].Value = "Sole Trader 1 Occupied Since";
                ew.Cells["DA3"].Value = "Sole Trader 2: AddressType";
                ew.Cells["DB3"].Value = "Sole Trader 2: FullAddress";
                ew.Cells["DC3"].Value = "Sole Trader 2: StreetNo";
                ew.Cells["DD3"].Value = "Sole Trader 2: PostalCode";
                ew.Cells["DE3"].Value = "Sole Trader 2: Subdivision";
                ew.Cells["DF3"].Value = "Sole Trader 2 Barangay";
                ew.Cells["DG3"].Value = "Sole Trader 2 City";
                ew.Cells["DH3"].Value = "Sole Trader 2 Province";
                ew.Cells["DI3"].Value = "Sole Trader 2 Country";
                ew.Cells["DJ3"].Value = "Sole Trader 2 HouseOwner/Lessee";
                ew.Cells["DK3"].Value = "Sole Trader 2 Occupied Since";
                ew.Cells["DL3"].Value = "Sole Trader 1 Identification Type";
                ew.Cells["DM3"].Value = "Sole Trader 1 Identification Number";
                ew.Cells["DN3"].Value = "Sole Trader 2 Identification Type";
                ew.Cells["DO3"].Value = "Sole Trader 2 Identification Number";
                ew.Cells["DP3"].Value = "Sole Trader 1 Contact Type";
                ew.Cells["DQ3"].Value = "Sole Trader 1 Contact Value";
                ew.Cells["DR3"].Value = "Sole Trader 2 Contact Type";
                ew.Cells["DS3"].Value = "Sole Trader 2 Contact Value";

                Int32 ctr = 3;
                foreach(repCic cic in list)
                {
                    ctr++;
                    ew.Cells["C" + ctr.ToString()].Value = cic.branchCode;

                    // Title
                    // 10 Mr
                    // 11 Ms
                    // 12 Miss
                    // 13 Mrs
                    // 14 Dr
                    // 15 Prof
                    // 16 Hon
                    // 17 Lady
                    // 18 Major
                    // 19 Sir
                    // 20 Madam

                    String titleCode = "";
                    if (cic.title!=null)
                    {
                        switch(cic.title.ToLower())
                        {
                            case "mr":
                                titleCode = "10";
                                break;
                            case "ms":
                                titleCode = "11";
                                break;
                            case "miss":
                                titleCode = "12";
                                break;
                            case "mrs":
                                titleCode = "13";
                                break;
                            case "dr":
                                titleCode = "14";
                                break;
                            case "prof":
                                titleCode = "15";
                                break;
                            case "hon":
                                titleCode = "16";
                                break;
                            case "lady":
                                titleCode = "17";
                                break;
                            case "major":
                                titleCode = "18";
                                break;
                            case "sir":
                                titleCode = "19";
                                break;
                            case "madam":
                                titleCode = "20";
                                break;
                        }
                    }


                    ew.Cells["F" + ctr.ToString()].Value = titleCode;

                    ew.Cells["G" + ctr.ToString()].Value = cic.name.first;
                    ew.Cells["H" + ctr.ToString()].Value = cic.name.last;
                    ew.Cells["I" + ctr.ToString()].Value = cic.name.middle;

                    // Gender
                    // M = Male
                    // F = Female

                    if(cic.gender!=null)
                    {
                        ew.Cells["M" + ctr.ToString()].Value = cic.gender.Substring(0,1).ToUpper();
                    }

                    if(cic.birthdate!=null)
                    {
                        ew.Cells["N" + ctr.ToString()].Value = String.Format("{0:MM/dd/yyyy}",cic.birthdate);
                    }

                    // 1 = Single
                    // 2 = Married
                    // 3 = Divorced/Separated
                    // 4 = Widow

                    String csCode = "";
                    if(cic.civilStatus!=null)
                    {
                        switch(cic.civilStatus.ToLower())
                        {
                            case "single":
                                csCode = "1";
                                break;
                            case "married":
                                csCode = "2";
                                break;
                            case "divorced":
                                csCode = "3";
                                break;
                            case "separated":
                                csCode = "4";
                                break;
                        }
                    }

                    ew.Cells["S" + ctr.ToString()].Value = csCode;

                    ew.Cells["T" + ctr.ToString()].Value = cic.dependents;
                    if(cic.spouse!=null)
                    {
                        ew.Cells["V" + ctr.ToString()].Value = cic.spouse.name.first;
                        ew.Cells["W" + ctr.ToString()].Value = cic.spouse.name.last;
                        ew.Cells["X" + ctr.ToString()].Value = cic.spouse.name.middle;
                    }


                    if(cic.addresses!=null)
                    {
                        Int32 aCount = 0;
                        foreach(kmbi_core_master.coreMaster.Models.clientAddress a in cic.addresses)
                        {
                            if(aCount==0)
                            {
                                ew.Cells["AF" + ctr.ToString()].Value = "MI";
                                ew.Cells["AG" + ctr.ToString()].Value = a.full;
                                ew.Cells["AH" + ctr.ToString()].Value = a.street;
                                ew.Cells["AI" + ctr.ToString()].Value = a.postal;
                                ew.Cells["AJ" + ctr.ToString()].Value = a.subdivision;
                                ew.Cells["AK" + ctr.ToString()].Value = a.barangay.name;
                                ew.Cells["AL" + ctr.ToString()].Value = a.municipality.name;
                                ew.Cells["AM" + ctr.ToString()].Value = a.province.name;
                                ew.Cells["AN" + ctr.ToString()].Value = "Philippines";
                            }
                            else if(aCount==1)
                            {
                                ew.Cells["AQ" + ctr.ToString()].Value = "AI";
                                ew.Cells["AR" + ctr.ToString()].Value = a.full;
                                ew.Cells["AS" + ctr.ToString()].Value = a.street;
                                ew.Cells["AT" + ctr.ToString()].Value = a.postal;
                                ew.Cells["AU" + ctr.ToString()].Value = a.subdivision;
                                ew.Cells["AV" + ctr.ToString()].Value = a.barangay.name;
                                ew.Cells["AW" + ctr.ToString()].Value = a.municipality.name;
                                ew.Cells["AX" + ctr.ToString()].Value = a.province.name;
                                ew.Cells["AY" + ctr.ToString()].Value = "Philippines";
                            }
                            aCount++;
                        }
                    }

                    // Identification
                    // 10 = TIN
                    // 11 = SSS Card
                    // 12 = GSIS
                    // 13 = Philhealth Card
                    // 14 = Senior Citizen card
                    // 15 = UMID
                    // 16 = SEC registration number
                    // 17 = DTI registration number
                    // 18 = CDA registration number
                    // 19 = CooperativeId


                    //condition: if client id is not included in the required id types, client must not be included in the report

                    var idStatus1 = false;
                    var idStatus2 = false;
                    var idStatus3 = false;

                    if(cic.ids!=null)
                    {
                        Int32 iCount = 0;
                        foreach(kmbi_core_master.coreMaster.Models.id i in cic.ids)
                        {
                            String idCode = "";
                            if(i.type!=null)
                            {
                                switch(i.type.ToLower())
                                {
                                    case "tin":
                                        idCode = "10";
                                        break;
                                    case "sss card":
                                        idCode = "11";
                                        break;
                                    case "gsis":
                                        idCode = "12";
                                        break;
                                    case "philhealth card":
                                        idCode = "13";
                                        break;
                                    case "senior citizen card":
                                        idCode = "14";
                                        break;
                                    case "umid":
                                        idCode = "15";
                                        break;
                                    case "sec registration number":
                                        idCode = "16";
                                        break;
                                    case "dti registration number":
                                        idCode = "17";
                                        break;
                                    case "cda registration number":
                                        idCode = "18";
                                        break;
                                    case "ctc":
                                        idCode = "19";
                                        break;
                                    case "voter's id":
                                        idCode = "19";
                                        break;
                                    case "cooperativeid":
                                        idCode = "19";
                                        break;
                                }
                            }

                            if(iCount==0)
                            {
                                ew.Cells["BB" + ctr.ToString()].Value = idCode;
                                ew.Cells["BC" + ctr.ToString()].Value = i.number;
                                idStatus1 = true;
                            }
                            else if(iCount==1)
                            {
                                ew.Cells["BD" + ctr.ToString()].Value = idCode;
                                ew.Cells["BE" + ctr.ToString()].Value = i.number;
                                idStatus2 = true;
                            }
                            else if(iCount==2)
                            {
                                ew.Cells["BF" + ctr.ToString()].Value = idCode;
                                ew.Cells["BG" + ctr.ToString()].Value = i.number;
                                idStatus3 = true;
                            }
                            iCount++;
                        }
                    }

                    // Contact
                    // 1 = Main phone
                    // 2 = Additional phone
                    // 3 = Mobile phone
                    // 4 = Additional Mobile phone
                    // 5 = Fax
                    // 6 = Additional Fax
                    // 7 = E-mail
                    // 8 = Additional e-mail
                    // 9 = Social Network Link



                    if(cic.contacts!=null)
                    {
                        Int32 cCount = 0;
                        foreach(kmbi_core_master.coreMaster.Models.clientContact c in cic.contacts)
                        {
                            String contactCode = "";
                            if(c.name!=null)
                            {
                                switch(c.name.ToLower())
                                {
                                    case "main phone":
                                        contactCode = "1";
                                        break;
                                    case "additional phone":
                                        contactCode = "2";
                                        break;
                                    case "mobile phone":
                                        contactCode = "3";
                                        break;
                                    case "additional mobile phone":
                                        contactCode = "4";
                                        break;
                                    case "fax":
                                        contactCode = "5";
                                        break;
                                    case "additional fax":
                                        contactCode = "6";
                                        break;
                                    case "e-mail":
                                        contactCode = "7";
                                        break;
                                    case "additional e-mail":
                                        contactCode = "8";
                                        break;
                                    case "social network link":
                                        contactCode = "9";
                                        break;
                                }
                            }

                            if(cCount==0)
                            {
                                ew.Cells["BZ" + ctr.ToString()].Value = contactCode;
                                ew.Cells["CA" + ctr.ToString()].Value = c.number;
                            }
                            else if(cCount==1)
                            {
                                ew.Cells["CB" + ctr.ToString()].Value = contactCode;
                                ew.Cells["CC" + ctr.ToString()].Value = c.number;
                            }
                        }
                    }

                    if(idStatus1 && idStatus2 && idStatus3) {
                        ew.DeleteRow(ctr);
                        ctr--;
                    }

                }

                bytes = ep.GetAsByteArray();
                ep.Dispose();

            }

            var link = _upload.upload(bytes,"kmbidevreporeports/" + filename);
            return Redirect(link);
        }

        [HttpGetAttribute("midas")]
        public IActionResult GetMidas([FromQueryAttribute]String type, [FromQuery]DateTime? cutoffDate, [FromQuery]string branchSlug)
        {
            String test;
            String errMess = "";
            if(cutoffDate==null) {

                errMess+="Invalid Cutoff date";
            }

            if(errMess!="") {
                return BadRequest(new {
                    type = "error",
                    message = errMess
                });
            }
            
            DateTime co = Convert.ToDateTime(cutoffDate);
            List<repMidas> list = _reporting.getMidas(co, branchSlug).Where(x=>x.members.memberId!="EXCLUDED").ToList();

            var filename = "midas_" + type.Substring(0,3) + "_" + co.Date.ToString("MMddyyyy") + ".xlsx";
            byte[] bytes;

            using (OfficeOpenXml.ExcelPackage ep = new OfficeOpenXml.ExcelPackage())
            {
                ExcelWorksheet ew = ep.Workbook.Worksheets.Add("Sheet1");

                switch(type)
                {
                    case "pod":
                        ew.Cells["A1"].Value = "MIDAS PODs"; //good
                        ew.Cells["A2"].Value = "Institution";
                        ew.Cells["B2"].Value = "KMBI";
                        ew.Cells["A3"].Value = "Cut Off Date";
                        ew.Cells["B3"].Value = String.Format("{0:MM/dd/yyyy}",co);
                        ew.Cells["A4"].Value = "No. Of Clients";
                        ew.Cells["B4"].Value = list.Where(x=>x.members.health == 1).ToList().Count;
                        ew.Cells["A5"].Value = "BEGIN";
                        ew.Cells["A6"].Value = "CLIENT_REFERENCE";
                        ew.Cells["B6"].Value = "LAST_NAME";
                        ew.Cells["C6"].Value = "FIRST_NAME";
                        ew.Cells["D6"].Value = "MIDDLE_NAME";
                        ew.Cells["E6"].Value = "NO_STREET_SITIO_PUROK";
                        ew.Cells["F6"].Value = "BARANGAY_DISTRICT";
                        ew.Cells["G6"].Value = "CITY_MUNICIPALITY";
                        ew.Cells["H6"].Value = "PROVINCE";
                        ew.Cells["I6"].Value = "ZIP_CODE";
                        ew.Cells["J6"].Value = "BIRTHDATE";
                        ew.Cells["K6"].Value = "CONTACT_NO";
                        ew.Cells["L6"].Value = "ID_TYPE";
                        ew.Cells["M6"].Value = "ID_NO";
                        ew.Cells["N6"].Value = "TIN";
                        ew.Cells["O6"].Value = "LOAN_REFERENCE";
                        ew.Cells["P6"].Value = "LOAN_PRINCIPAL";
                        ew.Cells["Q6"].Value = "LOAN_BALANCE";
                        ew.Cells["R6"].Value = "DATE_GRANTED";
                        ew.Cells["S6"].Value = "DATE_DUE";
                        ew.Cells["T6"].Value = "INTEREST_RATE";
                        ew.Cells["U6"].Value = "PAY_FREQ";
                        ew.Cells["V6"].Value = "TERM";
                        ew.Cells["W6"].Value = "CURRENCY";
                        ew.Cells["X6"].Value = "LOAN_PURPOSE";
                        ew.Cells["Y6"].Value = "POD_TYPE";
                        ew.Cells["Z6"].Value = "REMARKS";

                        Int32 ctr=7;
                        foreach(repMidas r in list)
                        {

                            //good loan
                            if(r.members.totalAmount == 0)
                            {
                                if (r.members.health == 1)
                                {
                                    ew.Cells["A" + ctr.ToString()].Value = r.members.memberId;
                                    ew.Cells["B" + ctr.ToString()].Value = r.members.name.last;
                                    ew.Cells["C" + ctr.ToString()].Value = r.members.name.first;
                                    ew.Cells["D" + ctr.ToString()].Value = r.members.name.middle;
                                    ew.Cells["E" + ctr.ToString()].Value = r.members.address.Split('|')[0];
                                    ew.Cells["F" + ctr.ToString()].Value = r.members.address.Split('|')[1];
                                    ew.Cells["G" + ctr.ToString()].Value = r.members.address.Split('|')[2];
                                    ew.Cells["H" + ctr.ToString()].Value = r.members.address.Split('|')[3];
                                    ew.Cells["I" + ctr.ToString()].Value = "";
                                    ew.Cells["J" + ctr.ToString()].Value = String.Format("{0:MM/dd/yyyy}", r.members.birthdate); ;
                                    ew.Cells["K" + ctr.ToString()].Value = r.members.position;
                                    ew.Cells["L" + ctr.ToString()].Value = r.members.issuedId.name;
                                    ew.Cells["M" + ctr.ToString()].Value = r.members.issuedId.number;
                                    ew.Cells["N" + ctr.ToString()].Value = r.members.beneficiary;
                                    ew.Cells["O" + ctr.ToString()].Value = r.referenceId;
                                    ew.Cells["P" + ctr.ToString()].Value = r.members.principalAmount + (r.members.principalAmount * r.loanType.interestRate);
                                    ew.Cells["Q" + ctr.ToString()].Value = r.members.totalAmount;
                                    ew.Cells["R" + ctr.ToString()].Value = String.Format("{0:MM/dd/yyyy}", r.disbursementDate.Value.Date.AddDays(7));
                                    ew.Cells["S" + ctr.ToString()].Value = String.Format("{0:MM/dd/yyyy}", r.disbursementDate.Value.Date.AddDays(7).AddMonths(6));
                                    ew.Cells["T" + ctr.ToString()].Value = r.loanType.interestRate;
                                    ew.Cells["U" + ctr.ToString()].Value = r.loanType.frequency;
                                    ew.Cells["V" + ctr.ToString()].Value = r.loanType.term;
                                    ew.Cells["W" + ctr.ToString()].Value = "PHP";
                                    ew.Cells["X" + ctr.ToString()].Value = "ET";
                                    ew.Cells["Y" + ctr.ToString()].Value = "50-01";
                                    ew.Cells["Z" + ctr.ToString()].Value = "";
                                    ctr++;
                                }
                            }
                           

                        }
                        ew.Cells["A" +ctr.ToString()].Value = "END";

                        break;
                    case "podv2":
                        ew.Cells["A1"].Value = "MIDAS PODs"; //good
                        ew.Cells["A2"].Value = "Institution";
                        ew.Cells["B2"].Value = "KMBI";
                        ew.Cells["A3"].Value = "Cut Off Date";
                        ew.Cells["B3"].Value = String.Format("{0:MM/dd/yyyy}",co);
                        ew.Cells["A4"].Value = "No. Of Clients";
                        ew.Cells["B4"].Value = list.Where(x=>x.members.health == 1).ToList().Count;
                        ew.Cells["A5"].Value = "BEGIN";
                        ew.Cells["A6"].Value = "CLIENT_REFERENCE";
                        ew.Cells["B6"].Value = "LAST_NAME";
                        ew.Cells["C6"].Value = "FIRST_NAME";
                        ew.Cells["D6"].Value = "MIDDLE_NAME";
                        ew.Cells["E6"].Value = "NO_STREET_SITIO_PUROK";
                        ew.Cells["F6"].Value = "BARANGAY_DISTRICT";
                        ew.Cells["G6"].Value = "CITY_MUNICIPALITY";
                        ew.Cells["H6"].Value = "PROVINCE";
                        ew.Cells["I6"].Value = "ZIP_CODE";
                        ew.Cells["J6"].Value = "BIRTHDATE";
                        ew.Cells["K6"].Value = "BIRTHPLACE";
                        ew.Cells["L6"].Value = "GENDER";
                        ew.Cells["M6"].Value = "CIVILSTATUS";
                        ew.Cells["N6"].Value = "CONTACT_NO";
                        ew.Cells["O6"].Value = "MOTHER'S MAIDEN FIRST NAME";
                        ew.Cells["P6"].Value = "MOTHER'S MAIDEN MIDDLE NAME";
                        ew.Cells["Q6"].Value = "MOTHER'S MAIDEN LAST NAME";
                        ew.Cells["R6"].Value = "ID_TYPE";
                        ew.Cells["S6"].Value = "ID_NO";
                        ew.Cells["T6"].Value = "SSS/GSIS";
                        ew.Cells["U6"].Value = "PAGIBIG";
                        ew.Cells["V6"].Value = "PHILHEALTH";
                        ew.Cells["W6"].Value = "TIN";
                        ew.Cells["X6"].Value = "LOAN_REFERENCE";
                        ew.Cells["Y6"].Value = "CONTRACT_TYPE";
                        ew.Cells["Z6"].Value = "CONTRACT_PHASE";
                        ew.Cells["AA6"].Value = "TRANSACTION_TYPE";
                        ew.Cells["AB6"].Value = "LOAN_PRINCIPAL";
                        ew.Cells["AC6"].Value = "LOAN_BALANCE";
                        ew.Cells["AD6"].Value = "DATE_GRANTED";
                        ew.Cells["AE6"].Value = "DATE_DUE";
                        ew.Cells["AF6"].Value = "INTEREST_RATE";
                        ew.Cells["AG6"].Value = "PAY_FREQ";
                        ew.Cells["AH6"].Value = "TERM";
                        ew.Cells["AI6"].Value = "CURRENCY";
                        ew.Cells["AJ6"].Value = "LOAN_PURPOSE";
                        ew.Cells["AK6"].Value = "POD_TYPE";
                        ew.Cells["AL6"].Value = "TOTAL_LOAN_BALANCE";
                        ew.Cells["AM6"].Value = "CONTRACT_ACTUAL_END_DATE";
                        ew.Cells["AN6"].Value = "OVERDUE_DAYS";
                        ew.Cells["AO6"].Value = "MONTHLY_PAYMENT_AMOUNT";
                        ew.Cells["AP6"].Value = "NO_OF_OUTSTANDING_PAYMENTS";
                        ew.Cells["AQ6"].Value = "AMOUNT_OF_LAST_PAYMENT";
                        ew.Cells["AR6"].Value = "REMARKS";

                        Int32 podv2ctr=7;
                        foreach(repMidas r in list)
                        {

                            //good loan
                            if(r.members.health == 1) {
                                ew.Cells["A" +podv2ctr.ToString()].Value = r.members.memberId; //CLIENT_REFERENCE
                                ew.Cells["B" +podv2ctr.ToString()].Value = r.members.name.last; //LAST_NAME
                                ew.Cells["C" +podv2ctr.ToString()].Value = r.members.name.first; //FIRST_NAME
                                ew.Cells["D" +podv2ctr.ToString()].Value = r.members.name.middle; //MIDDLE_NAME
                                ew.Cells["E" +podv2ctr.ToString()].Value = r.members.address.Split('|')[0]; //NO_STREET_SITIO_PUROK
                                ew.Cells["F" +podv2ctr.ToString()].Value = r.members.address.Split('|')[1]; //BARANGAY_DISTRICT
                                ew.Cells["G" +podv2ctr.ToString()].Value = r.members.address.Split('|')[2]; //CITY_MUNICIPALITY
                                ew.Cells["H" +podv2ctr.ToString()].Value = r.members.address.Split('|')[3]; //PROVINCE
                                ew.Cells["I" +podv2ctr.ToString()].Value = ""; //ZIP_CODE
                                ew.Cells["J" +podv2ctr.ToString()].Value = String.Format("{0:MM/dd/yyyy}",r.members.birthdate);; //BIRTHDATE
                                ew.Cells["K" + podv2ctr.ToString()].Value = String.Format("{0:MM/dd/yyyy}", r.lmaidenName.birthplace); ; //BIRTHDATE
                                ew.Cells["L" +podv2ctr.ToString()].Value = "F"; //GENDER
                                // ew.Cells["M" +podv2ctr.ToString()].Value = r.members.civilStatus;
                                if (r.members.civilStatus == "Single") {
                                        ew.Cells["M" +podv2ctr.ToString()].Value = "1";
                                }
                                if (r.members.civilStatus == "Married") {
                                        ew.Cells["M" +podv2ctr.ToString()].Value = "2";
                                }
                                if (r.members.civilStatus == "Divorced/Separated") {
                                        ew.Cells["M" +podv2ctr.ToString()].Value = "3";
                                }
                                if (r.members.civilStatus == "Widow") {
                                    ew.Cells["M" +podv2ctr.ToString()].Value = "4";
                                }
                                ew.Cells["N" +podv2ctr.ToString()].Value = r.members.position; //CONTACT_NO
                                ew.Cells["O" +podv2ctr.ToString()].Value = r.lmaidenName.maidenName.first; //MOTHER'S MAIDEN FIRST NAME
                                ew.Cells["P" +podv2ctr.ToString()].Value = r.lmaidenName.maidenName.middle; //MOTHER'S MAIDEN MIDDLE NAME
                                ew.Cells["Q" +podv2ctr.ToString()].Value = r.lmaidenName.maidenName.last; //MOTHER'S MAIDEN LAST NAME
                                ew.Cells["R" +podv2ctr.ToString()].Value = r.members.issuedId.name; //ID_TYPE
                                ew.Cells["S" +podv2ctr.ToString()].Value = r.members.issuedId.number; //ID_NO
                                ew.Cells["T" +podv2ctr.ToString()].Value = r.members.businessName.Split('|')[1]; //SSS/GSIS
                                ew.Cells["U" +podv2ctr.ToString()].Value = r.members.businessName.Split('|')[2]; //PAGIBIG
                                ew.Cells["V" +podv2ctr.ToString()].Value = r.members.businessName.Split('|')[3]; //PHILHEALTH
                                ew.Cells["W" +podv2ctr.ToString()].Value = r.members.beneficiary; //TIN
                                ew.Cells["X" +podv2ctr.ToString()].Value = r.referenceId; //LOAN_REFERENCE
                                ew.Cells["Y" +podv2ctr.ToString()].Value = "18"; //CONTRACT_TYPE
                                ew.Cells["Z" +podv2ctr.ToString()].Value = "AC"; //CONTRACT_PHASE
                                ew.Cells["AA" +podv2ctr.ToString()].Value = "NA"; //TRANSACTION_TYPE
                                ew.Cells["AB" +podv2ctr.ToString()].Value = r.members.principalAmount; //LOAN_PRINCIPAL
                                ew.Cells["AC" +podv2ctr.ToString()].Value = r.members.totalAmount; //LOAN_BALANCE
                                ew.Cells["AD" +podv2ctr.ToString()].Value = String.Format("{0:MM/dd/yyyy}",r.disbursementDate.Value.Date.AddDays(7)); //DATE_GRANTED
                                ew.Cells["AE" +podv2ctr.ToString()].Value = String.Format("{0:MM/dd/yyyy}",r.disbursementDate.Value.Date.AddDays(7).AddMonths(6)); //DATE_DUE
                                ew.Cells["AF" +podv2ctr.ToString()].Value = r.loanType.interestRate; //INTEREST_RATE
                                ew.Cells["AG" +podv2ctr.ToString()].Value = r.loanType.frequency; //PAY_FREQ
                                ew.Cells["AH" +podv2ctr.ToString()].Value = r.loanType.term; //TERM
                                ew.Cells["AI" +podv2ctr.ToString()].Value = "PHP"; //CURRENCY
                                ew.Cells["AJ" +podv2ctr.ToString()].Value = "ET"; //LOAN_PURPOSE
                                ew.Cells["AK" +podv2ctr.ToString()].Value = "50-01"; //POD_TYPE
                                ew.Cells["AL" +podv2ctr.ToString()].Value = r.members.principalAmount + (r.members.principalAmount * r.loanType.interestRate); //TOTAL_LOAN_BALANCE
                                ew.Cells["AM" +podv2ctr.ToString()].Value = String.Format("{0:MM/dd/yyyy}",r.disbursementDate.Value.Date.AddDays(7).AddMonths(6)); //CONTRACT_ACTUAL_END_DATE
                                ew.Cells["AN" +podv2ctr.ToString()].Value = Convert.ToDouble(r.members.purpose.code.Split('|')[0]); //OVERDUE_DAYS
                                ew.Cells["AO" +podv2ctr.ToString()].Value = Convert.ToDouble(r.members.purpose.code.Split('|')[1]); //MONTHLY_PAYMENT_AMOUNT
                                ew.Cells["AP" +podv2ctr.ToString()].Value = Convert.ToDouble(r.members.purpose.code.Split('|')[2]); //NO_OF_OUTSTANDING_PAYMENTS
                                ew.Cells["AQ" +podv2ctr.ToString()].Value = Convert.ToDouble(r.members.purpose.code.Split('|')[3]); //AMOUNT_OF_LAST_PAYMENT
                                ew.Cells["AR" +podv2ctr.ToString()].Value = ""; //REMARKS
                                podv2ctr++;
                            }

                        }
                        ew.Cells["A" +podv2ctr.ToString()].Value = "END";

                        break;
                    case "bar":
                        ew.Cells["A1"].Value = "MIDAS BARs"; //bad
                        ew.Cells["A2"].Value = "Institution";
                        ew.Cells["B2"].Value = "KMBI";
                        ew.Cells["A3"].Value = "Cut Off Date";
                        ew.Cells["B3"].Value = String.Format("{0:MM/dd/yyyy}",co);
                        ew.Cells["A4"].Value = "No. Of Clients";
                        ew.Cells["B4"].Value = list.Where(x=>x.members.health == 0).ToList().Count;
                        ew.Cells["A5"].Value = "BEGIN";
                        ew.Cells["A6"].Value = "CLIENT_REFERENCE";
                        ew.Cells["B6"].Value = "LAST_NAME";
                        ew.Cells["C6"].Value = "FIRST_NAME";
                        ew.Cells["D6"].Value = "MIDDLE_NAME";
                        ew.Cells["E6"].Value = "NO_STREET_SITIO_PUROK";
                        ew.Cells["F6"].Value = "BARANGAY_DISTRICT";
                        ew.Cells["G6"].Value = "CITY_MUNICIPALITY";
                        ew.Cells["H6"].Value = "PROVINCE";
                        ew.Cells["I6"].Value = "ZIP_CODE";
                        ew.Cells["J6"].Value = "BIRTHDATE";
                        ew.Cells["K6"].Value = "LOAN_REFERENCE";
                        ew.Cells["L6"].Value = "TOTAL_LOAN_BALANCE";
                        ew.Cells["M6"].Value = "LOAN_PURPOSE";
                        ew.Cells["N6"].Value = "BAR_TYPE";
                        ew.Cells["O6"].Value = "REMARKS";

                        Int32 ctr2=7;
                        foreach(repMidas r in list)
                        {
                            //bad loan
                            if (cutoffDate > r.lastPaymentDate && r.members.totalAmount > 0)
                            {

                                if (r.members.health == 0)
                                {

                                    ew.Cells["A" + ctr2.ToString()].Value = r.members.memberId;
                                    ew.Cells["B" + ctr2.ToString()].Value = r.members.name.last;
                                    ew.Cells["C" + ctr2.ToString()].Value = r.members.name.first;
                                    ew.Cells["D" + ctr2.ToString()].Value = r.members.name.middle;
                                    ew.Cells["E" + ctr2.ToString()].Value = r.members.address.Split('|')[0];
                                    ew.Cells["F" + ctr2.ToString()].Value = r.members.address.Split('|')[1];
                                    ew.Cells["G" + ctr2.ToString()].Value = r.members.address.Split('|')[2];
                                    ew.Cells["H" + ctr2.ToString()].Value = r.members.address.Split('|')[3];
                                    ew.Cells["I" + ctr2.ToString()].Value = "";
                                    ew.Cells["J" + ctr2.ToString()].Value = String.Format("{0:MM/dd/yyyy}", r.members.birthdate);
                                    ew.Cells["K" + ctr2.ToString()].Value = r.referenceId;
                                    ew.Cells["L" + ctr2.ToString()].Value = r.members.totalAmount; ;
                                    ew.Cells["M" + ctr2.ToString()].Value = "ET";
                                    ew.Cells["N" + ctr2.ToString()].Value = "05-01";
                                    ew.Cells["O" + ctr2.ToString()].Value = "";
                                    ctr2++;
                                }
                            }
                        }

                        ew.Cells["A" +ctr2.ToString()].Value = "END";
                        break;
                    case "barv2":
                        ew.Cells["A1"].Value = "MIDAS BARs"; //bad
                        ew.Cells["A2"].Value = "Institution";
                        ew.Cells["B2"].Value = "KMBI";
                        ew.Cells["A3"].Value = "Cut Off Date";
                        ew.Cells["B3"].Value = String.Format("{0:MM/dd/yyyy}",co);
                        ew.Cells["A4"].Value = "No. Of Clients";
                        ew.Cells["B4"].Value = list.Where(x=>x.members.health == 0).ToList().Count;
                        ew.Cells["A5"].Value = "BEGIN";
                        ew.Cells["A6"].Value = "CLIENT_REFERENCE";
                        ew.Cells["B6"].Value = "LAST_NAME";
                        ew.Cells["C6"].Value = "FIRST_NAME";
                        ew.Cells["D6"].Value = "MIDDLE_NAME";
                        ew.Cells["E6"].Value = "NO_STREET_SITIO_PUROK";
                        ew.Cells["F6"].Value = "BARANGAY_DISTRICT";
                        ew.Cells["G6"].Value = "CITY_MUNICIPALITY";
                        ew.Cells["H6"].Value = "PROVINCE";
                        ew.Cells["I6"].Value = "ZIP_CODE";
                        ew.Cells["J6"].Value = "BIRTHDATE";
                        ew.Cells["K6"].Value = "BIRTHPLACE";
                        ew.Cells["L6"].Value = "GENDER";
                        ew.Cells["M6"].Value = "CIVILSTATUS";
                        ew.Cells["N6"].Value = "CONTACT_NO";
                        ew.Cells["O6"].Value = "MOTHER'S MAIDEN FIRST NAME";
                        ew.Cells["P6"].Value = "MOTHER'S MAIDEN MIDDLE NAME";
                        ew.Cells["Q6"].Value = "MOTHER'S MAIDEN LAST NAME";
                        ew.Cells["R6"].Value = "ID_TYPE";
                        ew.Cells["S6"].Value = "ID_NO";
                        ew.Cells["T6"].Value = "SSS/GSIS";
                        ew.Cells["U6"].Value = "PAGIBIG";
                        ew.Cells["V6"].Value = "PHILHEALTH";
                        ew.Cells["W6"].Value = "TIN";
                        ew.Cells["X6"].Value = "LOAN_REFERENCE";
                        ew.Cells["Y6"].Value = "CONTRACT_TYPE";
                        ew.Cells["Z6"].Value = "CONTRACT_PHASE";
                        ew.Cells["AA6"].Value = "TRANSACTION_TYPE";
                        ew.Cells["AB6"].Value = "LOAN_PRINCIPAL";
                        ew.Cells["AC6"].Value = "LOAN_BALANCE";
                        ew.Cells["AD6"].Value = "DATE_GRANTED";
                        ew.Cells["AE6"].Value = "DATE_DUE";
                        ew.Cells["AF6"].Value = "INTEREST_RATE";
                        ew.Cells["AG6"].Value = "PAY_FREQ";
                        ew.Cells["AH6"].Value = "TERM";
                        ew.Cells["AI6"].Value = "CURRENCY";
                        ew.Cells["AJ6"].Value = "LOAN_PURPOSE";
                        ew.Cells["AK6"].Value = "BAR_TYPE";
                        ew.Cells["AL6"].Value = "TOTAL_LOAN_BALANCE";
                        ew.Cells["AM6"].Value = "CONTRACT_ACTUAL_END_DATE";
                        ew.Cells["AN6"].Value = "OVERDUE_DAYS";
                        ew.Cells["AO6"].Value = "MONTHLY_PAYMENT_AMOUNT";
                        ew.Cells["AP6"].Value = "NO_OF_OUTSTANDING_PAYMENTS";
                        ew.Cells["AQ6"].Value = "AMOUNT_OF_LAST_PAYMENT";
                        ew.Cells["AR6"].Value = "REMARKS";

                        Int32 ctr22=7;
                        foreach(repMidas r in list)
                        {
                            //bad loan
                            if(r.members.health ==0) {
                                ew.Cells["A" +ctr22.ToString()].Value = r.members.memberId; //CLIENT_REFERENCE
                                ew.Cells["B" +ctr22.ToString()].Value = r.members.name.last; //LAST_NAME
                                ew.Cells["C" +ctr22.ToString()].Value = r.members.name.first; //FIRST_NAME
                                ew.Cells["D" +ctr22.ToString()].Value = r.members.name.middle; //MIDDLE_NAME
                                ew.Cells["E" +ctr22.ToString()].Value = r.members.address.Split('|')[0]; //NO_STREET_SITIO_PUROK
                                ew.Cells["F" +ctr22.ToString()].Value = r.members.address.Split('|')[1]; //BARANGAY_DISTRICT
                                ew.Cells["G" +ctr22.ToString()].Value = r.members.address.Split('|')[2]; //CITY_MUNICIPALITY
                                ew.Cells["H" +ctr22.ToString()].Value = r.members.address.Split('|')[3]; //PROVINCE
                                ew.Cells["I" +ctr22.ToString()].Value = ""; //ZIP_CODE
                                ew.Cells["J" +ctr22.ToString()].Value = String.Format("{0:MM/dd/yyyy}",r.members.birthdate); //BIRTHDATE
                                ew.Cells["K" + ctr22.ToString()].Value = r.lmaidenName.birthplace; //BIRTHDATE
                                ew.Cells["L" +ctr22.ToString()].Value = "F"; //GENDER
                                // ew.Cells["M" +ctr22.ToString()].Value = r.members.civilStatus;
                                if (r.members.civilStatus == "Single") {
                                    ew.Cells["M" +ctr22.ToString()].Value = "1";
                                } 
                                else if (r.members.civilStatus == "single") {
                                    ew.Cells["M" +ctr22.ToString()].Value = "1";
                                } 
                                else if (r.members.civilStatus == "single    ") {
                                    ew.Cells["M" +ctr22.ToString()].Value = "1";
                                }

                                if (r.members.civilStatus == "Married") {
                                    ew.Cells["M" +ctr22.ToString()].Value = "2";
                                }
                                else if (r.members.civilStatus == "married") {
                                    ew.Cells["M" +ctr22.ToString()].Value = "2";
                                }

                                if (r.members.civilStatus == "Divorced/Separated") {
                                    ew.Cells["M" +ctr22.ToString()].Value = "3";
                                }
                                else if (r.members.civilStatus == "divorced/separated") {
                                    ew.Cells["M" +ctr22.ToString()].Value = "3";
                                }
                                else if (r.members.civilStatus == "Divorce/Seperated") {
                                    ew.Cells["M" +ctr22.ToString()].Value = "3";
                                }

                                if (r.members.civilStatus == "Widow") {
                                    ew.Cells["M" +ctr22.ToString()].Value = "4";
                                }
                                else if (r.members.civilStatus == "widow") {
                                    ew.Cells["M" +ctr22.ToString()].Value = "4";
                                }
                                else if (r.members.civilStatus == "Widowed") {
                                    ew.Cells["M" +ctr22.ToString()].Value = "4";
                                }
                                else if (r.members.civilStatus == "widowed") {
                                    ew.Cells["M" +ctr22.ToString()].Value = "4";
                                }

                                ew.Cells["N" +ctr22.ToString()].Value = r.members.position; //CONTACT_NO
                                // ew.Cells["O" +ctr22.ToString()].Value = r.lmaidenName.maidenName.first;; //"MOTHER'S MAIDEN FIRST NAME
                                // ew.Cells["P" +ctr22.ToString()].Value = r.lmaidenName.maidenName.middle;; //"MOTHER'S MAIDEN MIDDLE NAME
                                // ew.Cells["Q" +ctr22.ToString()].Value = r.lmaidenName.maidenName.last;; //"MOTHER'S MAIDEN LAST NAME
                                if (r.lmaidenName.maidenName.first == "," || r.lmaidenName.maidenName.first == "*" || r.lmaidenName.maidenName.first == "_")
                                {
                                    ew.Cells["O" + ctr22.ToString()].Value = r.lmaidenName.maidenName.first == "-";
                                }
                                else
                                {
                                    ew.Cells["O" + ctr22.ToString()].Value = r.lmaidenName.maidenName.first; //MOTHER'S MAIDEN FIRST NAME
                                }
                                if (r.lmaidenName.maidenName.middle == "," || r.lmaidenName.maidenName.middle == "*" || r.lmaidenName.maidenName.middle == "_")
                                {
                                    ew.Cells["P" + ctr22.ToString()].Value = r.lmaidenName.maidenName.middle == "-";
                                }
                                else
                                {
                                    ew.Cells["P" + ctr22.ToString()].Value = r.lmaidenName.maidenName.middle; //MOTHER'S MAIDEN MIDDLE NAME
                                }
                                if (r.lmaidenName.maidenName.last == "," || r.lmaidenName.maidenName.last == "*" || r.lmaidenName.maidenName.last == "_")
                                {
                                    ew.Cells["Q" + ctr22.ToString()].Value = r.lmaidenName.maidenName.last == "-";
                                }
                                else
                                {
                                    ew.Cells["Q" + ctr22.ToString()].Value = r.lmaidenName.maidenName.last;  //MOTHER'S MAIDEN LAST NAME
                                }
                                ew.Cells["R" +ctr22.ToString()].Value = r.members.issuedId.name; //ID_TYPE
                                ew.Cells["S" +ctr22.ToString()].Value = r.members.issuedId.number; //ID_NO
                                ew.Cells["T" +ctr22.ToString()].Value = r.members.businessName.Split('|')[1]; //SSS/GSIC
                                ew.Cells["U" +ctr22.ToString()].Value = r.members.businessName.Split('|')[2]; //PAGIBIG
                                ew.Cells["V" +ctr22.ToString()].Value = r.members.businessName.Split('|')[3]; //PHILHEALTH
                                ew.Cells["W" +ctr22.ToString()].Value = r.members.beneficiary; //TIN
                                ew.Cells["X" +ctr22.ToString()].Value = r.referenceId; //LOAN_REFERENCE
                                ew.Cells["Y" +ctr22.ToString()].Value = "18"; //CONTRACT_TYPE
                                ew.Cells["Z" +ctr22.ToString()].Value = "AC"; //CONTRACT_PHASE
                                ew.Cells["AA" +ctr22.ToString()].Value = "NA"; //TRANSACTION_TYPE
                                ew.Cells["AB" +ctr22.ToString()].Value = r.members.principalAmount; //LOAN_PRINCIPAL
                                ew.Cells["AC" +ctr22.ToString()].Value = r.members.totalAmount; //LOAN_BALANCE
                                ew.Cells["AD" +ctr22.ToString()].Value = String.Format("{0:MM/dd/yyyy}",r.disbursementDate.Value.Date.AddDays(7)); //DATE_GRANTED
                                ew.Cells["AE" +ctr22.ToString()].Value = String.Format("{0:MM/dd/yyyy}",r.disbursementDate.Value.Date.AddDays(7).AddMonths(6)); //DATE_DUE
                                ew.Cells["AF" +ctr22.ToString()].Value = r.loanType.interestRate; //INTEREST_RATE
                                ew.Cells["AG" +ctr22.ToString()].Value = r.loanType.frequency; //PAY_FREQ
                                ew.Cells["AH" +ctr22.ToString()].Value = r.loanType.term; //TERM
                                ew.Cells["AI" +ctr22.ToString()].Value = "PHP"; //CURRENCY
                                ew.Cells["AJ" +ctr22.ToString()].Value = "ET"; //LOAN_PURPOSE
                                ew.Cells["AK" +ctr22.ToString()].Value = "05-01"; //BAR_TYPE
                                ew.Cells["AL" +ctr22.ToString()].Value = r.members.principalAmount + (r.members.principalAmount * r.loanType.interestRate); //TOTAL_LOAN_BALANCE
                                ew.Cells["AM" +ctr22.ToString()].Value = String.Format("{0:MM/dd/yyyy}",r.disbursementDate.Value.Date.AddDays(7).AddMonths(6)); //CONTRACT_ACTUAL_END_DATE
                                ew.Cells["AN" +ctr22.ToString()].Value = r.members.purpose.code.Split('|')[0]; //OVERDUE_DAYS
                                ew.Cells["AO" +ctr22.ToString()].Value = r.members.purpose.code.Split('|')[1]; //MONTHLY_PAYMENT_AMOUNT
                                ew.Cells["AP" +ctr22.ToString()].Value = r.members.purpose.code.Split('|')[2]; //NO_OF_OUTSTANDING_PAYMENTS
                                ew.Cells["AQ" +ctr22.ToString()].Value = r.members.purpose.code.Split('|')[3]; //AMOUNT_OF_LAST_PAYMENT
                                ew.Cells["AR" +ctr22.ToString()].Value = ""; //REMARKS
                                ctr22++;
                            }

                        }

                        ew.Cells["A" +ctr22.ToString()].Value = "END";
                        break;
                    default:
                        if(errMess!="") {
                            errMess+= ", ";
                        }
                        errMess+= "Invalid type";
                        break;
                
                }
                
                if(errMess!="") {
                    return BadRequest(new {
                        type = "error",
                        message = errMess
                    });
                }

                bytes = ep.GetAsByteArray();
                ep.Dispose();

            }

            var link = _upload.upload(bytes,"kmbidevreporeports/" + filename);
            return Redirect(link);
        }

        private void salaryFilter(IEnumerable<users> userList, String slug)
        {
            // 1 - 2 = Rank and File
            // 3 - 4 = Officers
            // 5 - 7 = Executives

            var us = _users.getBySlug(slug);

            userList.ToList().ForEach(e=>{
                var salary = e.employment.salary;
                e.employment.salary = "CONFIDENTIAL";

                if(us!=null) {
                    if(us.permissions!=null) {
                        if(us.permissions.Contains("payroll-payslip-rank-file")) {
                            
                        } else if(us.permissions.Contains("payroll-payslip-officer")) {
                            if(
                                e.employment.jobGrade == "1" || 
                                e.employment.jobGrade == "2" )
                                e.employment.salary = String.Format("{0:N2}", Convert.ToDecimal(salary));
                        } else if(us.permissions.Contains("payroll-payslip-manager-up")) {
                            if(
                                e.employment.jobGrade == "1" || 
                                e.employment.jobGrade == "2" ||
                                e.employment.jobGrade == "3" || 
                                e.employment.jobGrade == "4" )

                                e.employment.salary = String.Format("{0:N2}", Convert.ToDecimal(salary));
                        } else if(us.permissions.Contains("super-salary")) {
                            e.employment.salary = String.Format("{0:N2}", Convert.ToDecimal(salary));
                        }
                    }
                }

                
            });
            
        }

        [HttpGetAttribute("users")]
        public IActionResult GetUsers([FromQueryAttribute]String type="" , [FromQueryAttribute]String slug="")
        {
            
            
            IEnumerable<users> list = null;
            var filename = "";
            var sheetname = "";
            if(type=="all") {
                filename = "ManpowerList.xlsx";
                sheetname = "MANPOWER LIST";
                list = _users.AllUsers();
            } else if(type=="probationary") {
                filename = "ManpowerListProbationary.xlsx";
                list = _users.AllProbationaryUsers();
                sheetname = "MANPOWER LIST (PROBATIONARY)";
            };


            salaryFilter(list, slug);




            //get branches
            var branches = _branch.allBranch();
            if(list!=null) {

                byte[] bytes;

                using (OfficeOpenXml.ExcelPackage ep = new OfficeOpenXml.ExcelPackage())
                {
                    ExcelWorksheet ew = ep.Workbook.Worksheets.Add(sheetname);

                    ew.Cells["A1"].Value = "KABALIKAT SA MAUNLAD NA BUHAY, INC.";
                    ew.Cells["A2"].Value = "HUMAN RESOURCE INFORMATION SYSTEM UNIT (HRIS)";
                    ew.Cells["A3"].Value = "List of Employees as of " + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss tt");

                    ew.Cells["X6"].Value = "Promotion/Lateral Transfer";
                    ew.Cells["AB6"].Value = "Resignation Letter";
                    ew.Cells["AW6"].Value = "TRAININGS";

                    ew.Cells["A7"].Value = "NO.";
                    ew.Cells["B7"].Value = "ID NO";
                    ew.Cells["C7"].Value = "NAME OF EMPLOYEE";
                    ew.Cells["D7"].Value = "SURNAME";
                    ew.Cells["E7"].Value = "GIVEN NAME";
                    ew.Cells["F7"].Value = "MIDDLE NAME";
                    ew.Cells["G7"].Value = "NAME SUFFIX";
                    ew.Cells["H7"].Value = "BRANCH";
                    ew.Cells["I7"].Value = "DATE HIRED";
                    ew.Cells["J7"].Value = "LENGTH OF STAY (YR.MON)";
                    ew.Cells["K7"].Value = "POSITION";
                    ew.Cells["L7"].Value = "EMPLOYMENT STATUS";
                    ew.Cells["M7"].Value = "DATE OF REGULARIZATION";
                    ew.Cells["N7"].Value = "END OF CONTRACT";
                    ew.Cells["O7"].Value = "AGE";
                    ew.Cells["P7"].Value = "BIRTHDATE";
                    ew.Cells["Q7"].Value = "CIVIL STATUS";
                    ew.Cells["R7"].Value = "GENDER";
                    ew.Cells["S7"].Value = "SSS NO.";
                    ew.Cells["T7"].Value = "TIN";
                    ew.Cells["U7"].Value = "TAX EXEMPTION CODE";
                    ew.Cells["V7"].Value = "PHILHEALTH";
                    ew.Cells["W7"].Value = "HDMF";
                    ew.Cells["X7"].Value = "Position";
                    ew.Cells["Y7"].Value = "blankspace";
                    ew.Cells["Z7"].Value = "From";
                    ew.Cells["AA7"].Value = "To";
                    ew.Cells["AB7"].Value = "Date Received";
                    ew.Cells["AC7"].Value = "Effectivity Date";
                    ew.Cells["AD7"].Value = "PRESENT ADDRESS";
                    ew.Cells["AE7"].Value = "PERMANENT ADDRESS";
                    ew.Cells["AF7"].Value = "COURSE";
                    ew.Cells["AG7"].Value = "UNIVERSITIY SCHOOL";
                    ew.Cells["AH7"].Value = "BASIC SALARY";
                    ew.Cells["AI7"].Value = "RICE SUBSIDY";
                    ew.Cells["AJ7"].Value = "Meal Allowance";
                    ew.Cells["AK7"].Value = "Transpo Allowance";
                    ew.Cells["AL7"].Value = "previous rate/salary";
                    ew.Cells["AM7"].Value = "Higher Duty Allowance";
                    ew.Cells["AN7"].Value = "Relocation Allowance";
                    ew.Cells["AO7"].Value = "";
                    ew.Cells["AP7"].Value = "RELIGION";
                    ew.Cells["AQ7"].Value = "";
                    ew.Cells["AR7"].Value = "JOB GRADE";
                    ew.Cells["AS7"].Value = "JOB CLASSIFICATION";
                    ew.Cells["AT7"].Value = "JOB CATEGORY";
                    ew.Cells["AU7"].Value = "Step Range";

                    ew.Cells["AV7"].Value = "";

                    ew.Cells["AW7"].Value = "TITLE";
                    ew.Cells["AX7"].Value = "INCLUSIVE DATES";
                    ew.Cells["AY7"].Value = "skills";



                    Int32 index = 9;
                    foreach(users u in list.OrderBy(u=>u.employment.group).ThenBy(u=>u.employment.division).ThenBy(u=>u.employment.branch).ThenBy(u=>u.name.last)) {
                        //row number
                        ew.Cells["A" + index.ToString()].Value = index - 8;
                        //id no
                        ew.Cells["B" + index.ToString()].Value = u.employeeCode;
                        //name of employee
                        ew.Cells["C" + index.ToString()].Value = u.name.last + ", " + u.name.first + " " + u.name.middle;
                        //surname
                        ew.Cells["D" + index.ToString()].Value = u.name.last;
                        //given name
                        ew.Cells["E" + index.ToString()].Value = u.name.first;
                        //middlename
                        ew.Cells["F" + index.ToString()].Value = u.name.middle;
                        //name suffix
                        ew.Cells["G" + index.ToString()].Value = u.extension;
                        //branch
                        var branchName = (from x in branches where x.slug == u.employment.branch select x.name).FirstOrDefault();
                        ew.Cells["H" + index.ToString()].Value = branchName;
                        //date hired
                        ew.Cells["I" + index.ToString()].Value = String.Format("{0:MM/dd/yyyy}",u.dateHired);

                        //length of stay (change format to YY MMMM)
                        Int32 noOfMonths = -1;
                        for(DateTime dt = u.dateHired;dt<=DateTime.Now.Date;dt = dt.AddMonths(1)) {
                            noOfMonths++;
                        }

                        var year =  (noOfMonths / 12);
                        var month = (noOfMonths % 12);
                        var los = "";
                        if(year==1) {
                            los = year.ToString() + " year";
                        } else if(year>1) {
                            los = year.ToString() + " years";
                        }
                        if(month==1){
                            if(los!="") {
                                los += " and " + month.ToString() + " month";
                            } else {
                                los += month.ToString() + " month";
                            }
                        } else if(month>1) {
                            if(los!="") {
                                los += " and " + month.ToString() + " months";
                            } else {
                                los += month.ToString() + " months";
                            }
                        }

                        ew.Cells["J" + index.ToString()].Value =  los;

                        //position
                        ew.Cells["K" + index.ToString()].Value = u.employment.position;
                        //status
                        ew.Cells["L" + index.ToString()].Value = u.employment.status;
                        //
                        if(u.employment.status.ToLower()=="probationary") {
                            ew.Cells["M" + index.ToString()].Value = String.Format("{0:MM/dd/yyyy}",u.employment.effectivityDateFrom.AddMonths(6).Date);
                        } else if(u.employment.status.ToLower()=="regular" || u.employment.status.ToLower()=="regularization") {
                            ew.Cells["M" + index.ToString()].Value = String.Format("{0:MM/dd/yyyy}",u.employment.effectivityDateFrom.Date);
                        }
                        //end of contract
                        ew.Cells["N" + index.ToString()].Value = "";

                        //age
                        DateTime now = DateTime.Today;
                        int age = now.Year - u.birthdate.Year;
                        ew.Cells["O" + index.ToString()].Value = now < u.birthdate.AddYears(age) ? age-- : age;
                        //birthdate
                        ew.Cells["P" + index.ToString()].Value = String.Format("{0:MM/dd/yyyy}",u.birthdate);
                        //civil status
                        ew.Cells["Q" + index.ToString()].Value = u.civilStatus;
                        //gender
                        ew.Cells["R" + index.ToString()].Value = u.gender;
                        //sss
                        ew.Cells["S" + index.ToString()].Value = u.sss;
                        //tin
                        ew.Cells["T" + index.ToString()].Value = u.tin;
                        //tax code
                        ew.Cells["U" + index.ToString()].Value = u.taxCode;
                        //philhealth
                        ew.Cells["V" + index.ToString()].Value = u.philhealth;
                        //hdmf
                        ew.Cells["W" + index.ToString()].Value = u.pagibig;



                        List<employmentHistoryItem> empHistory = u.employmentHistory;
                        if(empHistory!=null) {
                            //promotion / lateral transfer
                            employmentHistoryItem lastHistory = (from x in empHistory orderby x.effectivityDateFrom descending select x).FirstOrDefault();
                            if(lastHistory!=null) {
                                ew.Cells["X" + index.ToString()].Value = lastHistory.position;
                                ew.Cells["Y" + index.ToString()].Value = String.Format("{0:MM/dd/yyyy}",lastHistory.effectivityDateFrom);
                                ew.Cells["Z" + index.ToString()].Value = "";
                                ew.Cells["AA" + index.ToString()].Value = String.Format("{0:MM/dd/yyyy}",lastHistory.effectivityDateTo);
                            }

                        }

                        //resignation letter
                        employmentHistoryItem resignedHistory = (from x in empHistory where x.status.ToLower() == "resigned" || x.status.ToLower() == "terminated" select x).FirstOrDefault();
                        if(resignedHistory!=null) {
                            ew.Cells["AB" + index.ToString()].Value = "";
                            ew.Cells["AC" + index.ToString()].Value = resignedHistory.effectivityDateFrom;
                        }

                        List<address> addresses = u.addresses;
                        if(addresses!=null) {
                            address present = u.addresses.Find(x => x.type.ToLower() =="present address");
                            address permanent = u.addresses.Find(x => x.type.ToLower() =="permanent address");
                            if(present!=null) {
                                ew.Cells["AD" + index.ToString()].Value = present.location;
                            }
                            if(permanent!=null) {
                                ew.Cells["AE" + index.ToString()].Value = permanent.location;
                            }
                        }


                        education latestEducation = (from x in u.educations orderby x.inclusiveDateFrom descending select x).LastOrDefault();
                        if(latestEducation!=null) {
                            ew.Cells["AF" + index.ToString()].Value =  latestEducation.degree;
                            ew.Cells["AG" + index.ToString()].Value = latestEducation.school;
                        }

                        ew.Cells["AH" + index.ToString()].Value = u.employment.salary;

                        //allowances
                        List<allowance> allowances = u.employment.allowances;


                        if(allowances!=null) {
                            if(allowances.Count > 0) {
                                //meal
                                var b = (from x in allowances where x.name.ToLower() == "meal allowance" select x.amount).FirstOrDefault();
                                ew.Cells["AJ" + index.ToString()].Value = b;
                                //transportation
                                var c = (from x in allowances where x.name.ToLower() == "transportation allowance" select x.amount).FirstOrDefault();
                                ew.Cells["AK" + index.ToString()].Value = c;
                                //higher duty
                                var e = (from x in allowances where x.name.ToLower() == "higher duty allowance" select x.amount).FirstOrDefault();
                                ew.Cells["AM" + index.ToString()].Value = e;
                                //relocation
                                var f = (from x in allowances where x.name.ToLower() == "relocation allowance" select x.amount).FirstOrDefault();
                                ew.Cells["AN" + index.ToString()].Value =f;
                            }

                        }

                        //benefits
                        List<benefit> benefits = u.employment.benefits;

                        if(benefits!=null) {
                            //rice subsidy
                            var a = (from x in benefits where x.name == "rice subsidy" select x.amount).FirstOrDefault();
                            ew.Cells["AI" + index.ToString()].Value = a;
                        }



                        if(empHistory!=null) {
                            //previous rate / salary
                            var d = (from x in empHistory orderby x.effectivityDateFrom descending select x.salary).FirstOrDefault();
                            ew.Cells["AL" + index.ToString()].Value = d;
                        }

                        ew.Cells["AO" + index.ToString()].Value = "";
                        ew.Cells["AO" + index.ToString()].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        ew.Cells["AO" + index.ToString()].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(148, 138, 84));

                        ew.Cells["AP" + index.ToString()].Value = u.religion;

                        ew.Cells["AQ" + index.ToString()].Value = "";
                        ew.Cells["AQ" + index.ToString()].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        ew.Cells["AQ" + index.ToString()].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(148, 138, 84));

                        ew.Cells["AR" + index.ToString()].Value = u.employment.jobGrade;
                        ew.Cells["AS" + index.ToString()].Value = u.employment.jobClassification;
                        ew.Cells["AT" + index.ToString()].Value = u.employment.jobCategory;

                        ew.Cells["AU" + index.ToString()].Value = "";

                        ew.Cells["AV" + index.ToString()].Value = "";
                        ew.Cells["AV" + index.ToString()].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        ew.Cells["AV" + index.ToString()].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(148, 138, 84));

                        training training = (from x in u.trainings orderby x.dateTo descending select x).FirstOrDefault();
                        if(training!=null) {
                            ew.Cells["AW" + index.ToString()].Value = training.title;
                            ew.Cells["AX" + index.ToString()].Value = String.Format("{0:MM/dd/yyyy}",training.dateFrom) + " - " + String.Format("{0:MM/dd/yyyy}",training.dateTo);
                        }

                        skill skill = (from x in u.skills orderby x.updatedAt descending select x).FirstOrDefault();
                        if(skill!=null) {
                            ew.Cells["AY" + index.ToString()].Value = skill!=null ? skill.type : "";
                        }
                        index++;
                    }

                    //format columns widhts
                    ew.Column(1).Width =7.84;
                    ew.Column(2).Width =12.68;
                    ew.Column(3).Width =35.42;
                    ew.Column(4).Width =16.42;
                    ew.Column(5).Width =18.11;
                    ew.Column(6).Width =15.84;
                    ew.Column(7).Width =12.42;
                    ew.Column(8).Width =17.42;
                    ew.Column(9).Width =15;
                    ew.Column(10).Width =17;
                    ew.Column(11).Width =38.68;
                    ew.Column(12).Width =15.11;
                    ew.Column(13).Width =16.68;
                    ew.Column(14).Width =12.11;
                    ew.Column(15).Width =6.58;
                    ew.Column(16).Width =11.84;
                    ew.Column(17).Width =8.58;
                    ew.Column(18).Width =9.26;
                    ew.Column(19).Width =14.42;
                    ew.Column(20).Width =13.42;
                    ew.Column(21).Width =14.26;
                    ew.Column(22).Width =18.84;
                    ew.Column(23).Width =19.84;
                    ew.Column(24).Width =9.68;
                    ew.Column(25).Width =13.26;
                    ew.Column(26).Width =12.68;
                    ew.Column(27).Width =11.11;
                    ew.Column(28).Width =19.68;
                    ew.Column(29).Width =13.68;
                    ew.Column(30).Width =23.11;
                    ew.Column(31).Width =22.42;
                    ew.Column(32).Width =24.42;
                    ew.Column(33).Width =43.58;
                    ew.Column(34).Width =20.84;
                    ew.Column(35).Width =11.42;
                    ew.Column(36).Width =13;
                    ew.Column(37).Width =11.42;
                    ew.Column(38).Width =12;
                    ew.Column(39).Width =9.84;
                    ew.Column(40).Width =10.84;
                    ew.Column(41).Width =5.11;
                    ew.Column(42).Width =19.84;
                    ew.Column(43).Width =5.11;
                    ew.Column(44).Width =10.11;
                    ew.Column(45).Width =19;
                    ew.Column(46).Width =15.11;
                    ew.Column(47).Width =15.11;
                    ew.Column(48).Width =3.11;
                    ew.Column(49).Width =8.58;
                    ew.Column(50).Width =8.58;

                    //set row 1 font to bold
                    ew.Row(1).Style.Font.Bold = true;
                    //set row 1 height to 25.5
                    ew.Row(1).Height = 25.5;

                    //set row 2 and 3 font to bold and italic
                    ew.Row(2).Style.Font.Bold = true;
                    ew.Row(2).Style.Font.Italic = true;
                    ew.Row(3).Style.Font.Bold = true;
                    ew.Row(3).Style.Font.Italic = true;

                    //set job info font to bold

                    ew.Cells["AR7"].Style.Font.Bold = true;
                    ew.Cells["AS7"].Style.Font.Bold = true;
                    ew.Cells["AT7"].Style.Font.Bold = true;
                    ew.Cells["AU7"].Style.Font.Bold = true;

                    // center NO.
                    ew.Row(1).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                    //fill background color to set color
                    ew.Row(6).Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    ew.Row(6).Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(153, 255, 153));
                    //set row 6 height to 10.5, horizontal and vertical alignment to center
                    ew.Row(6).Height = 10.5;
                    ew.Row(6).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    ew.Row(6).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;

                    //fill background color to set color
                    ew.Row(7).Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    ew.Row(7).Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(153, 255, 153));

                    //set row 6 height to 10.5, horizontal and vertical alignment to center
                    ew.Row(7).Height = 27;
                    ew.Row(7).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    ew.Row(7).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;

                    for(var ctr = 1;ctr<=index;ctr++) {

                        //set rows font to Tahoma
                        ew.Row(ctr).Style.Font.Name = "Tahoma";
                        //set rows font size to 10
                        ew.Row(ctr).Style.Font.Size = 10;

                        //fill columns with brownlines
                        ew.Cells["AO" + ctr.ToString()].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        ew.Cells["AO" + ctr.ToString()].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(148, 138, 84));

                        ew.Cells["AQ" + ctr.ToString()].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        ew.Cells["AQ" + ctr.ToString()].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(148, 138, 84));

                        ew.Cells["AV" + ctr.ToString()].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        ew.Cells["AV" + ctr.ToString()].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(148, 138, 84));

                    }


                    //merge Promotional/Lateral Transfer
                    ew.Cells["X6:AA6"].Merge = true;
                    //merge Resignation Letter
                    ew.Cells["AB6:AC6"].Merge = true;
                    //merge Trainings
                    ew.Cells["AW6:AX6"].Merge = true;

                    //merge NO.
                    ew.Cells["A6:A7"].Merge = true;
                    ew.Cells["A6:A7"].Value = "NO.";
                    //merge ID NO
                    ew.Cells["B6:B7"].Merge = true;
                    ew.Cells["B6:B7"].Value = "ID NO";
                    //merge NAME OF EMPLOYEE
                    ew.Cells["C6:C7"].Merge = true;
                    ew.Cells["C6:C7"].Value = "NAME OF EMPLOYEE";

                    //hide column 7 (Y)
                    ew.Column(25).Hidden = true;
                    //free pane on row 8 column C
                    ew.View.FreezePanes(8, 4);

                    //wordwrap row 7
                    ew.Row(7).Style.WrapText = true;
                    bytes = ep.GetAsByteArray();
                    ep.Dispose();
                }


                var link = _upload.upload(bytes,"kmbidevreporeports/" + filename);
                return Redirect(link);
            } else {
                return Ok(new {type="failed", message="Invalid type"});
            }

        }

        [HttpGet("orList")]
        public IActionResult getOrList([FromQuery]DateTime from, [FromQuery] DateTime to)
        {
            var rep = _reporting.getOrList(from,to);

                _rep.templateFilename = "orList.cshtml";
                _rep.model = rep;
                _rep.orientation = "portrait";
                _rep.footerText = "System Generated Report - Microfinance System - " + DateTime.Now;
                _rep.title = "OR List";
                _rep.isHalf = false;
                _rep.pageType ="letter";
                dynamic vb = new System.Dynamic.ExpandoObject();
                vb.dateFrom = from;
                vb.dateTo = to;
                _rep.viewBag = vb;
                iResult res = _rep.generatePDF();

            var link = _upload.upload(res.bytes,"kmbidevreporeports/orList.pdf");
            return Redirect(link);
        }

        // GET DTR
        //REGION
        [HttpGet("getDtrRegion")]
        public IActionResult getdtrRegion([FromQuery]string region, [FromQuery]DateTime from, [FromQuery] DateTime to)
        {
            var rep = _reporting.getDtrRegion(region, from, to);
            // return new ObjectResult(rep);
            _rep.templateFilename = "dtrDetailed.cshtml";
            _rep.model = rep;
            _rep.orientation = "portrait";
            _rep.footerText = "System Generated Report - Microfinance System - " + DateTime.Now;
            _rep.title = region + " DTR(" + String.Format("{0:MMddyyyy}",from) + "-" + String.Format("{0:MMddyyyy}",to) +")";
            _rep.isHalf = false;
            _rep.pageType = "letter";
            dynamic vb = new System.Dynamic.ExpandoObject();
            vb.dateFrom = from;
            vb.dateTo = to;
            _rep.viewBag = vb;

            if(rep!=null){
                if(rep.Count > 0) {
                    iResult res = _rep.generatePDF();

                    var link = _upload.upload(res.bytes,"kmbidevreporeports/" + region + "-" + String.Format("{0:MMddyyyy}",from) + "-" + String.Format("{0:MMddyyyy}",to) + "-dtr.pdf");
                    return Redirect(link);
                } else {
                    return Ok(new {type="Information", messages="No data to generate"});
                }
            } else {
                return Ok(new {type="Information", messages="No data to generate"});
            }

        }

        [HttpGet("getDtrArea")]
        public IActionResult getdtrArea([FromQuery]string area, [FromQuery]DateTime from, [FromQuery] DateTime to)
        {
            var rep = _reporting.getDtrArea(area, from, to);
            // return new ObjectResult(rep);
            _rep.templateFilename = "dtrDetailed.cshtml";
            _rep.model = rep;
            _rep.orientation = "portrait";
            _rep.footerText = "System Generated Report - Microfinance System - " + DateTime.Now;
            _rep.title = area + " DTR(" + String.Format("{0:MMddyyyy}",from) + "-" + String.Format("{0:MMddyyyy}",to) +")";
            _rep.isHalf = false;
            _rep.pageType = "letter";
            dynamic vb = new System.Dynamic.ExpandoObject();
            vb.dateFrom = from;
            vb.dateTo = to;
            _rep.viewBag = vb;

            if(rep!=null){
                if(rep.Count > 0) {
                    iResult res = _rep.generatePDF();

                    var link = _upload.upload(res.bytes, "kmbidevreporeports/" + area + "-" + String.Format("{0:MMddyyyy}",from) + "-" + String.Format("{0:MMddyyyy}",to) + "-dtr.pdf");
                    return Redirect(link);
                } else {
                    return Ok(new {type="Information", messages="No data to generate"});
                }
            } else {
                return Ok(new {type="Information", messages="No data to generate"});
            }
        }

        [HttpGet("ledgerIndividual")]
        public IActionResult getLedgerIndividual([FromQuery]string referenceId, [FromQuery]String memberId)
        {
            var rep = _reporting.getLedgerIndividual(referenceId, memberId);

            _rep.templateFilename = "ledgerIndividual.cshtml";
            _rep.model = rep;
            _rep.orientation = "landscape";
            _rep.footerText = "System Generated Report - Microfinance System - " + DateTime.Now;
            _rep.title = "Ledger Per Client";
            _rep.isHalf = false;
            _rep.pageType = "letter";
            // dynamic vb = new System.Dynamic.ExpandoObject();
            // vb.dateFrom = from;
            // vb.dateTo = to;
            //_rep.viewBag = vb;
            iResult res = _rep.generatePDF();

            var link = _upload.upload(res.bytes, "kmbidevreporeports/" + (rep.clientName + " - Ledger (Individual).pdf").Replace("Ñ", "N").Replace("ñ", "n").Replace(" ", "-").ToLower());
            return Redirect(link);

            //var rep = _reporting.getLedgerIndividual(referenceId, memberId);
            //var data = JsonConvert.SerializeObject(rep);
            //var repDesc = "Individula Ledger";
            //return generateReport("idivLedger", data, repDesc);
        }

        [HttpGetAttribute("rfpCBUWithdrawal")]
        public IActionResult getRfpCBUWithdrawal([FromQueryAttribute]String id = "")
        {
            var rep = _reporting.getRfpCvCBUWidthdrawal(id);
            var data = JsonConvert.SerializeObject(rep);
            var repDesc = "Request for Payment";
            return generateReport("rfpCbu", data, repDesc);
        }

        [HttpGetAttribute("cvCBUWithdrawal")]
        public IActionResult getCvCBUWithdrawal([FromQueryAttribute]String id = "")
        {
            var rep = _reporting.getRfpCvCBUWidthdrawal(id);
            var data = JsonConvert.SerializeObject(rep);
            var repDesc = "Check Voucher";
            return generateReport("cvCbu", data, repDesc);
        }





        private List<repDtrDetailed> lateLogicParser(List<repDtrDetailed> dtrs)
        {
            // late logic
            // if managers and up do not include less than 15mins
            // else include lates
            dtrs.ForEach(d=>{
                if(d.employment.jobGrade=="5" || d.employment.jobGrade=="6" || d.employment.jobGrade=="7") {
                    d.dtr.ForEach(dtr=>{
                        if(dtr.late.am <= .25 && dtr.late.am > 0)
                            dtr.late.am = 0;

                        if(dtr.late.pm <= .25 && dtr.late.pm > 0)
                            dtr.late.pm = 0;
                    });
                }
            });

            return dtrs;
        }
        [HttpGet("dtrDetailedPerEmployee/{employeeCode}")]
        public IActionResult dtrDetailedPerEmployee(String employeeCode, [FromQuery]DateTime from, [FromQuery]DateTime to) {
            
           
            var rep = _reporting.dtrDetailedPerEmployee(employeeCode, from,to);
            _rep.templateFilename = "dtrDetailed.cshtml";
            _rep.model = rep;
            _rep.orientation = "portrait";

            //string myString =  rep[0].fullname; 
            // byte[] bytes = Encoding.Default.GetBytes(myString);
            // myString = Encoding.UTF8.GetString(bytes);
            
            
        
            _rep.footerText = "System Generated Report - HRIS System - " + DateTime.Now;
            _rep.title = rep[0].fullname + " DTR (" + String.Format("{0:MMddyyyy}",from) + "-" + String.Format("{0:MMddyyyy}",to) +")";
            _rep.isHalf = false;
            _rep.pageType ="legal";
             dynamic vb = new System.Dynamic.ExpandoObject();
            vb.dateFrom = from;
            vb.dateTo = to;
            _rep.viewBag = vb;

            
            rep = lateLogicParser(rep);
            if(rep!=null){
                if(rep.Count > 0) {
                    iResult res = _rep.generatePDF();


                    var link = _upload.upload(res.bytes,"kmbidevreporeports/" + rep[0].employeeCode + "-" + rep[0].slug + "-" + String.Format("{0:MMddyyyy}",from) + "-" + String.Format("{0:MMddyyyy}",to) + "-dtr.pdf");
                    return Redirect(link);
                } else {
                    return Ok(new {type="Information", messages="No data to generate"});
                }
            } else {
                return Ok(new {type="Information", messages="No data to generate"});
            }


        }

        [HttpGet("dtrDetailedPerBranch/{branchSlug}")]
        public IActionResult dtrDetailedPerBranch(String branchSlug, [FromQuery]DateTime from, [FromQuery]DateTime to) {
            var rep = _reporting.dtrDetailedPerBranch(branchSlug, from,to);
            rep.ForEach(x=> {
               x.dtr = (from y in x.dtr orderby y.date select y).ToList();
            });
            _rep.templateFilename = "dtrDetailed.cshtml";
            _rep.model = rep;
            _rep.orientation = "portrait";

            _rep.footerText = "System Generated Report - HRIS System - " + DateTime.Now;

            var bname = "";

            if(rep!=null) {
                if(rep.Count() > 0) {
                    if(rep[0].branch.Count() > 0) {
                        bname = rep[0].branch[0].name;
                    }
                }
            }

            _rep.title = bname + " DTR (" + String.Format("{0:MMddyyyy}",from) + "-" + String.Format("{0:MMddyyyy}",to) +")";
            _rep.isHalf = false;
            _rep.pageType ="legal";
             dynamic vb = new System.Dynamic.ExpandoObject();
            vb.dateFrom = from;
            vb.dateTo = to;
            _rep.viewBag = vb;

            rep = lateLogicParser(rep);
            if(rep!=null){
                if(rep.Count > 0) {
                    iResult res = _rep.generatePDF();
                    var link = _upload.upload(res.bytes,"kmbidevreporeports/" + bname.Replace(" ","-") + "-" + String.Format("{0:MMddyyyy}",from) + "-" + String.Format("{0:MMddyyyy}",to) + "-dtr.pdf");
                    return Redirect(link);
                } else {
                    return Ok(new {type="Information", messages="No data to generate"});
                }
            } else {
                return Ok(new {type="Information", messages="No data to generate"});
            }


        }

        [HttpGet("dtrDetailedPerDepartment/{departmentName}")]
        public IActionResult dtrDetailedPerdepartment(String departmentName, [FromQuery]DateTime from, [FromQuery]DateTime to) {
            var rep = _reporting.dtrDetailedPerDepartment(departmentName, from,to);
             rep.ForEach(x=> {
               x.dtr = (from y in x.dtr orderby y.date select y).ToList();
            });
            _rep.templateFilename = "dtrDetailed.cshtml";
            _rep.model = rep;
            _rep.orientation = "portrait";

            _rep.footerText = "System Generated Report - HRIS System - " + DateTime.Now;

          

            _rep.title = departmentName + " DTR (" + String.Format("{0:MMddyyyy}",from) + "-" + String.Format("{0:MMddyyyy}",to) +")";
            _rep.isHalf = false;
            _rep.pageType ="legal";
             dynamic vb = new System.Dynamic.ExpandoObject();
            vb.dateFrom = from;
            vb.dateTo = to;
            _rep.viewBag = vb;

            rep = lateLogicParser(rep);
            if(rep!=null){
                if(rep.Count > 0) {
                    iResult res = _rep.generatePDF();
                    var link = _upload.upload(res.bytes,"kmbidevreporeports/" + departmentName.Replace(" ","-") + "-" + String.Format("{0:MMddyyyy}",from) + "-" + String.Format("{0:MMddyyyy}",to) + "-dtr.pdf");
                    return Redirect(link);
                } else {
                    return Ok(new {type="Information", messages="No data to generate"});
                }
            } else {
                return Ok(new {type="Information", messages="No data to generate"});
            }
        }


        [HttpGet("timekeepingAndAttendanceSummary")]
        public IActionResult timekeepingAndAttendanceSummary([FromQuery]DateTime from, [FromQuery]DateTime to, [FromQuery]String branchSlug)
        {

            List<repDtrDetailed> list = new List<repDtrDetailed>();
            //list = new List<repDtrDetailed> {_reporting.timekeepingAndAttendanceSummary(from,to).Find(x=>x.employeeCode=="0014-3218")};
            list = _reporting.timekeepingAndAttendanceSummary(from,to).ToList();
            if(branchSlug!=null) {
                if(branchSlug!="") {
                    // list = new List<repDtrDetailed> {_reporting.timekeepingAndAttendanceSummary(from,to).Find(x=>x.employeeCode=="0014-3218")};
                    list = _reporting.timekeepingAndAttendanceSummary(from,to,branchSlug).ToList();
                }
            }
             
            // list = list.Where(x=>x.employeeCode == "0005-4562").ToList();
            var pPfrom = _cutoff.getCurrentCutOff().periodRange.from;
            var pPto = _cutoff.getCurrentCutOff().periodRange.to;
            var filename = "TimekeepingAndAttendanceSummary.xlsx";
            byte[] bytes;

            using (OfficeOpenXml.ExcelPackage ep = new OfficeOpenXml.ExcelPackage()) {

                ExcelWorksheet ew = ep.Workbook.Worksheets.Add("OFFICERS AND R&F");

                ew.Cells["A1"].Value = "KABALIKAT PARA SA MAUNLAD NA BUHAY, INC.";
                ew.Cells["A2"].Value = "12 San Francisco St., Karuhatan Valenzuela City"; ew.Cells["A3:Z3"].Merge = true;
                ew.Cells["A3:Z3"].Value = "TIMEKEEPING & ATTENDANCE SUMMARY";
                ew.Cells["A3:Z3"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ew.Cells["A3:Z3"].Style.Font.UnderLine = true;
                ew.Cells["A3:Z3"].Style.Font.Bold = true;
                ew.Cells["A4"].Value = "Payroll Period:";
                ew.Cells["A5"].Value = "Payroll Cut-off:";
                ew.Cells["C4"].Value = String.Format("{0:MMM dd} to {1:MMM dd} {1:yyyy}",pPfrom,pPto);
                ew.Cells["C5"].Value = String.Format("{0:MMM dd} to {1:MMM dd} {1:yyyy}",from,to);

                ew.Cells["A6:A8"].Merge = true;
                ew.Cells["A6:A8"].Value = "#";
                ew.Cells["B6:B8"].Merge = true;
                ew.Cells["B6:B8"].Value = "EMP. ID";
                ew.Cells["C6:C8"].Merge = true;
                ew.Cells["C6:C8"].Value = "NAME";
                ew.Cells["D6:H6"].Merge = true;
                ew.Cells["D6:H6"].Value = "OVERTIME";
                ew.Cells["D7:D8"].Merge = true;
                ew.Cells["D7:D8"].Value = "REG";
                ew.Cells["E7:E8"].Merge = true;
                ew.Cells["E7:E8"].Value = "ND";
                ew.Cells["F7:F8"].Merge = true;
                ew.Cells["F7:F8"].Value = "RD";
                ew.Cells["G7:G8"].Merge = true;
                ew.Cells["G7:G8"].Value = "EXCESS RD";
                ew.Cells["H7:H8"].Merge = true;
                ew.Cells["H7:H8"].Value = "RD ND";
                ew.Cells["L6:O6"].Merge = true;
                ew.Cells["L6:O6"].Value = "ABSENCES";
                ew.Cells["L7:M7"].Merge = true;
                ew.Cells["L7:M7"].Value = "W/ PAY";
                ew.Cells["N7:O7"].Merge = true;
                ew.Cells["N7:O7"].Value = "W/O PAY";
                ew.Cells["L8"].Value = "VL";
                ew.Cells["M8"].Value = "SL";
                ew.Cells["N8"].Value = "VL";
                ew.Cells["O8"].Value = "SL";
                ew.Cells["P6:P8"].Merge = true;
                ew.Cells["P6:P8"].Value = "DATE";
                ew.Cells["Q6:Q8"].Merge = true;
                ew.Cells["Q6:Q8"].Value = "ABSENT";
                ew.Cells["R6:R8"].Merge = true;
                ew.Cells["R6:R8"].Value = "DATE";
                ew.Cells["S6:S8"].Merge = true;
                ew.Cells["S6:S8"].Value = "TRAVEL LEAVE / SOLO PARENT / PATERNITY LEAVE";
                ew.Cells["T6:T8"].Merge = true;
                ew.Cells["T6:T8"].Value = "DATE";
                ew.Cells["U6:V6"].Merge = true;
                ew.Cells["U6:V6"].Value = "UNDERTIME";
                ew.Cells["U7:U8"].Merge = true;
                ew.Cells["U7:U8"].Value = "W/ PAY (in hrs.)";
                ew.Cells["V7:V8"].Merge = true;
                ew.Cells["V7:V8"].Value = "W/O PAY (in hrs.)";
                ew.Cells["W6:W8"].Merge = true;
                ew.Cells["W6:W8"].Value = "DATE";
                ew.Cells["X6:X8"].Merge = true;
                ew.Cells["X6:X8"].Value = "LATE (hrs)";
                ew.Cells["Y6:Y8"].Merge = true;
                ew.Cells["Y6:Y8"].Value = "LATE (hrs)";
                ew.Cells["Y6:Y8"].Merge = true;
                ew.Cells["Y6:Y8"].Value = "Date";
                ew.Cells["Z6"].Value = "ADJUSTMENTS";
                ew.Cells["Z7:Z8"].Merge = true;
                ew.Cells["Z7:Z8"].Value = "PARTICULARS";



                Int32 liner = 9;
                Int32 ctr = 1;

                var grandTotReg = 0.0;
                var grandTotNd = 0.0;
                var grandTotRd = 0.0;
                var grandTotExcessRd = 0.0;
                var grandTotRdNd = 0.0;
                var grandTotVLWP = 0.0;
                var grandTotVLWoP = 0.0;
                var grandTotSlWP = 0.0;
                var grandTotSlWoP = 0.0;
                var grandTotAbsent = 0.0;
                var grandTotTLSPLPL = 0.0;
                var grandTotUtWP = 0.0;
                var grandTotUtWoP = 0.0;
                var grandTotLateHrs = 0.0;

                //get distinct branches

                // 1 - 2 = Rank and File
                // 3 - 4 = Officers
                // 5 - 7 = Executives

                // change jobgrade based on legend above

                list.ForEach((x) => {
                    switch(x.employment.jobGrade) {
                        case "1":
                            x.employment.jobClassification = "B";
                            x.employment.jobGrade = "Rank & File";
                            break;
                        case "2": 
                            x.employment.jobClassification = "B";
                            x.employment.jobGrade = "Rank & File";
                            break;
                        case "3": 
                        x.employment.jobClassification = "C";
                            x.employment.jobGrade = "Officers";
                            break;
                        case "4": 
                            x.employment.jobClassification = "C";
                            x.employment.jobGrade = "Officers";
                            break;
                        case "5": 
                            x.employment.jobClassification = "D";
                            x.employment.jobGrade = "Executives and Managers";
                            break;
                        case "6": 
                            x.employment.jobClassification = "D";
                            x.employment.jobGrade = "Executives and Managers";
                            break;
                        case "7": 
                            x.employment.jobClassification = "D";
                            x.employment.jobGrade = "Executives and Managers";
                            break;
                        default:
                            x.employment.jobClassification = "A";
                            x.employment.jobGrade = "Uncategorized";
                            break;
                    }
                });

                //select distinct jobGrade
                var distJobGrade = (from x in list orderby x.employment.jobClassification descending select x.employment.jobGrade).Distinct();
                // var hols = unit.total.GetHolidays(0,0);
                foreach(String jobGrade in distJobGrade) {
                    
                    ew.Cells[liner,1].Style.Font.Bold = true;
                    ew.Cells[liner,1,liner,26].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    ew.Cells[liner,1,liner,26].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(150, 150, 150));
                    ew.Cells[liner,1].Value = jobGrade;
                    ew.Cells[liner,1].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                    ew.Cells[liner,1].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                    liner++;

                    var totReg = 0.0;
                    var totNd = 0.0;
                    var totRd = 0.0;
                    var totExcessRd = 0.0;
                    var totRdNd = 0.0;
                    var totVLWP = 0.0;
                    var totSlWP = 0.0;
                    var totVLWoP = 0.0;
                    var totSlWoP = 0.0;
                    var totAbsent = 0.0;
                    var totTLSPLPL = 0.0;
                    var totUtWP = 0.0;
                    var totUtWoP = 0.0;
                    var totLateHrs = 0.0;

                    var rem = "";


                    foreach(repDtrDetailed u in list.Where(x=>x.employment.jobGrade == jobGrade))
                    {
                        
                        // var totPerEmpReg = 0.0;
                        // var totPerEmpNd = 0.0;
                        // var totPerEmpRd = 0.0;
                        // var totPerEmpExcessRd = 0.0;
                        // var totPerEmpRdNd = 0.0;
                        // var totPerEmpVLWP = 0.0;
                        // var totPerEmpSlWP = 0.0;
                        // var totPerEmpVLWoP = 0.0;
                        // var totPerEmpSlWoP = 0.0;
                        // var totPerEmpAbsent = 0.0;
                        //var totPerEmpTLSPLPL = 0.0;
                        //var totPerEmpUtWP = 0.0;
                        var totPerEmpUtWoP = 0.0;
                        var totPerEmpLateHrs = 0.0;
                        DateTime _from = from;
                        DateTime _to = to;


                        ew.Cells[liner,1].Value = ctr.ToString();
                        ctr++;
                        ew.Cells[liner,2].Value = u.employeeCode;
                        ew.Cells[liner,3].Value = u.fullname;

                        List<String> otabs = new List<String>();



                        for(DateTime f = _from;f<=_to;f = f.AddDays(1))
                        {
                            var att = u.dtr.Where(x=>x.date.Date == f.Date).FirstOrDefault();
                            var lv = u.leaveLogs.Where(
                                x=>
                                    (x.dates.from >= f.Date && x.dates.from <= f.Date.AddDays(1).AddSeconds(-1)) ||
                                    (x.dates.to >= f.Date && x.dates.to <= f.Date.AddDays(1).AddSeconds(-1)) ||
                                    (x.dates.from <= f.Date && x.dates.to >= f.Date)
                                ).FirstOrDefault();

                            var _reg = 0.0;
                            var _nd= 0.0;
                            var _rd = 0.0;
                            var _excRd = 0.0;
                            var _rdnd = 0.0;
                            var _slwp = 0.0;
                            var _vlwp = 0.0;
                            var _slwop = 0.0;
                            var _vlwop = 0.0;
                            var _otl = 0.0;
                            var _lt = 0.0;
                            var _abs = 0.0;
                            var _utwop = 0.0;
                            if(att!=null) {
                                
                                // overtime should be inputed as hour
                                if(att.overtime.am + att.overtime.pm > 0) {
                                    if(f.DayOfWeek == DayOfWeek.Sunday || f.DayOfWeek == DayOfWeek.Saturday) {
                                        
                                        if(((att.schedule.inAm - att.schedule.outPm).TotalHours - 1) > _rd) {
                                            _rd = (att.overtime.am + att.overtime.pm);
                                            totRd += _rd;
                                            _excRd = (att.overtime.am + att.overtime.pm) - _rd;
                                            totExcessRd += _excRd;
                                        } else {
                                            _rd = (att.overtime.am + att.overtime.pm);
                                            totRd += _rd;
                                        }
                                        
                                        _rdnd = att.nd;
                                        totRdNd += _rdnd;
                                    } else {    
                                        _reg = (att.overtime.am + att.overtime.pm);
                                        totReg += _reg;
                                        _nd = att.nd;
                                        totNd += _nd;
                                    }
                                }
                                
                                //end overtime
                                // totAbsent += 0.0;
                                // totUtWP += 0.0;
                                //_utwop = (att.undertime.am + att.undertime.pm) / 8;
                                //undertime by hour

                                _utwop = (att.undertime.am + att.undertime.pm);
                                totUtWoP += _utwop;
                                
                                // remove late if managers and up
                                if(jobGrade == "Executives and Managers") {
                                    if(att.late.am > .25)
                                        _lt += att.late.am;

                                    if(att.late.pm > .25)
                                        _lt += att.late.pm;
                                        
                                } else {
                                    _lt = att.late.am + att.late.pm;
                                }
                                //_lt = _lt / 8;
                                //late by hour
                                totLateHrs += _lt;
                                _abs = (att.absent.am + att.absent.pm) / 8;
                                totAbsent += _abs;
                                rem = att.remarks;
                            }

                            if(lv!=null) {

                                if(lv.withPay!=null) {
                                    var _f =  Convert.ToDateTime(f.Date.ToString("MM/dd/yyyy") + " " + u.employeeSchedules[0].timeInAm);
                                    var _t =  Convert.ToDateTime(f.Date.ToString("MM/dd/yyyy") + " " + u.employeeSchedules[0].timeOutPm);
                                    if(f.Date == lv.dates.from.Date) 
                                        _f = lv.dates.from;
                                    
                                    if(f.Date == lv.dates.to.Date)
                                        _t = lv.dates.to;
                                    
                                    var hours = (_t - _f).TotalHours;
                                    if(hours>9)
                                        hours = 9;
                                    
                                    var h = hours;
                                    if (hours > 4)
                                        h = (hours - 1) / 8;
                                    else
                                        h = hours / 8;

                                    
                                    if(lv.type == "Sick Leave") {
                                        if((bool)lv.withPay) {
                                            totSlWP += h;
                                            _slwp = h;
                                        } else {
                                            totSlWoP += h;
                                            _slwop = h;
                                        }
                                        _abs -= h;
                                    } else if(lv.type == "Vacation Leave") {
                                        if((bool)lv.withPay) {
                                            totVLWP += h;
                                            _vlwp = h;
                                        } else {
                                            totVLWoP += h;
                                            _vlwop = h;
                                        }
                                        _abs -= _vlwp;
                                    } else {
                                        _otl = h;
                                        totTLSPLPL += _otl;
                                        _abs -= _otl;
                                    }
                                }
                            }

                            if(_abs>0) {
                                if(_vlwp + _slwp + _otl > 0) {
                                    totAbsent -= _abs;
                                    _abs = 0;
                                }

                            } else {
                                if(_vlwp + _slwp + _otl > 0) {
                                    totUtWoP -= _utwop;
                                    _utwop = 0;
                                }
                                
                            }

                            // totPerEmpReg += _reg;
                            // totPerEmpNd += _nd;
                            // totPerEmpRd += _rd;
                            // totPerEmpExcessRd = _excRd;
                            // totPerEmpRdNd = _rdnd;
                            // totPerEmpVLWP = _vlwp;
                            // totPerEmpSlWP = _slwp;
                            // totPerEmpVLWoP = _vlwop;
                            // totPerEmpSlWoP = _slwop;
                            // totPerEmpAbsent = _abs;
                            //totPerEmpTLSPLPL = 0;
                            //totPerEmpUtWP = 0;
                            totPerEmpUtWoP = _utwop;
                            totPerEmpLateHrs = _lt;
                            
                            

                            if(_reg+_nd+_rd+_excRd+_rdnd > 0) {
                                otabs.Add(String.Format(
                                    "ot,{0},{1},{2},{3},{4},{5},{6}",
                                    _reg,_nd,_rd,_excRd,_rdnd,f,rem)
                                );
                            }

                            if(_vlwp+_slwp+_vlwop+_slwop > 0) {
                                otabs.Add(String.Format(
                                    "lv,{0},{1},{2},{3},{4},{5}",
                                    _vlwp,_slwp,_vlwop,_slwop,f,rem)
                                );
                            }

                            if(_abs>0) {
                                otabs.Add(String.Format(
                                    "abs,{0},{1},{2}",
                                    _abs,f,rem)
                                );
                            }
                            if(_otl>0) {
                                otabs.Add(String.Format(
                                    "otl,{0},{1},{2}",
                                    _otl,f,rem)
                                );
                            }
                            

                            if(_utwop > 0) {
                                otabs.Add(String.Format(
                                    "ut,{0},{1},{2},{3}",
                                    0,_utwop,f,rem)
                                );
                            }

                            if(_lt>0) {
                                otabs.Add(String.Format(
                                    "lt,{0},{1},{2}",
                                    _lt,f,rem)
                                );
                            }
                        }

                        // ot
                        ew.Cells[liner,4].Value = String.Format("{0:0.000;-0.000;-}", (from x in otabs where x.Split(',')[0] == "ot" select x.Split(',')[1]).Sum(x=> Convert.ToDouble(x)));
                        ew.Cells[liner,5].Value = String.Format("{0:0.000;-0.000;-}", (from x in otabs where x.Split(',')[0] == "ot" select x.Split(',')[2]).Sum(x=> Convert.ToDouble(x)));
                        ew.Cells[liner,6].Value = String.Format("{0:0.000;-0.000;-}", (from x in otabs where x.Split(',')[0] == "ot" select x.Split(',')[3]).Sum(x=> Convert.ToDouble(x)));
                        ew.Cells[liner,7].Value = String.Format("{0:0.000;-0.000;-}", (from x in otabs where x.Split(',')[0] == "ot" select x.Split(',')[4]).Sum(x=> Convert.ToDouble(x)));
                        ew.Cells[liner,8].Value = String.Format("{0:0.000;-0.000;-}", (from x in otabs where x.Split(',')[0] == "ot" select x.Split(',')[5]).Sum(x=> Convert.ToDouble(x)));

                        var startliner = liner;
                        var highestline = liner;
                        // "lv,{0},{1},{2},{3}"
                        foreach(String hey in from x in otabs where x.Split(',')[0] == "lv" select x) {
                            String[] val = hey.Split(',');
                            ew.Cells[startliner,12].Value = String.Format("{0:0.000;-0.000;-}", Convert.ToDouble(val[1]));
                            ew.Cells[startliner,13].Value = String.Format("{0:0.000;-0.000;-}", Convert.ToDouble(val[2]));
                            ew.Cells[startliner,14].Value = String.Format("{0:0.000;-0.000;-}", Convert.ToDouble(val[3]));
                            ew.Cells[startliner,15].Value = String.Format("{0:0.000;-0.000;-}", Convert.ToDouble(val[4]));
                            ew.Cells[startliner,16].Value = String.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(val[5]));
                            
                            startliner++;
                            if (startliner > highestline) {
                                highestline = startliner;
                            }
                        }


                        startliner = liner;
                        // "abs,{0},{1},{2}"
                        foreach(String hey in from x in otabs where x.Split(',')[0] == "abs" select x) {
                            String[] val = hey.Split(',');
                            ew.Cells[startliner,17].Value = String.Format("{0:0.000;-0.000;-}", Convert.ToDouble(val[1]));
                            ew.Cells[startliner,18].Value = String.Format("{0:MM/dd/yyyy}",Convert.ToDateTime(val[2]));
                            //ew.Cells[startliner,26].Value = val[3];
                            
                            startliner++;
                            if (startliner > highestline) {
                                highestline = startliner;
                            }
                        }
                        startliner = liner;
                        // "otl,{0},{1}"
                        foreach(String hey in from x in otabs where x.Split(',')[0] == "otl" select x) {
                            String[] val = hey.Split(',');
                            ew.Cells[startliner,19].Value = String.Format("{0:0.000;-0.000;-}", Convert.ToDouble(val[1]));
                            ew.Cells[startliner,20].Value = String.Format("{0:MM/dd/yyyy}",Convert.ToDateTime(val[2]));
                            //ew.Cells[startliner,26].Value = val[3];
                            
                            startliner++;
                            if (startliner > highestline) {
                                highestline = startliner;
                            }
                        }
                        startliner = liner;
                        // "ut,{0},{1},{2},{3}"
                        foreach(String hey in from x in otabs where x.Split(',')[0] == "ut" select x) {
                            String[] val = hey.Split(',');
                            ew.Cells[startliner,21].Value = String.Format("{0:0.000;-0.000;-}", Convert.ToDouble(val[1]));
                            ew.Cells[startliner,22].Value = String.Format("{0:0.000;-0.000;-}", Convert.ToDouble(val[2]));
                            ew.Cells[startliner,23].Value = String.Format("{0:MM/dd/yyyy}",Convert.ToDateTime(val[3]));
                            //ew.Cells[startliner,26].Value = val[4];
                            
                            startliner++;   
                            if (startliner > highestline) {
                                highestline = startliner;
                            }
                        }

                        startliner = liner;
                        // "lt,{0},{1},{2}"
                        foreach(String hey in from x in otabs where x.Split(',')[0] == "lt" select x) {
                            String[] val = hey.Split(',');
                            ew.Cells[startliner,24].Value = String.Format("{0:0.000;-0.000;-}", Convert.ToDouble(val[1]));
                            ew.Cells[startliner,25].Value = String.Format("{0:MM/dd/yyyy}",Convert.ToDateTime(val[2]));
                            //ew.Cells[startliner,26].Value = val[3];
                            startliner++;
                            if (startliner > highestline) {
                                highestline = startliner;
                            }
                        }
                        

                        //lining
                        for(var l = liner; l<=highestline;l++) {
                            ew.Cells[l,4,l,4].Style.Font.Name = "Trebuchet MS";
                            ew.Cells[l,4,l,26].Style.Font.Size = 11;

                            ew.Cells[l,4,l,26].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                            ew.Cells[l,4,l,26].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;

                            ew.Cells[l,4,l,26].Style.HorizontalAlignment  = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                            ew.Cells[l,1,l,26].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.DashDot;
                        }
                        
                        liner = highestline;
                        ew.Cells[liner,1,liner,26].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                        liner++;
                        ew.Cells[liner,1,liner,26].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                        // ew.Cells[liner,2,liner,2].Value = "SUB TOTAL";
                        // ew.Cells[liner,4,liner,26].Value = String.Format("{0:0.00;-0.00;-}", totPerEmpReg);
                        // ew.Cells[liner,5,liner,26].Value = String.Format("{0:0.00;-0.00;-}", totPerEmpNd);
                        // ew.Cells[liner,6,liner,26].Value = String.Format("{0:0.00;-0.00;-}", totPerEmpRd);
                        // ew.Cells[liner,7,liner,26].Value = String.Format("{0:0.00;-0.00;-}", totPerEmpExcessRd);
                        // ew.Cells[liner,8,liner,26].Value = String.Format("{0:0.00;-0.00;-}", totPerEmpRdNd);
                        // ew.Cells[liner,12,liner,26].Value = String.Format("{0:0.00;-0.00;-}", totPerEmpVLWP);
                        // ew.Cells[liner,13,liner,26].Value = String.Format("{0:0.00;-0.00;-}", totPerEmpVLWoP);
                        // ew.Cells[liner,14,liner,26].Value = String.Format("{0:0.00;-0.00;-}", totPerEmpSlWP);
                        // ew.Cells[liner,15,liner,26].Value = String.Format("{0:0.00;-0.00;-}", totPerEmpSlWoP);
                        // ew.Cells[liner,17,liner,26].Value = String.Format("{0:0.00;-0.00;-}", totPerEmpAbsent);
                        // ew.Cells[liner,19,liner,26].Value = String.Format("{0:0.00;-0.00;-}", totPerEmpTLSPLPL);
                        // ew.Cells[liner,21,liner,26].Value = String.Format("{0:0.00;-0.00;-}", totPerEmpUtWP);
                        // ew.Cells[liner,22,liner,26].Value = String.Format("{0:0.00;-0.00;-}", totPerEmpUtWoP);
                        // ew.Cells[liner,24,liner,26].Value = String.Format("{0:0.00;-0.00;-}", totPerEmpLateHrs);

                        // ew.Cells[liner,4,liner,26].Style.Font.Name = "Trebuchet MS";
                        // ew.Cells[liner,4,liner,26].Style.Font.Size = 11;
                        // ew.Cells[liner,4,liner,26].Style.HorizontalAlignment  = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                        // ew.Cells[liner,4,liner,26].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                        // ew.Cells[liner,4,liner,26].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;

                        // ew.Cells[liner,1,liner,26].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        // ew.Cells[liner,1,liner,26].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(0, 217, 0));
                        // ew.Cells[liner,4,liner,26].Style.Font.Bold = true;
                         liner++;

                    }

                    grandTotReg += totReg;
                    grandTotNd += totNd;
                    grandTotRd += totRd;
                    grandTotExcessRd += totExcessRd;
                    grandTotRdNd += totRdNd;
                    grandTotVLWP += totVLWP;
                    grandTotSlWP += totSlWP;
                    grandTotVLWoP += totVLWoP;
                    grandTotSlWoP += totSlWoP;
                    grandTotAbsent += totAbsent;
                    grandTotTLSPLPL += totTLSPLPL;
                    grandTotUtWP += totUtWP;
                    grandTotUtWoP += totUtWoP;
                    grandTotLateHrs += totLateHrs;

                    ew.Cells[liner,2].Value = "TOTAL";
                    ew.Cells[liner,4].Value = String.Format("{0:0.000;-0.000;-}", totReg);
                    ew.Cells[liner,5].Value = String.Format("{0:0.000;-0.000;-}", totNd);
                    ew.Cells[liner,6].Value = String.Format("{0:0.000;-0.000;-}", totRd);
                    ew.Cells[liner,7].Value = String.Format("{0:0.000;-0.000;-}", totExcessRd);
                    ew.Cells[liner,8].Value = String.Format("{0:0.000;-0.000;-}", totRdNd);
                    ew.Cells[liner,12].Value = String.Format("{0:0.000;-0.000;-}", totVLWP);
                    ew.Cells[liner,13].Value = String.Format("{0:0.000;-0.000;-}", totSlWP);
                    ew.Cells[liner,14].Value = String.Format("{0:0.000;-0.000;-}", totVLWoP);
                    ew.Cells[liner,15].Value = String.Format("{0:0.000;-0.000;-}", totSlWoP);
                    ew.Cells[liner,17].Value = String.Format("{0:0.000;-0.000;-}", totAbsent);
                    ew.Cells[liner,19].Value = String.Format("{0:0.000;-0.000;-}", totTLSPLPL);
                    ew.Cells[liner,21].Value = String.Format("{0:0.000;-0.000;-}", totUtWP);
                    ew.Cells[liner,22].Value = String.Format("{0:0.000;-0.000;-}", totUtWoP);
                    ew.Cells[liner,24].Value = String.Format("{0:0.000;-0.000;-}", totLateHrs);

                    ew.Cells[liner,4,liner,26].Style.Font.Name = "Trebuchet MS";
                    ew.Cells[liner,4,liner,26].Style.Font.Size = 11;
                    ew.Cells[liner,4,liner,25].Style.HorizontalAlignment  = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                    ew.Cells[liner,4,liner,26].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                    ew.Cells[liner,4,liner,26].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;

                    ew.Cells[liner,1,liner,26].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    ew.Cells[liner,1,liner,26].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(221, 217, 196));
                    ew.Cells[liner,4,liner,26].Style.Font.Bold = true;
                    liner++;
                }

                ew.Cells[liner,2].Value = "GRAND TOTAL";
                ew.Cells[liner,4].Value = String.Format("{0:0.000;-0.000;-}", grandTotReg);
                ew.Cells[liner,5].Value = String.Format("{0:0.000;-0.000;-}", grandTotNd);
                ew.Cells[liner,6].Value = String.Format("{0:0.000;-0.000;-}", grandTotRd);
                ew.Cells[liner,7].Value = String.Format("{0:0.000;-0.000;-}", grandTotExcessRd);
                ew.Cells[liner,8].Value = String.Format("{0:0.000;-0.000;-}", grandTotRdNd);
                ew.Cells[liner,12].Value = String.Format("{0:0.000;-0.000;-}", grandTotVLWP);
                ew.Cells[liner,13].Value = String.Format("{0:0.000;-0.000;-}", grandTotSlWP);
                ew.Cells[liner,14].Value = String.Format("{0:0.000;-0.000;-}", grandTotVLWoP);
                ew.Cells[liner,15].Value = String.Format("{0:0.000;-0.000;-}", grandTotSlWoP);
                ew.Cells[liner,17].Value = String.Format("{0:0.000;-0.000;-}", grandTotAbsent);
                ew.Cells[liner,19].Value = String.Format("{0:0.000;-0.000;-}", grandTotTLSPLPL);
                ew.Cells[liner,21].Value = String.Format("{0:0.000;-0.000;-}", grandTotUtWP);
                ew.Cells[liner,22].Value = String.Format("{0:0.000;-0.000;-}", grandTotUtWoP);
                ew.Cells[liner,24].Value = String.Format("{0:0.000;-0.000;-}", grandTotLateHrs);

                ew.Cells[liner,1,liner,26].Style.Font.Name = "Trebuchet MS";
                ew.Cells[liner,1,liner,26].Style.Font.Size = 11;
                ew.Cells[liner,4,liner,25].Style.HorizontalAlignment  = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                ew.Cells[liner,1,liner,26].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                ew.Cells[liner,1,liner,26].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;

                ew.Cells[liner,1,liner,26].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                ew.Cells[liner,1,liner,26].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(225, 225, 0));

                ew.Cells[liner,1,liner,26].Style.Font.Bold = true;
                liner+=2;
                ew.Cells[liner,3].Value = "Prepared by:";
                ew.Cells[liner,13].Value = "Checked by:";
                ew.Cells[liner,20].Value = "Approved by:";
                ew.Row(liner).Style.Font.Name = "Trebuchet MS";
                ew.Row(liner).Style.Font.Size = 10;
                liner+=2;
                ew.Cells[liner,3].Value = "BERNADETTE B. LEY:";
                ew.Cells[liner,13].Value = "EVELYN Y. CADAG";
                ew.Cells[liner,20].Value = "SHARON O. DIONCO";
                ew.Row(liner).Style.Font.Name = "Trebuchet MS";
                ew.Row(liner).Style.Font.Size = 10;
                ew.Row(liner).Style.Font.Bold = true;
                liner++;
                ew.Cells[liner,3].Value = "HC Assistant,TRMD";
                ew.Cells[liner,13].Value = "HC Officer, TRMD";
                ew.Cells[liner,20].Value = "Acting Director, Human Capital Department";
                ew.Row(liner).Style.Font.Name = "Trebuchet MS";
                ew.Row(liner).Style.Font.Size = 10;
                ew.Row(liner).Style.Font.Italic = true;

                // formatting
                ew.Column(1).Width = 3.83;
                ew.Column(2).Width = 13.67;
                ew.Column(3).Width = 29.83;
                ew.Column(4).Width = 8.22;
                ew.Column(5).Width = 6.67;
                ew.Column(6).Width = 7.94;
                ew.Column(7).Width = 8.11;
                ew.Column(8).Width = 6.67;
                ew.Column(9).Width = 0;
                ew.Column(10).Width = 0;
                ew.Column(11).Width = 0;
                ew.Column(12).Width = 7.22;
                ew.Column(13).Width = 6.39;
                ew.Column(14).Width = 7.56;
                ew.Column(15).Width = 5.22;
                ew.Column(16).Width = 13.94;
                ew.Column(17).Width = 8.22;
                ew.Column(18).Width = 13.11;
                ew.Column(19).Width = 11.67;
                ew.Column(20).Width = 10.11;
                ew.Column(21).Width = 6.39;
                ew.Column(22).Width = 7.22;
                ew.Column(23).Width = 10.11;
                ew.Column(24).Width = 7.39;
                ew.Column(25).Width = 10.83;
                ew.Column(26).Width = 32.94;

                ew.Cells["A1:Z9"].Style.Font.Name = "Trebuchet MS";
                ew.Cells["A1:Z2"].Style.Font.Size = 12;
                ew.Cells["A3:Z3"].Style.Font.Size = 14;
                ew.Cells["A4:Z5"].Style.Font.Size = 12;
                ew.Cells["A6:Z9"].Style.Font.Size = 11;

                ew.Cells["A6:Z8"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ew.Cells["A6:Z8"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                ew.Cells["A6:Z8"].Style.WrapText = true;
                ew.Cells["A6:Z8"].Style.Font.Bold = true;



                ew.Cells["A6:Z8"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                ew.Cells["A6:Z8"].Style.Font.Bold = true;

                ew.Cells["A6:Z6"].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium);
                ew.Cells["A6:Z6"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                ew.Cells["A6:Z6"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;

                ew.Cells["A7:Z7"].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium);
                ew.Cells["A7:Z7"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                ew.Cells["A7:Z7"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;

                ew.Cells["A8:Z8"].Style.Border.BorderAround(OfficeOpenXml.Style.ExcelBorderStyle.Medium);
                ew.Cells["A8:Z8"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                ew.Cells["A8:Z8"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;

                //free pane on row 8 column C
                ew.View.FreezePanes(9, 4);

                //Travel Leave / Solo Parent / Paternity
                ew.Cells["S6:S8"].Style.Font.Size = 8;
                //Undertime under
                ew.Cells["U7:U8"].Style.Font.Size = 7;
                ew.Cells["V7:V8"].Style.Font.Size = 7;
                bytes = ep.GetAsByteArray();
                ep.Dispose();
            }

            var link = _upload.upload(bytes,"kmbidevreporeports/" + filename);
            return Redirect(link);
        }

        [HttpGet("ledgerPerWeek")]
        public IActionResult getLedgerPerClient([FromQuery]string referenceId)
        {
            var rep = _reporting.getLedgerPerWeek(referenceId);

            _rep.templateFilename = "ledgerPerWeek.cshtml";
            _rep.model = rep;
            _rep.orientation = "landscape";
            _rep.footerText = "System Generated Report - Microfinance System - " + DateTime.Now;
            _rep.title = "Ledger Per Week";
            _rep.isHalf = false;
            _rep.pageType = "letter";
            // dynamic vb = new System.Dynamic.ExpandoObject();
            // vb.dateFrom = from;
            // vb.dateTo = to;
            //_rep.viewBag = vb;
            iResult res = _rep.generatePDF();

            var link = _upload.upload(res.bytes,"kmbidevreporeports/" + referenceId + " - Ledger (Per Week).pdf");
            return Redirect(link);
        }

        [HttpGet("ledgerPerClientPerWeek")]
        public IActionResult getLedgerPerClient([FromQuery]String referenceId, [FromQuery]Int32 weekNo)
        {
            var rep = _reporting.getLedgerPerClientPerWeek(referenceId, weekNo);

            _rep.templateFilename = "ledgerPerClientPerWeek.cshtml";
            _rep.model = rep;
            _rep.orientation = "landscape";
            _rep.footerText = "System Generated Report - Microfinance System - " + DateTime.Now;
            _rep.title = "Ledger Per Client Per Week";
            _rep.isHalf = false;
            _rep.pageType = "letter";
            // dynamic vb = new System.Dynamic.ExpandoObject();
            // vb.dateFrom = from;
            // vb.dateTo = to;
            //_rep.viewBag = vb;
            iResult res = _rep.generatePDF();

            var link = _upload.upload(res.bytes,"kmbidevreporeports/" + referenceId + " - Ledger (Per Client Per Week).pdf");
            return Redirect(link);
        }

        [HttpGet("payrollSummary")]
        public IActionResult payrollSummary([FromQuery]DateTime? cutoffFrom, [FromQuery]DateTime? cutoffTo, [FromQuery]Int32 jobGrade=0, [FromQuery]String branch = null, [FromQuery]String type=null) {
            var err = new List<String>();

            if(branch==null)
                err.Add("branch is required");

            if(cutoffFrom==null)
                err.Add("cut-off from is required");

            if(cutoffTo==null)
                err.Add("cut-off to is required");

            if(jobGrade>3)
                err.Add("Job grade must be either 1, 2 or 3");

            if(err.Count > 0 )
                type = "error";

            repPayrollModel item = _reporting.getPayroll(Convert.ToDateTime(cutoffFrom), Convert.ToDateTime(cutoffTo), jobGrade, branch);

            switch(type) {
                case "error":
                    return BadRequest(new {type="error", message = String.Join(",",err)});
                case "data":
                    return Ok(item);
                case "excel":
                    var excelLink = "";

                    using (OfficeOpenXml.ExcelPackage ep = new OfficeOpenXml.ExcelPackage()) {

                        ExcelWorksheet ew = ep.Workbook.Worksheets.Add("Payroll Summary");

                        ew.Cells["A1"].Value = "KABALIKAT PARA SA MAUNLAD NA BUHAY, INC.";
                        ew.Cells["A2"].Value = item.jobGradeDesc;
                        ew.Cells["A3"].Value = "Payroll Summary";
                        ew.Cells["A4"].Value = "For the period of " + item.periodFrom.ToString("MMMM dd") + " - " + item.periodFrom.ToString("dd, yyyy");
                        ew.Cells["A5"].Value = "Cut-off: " + item.cutOffFrom.ToString("MMMM dd")  + " - " + item.cutOffTo.ToString("MMMM dd, yyyy");

                        //1-15
                        var lastLine = 0;
                        ew.Cells["A6"].Value = "";
                        ew.Cells["B6"].Value = "DEPT";
                        ew.Cells["C6"].Value = "NAME OF EMPLOYEE";
                        ew.Cells["D6"].Value = "TAX CODE";
                        ew.Cells["E6"].Value = "MONTHLY RATE";
                        ew.Cells["F6"].Value = "RATE/DAY";
                        ew.Cells["G6"].Value = "PERIOD";
                        ew.Cells["H6"].Value = "BASIC";
                        ew.Cells["I6"].Value = "ADJUSTMENT";
                        ew.Cells["J6"].Value = "HOLIDAY PAY";
                        ew.Cells["K6"].Value = "OVERTIME PAY";
                        ew.Cells["L6"].Value = "ABSENT / LATE";
                        
                        ew.Cells["M6"].Value = "GROSS TAXABLE INCOME";
                        
                        ew.Cells["N6"].Value = "RICE SUBSIDY";
                        ew.Cells["O6"].Value = "AP";
                        
                        ew.Cells["P6"].Value = "GROSS PAY";

                        ew.Cells["Q6"].Value = "WITHOLDING TAX";

                        ew.Cells["R6"].Value = "HDMF PREMIUM CONTRIBUTION";
                        ew.Cells["S6"].Value = "HDMF CALAMITY";
                        ew.Cells["T6"].Value = "HDMF HOUSING";
                        ew.Cells["U6"].Value = "HDMF MPL";
                        ew.Cells["V6"].Value = "HDMF MP2";
                        
                        ew.Cells["W6"].Value = "SSS PREMIUM CONTRIBUTION";
                        ew.Cells["X6"].Value = "SSS SALARY LOAN";

                        ew.Cells["Y6"].Value = "PHILHEALTH CONTRIBUTION";

                        ew.Cells["Z6"].Value = "FIDELITY";
                        ew.Cells["AA6"].Value = "FHG LOAN PAYABLE";
                        ew.Cells["AB6"].Value = "FHG SHARE CAPITAL";
                        ew.Cells["AC6"].Value = "FHG SAVINGS";
                        ew.Cells["AD6"].Value = "FHG INSURANCE";
                        ew.Cells["AE6"].Value = "FHG OTHERS";

                        ew.Cells["AF6"].Value = "AR";
                        ew.Cells["AG6"].Value = "AR OTHERS";
                        ew.Cells["AH6"].Value = "AR MISC";
                        ew.Cells["AI6"].Value = "AR PERSONAL CALLS";
                        
                        ew.Cells["AJ6"].Value = "STATUTORY";
                        
                        ew.Cells["AK6"].Value = "TOTAL DEDUCTION";
                        
                        ew.Cells["AL6"].Value = "NET PAY";
                        
                        ew.Cells["AM6"].Value = "";

                        var liner = 7;

                        ew.Cells[6,1, 6,37].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                        ew.Cells[6,1, 6,37].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                        ew.Cells[6,1, 6,37].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                        ew.Cells[6,1, 6,37].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                        ew.Row(6).Height = 44;
                        ew.Cells[6,1, 6,37].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        ew.Cells[6,1, 6,37].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            
                        item.details.ForEach(p=>{
                            ew.Cells[liner,1].Value = liner-6;
                            ew.Cells[liner,2].Value = _reporting.getDepartmentAcro(p.department); //DEPT
                            ew.Cells[liner,3].Value = p.fullname; //NAME OF EMPLOYEE
                            ew.Cells[liner,4].Value = p.taxCode; //TAX CODE
                            ew.Cells[liner,1, liner,31].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            ew.Cells[liner,5].Value = p.monthlyRate; //MONTHLY RATE
                            ew.Cells[liner,6].Value = p.ratePerDay; //RATE/DAY
                            ew.Cells[liner,7].Value = item.cutOffFrom.ToString("MMMM dd") + " - " + item.cutOffTo.ToString("dd, yyyy"); //PERIOD
                            ew.Cells[liner,8].Value = p.basicPay; //BASIC
                            ew.Cells[liner,9].Value = p.adjustment; //ADJUSTMENT
                            ew.Cells[liner,10].Value = p.holidayPay; //HOLIDAY PAY
                            ew.Cells[liner,11].Value = p.overtimePay; //OVERTIME PAY
                            ew.Cells[liner,12].Value = p.absentLate; //ABSENT / LATE
                            
                            ew.Cells[liner,13].Value = p.grossTaxableIncome;//GROSS TAXABLE INCOME

                            ew.Cells[liner,14].Value = p.riceSubsidy; //RICE SUBSIDY
                            ew.Cells[liner,15].Value = p.ap; //AP

                            ew.Cells[liner,16].Value = p.grossPay; //GROSS PAY

                            ew.Cells[liner,17].Value = p.witholdingTax; //WITHOLDING TAX

                            ew.Cells[liner,18].Value = p.hdmfPremiumContribution; //HDMF PREMIUM CONTRIBUTION
                            ew.Cells[liner,19].Value = p.hdmfCalamity; //HDMF CALAMITY
                            ew.Cells[liner,20].Value = p.hdmfHousing; //HDMF HOUSING
                            ew.Cells[liner,21].Value = p.hdmfMpl; //HDMF MPL
                            ew.Cells[liner,22].Value = p.hdmfMp2; //HDMF MP2

                            ew.Cells[liner,23].Value = p.sssPremiumContribution; //SSS PREMIUM CONTRIBUTION
                            ew.Cells[liner,24].Value = p.sssSalaryLoan; //SSS SALARY LOAN

                            ew.Cells[liner,25].Value = p.phicPremiumContribution; //PHILHEALTH CONTRIBUTION
                            ew.Cells[liner,26].Value = p.fidelity; //FIDELITY
                            ew.Cells[liner,27].Value = p.fhgLoanPayable; //FHG LOAN PAYABLE
                            ew.Cells[liner,28].Value = p.fhgShareCapital; //FHG SHARE CAPITAL
                            ew.Cells[liner,29].Value = p.fhgSavings; //FHG SAVINGS
                            ew.Cells[liner,30].Value = p.fhgInsurance; //FHG INSURANCE
                            ew.Cells[liner,31].Value = p.arOthers; //FHG OTHERS

                            ew.Cells[liner,32].Value = p.ar; // AR
                            ew.Cells[liner,33].Value = p.arOthers; // AR OTHERS
                            ew.Cells[liner,34].Value = p.arMisc; // AR MISC
                            ew.Cells[liner,35].Value = p.arPersonalCalls; // AR PERSONAL CALLS

                            ew.Cells[liner,36].Value = p.statutory; // STATUTORY
                            
                            ew.Cells[liner,37].Value = p.totalDeduction;//TOTAL DEDUCTION

                            ew.Cells[liner,38].Value = p.netPay; //NET PAY

                            ew.Cells[liner,39].Value = liner-6;

                            ew.Cells[liner,1, liner,38].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                            ew.Cells[liner,1, liner,38].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                            ew.Cells[liner,1, liner,38].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                            ew.Cells[liner,1, liner,38].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                            ew.Cells[liner,5, liner,6].Style.Numberformat.Format = "#,##0.00;(#,##0.00);-";
                            ew.Cells[liner,8, liner,38].Style.Numberformat.Format = "#,##0.00;(#,##0.00);-";
                            liner++;
                        });

                        //total

                        // ew.Cells[liner,1].Value = liner-6;
                        ew.Cells[liner,2].Value = "";
                        ew.Cells[liner,3].Value = "";
                        ew.Cells[liner,4].Value = "TOTAL";
                        ew.Cells[liner,1, liner,31].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        ew.Cells[liner,5].Value = item.details.Sum(x=>x.monthlyRate); //MONTHLY RATE
                        ew.Cells[liner,6].Value = item.details.Sum(x=>x.ratePerDay); //RATE/DAY
                        ew.Cells[liner,7].Value = item.cutOffFrom.ToString("MMMM dd") + " - " + item.cutOffTo.ToString("dd, yyyy"); //PERIOD
                        ew.Cells[liner,8].Value = item.details.Sum(x=>x.basicPay); //BASIC
                        ew.Cells[liner,9].Value = item.details.Sum(x=>x.adjustment); //ADJUSTMENT
                        ew.Cells[liner,10].Value = item.details.Sum(x=>x.holidayPay); //HOLIDAY PAY
                        ew.Cells[liner,11].Value = item.details.Sum(x=>x.overtimePay); //OVERTIME PAY
                        ew.Cells[liner,12].Value = item.details.Sum(x=>x.absentLate); //ABSENT / LATE
                        
                        ew.Cells[liner,13].Value = item.details.Sum(x=>x.grossTaxableIncome);//GROSS TAXABLE INCOME

                        ew.Cells[liner,14].Value = item.details.Sum(x=>x.riceSubsidy); //RICE SUBSIDY
                        ew.Cells[liner,15].Value = item.details.Sum(x=>x.ap); //AP

                        ew.Cells[liner,16].Value = item.details.Sum(x=>x.grossPay); //GROSS PAY

                        ew.Cells[liner,17].Value = item.details.Sum(x=>x.witholdingTax); //WITHOLDING TAX

                        ew.Cells[liner,18].Value = item.details.Sum(x=>x.hdmfPremiumContribution); //HDMF PREMIUM CONTRIBUTION
                        ew.Cells[liner,19].Value = item.details.Sum(x=>x.hdmfCalamity); //HDMF CALAMITY
                        ew.Cells[liner,20].Value = item.details.Sum(x=>x.hdmfHousing); //HDMF HOUSING
                        ew.Cells[liner,21].Value = item.details.Sum(x=>x.hdmfMpl); //HDMF MPL
                        ew.Cells[liner,22].Value = item.details.Sum(x=>x.hdmfMp2); //HDMF MP2

                        ew.Cells[liner,23].Value = item.details.Sum(x=>x.sssPremiumContribution); //SSS PREMIUM CONTRIBUTION
                        ew.Cells[liner,24].Value = item.details.Sum(x=>x.sssSalaryLoan); //SSS SALARY LOAN

                        ew.Cells[liner,25].Value = item.details.Sum(x=>x.phicPremiumContribution); //PHILHEALTH CONTRIBUTION

                        ew.Cells[liner,26].Value = item.details.Sum(x=>x.fidelity); //FIDELITY
                        ew.Cells[liner,27].Value = item.details.Sum(x=>x.fhgLoanPayable); //FHG LOAN PAYABLE
                        ew.Cells[liner,28].Value = item.details.Sum(x=>x.fhgShareCapital); //FHG SHARE CAPITAL
                        ew.Cells[liner,29].Value = item.details.Sum(x=>x.fhgSavings); //FHG SAVINGS
                        ew.Cells[liner,30].Value = item.details.Sum(x=>x.fhgInsurance); //FHG INSURANCE
                        ew.Cells[liner,31].Value = item.details.Sum(x=>x.fhgOthers); //FHG OTHERS

                        ew.Cells[liner,32].Value = item.details.Sum(x=>x.ar); // AR
                        ew.Cells[liner,33].Value = item.details.Sum(x=>x.arOthers); // AR OTHERS
                        ew.Cells[liner,34].Value = item.details.Sum(x=>x.arMisc); // AR MISC
                        ew.Cells[liner,35].Value = item.details.Sum(x=>x.arPersonalCalls); // AR PERSONAL CALLS

                        ew.Cells[liner,36].Value = item.details.Sum(x=>x.statutory); // STATUTORY
                        
                        ew.Cells[liner,37].Value = item.details.Sum(x=>x.totalDeduction);//TOTAL DEDUCTION

                        ew.Cells[liner,38].Value = item.details.Sum(x=>x.netPay); //NET PAY

                        // ew.Cells[liner,38].Value = liner-6;

                        ew.Cells[liner,1, liner,38].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                        ew.Cells[liner,1, liner,38].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                        ew.Cells[liner,1, liner,38].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                        ew.Cells[liner,1, liner,38].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Medium;
                        ew.Cells[liner,5, liner,6].Style.Numberformat.Format = "#,##0.00;(#,##0.00);-";
                        ew.Cells[liner,8, liner,38].Style.Numberformat.Format = "#,##0.00;(#,##0.00);-";
                        
                        lastLine = liner;

                        //format
                        var allCells = ew.Cells[1,1,lastLine,31];
                        var cellFont = allCells.Style.Font;
                        cellFont.SetFromFont(new Font("Trebuchet MS", 11));
                        
                        // cellFont.Bold = true;
                        // cellFont.Italic = true;

                        //width def
                        Int32[] widthDef = new Int32[]{5,7,35,10,16,13,0,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,0,20,5};

                        for(Int32 ctr=0;ctr<=37;ctr++) {
                            ew.Column(ctr+1).Width = widthDef[ctr];
                        }
                        
                        ew.View.FreezePanes(6, 4);

                        var filename = "payrollSummary-" + item.cutOffFrom.ToString("MMMM-dd-yyyy") + "-" + item.cutOffTo.ToString("MMMM-dd-yyyy");
                        byte[] bytes;

                        bytes = ep.GetAsByteArray();
                        ep.Dispose();

                        excelLink = _upload.upload(bytes,"kmbidevreporeports/" + filename + ".xlsx");
                    }

                    return Redirect(excelLink);
                default:
                    var pdfLink = "";

                    _rep.templateFilename = "payrollSummary.cshtml";
                    _rep.model = item;
                    _rep.orientation = "landscape";
                    _rep.footerText = "System Generated Report - Payroll System - " + DateTime.Now;
                    _rep.title = "Payroll Summary " + item.cutOffFrom.ToString("MM-dd-yyyy") + " - " + item.cutOffTo.ToString("MM-dd-yyyy") + ")";
                    _rep.isHalf = false;
                    _rep.pageType = "legal";
                    iResult res = _rep.generatePDF();

                    pdfLink = _upload.upload(res.bytes,"kmbidevreporeports/" + _rep.title.Replace(" ","-").ToLower() + ".pdf");
                    return Redirect(pdfLink);
            }
        }

        class bankDef {
            public String name {get;set;}
            public String[] locations {get;set;}
            public ExcelHorizontalAlignment[] locationsAlignments {get;set;}
            public Font[] fonts {get;set;}
            public Double[] colWidths {get;set;}
            public Double[] rowHeights {get;set;}
            public decimal[] margins {get;set;}
        }

        [HttpGet("allowance")]
        public IActionResult allowance([FromQuery]String branchSlug = "", [FromQuery]Int32 jobGrade = -1, [FromQuery]Int32 month = 0, [FromQuery]Int32 year = 0) {
            
            byte[] bytes = new byte[]{};

            using (OfficeOpenXml.ExcelPackage ep = new OfficeOpenXml.ExcelPackage()) {
                List<users> users = _users.AllUsers().ToList();
                
                //jobgrade
                switch (jobGrade) {
                    case 0:
                        users =  (from x in users where 
                            x.employment.jobGrade == "1" || x.employment.jobGrade == "2"
                        select x).ToList();
                        break;
                    case 1:
                        users =  (from x in users where 
                            x.employment.jobGrade == "3" || x.employment.jobGrade == "4"
                        select x).ToList();
                        break;
                    case 2:
                        users =  (from x in users where 
                            x.employment.jobGrade == "5" || x.employment.jobGrade == "6"
                        select x).ToList();
                        break;
                    default:
                        users = new List<users>();
                        break;
                }

             

                //branch
                users = (from x in users where x.employment.branch == branchSlug select x).ToList();
                
                DateTime now = DateTime.Now.Date;
                ExcelWorksheet ew = ep.Workbook.Worksheets.Add("ALLOWANCES");

                ew.Cells["A1"].Value = "KABALIKAT PARA SA MAUNLAD NA BUHAY, INC.";
                ew.Cells["A2"].Value = "TOTAL Rewards Management Division";
                ew.Cells["A4"].Value = now.Year.ToString() + " Monthly Allowance";

                ew.Cells["A5:A7"].Merge = true;
                ew.Cells["A5:A7"].Value = "#";

                ew.Cells["B5:B7"].Merge = true;
                ew.Cells["B5:B7"].Value = "NAME";

                ew.Cells["D5:K5"].Merge = true;
                ew.Cells["D5:K5"].Value = Convert.ToDateTime(month.ToString() + "/1/1970").ToString("MMMM");

                ew.Cells["D6:D7"].Merge = true;
                ew.Cells["D6:D7"].Value = "Meal";

                ew.Cells["E6:E7"].Merge = true;
                ew.Cells["E6:E7"].Value = "FBT";

                ew.Cells["F6:F7"].Merge = true;
                ew.Cells["F6:F7"].Value = "Transportation";

                ew.Cells["G6:G7"].Merge = true;
                ew.Cells["G6:G7"].Value = "FBT";

                ew.Cells["H6:H7"].Merge = true;
                ew.Cells["H6:H7"].Value = "Higher Duty";

                ew.Cells["I6:I7"].Merge = true;
                ew.Cells["I6:I7"].Value = "FBT";

                ew.Cells["J6:K6"].Merge = true;
                ew.Cells["J6:K6"].Value = "TOTAL";

                ew.Cells["J7"].Value = "Allowance";
                ew.Cells["K7"].Value = "FBT";

                Int32 ctr = 1;
                Int32 liner = 8;

                Double[] matrix = new Double[6];

                users.ToList().ForEach(u=>{
                    ew.Cells[liner,1].Value = ctr;
                    ew.Cells[liner,2].Value = u.name.last + ", " + u.name.first + " " + u.extension + " " + u.name.middle.Substring(0,1) + ".";

                    var selectionDate = Convert.ToDateTime(month.ToString() + "/1/" + year.ToString());
                    var allowances = 
                        from x in u.employment.allowances
                            where 
                                selectionDate >= x.effectivityDateFrom && 
                                selectionDate <= x.effectivityDateTo
                        select x;

                    var meal = allowances.ToList().Find(b=>b.name == "MEAL ALLOWANCE");
                    var transpo = allowances.ToList().Find(b=>b.name == "TRANSPORTATION ALLOWANCE");
                    var higher = allowances.ToList().Find(b=>b.name == "HIGHER DUTY ALLOWANCE");

                    var mealAmount = meal!=null ? meal.amount : 0;
                    var transpoAmount = transpo!=null ? transpo.amount : 0;
                    var higherAmount = higher!=null ? higher.amount : 0;

                    matrix[0] += mealAmount;
                    matrix[1] += transpoAmount;
                    matrix[2] += higherAmount;

                    var mealFbt = Math.Round((mealAmount / 0.68) * .32,2);
                    var transpoFbt = Math.Round((transpoAmount / 0.68) * .32);
                    var higherFbt = Math.Round((higherAmount / 0.68) * .32);

                    matrix[3] += mealFbt;
                    matrix[4] += transpoFbt;
                    matrix[5] += higherFbt;

                    ew.Cells[liner,4].Value = mealAmount; // meal
                    ew.Cells[liner,5].Value = mealFbt; // meal fbt
                    ew.Cells[liner,6].Value = transpoAmount; // trans
                    ew.Cells[liner,7].Value = transpoFbt; // trans fbt
                    ew.Cells[liner,8].Value = higherAmount; // higher duty
                    ew.Cells[liner,9].Value = higherFbt; // higher duty fbt
                    ew.Cells[liner,10].Value = mealAmount + transpoAmount + higherAmount; // total allowance
                    ew.Cells[liner,11].Value = mealFbt + transpoFbt + higherFbt; // total fbt

                    liner++;
                    ctr++;
                });
                liner++;
                ew.Cells[liner,4,liner,9].Merge = true;
                ew.Cells[liner,4,liner,9].Value = Convert.ToDateTime(month.ToString() + "/1/1970").ToString("MMMM");

                ew.Cells[liner,10,liner,11].Merge = true;
                ew.Cells[liner,10,liner,11].Value = "TOTAL";

                liner++;

                ew.Cells[liner-1,1,liner,2].Merge = true;
                ew.Cells[liner-1,1,liner,2].Value = "MONTHLY ALLOWANCE";

                ew.Cells[liner,4].Value = "Meal";
                ew.Cells[liner,5].Value = "FBT";
                ew.Cells[liner,6].Value = "Transportation";
                ew.Cells[liner,7].Value = "FBT";
                ew.Cells[liner,8].Value = "Higher Duty";
                ew.Cells[liner,9].Value = "FBT";
                ew.Cells[liner,10].Value = "Allowance";
                ew.Cells[liner,11].Value = "FBT";
                liner++;

                ew.Cells[liner,1,liner,2].Merge = true;
                ew.Cells[liner,1,liner,2].Value = "TOTAL";

                ew.Cells[liner,4].Value = matrix[0];
                ew.Cells[liner,5].Value = matrix[1];
                ew.Cells[liner,6].Value = matrix[2];
                ew.Cells[liner,7].Value = matrix[3];
                ew.Cells[liner,8].Value = matrix[4];
                ew.Cells[liner,9].Value = matrix[5];
                ew.Cells[liner,10].Value = matrix[0] + matrix[1] + matrix[2];
                ew.Cells[liner,11].Value = matrix[3] + matrix[4] + matrix[5];


                liner+=5;

                ew.Cells[liner,2].Value = "Prepared by:";
                ew.Cells[liner,6].Value = "Approved by:";

                liner+=2;

                ew.Cells[liner,2].Value = "EVELYN Y. CADAG";
                ew.Cells[liner,6].Value = "SHARON O. DIONCO";
                liner++;
                ew.Cells[liner,2].Value = "HHC Officer";
                ew.Cells[liner,6].Value = "Acting Director, Human Capital Department";



                //formatting

                var colWidths = new[] {
                                4.05, 31.3, 0.23, 12.3, 12.05, 18.8, 11.05, 12.45, 11.95, 13.45, 12.45
                            };
                for(Int32 colCtr=0;colCtr<=colWidths.Length-1;colCtr++) {
                    ew.Column(colCtr+1).Width = colWidths[colCtr] + .58;
                }
                
                ew.Row(5).Height = 12;
                ew.Row(6).Height = 11.8;
                ew.Row(liner-11).Height = 8.1;

                ew.View.FreezePanes(8, 3);


                ew.Cells[1,1,7,11].Style.Font.SetFromFont(new Font("Century Gothic", 12));
                ew.Cells[8,1,liner-11,11].Style.Font.SetFromFont(new Font("Tahoma", 11));
                ew.Cells["A1"].Style.Font.Bold = true;
                ew.Cells["A2"].Style.Font.Italic = true;

                ew.Cells[8,2,liner-11,2].Style.Font.Bold = true;
                ew.Cells[liner-10,1,liner-9,11].Style.Font.Bold = true;

                ew.Cells[liner-10,1,liner,11].Style.Font.SetFromFont(new Font("Century Gothic", 12));
                ew.Cells[liner-10,1,liner - 9,11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells[liner-10,1,liner - 9,11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ew.Cells[liner-10,1,liner - 8,11].Style.Font.Bold = true;
                ew.Cells[liner-8,1,liner - 8,11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                ew.Cells["A5:K7"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                
                
                ew.Cells[liner-1,1,liner-1,11].Style.Font.Bold = true;

                for(Int32 rowCtr = 5;rowCtr<=liner-8;rowCtr++) {
                    ew.Cells[rowCtr,1,rowCtr,11].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    ew.Cells[rowCtr,1,rowCtr,11].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    ew.Cells[rowCtr,1,rowCtr,11].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    ew.Cells[rowCtr,1,rowCtr,11].Style.Border.Right.Style = ExcelBorderStyle.Thin;

                    ew.Cells[rowCtr,4, rowCtr,11].Style.Numberformat.Format = "#,##0.00;(#,##0.00);-";
                }
                bytes = ep.GetAsByteArray();
                ep.Dispose();
            };

            
            var link = _upload.upload(bytes,"kmbidevreporeports/allowanceReportSummary.xlsx");
            return Redirect(link);
        }


        


        [HttpGet("alphaList")]
        public IActionResult alphaList([FromQuery]Int32 year, [FromQuery]Int32? jobGrade, [FromQuery]String branchSlug, [FromQuery]Int32 test) {
            var filename = "alphalist-" + year.ToString();
            byte[] bytes;
            alphalist alphalist = _reporting.alphalist(year, branchSlug, Convert.ToInt32(jobGrade));

            using (OfficeOpenXml.ExcelPackage ep = new OfficeOpenXml.ExcelPackage()) {
                
                

                ExcelWorksheet grossEw = ep.Workbook.Worksheets.Add("GROSS");


                grossEw.Cells[1,1].Value = "KABALIKAT PARA SA MAUNLAD NA BUHAY, INC. (KMBI)";
                grossEw.Cells[2,1].Value = "HEAD OFFICE - " + alphalist.position.ToUpper();
                grossEw.Cells[3,1].Value = "SUMMARY OF GROSS COMPENSATION";
                grossEw.Cells[4,1].Value = "For The Taxable Year " + alphalist.year.ToString();

                var liner2 = 7;

                for(int ctr=4;ctr<=15;ctr++) {
                    grossEw.Cells[6,ctr].Value = Convert.ToDateTime((ctr-3).ToString() + "/1/" + year.ToString()).ToString("MMM");
                }

                Dictionary<String, Double> grosss = new Dictionary<String, Double>();
                alphalist.employees.ToList().ForEach(t=>{
                    grossEw.Cells[liner2,1].Value = t.tin;
                    grossEw.Cells[liner2,2].Value = t.last + ", " + t.first + " " + t.middle;
                    grossEw.Cells[liner2,3].Value = t.status;

                    

                    var total = 0.0;
                    t.entries.Where(e=>e.description == "Gross Pay").ToList().ForEach(ent=>{
                        ent.details.ForEach(det=>{
                            grossEw.Cells[liner2,4+(det.month-1)].Value = det.amount;
                            total += det.amount;
                        });
                    });
                    grossEw.Cells[liner2,(4+12)-1].Value = total;
                    grosss.Add(t.employeeCode, total);
                    liner2++;
                });


                
                ExcelWorksheet taxWithheldEw = ep.Workbook.Worksheets.Add("TAX WITHHELD");

                taxWithheldEw.Cells[1,1].Value = "KABALIKAT PARA SA MAUNLAD NA BUHAY, INC. (KMBI)";
                taxWithheldEw.Cells[2,1].Value = "HEAD OFFICE - " + alphalist.position.ToUpper();
                taxWithheldEw.Cells[3,1].Value = "SUMMARY OF TAX WITHHELD";
                taxWithheldEw.Cells[4,1].Value = "For The Taxable Year " + alphalist.year.ToString();

                for(int ctr=4;ctr<=15;ctr++) {
                    taxWithheldEw.Cells[6,ctr].Value = Convert.ToDateTime((ctr-3).ToString() + "/1/" + year.ToString()).ToString("MMM");
                }

                var liner3 = 7;
                alphalist.employees.ToList().ForEach(t=>{
                    taxWithheldEw.Cells[liner3,1].Value = t.tin;
                    taxWithheldEw.Cells[liner3,2].Value = t.last + ", " + t.first + " " + t.middle;
                    taxWithheldEw.Cells[liner3,3].Value = t.status;

                    t.entries.Where(e=>e.description == "Tax Withheld").ToList().ForEach(ent=>{
                        ent.details.ForEach(det=>{
                            taxWithheldEw.Cells[liner3,4+(det.month-1)].Value = det.amount;
                        });
                    });

                    liner3++;
                });

                ExcelWorksheet sssPremiumEw = ep.Workbook.Worksheets.Add("SSS");

                sssPremiumEw.Cells[1,1].Value = "KABALIKAT PARA SA MAUNLAD NA BUHAY, INC. (KMBI)";
                sssPremiumEw.Cells[2,1].Value = "HEAD OFFICE - " + alphalist.position.ToUpper();
                sssPremiumEw.Cells[3,1].Value = "SUMMARY OF SSS CONTRIBUTIONS";
                sssPremiumEw.Cells[4,1].Value = "For The Taxable Year " + alphalist.year.ToString();

                for(int ctr=4;ctr<=15;ctr++) {
                    sssPremiumEw.Cells[6,ctr].Value = Convert.ToDateTime((ctr-3).ToString() + "/1/" + year.ToString()).ToString("MMM");
                }

                var liner4 = 7;
                alphalist.employees.ToList().ForEach(t=>{
                    sssPremiumEw.Cells[liner4,1].Value = t.tin;
                    sssPremiumEw.Cells[liner4,2].Value = t.last + ", " + t.first + " " + t.middle;
                    sssPremiumEw.Cells[liner4,3].Value = t.status;

                    t.entries.Where(e=>e.description == "SSS Premium").ToList().ForEach(ent=>{
                        ent.details.ForEach(det=>{
                            sssPremiumEw.Cells[liner4,4+(det.month-1)].Value = det.amount;
                        });
                    });

                    liner4++;
                });

                ExcelWorksheet philhealtEw = ep.Workbook.Worksheets.Add("PHILHEALTH");

                philhealtEw.Cells[1,1].Value = "KABALIKAT PARA SA MAUNLAD NA BUHAY, INC. (KMBI)";
                philhealtEw.Cells[2,1].Value = "HEAD OFFICE - " + alphalist.position.ToUpper();
                philhealtEw.Cells[3,1].Value = "SUMMARY OF PHILHEALTH CONTRIBUTIONS";
                philhealtEw.Cells[4,1].Value = "For The Taxable Year " + alphalist.year.ToString();

                for(int ctr=4;ctr<=15;ctr++) {
                    philhealtEw.Cells[6,ctr].Value = Convert.ToDateTime((ctr-3).ToString() + "/1/" + year.ToString()).ToString("MMM");
                }

                var liner5 = 7;
                alphalist.employees.ToList().ForEach(t=>{
                    philhealtEw.Cells[liner5,1].Value = t.tin;
                    philhealtEw.Cells[liner5,2].Value = t.last + ", " + t.first + " " + t.middle;
                    philhealtEw.Cells[liner5,3].Value = t.status;

                    t.entries.Where(e=>e.description == "Philhealth Premium").ToList().ForEach(ent=>{
                        ent.details.ForEach(det=>{
                            philhealtEw.Cells[liner5,4+(det.month-1)].Value = det.amount;
                        });
                    });

                    liner5++;
                });

                ExcelWorksheet pagibigEw = ep.Workbook.Worksheets.Add("PAGIBIG");

                pagibigEw.Cells[1,1].Value = "KABALIKAT PARA SA MAUNLAD NA BUHAY, INC. (KMBI)";
                pagibigEw.Cells[2,1].Value = "HEAD OFFICE - " + alphalist.position.ToUpper();
                pagibigEw.Cells[3,1].Value = "SUMMARY OF HDMF CONTRIBUTIONS";
                pagibigEw.Cells[4,1].Value = "For The Taxable Year " + alphalist.year.ToString();

                for(int ctr=4;ctr<=15;ctr++) {
                    pagibigEw.Cells[6,ctr].Value = Convert.ToDateTime((ctr-3).ToString() + "/1/" + year.ToString()).ToString("MMM");
                }

                var liner6 = 7;
                alphalist.employees.ToList().ForEach(t=>{
                    pagibigEw.Cells[liner6,1].Value = t.tin;
                    pagibigEw.Cells[liner6,2].Value = t.last + ", " + t.first + " " + t.middle;
                    pagibigEw.Cells[liner6,3].Value = t.status;

                    t.entries.Where(e=>e.description == "Pagibig Premium").ToList().ForEach(ent=>{
                        ent.details.ForEach(det=>{
                            pagibigEw.Cells[liner6,4+(det.month-1)].Value = det.amount;
                        });
                    });

                    liner6++;
                });

                ExcelWorksheet benefitsEw = ep.Workbook.Worksheets.Add("BENEFITS");

                benefitsEw.Cells[1,1].Value = "KABALIKAT PARA SA MAUNLAD NA BUHAY, INC. (KMBI)";
                benefitsEw.Cells[2,1].Value = "HEAD OFFICE - " + alphalist.position.ToUpper();
                benefitsEw.Cells[3,1].Value = "SUMMARY OF BENEFITS RECEIVED";
                benefitsEw.Cells[4,1].Value = "For The Taxable Year " + alphalist.year.ToString();

                benefitsEw.Cells[6,4].Value = "MEAL ALLOWANCES";
                benefitsEw.Cells[6,5].Value = "13th Month May";
                benefitsEw.Cells[6,6].Value = "13TH MONTH";
                benefitsEw.Cells[6,7].Value = "13TH MONTH ADJ";
                benefitsEw.Cells[6,8].Value = "DEC - BONUS";
                benefitsEw.Cells[6,9].Value = "TAX ON BONUS";
                benefitsEw.Cells[6,10].Value = "SERVICE AWARDS";
                benefitsEw.Cells[6,11].Value = "TOTAL";
                benefitsEw.Cells[6,12].Value = "HOLIDAY MEALS";
                benefitsEw.Cells[6,13].Value = "Anniv Gift (Gratuity)";
                benefitsEw.Cells[6,14].Value = "VL Conversion";
                benefitsEw.Cells[6,15].Value = "Uniform";
                benefitsEw.Cells[6,16].Value = "Laundry Allowance";
                benefitsEw.Cells[6,17].Value = "RICE SUBSIDY";
                benefitsEw.Cells[6,18].Value = "HEALTH INSURANCE";
                benefitsEw.Cells[6,19].Value = "MEAL - OVERTIME";
                benefitsEw.Cells[6,20].Value = "TOTAL";


                var liner7 = 7;
                Dictionary<String, Double> benefits = new Dictionary<String, Double>();
                alphalist.employees.ToList().ForEach(t=>{
                    benefitsEw.Cells[liner7,1].Value = t.tin;
                    benefitsEw.Cells[liner7,2].Value = t.last + ", " + t.first + " " + t.middle;
                    benefitsEw.Cells[liner7,3].Value = t.status;

                    // meals
                    var total = 0.0;
                    var total2 = 0.0;
                    t.entries.Where(e=>e.description == "Meals").ToList().ForEach(ent=>{
                        var totalMeals = 0.0;
                        var total13MonthMay = 0.0;
                        var total13Month = 0.0;
                        var total13MonthAdj = 0.0;
                        var totalDecBonus = 0.0;
                        var totalTaxOnBonus = 0.0;
                        var totalServiceAward = 0.0;

                        var totalHolidayMeals = 0.0;
                        var totalAnnivGift = 0.0;
                        var totalVLConversion = 0.0;
                        var totalUniform = 0.0;
                        var totalLaundryAllowance = 0.0;
                        var totalRiceSubsidy = 0.0;
                        var totalHealthInsurance = 0.0;
                        var totalMealOvertime = 0.0;

                        ent.details.ForEach(det=>{
                            totalMeals = det.amount;
                        });

                        benefitsEw.Cells[liner7,4].Value = totalMeals;
                        benefitsEw.Cells[liner7,5].Value = total13MonthMay;
                        benefitsEw.Cells[liner7,6].Value = total13Month;
                        benefitsEw.Cells[liner7,7].Value = total13MonthAdj;
                        benefitsEw.Cells[liner7,8].Value = totalDecBonus;
                        benefitsEw.Cells[liner7,9].Value = totalTaxOnBonus;
                        benefitsEw.Cells[liner7,10].Value = totalServiceAward;
                        total = totalMeals + total13MonthMay + total13Month + total13MonthAdj + totalDecBonus + totalTaxOnBonus + totalServiceAward;
                        benefitsEw.Cells[liner7,11].Value = total;

                        benefitsEw.Cells[liner7,12].Value = totalHolidayMeals;
                        benefitsEw.Cells[liner7,13].Value = totalAnnivGift;
                        benefitsEw.Cells[liner7,14].Value = totalVLConversion;
                        benefitsEw.Cells[liner7,15].Value = totalUniform;
                        benefitsEw.Cells[liner7,16].Value = totalLaundryAllowance;
                        benefitsEw.Cells[liner7,17].Value = totalRiceSubsidy;
                        benefitsEw.Cells[liner7,18].Value = totalHealthInsurance;
                        benefitsEw.Cells[liner7,19].Value = totalMealOvertime;
                        total2 = totalHolidayMeals + totalAnnivGift + totalVLConversion + totalUniform + totalLaundryAllowance + totalRiceSubsidy + totalHealthInsurance + totalMealOvertime;
                        benefitsEw.Cells[liner7,20].Value = total2;
                        
                    });
                    benefits.Add(t.employeeCode, total);

                    liner7++;
                });

                ExcelWorksheet mealsEw = ep.Workbook.Worksheets.Add("MEALS");

                mealsEw.Cells[1,1].Value = "KABALIKAT PARA SA MAUNLAD NA BUHAY, INC. (KMBI)";
                mealsEw.Cells[2,1].Value = "HEAD OFFICE - " + alphalist.position.ToUpper();
                mealsEw.Cells[3,1].Value = "SUMMARY OF HOLIDAY MEALS";
                mealsEw.Cells[4,1].Value = "For The Taxable Year " + alphalist.year.ToString();


                for(int ctr=4;ctr<=15;ctr++) {
                    mealsEw.Cells[6,ctr].Value = Convert.ToDateTime((ctr-3).ToString() + "/1/" + year.ToString()).ToString("MMM");
                }

                var liner8 = 7;
                alphalist.employees.ToList().ForEach(t=>{
                    mealsEw.Cells[liner8,1].Value = t.tin;
                    mealsEw.Cells[liner8,2].Value = t.last + ", " + t.first + " " + t.middle;
                    mealsEw.Cells[liner8,3].Value = t.status;

                    t.entries.Where(e=>e.description == "Meals").ToList().ForEach(ent=>{
                        ent.details.ForEach(det=>{
                            mealsEw.Cells[liner8,4+(det.month-1)].Value = det.amount;
                        });
                    });

                    liner8++;
                });

                ExcelWorksheet riceEw = ep.Workbook.Worksheets.Add("RICE");

                riceEw.Cells[1,1].Value = "KABALIKAT PARA SA MAUNLAD NA BUHAY, INC. (KMBI)";
                riceEw.Cells[2,1].Value = "HEAD OFFICE - " + alphalist.position.ToUpper();
                riceEw.Cells[3,1].Value = "SUMMARY OF RICE SUBSIDY";
                riceEw.Cells[4,1].Value = "For The Taxable Year " + alphalist.year.ToString();

                for(int ctr=4;ctr<=15;ctr++) {
                    riceEw.Cells[6,ctr].Value = Convert.ToDateTime((ctr-3).ToString() + "/1/" + year.ToString()).ToString("MMM");
                }

                var liner9 = 7;
                alphalist.employees.ToList().ForEach(t=>{
                    riceEw.Cells[liner9,1].Value = t.tin;
                    riceEw.Cells[liner9,2].Value = t.last + ", " + t.first + " " + t.middle;
                    riceEw.Cells[liner9,3].Value = t.status;

                    t.entries.Where(e=>e.description == "Rice").ToList().ForEach(ent=>{
                        ent.details.ForEach(det=>{
                            riceEw.Cells[liner9,4+(det.month-1)].Value = det.amount;
                        });
                    });

                    liner9++;
                });

                ExcelWorksheet hiEw = ep.Workbook.Worksheets.Add("HI");

                hiEw.Cells[1,1].Value = "KABALIKAT PARA SA MAUNLAD NA BUHAY, INC. (KMBI)";
                hiEw.Cells[2,1].Value = "HEAD OFFICE - " + alphalist.position.ToUpper();
                hiEw.Cells[3,1].Value = "SUMMARY OF HEALTH INSURANCE";
                hiEw.Cells[4,1].Value = "For The Taxable Year " + alphalist.year.ToString();

                for(int ctr=4;ctr<=15;ctr++) {
                    hiEw.Cells[6,ctr].Value = Convert.ToDateTime((ctr-3).ToString() + "/1/" + year.ToString()).ToString("MMM");
                }

                var liner10 = 7;
                alphalist.employees.ToList().ForEach(t=>{
                    hiEw.Cells[liner10,1].Value = t.tin;
                    hiEw.Cells[liner10,2].Value = t.last + ", " + t.first + " " + t.middle;
                    hiEw.Cells[liner10,3].Value = t.status;

                    t.entries.Where(e=>e.description == "Hi").ToList().ForEach(ent=>{
                        ent.details.ForEach(det=>{
                            hiEw.Cells[liner10,4+(det.month-1)].Value = det.amount;
                        });
                    });

                    liner10++;
                });


                ExcelWorksheet ew = ep.Workbook.Worksheets.Add("Alphalist");
                

                ew.Cells[1,1].Value = "KABALIKAT PARA SA MAUNLAD NA BUHAY INC. (KMBI)";
                ew.Cells[2,1].Value = "HEAD OFFICE - " + alphalist.position.ToUpper();
                ew.Cells[3,1].Value = "SUMMARY OF ANNUALIZED TAX";
                ew.Cells[4,1].Value = "For The Taxable Year " + year.ToString();


                ew.Cells[6,1].Value = "TIN";
                ew.Cells[6,2].Value = "Name";
                ew.Cells[6,3].Value = "Status";
                ew.Cells[6,4].Value = "13th month pay & other benefits";
                ew.Cells[6,5].Value = "SSS,PHIC, HDMF CONT";
                ew.Cells[6,6].Value = "Salaries & other  Compensation";

                var liner = 7;
                alphalist.employees.ToList().ForEach(t=>{
                    ew.Cells[liner,1].Value = t.tin;
                    ew.Cells[liner,2].Value = t.last + ", " + t.first + " " + t.middle;
                    ew.Cells[liner,3].Value = t.status;

                    var totalBenefits = benefits.Where(x=>x.Key == t.employeeCode).FirstOrDefault();

                    ew.Cells[liner,4].Value = (totalBenefits.Value > 82000) ?  82000 : totalBenefits.Value;
                    // ew.Cells[liner,5].Value = totalSSS + totalPagibig + totalPhilhealth;
                    // ew.Cells[liner,6].Value = (totalSSS + totalPagibig + totalPhilhealth); // plus others

                    // ew.Cells[liner,7].Value = (totalBenefits > 82000) ? 82000 - totalBenefits : 0;

                    var totalGross = grosss.Where(x=>x.Key == t.employeeCode).FirstOrDefault();
                    ew.Cells[liner,8].Value = totalGross.Value;
                    // ew.Cells[liner,9].Value = "";
                    // ew.Cells[liner,10].Value = totalTax;


                    liner++;
                });



                bytes = ep.GetAsByteArray();
                ep.Dispose();
            }

            var link = _upload.upload(bytes,"kmbidevreporeports/" + filename + ".xlsx");

            switch(test){
                case 1:
                    return Content(link);
                default:
                    return Redirect(link);  
            }
        }

        [HttpGet("alphaList2")]
        public IActionResult alphaList2([FromQuery]Int32 year, [FromQuery]Int32? jobGrade, [FromQuery]String branchSlug, [FromQuery]Int32 test) {
            
            repAlphalistReportModel alphalist = _reporting.alphalist2(year, branchSlug, Convert.ToInt32(jobGrade));
            
            var line = 0;

            using (OfficeOpenXml.ExcelPackage ep = new OfficeOpenXml.ExcelPackage()) {
                
                // alphalist

                ExcelWorksheet alphalistEw = ep.Workbook.Worksheets.Add("ALPHALIST");

                alphalistEw.Cells[1,1].Value = "KABALIKAT PARA SA MAUNLAD NA BUHAY, INC. (KMBI)";
                alphalistEw.Cells[2,1].Value = "HEAD OFFICE - " + alphalist.position.ToUpper();
                alphalistEw.Cells[3,1].Value = "SUMMARY OF ANNUALIZED TAX";
                alphalistEw.Cells[4,1].Value = "For The Taxable Year " + alphalist.year.ToString();

                alphalistEw.Cells[7,1].Value = "(a)";
                alphalistEw.Cells[7,2].Value = "(b)";
                alphalistEw.Cells[7,3].Value = "";
                alphalistEw.Cells[7,4].Value = "(i)";
                alphalistEw.Cells[7,5].Value = "(j)";
                alphalistEw.Cells[7,6].Value = "(k)";
                alphalistEw.Cells[7,7].Value = "(l)";
                alphalistEw.Cells[7,8].Value = "(m)";
                alphalistEw.Cells[7,9].Value = "(n)";
                alphalistEw.Cells[7,10].Value = "(o)";
                alphalistEw.Cells[7,11].Value = "(p)";
                alphalistEw.Cells[7,12].Value = "(q)";
                alphalistEw.Cells[7,13].Value = "(r)";
                alphalistEw.Cells[7,14].Value = "(s)";
                alphalistEw.Cells[7,15].Value = "(t)";
                alphalistEw.Cells[7,16].Value = "(u)";
                alphalistEw.Cells[7,17].Value = "(v)";
                alphalistEw.Cells[7,18].Value = "(w)";
                alphalistEw.Cells[7,19].Value = "(x)";
                alphalistEw.Cells[7,20].Value = "(y)";
                alphalistEw.Cells[7,21].Value = "(z)";
                alphalistEw.Cells[7,22].Value = "(aa)";

                alphalistEw.Cells[7,1].Value = "TIN";
                alphalistEw.Cells[7,2].Value = "Name of Employee";
                alphalistEw.Cells[7,3].Value = "STATUS";
                //kmbi
                //non taxable
                alphalistEw.Cells[7,4].Value = "13th month pay & other benefits";
                alphalistEw.Cells[7,5].Value = "SSS, PHIC, HDMF CONT.";
                alphalistEw.Cells[7,6].Value = "Salaries & other Compensation";
                //taxable
                alphalistEw.Cells[7,7].Value = "13th month pay & other benefits";
                alphalistEw.Cells[7,8].Value = "SALARIES FROM KMBI";
                alphalistEw.Cells[7,9].Value = "ACTUAL SALARY FOR 5 MONTHS";
                alphalistEw.Cells[7,10].Value = "Taxes Withheld";
                //consolidated
                //non taxable
                alphalistEw.Cells[7,11].Value = "13th month pay & other benefits";
                alphalistEw.Cells[7,12].Value = "SSS, PHIC, HDMF CONT.";
                alphalistEw.Cells[7,13].Value = "Salaries & other Compensation";
                //taxable
                alphalistEw.Cells[7,14].Value = "13th month pay & other benefits";
                alphalistEw.Cells[7,15].Value = "SALARIES FROM KMBI";
                alphalistEw.Cells[7,16].Value = "ACTUAL SALARY FOR 5 MONTHS (AUGUST to DECEMBER)";

                alphalistEw.Cells[7,17].Value = "TOTAL GROSS TAXABLE INCOME";
                alphalistEw.Cells[7,18].Value = "AMOUNT OF PERSONAL EXEMPTION";
                alphalistEw.Cells[7,19].Value = "NET TAXABLE INCOME";
                alphalistEw.Cells[7,20].Value = "TAX DUE";
                alphalistEw.Cells[7,21].Value = "Taxes Withheld";
                alphalistEw.Cells[7,22].Value = "Tax still (refund) / due";

                line = 10;
                alphalist.alphalist.ForEach(al=>{

                    alphalistEw.Cells[line,1].Value = al.code;
                    alphalistEw.Cells[line,2].Value = al.name;
                    alphalistEw.Cells[line,3].Value = al.status;
                    alphalistEw.Cells[line,4].Value = al.columnI;
                    alphalistEw.Cells[line,5].Value = al.columnI;
                    alphalistEw.Cells[line,6].Value = al.columnJ;
                    alphalistEw.Cells[line,7].Value = al.columnK;
                    alphalistEw.Cells[line,8].Value = al.columnL;
                    alphalistEw.Cells[line,9].Value = al.columnM;
                    alphalistEw.Cells[line,10].Value = al.columnN;
                    alphalistEw.Cells[line,11].Value = al.columnO;
                    alphalistEw.Cells[line,12].Value = al.columnP;
                    alphalistEw.Cells[line,13].Value = al.columnQ;
                    alphalistEw.Cells[line,14].Value = al.columnR;
                    alphalistEw.Cells[line,15].Value = al.columnS;
                    alphalistEw.Cells[line,16].Value = al.columnT;
                    alphalistEw.Cells[line,17].Value = al.columnU;
                    alphalistEw.Cells[line,18].Value = al.columnV;
                    alphalistEw.Cells[line,19].Value = al.columnW;
                    alphalistEw.Cells[line,20].Value = al.columnX;
                    alphalistEw.Cells[line,21].Value = al.columnY;
                    alphalistEw.Cells[line,22].Value = al.columnZ;
                    alphalistEw.Cells[line,23].Value = al.columnAA;
                    line++;
                });

                // gross

                ExcelWorksheet grossEw = ep.Workbook.Worksheets.Add("GROSS");

                grossEw.Cells[1,1].Value = "KABALIKAT PARA SA MAUNLAD NA BUHAY, INC. (KMBI)";
                grossEw.Cells[2,1].Value = "HEAD OFFICE - " + alphalist.position.ToUpper();
                grossEw.Cells[3,1].Value = "SUMMARY OF GROSS COMPENSATION";
                grossEw.Cells[4,1].Value = "For The Taxable Year " + alphalist.year.ToString();

                for(int ctr=4;ctr<=15;ctr++) {
                    grossEw.Cells[6,ctr].Value = Convert.ToDateTime((ctr-3).ToString() + "/1/" + year.ToString()).ToString("MMM");
                }

                line = 7;
                alphalist.gross.ForEach(g=>{

                    grossEw.Cells[line,1].Value = g.code;
                    grossEw.Cells[line,2].Value = g.name;
                    grossEw.Cells[line,3].Value = g.status;
                    grossEw.Cells[line,4].Value = g.jan;
                    grossEw.Cells[line,5].Value = g.feb;
                    grossEw.Cells[line,6].Value = g.mar;
                    grossEw.Cells[line,7].Value = g.apr;
                    grossEw.Cells[line,8].Value = g.may;
                    grossEw.Cells[line,9].Value = g.jun;
                    grossEw.Cells[line,10].Value = g.jul;
                    grossEw.Cells[line,11].Value = g.aug;
                    grossEw.Cells[line,12].Value = g.sep;
                    grossEw.Cells[line,13].Value = g.oct;
                    grossEw.Cells[line,14].Value = g.nov;
                    grossEw.Cells[line,15].Value = g.dec;
                    line++;
                });

                // tax withheld

                ExcelWorksheet taxEw = ep.Workbook.Worksheets.Add("TAX WITHHELD");

                taxEw.Cells[1,1].Value = "KABALIKAT PARA SA MAUNLAD NA BUHAY, INC. (KMBI)";
                taxEw.Cells[2,1].Value = "HEAD OFFICE - " + alphalist.position.ToUpper();
                taxEw.Cells[3,1].Value = "SUMMARY OF TAX WITHHELD";
                taxEw.Cells[4,1].Value = "For The Taxable Year " + alphalist.year.ToString();

                for(int ctr=4;ctr<=15;ctr++) {
                    taxEw.Cells[6,ctr].Value = Convert.ToDateTime((ctr-3).ToString() + "/1/" + year.ToString()).ToString("MMM");
                }

                line = 7;
                alphalist.taxWithheld.ForEach(t=>{

                    taxEw.Cells[line,1].Value = t.code;
                    taxEw.Cells[line,2].Value = t.name;
                    taxEw.Cells[line,3].Value = t.status;
                    taxEw.Cells[line,4].Value = t.jan;
                    taxEw.Cells[line,5].Value = t.feb;
                    taxEw.Cells[line,6].Value = t.mar;
                    taxEw.Cells[line,7].Value = t.apr;
                    taxEw.Cells[line,8].Value = t.may;
                    taxEw.Cells[line,9].Value = t.jun;
                    taxEw.Cells[line,10].Value = t.jul;
                    taxEw.Cells[line,11].Value = t.aug;
                    taxEw.Cells[line,12].Value = t.sep;
                    taxEw.Cells[line,13].Value = t.oct;
                    taxEw.Cells[line,14].Value = t.nov;
                    taxEw.Cells[line,15].Value = t.dec;
                    line++;
                });

                // sss

                ExcelWorksheet sssEw = ep.Workbook.Worksheets.Add("SSS");

                sssEw.Cells[1,1].Value = "KABALIKAT PARA SA MAUNLAD NA BUHAY, INC. (KMBI)";
                sssEw.Cells[2,1].Value = "HEAD OFFICE - " + alphalist.position.ToUpper();
                sssEw.Cells[3,1].Value = "SUMMARY OF SSS CONTRIBUTIONS";
                sssEw.Cells[4,1].Value = "For The Taxable Year " + alphalist.year.ToString();

                for(int ctr=4;ctr<=15;ctr++) {
                    sssEw.Cells[6,ctr].Value = Convert.ToDateTime((ctr-3).ToString() + "/1/" + year.ToString()).ToString("MMM");
                }

                line = 7;
                alphalist.sss.ForEach(s=>{

                    sssEw.Cells[line,1].Value = s.code;
                    sssEw.Cells[line,2].Value = s.name;
                    sssEw.Cells[line,3].Value = s.status;
                    sssEw.Cells[line,4].Value = s.jan;
                    sssEw.Cells[line,5].Value = s.feb;
                    sssEw.Cells[line,6].Value = s.mar;
                    sssEw.Cells[line,7].Value = s.apr;
                    sssEw.Cells[line,8].Value = s.may;
                    sssEw.Cells[line,9].Value = s.jun;
                    sssEw.Cells[line,10].Value = s.jul;
                    sssEw.Cells[line,11].Value = s.aug;
                    sssEw.Cells[line,12].Value = s.sep;
                    sssEw.Cells[line,13].Value = s.oct;
                    sssEw.Cells[line,14].Value = s.nov;
                    sssEw.Cells[line,15].Value = s.dec;
                    line++;
                });

                // philhealth

                ExcelWorksheet philhealthEw = ep.Workbook.Worksheets.Add("PHILHEALTH");

                philhealthEw.Cells[1,1].Value = "KABALIKAT PARA SA MAUNLAD NA BUHAY, INC. (KMBI)";
                philhealthEw.Cells[2,1].Value = "HEAD OFFICE - " + alphalist.position.ToUpper();
                philhealthEw.Cells[3,1].Value = "SUMMARY OF PHILHEALTH";
                philhealthEw.Cells[4,1].Value = "For The Taxable Year " + alphalist.year.ToString();

                for(int ctr=4;ctr<=15;ctr++) {
                    philhealthEw.Cells[6,ctr].Value = Convert.ToDateTime((ctr-3).ToString() + "/1/" + year.ToString()).ToString("MMM");
                }

                line = 7;
                alphalist.philHealth.ForEach(p=>{

                    philhealthEw.Cells[line,1].Value = p.code;
                    philhealthEw.Cells[line,2].Value = p.name;
                    philhealthEw.Cells[line,3].Value = p.status;
                    philhealthEw.Cells[line,4].Value = p.jan;
                    philhealthEw.Cells[line,5].Value = p.feb;
                    philhealthEw.Cells[line,6].Value = p.mar;
                    philhealthEw.Cells[line,7].Value = p.apr;
                    philhealthEw.Cells[line,8].Value = p.may;
                    philhealthEw.Cells[line,9].Value = p.jun;
                    philhealthEw.Cells[line,10].Value = p.jul;
                    philhealthEw.Cells[line,11].Value = p.aug;
                    philhealthEw.Cells[line,12].Value = p.sep;
                    philhealthEw.Cells[line,13].Value = p.oct;
                    philhealthEw.Cells[line,14].Value = p.nov;
                    philhealthEw.Cells[line,15].Value = p.dec;
                    line++;
                });

                // pagibig

                ExcelWorksheet pagibigEw = ep.Workbook.Worksheets.Add("PAGIBIG");

                pagibigEw.Cells[1,1].Value = "KABALIKAT PARA SA MAUNLAD NA BUHAY, INC. (KMBI)";
                pagibigEw.Cells[2,1].Value = "HEAD OFFICE - " + alphalist.position.ToUpper();
                pagibigEw.Cells[3,1].Value = "SUMMARY OF HDMF CONTRIBUTIONS";
                pagibigEw.Cells[4,1].Value = "For The Taxable Year " + alphalist.year.ToString();

                for(int ctr=4;ctr<=15;ctr++) {
                    pagibigEw.Cells[6,ctr].Value = Convert.ToDateTime((ctr-3).ToString() + "/1/" + year.ToString()).ToString("MMM");
                }

                line = 7;
                alphalist.pagibig.ForEach(p=>{

                    pagibigEw.Cells[line,1].Value = p.code;
                    pagibigEw.Cells[line,2].Value = p.name;
                    pagibigEw.Cells[line,3].Value = p.status;
                    pagibigEw.Cells[line,4].Value = p.jan;
                    pagibigEw.Cells[line,5].Value = p.feb;
                    pagibigEw.Cells[line,6].Value = p.mar;
                    pagibigEw.Cells[line,7].Value = p.apr;
                    pagibigEw.Cells[line,8].Value = p.may;
                    pagibigEw.Cells[line,9].Value = p.jun;
                    pagibigEw.Cells[line,10].Value = p.jul;
                    pagibigEw.Cells[line,11].Value = p.aug;
                    pagibigEw.Cells[line,12].Value = p.sep;
                    pagibigEw.Cells[line,13].Value = p.oct;
                    pagibigEw.Cells[line,14].Value = p.nov;
                    pagibigEw.Cells[line,15].Value = p.dec;
                    line++;
                });

                // rice

                ExcelWorksheet riceEw = ep.Workbook.Worksheets.Add("RICE");

                riceEw.Cells[1,1].Value = "KABALIKAT PARA SA MAUNLAD NA BUHAY, INC. (KMBI)";
                riceEw.Cells[2,1].Value = "HEAD OFFICE - " + alphalist.position.ToUpper();
                riceEw.Cells[3,1].Value = "SUMMARY OF RICE SUBSIDY";
                riceEw.Cells[4,1].Value = "For The Taxable Year " + alphalist.year.ToString();

                for(int ctr=4;ctr<=15;ctr++) {
                    riceEw.Cells[6,ctr].Value = Convert.ToDateTime((ctr-3).ToString() + "/1/" + year.ToString()).ToString("MMM");
                }

                line = 7;
                alphalist.rice.ForEach(p=>{

                    riceEw.Cells[line,1].Value = p.code;
                    riceEw.Cells[line,2].Value = p.name;
                    riceEw.Cells[line,3].Value = p.status;
                    riceEw.Cells[line,4].Value = p.jan;
                    riceEw.Cells[line,5].Value = p.feb;
                    riceEw.Cells[line,6].Value = p.mar;
                    riceEw.Cells[line,7].Value = p.apr;
                    riceEw.Cells[line,8].Value = p.may;
                    riceEw.Cells[line,9].Value = p.jun;
                    riceEw.Cells[line,10].Value = p.jul;
                    riceEw.Cells[line,11].Value = p.aug;
                    riceEw.Cells[line,12].Value = p.sep;
                    riceEw.Cells[line,13].Value = p.oct;
                    riceEw.Cells[line,14].Value = p.nov;
                    riceEw.Cells[line,15].Value = p.dec;
                    line++;
                });

                // hi

                ExcelWorksheet hiEw = ep.Workbook.Worksheets.Add("HI");

                hiEw.Cells[1,1].Value = "KABALIKAT PARA SA MAUNLAD NA BUHAY, INC. (KMBI)";
                hiEw.Cells[2,1].Value = "HEAD OFFICE - " + alphalist.position.ToUpper();
                hiEw.Cells[3,1].Value = "SUMMARY OF HEALTH INSURANCE";
                hiEw.Cells[4,1].Value = "For The Taxable Year " + alphalist.year.ToString();

                for(int ctr=4;ctr<=15;ctr++) {
                    hiEw.Cells[6,ctr].Value = Convert.ToDateTime((ctr-3).ToString() + "/1/" + year.ToString()).ToString("MMM");
                }

                line = 7;
                alphalist.hi.ForEach(p=>{

                    hiEw.Cells[line,1].Value = p.code;
                    hiEw.Cells[line,2].Value = p.name;
                    hiEw.Cells[line,3].Value = p.status;
                    hiEw.Cells[line,4].Value = p.jan;
                    hiEw.Cells[line,5].Value = p.feb;
                    hiEw.Cells[line,6].Value = p.mar;
                    hiEw.Cells[line,7].Value = p.apr;
                    hiEw.Cells[line,8].Value = p.may;
                    hiEw.Cells[line,9].Value = p.jun;
                    hiEw.Cells[line,10].Value = p.jul;
                    hiEw.Cells[line,11].Value = p.aug;
                    hiEw.Cells[line,12].Value = p.sep;
                    hiEw.Cells[line,13].Value = p.oct;
                    hiEw.Cells[line,14].Value = p.nov;
                    hiEw.Cells[line,15].Value = p.dec;
                    line++;
                });
               
                byte[] bytes = ep.GetAsByteArray();
                ep.Dispose();
                var link = _upload.upload(bytes,"kmbidevreporeports/alphalist-" + year.ToString()+ ".xlsx");

                switch(test){
                    case 1:
                        return Content(link);
                    default:
                        return Redirect(link);  
                }
            }


        }
        [HttpGetAttribute("detailedSearch")]
        public IActionResult detailedSearch([FromQuery]String branchSlug, [FromQuery]String field, [FromQuery]double from, [FromQuery]double to)
        {

            var rep = _reporting.clientDetailedSearch(branchSlug, field, from, to);
            var data = JsonConvert.SerializeObject(rep);
            var repDesc = "Client Search";
            return generateReportExcel("detailedSearch", data, repDesc);
        }

        [HttpGet("monthlyRemittance/{month}/{year}")]
        public IActionResult monthlyRemittance(Int32 month, Int32 year, String branchSlug)
        {
            var rep = _reporting.getMonthlyRemittance(month, year, branchSlug);
            var monthYear = Convert.ToDateTime(rep.month.ToString() + "/1/" + rep.year.ToString()).ToString("MMMM yyyy");
            var filename = "monthly-remittances-" + monthYear.Replace(" ","-").ToLower();
            byte[] bytes;
            
            using (OfficeOpenXml.ExcelPackage ep = new OfficeOpenXml.ExcelPackage()) {
                
                ExcelWorksheet ew = ep.Workbook.Worksheets.Add("MONTHLY REMITTANCES");
                
                ew.Cells[3,2].Value = "MONTHLY REMITTANCES";
                ew.Cells[4,2].Value = "FOR THE MONTH OF " + monthYear;

                ew.Cells[6,2].Value = "NAME OF EMPLOYEE";
                ew.Cells[6,3].Value = "TAX CODE";
                ew.Cells[6,4].Value = "MONTHLY RATE";
                ew.Cells[6,5].Value = "GROSS COMPENSATION";
                ew.Cells[6,6].Value = "13TH MONTH ACCRUAL";
                ew.Cells[6,7].Value = "W/HOLDING TAX";
                ew.Cells[6,8].Value = "SSS-EE";
                ew.Cells[6,9].Value = "SSS-ER";
                ew.Cells[6,10].Value = "SSS-EC";
                ew.Cells[6,11].Value = "TOTAL";
                ew.Cells[6,12].Value = "PHIC-EE";
                ew.Cells[6,13].Value = "PHIC-ER";
                ew.Cells[6,14].Value = "TOTAL";
                ew.Cells[6,15].Value = "HDMF-EE";
                ew.Cells[6,16].Value = "HDMF-ER";
                ew.Cells[6,17].Value = "TOTAL";
                ew.Cells[6,18].Value = "HDMF HOUSING LOAN";
                ew.Cells[6,19].Value = "HDMF MPL";
                ew.Cells[6,20].Value = "HDMF CALAMITY";
                ew.Cells[6,21].Value = "SSS LOAN";
                ew.Cells[6,22].Value = "MP2";
                
                var liner = 7;
                rep.details.ForEach(mr=>{
                    ew.Cells[liner,2].Value = mr.fullname;
                    ew.Cells[liner,3].Value = mr.taxCode;
                    ew.Cells[liner,4].Value = mr.monthlyRate;
                    ew.Cells[liner,5].Value = mr.grossCompensation;
                    ew.Cells[liner,6].Value = mr.thirteenthMonthAccrual;
                    ew.Cells[liner,7].Value = mr.witholdingTax;
                    ew.Cells[liner,8].Value = mr.sssEe;
                    ew.Cells[liner,9].Value = mr.sssEr;
                    ew.Cells[liner,10].Value = mr.ec;
                    ew.Cells[liner,11].Value = mr.sssTotal;
                    ew.Cells[liner,12].Value = mr.phicEe;
                    ew.Cells[liner,13].Value = mr.phicEr;
                    ew.Cells[liner,14].Value = mr.phicTotal;
                    ew.Cells[liner,15].Value = mr.hdmfEe;
                    ew.Cells[liner,16].Value = mr.hdmfEr;
                    ew.Cells[liner,17].Value = mr.hdmfTotal;
                    ew.Cells[liner,18].Value = mr.hdmfHousingLoan;
                    ew.Cells[liner,19].Value = mr.hdmfMpl;
                    ew.Cells[liner,20].Value = mr.hdmfCalamity;
                    ew.Cells[liner,21].Value = mr.sssLoan;
                    ew.Cells[liner,22].Value = mr.mp2;
                    liner++;
                });

                ew.Cells[liner,3].Value = "TOTAL";
                ew.Cells[liner,4].Value = rep.details.Sum(d=>d.monthlyRate);
                ew.Cells[liner,5].Value = rep.details.Sum(d=>d.grossCompensation);
                ew.Cells[liner,6].Value = rep.details.Sum(d=>d.thirteenthMonthAccrual);
                ew.Cells[liner,7].Value = rep.details.Sum(d=>d.witholdingTax);
                ew.Cells[liner,8].Value = rep.details.Sum(d=>d.sssEe);
                ew.Cells[liner,9].Value = rep.details.Sum(d=>d.sssEr);
                ew.Cells[liner,10].Value = rep.details.Sum(d=>d.ec);
                ew.Cells[liner,11].Value = rep.details.Sum(d=>d.sssTotal);
                ew.Cells[liner,12].Value = rep.details.Sum(d=>d.phicEe);
                ew.Cells[liner,13].Value = rep.details.Sum(d=>d.phicEr);
                ew.Cells[liner,14].Value = rep.details.Sum(d=>d.phicTotal);
                ew.Cells[liner,15].Value = rep.details.Sum(d=>d.hdmfEe);
                ew.Cells[liner,16].Value = rep.details.Sum(d=>d.hdmfEr);
                ew.Cells[liner,17].Value = rep.details.Sum(d=>d.hdmfTotal);
                ew.Cells[liner,18].Value = rep.details.Sum(d=>d.hdmfHousingLoan);
                ew.Cells[liner,19].Value = rep.details.Sum(d=>d.hdmfMpl);
                ew.Cells[liner,20].Value = rep.details.Sum(d=>d.hdmfCalamity);
                ew.Cells[liner,21].Value = rep.details.Sum(d=>d.sssLoan);
                ew.Cells[liner,22].Value = rep.details.Sum(d=>d.mp2);

                bytes = ep.GetAsByteArray();
                ep.Dispose();
            }
            
            var link = _upload.upload(bytes,"kmbidevreporeports/" + filename + ".xlsx");
            return Redirect(link);
        }


#region "Accounting Reports"
        
        [HttpGet("cashReceiptList")]
        public IActionResult getCashReceiptList([FromQuery]DateTime from, [FromQuery] DateTime to, [FromQuery]Boolean isData = false) {
            var rep = _reporting.getCashReceiptList(from,to);
            
            _rep.templateFilename = "cashReceiptList.cshtml";
            _rep.model = rep;
            _rep.orientation = "portrait";
            _rep.footerText = "System Generated Report - Accounting System - " + DateTime.Now;
            _rep.title = "Cash Receipt List";
            _rep.isHalf = false;
            _rep.pageType ="letter";
            dynamic vb = new System.Dynamic.ExpandoObject();
            vb.dateFrom = from;
            vb.dateTo = to;
            _rep.viewBag = vb;
            iResult res = _rep.generatePDF();


            if(isData) {
                List<repCashReceiptReportModel> list = new List<repCashReceiptReportModel>();
                rep.ForEach(r=>{
                    r.accounts.credit.ToList().ForEach(c=>{
                        list.Add(new repCashReceiptReportModel {
                            date = r.timestamps.issuedAt,
                            receivedFrom = r.payor.name,
                            particulars = r.particulars,
                            orNo = r.number,
                            account = c.name,
                            type = "credit",
                            debit = 0,
                            credit = c.amount
                        });
                    });
                    r.accounts.debit.ToList().ForEach(d=>{
                        list.Add(new repCashReceiptReportModel {
                            date = r.timestamps.issuedAt,
                            receivedFrom = r.payor.name,
                            particulars = r.particulars,
                            orNo = r.number,
                            account = d.name,
                            type = "debit",
                            debit = d.amount,
                            credit = 0
                        });
                    });
                });
                return Ok(list);
            } else {
                var link = _upload.upload(res.bytes,@"kmbidevreporeports/cashReceipt.pdf");
                return Redirect(link);
            }
        }

        [HttpGet("cashDisbursementList")]
        public IActionResult getCashDisbursementList([FromQuery]DateTime from, [FromQuery] DateTime to, [FromQuery]Boolean isData = false) {
            var rep = _reporting.getCashDisbursementList(from,to);

            _rep.templateFilename = "cashDisbursementList.cshtml";
            _rep.model = rep;
            _rep.orientation = "portrait";
            _rep.footerText = "System Generated Report - Accounting System - " + DateTime.Now;
            _rep.title = "Cash Disbursement List";
            _rep.isHalf = false;
            _rep.pageType ="letter";
            dynamic vb = new System.Dynamic.ExpandoObject();
            vb.dateFrom = from;
            vb.dateTo = to;
            _rep.viewBag = vb;
            iResult res = _rep.generatePDF();

            if(isData) {
                List<repCashDisbursementReportModel> list = new List<repCashDisbursementReportModel>();
                rep.ForEach(r=>{
                    r.accounts.credit.ToList().ForEach(c=>{
                        list.Add(new repCashDisbursementReportModel {
                            date = r.timestamps.issuedAt,
                            supplier = r.supplier.name,
                            particulars = r.particulars,
                            cvNo = r.cvNumber,
                            chequeNo = r.chequeNumber,
                            account = c.name,
                            type = "credit",
                            debit = 0,
                            credit = c.amount
                        });
                    });
                    r.accounts.debit.ToList().ForEach(d=>{
                        list.Add(new repCashDisbursementReportModel {
                            date = r.timestamps.issuedAt,
                            supplier = r.supplier.name,
                            particulars = r.particulars,
                            cvNo = r.cvNumber,
                            chequeNo = r.chequeNumber,
                            account = d.name,
                            type = "debit",
                            debit = d.amount,
                            credit = 0
                        });
                    });
                });
                return Ok(list);
            } else {
                var link = _upload.upload(res.bytes,"kmbidevreporeports/cashDisbursement.pdf");
                return Redirect(link);
            }
        }

        [HttpGet("cashPosition")]
        public IActionResult cashPosition([FromQuery]DateTime asOf, [FromQuery]Boolean isData = false) {
            var translist = _acc.transactionDetailedList(Convert.ToDateTime("1/1/0001"),asOf,101,true);
            
            var list = new List<repCashPositionReportModel>();

            translist.GroupBy(x=>x.code).Select(g=>g.First()).ToList().ForEach(d=>{
                var begBal = translist.Where(x=>x.code.ToString() == d.code.ToString() && x.issuedAt < asOf).Sum(y=>y.amount);
                var receipts = translist.Where(x=>x.code.ToString() == d.code.ToString() && x.issuedAt == asOf && x.accountType == "debit").Sum(y=>y.amount);
                var disbursements = translist.Where(x=>x.code.ToString() == d.code.ToString() && x.issuedAt == asOf && x.accountType == "credit").Sum(y=>y.amount);
                list.Add(new repCashPositionReportModel {
                    accountNo = d.code.ToString(),
                    bankAccount = d.name,
                    begBal = begBal,
                    receipts = receipts,
                    disbursements = disbursements,
                    endBal = (begBal + receipts) - disbursements,
                    asof = asOf.ToString("MMMM-dd-yyyy").ToLower()
                });
            });

            if(!isData) {
                var filename = "cash-position-" + asOf.ToString("MMMM-dd-yyyy").ToLower();
                byte[] bytes;

                using (OfficeOpenXml.ExcelPackage ep = new OfficeOpenXml.ExcelPackage()) {

                    ExcelWorksheet ew = ep.Workbook.Worksheets.Add("cash-position");

                    ew.Cells["A1"].Value = "KABALIKAT PARA SA MAUNLAD NA BUHAY, INC.";
                    ew.Cells["A2"].Value = "CASH POSITION REPORT";
                    ew.Cells["A3"].Value = "AS OF " + asOf.ToString("MMMM dd, yyyy");
                    
                    ew.Cells["A5"].Value = "BANK ACCOUNTS";
                    ew.Cells["B5"].Value = "ACCOUNT NO";
                    ew.Cells["C5"].Value = "BEGINNING BAL";
                    ew.Cells["D5"].Value = "RECEIPTS";
                    ew.Cells["E5"].Value = "DISBURSEMENT";
                    ew.Cells["F5"].Value = "ENDING BALANCE";

                    var line = 6;
                    foreach(var item in list) {
                        ew.Cells[line,1].Value = item.accountNo;
                        ew.Cells[line,2].Value = item.bankAccount;
                        ew.Cells[line,3].Value = String.Format("{0:0,000.00;-0,000.00;-}", item.begBal);
                        ew.Cells[line,4].Value = String.Format("{0:0,000.00;-0,000.00;-}", item.receipts);
                        ew.Cells[line,5].Value = String.Format("{0:0,000.00;-0,000.00;-}", item.disbursements);
                        ew.Cells[line,6].Value = String.Format("{0:0,000.00;-0,000.00;-}", item.endBal);
                        line++;
                    }

                    // formatting

                    ew.Column(1).Width = 19;
                    ew.Column(2).Width = 50;
                    ew.Column(3).Width = 17;
                    ew.Column(4).Width = 17;
                    ew.Column(5).Width = 17;
                    ew.Column(6).Width = 17;

                    ew.Column(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    ew.Column(4).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    ew.Column(5).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    ew.Column(6).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                    

                    ew.Row(5).Style.Font.Bold = true;

                    bytes = ep.GetAsByteArray();
                    ep.Dispose();
                }

                var link = _upload.upload(bytes,"kmbidevreporeports/" + filename + ".xlsx");
                return Redirect(link);
            } else {
                
                var data = JsonConvert.SerializeObject(list);
                var repDesc = "Cash Position";
                return generateReport("cashPosition", data, repDesc);
                // return Ok(list);
            }
        }

        [HttpGet("generalJournalList")]
        public IActionResult getGeneralJournalList([FromQuery]DateTime? from, [FromQuery] DateTime? to, [FromQuery]Boolean isData = false) {
            DateTime _from;
            DateTime _to;

            if(from==null) {
                return BadRequest(new {
                    type="failed",
                    message="Invalid parameter"
                });
            }
   
            _from = Convert.ToDateTime(from).Date;
            _to = _from;

            if(to!=null) {
                _to = Convert.ToDateTime(to).Date;
            }
            

            var rep = _reporting.getGeneralJournalList(_from,_to);

            _rep.templateFilename = "generalJournalList.cshtml";
            _rep.model = rep;
            _rep.orientation = "portrait";
            _rep.footerText = "System Generated Report - Accounting System - " + DateTime.Now;
            _rep.title = "General Journal List";
            _rep.isHalf = false;
            _rep.pageType ="letter";
            dynamic vb = new System.Dynamic.ExpandoObject();
            vb.dateFrom = _from;
            vb.dateTo = _to;
            _rep.viewBag = vb;
            iResult res = _rep.generatePDF();

            if(isData) {
                List<repGeneralJournalReportModel> list = new List<repGeneralJournalReportModel>();
                rep.ForEach(r=>{
                    r.accounts.credit.ToList().ForEach(c=>{
                        list.Add(new repGeneralJournalReportModel {
                            date = r.timestamps.issuedAt,
                            jvNo = r.jvNumber.ToString(),
                            particulars = r.particulars,
                            account = c.name,
                            accountType = "credit",
                            debit = 0,
                            credit = c.amount
                        });
                    });
                    r.accounts.debit.ToList().ForEach(d=>{
                        list.Add(new repGeneralJournalReportModel {
                            date = r.timestamps.issuedAt,
                            jvNo = r.jvNumber.ToString(),
                            particulars = r.particulars,
                            account = d.name,
                            accountType = "credit",
                            debit = d.amount,
                            credit = 0
                        });
                    });
                });
                return Ok(list);
            } else {
                var link = _upload.upload(res.bytes,"kmbidevreporeports/generalJournal.pdf");
                return Redirect(link);
            }
        }

        [HttpGet("checkVoucher/{cvNumber}")]
        public IActionResult checkVoucher(String cvNumber, [FromQuery]Boolean isData = false) {
            var rep = _reporting.checkVoucher(cvNumber);

            _rep.templateFilename = "checkVoucher.cshtml";
            _rep.model = rep;
            _rep.orientation = "portrait";

            //_rep.footerText = "System Generated Report - Accounting System - " + DateTime.Now;
            _rep.title = "Check Voucher";
            _rep.isHalf = false;
            _rep.pageType ="legal";
            iResult res = _rep.generatePDF();

            if(isData) {
                return Ok(rep);
            } else {
                var link = _upload.upload(res.bytes,"kmbidevreporeports/checkVoucher.pdf");
                return Redirect(link);
            }
        }

        [HttpGet("check/{cvNumber}")]
        public IActionResult check(String cvNumber) {
            var err = "";
            repCheckVoucher item = new repCheckVoucher();
            if(cvNumber!=null) {
                if(cvNumber=="") {
                    err = "CV Number is required.";
                } else {
                    item = _reporting.checkVoucher(cvNumber);
                    if(item == null) {
                        err = "CV Voucher not found.";
                    }
                }
            }

            if(err!="") {
                return BadRequest(new {
                    type = "error",
                    message = err
                });
            } else {
                var checkNo = item.chequeNumber;
                var amount = item.amount.byNumbers;
                var amountInWords = item.amount.byWords;
                var date = item.timestamps.issuedAt;
                var supplier = item.supplier.name;

                var link = "";

                List<bankDef> list = new List<bankDef> {
                    new bankDef {
                        name = "BOC NEW",
                        locations = new [] {
                            "L1","L13","H15","L15","H17","K18"
                        },
                        locationsAlignments = new [] {
                            ExcelHorizontalAlignment.Right,
                            ExcelHorizontalAlignment.Right,
                            ExcelHorizontalAlignment.Left,
                            ExcelHorizontalAlignment.Left,
                            ExcelHorizontalAlignment.Left,
                            ExcelHorizontalAlignment.Left
                        },
                        fonts = new[] {
                            new Font("Calibri", 11),
                            new Font("Calibri", 12),
                            new Font("Cambria", 14),
                            new Font("Cambria", 13),
                            new Font("Bookman Old Style",8.5F),
                            new Font("Calibri", 12)
                        },
                        colWidths = new[] {
                            8.53, 8.53, 2.32, 8.53, 4.84, 4.53, 2.32, 4.32, 9.32, 8.53, 34.42, 23.74
                        },
                        rowHeights = new[] {
                            14.4, 14.4, 14.4, 14.4, 14.4, 14.4, 14.4, 14.4, 14.4, 14.4, 14.4, 10.5, 15.8, 12.8, 16.5, 9, 14.4, 15.6, 14.4, 14.4, 14.4, 14.4, 14.4,
                        },
                        margins = new[] {
                            1.9M,1.9M,1.8M,0.8M
                        }
                    },
                    new bankDef {
                        name = "BOC OLD",
                        locations = new [] {
                            "L1","L15","G17","L17","G19","K20"
                        },
                        locationsAlignments = new [] {
                            ExcelHorizontalAlignment.Right,
                            ExcelHorizontalAlignment.Center,
                            ExcelHorizontalAlignment.Left,
                            ExcelHorizontalAlignment.Center,
                            ExcelHorizontalAlignment.Left,
                            ExcelHorizontalAlignment.Left
                        },
                        fonts = new[] {
                            new Font("Calibri", 11),
                            new Font("Calibri", 12),
                            new Font("Cambria", 14),
                            new Font("Cambria", 13),
                            new Font("Bookman Old Style",7.5F),
                            new Font("Calibri", 12)
                        },
                        colWidths = new[] {
                            8.53, 8.53, 2.32, 8.53, 4.84, 6.74, 2.32, 4.32, 9.32, 8.53, 32.42, 31.53
                        },
                        rowHeights = new[] {
                            14.4, 14.4, 14.4, 14.4, 14.4, 14.4, 14.4, 14.4, 14.4, 14.4, 14.4, 6.8, 15, 8.3, 16.5, 0, 14.4, 15.6, 14.4, 14.4, 14.4, 14.4, 14.4
                        },
                        margins = new[] {
                            1.9M,1.9M,0.6M,0.3M
                        }
                    },
                    new bankDef {
                        name = "PNB ALLIED",
                        locations = new [] {
                            "L1","L13","I15","L15","H17","K18"
                        },
                        locationsAlignments = new [] {
                            ExcelHorizontalAlignment.Right,
                            ExcelHorizontalAlignment.Center,
                            ExcelHorizontalAlignment.Left,
                            ExcelHorizontalAlignment.Center,
                            ExcelHorizontalAlignment.Left,
                            ExcelHorizontalAlignment.Left
                        },
                        fonts = new[] {
                            new Font("Calibri", 11),
                            new Font("Calibri", 12),
                            new Font("Cambria", 12),
                            new Font("Cambria", 13),
                            new Font("Bookman Old Style",9F),
                            new Font("Calibri", 12)
                        },
                        colWidths = new[] {
                            8.53, 8.53, 2.32, 8.53, 4.84, 4.53, 2.32, 4.32, 9.32, 8.53, 45.74, 19.84
                        },
                        rowHeights = new[] {
                            14.4, 14.4, 14.4, 14.4, 14.4, 14.4, 14.4, 14.4, 14.4, 14.4, 14.3, 6, 19.5, 7.5, 16.5, 9, 14.4, 15.6, 14.4, 14.4, 14.4, 14.4, 14.4

                        },
                        margins = new[] {
                            1.9M,1.9M,0.6M,0.3M
                        }
                    },
                    new bankDef {
                        name = "PRODUCERS",
                        locations = new [] {
                            "L1","L13","J15","L15","J17","K18"
                        },
                        locationsAlignments = new [] {
                            ExcelHorizontalAlignment.Right,
                            ExcelHorizontalAlignment.Right,
                            ExcelHorizontalAlignment.Left,
                            ExcelHorizontalAlignment.Right,
                            ExcelHorizontalAlignment.Left,
                            ExcelHorizontalAlignment.Left
                        },
                        fonts = new[] {
                            new Font("Calibri", 11),
                            new Font("Calibri", 12),
                            new Font("Cambria", 11),
                            new Font("Cambria", 13),
                            new Font("Bookman Old Style",10F),
                            new Font("Calibri", 12)
                        },
                        colWidths = new[] {
                            8.53, 8.53, 2.32, 8.53, 4.84, 4.53, 2.32, 4.32, 4.74, 8.53, 43.32, 19.84
                        },
                        rowHeights = new[] {
                            14.4, 14.4, 14.4, 14.4, 14.4, 14.4, 14.4, 14.4, 14.4, 14.4, 14.4, 11.3, 15, 17.3, 16.5, 9, 14.4, 15.6, 14.4, 14.4, 14.4, 14.4, 14.4
                        },
                        margins = new[] {
                            1.9M,1.9M,1.8M,0.8M
                        }
                    },
                    new bankDef {
                        name = "SECURITY",
                        locations = new [] {
                            "L1","L13","H15","L15","H17","K18"
                        },
                        locationsAlignments = new [] {
                            ExcelHorizontalAlignment.Right,
                            ExcelHorizontalAlignment.Right,
                            ExcelHorizontalAlignment.Left,
                            ExcelHorizontalAlignment.Right,
                            ExcelHorizontalAlignment.Left,
                            ExcelHorizontalAlignment.Left
                        },
                        fonts = new[] {
                            new Font("Calibri", 11),
                            new Font("Calibri", 12),
                            new Font("Cambria", 14),
                            new Font("Cambria", 13),
                            new Font("Bookman Old Style",9F),
                            new Font("Calibri", 12)
                        },
                        colWidths = new[] {
                            8.53, 8.53, 2.32, 8.53, 4.84, 0, 2.32, 4.32, 9.32, 8.53, 39.32, 23.11
                        },
                        rowHeights = new[] {
                            14.4, 14.4, 14.4, 14.4, 14.4, 14.4, 14.4, 14.4, 14.4, 14.4, 12, 9.8, 15, 8.3, 16.5, 9, 14.4, 15.6, 14.4, 14.4, 14.4, 14.4, 14.4
                        },
                        margins = new[] {
                            1.9M,1.9M,1.8M,0.8M
                        }
                    },
                    new bankDef {
                        name = "PBB",
                        locations = new [] {
                            "L1","L13","H15","L15","H17","K18"
                        },
                        locationsAlignments = new [] {
                            ExcelHorizontalAlignment.Right,
                            ExcelHorizontalAlignment.Right,
                            ExcelHorizontalAlignment.Left,
                            ExcelHorizontalAlignment.Left,
                            ExcelHorizontalAlignment.Left,
                            ExcelHorizontalAlignment.Left
                        },
                        fonts = new[] {
                            new Font("Calibri", 11),
                            new Font("Calibri", 12),
                            new Font("Cambria", 14),
                            new Font("Cambria", 12),
                            new Font("Bookman Old Style",9F),
                            new Font("Calibri", 11)
                        },
                        colWidths = new[] {
                            8.53, 8.53, 2.32, 8.53, 4.84, 4.53, 2.32, 4.32, 9.32, 8.53, 36.74, 19.84
                        },
                        rowHeights = new[] {
                            14.4, 14.4, 14.4, 14.4, 14.4, 14.4, 14.4, 14.4, 14.4, 14.4, 14.4, 6.8, 15, 8.3, 16.5, 9, 14.4, 15.6, 14.4, 14.4, 14.4, 14.4, 14.4

                        },
                        margins = new[] {
                            1.9M,1.9M,1.8M,0.8M
                        }
                    }
                };

                using (OfficeOpenXml.ExcelPackage ep = new OfficeOpenXml.ExcelPackage()) {
                    
                    
                    list.ForEach(b=>{
                        ExcelWorksheet ew = ep.Workbook.Worksheets.Add(b.name);
                        ew.PrinterSettings.Orientation = eOrientation.Landscape;
                        ew.PrinterSettings.PaperSize = ePaperSize.Letter;
                        
                        ew.PrinterSettings.TopMargin = b.margins[0] / 2.54M;
                        ew.PrinterSettings.BottomMargin = b.margins[1] / 2.54M;
                        ew.PrinterSettings.LeftMargin = b.margins[2] / 2.54M;
                        ew.PrinterSettings.RightMargin = b.margins[3] / 2.54M;

                        ew.Cells[b.locations[0]].Value = String.Format("{0:N2}",amount);
                        ew.Cells[b.locations[1]].Value = String.Format("{0}",date.ToString("MMMM dd, yyyy"));
                        ew.Cells[b.locations[2]].Value = String.Format("*** {0} ***",supplier);
                        ew.Cells[b.locations[3]].Value = String.Format("*** {0:N2} ***",amount);
                        ew.Cells[b.locations[4]].Value = String.Format("{0}",amountInWords);
                        ew.Cells[b.locations[5]].Value = String.Format("{0}",checkNo);

                        // format
                        ew.DefaultColWidth = 0;

                        for(Int32 ctr=1;ctr<=12;ctr++) {
                            ew.Column(ctr).Width = b.colWidths[ctr-1] + .58;
                        }

                        ew.DefaultRowHeight = 0;

                        for(Int32 ctr=1;ctr<=23;ctr++) {
                            ew.Row(ctr).Height = b.rowHeights[ctr-1];
                        }
                        
                        
                        ew.Cells[b.locations[0]].Style.Font.SetFromFont(b.fonts[0]);
                        ew.Cells[b.locations[0]].Style.HorizontalAlignment = b.locationsAlignments[0];

                        ew.Cells[b.locations[1]].Style.Font.SetFromFont(b.fonts[1]);
                        ew.Cells[b.locations[1]].Style.HorizontalAlignment = b.locationsAlignments[1];

                        ew.Cells[b.locations[2]].Style.Font.SetFromFont(b.fonts[2]);
                        ew.Cells[b.locations[2]].Style.HorizontalAlignment = b.locationsAlignments[2];
                        ew.Cells[b.locations[2]].Style.Font.Bold = true;

                        ew.Cells[b.locations[3]].Style.Font.SetFromFont(b.fonts[3]);
                        ew.Cells[b.locations[3]].Style.HorizontalAlignment = b.locationsAlignments[3];
                        ew.Cells[b.locations[3]].Style.Font.Bold = true;

                        ew.Cells[b.locations[4]].Style.Font.SetFromFont(b.fonts[4]);
                        ew.Cells[b.locations[4]].Style.HorizontalAlignment = b.locationsAlignments[4];
                        ew.Cells[b.locations[4]].Value = String.Format("{0}",amountInWords);
                        ew.Cells[b.locations[4]].Style.Font.Bold = true;

                        ew.Cells[b.locations[5]].Style.Font.SetFromFont(b.fonts[5]);
                        ew.Cells[b.locations[5]].Style.HorizontalAlignment = b.locationsAlignments[5];
                        ew.Cells[b.locations[5]].Value = String.Format("{0}",checkNo);
                        ew.Cells[b.locations[5]].Style.Font.Bold = true;

                        
                        ew.Cells["A1:L23"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        ew.Cells["A1:L23"].Style.Border.Top.Color.SetColor(Color.White);
                        ew.Cells["A1:L23"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        ew.Cells["A1:L23"].Style.Border.Bottom.Color.SetColor(Color.White);
                        ew.Cells["A1:L23"].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        ew.Cells["A1:L23"].Style.Border.Left.Color.SetColor(Color.White);
                        ew.Cells["A1:L23"].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        ew.Cells["A1:L23"].Style.Border.Right.Color.SetColor(Color.White);
                    });
                    
                        
                    byte[] bytes;
                    bytes = ep.GetAsByteArray();

                    var filename = "check-" + checkNo;
                    ep.Dispose();
                    link = _upload.upload(bytes,"kmbidevreporeports/" + filename + ".xlsx");

                    return Redirect(link);
                }
            }
        }

        [HttpGet("journalVoucher")]
        public IActionResult journalVoucher([FromQuery]DateTime date, [FromQuery]Int32 test = 0, [FromQuery]Boolean isData = false) {
            var rep = _reporting.journalVoucher(date);
            
            List<dynamic> list = new List<dynamic>();
            
            var maxPartLength = 70;
            rep.ForEach(jv => {
                
                jv.accounts.debit.ToList().ForEach(db => {
                    var ctrDb = 0;
                    var partLengthDb = (db.name + " - " + db.particular).Length;
                    while(ctrDb < partLengthDb) {
                        var toGet = maxPartLength;
                        if(maxPartLength>partLengthDb - ctrDb) {
                            toGet = partLengthDb - ctrDb;
                        }

                        if(toGet<partLengthDb) {
                            var toExit = false;
                            while(toGet < partLengthDb - ctrDb && !toExit) {
                                if((db.name + " - " + db.particular).Substring(toGet,1) != " ") {
                                    toGet++;
                                } else {
                                    toExit = true;
                                }
                            }
                        }
                       
                        dynamic itemDb = new ExpandoObject();
                        itemDb.jvNo = jv.jvNumber;
                        
                        
                        itemDb.particulars = (db.name + " - " + db.particular).Substring(ctrDb,toGet);


                        if(ctrDb==0) {
                            itemDb.debit = db.amount;
                        } else {
                            itemDb.debit = 0;
                        }
                        itemDb.credit = 0;
                        list.Add(itemDb);
                        ctrDb+= toGet;
                    }
                    

                });

                jv.accounts.credit.ToList().ForEach(cr => {
                    var ctrCr = 0;
                    var partLengthCr = (cr.name + " - " + cr.particular).Length ;
                    while(ctrCr < partLengthCr) {
                        var toGet = maxPartLength;
                        if(maxPartLength>partLengthCr - ctrCr) {
                            toGet = partLengthCr - ctrCr;    
                        }

                        
                        if(toGet<partLengthCr) {
                            var toExit = false;
                            while(toGet < partLengthCr - ctrCr && !toExit) {
                                if((cr.name + " - " + cr.particular).Substring(toGet,1) != " ") {
                                    toGet++;
                                } else {
                                    toExit = true;
                                }
                            }
                        }

                        dynamic itemCr = new ExpandoObject();
                        itemCr.jvNo = jv.jvNumber;
                        
                        
                        itemCr.particulars = (cr.name + " - " + cr.particular).Substring(ctrCr,toGet);
                        

                        if(ctrCr==0) {
                            itemCr.credit = cr.amount;
                        } else {
                            itemCr.credit = 0;
                        }
                        itemCr.debit = 0;
                        list.Add(itemCr);
                        ctrCr+= toGet;
                    }
                });

                
             

                var ctrMain = 0;
                var partLengthMain = jv.particulars.Length;
                while(ctrMain < partLengthMain) {
                    var toGet = maxPartLength;
                    if(maxPartLength>partLengthMain - ctrMain) {
                        toGet = partLengthMain - ctrMain;
                    }

                    if(toGet<partLengthMain) {
                        var toExit = false;
                        while(toGet < partLengthMain - ctrMain && !toExit) {
                            if(jv.particulars.Substring(toGet,1) != " ") {
                                toGet++;
                            } else {
                                toExit = true;
                            }
                        }
                    }
                    
                    dynamic itemMain = new ExpandoObject();
                    itemMain.jvNo = jv.jvNumber;
                    itemMain.particulars = jv.particulars.Substring(ctrMain,toGet);
                    itemMain.debit = 0;
                    itemMain.credit = 0;
                    list.Add(itemMain);
                    ctrMain+= toGet;
                }

                
                dynamic itemSpace = new ExpandoObject();
                itemSpace.jvNo = "";
                itemSpace.particulars = "blankspacebaby";
                itemSpace.credit = 0;
                itemSpace.debit = 0;
                list.Add(itemSpace);
                
            });

            if(list!=null) {
                if(list.Count > 0) {

                } else {
                    return Ok(new {
                        type = "information",
                        message = "No data to generate"
                    });
                }
            } else {
                return Ok(new {
                    type = "information",
                    message = "No data to generate"
                });
            }
            _rep.templateFilename = "journalVoucher.cshtml";
            _rep.model = list;
            _rep.orientation = "portrait";
            _rep.footerText = "System Generated Report - Accounting System - " + DateTime.Now;
            _rep.title = "Journal Voucher";
            _rep.isHalf = false;
            _rep.pageType = "letter";
            dynamic vb = new System.Dynamic.ExpandoObject();
            vb.root = root;
            vb.date = date;
            vb.jvNumberRange = rep[0].jvNumber + " - " + rep[rep.Count-1].jvNumber;
            _rep.viewBag = vb;
            iResult res = _rep.generatePDF();

            var link = _upload.upload(res.bytes,"kmbidevreporeports/journal_voucher_" + date.ToString("MM-dd-yyyy") + ".pdf");

            if(isData) {
                return Ok(list);
            }

            if(list!=null) {
                if(list.Count>0) {
                    switch (test) {
                        case 1:
                            return Content(link);
                        case 2:
                            return View(relativeTemplatePath + _rep.templateFilename,list);
                        default:
                            return Redirect(link);
                    }
                }
            } 
            
            return Ok(new {
                type = "information",
                message = "No data to generate"
            });
        }

        [HttpGet("getGeneralLedger")]
        public IActionResult getGeneralLedger([FromQuery]Int32? account, [FromQuery]Int32? year, [FromQuery]Int32? test, [FromQuery]Boolean isData = false) {           

            DateTime outD = new DateTime();
            var isValidDate = DateTime.TryParse("1/1/" + year.ToString(), out outD);
            
            if(year==null) {
                isValidDate = true;
                outD = DateTime.Now.Date;
            }

            if(!isValidDate) {
                
                return BadRequest(new {
                    type = "error",
                    message = "Invalid year"
                });
            }
            
            var firstDayOfYear = Convert.ToDateTime("1/1/" + outD.Year.ToString());
            var currentMonth = Convert.ToDateTime(DateTime.Now.AddMonths(1).Month.ToString() + "/1/" + outD.Year.ToString()).AddDays(-1);
           
            var coaList = _coa.all();
            var crList = _cr.all();   
            var cvList = _acv.all();
            var jvList = _ajv.all();

            Int32 coaCode = 99999999;
            String coaName = "";
            Int32 subsidiaryCode = 99999999;
            String subsidiaryName = "";
            
            if(account != null) {
                var coa = (from x in coaList where x.code == Convert.ToInt32(account) select x).FirstOrDefault();
                if(coa!=null) {
                    coaCode = (Int32)account;
                    coaName = coa.name;
                }

                coaList.ToList().ForEach(c=> {
                    c.subsidiary.ToList().ForEach(s=>{
                        if(s.code == (Int32)account) {
                            coaCode = c.code;
                            coaName = c.name;
                            subsidiaryCode = (Int32)account;
                            subsidiaryName = s.name;
                        }
                    });
                });
            }

            List<acctngModule.Models.coa> _coaList = new List<acctngModule.Models.coa>();
            acctngModule.Models.coa coaItem = new acctngModule.Models.coa();

            if(coaCode > 0 && coaCode != 99999999) {
                coaItem = (from x in coaList where x.code == coaCode select x).FirstOrDefault();
                if(coaItem!=null) {
                    _coaList.Add(coaItem);
                    if(subsidiaryCode > 0 && subsidiaryCode != 99999999) {
                        _coaList[0].subsidiary = (from x in coaItem.subsidiary where x.code == subsidiaryCode select x).ToArray();
                    }
                }
            }

            if(account == null) {
                _coaList.AddRange(coaList);
            }

            List<ExpandoObject> mains= new List<ExpandoObject>();

            _coaList.ToList().ForEach(coa => {
                
                //prevs

                dynamic prev = new ExpandoObject();
                prev.accountNo = coa.code;
                prev.accountName = coa.name;
                var prevYear = firstDayOfYear.AddSeconds(-1);
                prev.month = prevYear.ToString("MMMM yyyy");
                
                var prevdebit = 0.0;
                var prevcredit = 0.0;

                crList.Where(c=> c.timestamps.issuedAt <= prevYear).ToList().ForEach(c => {
                    c.accounts.debit.Where(d => d.code == coa.code).ToList().ForEach(db => {
                        prevdebit += db.amount; 
                    });

                    c.accounts.credit.Where(d => d.code == coa.code).ToList().ForEach(db => {
                        prevcredit += db.amount; 
                    });
                });

                cvList.Where(c=> c.timestamps.issuedAt <= prevYear).ToList().ForEach(c => {
                    c.accounts.debit.Where(d => d.code == coa.code).ToList().ForEach(db => {
                        prevdebit += db.amount; 
                    });

                    c.accounts.credit.Where(d => d.code == coa.code).ToList().ForEach(db => {
                        prevcredit += db.amount; 
                    });
                });

                jvList.Where(c=> c.timestamps.issuedAt <= prevYear).ToList().ForEach(c => {
                    c.accounts.debit.Where(d => d.code == coa.code).ToList().ForEach(db => {
                        prevdebit += db.amount; 
                    });

                    c.accounts.credit.Where(d => d.code == coa.code).ToList().ForEach(db => {
                        prevcredit += db.amount; 
                    });
                });

                coa.subsidiary.ToList().ForEach(sub=> {
                    crList.Where(c=> c.timestamps.issuedAt <= prevYear).ToList().ForEach(c => {
                        c.accounts.debit.Where(d => d.code == sub.code).ToList().ForEach(db => {
                            prevdebit += db.amount; 
                        });

                        c.accounts.credit.Where(d => d.code == sub.code).ToList().ForEach(db => {
                            prevcredit += db.amount; 
                        });
                    });

                    cvList.Where(c=> c.timestamps.issuedAt <= prevYear).ToList().ForEach(c => {
                        c.accounts.debit.Where(d => d.code == sub.code).ToList().ForEach(db => {
                            prevdebit += db.amount; 
                        });

                        c.accounts.credit.Where(d => d.code == sub.code).ToList().ForEach(db => {
                            prevcredit += db.amount; 
                        });
                    });

                    jvList.Where(c=> c.timestamps.issuedAt <= prevYear).ToList().ForEach(c => {
                        c.accounts.debit.Where(d => d.code == sub.code).ToList().ForEach(db => {
                            prevdebit += db.amount; 
                        });

                        c.accounts.credit.Where(d => d.code == sub.code).ToList().ForEach(db => {
                            prevcredit += db.amount; 
                        });
                    });
                });
                
                prev.debit = prevdebit;
                prev.credit = prevcredit;
                prev.balanceForwarded = prevdebit - prevcredit;
                
                if(coaCode!=99999999 && subsidiaryCode == 99999999)
                    mains.Add(prev);
                else if(account==null) 
                    mains.Add(prev);
            
                for(DateTime curDate = firstDayOfYear;curDate<=currentMonth;curDate = curDate.AddMonths(1)) {
                    DateTime dFrom = Convert.ToDateTime(curDate);
                    DateTime dTo = Convert.ToDateTime(curDate.AddMonths(1).AddSeconds(-1));
 
                    dynamic main = new ExpandoObject();
                    main.accountNo = coa.code;
                    main.accountName = coa.name;
                    main.month = curDate.ToString("MMMM yyyy");
                    
                    var debit = 0.0;
                    var credit = 0.0;

                    crList.Where(c=> c.timestamps.issuedAt >= dFrom && c.timestamps.issuedAt <= dTo).ToList().ForEach(c => {
                        c.accounts.debit.Where(d => d.code == coa.code).ToList().ForEach(db => {
                            debit += db.amount; 
                        });

                        c.accounts.credit.Where(d => d.code == coa.code).ToList().ForEach(db => {
                            credit += db.amount; 
                        });
                    });

                    cvList.Where(c=> c.timestamps.issuedAt >= dFrom && c.timestamps.issuedAt <= dTo).ToList().ForEach(c => {
                        c.accounts.debit.Where(d => d.code == coa.code).ToList().ForEach(db => {
                            debit += db.amount; 
                        });

                        c.accounts.credit.Where(d => d.code == coa.code).ToList().ForEach(db => {
                            credit += db.amount; 
                        });
                    });

                    jvList.Where(c=> c.timestamps.issuedAt >= dFrom && c.timestamps.issuedAt <= dTo).ToList().ForEach(c => {
                        c.accounts.debit.Where(d => d.code == coa.code).ToList().ForEach(db => {
                            debit += db.amount; 
                        });

                        c.accounts.credit.Where(d => d.code == coa.code).ToList().ForEach(db => {
                            credit += db.amount; 
                        });
                    });

                    coa.subsidiary.ToList().ForEach(sub=> {
                        crList.Where(c=> c.timestamps.issuedAt >= dFrom && c.timestamps.issuedAt <= dTo).ToList().ForEach(c => {
                            c.accounts.debit.Where(d => d.code == sub.code).ToList().ForEach(db => {
                                debit += db.amount; 
                            });

                            c.accounts.credit.Where(d => d.code == sub.code).ToList().ForEach(db => {
                                credit += db.amount; 
                            });
                        });

                        cvList.Where(c=> c.timestamps.issuedAt >= dFrom && c.timestamps.issuedAt <= dTo).ToList().ForEach(c => {
                            c.accounts.debit.Where(d => d.code == sub.code).ToList().ForEach(db => {
                                debit += db.amount; 
                            });

                            c.accounts.credit.Where(d => d.code == sub.code).ToList().ForEach(db => {
                                credit += db.amount; 
                            });
                        });

                        jvList.Where(c=> c.timestamps.issuedAt >= dFrom && c.timestamps.issuedAt <= dTo).ToList().ForEach(c => {
                            c.accounts.debit.Where(d => d.code == sub.code).ToList().ForEach(db => {
                                debit += db.amount; 
                            });

                            c.accounts.credit.Where(d => d.code == sub.code).ToList().ForEach(db => {
                                credit += db.amount; 
                            });
                        });
                    });
                    
                    main.debit = debit;
                    main.credit = credit;
                    main.balanceForwarded = debit - credit;

                    if(coaCode!=99999999 && subsidiaryCode == 99999999)
                        mains.Add(main);
                    else if(account==null) 
                        mains.Add(main);

                }

                
                coa.subsidiary.ToList().ForEach(sub=> {
                    List<ExpandoObject> lines = new List<ExpandoObject>();
                    
                    dynamic prevline = new ExpandoObject();
                    prevline.accountNo = sub.code;
                    prevline.accountName = sub.name;
                    var prevYearLine = firstDayOfYear.AddSeconds(-1);
                    prevline.month = prevYearLine.ToString("MMMM yyyy");

                    var prevcreditS = 0.0;
                    var prevdebitS = 0.0;
                    
                    
                    crList.Where(c=> c.timestamps.issuedAt <= prevYearLine).ToList().ForEach(c => {
                        c.accounts.debit.Where(d => d.code == sub.code).ToList().ForEach(db => {
                            prevdebitS += db.amount; 
                        });

                        c.accounts.credit.Where(d => d.code == sub.code).ToList().ForEach(db => {
                            prevcreditS += db.amount; 
                        });
                    });

                    cvList.Where(c=> c.timestamps.issuedAt <= prevYearLine).ToList().ForEach(c => {
                        c.accounts.debit.Where(d => d.code == sub.code).ToList().ForEach(db => {
                            prevdebitS += db.amount; 
                        });

                        c.accounts.credit.Where(d => d.code == sub.code).ToList().ForEach(db => {
                            prevcreditS += db.amount; 
                        });
                    });

                    jvList.Where(c=> c.timestamps.issuedAt <= prevYearLine).ToList().ForEach(c => {
                        c.accounts.debit.Where(d => d.code == sub.code).ToList().ForEach(db => {
                            prevdebitS += db.amount; 
                        });

                        c.accounts.credit.Where(d => d.code == sub.code).ToList().ForEach(db => {
                            prevcreditS += db.amount; 
                        });
                    });

                    prevline.debit = prevdebitS;
                    prevline.credit = prevcreditS;
                    prevline.balanceForwarded = prevdebitS = prevcreditS;
                    lines.Add(prevline);

                    var creditS = 0.0;
                    var debitS = 0.0;

                    for(DateTime curDate = firstDayOfYear;curDate<=currentMonth;curDate = curDate.AddMonths(1)) {
                        DateTime dFrom = Convert.ToDateTime(curDate);
                        DateTime dTo = Convert.ToDateTime(curDate.AddMonths(1).AddSeconds(-1));

                        dynamic line = new ExpandoObject();
                        line.accountNo = sub.code;
                        line.accountName = sub.name;
                        line.month = curDate.ToString("MMMM yyyy");
                        crList.Where(c=> c.timestamps.issuedAt >= dFrom && c.timestamps.issuedAt <= dTo).ToList().ForEach(c => {
                            c.accounts.debit.Where(d => d.code == sub.code).ToList().ForEach(db => {
                                debitS += db.amount; 
                            });

                            c.accounts.credit.Where(d => d.code == sub.code).ToList().ForEach(db => {
                                creditS += db.amount; 
                            });
                        });

                        cvList.Where(c=> c.timestamps.issuedAt >= dFrom && c.timestamps.issuedAt <= dTo).ToList().ForEach(c => {
                            c.accounts.debit.Where(d => d.code == sub.code).ToList().ForEach(db => {
                                debitS += db.amount; 
                            });

                            c.accounts.credit.Where(d => d.code == sub.code).ToList().ForEach(db => {
                                creditS += db.amount; 
                            });
                        });

                        jvList.Where(c=> c.timestamps.issuedAt >= dFrom && c.timestamps.issuedAt <= dTo).ToList().ForEach(c => {
                            c.accounts.debit.Where(d => d.code == sub.code).ToList().ForEach(db => {
                                debitS += db.amount; 
                            });

                            c.accounts.credit.Where(d => d.code == sub.code).ToList().ForEach(db => {
                                creditS += db.amount; 
                            });
                        });

                        line.debit = debitS;
                        line.credit = creditS;
                        line.balanceForwarded = debitS = creditS;
                        lines.Add(line);
                        
                    }

                    if(coaCode!=99999999 && subsidiaryCode != 99999999)
                        mains.AddRange(lines);
                    else if(account==null) 
                        mains.AddRange(lines);
                });



            });

            //end procs

            _rep.templateFilename = "generalLedger.cshtml";
            _rep.model = mains;
            _rep.orientation = "portrait";
            _rep.footerText = "System Generated Report - Accounting System - " + DateTime.Now;
            _rep.title = "General Ledger";
            dynamic vb = new System.Dynamic.ExpandoObject();
            vb.year = year;
            _rep.viewBag = vb;
            _rep.pageType = "letter";
            var title = "";
            if(account!=null) { 
                if(subsidiaryName!="") {
                    title = "General Ledger Book (" + subsidiaryCode + "-"+ subsidiaryName + ") " + firstDayOfYear.ToString("MMM-yyyy") + "-" + currentMonth.ToString("MMM-yyyy");
                } else {
                    title = "General Ledger Book (" + coaCode + "-"+ coaName + ") " + firstDayOfYear.ToString("MMM-yyyy") + "-" + currentMonth.ToString("MMM-yyyy");
                }
            } else {
                title = "General Ledger Book " + firstDayOfYear.ToString("MMM-yyyy") + "-" + currentMonth.ToString("MMM-yyyy");
            }
            
            _rep.title = title;
            
            iResult res = _rep.generatePDF();


            var link = _upload.upload(res.bytes,"kmbidevreporeports/" + title.Replace(" ","_") + ".pdf");

            if(mains!=null) {
                
                if(isData) {
                    return Ok(mains);
                }

                if(mains.Count> 0) {
                    switch (test) {
                        case 1:
                            return Content(link);
                        case 2:
                            return View(relativeTemplatePath + _rep.templateFilename,mains);
                        default:
                            return Redirect(link);
                    }
                } else {
                    return Ok(new {
                        type = "information",
                        message = "No data to generate"
                    });                

                }
            } else {
                return Ok(new {
                    type = "information",
                    message = "No data to generate"
                });
            }
        }






        [HttpGet("budgetSummary/{year}")]
        public IActionResult budgetSummary(Int32 year, [FromQuery]Boolean isData = false) {
            var list = _acc.budgetSummary(year);
            if(isData) {
                return Ok(list);
            }
            return Ok(list);
        }

        [HttpGet("budgetPerTransaction/{year}")]
        public IActionResult budgetPerTransaction(Int32 year, [FromQuery]Boolean isData = false) {
            var list = _acc.budgetPerTransaction(year);
            if(isData) {
                return Ok(list);
            }
            return Ok(list);
        }

        [HttpGet("coaReport")]
        public IActionResult coaReport([FromQuery]String type) {
            var _type = type;
            var coaList = _coa.all();
            switch (_type) {
                case "data": {
                    return Ok(coaList);
                }
                case "excel": {
                    using (OfficeOpenXml.ExcelPackage ep = new OfficeOpenXml.ExcelPackage()) {
                        ExcelWorksheet ew = ep.Workbook.Worksheets.Add("COA List");

                        ew.Cells["A1"].Value = "Code";
                        ew.Cells["B1"].Value = "Name";
                        ew.Cells["C1"].Value = "Description";

                        var liner = 2;
                        foreach (coa item in coaList)
                        {
                            ew.Cells[liner,1].Value = item.code;
                            ew.Cells[liner,2].Value = item.name;
                            ew.Cells[liner,3].Value = item.description;
                            liner++;
                            foreach (subsidiary item2 in item.subsidiary)
                            {
                                ew.Cells[liner,1].Value = item2.code;
                                ew.Cells[liner,2].Value = item2.name;
                                ew.Cells[liner,3].Value = item2.description;
                                liner++;
                            }
                        }

                        var bytes = ep.GetAsByteArray();
                        ep.Dispose();
                        return File(bytes, "application/vnd.ms-excel", "coa-list-" + DateTime.Now.ToString("MM-dd-yyyy") + ".xlsx");
                    }
                }
                default: {
                    
                    coaList = from x in coaList orderby x.code select x;
                    _rep.templateFilename = "coaReport.cshtml";
                    _rep.model = coaList;
                    _rep.orientation = "portrait";
                    _rep.footerText = "System Generated Report - Accounting System - " + DateTime.Now;
                    _rep.title = "COA Report";
                    _rep.isHalf = false;
                    _rep.pageType ="letter";
                    iResult res = _rep.generatePDF();

                    var link = _upload.upload(res.bytes,"kmbidevreporeports/coaReport.pdf");
                    return Redirect(link);
                }
            }
        }

        [HttpGet("extractTransactions")]
        public IActionResult extractTransactions([FromQuery]DateTime from, [FromQuery]DateTime to, [FromQuery]Int32 accountno, [FromQuery]Boolean includeSub, [FromQuery]Boolean isData = false) {
            var filename = "transactions-" + from.ToString("MMMM-dd-yyyy") + " - " + to.ToString("MMMM-dd-yyyy");
            byte[] bytes;

            IEnumerable<transactionDetailed> list = _acc.transactionDetailedList(from,to,accountno,includeSub);
            using (OfficeOpenXml.ExcelPackage ep = new OfficeOpenXml.ExcelPackage()) {
                ExcelWorksheet ew = ep.Workbook.Worksheets.Add("Transaction_Detailed");

                ew.Cells["A1"].Value = "slug";
                ew.Cells["B1"].Value = "book";
                ew.Cells["C1"].Value = "number";
                ew.Cells["D1"].Value = "particulars";
                ew.Cells["E1"].Value = "accountType";
                ew.Cells["F1"].Value = "motherCode";
                ew.Cells["G1"].Value = "code";
                ew.Cells["H1"].Value = "codeElement";
                ew.Cells["I1"].Value = "name";
                ew.Cells["J1"].Value = "amount";
                ew.Cells["K1"].Value = "amountRightSign";
                ew.Cells["L1"].Value = "issuedAt";

                var liner = 2;
                list.ToList().ForEach(tl =>{
                    ew.Cells[liner,1].Value = tl.slug;
                    ew.Cells[liner,2].Value = tl.book;
                    ew.Cells[liner,3].Value = tl.number;
                    ew.Cells[liner,4].Value = tl.particulars;
                    ew.Cells[liner,5].Value = tl.accountType;
                    ew.Cells[liner,6].Value = tl.motherCode;
                    ew.Cells[liner,7].Value = tl.code;
                    ew.Cells[liner,8].Value = tl.codeElement;
                    ew.Cells[liner,9].Value = tl.name;
                    ew.Cells[liner,10].Value = tl.amount;
                    ew.Cells[liner,11].Value = (tl.accountType == "debit") ? tl.amount : -tl.amount;
                    ew.Cells[liner,12].Value = tl.issuedAt.ToString("MM/dd/yyyy");
                    liner++;
                });

                bytes = ep.GetAsByteArray();
                ep.Dispose();
            }

            var link = _upload.upload(bytes,"kmbidevreporeports/" + filename + ".xlsx");

            if(isData) {
                return Ok(list);
            }

            return Redirect(link);
        }

        [HttpGet("generalLedgerSchedule/{accountno}")]
        public IActionResult accountsPayable(Int32 accountno, [FromQuery]DateTime from, [FromQuery]DateTime to, [FromQuery]String type = null) {
            var rep = _reporting.generalLedgerSchedule(accountno,from,to);
                        
            switch (type) {
                case "data": { 
                    return Ok(rep);
                }
                case "pdf": {
                    var data = JsonConvert.SerializeObject(rep);
                    var repDesc = "General Ledger Schedule";
                    return generateReport("generalLedgerSchedule", data, repDesc);
                }
                default: {
                    var filename = "general-ledger-schedule-" + rep.accountNo.Replace(" ","-").ToLower() + "-" + rep.accountName.Replace(" ","-").ToLower() + "-" + from.ToString("MM-dd-yy").ToUpper() + "-" + to.ToString("MM-dd-yy").ToUpper();
                    byte[] bytes;

                    using (OfficeOpenXml.ExcelPackage ep = new OfficeOpenXml.ExcelPackage()) {
                       ExcelWorksheet ew = ep.Workbook.Worksheets.Add(rep.accountNo.Replace(" ","-").ToLower() + "-" + rep.accountName.Replace(" ","-").ToLower());

                       ew.Cells["A1"].Value = "KABALIKAT PARA SA MAUNLAD NA BUHAY, INC.";
                       ew.Cells["A2"].Value = "SCHEDULE OF " + rep.accountName;
                       ew.Cells["A3"].Value = "FROM " + rep.from.ToString("MMMM-dd-yyyy") + " TO " + rep.to.ToString("MMMM-dd-yyyy")  ;
                       ew.Cells["A1:A3"].Style.Font.Bold = true;

                       ew.Cells["A5"].Value = "DATE";
                       ew.Cells["B5"].Value = "REF";
                       ew.Cells["C5"].Value = "PARTICULARS";
                       ew.Cells["D5"].Value = "DR.";
                       ew.Cells["E5"].Value = "CR.";
                       ew.Cells["F5"].Value = "BALANCE";
                       ew.Cells["A5:F5"].Style.Font.Bold = true;
                       ew.Cells["A5:F5"].Style.Font.UnderLine = true;  
                       ew.Cells["A5:F5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                       var liner = 6;

                       rep.subsidiary.ToList().ForEach(sub => {
                           ew.Cells[liner, 3].Value = sub.particulars;
                           ew.Cells[liner, 3].Style.Font.Bold = true;
                           ew.Cells[liner, 4].Value = sub.previousEntryDebit;
                           ew.Cells[liner, 5].Value = sub.previousEntryCredit;
                           ew.Cells[liner, 6].Value = sub.previousEntry;
                           liner++;
                           sub.details.ForEach(tl => {

                               tl.details.ForEach(det=>{

                               ew.Cells[liner, 1].Value = det.date.ToString("MM/dd/yyyy");
                               ew.Cells[liner, 2].Value = det.reference;
                               ew.Cells[liner, 3].Value = det.particulars;

                               ew.Cells[liner, 4].Value = det.dr;
                               ew.Cells[liner, 5].Value = det.cr;
                               ew.Cells[liner, 6].Value = det.balance;
                               liner++;
                               });

                               ew.Cells[liner, 1].Value = "TOTAL AS OF " + tl.month.ToString("MMMM yyyy").ToUpper();
                               ew.Cells[liner, 6].Value = tl.balance;
                               ew.Cells[liner,1,liner,6].Style.Font.Bold = true;

                               liner++;liner++;
                           });
                           liner++;
                       });

                       // ew.Cells[liner,3].Value = "Total DR/CR";
                       // ew.Cells[liner,3].Style.Font.Bold = true;
                       // ew.Cells[liner,4].Value = list.Where(l=>l.accountType == "debit").Sum(l=>l.amount);
                       // ew.Cells[liner,5].Value = list.Where(l=>l.accountType == "credit").Sum(l=>l.amount);
                       // ew.Cells[liner,6].Value = list.Sum(l=>l.amount);

                       ew.Cells[6,1,liner,6].Style.Numberformat.Format = "#,##0.00;(#,##0.00);-";

                       // formatting

                       ew.Column(1).Width = 13;
                       ew.Column(2).Width = 14;
                       ew.Column(3).Width = 130;
                       ew.Column(4).Width = 18;
                       ew.Column(5).Width = 18;
                       ew.Column(6).Width = 18;

                       bytes = ep.GetAsByteArray();
                       ep.Dispose();
                       var link = _upload.upload(bytes,"kmbidevreporeports/" + filename + ".xlsx");
                       return Redirect(link);
                    }
                }
            }


        }

        [HttpGet("getGeneralLedgerDetailed")]
        public IActionResult getGeneralLedgerDetailed([FromQuery]Int32? account, [FromQuery]Int32? year, [FromQuery]Int32? test, [FromQuery]Int32 month, [FromQuery]Boolean isData = false) {
            var rep = _acc.generalLedger(account, year);

            _rep.templateFilename = "generalLedgerDetailed.cshtml";
            _rep.model = rep;
            _rep.orientation = "portrait";
            _rep.footerText = "System Generated Report - Accounting System - " + DateTime.Now;
            
            _rep.isHalf = false;
            _rep.pageType = "letter";
            dynamic vb = new System.Dynamic.ExpandoObject();
            vb.year = year;
            // vb.month = DateTime.Parse(month.ToString() + "/1/2001").ToString("MMMM");
            vb.account = account;
            _rep.viewBag = vb;

            _rep.title = "General Ledger Detailed - " + account + " " + vb.year;

            iResult res = _rep.generatePDF();

            var link = _upload.upload(res.bytes,"kmbidevreporeports/" + _rep.title.Replace(" ","_") + ".pdf");

            if(rep!=null) {
                if(isData) {
                    return Ok(rep);
                }
                if(rep.Count() > 0) {
                    switch (test) {
                        case 1:
                            return Content(link);
                        case 2:
                            return View(relativeTemplatePath + _rep.templateFilename,rep);
                        default:
                            return Redirect(link);
                    }
                } else {
                    return Ok(new {
                        type = "information",
                        message = "No data to generate"
                    });                

                }
            } else {
                return Ok(new {
                    type = "information",
                    message = "No data to generate"
                });
            }
        }

        [HttpGet("dailyBalances")]
        public IActionResult dailyBalances([FromQuery]DateTime from, [FromQuery]DateTime to, [FromQuery]Boolean withSubsidiary = false, [FromQuery]String type = "") {
            switch (type) {
                case "data": {
                    var rep = _reporting.dailyBalance(from, to);
                    return Ok(rep);
                }
                case "pdf": {
                    var rep = _reporting.dailyBalance(from, to);
                    _rep.templateFilename = "dailyBalances.cshtml";
                    _rep.model = rep;
                    _rep.orientation = "portrait";
                    _rep.footerText = "System Generated Report - Accounting System - " + DateTime.Now;
                    _rep.title = "Daily Balances (" + from.ToString("MM-dd-yyyy") + " - " + to.ToString("MM-dd-yyyy") + ")";
                    _rep.isHalf = false;
                    _rep.pageType = "letter";
                    iResult res = _rep.generatePDF();

                    var link = _upload.upload(res.bytes,"kmbidevreporeports/" + _rep.title.Replace(" ","_") + ".pdf");
                    return Redirect(link);
                }
                default: {
                    var rep = _reporting.dailyBalance(from, to);

                    using (OfficeOpenXml.ExcelPackage ep = new OfficeOpenXml.ExcelPackage()) {
                       ExcelWorksheet ew = ep.Workbook.Worksheets.Add("DBA");

                       ew.Cells["A1:E1"].Merge = true;
                       ew.Cells["A1:E1"].Value = "KABALIKAT PARA SA MAUNLAD NA BUHAY, INC.";
                       ew.Cells["A2:E2"].Merge = true;
                       ew.Cells["A2:E2"].Value = "HEAD OFFICE";
                       ew.Cells["A3:E3"].Merge = true;
                       ew.Cells["A3:E3"].Value = "DAILY BALANCES OF ACCOUNTS";
                       ew.Cells["A4:E4"].Merge = true;
                       ew.Cells["A4:E4"].Value = from.ToString("MM/dd/yyyy") + " - " + to.ToString("MM/dd/yyyy") + " FINAL";
                       

                       ew.Cells["A6"].Value = "Acct.";
                       ew.Cells["B6"].Value = "Account Title";
                       ew.Cells["C6"].Value = "Previous";
                       ew.Cells["D6"].Value = "Today";
                       ew.Cells["E6"].Value = "To Date";

                        ew.Cells["A1:E5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        ew.Cells["A1:E5"].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                       var liner = 7;
                       rep.assetDetails.ToList().ForEach(item=>{
                           ew.Cells[liner,1].Value = item.acct;
                           ew.Cells[liner,2].Value = item.accountTitle;
                           ew.Cells[liner,3].Value = item.previous;
                           ew.Cells[liner,4].Value = item.today;
                           ew.Cells[liner,5].Value = item.toDate;
                           liner++;
                           if(item.subsidiaries.Length > 0) {
                               item.subsidiaries.ToList().ForEach(item2=>{
                                ew.Cells[liner,1].Value = item2.acct;
                                ew.Cells[liner,2].Value = item2.accountTitle;
                                ew.Cells[liner,3].Value = item2.previous;
                                ew.Cells[liner,4].Value = item2.today;
                                ew.Cells[liner,5].Value = item2.toDate;
                                ew.Cells[liner,1,liner,5].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                                liner++;
                               });
                           }
                           ew.Cells[liner,1,liner,5].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                       });
                       liner++;

                       rep.liabilitiesDetails.ToList().ForEach(item=>{
                           ew.Cells[liner,1].Value = item.acct;
                           ew.Cells[liner,2].Value = item.accountTitle;
                           ew.Cells[liner,3].Value = item.previous;
                           ew.Cells[liner,4].Value = item.today;
                           ew.Cells[liner,5].Value = item.toDate;
                           liner++;
                           if(item.subsidiaries.Length > 0) {
                               item.subsidiaries.ToList().ForEach(item2=>{
                                ew.Cells[liner,1].Value = item2.acct;
                                ew.Cells[liner,2].Value = item2.accountTitle;
                                ew.Cells[liner,3].Value = item2.previous;
                                ew.Cells[liner,4].Value = item2.today;
                                ew.Cells[liner,5].Value = item2.toDate;
                                ew.Cells[liner,1,liner,5].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                                liner++;
                               });
                           }
                           ew.Cells[liner,1,liner,5].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                       });
                       liner++;

                       rep.fundBalanceDetails.ToList().ForEach(item=>{
                           ew.Cells[liner,1].Value = item.acct;
                           ew.Cells[liner,2].Value = item.accountTitle;
                           ew.Cells[liner,3].Value = item.previous;
                           ew.Cells[liner,4].Value = item.today;
                           ew.Cells[liner,5].Value = item.toDate;
                           liner++;
                           if(item.subsidiaries.Length > 0) {
                               item.subsidiaries.ToList().ForEach(item2=>{
                                ew.Cells[liner,1].Value = item2.acct;
                                ew.Cells[liner,2].Value = item2.accountTitle;
                                ew.Cells[liner,3].Value = item2.previous;
                                ew.Cells[liner,4].Value = item2.today;
                                ew.Cells[liner,5].Value = item2.toDate;
                                ew.Cells[liner,1,liner,5].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                                liner++;
                               });
                           }
                           ew.Cells[liner,1,liner,5].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                       });
                       liner++;

                       rep.incomeDetails.ToList().ForEach(item=>{
                           ew.Cells[liner,1].Value = item.acct;
                           ew.Cells[liner,2].Value = item.accountTitle;
                           ew.Cells[liner,3].Value = item.previous;
                           ew.Cells[liner,4].Value = item.today;
                           ew.Cells[liner,5].Value = item.toDate;
                           liner++;
                           if(item.subsidiaries.Length > 0) {
                               item.subsidiaries.ToList().ForEach(item2=>{
                                ew.Cells[liner,1].Value = item2.acct;
                                ew.Cells[liner,2].Value = item2.accountTitle;
                                ew.Cells[liner,3].Value = item2.previous;
                                ew.Cells[liner,4].Value = item2.today;
                                ew.Cells[liner,5].Value = item2.toDate;
                                ew.Cells[liner,1,liner,5].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                                liner++;
                               });
                           }
                           ew.Cells[liner,1,liner,5].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                       });
                       liner++;

                       rep.expensesDetails.ToList().ForEach(item=>{
                           ew.Cells[liner,1].Value = item.acct;
                           ew.Cells[liner,2].Value = item.accountTitle;
                           ew.Cells[liner,3].Value = item.previous;
                           ew.Cells[liner,4].Value = item.today;
                           ew.Cells[liner,5].Value = item.toDate;
                           liner++;
                           if(item.subsidiaries.Length > 0) {
                               item.subsidiaries.ToList().ForEach(item2=>{
                                ew.Cells[liner,1].Value = item2.acct;
                                ew.Cells[liner,2].Value = item2.accountTitle;
                                ew.Cells[liner,3].Value = item2.previous;
                                ew.Cells[liner,4].Value = item2.today;
                                ew.Cells[liner,5].Value = item2.toDate;
                                ew.Cells[liner,1,liner,5].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                                liner++;
                               });
                           }
                           ew.Cells[liner,1,liner,5].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                       });
                       liner++;

                       ew.Cells[liner,2].Value = "Variance";
                       ew.Cells[liner,3].Value = rep.variance.previous;
                       ew.Cells[liner,4].Value = rep.variance.today;
                       ew.Cells[liner,5].Value = rep.variance.toDate;
                       ew.Cells[liner,1,liner,5].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                       liner+=2;
                       ew.Cells[liner,1, liner, 5].Merge = true;
                       ew.Cells[liner,1, liner, 5].Value = "SUMMARY";
                       ew.Cells[liner,1, liner, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                       liner++;

                       ew.Cells[liner,2].Value = "Asset";
                       ew.Cells[liner,3].Value = rep.summary.assetSummary.previous;
                       ew.Cells[liner,4].Value = rep.summary.assetSummary.today;
                       ew.Cells[liner,5].Value = rep.summary.assetSummary.toDate;
                       liner++;

                       ew.Cells[liner,2].Value = "Liabilities";
                       ew.Cells[liner,3].Value = rep.summary.liabilitiesSummary.previous;
                       ew.Cells[liner,4].Value = rep.summary.liabilitiesSummary.today;
                       ew.Cells[liner,5].Value = rep.summary.liabilitiesSummary.toDate;
                       liner++;

                       ew.Cells[liner,2].Value = "Fund Balance";
                       ew.Cells[liner,3].Value = rep.summary.fundBalanceSummary.previous;
                       ew.Cells[liner,4].Value = rep.summary.fundBalanceSummary.today;
                       ew.Cells[liner,5].Value = rep.summary.fundBalanceSummary.toDate;
                       liner++;

                       ew.Cells[liner,2].Value = "Income";
                       ew.Cells[liner,3].Value = rep.summary.incomeSummary.previous;
                       ew.Cells[liner,4].Value = rep.summary.incomeSummary.today;
                       ew.Cells[liner,5].Value = rep.summary.incomeSummary.toDate;
                       liner++;

                       ew.Cells[liner,2].Value = "Expenses";
                       ew.Cells[liner,3].Value = rep.summary.expensesSummary.previous;
                       ew.Cells[liner,4].Value = rep.summary.expensesSummary.today;
                       ew.Cells[liner,5].Value = rep.summary.expensesSummary.toDate;
                       liner++;

                       ew.Cells[liner,2].Value = "NET INCOME (NET LOSS)";
                       ew.Cells[liner,3].Value = rep.summary.netIncomeLoss.previous;
                       ew.Cells[liner,4].Value = rep.summary.netIncomeLoss.today;
                       ew.Cells[liner,5].Value = rep.summary.netIncomeLoss.toDate;
                       liner++;
                       ew.Cells[liner+2,1].Style.Font.Italic = true;
                       ew.Cells[liner+2,1].Value = "Note: Except for the SUMMARY, positive amount represents debit balance, negative amount represents credit balance";


                        ew.Column(1).Width = 8;
                        ew.Column(2).Width = 60;
                        ew.Column(3).Width = 18;
                        ew.Column(4).Width = 18;
                        ew.Column(5).Width = 18;

                        ew.Cells[7,1,liner,5].Style.Numberformat.Format = "#,##0.00;(#,##0.00);-";
                        ew.Cells[liner-7,1,liner-1,5].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        

                       var bytes = ep.GetAsByteArray();
                       ep.Dispose();
                       var link = _upload.upload(bytes,"kmbidevreporeports/dba.xlsx");
                       return Redirect(link);
                    }
                }
            }
        }

        [HttpGet("fs/balanceSheet")]
        public IActionResult balanceSheet([FromQuery]DateTime asOf, [FromQuery]String type = "default") {
            switch(type) {
                case "pdf": {
                    return Ok();
                }
                case "data": {
                    var rep = _reporting.balanceSheet2(asOf);
                    return Ok(rep);
                }
                default: {
                    var rep = _reporting.balanceSheet2(asOf);
                    using (OfficeOpenXml.ExcelPackage ep = new OfficeOpenXml.ExcelPackage())
                    {
                        ExcelWorksheet ew = ep.Workbook.Worksheets.Add("BS");
                        
                        
                        ew.Cells["A1"].Value = "KABALIKAT PARA SA MAUNLAD NA BUHAY, INC.";
                        ew.Cells["A1:F1"].Merge = true;
                        ew.Cells["A1:F1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        ew.Cells["A2"].Value = "( A Non-Stock, Non-Profit Organization )";
                        ew.Cells["A2:F2"].Merge = true;
                        ew.Cells["A2:F2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        ew.Cells["A4"].Value = "STATEMENT OF ASSETS, LIABILITIES AND FUND BALANCE";
                        ew.Cells["A4:F4"].Merge = true;
                        ew.Cells["A4:F4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        ew.Cells["A5"].Value = asOf.ToString("MMMM dd, yyyy");
                        ew.Cells["A5:F5"].Merge = true;
                        ew.Cells["A5:F5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        ew.Cells["B8"].Value = "NOTE";
                        ew.Cells["C8"].Value = rep.currentYear;
                        ew.Cells["D8"].Value = rep.previousYear;
                        ew.Cells["E8"].Value = "INCREASE (DECREASE)";
                        ew.Cells["F8"].Value = "%";

                        ew.Cells[10,1].Value = "ASSETS";
                        ew.Cells[12,1].Value = "Current Assets";

                        ew.Cells[13,1].Value = rep.assets.currentAssets.cashAndCashEquivalents.name;
                        ew.Cells[13,2].Value = rep.assets.currentAssets.cashAndCashEquivalents.note;
                        ew.Cells[13,3].Value = rep.assets.currentAssets.cashAndCashEquivalents.currentYear;
                        ew.Cells[13,4].Value = rep.assets.currentAssets.cashAndCashEquivalents.previousYear;
                        ew.Cells[13,5].Value = rep.assets.currentAssets.cashAndCashEquivalents.increaseDecrease;
                        ew.Cells[13,6].Value = rep.assets.currentAssets.cashAndCashEquivalents.percentage + "%";

                        ew.Row(14).Hidden = true;

                        ew.Cells[15,1].Value = rep.assets.currentAssets.marketableEquitySecuritiesNet.name;
                        ew.Cells[15,2].Value = rep.assets.currentAssets.marketableEquitySecuritiesNet.note;
                        ew.Cells[15,3].Value = rep.assets.currentAssets.marketableEquitySecuritiesNet.currentYear;
                        ew.Cells[15,4].Value = rep.assets.currentAssets.marketableEquitySecuritiesNet.previousYear;
                        ew.Cells[15,5].Value = rep.assets.currentAssets.marketableEquitySecuritiesNet.increaseDecrease;
                        ew.Cells[15,6].Value = rep.assets.currentAssets.marketableEquitySecuritiesNet.percentage + "%";

                        ew.Cells[16,1].Value = rep.assets.currentAssets.loansReceivableNet.name;
                        ew.Cells[16,2].Value = rep.assets.currentAssets.loansReceivableNet.note;
                        ew.Cells[16,3].Value = rep.assets.currentAssets.loansReceivableNet.currentYear;
                        ew.Cells[16,4].Value = rep.assets.currentAssets.loansReceivableNet.previousYear;
                        ew.Cells[16,5].Value = rep.assets.currentAssets.loansReceivableNet.increaseDecrease;
                        ew.Cells[16,6].Value = rep.assets.currentAssets.loansReceivableNet.percentage + "%";

                        ew.Cells[17,1].Value = rep.assets.currentAssets.interestReceivable.name;
                        ew.Cells[17,2].Value = rep.assets.currentAssets.interestReceivable.note;
                        ew.Cells[17,3].Value = rep.assets.currentAssets.interestReceivable.currentYear;
                        ew.Cells[17,4].Value = rep.assets.currentAssets.interestReceivable.previousYear;
                        ew.Cells[17,5].Value = rep.assets.currentAssets.interestReceivable.increaseDecrease;
                        ew.Cells[17,6].Value = rep.assets.currentAssets.interestReceivable.percentage + "%";

                        ew.Cells[18,1].Value = rep.assets.currentAssets.otherReceivablesNet.name;
                        ew.Cells[18,2].Value = rep.assets.currentAssets.otherReceivablesNet.note;
                        ew.Cells[18,3].Value = rep.assets.currentAssets.otherReceivablesNet.currentYear;
                        ew.Cells[18,4].Value = rep.assets.currentAssets.otherReceivablesNet.previousYear;
                        ew.Cells[18,5].Value = rep.assets.currentAssets.otherReceivablesNet.increaseDecrease;
                        ew.Cells[18,6].Value = rep.assets.currentAssets.otherReceivablesNet.percentage + "%";

                        ew.Cells[19,1].Value = rep.assets.currentAssets.otherCurrentAssets.name;
                        ew.Cells[19,2].Value = rep.assets.currentAssets.otherCurrentAssets.note;
                        ew.Cells[19,3].Value = rep.assets.currentAssets.otherCurrentAssets.currentYear;
                        ew.Cells[19,4].Value = rep.assets.currentAssets.otherCurrentAssets.previousYear;
                        ew.Cells[19,5].Value = rep.assets.currentAssets.otherCurrentAssets.increaseDecrease;
                        ew.Cells[19,6].Value = rep.assets.currentAssets.otherCurrentAssets.percentage + "%";

                        ew.Cells[20,1].Value = rep.assets.currentAssets.totalCurrentAssets.name;
                        ew.Cells[20,2].Value = rep.assets.currentAssets.totalCurrentAssets.note;
                        ew.Cells[20,3].Value = rep.assets.currentAssets.totalCurrentAssets.currentYear;
                        ew.Cells[20,4].Value = rep.assets.currentAssets.totalCurrentAssets.previousYear;
                        ew.Cells[20,5].Value = rep.assets.currentAssets.totalCurrentAssets.increaseDecrease;
                        ew.Cells[20,6].Value = rep.assets.currentAssets.totalCurrentAssets.percentage + "%";

                        ew.Cells[22,1].Value = "Noncurrent Assets";

                        ew.Cells[23,1].Value = rep.assets.nonCurrentAssets.availableForSaleAfsFinancialAsset.name;
                        ew.Cells[23,2].Value = rep.assets.nonCurrentAssets.availableForSaleAfsFinancialAsset.note;
                        ew.Cells[23,3].Value = rep.assets.nonCurrentAssets.availableForSaleAfsFinancialAsset.currentYear;
                        ew.Cells[23,4].Value = rep.assets.nonCurrentAssets.availableForSaleAfsFinancialAsset.previousYear;
                        ew.Cells[23,5].Value = rep.assets.nonCurrentAssets.availableForSaleAfsFinancialAsset.increaseDecrease;
                        ew.Cells[23,6].Value = rep.assets.nonCurrentAssets.availableForSaleAfsFinancialAsset.percentage + "%";

                        ew.Cells[24,1].Value = rep.assets.nonCurrentAssets.restrictedCash.name;
                        ew.Cells[24,2].Value = rep.assets.nonCurrentAssets.restrictedCash.note;
                        ew.Cells[24,3].Value = rep.assets.nonCurrentAssets.restrictedCash.currentYear;
                        ew.Cells[24,4].Value = rep.assets.nonCurrentAssets.restrictedCash.previousYear;
                        ew.Cells[24,5].Value = rep.assets.nonCurrentAssets.restrictedCash.increaseDecrease;
                        ew.Cells[24,6].Value = rep.assets.nonCurrentAssets.restrictedCash.percentage + "%";

                        ew.Cells[25,1].Value = rep.assets.nonCurrentAssets.propertyAndEquipment.name;
                        ew.Cells[25,2].Value = rep.assets.nonCurrentAssets.propertyAndEquipment.note;
                        ew.Cells[25,3].Value = rep.assets.nonCurrentAssets.propertyAndEquipment.currentYear;
                        ew.Cells[25,4].Value = rep.assets.nonCurrentAssets.propertyAndEquipment.previousYear;
                        ew.Cells[25,5].Value = rep.assets.nonCurrentAssets.propertyAndEquipment.increaseDecrease;
                        ew.Cells[25,6].Value = rep.assets.nonCurrentAssets.propertyAndEquipment.percentage + "%";

                        ew.Cells[26,1].Value = rep.assets.nonCurrentAssets.advancesToAffiliates.name;
                        ew.Cells[26,2].Value = rep.assets.nonCurrentAssets.advancesToAffiliates.note;
                        ew.Cells[26,3].Value = rep.assets.nonCurrentAssets.advancesToAffiliates.currentYear;
                        ew.Cells[26,4].Value = rep.assets.nonCurrentAssets.advancesToAffiliates.previousYear;
                        ew.Cells[26,5].Value = rep.assets.nonCurrentAssets.advancesToAffiliates.increaseDecrease;
                        ew.Cells[26,6].Value = rep.assets.nonCurrentAssets.advancesToAffiliates.percentage + "%";

                        ew.Cells[27,1].Value = rep.assets.nonCurrentAssets.otherNoncurrentAssets.name;
                        ew.Cells[27,2].Value = rep.assets.nonCurrentAssets.otherNoncurrentAssets.note;
                        ew.Cells[27,3].Value = rep.assets.nonCurrentAssets.otherNoncurrentAssets.currentYear;
                        ew.Cells[27,4].Value = rep.assets.nonCurrentAssets.otherNoncurrentAssets.previousYear;
                        ew.Cells[27,5].Value = rep.assets.nonCurrentAssets.otherNoncurrentAssets.increaseDecrease;
                        ew.Cells[27,6].Value = rep.assets.nonCurrentAssets.otherNoncurrentAssets.percentage + "%";

                        ew.Cells[28,1].Value = rep.assets.nonCurrentAssets.totalNonCurrentAssets.name;
                        ew.Cells[28,2].Value = rep.assets.nonCurrentAssets.totalNonCurrentAssets.note;
                        ew.Cells[28,3].Value = rep.assets.nonCurrentAssets.totalNonCurrentAssets.currentYear;
                        ew.Cells[28,4].Value = rep.assets.nonCurrentAssets.totalNonCurrentAssets.previousYear;
                        ew.Cells[28,5].Value = rep.assets.nonCurrentAssets.totalNonCurrentAssets.increaseDecrease;
                        ew.Cells[28,6].Value = rep.assets.nonCurrentAssets.totalNonCurrentAssets.percentage + "%";

                        for(Int32 ctr=30;ctr<=42;ctr++)
                            ew.Row(ctr).Hidden = true;

                        ew.Cells[43,1].Value = rep.assets.totalAssets.name;
                        ew.Cells[43,2].Value = rep.assets.totalAssets.note;
                        ew.Cells[43,3].Value = rep.assets.totalAssets.currentYear;
                        ew.Cells[43,4].Value = rep.assets.totalAssets.previousYear;
                        ew.Cells[43,5].Value = rep.assets.totalAssets.increaseDecrease;
                        ew.Cells[43,6].Value = rep.assets.totalAssets.percentage + "%";

                        ew.Cells[46,1].Value = "LIABILITIES AND FUND BALANCE";

                        ew.Cells[48,1].Value = "Current Liabilities";

                        ew.Cells[49,1].Value = rep.liabilitiesAndFundBalance.currentLiabilities.tradeAndOtherPayables.name;
                        ew.Cells[49,2].Value = rep.liabilitiesAndFundBalance.currentLiabilities.tradeAndOtherPayables.note;
                        ew.Cells[49,3].Value = rep.liabilitiesAndFundBalance.currentLiabilities.tradeAndOtherPayables.currentYear;
                        ew.Cells[49,4].Value = rep.liabilitiesAndFundBalance.currentLiabilities.tradeAndOtherPayables.previousYear;
                        ew.Cells[49,5].Value = rep.liabilitiesAndFundBalance.currentLiabilities.tradeAndOtherPayables.increaseDecrease;
                        ew.Cells[49,6].Value = rep.liabilitiesAndFundBalance.currentLiabilities.tradeAndOtherPayables.percentage + "%";

                        ew.Cells[50,1].Value = rep.liabilitiesAndFundBalance.currentLiabilities.clientsCbu.name;
                        ew.Cells[50,2].Value = rep.liabilitiesAndFundBalance.currentLiabilities.clientsCbu.note;
                        ew.Cells[50,3].Value = rep.liabilitiesAndFundBalance.currentLiabilities.clientsCbu.currentYear;
                        ew.Cells[50,4].Value = rep.liabilitiesAndFundBalance.currentLiabilities.clientsCbu.previousYear;
                        ew.Cells[50,5].Value = rep.liabilitiesAndFundBalance.currentLiabilities.clientsCbu.increaseDecrease;
                        ew.Cells[50,6].Value = rep.liabilitiesAndFundBalance.currentLiabilities.clientsCbu.percentage + "%";

                        ew.Cells[51,1].Value = rep.liabilitiesAndFundBalance.currentLiabilities.taxPayable.name;
                        ew.Cells[51,2].Value = rep.liabilitiesAndFundBalance.currentLiabilities.taxPayable.note;
                        ew.Cells[51,3].Value = rep.liabilitiesAndFundBalance.currentLiabilities.taxPayable.currentYear;
                        ew.Cells[51,4].Value = rep.liabilitiesAndFundBalance.currentLiabilities.taxPayable.previousYear;
                        ew.Cells[51,5].Value = rep.liabilitiesAndFundBalance.currentLiabilities.taxPayable.increaseDecrease;
                        ew.Cells[51,6].Value = rep.liabilitiesAndFundBalance.currentLiabilities.taxPayable.percentage + "%";

                        ew.Cells[52,1].Value = rep.liabilitiesAndFundBalance.currentLiabilities.miCgliPayables.name;
                        ew.Cells[52,2].Value = rep.liabilitiesAndFundBalance.currentLiabilities.miCgliPayables.note;
                        ew.Cells[52,3].Value = rep.liabilitiesAndFundBalance.currentLiabilities.miCgliPayables.currentYear;
                        ew.Cells[52,4].Value = rep.liabilitiesAndFundBalance.currentLiabilities.miCgliPayables.previousYear;
                        ew.Cells[52,5].Value = rep.liabilitiesAndFundBalance.currentLiabilities.miCgliPayables.increaseDecrease;
                        ew.Cells[52,6].Value = rep.liabilitiesAndFundBalance.currentLiabilities.miCgliPayables.percentage + "%";

                        ew.Cells[53,1].Value = rep.liabilitiesAndFundBalance.currentLiabilities.unearnedInterestIncome.name;
                        ew.Cells[53,2].Value = rep.liabilitiesAndFundBalance.currentLiabilities.unearnedInterestIncome.note;
                        ew.Cells[53,3].Value = rep.liabilitiesAndFundBalance.currentLiabilities.unearnedInterestIncome.currentYear;
                        ew.Cells[53,4].Value = rep.liabilitiesAndFundBalance.currentLiabilities.unearnedInterestIncome.previousYear;
                        ew.Cells[53,5].Value = rep.liabilitiesAndFundBalance.currentLiabilities.unearnedInterestIncome.increaseDecrease;
                        ew.Cells[53,6].Value = rep.liabilitiesAndFundBalance.currentLiabilities.unearnedInterestIncome.percentage + "%";

                        ew.Cells[54,1].Value = rep.liabilitiesAndFundBalance.currentLiabilities.provisionForContingencies.name;
                        ew.Cells[54,2].Value = rep.liabilitiesAndFundBalance.currentLiabilities.provisionForContingencies.note;
                        ew.Cells[54,3].Value = rep.liabilitiesAndFundBalance.currentLiabilities.provisionForContingencies.currentYear;
                        ew.Cells[54,4].Value = rep.liabilitiesAndFundBalance.currentLiabilities.provisionForContingencies.previousYear;
                        ew.Cells[54,5].Value = rep.liabilitiesAndFundBalance.currentLiabilities.provisionForContingencies.increaseDecrease;
                        ew.Cells[54,6].Value = rep.liabilitiesAndFundBalance.currentLiabilities.provisionForContingencies.percentage + "%";

                        ew.Cells[55,1].Value = rep.liabilitiesAndFundBalance.currentLiabilities.totalCurrentLiabilities.name;
                        ew.Cells[55,2].Value = rep.liabilitiesAndFundBalance.currentLiabilities.totalCurrentLiabilities.note;
                        ew.Cells[55,3].Value = rep.liabilitiesAndFundBalance.currentLiabilities.totalCurrentLiabilities.currentYear;
                        ew.Cells[55,4].Value = rep.liabilitiesAndFundBalance.currentLiabilities.totalCurrentLiabilities.previousYear;
                        ew.Cells[55,5].Value = rep.liabilitiesAndFundBalance.currentLiabilities.totalCurrentLiabilities.increaseDecrease;
                        ew.Cells[55,6].Value = rep.liabilitiesAndFundBalance.currentLiabilities.totalCurrentLiabilities.percentage + "%";

                        ew.Cells[57,1].Value = "Noncurrent Liabilities";

                        ew.Cells[58,1].Value = rep.liabilitiesAndFundBalance.nonCurrentLiabilities.deferredTaxLiability.name;
                        ew.Cells[58,2].Value = rep.liabilitiesAndFundBalance.nonCurrentLiabilities.deferredTaxLiability.note;
                        ew.Cells[58,3].Value = rep.liabilitiesAndFundBalance.nonCurrentLiabilities.deferredTaxLiability.currentYear;
                        ew.Cells[58,4].Value = rep.liabilitiesAndFundBalance.nonCurrentLiabilities.deferredTaxLiability.previousYear;
                        ew.Cells[58,5].Value = rep.liabilitiesAndFundBalance.nonCurrentLiabilities.deferredTaxLiability.increaseDecrease;
                        ew.Cells[58,6].Value = rep.liabilitiesAndFundBalance.nonCurrentLiabilities.deferredTaxLiability.percentage + "%";

                        ew.Cells[59,1].Value = rep.liabilitiesAndFundBalance.nonCurrentLiabilities.retirementBenefitLiability.name;
                        ew.Cells[59,2].Value = rep.liabilitiesAndFundBalance.nonCurrentLiabilities.retirementBenefitLiability.note;
                        ew.Cells[59,3].Value = rep.liabilitiesAndFundBalance.nonCurrentLiabilities.retirementBenefitLiability.currentYear;
                        ew.Cells[59,4].Value = rep.liabilitiesAndFundBalance.nonCurrentLiabilities.retirementBenefitLiability.previousYear;
                        ew.Cells[59,5].Value = rep.liabilitiesAndFundBalance.nonCurrentLiabilities.retirementBenefitLiability.increaseDecrease;
                        ew.Cells[59,6].Value = rep.liabilitiesAndFundBalance.nonCurrentLiabilities.retirementBenefitLiability.percentage + "%";

                        ew.Cells[60,1].Value = rep.liabilitiesAndFundBalance.nonCurrentLiabilities.totalNonCurrentLiabilities.name;
                        ew.Cells[60,2].Value = rep.liabilitiesAndFundBalance.nonCurrentLiabilities.totalNonCurrentLiabilities.note;
                        ew.Cells[60,3].Value = rep.liabilitiesAndFundBalance.nonCurrentLiabilities.totalNonCurrentLiabilities.currentYear;
                        ew.Cells[60,4].Value = rep.liabilitiesAndFundBalance.nonCurrentLiabilities.totalNonCurrentLiabilities.previousYear;
                        ew.Cells[60,5].Value = rep.liabilitiesAndFundBalance.nonCurrentLiabilities.totalNonCurrentLiabilities.increaseDecrease;
                        ew.Cells[60,6].Value = rep.liabilitiesAndFundBalance.nonCurrentLiabilities.totalNonCurrentLiabilities.percentage + "%";

                        ew.Cells[61,1].Value = rep.liabilitiesAndFundBalance.totalLiabilities.name;
                        ew.Cells[61,2].Value = rep.liabilitiesAndFundBalance.totalLiabilities.note;
                        ew.Cells[61,3].Value = rep.liabilitiesAndFundBalance.totalLiabilities.currentYear;
                        ew.Cells[61,4].Value = rep.liabilitiesAndFundBalance.totalLiabilities.previousYear;
                        ew.Cells[61,5].Value = rep.liabilitiesAndFundBalance.totalLiabilities.increaseDecrease;
                        ew.Cells[61,6].Value = rep.liabilitiesAndFundBalance.totalLiabilities.percentage + "%";

                        ew.Cells[63,1].Value = "Fund Balance";

                        ew.Cells[64,1].Value = rep.liabilitiesAndFundBalance.fundBalance.retainedEarnings.name;
                        ew.Cells[64,2].Value = rep.liabilitiesAndFundBalance.fundBalance.retainedEarnings.note;
                        ew.Cells[64,3].Value = rep.liabilitiesAndFundBalance.fundBalance.retainedEarnings.currentYear;
                        ew.Cells[64,4].Value = rep.liabilitiesAndFundBalance.fundBalance.retainedEarnings.previousYear;
                        ew.Cells[64,5].Value = rep.liabilitiesAndFundBalance.fundBalance.retainedEarnings.increaseDecrease;
                        ew.Cells[64,6].Value = rep.liabilitiesAndFundBalance.fundBalance.retainedEarnings.percentage + "%";

                        ew.Cells[65,1].Value = rep.liabilitiesAndFundBalance.fundBalance.cumulativeRemeasurementGainsOnRetirementBenefit.name;
                        ew.Cells[65,2].Value = rep.liabilitiesAndFundBalance.fundBalance.cumulativeRemeasurementGainsOnRetirementBenefit.note;
                        ew.Cells[65,3].Value = rep.liabilitiesAndFundBalance.fundBalance.cumulativeRemeasurementGainsOnRetirementBenefit.currentYear;
                        ew.Cells[65,4].Value = rep.liabilitiesAndFundBalance.fundBalance.cumulativeRemeasurementGainsOnRetirementBenefit.previousYear;
                        ew.Cells[65,5].Value = rep.liabilitiesAndFundBalance.fundBalance.cumulativeRemeasurementGainsOnRetirementBenefit.increaseDecrease;
                        ew.Cells[65,6].Value = rep.liabilitiesAndFundBalance.fundBalance.cumulativeRemeasurementGainsOnRetirementBenefit.percentage + "%";

                        ew.Cells[67,1].Value = rep.liabilitiesAndFundBalance.fundBalance.fairValueReserveOnAfsFinancialAsset.name;
                        ew.Cells[67,2].Value = rep.liabilitiesAndFundBalance.fundBalance.fairValueReserveOnAfsFinancialAsset.note;
                        ew.Cells[67,3].Value = rep.liabilitiesAndFundBalance.fundBalance.fairValueReserveOnAfsFinancialAsset.currentYear;
                        ew.Cells[67,4].Value = rep.liabilitiesAndFundBalance.fundBalance.fairValueReserveOnAfsFinancialAsset.previousYear;
                        ew.Cells[67,5].Value = rep.liabilitiesAndFundBalance.fundBalance.fairValueReserveOnAfsFinancialAsset.increaseDecrease;
                        ew.Cells[67,6].Value = rep.liabilitiesAndFundBalance.fundBalance.fairValueReserveOnAfsFinancialAsset.percentage + "%";

                        ew.Cells[68,1].Value = rep.liabilitiesAndFundBalance.fundBalance.totalFundBalance.name;
                        ew.Cells[68,2].Value = rep.liabilitiesAndFundBalance.fundBalance.totalFundBalance.note;
                        ew.Cells[68,3].Value = rep.liabilitiesAndFundBalance.fundBalance.totalFundBalance.currentYear;
                        ew.Cells[68,4].Value = rep.liabilitiesAndFundBalance.fundBalance.totalFundBalance.previousYear;
                        ew.Cells[68,5].Value = rep.liabilitiesAndFundBalance.fundBalance.totalFundBalance.increaseDecrease;
                        ew.Cells[68,6].Value = rep.liabilitiesAndFundBalance.fundBalance.totalFundBalance.percentage + "%";

                        ew.Cells[70,1].Value = rep.liabilitiesAndFundBalance.totalLiabilitiesAndFundBalance.name;
                        ew.Cells[70,2].Value = rep.liabilitiesAndFundBalance.totalLiabilitiesAndFundBalance.note;
                        ew.Cells[70,3].Value = rep.liabilitiesAndFundBalance.totalLiabilitiesAndFundBalance.currentYear;
                        ew.Cells[70,4].Value = rep.liabilitiesAndFundBalance.totalLiabilitiesAndFundBalance.previousYear;
                        ew.Cells[70,5].Value = rep.liabilitiesAndFundBalance.totalLiabilitiesAndFundBalance.increaseDecrease;
                        ew.Cells[70,6].Value = rep.liabilitiesAndFundBalance.totalLiabilitiesAndFundBalance.percentage + "%";

                        ew.Cells[73,1].Value = rep.totals.name;
                        ew.Cells[73,2].Value = rep.totals.note;
                        ew.Cells[73,3].Value = rep.totals.currentYear;
                        ew.Cells[73,4].Value = rep.totals.previousYear;
                        ew.Cells[73,5].Value = rep.totals.increaseDecrease;
                        ew.Cells[73,6].Value = rep.totals.percentage + "%";


                        ew.Column(1).Width = 35;
                        ew.Column(2).Width = 5;
                        ew.Column(3).Width = 21;
                        ew.Column(4).Width = 21;
                        ew.Column(5).Width = 21;
                        ew.Column(6).Width = 12;
                        ew.Column(6).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                        ew.Cells[13,3,73,6].Style.Numberformat.Format = "#,##0.00;(#,##0.00);-#";

                        ExcelWorksheet ewDetails = ep.Workbook.Worksheets.Add("details");
                        var _count = rep.details.Count();

                        ewDetails.Cells[1,1].Value = "slug";
                        ewDetails.Cells[1,2].Value = "book";
                        ewDetails.Cells[1,3].Value = "number";
                        ewDetails.Cells[1,4].Value = "particulars";
                        ewDetails.Cells[1,5].Value = "accountType";
                        ewDetails.Cells[1,6].Value = "motherCode";
                        ewDetails.Cells[1,7].Value = "code";
                        ewDetails.Cells[1,8].Value = "element";
                        ewDetails.Cells[1,9].Value = "name";
                        ewDetails.Cells[1,10].Value = "amount";
                        ewDetails.Cells[1,11].Value = "issuedAt";
                        ewDetails.Cells[1,12].Value = "IS reference";

                        for(Int32 ctr=1;ctr<= _count - 1;ctr++) {
                            ewDetails.Cells[ctr+1,1].Value = rep.details[ctr].slug;
                            ewDetails.Cells[ctr+1,2].Value = rep.details[ctr].book;
                            ewDetails.Cells[ctr+1,3].Value = rep.details[ctr].number;
                            ewDetails.Cells[ctr+1,4].Value = rep.details[ctr].particulars;
                            ewDetails.Cells[ctr+1,5].Value = rep.details[ctr].accountType;
                            ewDetails.Cells[ctr+1,6].Value = rep.details[ctr].motherCode;
                            ewDetails.Cells[ctr+1,7].Value = rep.details[ctr].code;
                            ewDetails.Cells[ctr+1,8].Value = rep.details[ctr].codeElement;
                            ewDetails.Cells[ctr+1,9].Value = rep.details[ctr].name;
                            ewDetails.Cells[ctr+1,10].Value = rep.details[ctr].amount;
                            ewDetails.Cells[ctr+1,11].Value = rep.details[ctr].issuedAt.ToShortDateString();
                            ewDetails.Cells[ctr+1,12].Value = rep.details[ctr].isItem;
                        }
                        
                        byte[] bytes = ep.GetAsByteArray();
                        ep.Dispose();
                        return File(bytes, "application/vnd.ms-excel", "balance-sheet-" + rep.asOf.ToString("MM-dd-yyyy") + ".xlsx");
                    
                    }
                }
            }
        }

        [HttpGet("fs/incomeStatement")]
        public IActionResult incomeStatement([FromQuery]DateTime asOf, [FromQuery]String type = "default") {
            switch(type) {
                case "pdf": {
                    var rep = _reporting.incomeStatement(asOf);
                    var data = JsonConvert.SerializeObject(rep);
                    var repDesc = "Income Statement";
                    return generateReport("incomestatement", data, repDesc);
                }
                case "data": {
                    var rep = _reporting.incomeStatement2(asOf);
                    return Ok(rep);
                }
                default: {
                    var rep = _reporting.incomeStatement2(asOf);
                    using (OfficeOpenXml.ExcelPackage ep = new OfficeOpenXml.ExcelPackage())
                    {
                        ExcelWorksheet ew = ep.Workbook.Worksheets.Add("IS");
                        
                        ew.Cells["A1"].Value = "KABALIKAT PARA SA MAUNLAD NA BUHAY, INC.";
                        ew.Cells["A1:F1"].Merge = true;
                        ew.Cells["A1:F1"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        ew.Cells["A2"].Value = "( A Non-Stock, Non-Profit Organization )";
                        ew.Cells["A2:F2"].Merge = true;
                        ew.Cells["A2:F2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        ew.Cells["A4"].Value = "STATEMENT OF COMPREHENSIVE INCOME";
                        ew.Cells["A4:F4"].Merge = true;
                        ew.Cells["A4:F4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        ew.Cells["A5"].Value = "FOR THE YEAR ENDED " + rep.asOf.ToString("MMMM dd, yyyy");
                        ew.Cells["A5:F5"].Merge = true;
                        ew.Cells["A5:F5"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        ew.Cells["A6"].Value = "( With Comparative Figures for " + rep.previousYear + " )";
                        ew.Cells["A6:F6"].Merge = true;
                        ew.Cells["A6:F6"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        ew.Cells["B8"].Value = "NOTE";
                        ew.Cells["C8"].Value = rep.currentYear;
                        ew.Cells["D8"].Value = rep.previousYear;
                        ew.Cells["E8"].Value = "INCREASE (DECREASE)";
                        ew.Cells["F8"].Value = "%";

                        
                        ew.Cells[10,1].Value = "REVENUE";
                        
                        ew.Cells[11,1].Value = rep.revenue.interestIncomeOnLoansReceivable.name;
                        ew.Cells[11,2].Value = rep.revenue.interestIncomeOnLoansReceivable.note;
                        ew.Cells[11,3].Value = rep.revenue.interestIncomeOnLoansReceivable.currentYear;
                        ew.Cells[11,4].Value = rep.revenue.interestIncomeOnLoansReceivable.previousYear;
                        ew.Cells[11,5].Value = rep.revenue.interestIncomeOnLoansReceivable.increaseDecrease;
                        ew.Cells[11,6].Value = rep.revenue.interestIncomeOnLoansReceivable.percentage + "%";

                        ew.Cells[12,1].Value = rep.revenue.serviceIncome.name;
                        ew.Cells[12,2].Value = rep.revenue.serviceIncome.note;
                        ew.Cells[12,3].Value = rep.revenue.serviceIncome.currentYear;
                        ew.Cells[12,4].Value = rep.revenue.serviceIncome.previousYear;
                        ew.Cells[12,5].Value = rep.revenue.serviceIncome.increaseDecrease;
                        ew.Cells[12,6].Value = rep.revenue.serviceIncome.percentage + "%";

                        ew.Cells[13,1].Value = rep.revenue.interestOnDepositAndPlacements.name;
                        ew.Cells[13,2].Value = rep.revenue.interestOnDepositAndPlacements.note;
                        ew.Cells[13,3].Value = rep.revenue.interestOnDepositAndPlacements.currentYear;
                        ew.Cells[13,4].Value = rep.revenue.interestOnDepositAndPlacements.previousYear;
                        ew.Cells[13,5].Value = rep.revenue.interestOnDepositAndPlacements.increaseDecrease;
                        ew.Cells[13,6].Value = rep.revenue.interestOnDepositAndPlacements.percentage + "%";

                        ew.Cells[14,1].Value = rep.revenue.forexGainOrLoss.name;
                        ew.Cells[14,2].Value = rep.revenue.forexGainOrLoss.note;
                        ew.Cells[14,3].Value = rep.revenue.forexGainOrLoss.currentYear;
                        ew.Cells[14,4].Value = rep.revenue.forexGainOrLoss.previousYear;
                        ew.Cells[14,5].Value = rep.revenue.forexGainOrLoss.increaseDecrease;
                        ew.Cells[14,6].Value = rep.revenue.forexGainOrLoss.percentage + "%";

                        ew.Cells[15,1].Value = rep.revenue.otherIncome.name;
                        ew.Cells[15,2].Value = rep.revenue.otherIncome.note;
                        ew.Cells[15,3].Value = rep.revenue.otherIncome.currentYear;
                        ew.Cells[15,4].Value = rep.revenue.otherIncome.previousYear;
                        ew.Cells[15,5].Value = rep.revenue.otherIncome.increaseDecrease;
                        ew.Cells[15,6].Value = rep.revenue.otherIncome.percentage + "%";

                        ew.Cells[16,1].Value = rep.revenue.totalGrossIncome.name;
                        ew.Cells[16,2].Value = rep.revenue.totalGrossIncome.note;
                        ew.Cells[16,3].Value = rep.revenue.totalGrossIncome.currentYear;
                        ew.Cells[16,4].Value = rep.revenue.totalGrossIncome.previousYear;
                        ew.Cells[16,5].Value = rep.revenue.totalGrossIncome.increaseDecrease;
                        ew.Cells[16,6].Value = rep.revenue.totalGrossIncome.percentage + "%";

                        ew.Cells[18,1].Value = rep.revenue.interestExpenseOnClientsCbu.name;
                        ew.Cells[18,2].Value = rep.revenue.interestExpenseOnClientsCbu.note;
                        ew.Cells[18,3].Value = rep.revenue.interestExpenseOnClientsCbu.currentYear;
                        ew.Cells[18,4].Value = rep.revenue.interestExpenseOnClientsCbu.previousYear;
                        ew.Cells[18,5].Value = rep.revenue.interestExpenseOnClientsCbu.increaseDecrease;
                        ew.Cells[18,6].Value = rep.revenue.interestExpenseOnClientsCbu.percentage + "%";

                        ew.Cells[19,1].Value = rep.revenue.totalFinancialCost.name;
                        ew.Cells[19,2].Value = rep.revenue.totalFinancialCost.note;
                        ew.Cells[19,3].Value = rep.revenue.totalFinancialCost.currentYear;
                        ew.Cells[19,4].Value = rep.revenue.totalFinancialCost.previousYear;
                        ew.Cells[19,5].Value = rep.revenue.totalFinancialCost.increaseDecrease;
                        ew.Cells[19,6].Value = rep.revenue.totalFinancialCost.percentage + "%";

                        ew.Cells[21,1].Value = rep.revenue.badDebtsExpense.name;
                        ew.Cells[21,2].Value = rep.revenue.badDebtsExpense.note;
                        ew.Cells[21,3].Value = rep.revenue.badDebtsExpense.currentYear;
                        ew.Cells[21,4].Value = rep.revenue.badDebtsExpense.previousYear;
                        ew.Cells[21,5].Value = rep.revenue.badDebtsExpense.increaseDecrease;
                        ew.Cells[21,6].Value = rep.revenue.badDebtsExpense.percentage + "%";

                        ew.Cells[23,1].Value = rep.revenue.netMargin.name;
                        ew.Cells[23,2].Value = rep.revenue.netMargin.note;
                        ew.Cells[23,3].Value = rep.revenue.netMargin.currentYear;
                        ew.Cells[23,4].Value = rep.revenue.netMargin.previousYear;
                        ew.Cells[23,5].Value = rep.revenue.netMargin.increaseDecrease;
                        ew.Cells[23,6].Value = rep.revenue.netMargin.percentage + "%";
                        
                        ew.Cells[25,1].Value = "EXPENSES";

                        ew.Cells[27,1].Value = rep.expenses.personnelCosts.name;
                        ew.Cells[27,2].Value = rep.expenses.personnelCosts.note;
                        ew.Cells[27,3].Value = rep.expenses.personnelCosts.currentYear;
                        ew.Cells[27,4].Value = rep.expenses.personnelCosts.previousYear;
                        ew.Cells[27,5].Value = rep.expenses.personnelCosts.increaseDecrease;
                        ew.Cells[27,6].Value = rep.expenses.personnelCosts.percentage + "%";

                        ew.Cells[28,1].Value = rep.expenses.outstation.name;
                        ew.Cells[28,2].Value = rep.expenses.outstation.note;
                        ew.Cells[28,3].Value = rep.expenses.outstation.currentYear;
                        ew.Cells[28,4].Value = rep.expenses.outstation.previousYear;
                        ew.Cells[28,5].Value = rep.expenses.outstation.increaseDecrease;
                        ew.Cells[28,6].Value = rep.expenses.outstation.percentage + "%";

                        ew.Cells[29,1].Value = rep.expenses.fringeBenefit.name;
                        ew.Cells[29,2].Value = rep.expenses.fringeBenefit.note;
                        ew.Cells[29,3].Value = rep.expenses.fringeBenefit.currentYear;
                        ew.Cells[29,4].Value = rep.expenses.fringeBenefit.previousYear;
                        ew.Cells[29,5].Value = rep.expenses.fringeBenefit.increaseDecrease;
                        ew.Cells[29,6].Value = rep.expenses.fringeBenefit.percentage + "%";

                        ew.Cells[30,1].Value = rep.expenses.retirement.name;
                        ew.Cells[30,2].Value = rep.expenses.retirement.note;
                        ew.Cells[30,3].Value = rep.expenses.retirement.currentYear;
                        ew.Cells[30,4].Value = rep.expenses.retirement.previousYear;
                        ew.Cells[30,5].Value = rep.expenses.retirement.increaseDecrease;
                        ew.Cells[30,6].Value = rep.expenses.retirement.percentage + "%";

                        ew.Cells[31,1].Value = rep.expenses.outsideServices.name;
                        ew.Cells[31,2].Value = rep.expenses.outsideServices.note;
                        ew.Cells[31,3].Value = rep.expenses.outsideServices.currentYear;
                        ew.Cells[31,4].Value = rep.expenses.outsideServices.previousYear;
                        ew.Cells[31,5].Value = rep.expenses.outsideServices.increaseDecrease;
                        ew.Cells[31,6].Value = rep.expenses.outsideServices.percentage + "%";

                        ew.Cells[32,1].Value = rep.expenses.professionalFees.name;
                        ew.Cells[32,2].Value = rep.expenses.professionalFees.note;
                        ew.Cells[32,3].Value = rep.expenses.professionalFees.currentYear;
                        ew.Cells[32,4].Value = rep.expenses.professionalFees.previousYear;
                        ew.Cells[32,5].Value = rep.expenses.professionalFees.increaseDecrease;
                        ew.Cells[32,6].Value = rep.expenses.professionalFees.percentage + "%";

                        ew.Cells[33,1].Value = rep.expenses.meetingsTrainingsAndConferences.name;
                        ew.Cells[33,2].Value = rep.expenses.meetingsTrainingsAndConferences.note;
                        ew.Cells[33,3].Value = rep.expenses.meetingsTrainingsAndConferences.currentYear;
                        ew.Cells[33,4].Value = rep.expenses.meetingsTrainingsAndConferences.previousYear;
                        ew.Cells[33,5].Value = rep.expenses.meetingsTrainingsAndConferences.increaseDecrease;
                        ew.Cells[33,6].Value = rep.expenses.meetingsTrainingsAndConferences.percentage + "%";

                        ew.Cells[34,1].Value = rep.expenses.transformationExpense.name;
                        ew.Cells[34,2].Value = rep.expenses.transformationExpense.note;
                        ew.Cells[34,3].Value = rep.expenses.transformationExpense.currentYear;
                        ew.Cells[34,4].Value = rep.expenses.transformationExpense.previousYear;
                        ew.Cells[34,5].Value = rep.expenses.transformationExpense.increaseDecrease;
                        ew.Cells[34,6].Value = rep.expenses.transformationExpense.percentage + "%";

                        ew.Cells[35,1].Value = rep.expenses.litigation.name;
                        ew.Cells[35,2].Value = rep.expenses.litigation.note;
                        ew.Cells[35,3].Value = rep.expenses.litigation.currentYear;
                        ew.Cells[35,4].Value = rep.expenses.litigation.previousYear;
                        ew.Cells[35,5].Value = rep.expenses.litigation.increaseDecrease;
                        ew.Cells[35,6].Value = rep.expenses.litigation.percentage + "%";

                        ew.Cells[36,1].Value = rep.expenses.supplies.name;
                        ew.Cells[36,2].Value = rep.expenses.supplies.note;
                        ew.Cells[36,3].Value = rep.expenses.supplies.currentYear;
                        ew.Cells[36,4].Value = rep.expenses.supplies.previousYear;
                        ew.Cells[36,5].Value = rep.expenses.supplies.increaseDecrease;
                        ew.Cells[36,6].Value = rep.expenses.supplies.percentage + "%";

                        ew.Cells[37,1].Value = rep.expenses.utilities.name;
                        ew.Cells[37,2].Value = rep.expenses.utilities.note;
                        ew.Cells[37,3].Value = rep.expenses.utilities.currentYear;
                        ew.Cells[37,4].Value = rep.expenses.utilities.previousYear;
                        ew.Cells[37,5].Value = rep.expenses.utilities.increaseDecrease;
                        ew.Cells[37,6].Value = rep.expenses.utilities.percentage + "%";

                        ew.Cells[38,1].Value = rep.expenses.communication.name;
                        ew.Cells[38,2].Value = rep.expenses.communication.note;
                        ew.Cells[38,3].Value = rep.expenses.communication.currentYear;
                        ew.Cells[38,4].Value = rep.expenses.communication.previousYear;
                        ew.Cells[38,5].Value = rep.expenses.communication.increaseDecrease;
                        ew.Cells[38,6].Value = rep.expenses.communication.percentage + "%";
                        
                        ew.Cells[39,1].Value = rep.expenses.courierCharges.name;
                        ew.Cells[39,2].Value = rep.expenses.courierCharges.note;
                        ew.Cells[39,3].Value = rep.expenses.courierCharges.currentYear;
                        ew.Cells[39,4].Value = rep.expenses.courierCharges.previousYear;
                        ew.Cells[39,5].Value = rep.expenses.courierCharges.increaseDecrease;
                        ew.Cells[39,6].Value = rep.expenses.courierCharges.percentage + "%";

                        ew.Cells[40,1].Value = rep.expenses.rental.name;
                        ew.Cells[40,2].Value = rep.expenses.rental.note;
                        ew.Cells[40,3].Value = rep.expenses.rental.currentYear;
                        ew.Cells[40,4].Value = rep.expenses.rental.previousYear;
                        ew.Cells[40,5].Value = rep.expenses.rental.increaseDecrease;
                        ew.Cells[40,6].Value = rep.expenses.rental.percentage + "%";

                        ew.Cells[41,1].Value = rep.expenses.insurance.name;
                        ew.Cells[41,2].Value = rep.expenses.insurance.note;
                        ew.Cells[41,3].Value = rep.expenses.insurance.currentYear;
                        ew.Cells[41,4].Value = rep.expenses.insurance.previousYear;
                        ew.Cells[41,5].Value = rep.expenses.insurance.increaseDecrease;
                        ew.Cells[41,6].Value = rep.expenses.insurance.percentage + "%";

                        ew.Cells[42,1].Value = rep.expenses.transporationAndTravel.name;
                        ew.Cells[42,2].Value = rep.expenses.transporationAndTravel.note;
                        ew.Cells[42,3].Value = rep.expenses.transporationAndTravel.currentYear;
                        ew.Cells[42,4].Value = rep.expenses.transporationAndTravel.previousYear;
                        ew.Cells[42,5].Value = rep.expenses.transporationAndTravel.increaseDecrease;
                        ew.Cells[42,6].Value = rep.expenses.transporationAndTravel.percentage + "%";

                        ew.Cells[43,1].Value = rep.expenses.taxesAndLicenses.name;
                        ew.Cells[43,2].Value = rep.expenses.taxesAndLicenses.note;
                        ew.Cells[43,3].Value = rep.expenses.taxesAndLicenses.currentYear;
                        ew.Cells[43,4].Value = rep.expenses.taxesAndLicenses.previousYear;
                        ew.Cells[43,5].Value = rep.expenses.taxesAndLicenses.increaseDecrease;
                        ew.Cells[43,6].Value = rep.expenses.taxesAndLicenses.percentage + "%";

                        ew.Cells[44,1].Value = rep.expenses.repairsAndMaintenance.name;
                        ew.Cells[44,2].Value = rep.expenses.repairsAndMaintenance.note;
                        ew.Cells[44,3].Value = rep.expenses.repairsAndMaintenance.currentYear;
                        ew.Cells[44,4].Value = rep.expenses.repairsAndMaintenance.previousYear;
                        ew.Cells[44,5].Value = rep.expenses.repairsAndMaintenance.increaseDecrease;
                        ew.Cells[44,6].Value = rep.expenses.repairsAndMaintenance.percentage + "%";

                        ew.Cells[45,1].Value = rep.expenses.depreciationAndAmortization.name;
                        ew.Cells[45,2].Value = rep.expenses.depreciationAndAmortization.note;
                        ew.Cells[45,3].Value = rep.expenses.depreciationAndAmortization.currentYear;
                        ew.Cells[45,4].Value = rep.expenses.depreciationAndAmortization.previousYear;
                        ew.Cells[45,5].Value = rep.expenses.depreciationAndAmortization.increaseDecrease;
                        ew.Cells[45,6].Value = rep.expenses.depreciationAndAmortization.percentage + "%";

                        ew.Cells[46,1].Value = rep.expenses.lossOnAssetWriteOff.name;
                        ew.Cells[46,2].Value = rep.expenses.lossOnAssetWriteOff.note;
                        ew.Cells[46,3].Value = rep.expenses.lossOnAssetWriteOff.currentYear;
                        ew.Cells[46,4].Value = rep.expenses.lossOnAssetWriteOff.previousYear;
                        ew.Cells[46,5].Value = rep.expenses.lossOnAssetWriteOff.increaseDecrease;
                        ew.Cells[46,6].Value = rep.expenses.lossOnAssetWriteOff.percentage + "%";

                        ew.Cells[47,1].Value = rep.expenses.others.name;
                        ew.Cells[47,2].Value = rep.expenses.others.note;
                        ew.Cells[47,3].Value = rep.expenses.others.currentYear;
                        ew.Cells[47,4].Value = rep.expenses.others.previousYear;
                        ew.Cells[47,5].Value = rep.expenses.others.increaseDecrease;
                        ew.Cells[47,6].Value = rep.expenses.others.percentage + "%";

                        ew.Cells[48,1].Value = rep.expenses.totalExpenses.name;
                        ew.Cells[48,2].Value = rep.expenses.totalExpenses.note;
                        ew.Cells[48,3].Value = rep.expenses.totalExpenses.currentYear;
                        ew.Cells[48,4].Value = rep.expenses.totalExpenses.previousYear;
                        ew.Cells[48,5].Value = rep.expenses.totalExpenses.increaseDecrease;
                        ew.Cells[48,6].Value = rep.expenses.totalExpenses.percentage + "%";

                        ew.Cells[50,1].Value = rep.netIncomeLossBeforeTax.name;
                        ew.Cells[50,2].Value = rep.netIncomeLossBeforeTax.note;
                        ew.Cells[50,3].Value = rep.netIncomeLossBeforeTax.currentYear;
                        ew.Cells[50,4].Value = rep.netIncomeLossBeforeTax.previousYear;
                        ew.Cells[50,5].Value = rep.netIncomeLossBeforeTax.increaseDecrease;
                        ew.Cells[50,6].Value = rep.netIncomeLossBeforeTax.percentage + "%";

                        ew.Cells[52,1].Value = rep.incomeTaxExpense.name;
                        ew.Cells[52,2].Value = rep.incomeTaxExpense.note;
                        ew.Cells[52,3].Value = rep.incomeTaxExpense.currentYear;
                        ew.Cells[52,4].Value = rep.incomeTaxExpense.previousYear;
                        ew.Cells[52,5].Value = rep.incomeTaxExpense.increaseDecrease;
                        ew.Cells[52,6].Value = rep.incomeTaxExpense.percentage + "%";

                        ew.Cells[54,1].Value = rep.netIncomeLoss.name;
                        ew.Cells[54,2].Value = rep.netIncomeLoss.note;
                        ew.Cells[54,3].Value = rep.netIncomeLoss.currentYear;
                        ew.Cells[54,4].Value = rep.netIncomeLoss.previousYear;
                        ew.Cells[54,5].Value = rep.netIncomeLoss.increaseDecrease;
                        ew.Cells[54,6].Value = rep.netIncomeLoss.percentage + "%";

                        ew.Column(1).Width = 35;
                        ew.Column(2).Width = 5;
                        ew.Column(3).Width = 21;
                        ew.Column(4).Width = 21;
                        ew.Column(5).Width = 21;
                        ew.Column(6).Width = 12;
                        ew.Column(6).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

                        ew.Cells[11,3,54,6].Style.Numberformat.Format = "#,##0.00;(#,##0.00);-#";

                        ExcelWorksheet ewDetails = ep.Workbook.Worksheets.Add("details");
                        var _count = rep.details.Count();

                        ewDetails.Cells[1,1].Value = "slug";
                        ewDetails.Cells[1,2].Value = "book";
                        ewDetails.Cells[1,3].Value = "number";
                        ewDetails.Cells[1,4].Value = "particulars";
                        ewDetails.Cells[1,5].Value = "accountType";
                        ewDetails.Cells[1,6].Value = "motherCode";
                        ewDetails.Cells[1,7].Value = "code";
                        ewDetails.Cells[1,8].Value = "element";
                        ewDetails.Cells[1,9].Value = "name";
                        ewDetails.Cells[1,10].Value = "amount";
                        ewDetails.Cells[1,11].Value = "issuedAt";
                        ewDetails.Cells[1,12].Value = "IS reference";

                        for(Int32 ctr=1;ctr<= _count - 1;ctr++) {
                            ewDetails.Cells[ctr+1,1].Value = rep.details[ctr].slug;
                            ewDetails.Cells[ctr+1,2].Value = rep.details[ctr].book;
                            ewDetails.Cells[ctr+1,3].Value = rep.details[ctr].number;
                            ewDetails.Cells[ctr+1,4].Value = rep.details[ctr].particulars;
                            ewDetails.Cells[ctr+1,5].Value = rep.details[ctr].accountType;
                            ewDetails.Cells[ctr+1,6].Value = rep.details[ctr].motherCode;
                            ewDetails.Cells[ctr+1,7].Value = rep.details[ctr].code;
                            ewDetails.Cells[ctr+1,8].Value = rep.details[ctr].codeElement;
                            ewDetails.Cells[ctr+1,9].Value = rep.details[ctr].name;
                            ewDetails.Cells[ctr+1,10].Value = rep.details[ctr].amount;
                            ewDetails.Cells[ctr+1,11].Value = rep.details[ctr].issuedAt.ToShortDateString();
                            ewDetails.Cells[ctr+1,12].Value = rep.details[ctr].isItem;
                        }
                        

                        byte[] bytes = ep.GetAsByteArray();
                        ep.Dispose();
                        return File(bytes, "application/vnd.ms-excel", "income-statement-" + rep.asOf.ToString("MM-dd-yyyy") + ".xlsx");
                    
                    }
                }
            }
        }
#endregion

        [HttpGet("lrcbu")]
        public IActionResult lrcbu([FromQuery]String branchSlug, [FromQuery]DateTime? date = null ) {

            
            if(date==null) {
                return Ok(new {
                    type = "failed",
                    message = "Date start is invalid"
                });
            }

            var _date = Convert.ToDateTime(date);
            var rep = _reporting.lrCbuReport(branchSlug, _date);
            
            if(rep!=null) {
                
                using (OfficeOpenXml.ExcelPackage ep = new OfficeOpenXml.ExcelPackage())
                {
                    rep.unit.ToList().ForEach(unit=>{
                        ExcelWorksheet ew = ep.Workbook.Worksheets.Add("LRCBU-" + unit.name.ToUpper().Substring(unit.name.Length-1,1));
                        ew.Cells["A1"].Value = "KABALIKAT PARA SA MAUNLAD NA BUHAY, INC.";
                        ew.Cells["A1"].Style.Font.Bold = true;
                        ew.Cells["A2"].Value = rep.branch.ToUpper() + " BRANCH";
                        ew.Cells["A2"].Style.Font.Bold = true;
                        ew.Cells["A3"].Value = "LOAN & CBU BALANCES UNIT " + unit.name.ToUpper().Substring(unit.name.Length-1,1);
                        ew.Cells["A3"].Style.Font.Bold = true;
                        ew.Cells["A4"].Value = "AS OF " + Convert.ToDateTime(date).ToString("MMMM dd, yyyy");
                        ew.Cells["A4"].Style.Font.Bold = true;

                        var liner = 6;

                        unit.po.ToList().ForEach(po=>{
                            ew.Cells[liner,1, liner+1,1].Value = "CENTER NAME";
                            ew.Cells[liner,1, liner+1,1].Merge = true;
                            ew.Cells[liner,2, liner+1,4].Value = "BALANCES";
                            ew.Cells[liner,2, liner+1,4].Merge = true;

                            ew.Cells[liner,5, liner,11].Value = "CLIENTS";
                            ew.Cells[liner,5, liner,11].Merge = true;

                            ew.Row(liner).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            ew.Row(liner).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            ew.Row(liner).Style.WrapText = true;
                            ew.Row(liner).Style.Font.Bold = true;
                            
                            for(Int32 ctr=1;ctr<=11;ctr++) {
                                ew.Cells[liner,ctr,liner,ctr].Style.Border.BorderAround(ExcelBorderStyle.Thick);
                            }

                            liner++;

                            // ew.Cells[liner,2, liner,4].Value = "BALANCES";
                            // ew.Cells[liner,2, liner,4].Merge = true;

                            ew.Cells[liner,5, liner+1,5].Value = "WITH LOAN";
                            ew.Cells[liner,5, liner+1,5].Merge = true;
                            ew.Cells[liner,6, liner+1,6].Value = "PENDING FOR RELOAN";
                            ew.Cells[liner,6, liner+1,6].Merge = true;
                            ew.Cells[liner,7, liner+1,7].Value = "DORMANT CLIENTS W/ CBU";
                            ew.Cells[liner,7, liner+1,7].Merge = true;
                            ew.Cells[liner,8, liner+1,8].Value = "DEFAULT";
                            ew.Cells[liner,8, liner+1,8].Merge = true;

                            ew.Cells[liner,9, liner+1,9].Value = "TOTAL";
                            ew.Cells[liner,9, liner+1,9].Merge = true;

                            ew.Cells[liner,10, liner+1,10].Value = "RETURNING CLIENTS";
                            ew.Cells[liner,10, liner+1,10].Merge = true;

                            ew.Cells[liner,11, liner+1,11].Value = "RESIGNED";
                            ew.Cells[liner,11, liner+1,11].Merge = true;

                            ew.Cells[liner,13, liner+1,13].Value = "DISBURSEMENT FOR THE MONTH";
                            ew.Cells[liner,13, liner+1,13].Merge = true;
                            ew.Cells[liner,14, liner,14].Value = "DAILY/MATURED";
                            ew.Cells[liner,15, liner,17].Value = "CENTER STATUS";
                            ew.Cells[liner,15, liner,17].Merge = true;
                            ew.Cells[liner,18, liner,21].Value = "CLIENT";
                            ew.Cells[liner,18, liner,21].Merge = true;
                            ew.Cells[liner,22, liner,23].Value = "FALL-OUT";
                            ew.Cells[liner,22, liner,23].Merge = true;
                            ew.Cells[liner,22, liner,23].Value = "FALL-OUT";
                            ew.Cells[liner,22, liner,23].Merge = true;
                            ew.Cells[liner,24, liner,26].Value = "RETENTION";
                            ew.Cells[liner,24, liner,26].Merge = true;
                            
                            ew.Row(liner).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            ew.Row(liner).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            ew.Row(liner).Style.WrapText = true;
                            ew.Row(liner).Style.Font.Bold = true;
                            
                            for(Int32 ctr=1;ctr<=26;ctr++) {
                                if(ctr!=12)
                                    ew.Cells[liner,ctr,liner,ctr].Style.Border.BorderAround(ExcelBorderStyle.Thick);
                            }

                            liner++;

                            ew.Cells[liner,1].Value = po.name;
                            ew.Cells[liner,2].Value = "LOANS RECEIVABLE";
                            ew.Cells[liner,3].Value = "INTEREST RECEIVABLE";
                            ew.Cells[liner,4].Value = "CBU";

                            ew.Cells[liner,14].Value = "PAR AMOUNT";
                            ew.Cells[liner,15].Value = "A";
                            ew.Cells[liner,16].Value = "I";
                            ew.Cells[liner,17].Value = "M";
                            ew.Cells[liner,18].Value = "ACTIVE";
                            ew.Cells[liner,19].Value = "INACTIVE";
                            ew.Cells[liner,20].Value = "MATURED";
                            ew.Cells[liner,21].Value = "TOTAL";
                            ew.Cells[liner,22].Value = "RESIGNED";
                            ew.Cells[liner,23].Value = "RATE";
                            ew.Cells[liner,24].Value = "NEW LOAN";
                            ew.Cells[liner,25].Value = "RELOAN";
                            ew.Cells[liner,26].Value = "RATE";

                            ew.Row(liner).Height = 30;
                            ew.Row(liner).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            ew.Row(liner).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            ew.Row(liner).Style.WrapText = true;
                            ew.Row(liner).Style.Font.Bold = true;
                            
                            for(Int32 ctr=1;ctr<=26;ctr++) {
                                if(ctr!=12)
                                    ew.Cells[liner,ctr,liner,ctr].Style.Border.BorderAround(ExcelBorderStyle.Thick);
                            }

                            liner++;

                            po.center.ToList().ForEach(cent=>{
                                ew.Cells[liner,1].Value = cent.name;
                                ew.Cells[liner,2].Value = cent.balances.loanReceivable;
                                ew.Cells[liner,3].Value = cent.balances.interestReceivable;
                                ew.Cells[liner,4].Value = cent.balances.cbu;
                                ew.Cells[liner,5].Value = cent.clientLoanStatuses.withLoan;
                                ew.Cells[liner,6].Value = cent.clientLoanStatuses.pendingForReloan;
                                ew.Cells[liner,7].Value = cent.clientLoanStatuses.dormantClientsWithCbu;
                                ew.Cells[liner,8].Value = cent.clientLoanStatuses.defaultClients;
                                ew.Cells[liner,9].Value = cent.clientLoanStatuses.total;
                                ew.Cells[liner,10].Value = cent.clientLoanStatuses.returningClients;
                                ew.Cells[liner,11].Value = cent.clientLoanStatuses.resigned;
                                ew.Cells[liner,13].Value = cent.disbursementForTheMonth;
                                ew.Cells[liner,14].Value = cent.dailyMatured.parAmount;

                                ew.Cells[liner,15,liner,17].Merge = true;
                                ew.Cells[liner,15,liner,17].Value = cent.centerStatus.status;
                                ew.Cells[liner,15,liner,17].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                ew.Cells[liner,18].Value = cent.clientStatuses.active;
                                ew.Cells[liner,19].Value = cent.clientStatuses.inActive;
                                ew.Cells[liner,20].Value = cent.clientStatuses.matured;
                                ew.Cells[liner,21].Value = cent.clientStatuses.total;
                                ew.Cells[liner,22].Value = cent.fallout.resigned;
                                ew.Cells[liner,23].Value = cent.fallout.rate.ToString() + " %";
                                ew.Cells[liner,24].Value = cent.retention.newLoan;
                                ew.Cells[liner,25].Value = cent.retention.reloan;
                                ew.Cells[liner,26].Value = cent.retention.rate.ToString() + " %";

                                for(Int32 ctr=1;ctr<=26;ctr++) {
                                    if(ctr!=12)
                                        ew.Cells[liner,ctr,liner,ctr].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                                    if((ctr>=2&&ctr<=4)||(ctr>=13&&ctr<=14))
                                        ew.Cells[liner,ctr,liner,ctr].Style.Numberformat.Format = "#,##0.00;(#,##0.00);-";
                                }

                                liner++;
                            });
                            
                            ew.Cells[liner,1].Value = "TOTAL";
                            ew.Cells[liner,1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                            ew.Cells[liner,2].Value = po.totals.balances.loanReceivable;
                            ew.Cells[liner,3].Value = po.totals.balances.interestReceivable;
                            ew.Cells[liner,4].Value = po.totals.balances.cbu;
                            ew.Cells[liner,5].Value = po.totals.clientLoanStatuses.withLoan;
                            ew.Cells[liner,6].Value = po.totals.clientLoanStatuses.pendingForReloan;
                            ew.Cells[liner,7].Value = po.totals.clientLoanStatuses.dormantClientsWithCbu;
                            ew.Cells[liner,8].Value = po.totals.clientLoanStatuses.defaultClients;
                            ew.Cells[liner,9].Value = po.totals.clientLoanStatuses.total;
                            ew.Cells[liner,10].Value = po.totals.clientLoanStatuses.returningClients;
                            ew.Cells[liner,11].Value = po.totals.clientLoanStatuses.resigned;
                            ew.Cells[liner,13].Value = po.totals.disbursementForTheMonth;
                            ew.Cells[liner,14].Value = po.totals.dailyMatured.parAmount;
                            ew.Cells[liner,15].Value = po.totals.centerStatus.active;
                            ew.Cells[liner,16].Value = po.totals.centerStatus.inActive;
                            ew.Cells[liner,17].Value = po.totals.centerStatus.matured;
                            ew.Cells[liner,18].Value = po.totals.clientStatuses.active;
                            ew.Cells[liner,19].Value = po.totals.clientStatuses.inActive;
                            ew.Cells[liner,20].Value = po.totals.clientStatuses.matured;
                            ew.Cells[liner,21].Value = po.totals.clientStatuses.total;
                            ew.Cells[liner,22].Value = po.totals.fallout.resigned;
                            ew.Cells[liner,23].Value = po.totals.fallout.rate;
                            ew.Cells[liner,24].Value = po.totals.retention.newLoan;
                            ew.Cells[liner,25].Value = po.totals.retention.reloan;
                            ew.Cells[liner,26].Value = po.totals.retention.rate;

                            for(Int32 ctr=1;ctr<=26;ctr++) {
                                if(ctr!=12) {
                                    ew.Cells[liner,ctr,liner,ctr].Style.Border.BorderAround(ExcelBorderStyle.Thick);
                                    ew.Cells[liner,ctr,liner,ctr].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                                    ew.Cells[liner,ctr,liner,ctr].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(0, 255, 0));
                                    ew.Cells[liner,ctr,liner,ctr].Style.Font.Bold = true;

                                }

                                if((ctr>=2&&ctr<=4)||(ctr>=13&&ctr<=14))
                                    ew.Cells[liner,ctr,liner,ctr].Style.Numberformat.Format = "#,##0.00;(#,##0.00);-";
                            }

                            

                            liner+=2;
                        });
                        

                        ew.Cells[liner,1].Value = "GRAND TOTAL UNIT " + unit.name.ToUpper().Substring(unit.name.Length-1,1);
                        ew.Cells[liner,1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                        ew.Cells[liner,2].Value = unit.totals.balances.loanReceivable;
                        ew.Cells[liner,3].Value = unit.totals.balances.interestReceivable;
                        ew.Cells[liner,4].Value = unit.totals.balances.cbu;
                        ew.Cells[liner,5].Value = unit.totals.clientLoanStatuses.withLoan;
                        ew.Cells[liner,6].Value = unit.totals.clientLoanStatuses.pendingForReloan;
                        ew.Cells[liner,7].Value = unit.totals.clientLoanStatuses.dormantClientsWithCbu;
                        ew.Cells[liner,8].Value = unit.totals.clientLoanStatuses.defaultClients;
                        ew.Cells[liner,9].Value = unit.totals.clientLoanStatuses.total;
                        ew.Cells[liner,10].Value = unit.totals.clientLoanStatuses.returningClients;
                        ew.Cells[liner,11].Value = unit.totals.clientLoanStatuses.resigned;

                        ew.Cells[liner,13].Value = unit.totals.disbursementForTheMonth;
                        ew.Cells[liner,14].Value = unit.totals.dailyMatured.parAmount;
                        ew.Cells[liner,15].Value = unit.totals.centerStatus.active;
                        ew.Cells[liner,16].Value = unit.totals.centerStatus.inActive;
                        ew.Cells[liner,17].Value = unit.totals.centerStatus.matured;
                        ew.Cells[liner,18].Value = unit.totals.clientStatuses.active;
                        ew.Cells[liner,19].Value = unit.totals.clientStatuses.inActive;
                        ew.Cells[liner,20].Value = unit.totals.clientStatuses.matured;
                        ew.Cells[liner,21].Value = unit.totals.clientStatuses.total;
                        ew.Cells[liner,22].Value = unit.totals.fallout.resigned;
                        ew.Cells[liner,23].Value = unit.totals.fallout.rate;
                        ew.Cells[liner,24].Value = unit.totals.retention.newLoan;
                        ew.Cells[liner,25].Value = unit.totals.retention.reloan;
                        ew.Cells[liner,26].Value = unit.totals.retention.rate;

                        for(Int32 ctr=1;ctr<=26;ctr++) {
                            if(ctr!=12) {
                                ew.Cells[liner,ctr,liner,ctr].Style.Border.BorderAround(ExcelBorderStyle.Thick);
                                ew.Cells[liner,ctr,liner,ctr].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                                ew.Cells[liner,ctr,liner,ctr].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(0, 255, 0));
                                ew.Cells[liner,ctr,liner,ctr].Style.Font.Bold = true;

                            }

                            if((ctr>=2&&ctr<=4)||(ctr>=13&&ctr<=14))
                                ew.Cells[liner,ctr,liner,ctr].Style.Numberformat.Format = "#,##0.00;(#,##0.00);-";
                        }


                        // sigs

                        liner++;
                        liner++;

                        ew.Cells[liner,1].Value = unit.preparedBy;
                        ew.Cells[liner+1,1].Value = unit.preparedByPosition;

                        ew.Cells[liner,3].Value = unit.checkedBy;
                        ew.Cells[liner+1,3].Value = unit.checkedByPosition;

                        ew.Cells[liner,5].Value = unit.notedBy;
                        ew.Cells[liner+1,5].Value = unit.notedByPosition;
                        

                        // formatting

                        ew.Column(1).Width = 27;
                        
                        ew.Column(2).Width = 15;
                        ew.Column(3).Width = 15;
                        ew.Column(4).Width = 15;

                        ew.Column(5).Width = 12;
                        ew.Column(6).Width = 12;
                        ew.Column(7).Width = 12;
                        ew.Column(8).Width = 12;
                        ew.Column(9).Width = 12;
                        ew.Column(10).Width = 12;
                        ew.Column(11).Width = 12;

                        ew.Column(12).Width = 2;

                        ew.Column(13).Width = 17;
                        ew.Column(14).Width = 17;

                        ew.Column(15).Width = 10;
                        ew.Column(16).Width = 10;
                        ew.Column(17).Width = 10;
                        ew.Column(18).Width = 10;
                        ew.Column(19).Width = 10;
                        ew.Column(20).Width = 10;
                        ew.Column(21).Width = 10;
                        ew.Column(22).Width = 10;
                        ew.Column(23).Width = 10;
                        ew.Column(24).Width = 10;
                        ew.Column(25).Width = 10;
                        ew.Column(26).Width = 10;

                    });

                    ExcelWorksheet ewSummary = ep.Workbook.Worksheets.Add("Summary");
                    ewSummary.Cells["A1"].Value = "KABALIKAT PARA SA MAUNLAD NA BUHAY, INC.";
                    ewSummary.Cells["A1"].Style.Font.Bold = true;
                    ewSummary.Cells["A2"].Value = rep.branch.ToUpper() + " BRANCH";
                    ewSummary.Cells["A2"].Style.Font.Bold = true;
                    ewSummary.Cells["A3"].Value = "LOAN AND CBU BALANCES";
                    ewSummary.Cells["A3"].Style.Font.Bold = true;
                    ewSummary.Cells["A4"].Value = "AS OF " + rep.date.ToString("MMMM dd, yyyy");
                    ewSummary.Cells["A4"].Style.Font.Bold = true;

                    var linerSummary = 6;
                    rep.summary.unit.ToList().ForEach(unit=>{

                        ewSummary.Cells[linerSummary,1,linerSummary + 3,1].Value = "UNIT " + unit.name;
                        ewSummary.Cells[linerSummary,1,linerSummary + 3, 1].Merge = true;
                        ewSummary.Cells[linerSummary,2,linerSummary+1 ,7].Value = "BALANCES";
                        ewSummary.Cells[linerSummary,2,linerSummary+1 ,7].Merge = true;
                        ewSummary.Cells[linerSummary,8,linerSummary+1,11].Value = "CENTERS";
                        ewSummary.Cells[linerSummary,8,linerSummary+1,11].Merge = true;
                        ewSummary.Cells[linerSummary,12,linerSummary,20].Value = "CLIENTS";
                        ewSummary.Cells[linerSummary,12,linerSummary,20].Merge = true;

                        ewSummary.Cells[linerSummary,23,linerSummary + 3,23].Value = "UNIT " + unit.name;
                        ewSummary.Cells[linerSummary,23,linerSummary + 3,23].Merge = true;

                        ewSummary.Cells[linerSummary,24,linerSummary,34].Value = "LOAN DISBURSEMENT";
                        ewSummary.Cells[linerSummary,24,linerSummary,34].Merge = true;
                        

                        linerSummary++;

                        // ewSummary.Cells[linerSummary,2,linerSummary ,7].Value = "BALANCES";
                        // ewSummary.Cells[linerSummary,2,linerSummary ,7].Merge = true;
                        ewSummary.Cells[linerSummary,12,linerSummary,13].Value = "ACTIVE";
                        ewSummary.Cells[linerSummary,12,linerSummary,13].Merge = true;
                        ewSummary.Cells[linerSummary,14,linerSummary,15].Value = "INACTIVE";
                        ewSummary.Cells[linerSummary,14,linerSummary,15].Merge = true;

                        ewSummary.Cells[linerSummary,16,linerSummary+2,16].Value = "TOTAL";
                        ewSummary.Cells[linerSummary,16,linerSummary+2,16].Merge = true;

                        ewSummary.Cells[linerSummary,17,linerSummary,18].Value = "FOR THE MONTH";
                        ewSummary.Cells[linerSummary,17,linerSummary,18].Merge = true;
                        
                        ewSummary.Cells[linerSummary,19, linerSummary+2, 19].Value = "RETURNING CLIENTS";
                        ewSummary.Cells[linerSummary,19, linerSummary+2, 19].Merge = true;
                        

                        ewSummary.Cells[linerSummary,20,linerSummary,21].Value = "RESIGNED";
                        ewSummary.Cells[linerSummary,20,linerSummary,21].Merge = true;

                        // ewSummary.Cells[linerSummary,20,linerSummary+2,20].Value = "TOTAL";
                        // ewSummary.Cells[linerSummary,20,linerSummary+2,20].Merge = true;

                        ewSummary.Cells[linerSummary,24,linerSummary+1,24].Value = "NEW LOAN";
                        ewSummary.Cells[linerSummary,24,linerSummary+1,24].Merge = true;
                        ewSummary.Cells[linerSummary,25,linerSummary+1,25].Value = "RELOAN NOT IN LPR";
                        ewSummary.Cells[linerSummary,25,linerSummary+1,25].Merge = true;
                        ewSummary.Cells[linerSummary,26,linerSummary+1,26].Value = "RELOAN LAST PAYMENT RELEASE";
                        ewSummary.Cells[linerSummary,26,linerSummary+1,26].Merge = true;
                        ewSummary.Cells[linerSummary,27,linerSummary+1,28].Value = "TOTAL CENTRE DISBURSED";
                        ewSummary.Cells[linerSummary,27,linerSummary+1,28].Merge = true;
                        ewSummary.Cells[linerSummary,29,linerSummary+1,30].Value = "CENTER FOR THE MONTH";
                        ewSummary.Cells[linerSummary,29,linerSummary+1,30].Merge = true;
                        ewSummary.Cells[linerSummary,31,linerSummary+1,34].Value = "AMOUNT";
                        ewSummary.Cells[linerSummary,31,linerSummary+1,34].Merge = true;

                        linerSummary++;

                        ewSummary.Cells[linerSummary,2,linerSummary ,4].Value = "LOAN";
                        ewSummary.Cells[linerSummary,2,linerSummary ,4].Merge = true;
                        ewSummary.Cells[linerSummary,5,linerSummary ,6].Value = "PAR";
                        ewSummary.Cells[linerSummary,5,linerSummary ,6].Merge = true;
                        ewSummary.Cells[linerSummary,7,linerSummary+1,7].Value = "CBU";
                        ewSummary.Cells[linerSummary,7,linerSummary+1,7].Merge = true;
                        ewSummary.Cells[linerSummary,8,linerSummary+1,8].Value = "ACTIVE";
                        ewSummary.Cells[linerSummary,8,linerSummary+1,8].Merge = true;
                        ewSummary.Cells[linerSummary,9,linerSummary+1,9].Value = "INACTIVE";
                        ewSummary.Cells[linerSummary,9,linerSummary+1,9].Merge = true;
                        ewSummary.Cells[linerSummary,10,linerSummary+1,10].Value = "DISSOLVED";
                        ewSummary.Cells[linerSummary,10,linerSummary+1,10].Merge = true;
                        ewSummary.Cells[linerSummary,11,linerSummary+1,11].Value = "TOTAL";
                        ewSummary.Cells[linerSummary,11,linerSummary+1,11].Merge = true;
                        ewSummary.Cells[linerSummary,12,linerSummary+1,12].Value = "WITH LOAN";
                        ewSummary.Cells[linerSummary,12,linerSummary+1,12].Merge = true;
                        ewSummary.Cells[linerSummary,13,linerSummary+1,13].Value = "PENDING FOR RELOAN";
                        ewSummary.Cells[linerSummary,13,linerSummary+1,13].Merge = true;
                        ewSummary.Cells[linerSummary,14,linerSummary+1,14].Value = "DORMANT CLIENTS";
                        ewSummary.Cells[linerSummary,14,linerSummary+1,14].Merge = true;
                        ewSummary.Cells[linerSummary,15,linerSummary+1,15].Value = "DEFAULT";
                        ewSummary.Cells[linerSummary,15,linerSummary+1,15].Merge = true;

                        ewSummary.Cells[linerSummary,17,linerSummary+1,17].Value = "NEW LOAN";
                        ewSummary.Cells[linerSummary,17,linerSummary+1,17].Merge = true;
                        ewSummary.Cells[linerSummary,18,linerSummary+1,18].Value = "RELOAN";
                        ewSummary.Cells[linerSummary,18,linerSummary+1,18].Merge = true;
                        ewSummary.Cells[linerSummary,20,linerSummary+1,20].Value = "FOR THE MONTH";
                        ewSummary.Cells[linerSummary,20,linerSummary+1,20].Merge = true;
                        ewSummary.Cells[linerSummary,21,linerSummary+1,21].Value = "CUMULATIVE";
                        ewSummary.Cells[linerSummary,21,linerSummary+1,21].Merge = true;

                        linerSummary++;

                        ewSummary.Cells[linerSummary,2].Value = "PRINCIPAL";
                        ewSummary.Cells[linerSummary,3].Value = "INTEREST";
                        ewSummary.Cells[linerSummary,4].Value = "TOTAL LOAN";
                        ewSummary.Cells[linerSummary,5].Value = "AMOUNT";
                        ewSummary.Cells[linerSummary,6].Value = "RATE%";

                        ewSummary.Cells[linerSummary,24, linerSummary, 27].Value = "CUMULATIVE";
                        ewSummary.Cells[linerSummary,24, linerSummary, 27].Merge = true;
                        ewSummary.Cells[linerSummary,28].Value = "FOR THE MONTH";
                        ewSummary.Cells[linerSummary,29].Value = "NEW LOAN";
                        ewSummary.Cells[linerSummary,30].Value = "RELOAN";
                        ewSummary.Cells[linerSummary,31].Value = "NEW LOAN";
                        ewSummary.Cells[linerSummary,32].Value = "RELOAN";
                        ewSummary.Cells[linerSummary,33].Value = "CUMULATIVE";
                        ewSummary.Cells[linerSummary,34].Value = "FOR THE MONTH";

                        linerSummary++;

                        for(Int32 ctrRow=linerSummary-4;ctrRow<=linerSummary-1;ctrRow++) {
                            ewSummary.Row(ctrRow).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            ewSummary.Row(ctrRow).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            ewSummary.Row(ctrRow).Style.WrapText = true;
                            ewSummary.Row(ctrRow).Style.Font.Bold = true;
                                
                            for(Int32 ctr=1;ctr<=34;ctr++) {
                                if(ctr!=22) {
                                    ewSummary.Cells[ctrRow,ctr].Style.Border.BorderAround(ExcelBorderStyle.Thick);
                                    ewSummary.Cells[ctrRow,ctr].Style.Font.Bold = true;
                                }
                            }

                        }

                        unit.po.ToList().ForEach(po=>{
                            ewSummary.Cells[linerSummary,1].Value = po.name;
                            ewSummary.Cells[linerSummary,2].Value = po.balances.loan.lrPrincipal;
                            ewSummary.Cells[linerSummary,3].Value = po.balances.loan.eir;
                            ewSummary.Cells[linerSummary,4].Value = po.balances.loan.total;
                            ewSummary.Cells[linerSummary,5].Value = po.balances.par.amount;
                            ewSummary.Cells[linerSummary,6].Value = Math.Round(po.balances.par.rate,2).ToString() + " %";
                            ewSummary.Cells[linerSummary,7].Value = po.balances.cbu;
                            ewSummary.Cells[linerSummary,8].Value = po.centers.active;
                            ewSummary.Cells[linerSummary,9].Value = po.centers.inActive;
                            ewSummary.Cells[linerSummary,10].Value = po.centers.dissolved;
                            ewSummary.Cells[linerSummary,11].Value = po.centers.total;
                            ewSummary.Cells[linerSummary,12].Value = po.clients.active.withLoan;
                            ewSummary.Cells[linerSummary,13].Value = po.clients.active.pendingForReloan;
                            ewSummary.Cells[linerSummary,14].Value = po.clients.inActive.dormantClients;
                            ewSummary.Cells[linerSummary,15].Value = po.clients.inActive.defaultClients;
                            ewSummary.Cells[linerSummary,16].Value = po.clients.total;
                            ewSummary.Cells[linerSummary,17].Value = po.clients.forTheMonth.newLoan;
                            ewSummary.Cells[linerSummary,18].Value = po.clients.forTheMonth.reloan;
                            ewSummary.Cells[linerSummary,19].Value = po.clients.returningClients;
                            ewSummary.Cells[linerSummary,20].Value = po.clients.resigned.forTheMonth;
                            ewSummary.Cells[linerSummary,21].Value = po.clients.resigned.cumulative;

                            ewSummary.Cells[linerSummary,23].Value = po.name;
                            ewSummary.Cells[linerSummary,24].Value = po.loanDisbursement.newLoan;
                            ewSummary.Cells[linerSummary,25].Value = po.loanDisbursement.reloanNotInLPR;
                            ewSummary.Cells[linerSummary,26].Value = po.loanDisbursement.reloanLastPaymentRelease;
                            ewSummary.Cells[linerSummary,27].Value = po.loanDisbursement.totalCenterDisbursed.cumulative;
                            ewSummary.Cells[linerSummary,28].Value = po.loanDisbursement.totalCenterDisbursed.forTheMonth;
                            ewSummary.Cells[linerSummary,29].Value = po.loanDisbursement.centerForTheMonth.newLoan;
                            ewSummary.Cells[linerSummary,30].Value = po.loanDisbursement.centerForTheMonth.reloan;
                            ewSummary.Cells[linerSummary,31].Value = po.loanDisbursement.amount.newLoan;
                            ewSummary.Cells[linerSummary,32].Value = po.loanDisbursement.amount.reloan;
                            ewSummary.Cells[linerSummary,33].Value = po.loanDisbursement.amount.cumulative;
                            ewSummary.Cells[linerSummary,34].Value = po.loanDisbursement.amount.forTheMonth;

                            for(Int32 ctr=1;ctr<=34;ctr++) {
                                if(ctr!=22)
                                    ewSummary.Cells[linerSummary,ctr,linerSummary,ctr].Style.Border.BorderAround(ExcelBorderStyle.Thin);

                                if((ctr>=2&&ctr<=5)||(ctr==7)||(ctr>=31&&ctr<=34))
                                    ewSummary.Cells[linerSummary,ctr,linerSummary,ctr].Style.Numberformat.Format = "#,##0.00;(#,##0.00);-";
                            }

                            linerSummary++;
                        });

                        ewSummary.Cells[linerSummary,1].Value = "TOTAL";
                        ewSummary.Cells[linerSummary,2].Value = unit.total.balances.loan.lrPrincipal;
                        ewSummary.Cells[linerSummary,3].Value = unit.total.balances.loan.eir;
                        ewSummary.Cells[linerSummary,4].Value = unit.total.balances.loan.total;
                        ewSummary.Cells[linerSummary,5].Value = unit.total.balances.par.amount;
                        ewSummary.Cells[linerSummary,6].Value = Math.Round(unit.total.balances.par.rate,2).ToString() + " %";
                        ewSummary.Cells[linerSummary,7].Value = unit.total.balances.cbu;
                        ewSummary.Cells[linerSummary,8].Value = unit.total.centers.active;
                        ewSummary.Cells[linerSummary,9].Value = unit.total.centers.inActive;
                        ewSummary.Cells[linerSummary,10].Value = unit.total.centers.matured;
                        ewSummary.Cells[linerSummary,11].Value = unit.total.centers.total;
                        
                        ewSummary.Cells[linerSummary,12].Value = unit.total.clients.active.withLoan;
                        ewSummary.Cells[linerSummary,13].Value = unit.total.clients.active.pendingForReloan;
                        ewSummary.Cells[linerSummary,14].Value = unit.total.clients.inActive.dormantClients;
                        ewSummary.Cells[linerSummary,15].Value = unit.total.clients.inActive.defaultClients;
                        ewSummary.Cells[linerSummary,16].Value = unit.total.clients.total;
                        ewSummary.Cells[linerSummary,17].Value = unit.total.clients.forTheMonth.newLoan;
                        ewSummary.Cells[linerSummary,18].Value = unit.total.clients.forTheMonth.reloan;
                        ewSummary.Cells[linerSummary,19].Value = unit.total.clients.returningClients;
                        ewSummary.Cells[linerSummary,20].Value = unit.total.clients.resigned.forTheMonth;
                        ewSummary.Cells[linerSummary,21].Value = unit.total.clients.resigned.cumulative;

                        ewSummary.Cells[linerSummary,23].Value = "TOTAL";
                        ewSummary.Cells[linerSummary,24].Value = unit.total.loanDisbursement.newLoan;
                        ewSummary.Cells[linerSummary,25].Value = unit.total.loanDisbursement.reloanNotInLPR;
                        ewSummary.Cells[linerSummary,26].Value = unit.total.loanDisbursement.reloanLastPaymentRelease;
                        ewSummary.Cells[linerSummary,27].Value = unit.total.loanDisbursement.totalCenterDisbursed.cumulative;
                        ewSummary.Cells[linerSummary,28].Value = unit.total.loanDisbursement.totalCenterDisbursed.forTheMonth;
                        ewSummary.Cells[linerSummary,29].Value = unit.total.loanDisbursement.centerForTheMonth.newLoan;
                        ewSummary.Cells[linerSummary,30].Value = unit.total.loanDisbursement.centerForTheMonth.reloan;
                        ewSummary.Cells[linerSummary,31].Value = unit.total.loanDisbursement.amount.newLoan;
                        ewSummary.Cells[linerSummary,32].Value = unit.total.loanDisbursement.amount.reloan;
                        ewSummary.Cells[linerSummary,33].Value = unit.total.loanDisbursement.amount.cumulative;
                        ewSummary.Cells[linerSummary,34].Value = unit.total.loanDisbursement.amount.forTheMonth;

                        for(Int32 ctr=1;ctr<=34;ctr++) {
                    
                            if(ctr!=22) {
                                ewSummary.Cells[linerSummary,ctr,linerSummary,ctr].Style.Border.BorderAround(ExcelBorderStyle.Thick);
                                ewSummary.Cells[linerSummary,ctr,linerSummary,ctr].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                                ewSummary.Cells[linerSummary,ctr,linerSummary,ctr].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(0, 255, 0));
                                ewSummary.Cells[linerSummary,ctr,linerSummary,ctr].Style.Font.Bold = true;
                            }

                            if((ctr>=2&&ctr<=5)||(ctr==7)||(ctr>=31&&ctr<=34))
                                ewSummary.Cells[linerSummary,ctr,linerSummary,ctr].Style.Numberformat.Format = "#,##0.00;(#,##0.00);-";
                        }
                        
                        linerSummary+=2;
                        
                    });

                    linerSummary++;

                    ewSummary.Cells[linerSummary,1].Value = "GRAND TOTAL";
                    ewSummary.Cells[linerSummary,2].Value = rep.summary.total.balances.loan.lrPrincipal;
                    ewSummary.Cells[linerSummary,3].Value = rep.summary.total.balances.loan.eir;
                    ewSummary.Cells[linerSummary,4].Value = rep.summary.total.balances.loan.total;
                    ewSummary.Cells[linerSummary,5].Value = rep.summary.total.balances.par.amount;
                    ewSummary.Cells[linerSummary,6].Value = Math.Round(rep.summary.total.balances.par.rate,2).ToString() + " %";
                    ewSummary.Cells[linerSummary,7].Value = rep.summary.total.balances.cbu;
                    ewSummary.Cells[linerSummary,8].Value = rep.summary.total.centers.active;
                    ewSummary.Cells[linerSummary,9].Value = rep.summary.total.centers.inActive;
                    ewSummary.Cells[linerSummary,10].Value = rep.summary.total.centers.matured;
                    ewSummary.Cells[linerSummary,11].Value = rep.summary.total.centers.total;
                    ewSummary.Cells[linerSummary,12].Value = rep.summary.total.clients.active.withLoan;
                    ewSummary.Cells[linerSummary,13].Value = rep.summary.total.clients.active.pendingForReloan;
                    ewSummary.Cells[linerSummary,14].Value = rep.summary.total.clients.inActive.dormantClients;
                    ewSummary.Cells[linerSummary,15].Value = rep.summary.total.clients.inActive.defaultClients;
                    ewSummary.Cells[linerSummary,16].Value = rep.summary.total.clients.total;
                    ewSummary.Cells[linerSummary,17].Value = rep.summary.total.clients.forTheMonth.newLoan;
                    ewSummary.Cells[linerSummary,18].Value = rep.summary.total.clients.forTheMonth.reloan;
                    ewSummary.Cells[linerSummary,19].Value = rep.summary.total.clients.returningClients;
                    ewSummary.Cells[linerSummary,20].Value = rep.summary.total.clients.resigned.forTheMonth;
                    ewSummary.Cells[linerSummary,21].Value = rep.summary.total.clients.resigned.cumulative;

                    ewSummary.Cells[linerSummary,23].Value = "GRAND TOTAL";
                    ewSummary.Cells[linerSummary,24].Value = rep.summary.total.loanDisbursement.newLoan;
                    ewSummary.Cells[linerSummary,25].Value = rep.summary.total.loanDisbursement.reloanNotInLPR;
                    ewSummary.Cells[linerSummary,26].Value = rep.summary.total.loanDisbursement.reloanLastPaymentRelease;
                    ewSummary.Cells[linerSummary,27].Value = rep.summary.total.loanDisbursement.totalCenterDisbursed.cumulative;
                    ewSummary.Cells[linerSummary,28].Value = rep.summary.total.loanDisbursement.totalCenterDisbursed.forTheMonth;
                    ewSummary.Cells[linerSummary,29].Value = rep.summary.total.loanDisbursement.centerForTheMonth.newLoan;
                    ewSummary.Cells[linerSummary,30].Value = rep.summary.total.loanDisbursement.centerForTheMonth.reloan;
                    ewSummary.Cells[linerSummary,31].Value = rep.summary.total.loanDisbursement.amount.newLoan;
                    ewSummary.Cells[linerSummary,32].Value = rep.summary.total.loanDisbursement.amount.reloan;
                    ewSummary.Cells[linerSummary,33].Value = rep.summary.total.loanDisbursement.amount.cumulative;
                    ewSummary.Cells[linerSummary,34].Value = rep.summary.total.loanDisbursement.amount.forTheMonth;

                    for(Int32 ctr=1;ctr<=34;ctr++) {
                    
                        if(ctr!=22) {
                            ewSummary.Cells[linerSummary,ctr,linerSummary,ctr].Style.Border.BorderAround(ExcelBorderStyle.Thick);
                            ewSummary.Cells[linerSummary,ctr,linerSummary,ctr].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            ewSummary.Cells[linerSummary,ctr,linerSummary,ctr].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(0, 255, 0));
                            ewSummary.Cells[linerSummary,ctr,linerSummary,ctr].Style.Font.Bold = true;
                        }

                        if((ctr>=2&&ctr<=5)||(ctr==7)||(ctr>=31&&ctr<=34))
                            ewSummary.Cells[linerSummary,ctr,linerSummary,ctr].Style.Numberformat.Format = "#,##0.00;(#,##0.00);-";
                    }

                    linerSummary++;


                    ewSummary.Column(1).Width = 25;
                    ewSummary.Column(2).Width = 17;
                    ewSummary.Column(3).Width = 17;
                    ewSummary.Column(4).Width = 17;
                    ewSummary.Column(5).Width = 17;
                    ewSummary.Column(6).Width = 17;
                    ewSummary.Column(7).Width = 17;
                    
                    for(Int32 ctr=8;ctr<=34;ctr++) {
                        ewSummary.Column(ctr).Width = 15;
                    }

                    ewSummary.Column(22).Width = 3;
                    ewSummary.Column(23).Width = 25;

                    for(Int32 ctr=24;ctr<=34;ctr++) {
                        ewSummary.Column(ctr).Width = 15;
                    }


                    // signatures


                    linerSummary++;
                    linerSummary++;

                    ewSummary.Cells[linerSummary,1].Value = rep.summary.preparedBy;
                    ewSummary.Cells[linerSummary+1,1].Value = rep.summary.preparedByPosition;

                    ewSummary.Cells[linerSummary,3].Value = rep.summary.checkedBy;
                    ewSummary.Cells[linerSummary+1,3].Value = rep.summary.checkedByPosition;

                    ewSummary.Cells[linerSummary,5].Value = rep.summary.approvedBy;
                    ewSummary.Cells[linerSummary+1,5].Value = rep.summary.approvedByPosition;

                    byte[] bytes = ep.GetAsByteArray();
                    ep.Dispose();
                    var link = _upload.upload(bytes,"kmbidevreporeports/LRCBU-" + rep.date.ToString("MM-dd-yyyy") + ".xlsx");
                    return Redirect(link);
                }

            } else {
                return Ok(new {
                    type = "failed",
                    
                });
            }
        }
    
        [HttpGet("orPrinting")]
        public IActionResult orPrinting([FromQuery]string orNo) {
            var result = _reporting.orPrinting(orNo);
            if(result!=null)
                return Ok(result);
            else 
                return Ok("Or Number " + orNo + " not found");
        }
    
        [HttpGet("mafPrinting")]
        public IActionResult mafPrinting([FromQuery]String clientSlug, [FromQuery] bool dataPrivacy, [FromQuery] bool pn) {
            var clientBySlug = _clientRepository.getSlug(clientSlug);
            var clientById = _clientRepository.Get(clientBySlug.Id);
            var result = _reporting.mafPrinting(clientById);
            if (result != null)
            {
                //return Ok(result);
                var data = JsonConvert.SerializeObject(result);
                var repDesc = "Member Application Form";
                if (dataPrivacy == true && pn == true)
                {
                    return generateReport("maf", data, repDesc);
                }
                else if (dataPrivacy == true && pn == false)
                {
                    return generateReport("maf_dp", data, repDesc);
                }
                else if (dataPrivacy == false && pn == true)
                {
                    return generateReport("maf_pn", data, repDesc);
                }
                else
                {
                    return generateReport("maf_only", data, repDesc);
                }

            }
            else
            {
                return Ok(new
                {
                    type = "error",
                    message = "Client not found"
                });
            }
        }

        [HttpGet("lrcbuschedule")]
        public IActionResult lrCbuSchedule(DateTime asOf) {
            GenerateLrCbuScheduleResponse response = _generateLrCbuSchedule.Handle(new GenerateLrCbuScheduleRequest {
                AsOf = asOf,
                branchSlug = null
            });
            var result = _presentGenerateLrCbuSchedule.Present(response);
            
            
            var link = _upload.upload(result.bytes,"kmbidevreporeports/lrcbu-schedule-" + asOf.ToString("MM-dd-yyyy") + ".xlsx");
            return Redirect(link);
        }

        [HttpGet("resignedsummary")]
        public IActionResult resignedsummary(DateTime asOf, String branchSlug) {
            GenerateResignedSummaryResponse response = _generateResignedSummary.Handle(new GenerateResignedSummaryRequest {
                AsOf = asOf,
                BranchSlug = branchSlug
            });

            var result = _presentGenerateResignedSummary.Present(response);
            
            
            var link = _upload.upload(result.bytes,"kmbidevreporeports/resigned-clients-summary-" + asOf.ToString("MM-dd-yyyy") + ".xlsx");
            return Redirect(link);
        }

        [HttpGet("ppiData")]
        public IActionResult ppiData() {
            var now = DateTime.Now.Date;
            GeneratePPIDataResponse response = _generatePPIData.Handle(new GeneratePPIDataRequest {
                AsOf = now
            });

            GeneratePPIDataPresenterResponse presentResponse = _presentGeneratePPIData.Present(response);

            var link = _upload.upload(presentResponse.bytes,"kmbidevreporeports/ppi-data-" + now.ToString("MM-dd-yyyy") + ".xlsx");
            return Redirect(link);
        }

        [HttpGet("ppiDatav2")]
        public IActionResult ppiDatav2() {
            var now = DateTime.Now.Date;
            GeneratePPIDataResponse response = _generatePPIData.Handle(new GeneratePPIDataRequest {
                AsOf = now,
                clients = _clientRepository.allClients().ToArray()
            });

            GeneratePPIDatav2PresenterResponse presentResponse = _presentGeneratePPIDatav2.Present(response);

            var link = _upload.upload(presentResponse.bytes,"kmbidevreporeports/ppi-data-" + now.ToString("MM-dd-yyyy") + ".xlsx");
            return Redirect(link);
        }

        [HttpGet("extractAddress")]
        public IActionResult exportAddress() {
            var rep = _reporting.exportAddress();
            using (OfficeOpenXml.ExcelPackage ep = new OfficeOpenXml.ExcelPackage()) {
                ExcelWorksheet ew = ep.Workbook.Worksheets.Add("Sample");

                ew.Cells["A1"].Value = "RegionCode";
                ew.Cells["B1"].Value = "Region";
                ew.Cells["C1"].Value = "ProvinceCode";
                ew.Cells["D1"].Value = "Province";
                ew.Cells["E1"].Value = "MunicipalityCode";
                ew.Cells["F1"].Value = "Municipality";
                ew.Cells["G1"].Value = "BarangayCode";
                ew.Cells["H1"].Value = "Barangay";

                var ctr = 2;
                rep.items.ToList().ForEach(item=>{
                    ew.Cells[ctr,1].Value = item.regionSlug;
                    ew.Cells[ctr,2].Value = item.regionDesc;
                    ew.Cells[ctr,3].Value = item.provinceSlug;
                    ew.Cells[ctr,4].Value = item.provinceDesc;
                    ew.Cells[ctr,5].Value = item.municipalitySlug;
                    ew.Cells[ctr,6].Value = item.municipalityDesc;
                    ew.Cells[ctr,7].Value = item.barangaySlug;
                    ew.Cells[ctr,8].Value = item.barangayDesc;
                    ctr++;
                });

                byte[] bytes = ep.GetAsByteArray();
                ep.Dispose();

                var link = _upload.upload(bytes,"kmbidevreporeports/dunamis-address.xlsx");
                return Redirect(link);
            }
        }

        [HttpGet("tinissuance")]
        public IActionResult TinIssuance([FromQuery]DateTime? from, [FromQuery]DateTime? to) {
            var request = new GenerateTINIssuanceReportRequest();
            request.from = from;
            request.to = to;

            GenerateTINIssuanceReportResponse response = _generateTinIssuanceReport.Handle(request);

            GenerateTINIssuanceReportPresenterResponse presentTinIssuanceReport = _presentTinIssuanceReport.Present(response);

            var _from = "";
            var _to = "";
            if(from!=null)
                _from = Convert.ToDateTime(from).ToString("MM-dd-yyyy");

            if(to!=null)
                _to = Convert.ToDateTime(to).ToString("MM-dd-yyyy");


            var link = _upload.upload(presentTinIssuanceReport.bytes, String.Format("kmbidevreporeports/tin-issuance-{0}-{1}.xlsx",_from,_to));
            return Redirect(link);
        }

        [HttpGet("generatedtrforpayroll")]
        public IActionResult generateDtrForPayroll([FromQuery]DateTime from, [FromQuery]DateTime to) {
            var request = new GenerateDtrForPayrollUseCaseRequest {
                From = from.Date, To = to.Date
            };

            var response = _generateDtrForPayroll.Handle(request);
            
            using (OfficeOpenXml.ExcelPackage ep = new OfficeOpenXml.ExcelPackage()) {
                byte[] bytes;
                
                ExcelWorksheet ew = ep.Workbook.Worksheets.Add("dtr-" + from.Date.ToString("MM-dd-yy") + "-" + to.Date.ToString("MM-dd-yy"));
                
                ew.Cells[1,1].Value = "Employee Code";
                ew.Cells[1,2].Value = "Employee Name";
                ew.Cells[1,3].Value = "RegDays";
                ew.Cells[1,4].Value = "RegHrs";
                ew.Cells[1,5].Value = "RegMins";
                ew.Cells[1,6].Value = "ABDays";
                ew.Cells[1,7].Value = "ABHrs";
                ew.Cells[1,8].Value = "LTHrs";
                ew.Cells[1,9].Value = "LTMins";
                ew.Cells[1,10].Value = "UTHrs";
                ew.Cells[1,11].Value = "UTMins";
                
                var ctr = 2;
                foreach(var item in response.items) {
                    ew.Cells[ctr,1].Value = item.EmployeeCode;
                    ew.Cells[ctr,2].Value = item.EmployeeName;
                    ew.Cells[ctr,3].Value = item.RegDays;
                    ew.Cells[ctr,4].Value = item.RegHrs;
                    ew.Cells[ctr,5].Value = item.RegMins;
                    ew.Cells[ctr,6].Value = item.ABDays;
                    ew.Cells[ctr,7].Value = item.ABHrs;
                    ew.Cells[ctr,8].Value = item.LTHrs;
                    ew.Cells[ctr,9].Value = item.LTMins;
                    ew.Cells[ctr,10].Value = item.UTHrs;
                    ew.Cells[ctr,11].Value = item.UTMins;
                    ctr++;
                }

                ew.Row(1).Style.Font.Bold = true;
                ew.Column(1).Width = 16;
                ew.Column(2).Width = 46;


                for(int _ctr=3;_ctr<=11;_ctr++) {
                    ew.Column(_ctr).Width = 11;
                }
                

                //details
                ExcelWorksheet ewDet = ep.Workbook.Worksheets.Add("details");

                ewDet.Cells[1,1].Value = "Employee Code";
                ewDet.Cells[1,2].Value = "Employee Name";
                ewDet.Cells[1,3].Value = "RegDays";
                ewDet.Cells[1,4].Value = "RegHrs";
                ewDet.Cells[1,5].Value = "RegMins";
                ewDet.Cells[1,6].Value = "ABDays";
                ewDet.Cells[1,7].Value = "ABHrs";
                ewDet.Cells[1,8].Value = "LTHrs";
                ewDet.Cells[1,9].Value = "LTMins";
                ewDet.Cells[1,10].Value = "UTHrs";
                ewDet.Cells[1,11].Value = "UTMins";
                ctr = 2;
                foreach(var item in response.itemDets) {
                    ewDet.Cells[ctr,1].Value = item.EmployeeCode;
                    ewDet.Cells[ctr,2].Value = item.EmployeeName;
                    ewDet.Cells[ctr,3].Value = item.RegDays;
                    ewDet.Cells[ctr,4].Value = item.RegHrs;
                    ewDet.Cells[ctr,5].Value = item.RegMins;
                    ewDet.Cells[ctr,6].Value = item.ABDays;
                    ewDet.Cells[ctr,7].Value = item.ABHrs;
                    ewDet.Cells[ctr,8].Value = item.LTHrs;
                    ewDet.Cells[ctr,9].Value = item.LTMins;
                    ewDet.Cells[ctr,10].Value = item.UTHrs;
                    ewDet.Cells[ctr,11].Value = item.UTMins;
                    ewDet.Cells[ctr,12].Value = item.Date;
                    ctr++;
                }


                bytes = ep.GetAsByteArray();
                var link = _upload.upload(bytes,"kmbidevreporeports/dtr-report-for-payroll.xlsx");
                ep.Dispose();
                return Redirect(link);
            }
        }

        [HttpGet("generateleavesforpayroll")]
        public IActionResult generateLeavesForPayroll([FromQuery]DateTime from, [FromQuery]DateTime to) {
            var request = new GenerateLeavesForPayrollUseCaseRequest {
                From = from, To = to
            };

            var response = _generateLeavesForPayroll.Handle(request);
            
            using (OfficeOpenXml.ExcelPackage ep = new OfficeOpenXml.ExcelPackage()) {
                byte[] bytes;
                
                ExcelWorksheet ew = ep.Workbook.Worksheets.Add("leaves-" + from.Date.ToString("MM-dd-yy") + "-" + to.Date.ToString("MM-dd-yy"));
                
                ew.Cells[1,1].Value = "Employee Code";
                ew.Cells[1,2].Value = "Employee Name";
                ew.Cells[1,3].Value = "SL (Days)";
                ew.Cells[1,4].Value = "VL (Days)";
                
                var ctr = 2;
                foreach(var item in response.items) {
                    ew.Cells[ctr,1].Value = item.EmployeeCode;
                    ew.Cells[ctr,2].Value = item.EmployeeName;
                    ew.Cells[ctr,3].Value = item.TotalSL;
                    ew.Cells[ctr,4].Value = item.TotalVL;
                    ctr++;
                }

                ew.Row(1).Style.Font.Bold = true;
                ew.Column(1).Width = 16;
                ew.Column(2).Width = 46;
                ew.Column(3).Width = 11;
                ew.Column(4).Width = 11;
                
                ExcelWorksheet detew = ep.Workbook.Worksheets.Add("details");

                detew.Cells[1,1].Value = "Employee Code";
                detew.Cells[1,2].Value = "Employee Name";
                detew.Cells[1,3].Value = "SL (Days)";
                detew.Cells[1,4].Value = "VL (Days)";
                detew.Cells[1,5].Value = "Date";

                ctr = 2;
                foreach(var item in response.dets) {
                    detew.Cells[ctr,1].Value = item.EmployeeCode;
                    detew.Cells[ctr,2].Value = item.EmployeeName;
                    detew.Cells[ctr,3].Value = item.TotalSL;
                    detew.Cells[ctr,4].Value = item.TotalVL;
                    detew.Cells[ctr,5].Value = item.Date;
                    ctr++;
                }

                bytes = ep.GetAsByteArray();
                var link = _upload.upload(bytes,"kmbidevreporeports/leaves-report-for-payroll.xlsx");
                ep.Dispose();
                return Redirect(link);
            }
        }

        [HttpGet("generateotforpayroll")]
        public IActionResult generateOtForPayroll([FromQuery]DateTime from, [FromQuery]DateTime to) {
            var request = new GenerateOtForPayrollUseCaseRequest {
                from = from, to = to
            };

            var response = _generateOtForPayroll.Handle(request);
            
            using (OfficeOpenXml.ExcelPackage ep = new OfficeOpenXml.ExcelPackage()) {
                byte[] bytes;
                
                ExcelWorksheet ew = ep.Workbook.Worksheets.Add("ot-" + from.Date.ToString("MM-dd-yy") + "-" + to.Date.ToString("MM-dd-yy"));
                
                ew.Cells[1,1].Value = "Employee Code";
                ew.Cells[1,2].Value = "Employee Name";
                ew.Cells[1,3].Value = "LEG";
                ew.Cells[1,4].Value = "LEGAL";
                ew.Cells[1,5].Value = "REGND";
                ew.Cells[1,6].Value = "REGNDOT";
                ew.Cells[1,7].Value = "REGOT";
                ew.Cells[1,8].Value = "RST";
                ew.Cells[1,9].Value = "RSTLEG";
                ew.Cells[1,10].Value = "RSTLEGND";
                ew.Cells[1,11].Value = "RSTLEGNDOT";
                ew.Cells[1,12].Value = "RSTLEGOT";
                ew.Cells[1,13].Value = "RSTND";
                ew.Cells[1,14].Value = "RSTNDOT";
                ew.Cells[1,15].Value = "RSTOT";
                ew.Cells[1,16].Value = "RSTSPE";
                ew.Cells[1,17].Value = "RSTSPEND";
                ew.Cells[1,18].Value = "RSTSPENDOT";
                ew.Cells[1,19].Value = "RSTSPEOT";
                ew.Cells[1,20].Value = "SPE";
                ew.Cells[1,21].Value = "SPEND";
                ew.Cells[1,22].Value = "SPENDOT";
                ew.Cells[1,23].Value = "SPEOT";
                ew.Cells[1,24].Value = "SPECIAL";
                ew.Cells[1,25].Value = "SPECIALND";
                ew.Cells[1,26].Value = "SPECIALNDOT";
                ew.Cells[1,27].Value = "SPECIALOT";
                ew.Cells[1,28].Value = "WLEGND";
                ew.Cells[1,29].Value = "WLEGNDOT";
                ew.Cells[1,30].Value = "WLEGOT";
                ew.Cells[1,31].Value = "WRKLEG";
                var ctr = 2;
                foreach(var item in response.items) {
                    ew.Cells[ctr,1].Value = item.EmployeeCode;
                    ew.Cells[ctr,2].Value = item.EmployeeName;
                    ew.Cells[ctr,3].Value = item.LEG;
                    ew.Cells[ctr,4].Value = item.LEGAL;
                    ew.Cells[ctr,5].Value = item.REGND;
                    ew.Cells[ctr,6].Value = item.REGNDOT;
                    ew.Cells[ctr,7].Value = item.REGOT;
                    ew.Cells[ctr,8].Value = item.RST;
                    ew.Cells[ctr,9].Value = item.RSTLEG;
                    ew.Cells[ctr,10].Value = item.RSTLEGND;
                    ew.Cells[ctr,11].Value = item.RSTLEGNDOT;
                    ew.Cells[ctr,12].Value = item.RSTLEGOT;
                    ew.Cells[ctr,13].Value = item.RSTND;
                    ew.Cells[ctr,14].Value = item.RSTNDOT;
                    ew.Cells[ctr,15].Value = item.RSTOT;
                    ew.Cells[ctr,16].Value = item.RSTSPE;
                    ew.Cells[ctr,17].Value = item.RSTSPEND;
                    ew.Cells[ctr,18].Value = item.RSTSPENDOT;
                    ew.Cells[ctr,19].Value = item.RSTSPEOT;
                    ew.Cells[ctr,20].Value = item.SPE;
                    ew.Cells[ctr,21].Value = item.SPEND;
                    ew.Cells[ctr,22].Value = item.SPENDOT;
                    ew.Cells[ctr,23].Value = item.SPEOT;
                    ew.Cells[ctr,24].Value = item.SPECIAL;
                    ew.Cells[ctr,25].Value = item.SPECIALND;
                    ew.Cells[ctr,26].Value = item.SPECIALNDOT;
                    ew.Cells[ctr,27].Value = item.SPECIALOT;
                    ew.Cells[ctr,28].Value = item.WLEGND;
                    ew.Cells[ctr,29].Value = item.WLEGNDOT;
                    ew.Cells[ctr,30].Value = item.WLEGOT;
                    ew.Cells[ctr,31].Value = item.WRKLEG;
                    ctr++;
                }

                ew.Row(1).Style.Font.Bold = true;
                
                ExcelWorksheet detew = ep.Workbook.Worksheets.Add("details");

                detew.Cells[1,1].Value = "Employee Code";
                detew.Cells[1,2].Value = "Employee Name";
                detew.Cells[1,3].Value = "LEG";
                detew.Cells[1,4].Value = "LEGAL";
                detew.Cells[1,5].Value = "REGND";
                detew.Cells[1,6].Value = "REGNDOT";
                detew.Cells[1,7].Value = "REGOT";
                detew.Cells[1,8].Value = "RST";
                detew.Cells[1,9].Value = "RSTLEG";
                detew.Cells[1,10].Value = "RSTLEGND";
                detew.Cells[1,11].Value = "RSTLEGNDOT";
                detew.Cells[1,12].Value = "RSTLEGOT";
                detew.Cells[1,13].Value = "RSTND";
                detew.Cells[1,14].Value = "RSTNDOT";
                detew.Cells[1,15].Value = "RSTOT";
                detew.Cells[1,16].Value = "RSTSPE";
                detew.Cells[1,17].Value = "RSTSPEND";
                detew.Cells[1,18].Value = "RSTSPENDOT";
                detew.Cells[1,19].Value = "RSTSPEOT";
                detew.Cells[1,20].Value = "SPE";
                detew.Cells[1,21].Value = "SPEND";
                detew.Cells[1,22].Value = "SPENDOT";
                detew.Cells[1,23].Value = "SPEOT";
                detew.Cells[1,24].Value = "SPECIAL";
                detew.Cells[1,25].Value = "SPECIALND";
                detew.Cells[1,26].Value = "SPECIALNDOT";
                detew.Cells[1,27].Value = "SPECIALOT";
                detew.Cells[1,28].Value = "WLEGND";
                detew.Cells[1,29].Value = "WLEGNDOT";
                detew.Cells[1,30].Value = "WLEGOT";
                detew.Cells[1,31].Value = "WRKLEG";
                detew.Cells[1,32].Value = "DATE";

                ctr = 2;
                foreach(var item in response.dets) {
                    detew.Cells[ctr,1].Value = item.EmployeeCode;
                    detew.Cells[ctr,2].Value = item.EmployeeName;
                    detew.Cells[ctr,3].Value = item.LEG;
                    detew.Cells[ctr,4].Value = item.LEGAL;
                    detew.Cells[ctr,5].Value = item.REGND;
                    detew.Cells[ctr,6].Value = item.REGNDOT;
                    detew.Cells[ctr,7].Value = item.REGOT;
                    detew.Cells[ctr,8].Value = item.RST;
                    detew.Cells[ctr,9].Value = item.RSTLEG;
                    detew.Cells[ctr,10].Value = item.RSTLEGND;
                    detew.Cells[ctr,11].Value = item.RSTLEGNDOT;
                    detew.Cells[ctr,12].Value = item.RSTLEGOT;
                    detew.Cells[ctr,13].Value = item.RSTND;
                    detew.Cells[ctr,14].Value = item.RSTNDOT;
                    detew.Cells[ctr,15].Value = item.RSTOT;
                    detew.Cells[ctr,16].Value = item.RSTSPE;
                    detew.Cells[ctr,17].Value = item.RSTSPEND;
                    detew.Cells[ctr,18].Value = item.RSTSPENDOT;
                    detew.Cells[ctr,19].Value = item.RSTSPEOT;
                    detew.Cells[ctr,20].Value = item.SPE;
                    detew.Cells[ctr,21].Value = item.SPEND;
                    detew.Cells[ctr,22].Value = item.SPENDOT;
                    detew.Cells[ctr,23].Value = item.SPEOT;
                    detew.Cells[ctr,24].Value = item.SPECIAL;
                    detew.Cells[ctr,25].Value = item.SPECIALND;
                    detew.Cells[ctr,26].Value = item.SPECIALNDOT;
                    detew.Cells[ctr,27].Value = item.SPECIALOT;
                    detew.Cells[ctr,28].Value = item.WLEGND;
                    detew.Cells[ctr,29].Value = item.WLEGNDOT;
                    detew.Cells[ctr,30].Value = item.WLEGOT;
                    detew.Cells[ctr,31].Value = item.WRKLEG;
                    detew.Cells[ctr,32].Value = item.Date.ToString("MM/dd/yyyy");
                    ctr++;
                }

                bytes = ep.GetAsByteArray();
                var link = _upload.upload(bytes,"kmbidevreporeports/ot-report-for-payroll.xlsx");
                ep.Dispose();
                return Redirect(link);
            }
        }
        [HttpPost("generateEcr")]
        public IActionResult generateEcr([FromBody] ecrDetails b, [FromQuery] string refId)
        {
           
            var path = Directory.GetCurrentDirectory() + @"\wwwroot\kmbidevrepo\";
           
            var rep = _reporting.getNewECR(b, refId);

            if (rep.batches.Count() > 1)
            {
                var data = JsonConvert.SerializeObject(rep);
                var repDesc = "ECR";
                saveReport("ecrBatching", data, repDesc);
                byte[] bytes = System.IO.File.ReadAllBytes(path + "ecrBatching.pdf");
                var link = _upload.upload(bytes, @"kmbidevreporeports/ecrBatching.pdf");

                return Ok(new
                {
                    link = link
                });
            }
            else
            {
                var data = JsonConvert.SerializeObject(rep);
                var repDesc = "ECR";
                saveReport("ecr", data, repDesc);
                byte[] bytes = System.IO.File.ReadAllBytes(path + "ecr.pdf");
                var link = _upload.upload(bytes, @"kmbidevreporeports/ecr.pdf");

                return Ok(new
                {
                    link = link
                });
            }

            
        }

        [HttpGet("downloaddtrtemplate")]
        public IActionResult downloadDtrTemplate([FromQuery]DateTime? from, [FromQuery]DateTime? to){
            using(ExcelPackage ep = new ExcelPackage()) {

                ExcelWorksheet ew = ep.Workbook.Worksheets.Add("dtr-template");

                ew.Cells["A1"].Value = "Date (MM/dd/yyyy)";
                ew.Cells["B1"].Value = "Time In (hh:mm AM or PM)";
                ew.Cells["C1"].Value = "Time Out (hh:mm AM or PM)";
                ew.Cells["D1"].Value = "Remarks";

                ew.Column(1).Width = 20;
                ew.Column(2).Width = 24;
                ew.Column(3).Width = 24;
                ew.Column(4).Width = 30;
                ew.Row(1).Style.Font.Bold = true;

                if(from!=null && to!=null) {
                    var _from = Convert.ToDateTime(from);
                    var _to = Convert.ToDateTime(to);

                    var line=2;
                    for(DateTime date = _from; date <= _to; date=date.AddDays(1)) {
                        if(date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday) {
                            ew.Cells[line,1].Value = date.ToShortDateString();
                            ew.Cells[line,1].Style.Font.Color.SetColor(Color.White);
                            ew.Cells[line,1,line,4].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            ew.Cells[line,1,line,4].Style.Fill.BackgroundColor.SetColor(Color.Orange);

                            ew.Cells[line,4].Value = "Rest Day";
                            // ew.Cells[line,4].Style.Font.Color.SetColor(Color.White);
                        } else {
                            ew.Cells[line,1].Value = date.ToShortDateString();
                        }
                        line++;
                    }
                }

                byte[] bytes = ep.GetAsByteArray();
                ep.Dispose();
                var link = _upload.upload(bytes,"kmbidevreporeports/dtr-template.xlsx");
                return Redirect(link);
            }
        }
    }
}