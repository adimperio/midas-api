using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.ComponentModel.DataAnnotations;

namespace kmbi_core_master.documentationModule.Models
{
    public class comment
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("slug")]
        public String slug {get;set;}

        [BsonElement("email")]
        [RequiredAttribute(ErrorMessage="email is required")]
        public String email {get;set;}

        [BsonElementAttribute("message")]
        [RequiredAttribute(ErrorMessage="message is required")]
        public String message {get;set;}

        [BsonElementAttribute("commentOnType")]
        public String commentOnType {get;set;}

        [BsonElementAttribute("commentOnId")]
        public String commentOnId {get;set;}

        public List<reply> replies {get;set;}

        [BsonElement("createdAt")]
        public DateTime createdAt {get;set;}
        
        [BsonElement("updatedAt")]
        public DateTime updatedAt {get;set;}
    }
}