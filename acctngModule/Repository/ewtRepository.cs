﻿using Microsoft.Extensions.Options;

using System;
using System.Collections.Generic;
using System.Linq;

using MongoDB.Bson;
using MongoDB.Driver;

using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.acctngModule.Models;
using kmbi_core_master.acctngModule.Interface;

namespace kmbi_core_master.acctngModule.Repository
{
    public class ewtRepository : dbCon, iewtRepository
    {
        
        public ewtRepository(IOptions<dbSettings> settings) : base(settings) {}

        public void add(ewt b)
        {

            b.createdAt = DateTime.Now;
            b.updatedAt = DateTime.Now;
            var coll = _db.GetCollection<ewt>("ewt");
            _db.GetCollection<ewt>("ewt").InsertOneAsync(b);
        }

    
        public IEnumerable<ewt> all()
        {
            var c = _db.GetCollection<ewt>("ewt").Find(new BsonDocument()).ToListAsync();
            return c.Result;
        }

        public ewt getSlug(String slug)
        {
            var query = Builders<ewt>.Filter.Eq(e => e.slug, slug);
            var c = _db.GetCollection<ewt>("ewt").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }
       
        public void update(String slug, ewt p)
        {
            p.slug = slug;
            var query = Builders<ewt>.Filter.Eq(e => e.slug, slug);
            var update = Builders<ewt>.Update
                .Set("name", p.name)
                .Set("description", p.description)
                .Set("code", p.code)
                .Set("taxRate", p.taxRate)
                .CurrentDate("updatedAt");

            var branch = _db.GetCollection<ewt>("ewt").UpdateOneAsync(query, update);

        }
        public void remove(String slug)
        {
            var query = Builders<ewt>.Filter.Eq(e => e.slug, slug);
            var result = _db.GetCollection<ewt>("ewt").DeleteOneAsync(query);
        }
    }
}
