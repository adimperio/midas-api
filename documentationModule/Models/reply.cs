using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.ComponentModel.DataAnnotations;

namespace kmbi_core_master.documentationModule.Models
{
    public class reply
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("slug")]
        public String slug {get;set;}

        [BsonElement("email")]
        [RequiredAttribute(ErrorMessage="email is required")]
        public String email {get;set;}

        [BsonElementAttribute("message")]
        [RequiredAttribute(ErrorMessage="message is required")]
        public String message {get;set;}

        public Boolean verifiedAnswer {get;set;}

        [BsonElement("createdAt")]
        public DateTime createdAt {get;set;}
        
        [BsonElement("updatedAt")]
        public DateTime updatedAt {get;set;}
    }
}