﻿using kmbi_core_master.adminModule.Inv.Models;
using kmbi_core_master.coreMaster.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.adminModule.Inv.IRepository
{
    public interface iinventoryRepository
    {
        void Add(inventory i);

        IEnumerable<inventory> getAll();

        inventory getBySlug(String slug);

        inventory getByTagNumber(String tagNumber);

        void update(String slug, inventory p);

        void remove(String slug);

        Int32 getCountByName(String name);
    }
}
