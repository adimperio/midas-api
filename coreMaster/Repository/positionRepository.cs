﻿using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Models;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace kmbi_core_master.coreMaster.Repository
{
    public class positionRepository : dbCon, ipositionRepository
    {
        public positionRepository(IOptions<dbSettings> settings) : base(settings)
        {
        }

        public void add(positions b)
        {

            b.createdAt = DateTime.Now;
            b.updatedAt = DateTime.Now;
            
            _db.GetCollection<positions>("positions").InsertOneAsync(b);
        }

        

        public IEnumerable<positions> getAll()
        {
            var d = _db.GetCollection<positions>("positions").Find(new BsonDocument()).ToListAsync();
            return d.Result;
        }



        public positions getSlug(String slug)
        {
            var query = Builders<positions>.Filter.Eq(e => e.slug, slug);
            var d = _db.GetCollection<positions>("positions").Find(query).ToListAsync();

            return d.Result.FirstOrDefault();
        }

     
        public void update(String slug, positions p)
        {
            p.slug = slug;

           
            var query = Builders<positions>.Filter.Eq(e => e.slug, slug);
            var update = Builders<positions>.Update
                .Set("name", p.name)
                .Set("department", p.department)
                .Set("job", p.job)
                .CurrentDate("updatedAt");

            var d = _db.GetCollection<positions>("positions").UpdateOneAsync(query, update);
        }

        
        public void delete(String slug)
        {
            var query = Builders<positions>.Filter.Eq(e => e.slug, slug);
            var d = _db.GetCollection<positions>("positions").DeleteOneAsync(query);
        }

       

        public IEnumerable<positions> search(String name)
        {

            var query = Builders<positions>.Filter.Regex("name", new BsonRegularExpression(name, options: "i"));
            var d = _db.GetCollection<positions>("positions").Find(query).ToListAsync();

            return d.Result;
        }

       
    }
}
