﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace kmbi_core_master.coreMaster.Models
{
    public class messages
    {
        public int success { get; set; }
        public string message { get; set; }
        
    }
}
