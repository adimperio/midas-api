﻿using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.payrollModule.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.payrollModule.IRepository
{
    public interface ikeysRepository
    {
        IEnumerable<keys> getAll();

        keys Get(string name);

        void Update(string name, keys c);

        users getPassword(string slug);
    }

}
