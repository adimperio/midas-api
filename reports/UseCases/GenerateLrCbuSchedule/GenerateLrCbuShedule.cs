using System;
using System.Collections.Generic;
using System.Linq;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Models;
using Microsoft.Extensions.Logging;

namespace kmbi_core_master.reports.UseCases.GenerateLrCbuSchedule {
    public class GenerateLrCbuSchedule : IUseCase<GenerateLrCbuScheduleRequest, GenerateLrCbuScheduleResponse>
    {
        public ibranchRepository _branchRepository { get; }
        public iregionsRepository _regionRepository { get; }
        public iclientsRepository _clientsRepository { get; }
        public icenterInstanceRepository _centerInstanceRepository { get; }
        public iloanRepository _loanRepository { get; }
        public ipaymentLoansRepository _paymentLoansRepository { get; }
        public icbuLogsRepository _cbuLogsRepository { get; }
        public ILogger<GenerateLrCbuSchedule> _logger { get; }

        public GenerateLrCbuSchedule(
            ibranchRepository branchRepository,
            iregionsRepository regionRepository,
            iclientsRepository clientsRepository,
            icenterInstanceRepository centerInstanceRepository,
            iloanRepository loanRepository,
            ipaymentLoansRepository paymentLoansRepository,
            icbuLogsRepository cbuLogsRepository,
            ILogger<GenerateLrCbuSchedule> logger
        ) {
            _branchRepository = branchRepository;
            _regionRepository = regionRepository;
            _clientsRepository = clientsRepository;
            _centerInstanceRepository = centerInstanceRepository;
            _loanRepository = loanRepository;
            _paymentLoansRepository = paymentLoansRepository;
            _cbuLogsRepository = cbuLogsRepository;
            _logger = logger;
        }

        public GenerateLrCbuScheduleResponse Handle(GenerateLrCbuScheduleRequest request) {
            _logger.LogInformation("Getting branches");
            var branchList = _branchRepository.allBranch();

            if(!(request.branchSlug=="" || request.branchSlug == null))
                branchList = branchList.Where(x=>x.slug == request.branchSlug);

            _logger.LogInformation("Getting regions");
            var regionList = _regionRepository.allRegion();

            _logger.LogInformation("Getting clients");
            var clientList = _clientsRepository.allClients().ToList().Select(x=>new LrCbuScheduleClient {
                Id = x.Id,
                name = x.fullname, 
                branchSlug = x.branchSlug, 
                centerSlug = x.centerSlug
            }).ToList();

            _logger.LogInformation("Getting center instances");
            var centerInstanceList = _centerInstanceRepository.allCenters();

            _logger.LogInformation("Getting loans");
            var loanList = _loanRepository.allLoans().ToList().Select(x=>new LrCbuScheduleLoan{
                referenceId = x.referenceId,
                branchSlug = x.branchSlug, 
                centerSlug = x.centerSlug, 
                principal = x.amounts.principalAmount, 
                interest = x.amounts.interestAmount, 
                cbu = x.amounts.cbuAmount,
                disbursementDate = x.disbursementDate, 
                lastPaymentDate = x.lastPaymentDate
            }).ToList();

            _logger.LogInformation("Getting payments");
            var paymentList = _paymentLoansRepository.all().ToList().Select(x=> new LrCbuSchedulePayment {
                referenceId = x.loan.referenceId,
                paidAt = x.officialReceipt.paidAt,
                branchSlug = x.loan.branch.slug,
                centerSlug = x.loan.center.slug,
                principal = x.officialReceipt.amounts.principal,
                interest = x.officialReceipt.amounts.interest,
                cbu = x.officialReceipt.amounts.cbu
            }).ToList();

            _logger.LogInformation("Getting cbu logs");
            var cbuLogsList = _cbuLogsRepository.getBeginningBalance().ToList();


            LrCbuSchedule Schedule = new LrCbuSchedule();
            
            List<BranchLrCbuSchedule> ScheduleDetails = new List<BranchLrCbuSchedule>();

            foreach(branch b in branchList) {
                _logger.LogInformation("Processing branch " + b.name);
                // filter clients in branch
                var _clientList = clientList.Where(x=>x.branchSlug == b.slug).ToList();

                // filter centerInstance in branch
                var _centerInstanceList = centerInstanceList.Where(x=>x.branchSlug == b.slug).ToList();

                // filter loanList in branch
                var _loanList = loanList.Where(x=>x.branchSlug == b.slug && x.status != "processing").ToList();

                // filter payment in branch and as of
                var _paymentsList = paymentList.Where(x=>x.branchSlug == b.slug && x.paidAt <= request.AsOf).ToList();

                BranchLrCbuSchedule Detail = new BranchLrCbuSchedule();
                Detail.Name = b.name;

                var areaName = "";
                foreach(regions r in regionList) {
                    foreach(areas a in r.areas) {
                        if(a.Id == b.areaId)
                            areaName = r.name;
                    }
                }

                Detail.Area = areaName;

                List<BranchLrCbuScheduleDetail> ScheduleDetailDetail = new List<BranchLrCbuScheduleDetail>();
                
                //schedule detail

                foreach(centerInstance instance in _centerInstanceList) {
                    BranchLrCbuScheduleDetail _ScheduleDetailDetail = new BranchLrCbuScheduleDetail();
                    // get no of clients
                    _ScheduleDetailDetail.NoOfClients = _clientList.Where(x=>x.centerSlug == instance.slug).Count();

                    // get unit
                    _ScheduleDetailDetail.Unit = instance.code.Substring(4,1);

                    // get code
                    _ScheduleDetailDetail.Center = instance.code;

                    // get last loan status
                    var lastLoan = _loanList.Where(x=>x.centerSlug == instance.slug && x.status!="processing").OrderByDescending(x=>x.disbursementDate).FirstOrDefault();
                    var centerStatus = "INACTIVE";
                    if(lastLoan!=null) {
                        if(lastLoan.status == "for-payment")
                            centerStatus = "ACTIVE";
                        
                        if(lastLoan.lastPaymentDate > DateTime.Now.Date)
                            centerStatus = "MATURED";
                    }

                    _ScheduleDetailDetail.CenterStatus = centerStatus;

                    // get loan amount
                    var loansPerCenter = _loanList.Where(x=>x.centerSlug == instance.slug);
                    _ScheduleDetailDetail.LoanAmount = loansPerCenter.Sum(x=>x.principal + x.interest);
                     // get payment as of only
                     var paymentsInCenter = _paymentsList.Where(x=>x.centerSlug == instance.slug);
                    var _payments = paymentsInCenter.Sum(aa=>aa.principal + aa.interest);
                    var _cbuPayment = paymentsInCenter.Sum(aa=>aa.cbu);
                    _ScheduleDetailDetail.OutstandingLoanReceivables = _ScheduleDetailDetail.LoanAmount - _payments;
                    // cbuBalance
                    var clientListId = _clientList.Where(x=>x.centerSlug == instance.slug).Select(x=>x.Id).ToList();
                    var cbuBegBal = cbuLogsList.Where(y=>clientListId.Contains(y.clientId)).Sum(x=>x.amount);
                    _ScheduleDetailDetail.CbuBalance = _cbuPayment + cbuBegBal;


                    
                    ScheduleDetailDetail.Add(_ScheduleDetailDetail);
                }

                Detail.Details = ScheduleDetailDetail.ToArray();
                Detail.Total = new BranchLrCbuScheduleTotal {
                    NoOfClients = ScheduleDetailDetail.Sum(x=>x.NoOfClients),
                    Active = ScheduleDetailDetail.Where(x=>x.CenterStatus == "ACTIVE").Count(),
                    InActive = ScheduleDetailDetail.Where(x=>x.CenterStatus == "INACTIVE").Count(),
                    Matured = ScheduleDetailDetail.Where(x=>x.CenterStatus == "MATURED").Count(),
                    LoanAmount = ScheduleDetailDetail.Sum(x=>x.LoanAmount),
                    OutstandingLoanReceivables = ScheduleDetailDetail.Sum(x=>x.OutstandingLoanReceivables),
                    CbuBalance = ScheduleDetailDetail.Sum(x=>x.CbuBalance)
                };
                
                ScheduleDetails.Add(Detail);
            }

            Schedule.Details = ScheduleDetails.ToArray();

            // sort
            Schedule.Details = Schedule.Details.ToList().OrderBy(x=>x.Area).ThenBy(x=>x.Name).ToArray();
            Schedule.Details.ToList().ForEach(x=>{
                x.Details = x.Details.OrderBy(y=>y.Unit).ThenBy(y=>y.Center).ToArray();
            });

            return new GenerateLrCbuScheduleResponse {
                asOf = request.AsOf,
                Schedule = Schedule,
            };
        }
    }

    public class GenerateLrCbuScheduleResponse {
        public DateTime asOf {get;set;}
        public LrCbuSchedule Schedule {get;set;}
    }

    public class GenerateLrCbuScheduleRequest {
        public DateTime AsOf {get;set;}
        public String branchSlug {get;set;}
    }




    public class LrCbuSchedule {
        public BranchLrCbuSchedule[] Details {get;set;}
    }

    public class BranchLrCbuSchedule {
        public String Name {get;set;}
        public String Area {get;set;}
        public BranchLrCbuScheduleDetail[] Details {get;set;}
        public BranchLrCbuScheduleTotal Total {get;set;}
    }

    public class BranchLrCbuScheduleDetail {
        public Int32 NoOfClients {get; set;}
        public String Unit {get; set;}
        public String Center {get; set;}
        public String CenterStatus {get; set;}
        public Double LoanAmount {get; set;}
        public Double OutstandingLoanReceivables {get; set;}
        public Double CbuBalance {get; set;}
    }

    public class BranchLrCbuScheduleTotal {
        public Int32 NoOfClients {get;set;}
        public Int32 Active {get;set;}
        public Int32 InActive {get;set;}
        public Int32 Matured {get;set;}
        public Double LoanAmount {get;set;}
        public Double OutstandingLoanReceivables {get; set;}
        public Double CbuBalance {get; set;}
    }

    public class LrCbuScheduleClient {
        public String Id {get;set;}
        public String name {get;set;}
        public String branchSlug {get;set;}
        public String centerSlug {get;set;}
    }

    public class LrCbuScheduleLoan {
        public String referenceId {get;set;}
        public String branchSlug {get;set;}
        public String centerSlug {get;set;}
        public String status {get;set;}
        public Double principal {get;set;}
        public Double interest {get;set;}
        public Double cbu {get;set;}
        public DateTime? disbursementDate {get;set;}
        public DateTime? lastPaymentDate {get;set;}
    }

    public class LrCbuSchedulePayment {
        public String referenceId {get;set;}
        public DateTime paidAt {get;set;}
        public String branchSlug {get;set;}
        public String centerSlug {get;set;}
        public Double principal {get;set;}
        public Double interest {get;set;}
        public Double cbu {get;set;}
    }

}