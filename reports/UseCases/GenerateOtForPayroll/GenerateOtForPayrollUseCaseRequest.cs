using System;

namespace UseCases.GenerateOtForPayroll
{
    public class GenerateOtForPayrollUseCaseRequest
    {
        public DateTime from {get;set;}
        public DateTime to {get;set;}
    }
}