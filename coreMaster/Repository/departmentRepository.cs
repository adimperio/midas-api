﻿using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Models;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace kmbi_core_master.coreMaster.Repository
{
    public class departmentRepository : dbCon, idepartmentRepository
    {
        public departmentRepository(IOptions<dbSettings> settings) : base(settings)
        {
        }

        public void add(departments b)
        {

            b.createdAt = DateTime.Now;
            b.updatedAt = DateTime.Now;
            
            _db.GetCollection<departments>("departments").InsertOneAsync(b);
        }

        

        public IEnumerable<departments> getAll()
        {
            var d = _db.GetCollection<departments>("departments").Find(new BsonDocument()).ToListAsync();
            return d.Result;
        }



        public departments getSlug(String slug)
        {
            var query = Builders<departments>.Filter.Eq(e => e.slug, slug);
            var d = _db.GetCollection<departments>("departments").Find(query).ToListAsync();

            return d.Result.FirstOrDefault();
        }

     
        public void update(String slug, departments p)
        {
            p.slug = slug;

           
            var query = Builders<departments>.Filter.Eq(e => e.slug, slug);
            var update = Builders<departments>.Update
                .Set("name", p.name)
                .Set("parent", p.parent)
                .Set("code", p.code)
                .CurrentDate("updatedAt");

            var d = _db.GetCollection<departments>("departments").UpdateOneAsync(query, update);
        }

        
        public void delete(String slug)
        {
            var query = Builders<departments>.Filter.Eq(e => e.slug, slug);
            var d = _db.GetCollection<departments>("departments").DeleteOneAsync(query);
        }

       

        public IEnumerable<departments> search(String name)
        {

            var query = Builders<departments>.Filter.Regex("name", new BsonRegularExpression(name, options: "i"));
            var d = _db.GetCollection<departments>("departments").Find(query).ToListAsync();

            return d.Result;
        }

       
    }
}
