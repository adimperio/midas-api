using System;

namespace UseCases.GenerateLeavesForPayroll {
    public class LeaveItem {
        public string EmployeeCode {get;set;}
        public string EmployeeName {get;set;}
        public Double TotalSL {get;set;}
        public Double TotalVL {get;set;}
    }

    public class LeaveItemDet : LeaveItem {
        public DateTime Date {get;set;}
    }
}