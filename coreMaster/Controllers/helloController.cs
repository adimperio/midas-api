﻿using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.helpers;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace kmbi_core_master.coreMaster.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/hello")]
    public class helloController : Controller
    {
        public IActionResult get()
        {
            return RedirectToAction("get","Diagnostics");
        }
    }
}

