using System;
using System.Collections.Generic;
using System.Linq;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Models;

namespace kmbi_core_master.reports.UseCases.GenerateResignedSummary {
    public class GenerateResignedSummary : IUseCase<GenerateResignedSummaryRequest, GenerateResignedSummaryResponse> {

        // resigned - cbuwithdrawal
        // offset - paymentLoans with mode = offset
        // writeoff - loans with status = written-off

        public icbuWithdrawalRepository _cbuWithdrawalRepo { get; }
        public iloanRepository _loanRepo { get; }
        public ipaymentLoansRepository _paymentLoanRepo { get; }
        public icenterInstanceRepository _centerInstanceRepo { get; }
        public ibranchRepository _branchRepo { get; }
        public iclientsRepository _clientRepo { get; }

        public GenerateResignedSummary(
            icbuWithdrawalRepository cbuWithdrawalRepo,
            iloanRepository loanRepo,
            ipaymentLoansRepository paymentLoanRepo,
            icenterInstanceRepository centerInstanceRepo,
            ibranchRepository branchRepo,
            iclientsRepository clientRepo,
            ipaymentLoansRepository paymentLoansRepoo
        ) {
            _cbuWithdrawalRepo = cbuWithdrawalRepo;
            _loanRepo = loanRepo;
            _paymentLoanRepo = paymentLoanRepo;
            _centerInstanceRepo = centerInstanceRepo;
            _branchRepo = branchRepo;
            _clientRepo = clientRepo;
        }


        public GenerateResignedSummaryResponse Handle(GenerateResignedSummaryRequest request) {
            
            var cbuWithdrawPerBranch = _cbuWithdrawalRepo.getBSlug(request.BranchSlug).ToList();
            var centerInstanceList = _centerInstanceRepo.getByBranch(request.BranchSlug).ToList();
            var branch = _branchRepo.getBranchSlug(request.BranchSlug);
            var clients = _clientRepo.getByProcessStatus("approved", request.BranchSlug).ToList();
            var payments = _paymentLoanRepo.all().Where(x=>x.loan.branch.slug == request.BranchSlug).ToList();
            var loans = _loanRepo.allLoans().Where(x=>x.branchSlug == request.BranchSlug).ToList();

            var ResignedSummary = new ResignedSummary();
            
            // resigned

            List<ResignedClientsDetail> clientList = new List<ResignedClientsDetail>();
            cbuWithdrawPerBranch.ForEach(cbuWith=>{
                var center = centerInstanceList.Where(centIns=>centIns.slug == cbuWith.center.slug).FirstOrDefault();
                
                var _unit = "";
                var _center = "";

                if(center!=null) {
                    _unit = center.code.Substring(4,1);
                    _center = center.code;
                }

                foreach(var mem in cbuWith.members) {

                    var _loanCycle = "";

                    var client = clients.Where(c=>c.slug == mem.slug).FirstOrDefault();
                    if(client!=null)
                        _loanCycle = client.loanCycle.ToString();
                    
                    clientList.Add(new ResignedClientsDetail {
                        Unit = _unit,
                        Center = _center,
                        ClientName = mem.name,
                        LoanCycle = _loanCycle,
                        ReasonForWithdrawal = mem.reason,
                        AmountOfCbuWithdrawal = Math.Round(mem.amount, 2),
                        DateOfCbuWithdrawal = Convert.ToDateTime(cbuWith.rfp.datePrepared)
                    });
                }
            });

            ResignedSummary.Resigned = new ResignedClients {
                Details = clientList.ToArray(),
                Total = new ResignedClientsDetailTotal {
                    NoOfClients = clientList.Count(),
                    AmountOfCbuWithdrawal = Math.Round(clientList.Sum(x=>x.AmountOfCbuWithdrawal),2)
                },
                PreparedBy = new PreparedByInfo{
                    PreparedBy = "asdf",
                    PreparedByPosition = "asdf-position"
                }
            };
            
            // resigned



            // offset && writeoff

            List<OffsetClientsDetail> offsetList = new List<OffsetClientsDetail>();
            List<WriteOffClientsDetail> writeoffList = new List<WriteOffClientsDetail>();

            loans.ToList().ForEach(loan=>{
                
                var _payments = payments.Where(pay=>pay.loan.referenceId == loan.referenceId && pay.officialReceipt.paidAt <= request.AsOf);

                
                loan.members.ToList().ForEach(mem=>{
                    
                    var center = centerInstanceList.Where(centIns=>centIns.slug == loan.centerSlug).FirstOrDefault();
                    var _centerName = "";
                    
                    if(center!=null)
                        _centerName = center.code;
                    
                    var _loanCycle = "";

                    var client = clients.Where(c=>c.slug == mem.slug).FirstOrDefault();
                    if(client!=null)
                        _loanCycle = client.loanCycle.ToString();
                    
                    var offsetPayment = _payments.Where(x=>x.mode == "offset").ToList();

                    var balBeforeOffset = new OffsetClientDetailLrIrCbu();
                    var amtOsOffset = new OffsetClientDetailLrIrCbu();
                    var balAfterOffset = new OffsetClientDetailLrIrCbu();
                    
                    var collAfterOffset = new OffsetClientDetailLrIrCbu();
                    var updatedBalAfterCollOffset = new OffsetClientDetailLrIrCbu();
                    var amtForWO = new  OffsetClientDetailLrIrCbu();

                    if(offsetPayment.Count > 0) {
                        List<paymentLoansMembers> offsetMem = offsetPayment.Select(x=>x.members.Where(y=>y.slug == mem.slug).FirstOrDefault()).ToList();

                        

                        balBeforeOffset.LR += Math.Round(mem.schedule.Sum(cb=>cb.currentPrincipalDue) - _payments.Where(payment=>payment.officialReceipt.paidAt <= offsetPayment.Last().officialReceipt.paidAt).Sum(memberPayment=>memberPayment.members.Where(y=>y.slug == mem.slug).Sum(xx=>xx.amounts.principal)) + offsetMem.Sum(x=>x.amounts.principal),2);
                        balBeforeOffset.IR += Math.Round(mem.schedule.Sum(cb=>cb.currentInterestDue) - _payments.Where(payment=>payment.officialReceipt.paidAt <= offsetPayment.Last().officialReceipt.paidAt).Sum(memberPayment=>memberPayment.members.Where(y=>y.slug == mem.slug).Sum(xx=>xx.amounts.interest)) + offsetMem.Sum(x=>x.amounts.interest),2);
                        balBeforeOffset.CBU += Math.Round(mem.schedule.Sum(cb=>cb.currentCbuDue) + _payments.Where(payment=>payment.officialReceipt.paidAt <= offsetPayment.Last().officialReceipt.paidAt).Sum(memberPayment=>memberPayment.members.Where(y=>y.slug == mem.slug).Sum(xx=>xx.amounts.cbu)) - offsetMem.Sum(x=>x.amounts.cbu),2);

                        amtOsOffset.LR += offsetMem.Sum(x=>x.amounts.principal);
                        amtOsOffset.IR += offsetMem.Sum(x=>x.amounts.interest);
                        amtOsOffset.CBU += offsetMem.Sum(x=>x.amounts.cbu);

                        balAfterOffset.LR += balBeforeOffset.LR - amtOsOffset.LR;
                        balAfterOffset.IR += balBeforeOffset.IR - amtOsOffset.IR;
                        balAfterOffset.CBU += balBeforeOffset.CBU + amtOsOffset.CBU;

                        offsetList.Add(new OffsetClientsDetail {
                            CenterName = _centerName,
                            Surname = mem.name.last,
                            Firstname = mem.name.first,
                            LoanCycle = (_loanCycle == "") ? 0 : Convert.ToInt32(_loanCycle),
                            LoanDisbursed = mem.totalAmount,
                            BalanceBeforeOffsetting = balBeforeOffset,
                            AmountForOs = amtOsOffset,
                            BalanceAfterOfsetting = balAfterOffset,
                        });
                        
                    }

                    if(loan.status=="written-off" && loan.updatedAt <= request.AsOf) {

                        if(offsetPayment.Count > 0) {
                            collAfterOffset.LR += Math.Round(mem.schedule.Sum(cb=>cb.currentPrincipalDue) - _payments.Where(payment=>payment.officialReceipt.paidAt > offsetPayment.Last().officialReceipt.paidAt).Sum(memberPayment=>memberPayment.members.Where(y=>y.slug == mem.slug).Sum(xx=>xx.amounts.principal)) + loan.amounts.principalAmount,2);
                            collAfterOffset.IR += Math.Round(mem.schedule.Sum(cb=>cb.currentInterestDue) - _payments.Where(payment=>payment.officialReceipt.paidAt > offsetPayment.Last().officialReceipt.paidAt).Sum(memberPayment=>memberPayment.members.Where(y=>y.slug == mem.slug).Sum(xx=>xx.amounts.interest)) + loan.amounts.interestAmount,2);
                            collAfterOffset.CBU += Math.Round(mem.schedule.Sum(cb=>cb.currentCbuDue) + _payments.Where(payment=>payment.officialReceipt.paidAt > offsetPayment.Last().officialReceipt.paidAt).Sum(memberPayment=>memberPayment.members.Where(y=>y.slug == mem.slug).Sum(xx=>xx.amounts.cbu)) - loan.amounts.cbuAmount,2);
                        } else {
                            collAfterOffset.LR += Math.Round(mem.schedule.Sum(cb=>cb.currentPrincipalDue) - _payments.Sum(memberPayment=>memberPayment.members.Where(y=>y.slug == mem.slug).Sum(xx=>xx.amounts.principal)) + loan.amounts.principalAmount,2);
                            collAfterOffset.IR += Math.Round(mem.schedule.Sum(cb=>cb.currentInterestDue) - _payments.Sum(memberPayment=>memberPayment.members.Where(y=>y.slug == mem.slug).Sum(xx=>xx.amounts.interest)) + loan.amounts.interestAmount,2);
                            collAfterOffset.CBU += Math.Round(mem.schedule.Sum(cb=>cb.currentCbuDue) + _payments.Sum(memberPayment=>memberPayment.members.Where(y=>y.slug == mem.slug).Sum(xx=>xx.amounts.cbu)) - loan.amounts.cbuAmount,2);
                        }
                        
                        amtForWO.LR += loan.amounts.principalAmount;
                        amtForWO.IR += loan.amounts.interestAmount;
                        amtForWO.CBU += loan.amounts.cbuAmount;
                        
                        updatedBalAfterCollOffset.LR += collAfterOffset.LR - amtForWO.LR;
                        updatedBalAfterCollOffset.IR += collAfterOffset.IR - amtForWO.IR;
                        updatedBalAfterCollOffset.CBU += collAfterOffset.CBU + amtForWO.CBU;

                        writeoffList.Add(new WriteOffClientsDetail {
                            CenterName = _centerName,
                            Surname = mem.name.last,
                            Firstname = mem.name.first,
                            LoanCycle = (_loanCycle == "") ? 0 : Convert.ToInt32(_loanCycle),
                            LoanDisbursed = mem.totalAmount,
                            BalanceBeforeOffsetting = balBeforeOffset,
                            AmountForOs = amtOsOffset,
                            BalanceAfterOfsetting = balAfterOffset,
                            CollectionAfterOfsetting = collAfterOffset,
                            UpdatedBalanceAfterOfsetting = updatedBalAfterCollOffset,
                            AmountForWriteOff = amtForWO,
                            DateWrittenOff = Convert.ToDateTime(loan.updatedAt)
                        });

                    }

                

                });
            });
            
            

            ResignedSummary.Offset = new OffsetClients {
                Details = offsetList.ToArray(),
                Total = new OffsetClientsDetailTotal {
                    NoOfClients = offsetList.Count(),
                    LoanDisbursed = offsetList.Sum(off=>off.LoanDisbursed),
                    BalanceBeforeOffsetting = new OffsetClientDetailLrIrCbu {
                        LR = offsetList.Sum(off=>off.BalanceBeforeOffsetting.LR),
                        IR = offsetList.Sum(off=>off.BalanceBeforeOffsetting.IR),
                        CBU = offsetList.Sum(off=>off.BalanceBeforeOffsetting.CBU)
                    },
                    AmountForOs = new OffsetClientDetailLrIrCbu {
                        LR = offsetList.Sum(off=>off.AmountForOs.LR),
                        IR = offsetList.Sum(off=>off.AmountForOs.IR),
                        CBU = offsetList.Sum(off=>off.AmountForOs.CBU)
                    },
                    BalanceAfterOfsetting = new OffsetClientDetailLrIrCbu {
                        LR = offsetList.Sum(off=>off.BalanceAfterOfsetting.LR),
                        IR = offsetList.Sum(off=>off.BalanceAfterOfsetting.IR),
                        CBU = offsetList.Sum(off=>off.BalanceAfterOfsetting.CBU)
                    }
                },
                PreparedBy = new PreparedByInfo{
                    PreparedBy = "asdf",
                    PreparedByPosition = "asdf-position"
                }
            };

            ResignedSummary.WriteOff = new WriteOffClients {
                Details = writeoffList.ToArray(),
                Total = new WriteOffClientsDetailTotal {
                    NoOfClients = writeoffList.Count(),
                    LoanDisbursed = writeoffList.Sum(off=>off.LoanDisbursed),
                    BalanceBeforeOffsetting = new OffsetClientDetailLrIrCbu {
                        LR = writeoffList.Sum(off=>off.BalanceBeforeOffsetting.LR),
                        IR = writeoffList.Sum(off=>off.BalanceBeforeOffsetting.IR),
                        CBU = writeoffList.Sum(off=>off.BalanceBeforeOffsetting.CBU)
                    },
                    AmountForOs = new OffsetClientDetailLrIrCbu {
                        LR = writeoffList.Sum(off=>off.AmountForOs.LR),
                        IR = writeoffList.Sum(off=>off.AmountForOs.IR),
                        CBU = writeoffList.Sum(off=>off.AmountForOs.CBU)
                    },
                    BalanceAfterOfsetting = new OffsetClientDetailLrIrCbu {
                        LR = writeoffList.Sum(off=>off.BalanceAfterOfsetting.LR),
                        IR = writeoffList.Sum(off=>off.BalanceAfterOfsetting.IR),
                        CBU = writeoffList.Sum(off=>off.BalanceAfterOfsetting.CBU)
                    },
                    CollectionAfterOfsetting = new OffsetClientDetailLrIrCbu {
                        LR = writeoffList.Sum(off=>off.CollectionAfterOfsetting.LR),
                        IR = writeoffList.Sum(off=>off.CollectionAfterOfsetting.IR),
                        CBU = writeoffList.Sum(off=>off.CollectionAfterOfsetting.CBU)
                    },
                    UpdatedBalanceAfterOfsetting = new OffsetClientDetailLrIrCbu {
                        LR = writeoffList.Sum(off=>off.UpdatedBalanceAfterOfsetting.LR),
                        IR = writeoffList.Sum(off=>off.UpdatedBalanceAfterOfsetting.IR),
                        CBU = writeoffList.Sum(off=>off.UpdatedBalanceAfterOfsetting.CBU)
                    },
                    AmountForWriteOff = new OffsetClientDetailLrIrCbu {
                        LR = writeoffList.Sum(off=>off.AmountForWriteOff.LR),
                        IR = writeoffList.Sum(off=>off.AmountForWriteOff.IR)
                    }
                },
                PreparedBy = new PreparedByInfo{
                    PreparedBy = "asdf",
                    PreparedByPosition = "asdf-position"
                }
            };
            
            // offset && writeoff

          

            return new GenerateResignedSummaryResponse {
                AsOf = request.AsOf,
                Branch = branch.name,
                ResignedSummary = ResignedSummary
            };
        }
    }

    public class GenerateResignedSummaryResponse {
        public DateTime AsOf {get;set;}
        public String Branch {get;set;}
        public ResignedSummary ResignedSummary {get;set;}
    }

    public class GenerateResignedSummaryRequest {
        public String BranchSlug {get;set;}
        public DateTime AsOf {get;set;}
    }

    public class ResignedSummary {
        public ResignedClients Resigned {get;set;}
        public OffsetClients Offset {get;set;}
        public WriteOffClients WriteOff {get;set;}
    }

    public class ResignedClients {
        public ResignedClientsDetail[] Details {get;set;}
        public ResignedClientsDetailTotal Total {get;set;}
        public PreparedByInfo PreparedBy {get;set;}
    }

    public class ResignedClientsDetail {
        public String Unit {get;set;}
        public String Center {get;set;}
        public String ClientName {get;set;}
        public String LoanCycle {get;set;}
        public String ReasonForWithdrawal {get;set;}
        public Double AmountOfCbuWithdrawal {get;set;}
        public DateTime DateOfCbuWithdrawal {get;set;}
    }

    public class ResignedClientsDetailTotal {
        public Int32 NoOfClients {get;set;}
        public Double AmountOfCbuWithdrawal {get;set;}
    }

    public class WriteOffClients {
        public WriteOffClientsDetail[] Details {get;set;}
        public WriteOffClientsDetailTotal Total {get;set;}
        public PreparedByInfo PreparedBy {get;set;}
    }

    public class WriteOffClientsDetail {
        public String CenterName {get;set;}
        public String Surname {get;set;}
        public String Firstname {get;set;}
        public Int32 LoanCycle {get;set;}
        public Double LoanDisbursed {get;set;}
        public OffsetClientDetailLrIrCbu BalanceBeforeOffsetting {get;set;}
        public OffsetClientDetailLrIrCbu AmountForOs {get;set;}
        public OffsetClientDetailLrIrCbu BalanceAfterOfsetting {get;set;}
        public OffsetClientDetailLrIrCbu CollectionAfterOfsetting {get;set;}
        public OffsetClientDetailLrIrCbu UpdatedBalanceAfterOfsetting {get;set;}
        public OffsetClientDetailLrIrCbu AmountForWriteOff {get;set;}
        public DateTime DateWrittenOff {get;set;}
    }

    public class WriteOffClientsDetailTotal {
        public Int32 NoOfClients {get;set;}
        public Double LoanDisbursed {get;set;}
        public OffsetClientDetailLrIrCbu BalanceBeforeOffsetting {get;set;}
        public OffsetClientDetailLrIrCbu AmountForOs {get;set;}
        public OffsetClientDetailLrIrCbu BalanceAfterOfsetting {get;set;}
        public OffsetClientDetailLrIrCbu CollectionAfterOfsetting {get;set;}
        public OffsetClientDetailLrIrCbu UpdatedBalanceAfterOfsetting {get;set;}
        public OffsetClientDetailLrIrCbu AmountForWriteOff {get;set;}
    }

    public class OffsetClients {
        public OffsetClientsDetail[] Details {get;set;}
        public OffsetClientsDetailTotal Total {get;set;}
        public PreparedByInfo PreparedBy {get;set;}
    }

    public class OffsetClientsDetail {
        public String CenterName {get;set;}
        public String Surname {get;set;}
        public String Firstname {get;set;}
        public Int32 LoanCycle {get;set;}
        public Double LoanDisbursed {get;set;}
        public OffsetClientDetailLrIrCbu BalanceBeforeOffsetting {get;set;}
        public OffsetClientDetailLrIrCbu AmountForOs {get;set;}
        public OffsetClientDetailLrIrCbu BalanceAfterOfsetting {get;set;}
    }

    public class OffsetClientsDetailTotal {
        public Int32 NoOfClients {get;set;}
        public Double LoanDisbursed {get;set;}
        public OffsetClientDetailLrIrCbu BalanceBeforeOffsetting {get;set;}
        public OffsetClientDetailLrIrCbu AmountForOs {get;set;}
        public OffsetClientDetailLrIrCbu BalanceAfterOfsetting {get;set;}
    }

    public class OffsetClientDetailLrIrCbu {
        public Double LR {get;set;}
        public Double IR {get;set;}
        public Double CBU {get;set;}
    }

    public class PreparedByInfo {
        public String PreparedBy {get;set;}
        public String PreparedByPosition {get;set;}
    }
}