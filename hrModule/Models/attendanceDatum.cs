using System;
using System.Collections.Generic;
using kmbi_core_master.documentationModule.Models;
using kmbi_core_master.hrModule.Models;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace kmbi_core_master.coreMaster.Models {

    public class attendanceDatum {
        public String branchSlug {get;set;}
        public String branchName {get;set;}
        public IEnumerable<holiday> holidays {get;set;}
        public List<employee> employees {get;set;}
    }

    public class employee {
        public String employeeCode {get;set;}
        public String fullname {get;set;}
        public String jobGrade {get;set;}
        public String position {get;set;}
        public List<employeeSchedule> schedules {get;set;}
        public IEnumerable<leaveLog> leaveLogs {get;set;}
        public List<dtrManual> dtrManuals {get;set;}
        public List<otLog> otLogs {get;set;}
        public IEnumerable<attendanceLogs> attendanceLogs {get;set;}
    }
}