﻿using kmbi_core_master.coreMaster.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.coreMaster.IRepository
{
    public interface icenterInstanceRepository
    {
        IEnumerable<centerInstance> allCenters();

        centerInstance getSlug(String slug);

        IEnumerable<centerInstance> getOfficer(String officerId);

        IEnumerable<centerInstance> getOfficerSlug(String officerSlug);

        IEnumerable<centerInstance> searchCode(string code, string branchId);

        IEnumerable<centerInstance> getByBranch(string branchSlug);

        void add(centerInstance b);

        void update(String slug, centerInstance b);

        void remove(String slug);

        IEnumerable<centerInstance> getByPOEmailAddress(string email);

    }
}
