﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.payrollModule.Models
{
    public class philhealth
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }
       
        [BsonElement("salaryRange")]
        public phSalaryRange salaryRange { get; set; }

        [BsonElement("salaryBased")]
        public double salaryBased { get; set; }

        [BsonElement("contribution")]
        public phContribution contribution { get; set; }

        [BsonElement("total")]
        public double total { get; set; }
    }

    public class phSalaryRange
    {

        [BsonElement("from")]
        public double from { get; set; }

        [BsonElement("to")]
        public double to { get; set; }
    }

    public class phContribution
    {
        [BsonElement("er")]
        public double er { get; set; }

        [BsonElement("ee")]
        public double ee { get; set; }
    }

}
