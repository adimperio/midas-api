﻿using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.payrollModule.IRepository;
using kmbi_core_master.payrollModule.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace kmbi_core_master.payrollModule.Repository
{
    public class fhgDeductionsRepository: dbCon, ifhgDeductionsRepository
    {
        IMongoCollection<fhgDeduction> _collection;
        public fhgDeductionsRepository(IOptions<dbSettings> settings) : base(settings) {
             _collection = _db.GetCollection<fhgDeduction>("fhgDeductions");
        }

        public fhgDeduction[] getAll()
        {
            var filter = Builders<fhgDeduction>.Filter.Empty;
            var list = _collection.Find(filter).ToList().ToArray();
            return list;
        }

        public fhgDeduction[] getByCutOff(DateTime from, DateTime to)
        {
            var filter1 = Builders<fhgDeduction>.Filter.Gte(x=>x.cutoff.from, from);
            var filter2 = Builders<fhgDeduction>.Filter.Lte(x=>x.cutoff.to, to);
            var filterAnd = Builders<fhgDeduction>.Filter.And(filter1,filter2);
            var list = _collection.Find(filterAnd).ToList().ToArray();
            return list;
        }

        public void insertAll(fhgDeduction[] deductions)
        {
            _collection.InsertMany(deductions);
        }

        public void deleteAll(DateTime from, DateTime to) {
            var filter1 = Builders<fhgDeduction>.Filter.Gte(x=>x.cutoff.from, from);
            var filter2 = Builders<fhgDeduction>.Filter.Lte(x=>x.cutoff.to, to);
            var filterAnd = Builders<fhgDeduction>.Filter.And(filter1,filter2);

            _collection.DeleteMany(filterAnd);
        }
    }
}
