using System;
using System.Collections.Generic;
using Microsoft.Extensions.Options;

using kmbi_core_master.documentationModule.Models;
using kmbi_core_master.documentationModule.iRepositories;
using kmbi_core_master.coreMaster.Models;
using MongoDB.Driver;
using System.Linq;

namespace kmbi_core_master.documentationModule.Repositories {
    public class holidayRepository :  dbCon, iholidayRepository
    {
        IMongoCollection<holiday> _coll;
        public holidayRepository(IOptions<dbSettings> settings) : base(settings) {
            _coll = _db.GetCollection<holiday>("holiday");
        }

        public IEnumerable<holiday> GetHolidays(int month = 0, int year = 0)
        {
            IEnumerable<holiday> list = null;
            
            if(month==0 && year==0)
            {
                
                list = _coll.Find(Builders<holiday>.Filter.Empty).ToList();
            }
            else
            {
                DateTime first;
                Boolean isValid = DateTime.TryParse(month.ToString() + "/1/" + year.ToString(),out first);
                DateTime last = first.AddMonths(1).AddDays(-1);
                
                var filterFrom = Builders<holiday>.Filter.Gte("date",first);
                var filterTo = Builders<holiday>.Filter.Lte("date",last);
                var filtercombine = Builders<holiday>.Filter.And(filterFrom,filterTo);

                if(isValid)
                {
                    list = _coll.Find(filtercombine).ToList();
                }
            }
            return list;           
        }

        public holiday GetHolidayBySlug(string slug)
        {
            return _coll.Find(Builders<holiday>.Filter.Eq("slug",slug)).FirstOrDefault();
        }

        public bool InsertHoliday(holiday holiday)
        {
            holiday.createdAt = DateTime.Now;
            holiday.updatedAt = DateTime.Now;
            _coll.InsertOne(holiday);

            holiday existingHoliday = GetHolidayBySlug(holiday.slug);

            if(existingHoliday!=null) {
                return true;
            } else {
                return false;
            }
        }

        public bool UpdateHoliday(string holidaySlug, holiday holiday)
        {
            var filter = Builders<holiday>
                .Filter
                .Eq("slug",holidaySlug);
            var update = Builders<holiday>
                .Update
                .Set("name", holiday.name)
                .Set("date", holiday.date)
                .Set("type", holiday.type)
                .CurrentDate("updatedAt");

            var IsSuccess = true; 
            _coll.UpdateOne(filter,update);
            if(IsSuccess) {
                return true;
            } else {
                return false;
            }
        }
        
        public bool DeleteHoliday(string holidaySlug)
        {
             var filter = Builders<holiday>
                .Filter
                .Eq("slug",holidaySlug);

            holiday holiday = GetHolidayBySlug(holidaySlug);
            var IsSuccess = true;
            _coll.DeleteOne(filter);

            if(IsSuccess) {    
                return true;
            } else {
                return false;
            }
        }

        public IEnumerable<holiday> getNextThirtyDaysHolidays()
        {
            var from = DateTime.Now.Date;
            var to = from.AddDays(30).AddHours(23).AddSeconds(59);
            return _coll.Find(
                    Builders<holiday>.Filter.And(
                        Builders<holiday>.Filter.Gte(h=>h.date,from),
                        Builders<holiday>.Filter.Lte(h=>h.date,to)
                    )
                ).SortBy(h=>h.createdAt).ToEnumerable();
        }


        public IEnumerable<holiday> getNext365Holidays(DateTime date)
        {
            var from = date;
            var to = from.AddDays(365).AddHours(23).AddSeconds(59);
            return _coll.Find(
                    Builders<holiday>.Filter.And(
                        Builders<holiday>.Filter.Gte(h => h.date, from),
                        Builders<holiday>.Filter.Lte(h => h.date, to)
                    )
                ).SortBy(h => h.createdAt).ToEnumerable();
        }

        public IEnumerable<holiday> GetHolidays()
        {
            return _coll.Find(Builders<holiday>.Filter.Empty).ToList();
        }

        public IEnumerable<holiday> GetHolidaysDateRange(DateTime dateFrom, DateTime dateTo)
        {
            IEnumerable<holiday> list = null;

            var from = dateFrom;
            var to = dateTo.Date.AddHours(23).AddMinutes(59).AddSeconds(59);

            //if (month == 0 && year == 0)
            //{

            //    list = _coll.Find(Builders<holiday>.Filter.Empty).ToList();
            //}
            //else
            //{
                //DateTime first;
                //Boolean isValid = DateTime.TryParse(month.ToString() + "/1/" + year.ToString(), out first);
                //DateTime last = first.AddMonths(1).AddDays(-1);

                var filterFrom = Builders<holiday>.Filter.Gte("date", from);
                var filterTo = Builders<holiday>.Filter.Lte("date", to);
                var filtercombine = Builders<holiday>.Filter.And(filterFrom, filterTo);

                //if (isValid)
                //{
                    list = _coll.Find(filtercombine).ToList();
                //}
            //}
            return list;
        }
    }
}