﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace kmbi_core_master.coreMaster.Models
{
    public class universalSearch
    {
        public String name {get;set;}
        public String email {get;set;}
        public String branchSlug {get;set;}
        public String address {get;set;}
        public Int32? ageFrom {get;set;}
        public Int32? ageTo {get;set;}

        public String civilStatus {get;set;}
        public String religion {get;set;}
        public String sss {get;set;}
        public String pagibig {get;set;}
        public String philhealth {get;set;}
        public String tin {get;set;}
        public String taxCode {get;set;}

        public String gender {get;set;}
        public String salutation {get;set;}
        public String extension {get;set;}
        public String mobile {get;set;}
        public String landline {get;set;}
        public DateTime? birthdateFrom {get;set;}
        public DateTime? birthdateTo {get;set;}
        public String skill {get;set;}
        public String position {get;set;}
        public String department {get;set;}
        public String jobGrade {get;set;}
        public String jobClassification {get;set;}
        public String jobCategory {get;set;}
        public String status {get;set;}
        public decimal? salaryFrom {get;set;}
        public decimal? salaryTo {get;set;}
        public decimal? allowanceFrom {get;set;}
        public decimal? allowanceTo {get;set;}
        public decimal? benefitsFrom {get;set;}
        public decimal? benefitsTo {get;set;}
        public String nickname {get;set;}






    }
}
