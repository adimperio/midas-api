﻿using kmbi_core_master.coreMaster.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System.Security.Cryptography.X509Certificates;

namespace kmbi_core_master
{
    public class dbCon
    {
        public static IMongoDatabase _db;
        public dbSettings _settings;
        public static IMongoClient _client;

        public dbCon(IOptions<dbSettings> settings)
        {
            _settings = settings.Value;
            _db = Connect();

            //SSL
            //connectMongoSSL();
        }

        public IMongoDatabase Connect()
        {
            var client = new MongoClient(_settings.MongoConnection);
            var database = client.GetDatabase(_settings.Database);
            
            return database;
        }


        public void connectMongoSSL()
        {
            var connectionString = _settings.MongoConnection;
            var clientSettings = MongoClientSettings.FromUrl(new MongoUrl(connectionString));
            var cert = new X509Certificate2("https://s3-ap-southeast-1.amazonaws.com/kmbidevrepo/uploads/mongodb.pfx", "kmbidemo");

            clientSettings = new MongoClientSettings
            {
                Server = new MongoServerAddress(_settings.ServerAddress),
                SslSettings = new SslSettings
                {
                    ClientCertificates = new[] { cert },
                    CheckCertificateRevocation = false
                },
                UseSsl = true,
                VerifySslCertificate = false
            };

            //clientSettings.;

            MongoClient client = new MongoClient(clientSettings);
            _db = client.GetDatabase(_settings.Database);
        }
    }
}
