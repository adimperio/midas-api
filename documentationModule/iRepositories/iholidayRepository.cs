using System;
using System.Collections.Generic;
using kmbi_core_master.documentationModule.Models;

namespace kmbi_core_master.documentationModule.iRepositories {

    public interface iholidayRepository {
        IEnumerable<holiday> GetHolidays(Int32 month, Int32 year);
        IEnumerable<holiday> GetHolidays();
        holiday GetHolidayBySlug(String slug);
        Boolean InsertHoliday(holiday holiday);
        Boolean UpdateHoliday(String holidaySlug, holiday holiday);
        Boolean DeleteHoliday(String holidaySlug);
        IEnumerable<holiday> getNextThirtyDaysHolidays();

        IEnumerable<holiday> getNext365Holidays(DateTime date);
        IEnumerable<holiday> GetHolidaysDateRange(DateTime dateFrom, DateTime dateTo);
    }
}