﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace kmbi_core_master.coreMaster.Models
{
    public class users
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("email")]
        [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessage ="Please input valid E-mail Address")]
        public string email { get; set; }

        [BsonElement("password")]
        [StringLength(60, MinimumLength = 8, ErrorMessage = "Password must have a minimum length of 8 characters.")]
        public string password { get; set; }

        [BsonElement("token")]
        public string token { get; set; }

        [BsonElement("verificationToken")]
        public string verificationToken { get; set; }

        [BsonElement("isActive")]
        public int? isActive { get; set; }

        [BsonElement("isVerified")]
        public int? isVerified { get; set; }

        [BsonElementAttribute("isPending")]
        public Boolean isPending {get;set;}

        public List<string> permissions { get; set; }

        public Name name { get; set; }

        [BsonElement("fullname")]
        public string fullname { get; set; }

        [BsonElement("employeeCode")]
        public String employeeCode {get;set;}

        [BsonElement("biometricsCode")]
        public String biometricsCode {get;set;}

        [BsonElementAttribute("gender")]
        public String gender {get;set;}

        [BsonElement("picture")]
        public String picture {get;set;}

        [BsonElementAttribute("salutation")]
        public String salutation {get;set;}

        [BsonElementAttribute("extension")]
        public String extension {get;set;}

        [BsonElementAttribute("addresses")]
        public List<address> addresses {get;set;}

        [BsonElement("religion")]
        public String religion {get;set;}

        [BsonElement("civilStatus")]
        public String civilStatus {get;set;}

        [BsonElement("contacts")]
        public List<contact> contacts {get;set;}

        [BsonElement("height")]
        public String height {get;set;}

        [BsonElement("weight")]
        public String weight {get;set;}

        [BsonElement("birthdate")]
        public DateTime birthdate {get;set;}

        [BsonElementAttribute("birthplace")]
        public String birthplace {get;set;}
        
        [BsonElementAttribute("congregation")]
        public String congregation {get;set;}

        [BsonElementAttribute("church")]
        public String church {get;set;}

        [BsonElementAttribute("skills")]
        public List<skill> skills {get;set;}

        [BsonElement("employment")]
        public employment employment {get;set;}
        
        [BsonElementAttribute("family")]
        public List<familyMember> family {get;set;}

        [BsonElementAttribute("trainings")]
        public List<training> trainings {get;set;}

        [BsonElementAttribute("educations")]
        public List<education> educations {get;set;}

        [BsonElementAttribute("previousEmployments")]
        public List<previousEmployment> previousEmployments {get;set;}
        
        [BsonElement("sss")]
        public String sss {get;set;}

        [BsonElement("pagibig")]
        public String pagibig {get;set;}

        [BsonElement("philhealth")]
        public String philhealth {get;set;}

        [BsonElement("taxCode")]
        public String taxCode {get;set;}

        [BsonElement("tin")]
        public String tin {get;set;}

        [BsonElementAttribute("employeeRelations")]
        public List<employeeRelation> employeeRelations {get;set;}


        [BsonElementAttribute("employeeSchedules")]
        public List<employeeSchedule> employeeSchedules {get;set;}

        [BsonElement("mothersMaidenName")]
        public mothersMaidenName mothersMaidenName {get;set;}

        [BsonElement("nickname")]
        public String nickname {get;set;}

        [BsonElementAttribute("emergencyContactPersons")]
        public List<emergencyContactPerson> emergencyContactPersons {get;set;}

        [BsonElementAttribute("languagesSpoken")]
        public List<language> languagesSpoken {get;set;}

        [BsonElementAttribute("employmentHistory")]
        public List<employmentHistoryItem> employmentHistory {get;set;}

        [BsonElementAttribute("bloodType")]
        public String bloodType {get;set;}

        [BsonElementAttribute("dateHired")]
        public DateTime dateHired {get;set;}

        [BsonElementAttribute("initials")]
        public String initials {get;set;}

        [BsonElementAttribute("leaves")]
        public leaves leaves {get;set;}

        [BsonElementAttribute("immediateSupervisor")]
        public String immediateSupervisor {get;set;}

        [BsonElement("createdAt")]
        public DateTime createdAt {get;set;}

        [BsonElement("updatedAt")]
        public DateTime updatedAt {get;set;}       

    }

    public class Name
    {
        [BsonElement("first")]
        [Required(ErrorMessage = "The First Name field is required.")]
        public string first { get; set; }

        [BsonElement("middle")]
        [Required(ErrorMessage = "The Middle Name field is required.")]
        public string middle { get; set; }

        [BsonElement("last")]
        [Required(ErrorMessage = "The Last Name field is required.")]
        public string last { get; set; }
    }

    public class nameSearch
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("fullname")]
        public string fullname { get; set; }

        [BsonElement("position")]
        public string position { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("isVerified")]
        public int? isVerified { get; set; }
    }


    public class emailSearch
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("fullname")]
        public string fullname { get; set; }

        [BsonElement("position")]
        public string position { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }
    }

    public class getBySlug
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("fullname")]
        public string fullname { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        public Name name { get; set; }

        [BsonElement("email")]
        public string email { get; set; }

        [BsonElement("isVerified")]
        public int? isVerified { get; set; }
    }

    public class auth
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("email")]
        public string email { get; set; }


        [BsonElement("oldEmail")]
        [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessage = "Please input valid E-mail Address")]
        public string oldEmail { get; set; }

        [BsonElement("newEmail")]
        [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessage = "Please input valid E-mail Address")]
        public string newEmail { get; set; }

        [BsonElement("oldPassword")]
        [StringLength(60, MinimumLength = 8, ErrorMessage = "Password must have a minimum length of 8 characters.")]
        public string oldPassword { get; set; }

        [BsonElement("newPassword")]
        [StringLength(60, MinimumLength = 8, ErrorMessage = "Password must have a minimum length of 8 characters.")]
        public string newPassword { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("isActive")]
        public int? isActive { get; set; }

        [BsonElement("isVerified")]
        public int? isVerified { get; set; }

        [BsonElement("verificationToken")]
        public string verificationToken { get; set; }

        [BsonElement("updatedAt")]
        public DateTime? updatedAt { get; set; }
    }

    public class credentials
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("token")]
        public string token { get; set; }

        [BsonElement("fullname")]
        public string fullname { get; set; }

        [BsonElement("email")]
        public string email { get; set; }       
    }

    public class getByPermission
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        public IList<string> permissions { get; set; }

    }


//hr
    public class address
    {
        [BsonElement("slug")]
        public String slug {get;set;}

        [BsonElement("type")]
        public String type {get;set;}

        [BsonElement("location")]
        public String location {get;set;}

        [BsonElement("createdAt")]
        public DateTime createdAt {get;set;}

        [BsonElement("updatedAt")]
        public DateTime updatedAt {get;set;}

    }

    public class contact
    {
        [BsonElement("slug")]
        public String slug {get;set;}

        [BsonElement("mobile")]
        public String mobile {get;set;}

        [BsonElement("landline")]
        public String landline {get;set;}

        [BsonElement("createdAt")]
        public DateTime createdAt {get;set;}

        [BsonElement("updatedAt")]
        public DateTime updatedAt {get;set;}

    }

    public class skill
    {
        [BsonElement("slug")]
        public String slug {get;set;}

        [BsonElement("type")]
        public String type {get;set;}

        [BsonElement("createdAt")]
        public DateTime createdAt {get;set;}

        [BsonElement("updatedAt")]
        public DateTime updatedAt {get;set;}

    }

    public class employment
    {
        [BsonElement("position")]
        public String position {get;set;}

        [BsonElement("group")]
        public String group {get;set;}

        [BsonElement("department")]
        public String department {get;set;}

        [BsonElement("division")]
        public String division {get;set;}

        [BsonElement("section")]
        public String section {get;set;}

        [BsonElement("region")]
        public String region {get;set;}

        [BsonElement("area")]
        public String area {get;set;}

        [BsonElement("branch")]
        public String branch {get;set;}

        [BsonElement("unit")]
        public String unit {get;set;}

        [BsonElementAttribute("jobGrade")]
        public String jobGrade{get;set;}

        [BsonElementAttribute("jobClassification")]
        public String jobClassification{get;set;}

        [BsonElementAttribute("jobCategory")]
        public String jobCategory{get;set;}

        [BsonElementAttribute("status")]
        public String status {get;set;}

        [BsonElementAttribute("effectivityDateFrom")]
        public DateTime effectivityDateFrom {get;set;}

        [BsonElementAttribute("effectivityDateTo")]
        public DateTime effectivityDateTo {get;set;}

        [BsonElementAttribute("salary")]
        public string salary {get;set;}

        [BsonElementAttribute("allowances")]
        public List<allowance> allowances {get;set;}

        [BsonElementAttribute("benefits")]
        public List<benefit> benefits {get;set;}

        [BsonElementAttribute("isHeadOffice")]
        public Boolean isHeadOffice{get;set;}
        
        [BsonElement("createdAt")]
        public DateTime createdAt {get;set;}

        [BsonElement("updatedAt")]
        public DateTime updatedAt {get;set;}

    }

    public class allowance
    {
        [BsonElement("slug")]
        public String slug {get;set;}

        [BsonElement("name")]
        public String name {get;set;}

        [BsonElement("amount")]
        public Double amount {get;set;}

        [BsonElement("temporary")]
        public Boolean temporary {get;set;}

        [BsonElement("effectivityDateFrom")]
        public DateTime effectivityDateFrom {get;set;}

        [BsonElement("effectivityDateTo")]
        public DateTime effectivityDateTo {get;set;}

        [BsonElement("createdAt")]
        public DateTime createdAt {get;set;}

        [BsonElement("updatedAt")]
        public DateTime updatedAt {get;set;}
    }

    public class benefit
    {
        [BsonElement("slug")]
        public String slug {get;set;}

        [BsonElement("name")]
        public String name {get;set;}

        [BsonElement("amount")]
        public Double amount {get;set;}

        [BsonElement("effectivityDateFrom")]
        public DateTime effectivityDateFrom {get;set;}

        [BsonElement("effectivityDateTo")]
        public DateTime effectivityDateTo {get;set;}
        
        [BsonElement("createdAt")]
        public DateTime createdAt {get;set;}

        [BsonElement("updatedAt")]
        public DateTime updatedAt {get;set;}
    }

    public class familyMember
    {
        [BsonElement("slug")]
        public String slug {get;set;}

        [BsonElement("last")]
        public String last {get;set;}

        [BsonElement("first")]
        public String first {get;set;}

        [BsonElement("middle")]
        public String middle {get;set;}

        [BsonElement("relationship")]
        public String relationship {get;set;}
        
        [BsonElement("birthdate")]
        public DateTime birthdate {get;set;}

        [BsonElement("occupation")]
        public String occupation {get;set;}

        [BsonElement("contact")]
        public String contact {get;set;}

        [BsonElement("dependents")]
        public IList<String> dependents {get;set;}

        [BsonElement("highestEducationalAttainment")]
        public String highestEducationalAttainment {get;set;}

        [BsonElement("createdAt")]
        public DateTime createdAt {get;set;}

        [BsonElement("updatedAt")]
        public DateTime updatedAt {get;set;}


    }

    public class training
    {
        [BsonElement("slug")]
        public String slug {get;set;}

        [BsonElement("title")]
        public String title {get;set;}

        [BsonElement("dateFrom")]
        public DateTime dateFrom {get;set;}

        [BsonElement("dateTo")]
        public DateTime dateTo {get;set;}

        [BsonElementAttribute("sponsor")]
        public String sponsor {get;set;}
        
        [BsonElement("venue")]
        public String venue {get;set;}

        [BsonElementAttribute("isResourceSpeaker")]
        public Boolean isResourceSpeaker {get;set;}

        [BsonElementAttribute("cost")]
        public double cost {get;set;}

        [BsonElementAttribute("personalCost")]
        public double personalCost {get;set;}

        [BsonElementAttribute("noOfHours")]
        public double noOfHours {get;set;}

        [BsonElementAttribute("holdingPeriod")]
        public Int32 holdingPeriod {get;set;}

        [BsonElementAttribute("holdingUntil")]
        public DateTime holdingUntil {get;set;}
        
        [BsonElementAttribute("documents")]
        public List<document> documents {get;set;}

        [BsonElement("createdAt")]
        public DateTime createdAt {get;set;}

        [BsonElement("updatedAt")]
        public DateTime updatedAt {get;set;}

    }

    public class education
    {
        [BsonElement("slug")]
        public String slug {get;set;}

        [BsonElement("educationType")]
        public String educationType {get;set;}

        [BsonElement("school")]
        public String school {get;set;}

        [BsonElement("address")]
        public String address {get;set;}

        [BsonElement("inclusiveDateFrom")]
        public Int32 inclusiveDateFrom {get;set;}

        [BsonElement("inclusiveDateTo")]
        public Int32 inclusiveDateTo {get;set;}

        [BsonElement("degree")]
        public String degree {get;set;}

        [BsonElement("major")]
        public String major {get;set;}

        [BsonElement("award")]
        public String award {get;set;}

        [BsonElement("notYetGraduated")]
        public Boolean notYetGraduated {get;set;}

        [BsonElement("noOfUnits")]
        public Int32 noOfUnits {get;set;}

        [BsonElement("yearLevel")]
        public Int32 yearLevel {get;set;}

        [BsonElementAttribute("documents")]
        public List<document> documents {get;set;}

        [BsonElement("createdAt")]
        public DateTime createdAt {get;set;}

        [BsonElement("updatedAt")]
        public DateTime updatedAt {get;set;}

    }

    public class previousEmployment
    {
        [BsonElement("slug")]
        public String slug {get;set;}

        [BsonElement("company")]
        public String company {get;set;}

        [BsonElement("address")]
        public String address {get;set;}

        [BsonElement("telNo")]
        public String telNo {get;set;}

        [BsonElement("inclusiveDateFrom")]
        public DateTime inclusiveDateFrom {get;set;}

        [BsonElement("inclusiveDateTo")]
        public DateTime inclusiveDateTo {get;set;}

        [BsonElement("position")]
        public String position {get;set;}

        [BsonElement("salaryStart")]
        public Double salaryStart{get;set;}

        [BsonElement("salaryEnd")]
        public Double salaryEnd{get;set;}

        [BsonElement("reasonOfSeverance")]
        public String reasonOfSeverance {get;set;}

        [BsonElementAttribute("immediateSupervisor")]
        public String immediateSupervisor{get;set;}

        [BsonElementAttribute("statusOfEmployment")]
        public String statusOfEmployment{get;set;}

        [BsonElementAttribute("jobFunction")]
        public String jobFunction{get;set;}

        [BsonElementAttribute("documents")]
        public List<document> documents {get;set;}

        [BsonElement("createdAt")]
        public DateTime createdAt {get;set;}

        [BsonElement("updatedAt")]
        public DateTime updatedAt {get;set;}

    }

    public class document
    {
        [BsonElement("slug")]
        public String slug {get;set;}

        [BsonElement("name")]
        public String name {get;set;}

        [BsonElement("filename")]
        public String filename {get;set;}

        [BsonElement("path")]
        public String path {get;set;}
        
        [BsonElement("description")]
        public String description {get;set;}

        [BsonElement("createdAt")]
        public DateTime createdAt {get;set;}

        [BsonElement("updatedAt")]
        public DateTime updatedAt {get;set;}

    }

    public class employeeRelation
    {
        [BsonElementAttribute("slug")]
        public String slug {get;set;}

        [BsonElementAttribute("docketNumber")]
        public String docketNumber {get;set;}

        [BsonElementAttribute("cocViolation")]
        public String cocViolation {get;set;}

        [BsonElementAttribute("natureOfOffense")]
        public String natureOfOffense {get;set;}

        [BsonElementAttribute("dateOfOffense")]
        public DateTime dateOfOffense {get;set;}

        [BsonElementAttribute("dateOfResolution")]
        public DateTime? dateOfResolution {get;set;}

        [BsonElementAttribute("sanction")]
        public String sanction {get;set;}

        [BsonElementAttribute("caseStatus")]
        public String caseStatus {get;set;}

        [BsonElementAttribute("documents")]
        public List<document> documents {get;set;}
        
        [BsonElementAttribute("createdAt")]
        public DateTime createdAt {get;set;}

        [BsonElementAttribute("updatedAt")]
        public DateTime updatedAt {get;set;}
    }

    public class employeeSchedule
    {
        [BsonElementAttribute("slug")]
        public String slug {get;set;}

        [BsonElementAttribute("timeInAm")]
        public String timeInAm {get;set;}

        [BsonElementAttribute("timeOutAm")]
        public String timeOutAm {get;set;}

        [BsonElementAttribute("timeInPm")]
        public String timeInPm {get;set;}

        [BsonElementAttribute("timeOutPm")]
        public String timeOutPm {get;set;}

        [BsonElementAttribute("effectivityDateFrom")]
        public DateTime effectivityDateFrom {get;set;}

        [BsonElementAttribute("effectivityDateTo")]
        public DateTime effectivityDateTo {get;set;}

        [BsonElementAttribute("createdAt")]
        public DateTime createdAt {get;set;}

        [BsonElementAttribute("updatedAt")]
        public DateTime updatedAt {get;set;}

        [BsonElementAttribute("isWorkFromHome")]
        public bool isWorkFromHome { get; set; }

        [BsonElementAttribute("isWorkFromBranch")]
        public bool isWorkFromBranch { get; set; }

        [BsonElementAttribute("days")]
        public string days { get; set; }

        [BsonElementAttribute("branchSlug")]
        public string branchSlug { get; set; }

        [BsonElementAttribute("isFlexibleTime")]
        public bool isFlexibleTime { get; set; }

        [BsonElement("flexible")]
        public flexible flexible {get;set;}
    }

    public class flexible
    {
        [BsonElementAttribute("timeIn")]
        public String timeIn {get;set;}

        [BsonElementAttribute("timeOut")]
        public String timeOut {get;set;}
    }

    public class emergencyContactPerson
    {
        [BsonElementAttribute("slug")]
        public String slug {get;set;}

        [BsonElementAttribute("contactPerson")]
        public String contactPerson {get;set;}

        [BsonElementAttribute("contactNumber")]
        public String contactNumber {get;set;}
        
        [BsonElementAttribute("address")]
        public String address {get;set;}

        [BsonElementAttribute("relationship")]
        public String relationship {get;set;}

        [BsonElementAttribute("createdAt")]
        public DateTime createdAt {get;set;}

        [BsonElementAttribute("updatedAt")]
        public DateTime updatedAt {get;set;}
    }

    public class language
    {
        [BsonElementAttribute("slug")]
        public String slug {get;set;}

        [BsonElementAttribute("name")]
        public String name {get;set;}

        [BsonElementAttribute("createdAt")]
        public DateTime createdAt {get;set;}

        [BsonElementAttribute("updatedAt")]
        public DateTime updatedAt {get;set;}
    }

    public class employmentHistoryItem : employment
    {
        [BsonElementAttribute("slug")]
        public String slug {get;set;}
    }

    public class mothersMaidenName 
    {
        [BsonElementAttribute("first")]
        public String first {get;set;}

        [BsonElementAttribute("last")]
        public String last {get;set;}

        [BsonElementAttribute("middle")]
        public String middle {get;set;}

        [BsonElementAttribute("fullname")]
        public String fullname {get;set;}
        
    }

    public class leaves{
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }
        
        public IEnumerable<types> types {get;set;}
    }

    public class types
    {
        [BsonElementAttribute("slug")]
        public String slug{get;set;}

        [BsonElementAttribute("type")]
        public String type{get;set;}

        [BsonElementAttribute("remaining")]
        public Double remaining {get;set;}

        [BsonElementAttribute("earning")]
        public Double earning {get;set;}
    }

    public class loginInfo
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string id { get; set; }
        public string token { get; set; }
        public string fullname { get; set; }
        public lEmployment employment { get; set; }
    }

    public class lEmployment
    {
        public string branch { get; set; }
    }
}
