using System;
using System.Dynamic;
using kmbi_core_master.helpers.interfaces;
using RazorLight;

namespace kmbi_core_master.helpers
{
    public class htmlBuilder : ihtmlBuilder {

        public String htmlBuild(String templatePath, String templateFilename, Object model, ExpandoObject viewBag)
        {
            String builder = "";
            // var eningex = EngineFactory.CreatePhysical(templatePath);
            // builder = eningex.Parse(templateFilename,model,viewBag);
            var engine = new EngineFactory();
            builder = engine.ForFileSystem(templatePath).CompileRenderAsync(templateFilename,model,typeof(object),viewBag).Result;
            return builder;
        }
    }
    
}