﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace kmbi_core_master.acctngModule.Models
{
    public class acctngEntity
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("slug")]
        public String slug { get; set; }

        [BsonElement("name")]
        public String name { get; set; }

        [BsonElement("tin")]
        public String tin { get; set; }

        [BsonElement("address")]
        public string address { get; set; }

        [BsonElement("type")]
        public string[] type { get; set; }

        [BsonElement("vatType")]
        public string vatType { get; set; }

        [BsonElement("ewtType")]
        public string ewtType { get; set; }

        [BsonElement("contacts")]
        public contacts[] contacts { get; set; }

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }
    }

    public class contacts
    {
        [BsonElement("name")]
        public String name { get; set; }

        [BsonElement("number")]
        public String number { get; set; }
    }
   
}
