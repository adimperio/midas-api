﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Filters;
using kmbi_core_master.hrModule.Interface;
using kmbi_core_master.hrModule.Models;
using Microsoft.AspNetCore.Cors;
using System;
using kmbi_core_master.acctngModule.Interface;
using kmbi_core_master.acctngModule.Models;
using kmbi_core_master.coreMaster;
using kmbi_core_master.coreMaster.IRepository;

namespace kmbi_core_master.acctngModule.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/coa")]
    [usersAuth]
    public class coaController : Controller
    {
        private readonly icoaRepository _Repository;
        private readonly iusersRepository _logInfo;
        private string token;

        public coaController(icoaRepository Repositor, iusersRepository logInfo)
        {
            this._Repository = Repositor;
            _logInfo = logInfo;
        }

        public void logger(string message)
        {
            //Insert Log//
            token = HttpContext.Request.Headers["token"];
            var lInfo = _logInfo.getLoginInfo(token);
            string log = message;
            global.createLog(lInfo.id, lInfo.fullname, lInfo.employment.branch, log);
            //Insert Log//
        }

        // GET: 
        [HttpGet]
        public IEnumerable<coa> GetAll()
        {
            var c = _Repository.all();
            return c;

        }

        //GET coa Slug
        [HttpGet("{slug}")]
        public IActionResult getSlug(string slug)
        {

            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

       

        [HttpPost]
        public IActionResult create([FromBody] coa b)
        {

            var chkCode = _Repository.checkParentCode(b.code);
            
            
            if (chkCode == null)
            {
                _Repository.add(b);

                logger("Insert new coa " + b.name); 

                return Ok(new
                {
                    type = "success",
                    message = "Item created successfully.",
                    slug = b.slug
                });
            }
            else
            {
                return Ok(new
                {
                    type = "warning",
                    message = b.code + " already exist."
                });
            }
            

        }

      
        [HttpPut("{slug}")]
        public IActionResult update(string slug, [FromBody] coa b)
        {
            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _Repository.update(slug, b);

            logger("Modified coa " + b.name);

            return Ok(new
            {
                type = "success",
                message = "Item updated successfully."
            });
        }

        

        [HttpDelete("{slug}")]
        public IActionResult delete(string slug)
        {

            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.remove(slug);

            logger("Delete coa " + item.name);

            return Ok(new
            {
                type = "success",
                message = "Item deleted successfully."
            });
        }
        
    }
}
