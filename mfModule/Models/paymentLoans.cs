﻿using kmbi_core_master.hrModule.Models;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.coreMaster.Models
{

    public class paymentLoans
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String id { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        public paymentLoansloan loan { get; set; }
        public paymentLoansMembers[] members { get; set; }
        public paymentLoansOfficialReceipt officialReceipt { get; set; }
        public paymentLoansCV cv { get; set; }
        public paymentLoansPayee payee { get; set; }

        [BsonElement("mode")]
        public string mode { get; set; }

        [BsonElement("remarks")]
        public string remarks { get; set; }

        [BsonElement("type")]
        public string type { get; set; }

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }
    }


    public class paymentLoansloan
    {
        [BsonElement("referenceId")]
        public string referenceId { get; set; }
        public paymentLoanBranch branch { get; set; }
        public paymentLoanCenter center { get; set; }
        public paymentLoanBank bank { get; set; }
    }

    public class paymentLoanBranch
    {
        [BsonElement("id")]
        public string id { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("name")]
        public string name { get; set; }
    }

    public class paymentLoanCenter
    {
        [BsonElement("id")]
        public string id { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("code")]
        public string code { get; set; }
    }

    public class paymentLoanBank
    {
        [BsonElement("id")]
        public string id { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("name")]
        public string name { get; set; }
    }

    public class paymentLoansMembers
    {
        [BsonElement("slug")]
        public string slug { get; set; }

        public paymentLoansName name { get; set; }
        public paymentLoansAmounts amounts { get; set; }
        public paymentLoanTermsMembers[] terms { get; set; }

    }

    public class paymentLoansName
    {
        [BsonElement("first")]
        public string first { get; set; }

        [BsonElement("middle")]
        public string middle { get; set; }

        [BsonElement("last")]
        public string last { get; set; }

        [BsonElement("fullname")]
        public string fullname { get; set; }
    }

    public class paymentLoansAmounts
    {
        [BsonElement("principal")]
        public double principal { get; set; }

        [BsonElement("interest")]
        public double interest { get; set; }

        [BsonElement("cbu")]
        public double cbu { get; set; }

        [BsonElement("total")]
        public double total { get; set; }
    }

    public class paymentLoanTermsMembers
    {
        public int term { get; set; }
        public paymentLoansAmountMembers amounts { get; set; }
    }

    public class paymentLoansAmountMembers
    {
        [BsonElement("principal")]
        public double principal { get; set; }

        [BsonElement("interest")]
        public double interest { get; set; }

        [BsonElement("cbu")]
        public double cbu { get; set; }

        [BsonElement("total")]
        public double total { get; set; }
    }

    public class paymentLoansOfficialReceipt
    {
        [BsonElement("number")]
        public string number { get; set; }

        public paymentLoansAmountOR amounts { get; set; }

        [BsonElement("paidAt")]
        public DateTime paidAt { get; set; }
    }

    public class paymentLoansAmountOR
    {
        [BsonElement("principal")]
        public double principal { get; set; }

        [BsonElement("interest")]
        public double interest { get; set; }

        [BsonElement("cbu")]
        public double cbu { get; set; }

        [BsonElement("total")]
        public double total { get; set; }
    }

    public class paymentLoansCV
    {
        [BsonElement("number")]
        public string number { get; set; }

        public paymentLoansAmountCV amounts { get; set; }

        [BsonElement("paidAt")]
        public DateTime paidAt { get; set; }
    }

    public class paymentLoansAmountCV
    {
        [BsonElement("principal")]
        public double principal { get; set; }

        [BsonElement("interest")]
        public double interest { get; set; }

        [BsonElement("cbu")]
        public double cbu { get; set; }

        [BsonElement("total")]
        public double total { get; set; }
    }

    public class paymentLoansPayee
    {
        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("address")]
        public string address { get; set; }
    }

    public class syncPaymentLoans
    {
        [BsonId]
        public ObjectId id { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        public paymentLoansloan loan { get; set; }
        public paymentLoansMembers[] members { get; set; }
        public paymentLoansOfficialReceipt officialReceipt { get; set; }
        public paymentLoansCV cv { get; set; }
        public paymentLoansPayee payee { get; set; }

        [BsonElement("mode")]
        public string mode { get; set; }

        [BsonElement("remarks")]
        public string remarks { get; set; }

        [BsonElement("type")]
        public string type { get; set; }

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }
    }

    public class paymentLoansWithSched
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String id { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        public paymentLoansloan loan { get; set; }
        public paymentLoansMembers[] members { get; set; }
        public paymentLoansOfficialReceipt officialReceipt { get; set; }
        public paymentLoansCV cv { get; set; }
        public paymentLoansPayee payee { get; set; }

        [BsonElement("mode")]
        public string mode { get; set; }

        [BsonElement("remarks")]
        public string remarks { get; set; }

        [BsonElement("type")]
        public string type { get; set; }

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }

        [BsonElement("schedules")]
        public List<schedule> schedules { get; set; }
    }

    public class loansWihSched

    {
        [BsonElement("members")]
        public loanMembersWithSched members { get; set; }

    }

    public class loanMembersWithSched

    {
        [BsonElement("schedule")]
        public List<withSchedule> schedule { get; set; }
        
    }

    public class withSchedule
    {
        //[BsonId]
        //[BsonRepresentation(BsonType.ObjectId)]
        //public String Id { get; set; }

        [BsonElement("weekNo")]
        public int weekNo { get; set; }

        //[BsonElement("interestRate")]
        //public double interestRate { get; set; }

        [BsonElement("collectionDate")]
        public DateTime collectionDate { get; set; }

        //[BsonElement("paidAt")]
        //public DateTime? paidAt { get; set; }

        //[BsonElement("principalBalance")]
        //public double principalBalance { get; set; }

        //[BsonElement("interestBalance")]
        //public double interestBalance { get; set; }

        //[BsonElement("cbuBalance")]
        //public double cbuBalance { get; set; }

        //[BsonElement("currentPrincipalDue")]
        //public double currentPrincipalDue { get; set; }

        //[BsonElement("currentInterestDue")]
        //public double currentInterestDue { get; set; }

        //[BsonElement("currentCbuDue")]
        //public double currentCbuDue { get; set; }

        //[BsonElement("pastPrincipalDue")]
        //public double pastPrincipalDue { get; set; }

        //[BsonElement("pastInterestDue")]
        //public double pastInterestDue { get; set; }

        //[BsonElement("pastCbuDue")]
        //public double pastCbuDue { get; set; }

        //[BsonElement("payments")]
        //public IList<string> payments { get; set; }

        //[BsonElement("remarks")]
        //public string remarks { get; set; }

        //[BsonElement("createdAt")]
        //public DateTime createdAt { get; set; }

        //[BsonElement("updatedAt")]
        //public DateTime updatedAt { get; set; }
    }

}
