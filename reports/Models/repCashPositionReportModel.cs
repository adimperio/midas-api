using System;
using kmbi_core_master.coreMaster.Models;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;


namespace kmbi_core_master.reports.Models
{
    public class repCashPositionReportModel
    {
        public String bankAccount {get;set;}
        public String accountNo {get;set;}
        public Double begBal {get;set;}
        public Double receipts {get;set;}
        public Double disbursements {get;set;}
        public Double endBal {get;set;}
        public string asof { get; set; }
    }
}