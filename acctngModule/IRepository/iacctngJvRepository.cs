﻿using kmbi_core_master.acctngModule.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.acctngModule.Interface
{
    public interface iacctngJvRepository
    {
        IEnumerable<acctngJv> all();

        IEnumerable<acctngJv> all(DateTime from, DateTime to);

        acctngJv getSlug(String slug);

        void add(acctngJv b);

        void update(String slug, acctngJv b);

        void remove(String slug);

        List<acctngJv> issuedAt(DateTime from, DateTime to);

        //CREDIT
        jvAccounts getCreditId(string creditId);

        void addCredit(jvCredit u, string jvSlug);

        void updateCredit(String jvSlug, string creditId, jvCredit u);

        void removeCredit(String jvSlug, string creditId);


        //DEBIT
        acctngJv getDebitId(string debitId);

        void addDebit(jvDebit u, string jvSlug);

        void updateDebit(String jvSlug, string debitId, jvDebit u);

        void removeDebit(String jvSlug, string debitId);

        //Check existing receiptNo
        acctngJv checkOr(String orNo);

        void updateIsCancelled(String slug, acctngJv b);

        void updateIsClosed(DateTime from, DateTime to, acctngJv b);
    }
}
