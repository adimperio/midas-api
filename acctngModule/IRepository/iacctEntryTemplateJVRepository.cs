﻿using kmbi_core_master.acctngModule.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.acctngModule.Interface
{
    public interface iacctEntryTemplateJVRepository
    {
        IEnumerable<acctEntryTemplateJV> all();

        acctEntryTemplateJV getSlug(String slug);

        void add(acctEntryTemplateJV b);

        void update(String slug, acctEntryTemplateJV b);

        void remove(String slug);
    }
}
