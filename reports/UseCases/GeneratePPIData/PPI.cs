using System;
using kmbi_core_master.spm.Models;

namespace kmbi_core_master.reports.UseCases.GeneratePPIData
{
    // UseCase Models
    public class PPI {
        public String Name {get;set;}
        public String Branch {get;set;}
        public String Center {get;set;}
        public PPIQuestionAnswer[] QuestionsAnswers {get;set;}
        public Double Score {get;set;}
        public Likelihood Likelihood {get;set;}
    }


}