﻿

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

using kmbi_core_master.coreMaster;
using kmbi_core_master.coreMaster.Filters;
using kmbi_core_master.payrollModule.IRepository;
using kmbi_core_master.payrollModule.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System;
using kmbi_core_master.coreMaster.IRepository;

namespace kmbi_core_master.payrollModule.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/keys")]
    [usersAuth]
    public class keysController : Controller
    {
        private readonly ikeysRepository _Repository;
        private readonly iusersRepository _logInfo;
        private string token;

        public keysController(ikeysRepository Repository, iusersRepository logInfo)
        {
            this._Repository = Repository;
            _logInfo = logInfo;
        }

        public void logger(string message)
        {
            //Insert Log//
            token = HttpContext.Request.Headers["token"];
            var lInfo = _logInfo.getLoginInfo(token);
            string log = message;
            global.createLog(lInfo.id, lInfo.fullname, lInfo.employment.branch, log);
            //Insert Log//
        }

        // GET: 
        [HttpGet]
        public IEnumerable<keys> GetAll()
        {
            var p = _Repository.getAll();
            return p;

        }

        // GET id
        [HttpGet("{name}")]
        public IActionResult Get(string name)
        {
            var item = _Repository.Get(name);
            if (item == null)
            {

                return Ok(new { });
            }

            return new ObjectResult(item);
        }




        // PUT 
        [HttpPut("{name}")]
        public IActionResult Put(string name, [FromBody] keys p)
        {
            var item = _Repository.Get(name);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.Update(name, p);

            logger("Modified keys for " + p.name);

            return Ok(new
            {
                type = "success",
                message = p.name + " updated successfully."
            });
        }


        [HttpPost]
        public IActionResult checkKeyPassword([FromQuery] string usersSlug, [FromBody] checkKeyPassword kp)
        {
            var itemKey = _Repository.Get("payroll");
            var itemPassword = _Repository.getPassword(usersSlug);
            var key = itemKey.value;
            var pw = itemPassword.password;
            bool validPassword = BCrypt.Net.BCrypt.Verify(kp.password, pw);

            //KEY NOT FOUND
            if (itemKey == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "Key not found."
                });
            }

            //SLUG NOT FOUND
            if (itemPassword == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "User's slug not found."
                });
            }

            //INVALID KEY
            if (key != kp.value && validPassword)
            {
                return Ok(new
                {
                    type = "error",
                    message = "Payroll Key does not match."
                });
            }

            //INVALID PASSWORD
            if (!validPassword && key == kp.value)
            {
                return Ok(new
                {
                    type = "error",
                    message = "Password does not match."
                });
            }

            //INVALID BOTH
            if (!validPassword && key != kp.value)
            {
                return Ok(new
                {
                    type = "error",
                    message = "Payroll Key and Password does not match."
                });
            }

            //SUCCESS
            //if (validPassword && key == kp.value)
            //{
            return Ok(new
            {
                type = "success",
                message = "Access Granted."
            });
            //}
        }


        [HttpPost("checkPayrollKey")]
        public IActionResult checkPayrollKey([FromBody] checkKeyPassword kp)
        {
            var itemKey = _Repository.Get("payroll");
            var key = itemKey.value;

            //KEY NOT FOUND
            if (itemKey == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "Key not found."
                });
            }

           
            //INVALID KEY
            if (key != kp.value)
            {
                return Ok(new
                {
                    type = "error",
                    message = "Payroll Key does not match."
                });
            }


            //SUCCESS
            //if (validPassword && key == kp.value)
            //{
            return Ok(new
            {
                type = "success",
                message = "Access Granted."
            });
            //}
        }

    }

    
}
