﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace kmbi_core_master.coreMaster.Models
{
    public class deletions
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        public string collection { get; set; }
        public string slug { get; set; }
        public string branchSlug { get; set; }
        public DateTime deletedAt { get;set;}
    }

    public class syncDeletions
    {
        [BsonId]
        public ObjectId Id { get; set; }
        public string collection { get; set; }
        public string slug { get; set; }
        public string branchSlug { get; set; }
        public DateTime deletedAt { get; set; }
    }
}
