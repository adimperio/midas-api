﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.ComponentModel.DataAnnotations;

namespace kmbi_core_master.coreMaster.Models
{
    public class resetPassword
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("email")]
        [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessage = "Please input valid E-mail Address")]
        public string email { get; set; }

        [BsonElement("token")]
        public string token { get; set; }

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }
    }
}
