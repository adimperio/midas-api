﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Filters;
using Microsoft.AspNetCore.Cors;
using kmbi_core_master.acctngModule.Interface;
using kmbi_core_master.acctngModule.Models;
using kmbi_core_master.coreMaster;
using System;
using kmbi_core_master.coreMaster.IRepository;

namespace kmbi_core_master.acctngModule.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/acctngCv")]
    [usersAuth]
    public class acctngCvController : Controller
    {
        private readonly iacctngCvRepository _Repository;
        private readonly iusersRepository _logInfo;
        private string token;

        public acctngCvController(iacctngCvRepository Repositor, iusersRepository logInfo)
        {
            this._Repository = Repositor;
            _logInfo = logInfo;
        }

        public void logger(string message)
        {
            //Insert Log//
            token = HttpContext.Request.Headers["token"];
            var lInfo = _logInfo.getLoginInfo(token);
            string log = message;
            global.createLog(lInfo.id, lInfo.fullname, lInfo.employment.branch, log);
            //Insert Log//
        }

        // GET: 
        [HttpGet]
        public IEnumerable<acctngCv> GetAll()
        {
            var c = _Repository.all();
            return c;

        }

        //GET cv Slug
        [HttpGet("{slug}")]
        public IActionResult getSlug(string slug)
        {

            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        [HttpGet("list")]
        public IActionResult issuedAt([FromQuery]DateTime dateFrom, [FromQuery]DateTime dateTo)
        {
            var dt = dateTo.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
            var item = _Repository.issuedAt(dateFrom, dt);
            if (item.Count == 0)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        [HttpPost]
        public IActionResult create([FromBody] acctngCv b)
        {

            _Repository.add(b);

            logger("Insert new cv " + b.cvNumber);

            return Ok(new
            {
                type = "success",
                message = b.particulars + " created successfully.",
                slug = b.slug
            });

        }

        // POST credit
        [HttpPost("insertCredit/{crSlug}")]
        public IActionResult addCredit([FromBody] cvCredit u, string crSlug)
        {
            _Repository.addCredit(u, crSlug);

            logger("Insert new cv credit " + u.code);

            return Ok(new
            {
                type = "success",
                message = u.name + " created successfully.",
                id = u.Id
            });
        }

        // POST debit
        [HttpPost("insertDebit/{crSlug}")]
        public IActionResult addDebit([FromBody] cvDebit u, string crSlug)
        {
            _Repository.addDebit(u, crSlug);

            logger("Insert new debit " + u.name);

            return Ok(new
            {
                type = "success",
                message = u.name + " created successfully.",
                id = u.Id
            });
        }

        [HttpPut("{slug}")]
        public IActionResult update(string slug, [FromBody] acctngCv b)
        {
            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _Repository.update(slug, b);

            logger("Modified cv " + b.cvNumber);

            return Ok(new
            {
                type = "success",
                message = b.particulars + " updated successfully."
            });
        }

        // PUT credit
        [errorHandler]
        [HttpPut("updateCredit/crSlug={crSlug}&creditId={creditId}")]
        public IActionResult updateCredit(string crSlug, string creditId, [FromBody] cvCredit u)
        {
            var item = _Repository.getCreditId(creditId);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _Repository.updateCredit(crSlug, creditId, u);

           logger("Modified credit " + u.name);

            return Ok(new
            {
                type = "success",
                message = u.name + " updated successfully."
            });

        }

        // PUT debit
        [errorHandler]
        [HttpPut("updateDebit/crSlug={crSlug}&debitId={debitId}")]
        public IActionResult updateDebit(string crSlug, string debitId, [FromBody] cvDebit u)
        {
            var item = _Repository.getDebitId(debitId);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _Repository.updateDebit(crSlug, debitId, u);

            logger("Modified debit " + u.name);

            return Ok(new
            {
                type = "success",
                message = u.name + " updated successfully."
            });

        }

        [HttpDelete("{slug}")]
        public IActionResult delete(string slug)
        {

            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.remove(slug);

            logger("Delete cv " + item.cvNumber);

            return Ok(new
            {
                type = "success",
                message = item.particulars + " deleted successfully."
            });
        }

        //DELETE credit
        [HttpDelete("removeCredit/crSlug={crSlug}&creditId={creditId}")]
        public IActionResult deleteCredit(string crSlug, string creditId)
        {
            var item = _Repository.getCreditId(creditId);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.removeCredit(crSlug, creditId);

           logger( "Delete credit " + item.credit[0].name);

            return Ok(new
            {
                type = "success",
                message = item.credit[0].name + " deleted successfully."
            });
        }


        //DELETE debit
        [HttpDelete("removeDebit/crSlug={crSlug}&debitId={debitId}")]
        public IActionResult deleteDebit(string crSlug, string debitId)
        {
            var item = _Repository.getDebitId(debitId);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.removeDebit(crSlug, debitId);

            logger( "Delete debit " + item.accounts.debit[0].name);

            return Ok(new
            {
                type = "success",
                message = item.accounts.debit[0].name + " deleted successfully."
            });
        }


        [HttpGet("checkCvNo/{cvNo}")]
        public IActionResult checkCv(string cvNo)
        {

            var item = _Repository.checkCv(cvNo);
            if (item == null)
            {
                return Ok(new
                {
                    existing = 0
                });
            }

            return Ok(new
            {
                existing = 1
            });
        }


        [HttpGet("checkChequeNo/{chequeNo}")]
        public IActionResult checkCheque(string chequeNo)
        {

            var item = _Repository.checkCheque(chequeNo);
            if (item == null)
            {
                return Ok(new
                {
                    existing = 0
                });
            }

            return Ok(new
            {
                existing = 1
            });
        }


        [HttpPut("cancelled/{slug}")]
        public IActionResult updateIsCancelled(string slug, [FromBody] acctngCv b)
        {
            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _Repository.updateIsCancelled(slug, b);
            return Ok(new
            {
                type = "success",
                message = b.particulars + " cancelled successfully."
            });
        }


        [HttpPut("closed")]
        public IActionResult updateIsClosed([FromQuery]DateTime dateFrom, [FromQuery]DateTime dateTo, [FromBody] acctngCv b)
        {
            var dt = dateTo.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
            _Repository.updateIsClosed(dateFrom, dateTo, b);
            return Ok(new
            {
                type = "success",
                message = b.particulars + " closed successfully."
            });
        }
    }
}
