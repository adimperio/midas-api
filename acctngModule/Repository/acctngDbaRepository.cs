﻿using Microsoft.Extensions.Options;

using System;
using System.Collections.Generic;
using System.Linq;

using MongoDB.Bson;
using MongoDB.Driver;

using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.acctngModule.Models;
using kmbi_core_master.acctngModule.Interface;

namespace kmbi_core_master.acctngModule.Repository
{
    public class acctngDbaRepository : dbCon, iacctngDbaRepository
    {
        
        public acctngDbaRepository(IOptions<dbSettings> settings) : base(settings) {}

        public void add(acctngDba b)
        {

            b.timestamps.closedAt = DateTime.Now;
            var coll = _db.GetCollection<acctngDba>("acctngDba");
            _db.GetCollection<acctngDba>("acctngDba").InsertOneAsync(b);
        }
        

        public IEnumerable<acctngDba> all()
        {
            var c = _db.GetCollection<acctngDba>("acctngDba").Find(new BsonDocument()).ToListAsync();
            return c.Result;
        }

        public acctngDba getSlug(String slug)
        {
            var query = Builders<acctngDba>.Filter.Eq(e => e.slug, slug);
            var c = _db.GetCollection<acctngDba>("acctngDba").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }
       
        public void update(String slug, acctngDba p)
        {
            p.slug = slug;
            var query = Builders<acctngDba>.Filter.Eq(e => e.slug, slug);
            var update = Builders<acctngDba>.Update
                .Set("status", p.status)
                .Set("timestamps.closedAt", p.timestamps.closedAt)
                .Set("timestamps.closedBy", p.timestamps.closedBy)
                .Set("perAccount", p.perAccount);

            var cr = _db.GetCollection<acctngDba>("acctngDba").UpdateOneAsync(query, update);

        }
        public void remove(String slug)
        {
            var query = Builders<acctngDba>.Filter.Eq(e => e.slug, slug);
            var result = _db.GetCollection<acctngDba>("acctngDba").DeleteOneAsync(query);
        }

        
    }
}
