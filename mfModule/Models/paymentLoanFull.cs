﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace kmbi_core_master.mfModule.Models
{
    public class paymentLoanFull
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string id { get; set; }
        public string slug { get; set; }
        public loan loan { get; set; }
        public terms[] terms { get; set; }
        public member member { get; set; }
        public officialReceipt officialReceipt { get; set; }
        public payee payee { get; set; }
        public string reason { get; set; }
        public string mode { get; set; }
        public string remarks { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
    }

    public class loan
    {
        public string referenceId { get; set; }
        public branch branch { get; set; }
        public center center { get; set; }
        public bank bank { get; set; }
    }

    public class branch
    {
        public string id { get; set; }
        public string slug { get; set; }
        public string name { get; set; }
    }

    public class center
    {
        public string id { get; set; }
        public string slug { get; set; }
        public string code { get; set; }
    }

    public class bank
    {
        public string id { get; set; }
        public string slug { get; set; }
        public string name { get; set; }
    }

    public class member
    {
        public string slug { get; set; }
        public name name { get; set; }
    }

    public class name
    {
        public string first { get; set; }
        public string middle { get; set; }
        public string last { get; set; }
        public string fullname { get; set; }
    }

    public class officialReceipt
    {
        public string number { get; set; }
        public amounts amounts { get; set; }
        public DateTime paidAt { get; set; }
    }

    public class amounts
    {
        public double principal { get; set; }
        public double interest { get; set; }
        public double cbu { get; set; }
        public double total { get; set; }
    }

    public class payee
    {
        public string name { get; set; }
        public string address { get; set; }
    }

    public class terms
    {
        public int weekNo { get; set; }
        public termAmounts amounts { get; set; }
    }

    public class termAmounts
    {
        public double principal { get; set; }
        public double interest { get; set; }
        public double cbu { get; set; }
        public double total { get; set; }
    }
}
