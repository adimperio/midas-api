﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.adminModule.Inv.Models
{
    public class inventory
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("referenceNumber")]
        public string referenceNumber { get; set; }

        [BsonElement("itemSlug")]
        public string itemSlug { get; set; }

        [BsonElement("serialNo")]
        public string serialNo { get; set; }

        [BsonElement("location")]
        public string location { get; set; }

        [BsonElement("purchase")]
        public invPurchase purchase { get; set; }

        [BsonElement("photo")]
        public string photo { get; set; }

        [BsonElement("amount")]
        public double amount { get; set; }

        [BsonElement("unitOfMeasure")]
        public string unitOfMeasure { get; set; }

        [BsonElement("quantity")]
        public int quantity { get; set; }

        [BsonElement("status")]
        public string status { get; set; }

        [BsonElement("supplierName")]
        public string supplierName { get; set; }

        [BsonElement("remarks")]
        public string remarks { get; set; }

        [BsonElement("history")]
        public List<invHistory> history { get; set; }

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }

        [BsonElement("createdBy")]
        public string createdBy { get; set; }

        [BsonElement("updatedBy")]
        public string updatedBy { get; set; }
    }

    public class invPurchase
    {
        [BsonElement("orderNumber")]
        public string orderNumber { get; set; }

        [BsonElement("deliveryNumber")]
        public string deliveryNumber { get; set; }

        [BsonElement("acquisitionDate")]
        public DateTime acquisitionDate { get; set; }

        [BsonElement("purchaseDate")]
        public DateTime purchaseDate { get; set; }
    }

    public class invHistory
    {
        [BsonElement("pcrNumber")]
        public string pcrNumber { get; set; }

        [BsonElement("employeeSlug")]
        public string employeeSlug { get; set; }

        [BsonElement("branchSlug")]
        public string branchSlug { get; set; }

        [BsonElement("dateFrom")]
        public DateTime dateFrom { get; set; }

        [BsonElement("dateTo")]
        public DateTime dateTo { get; set; }

        [BsonElement("untilPresent")]
        public Boolean untilPresent { get; set; }
    }
}
