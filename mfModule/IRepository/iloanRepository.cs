﻿using kmbi_core_master.coreMaster.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.coreMaster.IRepository
{
    public interface iloanRepository
    {
        IEnumerable<loans> allLoans();

        loans getSlug(String slug);

        loans getId(String id);

        IEnumerable<getbyBranchloans> getBranchId(string branchId);

        IEnumerable<getLoansByBranch> getLoansByBranchId(string branchId);

        IEnumerable<getbyBranchloans> getCenterSlug(string centerSlug);

        IEnumerable<getbyBranchCenterloans> getLoanByCenter(string centerSlug);

        getloanTypes getName(String name);

        void add(loans b);

        void update(String id, loans b);

        void updateSlug(String slug, loans b);

        void remove(String id);

        void removeSlug(String slug);

        void updateStatus(String id, loans b);

        void updateCenterLoanCycle(String slug, loans b);

        void getNewSchedule(string refId);

        void updateCbuInterest(List<string> p, string type, DateTime transactedAt, string loanId);

        List<loans> getClientSlug(String slug);

        void changeHealth(string refId, String memberSlug, loanMembers b);

        List<loanSched> collectionDate(DateTime collDate, string branchSlug);

        List<loans> disbDate(DateTime disbDate, string branchSlug);

        List<loans> dayWeek(int dayOfWeek, string branchSlug);

        List<loans> fDate(DateTime fDate, string branchSlug);

        List<loans> lastPDate(DateTime pDate);

        List<loans> getByStatus(String branchId, string status);

        List<loans> collectionDateRaw(DateTime collDate, string branchSlug);

        List<loans> getByCenterLoanType(String centerSlug, string loanTypeId, string status);

        //List<List<loans>> getByClientSlugs(List<string> clientSlugs, string status);

        List<arrayLoans> getByClientSlugs(List<string> clientSlugs, string status);

        List<loanBalances> getBalances(List<string> clientSlugs, string status);

        loanPortfolio getLoanPortfoliobyCenter(string centerSlug);

        loanPortfolio getLoanPortfoliobyUnit(string unitSlug);

        loanPortfolio getLoanPortfoliobyPo(string poSlug);

        List<loans> getByTypeBranchStatus(string type, string branch, string status);

        List<loansByTypeBranchStatus> getLoansByBranchTypeStatus(string type, string branch, string status);

        List<loans> getMatured(DateTime date, string branchSlug);

        List<loans> getAllforPayment(List<string> clientSlugs);

        void add(loans[] loans);

        List<loans> getActiveLoans();

        List<loans> getActiveLoans(DateTime from, DateTime to);

        List<loansECR> getLoansAndPayments(string loanSlug);

        IEnumerable<misIntegration> getMonthlyReport(string branchSlug, Int32 year, Int32 month);
    }
}
