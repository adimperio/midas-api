﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.coreMaster.Models
{

    public class checkVoucher

    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("referenceId")]
        public string referenceId { get; set; }

        [BsonElement("branchSlug")]
        public string branchSlug { get; set; }

        [BsonElement("centerSlug")]
        public string centerSlug { get; set; }

        [BsonElement("rfp")]
        public IList<rfp> rfp { get; set; }

        [BsonElement("cv")]
        public IList<cv> cv { get; set; }        

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }
    }


    public class rfp
    {
        [BsonElement("number")]
        public string number { get; set; }

        [BsonElement("members")]
        public IList<rfpMembers> members { get; set; }

        [BsonElement("payee")]
        public string payee { get; set; }

        [BsonElement("particulars")]
        public string particulars { get; set; }

        [BsonElement("loanAmount")]
        public double loanAmount { get; set; }

        [BsonElement("datePrepared")]
        public DateTime? datePrepared { get; set; }
        
        [BsonElement("totalAmount")]
        public double totalAmount { get; set; }
    }

    public class cv
    {
        [BsonElement("number")]
        public string number { get; set; }

        [BsonElement("controlNumber")]
        public string controlNumber { get; set; }

        [BsonElement("payee")]
        public string payee { get; set; }

        [BsonElement("particulars")]
        public string particulars { get; set; }

        [BsonElement("members")]
        public IList<cvMembers> members { get; set; }

        [BsonElement("loanAmount")]
        public double loanAmount { get; set; }

        [BsonElement("chequeNumber")]
        public string chequeNumber { get; set; }

        [BsonElement("datePrepared")]
        public DateTime? datePrepared { get; set; }

        [BsonElement("bank")]
        public cvBank bank { get; set; }

        [BsonElement("totalAmount")]
        public double totalAmount { get; set; }
    }

    //public class check
    //{
    //    [BsonElement("number")]
    //    public string number { get; set; }

    //    [BsonElement("amount")]
    //    public double amount { get; set; }
    //}

    public class rfpMembers
    {
        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("loanAmount")]
        public double loanAmount { get; set; }
    }

    public class cvMembers
    {
        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("loanAmount")]
        public double loanAmount { get; set; }
    }

    public class cvBank
    {
        [BsonElement("id")]
        public string id { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("name")]
        public string name { get; set; }
    }

    public class syncCheckVoucher

    {
        [BsonId]
        public ObjectId Id { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("referenceId")]
        public string referenceId { get; set; }

        [BsonElement("branchSlug")]
        public string branchSlug { get; set; }

        [BsonElement("centerSlug")]
        public string centerSlug { get; set; }

        [BsonElement("rfp")]
        public IList<rfp> rfp { get; set; }

        [BsonElement("cv")]
        public IList<cv> cv { get; set; }

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }
    }

}
