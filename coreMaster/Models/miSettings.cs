﻿using System;

namespace kmbi_core_master.coreMaster.Models
{
    public class miSettings
    {
        public miSettingsSet[] sets {get;set;}
    }

    public class miSettingsSet {
        public string name {get;set;}
        public int loanCycleFrom {get;set;}
        public int loanCycleTo {get;set;}
        public int ageFrom {get;set;}
        public int ageTo {get;set;}
        public double life {get;set;}
        public double admin {get;set;}
        public double hib {get;set;}
        public double pai {get;set;}
    }

}
