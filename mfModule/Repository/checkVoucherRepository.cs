﻿using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Models;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace kmbi_core_master.coreMaster.Repository
{
    public class checkVoucherRepository : dbCon, icheckVoucherRepository
    {
        public checkVoucherRepository(IOptions<dbSettings> settings) : base(settings)
        {
        }

        public void add(checkVoucher b)
        {

            b.createdAt = DateTime.Now;
            b.updatedAt = DateTime.Now;


            _db.GetCollection<checkVoucher>("checkVoucher").InsertOneAsync(b);
        }

        public IEnumerable<checkVoucher> all()
        {
            var c = _db.GetCollection<checkVoucher>("checkVoucher").Find(new BsonDocument()).ToListAsync();
            return c.Result;
        }

        public checkVoucher getSlug(String slug)
        {
            var query = Builders<checkVoucher>.Filter.Eq(e => e.slug, slug);
            var c = _db.GetCollection<checkVoucher>("checkVoucher").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }

        public checkVoucher getId(String id)
        {
            var query = Builders<checkVoucher>.Filter.Eq(e => e.Id, id);
            var c = _db.GetCollection<checkVoucher>("checkVoucher").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }


        public checkVoucher getRefId(String refId)
        {
            var query = Builders<checkVoucher>.Filter.Eq(e => e.referenceId, refId);
            var c = _db.GetCollection<checkVoucher>("checkVoucher").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }


        public void update(String id, checkVoucher p)
        {
            p.Id = id;
            var query = Builders<checkVoucher>.Filter.Eq(e => e.Id, id);
            var update = Builders<checkVoucher>.Update
                .Set("referenceId", p.referenceId)
                .Set("branchSlug", p.branchSlug)
                .Set("centerSlug", p.centerSlug)
                .Set("rfp", p.rfp)
                .Set("cv", p.cv)
                .CurrentDate("updatedAt");

            var l = _db.GetCollection<checkVoucher>("checkVoucher").UpdateOneAsync(query, update);

        }

        public void updateSlug(String slug, checkVoucher p)
        {
            // p.slug = slug;
            var query = Builders<checkVoucher>.Filter.Eq(e => e.slug, slug);
            var update = Builders<checkVoucher>.Update
                .Set("referenceId", p.referenceId)
                .Set("branchSlug", p.branchSlug)
                .Set("centerSlug", p.centerSlug)
                .Set("rfp", p.rfp)
                .Set("cv", p.cv)
                .CurrentDate("updatedAt");

            var l = _db.GetCollection<checkVoucher>("checkVoucher").UpdateOneAsync(query, update);

        }


        public void remove(String id)
        {
            var query = Builders<checkVoucher>.Filter.Eq(e => e.Id, id);
            var result = _db.GetCollection<checkVoucher>("checkVoucher").DeleteOneAsync(query);
        }


        public void removeSlug(String slug)
        {
            var query = Builders<checkVoucher>.Filter.Eq(e => e.slug, slug);
            var result = _db.GetCollection<checkVoucher>("checkVoucher").DeleteOneAsync(query);
        }


        public void addRfp(rfp u, string cvSlug)
        {
            var query = Builders<checkVoucher>.Filter.Eq(e => e.slug, cvSlug);
            var update = Builders<checkVoucher>.Update.Push(e => e.rfp, u);
            _db.GetCollection<checkVoucher>("checkVoucher").FindOneAndUpdateAsync(query, update);
        }


        public void updateRfp(string cvSlug, string rfpNumber, rfp u)
        {

            var query = Builders<checkVoucher>.Filter.And(Builders<checkVoucher>.Filter.Eq(x => x.slug, cvSlug),
            Builders<checkVoucher>.Filter.ElemMatch(x => x.rfp, x => x.number == rfpNumber));

            var update = Builders<checkVoucher>.Update
                .Set("rfp.$.number", u.number)
                .Set("rfp.$.members", u.members)
                .Set("rfp.$.datePrepared", u.datePrepared)
                .Set("rfp.$.totalAmount", u.totalAmount)
                .CurrentDate("updatedAt");

            var result = _db.GetCollection<checkVoucher>("checkVoucher").UpdateOneAsync(query, update);
        }

        public banksList getBranchSlug(String branchId)
        {
            var query = Builders<banksList>.Filter.Eq(x => x.Id, branchId);
            var fields = Builders<banksList>.Projection
                .Include(u => u.Id)
                .Include(u => u.slug)
                .Include(u => u.code)
                .Include(u => u.banks);

            var bank = _db.GetCollection<banksList>("branch").Find(query).Project<banksList>(fields).ToListAsync();
            return bank.Result.FirstOrDefault();

            //var query = Builders<branch>.Filter.Eq(e => e.Id, branchId);
            //var branch = _db.GetCollection<branch>("branch").Find(query).ToListAsync();

            //return branch.Result.FirstOrDefault();
        }
    }
}
