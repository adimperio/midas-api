﻿using kmbi_core_master.hrModule.Models;
using System;
using System.Collections.Generic;
using kmbi_core_master.coreMaster.Models;

namespace kmbi_core_master.hrModule.Interface
{
    public interface ipayrollDeductionRepository
    {
        payrollDeduction getPayrollDeduction(String slug);
        IEnumerable<payrollDeduction> getPayrollDeductions();
        void insertPayrollDeduction(payrollDeduction pd);
        void updatePayrollDeduction(String slug, payrollDeduction pd);
        void deletePayrollDeduction(String slug);

        payrollDeductionLoanType getLoanType(String pdSlug, String ltSlug);
        IEnumerable<payrollDeductionLoanType> getLoanTypes(String pdSlug);
        void insertLoanType(String pdSlug, payrollDeductionLoanType loanType);
        void updateLoanType(String pdSlug, String ltSlug, payrollDeductionLoanType loanType);
        void deleteLoanType(String pdSlug, String ltSlug);

        payrollDeduction getPayrollDeductionsByEmployeeCode(String employeeCode);
    }
}
