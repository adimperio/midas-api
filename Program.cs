﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Events;

namespace kmbi_core_master
{
    public class Program
    {
        public static void Main(string[] args)
        {
            string timestamp = DateTime.Now.ToString("yyyyMMdd_HHmm");
            Log.Logger = new LoggerConfiguration()
            .MinimumLevel.Error()
            // .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
            // .MinimumLevel.Fatal()
            // .Enrich.FromLogContext()
            .WriteTo.File($"logs\\log{timestamp}.txt")
            .WriteTo.Console()
            .CreateLogger();

            try
            {
                Log.Information("Starting host");
                BuildWebHost(args).Run();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Host terminated unexpectedly");
            }
            finally
            {
                Log.CloseAndFlush();
            }
            
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UseIISIntegration()
                .UseUrls("http://*:5000")
                .ConfigureLogging(config=>{
                    config.ClearProviders();
                    config.AddSerilog();
                })
                .Build();
    }
}
