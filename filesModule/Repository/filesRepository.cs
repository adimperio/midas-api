﻿using Microsoft.Extensions.Options;

using System;
using System.Collections.Generic;
using System.Linq;

using MongoDB.Bson;
using MongoDB.Driver;

using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.filesModule.Models;
using kmbi_core_master.filesModule.Interface;

namespace kmbi_core_master.filesModule.Repository
{
    public class filesRepository : dbCon, ifilesRepository
    {
        
        public filesRepository(IOptions<dbSettings> settings) : base(settings) {}

        public void add(files b)
        {

            b.createdAt = DateTime.Now;
            b.updatedAt = DateTime.Now;

            _db.GetCollection<files>("files").InsertOneAsync(b);
        }

      
        public IEnumerable<files> all()
        {
            var c = _db.GetCollection<files>("files").Find(new BsonDocument()).ToList();
            return c;
        }

        public files getSlug(String slug)
        {
            var query = Builders<files>.Filter.Eq(e => e.slug, slug);
            var c = _db.GetCollection<files>("files").Find(query).ToList();

            return c.FirstOrDefault();
        }

       
        public void update(String slug, files p)
        {
            p.slug = slug;
            var query = Builders<files>.Filter.Eq(e => e.slug, slug);
            var update = Builders<files>.Update
                .Set("name", p.name)
                .Set("type", p.type)
                .Set("description", p.description)
                .Set("file", p.file)
                .Set("tags", p.tags)
                .Set("alwaysDownloadable", p.alwaysDownloadable)
                .Set("updatedBy", p.updatedBy)
                .Set("categories", p.categories)
                .Set("inclusions", p.inclusions)
                .Set("jobLevel", p.jobLevel)
                .CurrentDate("updatedAt");

            var branch = _db.GetCollection<files>("files").UpdateOne(query, update);
        }

        public void remove(String slug)
        {
            var query = Builders<files>.Filter.Eq(e => e.slug, slug);
            var result = _db.GetCollection<files>("files").DeleteOne(query);
        }

        public List<files> search(String s)
        {
            string[] searchStr = s.Split(default(string[]), StringSplitOptions.RemoveEmptyEntries);

            List<BsonRegularExpression> a = new List<BsonRegularExpression>();

            foreach (string x in searchStr)
            {
                a.Add(new BsonRegularExpression(x, options: "i"));
            }

            var query1 = Builders<files>.Filter.In(e => e.name, a);
            var query2 = Builders<files>.Filter.In("tags", a);
            var query3 = Builders<files>.Filter.In(e => e.description, a);
            var query = Builders<files>.Filter.Or(query1, query2, query3);
            var list = _db.GetCollection<files>("files").Find(query);
            return list.ToList();
        }

    }
}
