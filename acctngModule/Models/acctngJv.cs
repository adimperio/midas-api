﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;
using System;

namespace kmbi_core_master.acctngModule.Models
{
    public class acctngJv
    {

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("slug")]
        public String slug { get; set; }

        [BsonElement("number")]
        public Int32 number { get; set; }

        [BsonElement("jvNumber")]
        public string jvNumber { get; set; }
        
        [BsonElement("particulars")]
        public string particulars { get; set; }

        [BsonElement("accounts")]
        public jvAccounts accounts { get; set; }
        
        [BsonElement("branch")]
        public jvBranch branch { get; set; }

        [BsonElement("amount")]
        public jvAmount amount { get; set; }

        [BsonElement("isCancelled")]
        public bool isCancelled { get; set; }

        [BsonElement("isClosed")]
        public bool isClosed { get; set; }

        [BsonElement("reason")]
        public string reason { get; set; }

        [BsonElement("status")]
        public string status { get; set; }
       
        [BsonElement("timestamps")]
        public jvTimestamps timestamps { get; set; }
    }

    public class jvAccounts
    {
        [BsonElement("credit")]
        public jvCredit[] credit { get; set; }

        [BsonElement("debit")]
        public jvDebit[] debit { get; set; }

    }

    public class jvCredit
    {
        [BsonId]
        [BsonIgnoreIfDefault]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("code")]
        public int code { get; set; }

        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("particular")]
        public string particular {get;set;}

        [BsonElement("entity")]
        public string entity { get; set; }

        [BsonElement("budget")]
        public string budget { get; set; }

        [BsonElement("amount")]
        public double amount { get; set; }
    }

    public class jvDebit
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("code")]
        public int code { get; set; }

        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("particular")]
        public string particular {get;set;}

        [BsonElement("entity")]
        public string entity { get; set; }

        [BsonElement("budget")]
        public string budget { get; set; }

        [BsonElement("amount")]
        public double amount { get; set; }
    }

    public class jvBranch
    {
        [BsonId]
        [BsonElement("id")]
        public string id { get; set; }

        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

    }

   
    public class jvAmount
    {
        [BsonElement("byNumbers")]
        public double byNumbers { get; set; }

        [BsonElement("byWords")]
        public string byWords { get; set; }
    }

    public class jvTimestamps
    {
        [BsonElement("issuedAt")]
        public DateTime issuedAt { get; set; }

        [BsonElement("issuedBy")]
        public string issuedBy { get; set; }

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("createdBy")]
        public string createdBy { get; set; }

        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }

        [BsonElement("updatedBy")]
        public string updatedBy { get; set; }
    }

    
}
