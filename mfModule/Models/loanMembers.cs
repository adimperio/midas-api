﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.coreMaster.Models
{

    public class loanMembers

    {
        //[BsonId]
        //[BsonRepresentation(BsonType.ObjectId)]
        //public String Id { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("name")]
        public name name { get; set; }

        // [BsonElement("maidenName")]
        // public maidenName maidenName { get; set; }

        [BsonElement("fullname")]
        public string fullname { get; set; }

        [BsonElement("birthdate")]
        public DateTime birthdate { get; set; }

        [BsonElement("civilStatus")]
        public string civilStatus { get; set; }

        [BsonElement("memberId")]
        public string memberId { get; set; }

        [BsonElement("address")]
        public string address { get; set; }

        [BsonElement("position")]
        public string position { get; set; }

        [BsonElement("businessName")]
        public string businessName { get; set; }

        [BsonElement("rfpNumber")]
        public string rfpNumber { get; set; }

        //[BsonElement("loanId")]
        //public string loanId { get; set; }

        [BsonElement("totalAmount")]
        public double totalAmount { get; set; }

        [BsonElement("principalAmount")]
        public double principalAmount { get; set; }

        [BsonElement("schedule")]
        public IList<schedule> schedule { get; set; }

        public upfrontFees upfrontFees { get; set; }

        [BsonElement("health")]
        public int health { get; set; }

        public issuedId issuedId { get; set; }

        public IList<loanMemberDependents> dependents { get; set; }

        [BsonElement("beneficiary")]
        public string beneficiary { get; set; }

        [BsonElement("spouse")]
        public loanSpouse spouse { get; set; }

        [BsonElement("creditLimitAmount")]
        public double creditLimitAmount { get; set; }

        [BsonElement("loanCycle")]
        public int loanCycle { get; set; }

        [BsonElementAttribute("purpose")]
        public purpose purpose {get;set;}


        //[BsonElement("createdAt")]
        //public DateTime createdAt { get; set; }

        //[BsonElement("updatedAt")]
        //public DateTime updatedAt { get; set; }
    }

    public class name
    {
        [BsonElement("first")]
        public string first { get; set; }

        [BsonElement("middle")]
        public string middle { get; set; }

        [BsonElement("last")]
        public string last { get; set; }
    }

    

    // public class maidenName
    // {
    //     [BsonElement("first")]
    //     public string first { get; set; }

    //     [BsonElement("middle")]
    //     public string middle { get; set; }

    //     [BsonElement("last")]
    //     public string last { get; set; }
    // }

    public class loanSpouse
    {
        [BsonElement("name")]
        public loanSpouseName name { get; set; }

        [BsonElement("birthdate")]
        public DateTime? birthdate { get; set; }
    }

    public class loanSpouseName
    {
        [BsonElement("first")]
        public string first { get; set; }

        [BsonElement("middle")]
        public string middle { get; set; }

        [BsonElement("last")]
        public string last { get; set; }

        [BsonElement("full")]
        public string full { get; set; }
    }

    public class issuedId
    {
        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("number")]
        public string number { get; set; }

        [BsonElement("date")]
        public DateTime? date { get; set; }

        [BsonElement("place")]
        public string place { get; set; }
    }

    public class loanMemberDependents
    {
       
        [BsonElement("type")]
        public string type { get; set; }

        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("birthdate")]
        public DateTime? birthdate { get; set; }

        [BsonElement("premiumAmount")]
        public double premiumAmount { get; set; }

        [BsonElement("newDep")]
        public int newDep { get; set; }

        [BsonElement("included")]
        public bool included { get; set; }

    }

    public class upfrontFees
    {

        [BsonElement("CBUAmount")]
        public double CBUAmount { get; set; }

        [BsonElement("loanProcessingFee")]
        public double loanProcessingFee { get; set; }

        [BsonElement("miPremium")]
        public double miPremium { get; set; }

        [BsonElement("cgli")]
        public double cgli { get; set; }

        [BsonElement("dst")]
        public double dst { get; set; }

        [BsonElement("total")]
        public double total { get; set; }

    }

    public class schedule
    {
        //[BsonId]
        //[BsonRepresentation(BsonType.ObjectId)]
        //public String Id { get; set; }

        [BsonElement("weekNo")]
        public int weekNo { get; set; }

        [BsonElement("interestRate")]
        public double interestRate { get; set; }

        [BsonElement("collectionDate")]
        public DateTime collectionDate { get; set; }

        [BsonElement("paidAt")]
        public DateTime? paidAt { get; set; }

        [BsonElement("principalBalance")]
        public double principalBalance { get; set; }

        [BsonElement("interestBalance")]
        public double interestBalance { get; set; }

        [BsonElement("cbuBalance")]
        public double cbuBalance { get; set; }

        [BsonElement("currentPrincipalDue")]
        public double currentPrincipalDue { get; set; }

        [BsonElement("currentInterestDue")]
        public double currentInterestDue { get; set; }

        [BsonElement("currentCbuDue")]
        public double currentCbuDue { get; set; }

        [BsonElement("pastPrincipalDue")]
        public double pastPrincipalDue { get; set; }

        [BsonElement("pastInterestDue")]
        public double pastInterestDue { get; set; }

        [BsonElement("pastCbuDue")]
        public double pastCbuDue { get; set; }

        [BsonElement("payments")]
        public IList<string> payments { get; set; }

        [BsonElement("remarks")]
        public string remarks { get; set; }

        //[BsonElement("createdAt")]
        //public DateTime createdAt { get; set; }

        //[BsonElement("updatedAt")]
        //public DateTime updatedAt { get; set; }
    }

    public class purpose
    {
        public String code {get;set;}
        public String name {get;set;}
    }

}
