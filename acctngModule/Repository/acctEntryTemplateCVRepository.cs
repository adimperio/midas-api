﻿using Microsoft.Extensions.Options;

using System;
using System.Collections.Generic;
using System.Linq;

using MongoDB.Bson;
using MongoDB.Driver;

using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.acctngModule.Models;
using kmbi_core_master.acctngModule.Interface;

namespace kmbi_core_master.acctngModule.Repository
{
    public class acctEntryTemplateCVRepository : dbCon, iacctEntryTemplateCVRepository
    {
        
        public acctEntryTemplateCVRepository(IOptions<dbSettings> settings) : base(settings) {}

        public void add(acctEntryTemplateCV b)
        {

            b.timestamps.createdAt = DateTime.Now;
            b.timestamps.updatedAt = DateTime.Now;
            for (int i = 0; i <= b.accounts.credit.Count() - 1; i++)
            {
                b.accounts.credit[i].Id = ObjectId.GenerateNewId().ToString();
            }

            for (int i = 0; i <= b.accounts.debit.Count() - 1; i++)
            {
                b.accounts.debit[i].Id = ObjectId.GenerateNewId().ToString();
            }
            var coll = _db.GetCollection<acctEntryTemplateCV>("acctEntryTemplateCV");
            _db.GetCollection<acctEntryTemplateCV>("acctEntryTemplateCV").InsertOneAsync(b);
        }
        

        public IEnumerable<acctEntryTemplateCV> all()
        {
            var c = _db.GetCollection<acctEntryTemplateCV>("acctEntryTemplateCV").Find(new BsonDocument()).ToListAsync();
            return c.Result;
        }

        public acctEntryTemplateCV getSlug(String slug)
        {
            var query = Builders<acctEntryTemplateCV>.Filter.Eq(e => e.slug, slug);
            var c = _db.GetCollection<acctEntryTemplateCV>("acctEntryTemplateCV").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }
       
        public void update(String slug, acctEntryTemplateCV p)
        {
            p.slug = slug;
            var query = Builders<acctEntryTemplateCV>.Filter.Eq(e => e.slug, slug);
            var update = Builders<acctEntryTemplateCV>.Update
                .Set("particulars", p.particulars)
                .Set("accounts", p.accounts)
                .Set("supplier", p.supplier)
                .Set("timestamps.issuedAt", p.timestamps.issuedAt)
                .Set("timestamps.issuedBy", p.timestamps.issuedBy)
                .Set("timestamps.createdBy", p.timestamps.createdBy)
                .Set("timestamps.updatedBy", p.timestamps.updatedBy)
                .CurrentDate("timestamps.updatedAt");

            var branch = _db.GetCollection<acctEntryTemplateCV>("acctEntryTemplateCV").UpdateOneAsync(query, update);

        }
        public void remove(String slug)
        {
            var query = Builders<acctEntryTemplateCV>.Filter.Eq(e => e.slug, slug);
            var result = _db.GetCollection<acctEntryTemplateCV>("acctEntryTemplateCV").DeleteOneAsync(query);
        }

       
    }
}
