using System;

namespace  UseCases.GenerateOtForPayroll {
    public class OtItem {
        public String EmployeeCode {get;set;}
        public String EmployeeName {get;set;}
        public Double LEG {get;set;}
        public Double LEGAL {get;set;}
        public Double REGND {get;set;}
        public Double REGNDOT {get;set;}
        public Double REGOT {get;set;}
        public Double RST {get;set;}
        public Double RSTLEG {get;set;}
        public Double RSTLEGND {get;set;}
        public Double RSTLEGNDOT {get;set;}
        public Double RSTLEGOT {get;set;}
        public Double RSTND {get;set;}
        public Double RSTNDOT {get;set;}
        public Double RSTOT {get;set;}
        public Double RSTSPE {get;set;}
        public Double RSTSPEND {get;set;}
        public Double RSTSPENDOT {get;set;}
        public Double RSTSPEOT {get;set;}
        public Double SPE {get;set;}
        public Double SPEND {get;set;}
        public Double SPENDOT {get;set;}
        public Double SPEOT {get;set;}
        public Double SPECIAL {get;set;}
        public Double SPECIALND {get;set;}
        public Double SPECIALNDOT {get;set;}
        public Double SPECIALOT {get;set;}
        public Double WLEGND {get;set;}
        public Double WLEGNDOT {get;set;}
        public Double WLEGOT {get;set;}
        public Double WRKLEG {get;set;}
    }
}