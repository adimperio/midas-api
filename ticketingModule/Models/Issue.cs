using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using kmbi_core_master.coreMaster.Models;

namespace kmbi_core_master.ticketing.Models
{
  public class Issue
  {
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public String id { get; set; }

    [BsonElement("title")]
    public string title { get; set; }

    [BsonElement("slug")]
    public string slug { get; set; }

    [BsonElement("branchSlug")]
    public string branchSlug { get; set; }

    [BsonElement("status")]
    public string status { get; set; }

    [BsonElement("type")]
    public string type { get; set; }

    [BsonElement("category")]
    public string category { get; set; }

    [BsonElement("comments")]
    public Comment[] comments { get; set; }

    [BsonElement("file")]
    public File file { get; set; }

    [BsonElement("seenBy")]
    public string[] seenBy { get; set; }

    [BsonElement("postedBy")]
    public string postedBy { get; set; }

    [BsonElement("createdAt")]
    public DateTime createdAt { get; set; }
  }

  public class IssueFull
  {
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public String id { get; set; }

    [BsonElement("title")]
    public string title { get; set; }

    [BsonElement("slug")]
    public string slug { get; set; }

    [BsonElement("branchSlug")]
    public string branchSlug { get; set; }

    [BsonElement("branch")]
    public branch[] branch { get; set; }

    [BsonElement("status")]
    public string status { get; set; }

    [BsonElement("type")]
    public string type { get; set; }

    [BsonElement("category")]
    public string category { get; set; }

    [BsonElement("comments")]
    public Comment[] comments { get; set; }

    [BsonElement("file")]
    public File file { get; set; }

    [BsonElement("seenBy")]
    public string[] seenBy { get; set; }

    [BsonElement("postedBy")]
    public string postedBy { get; set; }

    [BsonElement("user")]
    public string user { get; set; }

    [BsonElement("createdAt")]
    public DateTime createdAt { get; set; }
  }

  public class Comment
  {
    [BsonElement("slug")]
    public string slug { get; set; }
    
    [BsonElement("employerSlug")]
    public string employerSlug { get; set; }

    [BsonElement("employee")]
    public string employee { get; set; }

    [BsonElement("text")]
    public string text { get; set; }

    [BsonElement("status")]
    public string status { get; set; }

    [BsonElement("isEdited")]
    public string isEdited { get; set; }

    [BsonElement("createdAt")]
    public DateTime createdAt { get; set; }
  }

  public class File
  {
    [BsonElement("name")]
    public String name { get; set; }

    [BsonElement("path")]
    public String path { get; set; }

    [BsonElement("type")]
    public String type { get; set; }
  }

}
