using System;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using System.IO;
using kmbi_core_master.helpers.interfaces;
using kmbi_core_master.coreMaster.Models;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Hosting;
using System.Runtime.InteropServices;

namespace kmbi_core_master.helpers
{
    public class localUpload : iupload
    {
        String link = "";
        String _curdir = "";
        IHostingEnvironment _env;

        public UrlGetter _urlGetter { get; set;}
        public IOptions<localFileRepositorySettings> _localSetting { get; }

        public localUpload(IHostingEnvironment env, UrlGetter urlGetter, IOptions<localFileRepositorySettings> localSetting) : base()
        {
            _env = env;
            _urlGetter = urlGetter;
            _localSetting = localSetting;

            //_curdir = env.ContentRootPath + "\\";
            if (!_env.IsProduction()) {
                if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux) || RuntimeInformation.IsOSPlatform(OSPlatform.OSX)) {
                    _curdir = localSetting.Value.pathLinx;
                } else {
                    _curdir = localSetting.Value.pathWin;
                }
            }

            if (env.IsEnvironment("Production") || env.IsEnvironment("Staging") || env.IsEnvironment("Offline")) {
                if(RuntimeInformation.IsOSPlatform(OSPlatform.Linux) || RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
                    _curdir = "/var/www/aspnetcore/";
            }
            var envi = env.EnvironmentName;

            link = _urlGetter.get().Item2;
        }

        public String getRoot() {
            return link;
        }
        public String upload(Byte[] bytes, String fullPath)
        {
            File.WriteAllBytes(_curdir + "wwwroot/" + fullPath,bytes);
            return link + fullPath;
        }

        public void delete(String fullPath) {
            File.Delete(_curdir + "wwwroot/" + fullPath);
        }
    }
}