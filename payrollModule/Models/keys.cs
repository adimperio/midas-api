﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.payrollModule.Models
{
    public class keys
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }
       
        [BsonElement("name")]
        public string name { get; set; }
        
        [BsonElement("value")]
        public string value { get; set; }

        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }
    }

    public class checkKeyPassword
    {
        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("password")]
        public string password { get; set; }

        [BsonElement("value")]
        public string value { get; set; }
    }
}
