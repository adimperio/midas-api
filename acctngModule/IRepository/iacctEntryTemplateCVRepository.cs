﻿using kmbi_core_master.acctngModule.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.acctngModule.Interface
{
    public interface iacctEntryTemplateCVRepository
    {
        IEnumerable<acctEntryTemplateCV> all();

        acctEntryTemplateCV getSlug(String slug);

        void add(acctEntryTemplateCV b);

        void update(String slug, acctEntryTemplateCV b);

        void remove(String slug);
        
    }
}
