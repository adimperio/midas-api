﻿using System;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.coreMaster.IRepository;
using Microsoft.AspNetCore.Cors;
using kmbi_core_master.helpers.interfaces;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Hosting;
// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace kmbi_core_master.coreMaster.Controllers
{
    public class ThisModel {
        public String name {get;set;}
        public String systemName {get;set;}
        public String email {get;set;}
        public String link {get;set;}
    }
    [EnableCors("allowCors")]
    [Route("api/resetPassword")]
    public class resetPasswordController : Controller
    {
        private readonly iresetPasswordRepository _irpRepository;
        private readonly iNotification _email;
        private readonly ihtmlBuilder _htmlbuilder;
        private readonly IOptions<dbSettings> _settings;
        private readonly UrlGetter _urlGetter;
        private readonly IHostingEnvironment _env;
        private readonly iusersRepository _logInfo;
        private string token;

        public resetPasswordController(IOptions<dbSettings> settings, iresetPasswordRepository irpRepository, iNotification email, ihtmlBuilder htmlBuilder, UrlGetter urlGetter, IHostingEnvironment env, iusersRepository logInfo)
        {   this._settings = settings;
            this._irpRepository = irpRepository;
            _htmlbuilder = htmlBuilder;
            _email = email;
            _urlGetter = urlGetter;
            _env = env;
            _logInfo = logInfo;
        }

        public void logger(string message)
        {
            //Insert Log//
            token = HttpContext.Request.Headers["token"];
            var lInfo = _logInfo.getLoginInfo(token);
            string log = message;
            global.createLog(lInfo.id, lInfo.fullname, lInfo.employment.branch, log);
            //Insert Log//
        }

        [HttpGet("token={token}&email={email}")]
        public IActionResult getSlug(string token, string email)
        {

            var item = _irpRepository.checkEmailToken(token, email);
            if (item == null)
            {
                return BadRequest(new {
                    isValid = 0,
                    message = "Email/Token invalid."
                });
            }

            var createdDate = item.createdAt;
            var dateNow = DateTime.UtcNow;
            TimeSpan hrs = dateNow - createdDate;
            int x = (int)hrs.TotalHours;

            if (item != null && x > 24)
            {
                return BadRequest(new
                {
                    isValid = 0,
                    message = "Token is expired, request a new entry."
                });
            }

            return Ok(new
            {
                isValid = 1
            });
            
        }

        // POST 
        [HttpPost]
        public IActionResult add([FromBody] resetPassword user)
        {
            var email = _irpRepository.getCredential(user.email);
            if (email == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "Email address " + user.email + " not found"
                });
            }

            _irpRepository.add(user);
            
            // email
            
            _email.from = "no-reply@kmbidemo.com";
            _email.to = user.email;
            _email.subject = "IT Department Notification";

            // var dom = "";
            // switch (_env.EnvironmentName.ToLower()) 
            // {
            //     case "development":
            //         dom = _rootSettings.Value.frontend_development;
            //         break;
            //     case "staging":
            //         dom = _rootSettings.Value.frontend_staging;
            //         break;
            //     case "production":
            //         dom = _rootSettings.Value.frontend_production;
            //         break;
            // }

            //var dom = _rootSettings.Value.frontend;
            var dom = _urlGetter.get().Item1;
                
            var a = _irpRepository.checkEmailToken(email.verificationToken,email.email);
             _email.message = _htmlbuilder.htmlBuild(System.IO.Directory.GetCurrentDirectory() +"/wwwroot/templates/","passwordResetTemplate.cshtml",
             new ThisModel{ 
                 name = email.name.first,
                 link = dom + "/#/get-new-password?email=" + user.email + "&token=" + user.token,
                 systemName = "KMBI-DUNAMIS"
                 });
                    

            iResult res = _email.Send();
            if (res.code.ToLower()=="success") {


                //logger("Reset password " + user.email);

                return Ok(new
                    {
                        type = "success",
                        message = "Password reset link for user " + user.email + " sent succesfully"
                    });
            } else {
                return Ok(new
                    {
                        type = "failed",
                        message = res.description
                    });
            }

        }

        [HttpPut]
        public IActionResult updatePassword([FromBody] users user)
        {
            var item = _irpRepository.getCredential(user.email);
            
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "Email address " + user.email + " not found"
                });
            }

           
            _irpRepository.updatePassword(user.email, user);

           logger( "Reset password " + user.email);

            return Ok(new
            {
                type = "success",
                message = "Password updated successfully."
            });
        }


    }
}
