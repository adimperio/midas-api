﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Filters;
using kmbi_core_master.hrModule.Interface;
using kmbi_core_master.hrModule.Models;
using Microsoft.AspNetCore.Cors;

using System;
using System.Threading.Tasks;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.documentationModule.iRepositories;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.payrollModule.IRepository;
using System.Linq;

namespace kmbi_core_master.coreMaster.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/payrollDatum")]
    [usersAuth]
    public class payrollDatumController : Controller
    {
        private ibranchRepository _branch;
        private iholidayRepository _holiday;
        private iusersRepository _user;
        private ipayrollDeductionRepository _deduction;
        private iotLogsRepository _otLog;
        private ipayrollRepository _payroll;
        private idtrRepository _dtr;
        private ifhgDeductionsRepository _fhgDeds;

        public payrollDatumController(
            ibranchRepository branch,
            iholidayRepository holiday,
            iusersRepository user,
            ipayrollDeductionRepository deduction,
            iotLogsRepository otLog,
            ipayrollRepository payroll,
            idtrRepository dtr,
            ifhgDeductionsRepository fhgDeds
        ) {
            _branch = branch;
            _holiday = holiday;
            _user = user;
            _deduction = deduction;
            _otLog = otLog;
            _payroll = payroll;
            _dtr = dtr;
            _fhgDeds = fhgDeds;
        }

        [HttpGet("byBranch/{branchSlug}")]
        public payrollDatum byBranch(String branchSlug, [FromQuery]DateTime from, [FromQuery]DateTime to, [FromQuery]String employeeCode = "") {
            branch branch = _branch.getBranchSlug(branchSlug);
            
            payrollDatum ad = null;
            if(branch!=null) {
                ad = new payrollDatum();
                ad.branchSlug = branch.slug;
                ad.branchName = branch.name;
                ad.holidays = _holiday.GetHolidays(from.Date.Month,from.Date.Year);
                ad.payrolls = _payroll.getPayroll(ad.branchSlug);
                ad.employees = new List<payrollEmployee>();
                
                IEnumerable<users> users = _user.directoryByBranchSlug(branchSlug,"");
                if(employeeCode!="") {
                    users = users.Where(x=>x.employeeCode == employeeCode);
                }
                var fhgDedsData = _fhgDeds.getByCutOff(from, to);

                foreach(users user in users) {
                    payrollEmployee emp = new payrollEmployee();
                    emp.employeeSlug = user.slug;
                    emp.employeeCode = user.employeeCode;
                    emp.fullname = user.name.last + ", " + user.name.first + " " + user.name.middle + " " + user.extension;
                    emp.jobGrade = user.employment.jobGrade;
                    emp.benefits = user.employment.benefits;
                    emp.status = user.employment.status;
                    emp.deduction = _deduction.getPayrollDeductionsByEmployeeCode(user.employeeCode);
                    emp.otLogs = _otLog.getOtLogPerEmployeeCodePerDateRange(user.employeeCode, from, to);
                    emp.dtrs = _dtr.getDtrPerEmployeeCodePerDateRange(user.employeeCode, from,to);

                    // prorate rice subsidy
                    emp.benefits.ForEach(b=>{
                        if(b.name == "RICE SUBSIDY"){
                            // get absences from prev cut-off

                            // 7 - 21 subtract the absences
                            var adjFrom = to.AddMonths(-1).AddDays(1);
                            var adjTo = from.AddDays(-1);
                            var adjList = _dtr.getDtrPerEmployeeCodePerDateRange(user.employeeCode, adjFrom,adjTo);
                            var adjAbsences = 0.0;
                            
                            adjList.ForEach(a=>{
                                adjAbsences += ((a.absent.am + a.absent.pm) / 8);
                            });
                            
                            // 22 - 6 compute per day

                            var curFrom = from;
                            var curTo = to;
                            var curList = _dtr.getDtrPerEmployeeCodePerDateRange(user.employeeCode, curFrom,curTo);
                            var curHWork = 0.0;
                            
                            curList.ForEach(a=>{
                                curHWork += ((a.hoursWork.am + a.hoursWork.pm) / 8);
                            });

                            // 7 - 21 assume

                            var assuFrom = to.AddDays(1);
                            var assuTo = from.AddMonths(1).AddDays(-1);
                            var assuHWork = 0.0;

                            for(DateTime ctr = assuFrom;ctr<=assuTo;ctr=ctr.AddDays(1)) {
                                if(ctr.DayOfWeek != DayOfWeek.Saturday && ctr.DayOfWeek != DayOfWeek.Sunday)
                                    assuHWork += 1;
                            }

                            var rsPerDay = b.amount / 22;
                            
                            var rsAmount = ((assuHWork + curHWork) - adjAbsences) * rsPerDay;
                        
                        
                            b.amount = Math.Round(rsAmount,2);
                        }
                    });

                    var fhgDed = fhgDedsData.ToList().Where(x=>x.employeeCode == emp.employeeCode).FirstOrDefault();
                    payrollModule.Models.fhgDeductionExtractedForPayroll fhgDedFinal;

                    if(fhgDed != null) {
                        fhgDedFinal = new payrollModule.Models.fhgDeductionExtractedForPayroll {
                            fidelityBond = fhgDed.deductions.fidelityBond,
                            fhgLoanPayable = fhgDed.deductions.fhgLoanPayable,
                            fhgShareCapital = fhgDed.deductions.fhgShareCapital,
                            fhgSavings= fhgDed.deductions.fhgSavings,
                            fhgInsurance = fhgDed.deductions.fhgInsurance,
                            fhgOthers = fhgDed.deductions.fhgOthers
                        };
                    } else {
                        fhgDedFinal = new payrollModule.Models.fhgDeductionExtractedForPayroll {
                            fidelityBond = 0,
                            fhgLoanPayable = 0,
                            fhgShareCapital = 0,
                            fhgSavings= 0,
                            fhgInsurance = 0,
                            fhgOthers = 0
                        };
                    }
                    
                    emp.fhgDeduction = fhgDedFinal;
                    ad.employees.Add(emp);
                }
            }
            return ad;
        }



        class payroll {
            public DateTime from {get;set;}
            public DateTime to {get;set;}
            public payrollDetail[] details {get;set;}
        }

        class payrollDetail {
            public String employeeCode {get;set;}
            public String name {get;set;}
            public Double monthlyRate {get;set;}
            public Double dailyRate {get;set;}
            public payrollDetailEarnings earnings {get;set;}
            public Double totalTaxableIncome {get;set;}
            public payrollDetailDeductions deductions {get;set;}
        }

        class payrollDetailEarnings {
            public Double basicPay {get;set;}
            public Double overtimePay {get;set;}
            public payrollDetailEarningOvertimeDetails[] overtimeDetails {get;set;}
            public Double salaryAdjustment {get;set;}
            public Double absentLateUndertime {get;set;}
            public Double accountsPayable {get;set;}
            public Double riceSubsidy {get;set;}
            public payrollDetailEarningAbsentLateUndertimeDetails[] absentLateUndertimeDetails {get;set;}
            
        }

        class payrollDetailEarningOvertimeDetails {
            public DateTime date {get;set;}
            public String type {get;set;} 
            public Double hour {get;set;}
            public Double pay {get;set;}
            public Double excesshour {get;set;}
            public Double excesspay {get;set;}
        }

        class payrollDetailEarningAbsentLateUndertimeDetails {
            public DateTime date {get;set;}
            public String type {get;set;} 
            public Double hour {get;set;}
            public Double pay {get;set;}
        }

        class payrollDetailDeductions {
            public Double witholdingTax {get;set;}
        }

        [HttpGet("compute")]
        public IActionResult compute([FromQuery]DateTime from, [FromQuery]DateTime to, [FromQuery]String branchSlug = "", [FromQuery]String employeeCode = "") {

            var proll = byBranch(branchSlug, from, to, employeeCode);

            List<payrollDetail> details = new List<payrollDetail>();
            
            proll.employees.ForEach(employee=>{
                var monthlyRate = proll.payrolls.Where(x=>x.empCode == employee.employeeCode).First().salary;
                var dailyRate = monthlyRate / 22;
                var hourlyRate = dailyRate / 8;
                
                var basicPay = monthlyRate / 2;

                var overtimeDetails = new List<payrollDetailEarningOvertimeDetails>();
                employee.otLogs.Where(x=>x.duration > 0).ToList().ForEach(ot=>{
                    var isRegular = false;
                    var isRestDay = false;
                    var isRegularHoliday = false;
                    var isSpecialHoliday = false;

                    if(ot.dates.from.DayOfWeek == DayOfWeek.Saturday || ot.dates.from.DayOfWeek == DayOfWeek.Sunday) {
                        isRestDay = true;
                        isRegular = false;
                    }

                    var holidayDate = proll.holidays.Where(x=>x.date == ot.dates.from).FirstOrDefault();
                    if(holidayDate != null) {
                        isRegular = false;
                        if(holidayDate.type.ToLower() == "regular") {
                            isRegularHoliday = true;
                            isSpecialHoliday = false;
                        } else {
                            isRegularHoliday = false;
                            isSpecialHoliday = true;
                        }
                    }
                    if(!isRestDay && !isRegularHoliday && !isSpecialHoliday)
                        isRegular = true;

                    var excess = (ot.duration>8) ? ot.duration - 8 : 0;
                    var item = new payrollDetailEarningOvertimeDetails{
                        date = ot.dates.from,
                        type = "",
                        hour = 0,
                        pay = 0
                    };

                    if(isRegular && !isRestDay && !isRegularHoliday && !isSpecialHoliday) {
                        // Regular Overtime
                        item.type = "Regular Overtime";
                        item.hour = ot.duration - excess;
                        item.pay = item.hour * hourlyRate * 1.25;
                    } else if(!isRegular && isRestDay && !isRegularHoliday && !isSpecialHoliday) {
                        // Rest Day Overtime
                        item.type = "Rest Day Overtime";
                        item.hour = ot.duration - excess;
                        item.pay = item.hour * hourlyRate * 1.30;
                    } else if(!isRegular && !isRestDay && isRegularHoliday && !isSpecialHoliday) {
                        // Regular Holiday Overtime
                        item.type = "Regular Holiday Overtime";
                        item.hour = ot.duration - excess;
                        item.pay = item.hour * hourlyRate * 2;
                    } else if(!isRegular && isRestDay && isRegularHoliday && !isSpecialHoliday) {
                        // Regular Holiday & Rest Day Overtime
                        item.type = "Regular Holiday & Rest Day Overtime";
                        item.hour = ot.duration - excess;
                        item.pay = item.hour * hourlyRate * 2.6;
                    } else if(!isRegular && !isRestDay && !isRegularHoliday && isSpecialHoliday) {
                        // Special Holiday Overtime
                        item.type = "Special Holiday Overtime";
                        item.hour = ot.duration - excess;
                        item.pay = item.hour * hourlyRate * 1.3;
                    } else if(!isRegular && isRestDay && !isRegularHoliday && isSpecialHoliday) {
                        // Special Holiday & Rest Day Overtime
                        item.type = "Special Holiday & Rest Day Overtime";
                        item.hour = ot.duration - excess;
                        item.pay = item.hour * hourlyRate * 1.5;
                    }

                    item.excesshour = excess;
                    item.excesspay = item.hour * excess * .30;

                    overtimeDetails.Add(item);
                });

                
                var overtimePay = overtimeDetails.ToList().Sum(x=>x.pay);

                var salaryAdjustment = 0.0;
                var absentLateUndertimeDetails = new List<payrollDetailEarningAbsentLateUndertimeDetails>();
                employee.dtrs.ForEach(dtr=>{
                    if(dtr.absent.am > 0) {
                        absentLateUndertimeDetails.Add(new payrollDetailEarningAbsentLateUndertimeDetails{
                            date = dtr.date,
                            type = "absent am",
                            hour = dtr.absent.am,
                            pay = hourlyRate * dtr.absent.am
                        });
                    }

                    if(dtr.absent.pm > 0) {
                        absentLateUndertimeDetails.Add(new payrollDetailEarningAbsentLateUndertimeDetails{
                            date = dtr.date,
                            type = "absent pm",
                            hour = dtr.absent.pm,
                            pay = hourlyRate * dtr.absent.pm
                        });
                    }

                    if(dtr.late.am > 0) {
                        absentLateUndertimeDetails.Add(new payrollDetailEarningAbsentLateUndertimeDetails{
                            date = dtr.date,
                            type = "late am",
                            hour = dtr.late.am,
                            pay = hourlyRate * dtr.late.am
                        });
                    }

                    if(dtr.late.pm > 0) {
                        absentLateUndertimeDetails.Add(new payrollDetailEarningAbsentLateUndertimeDetails{
                            date = dtr.date,
                            type = "late pm",
                            hour = dtr.late.pm,
                            pay = hourlyRate * dtr.late.pm
                        });
                    }

                    if(dtr.late.am > 0) {
                        absentLateUndertimeDetails.Add(new payrollDetailEarningAbsentLateUndertimeDetails{
                            date = dtr.date,
                            type = "undertime am",
                            hour = dtr.undertime.am,
                            pay = hourlyRate * dtr.undertime.am
                        });
                    }

                    if(dtr.undertime.pm > 0) {
                        absentLateUndertimeDetails.Add(new payrollDetailEarningAbsentLateUndertimeDetails{
                            date = dtr.date,
                            type = "undertime pm",
                            hour = dtr.undertime.pm,
                            pay = hourlyRate * dtr.undertime.pm
                        });
                    }
                });

                var absentLateUndertime = absentLateUndertimeDetails.ToList().Sum(x=>x.pay);
                var accountsPayable = 0.0;
                var riceSubsidyBenefit = employee.benefits.Where(x=>x.name.ToLower() == "rice subsidy").FirstOrDefault();
                var riceSubsidy = 0.0;
                if(riceSubsidyBenefit!=null)
                    riceSubsidy = riceSubsidyBenefit.amount;


                var annualBasic = monthlyRate * 12;
                var sss = proll.payrolls.Where(x=>x.empCode == employee.employeeCode).First().sss;
                var philhealth = proll.payrolls.Where(x=>x.empCode == employee.employeeCode).First().philhealth;
                var pagibig = proll.payrolls.Where(x=>x.empCode == employee.employeeCode).First().pagibig;
                var annualStatutory = (sss + philhealth + pagibig) * 12;
                var thirteenMonthAndOtherBenefits = monthlyRate; // as of now, 13th month only
                var excessTaxable = 0.0;

                if(thirteenMonthAndOtherBenefits >=90000)
                    excessTaxable = 90000 - thirteenMonthAndOtherBenefits;

                var taxableIncome = 
                    (annualBasic +
                    excessTaxable) -
                    annualStatutory;

                var taxDue = 0.0;

                if(taxableIncome < 250000) {

                } else if(taxableIncome < 400000) {
                    taxDue = (annualBasic - 250000) * .2;
                } else if(taxableIncome < 800000) {
                    taxDue = 30000 + ((annualBasic - 400000) * .25);
                } else if(taxableIncome < 2000000) {
                    taxDue = 130000 + ((annualBasic - 800000) * .30);
                } else if(taxableIncome < 8000000) {
                    taxDue = 490000 + ((annualBasic - 2000000) * .32);
                } else {
                    taxDue = 2410000 + ((annualBasic - 8000000) * .35);
                }

                var taxDueMonthly = taxDue / 11;
                var taxDueSemiMonthly = taxDueMonthly / 2;


                details.Add(new payrollDetail {
                    employeeCode = employee.employeeCode,
                    name = employee.fullname,
                    monthlyRate = monthlyRate,
                    dailyRate = dailyRate,
                    earnings = new payrollDetailEarnings {
                        basicPay = basicPay,
                        overtimePay = overtimePay,
                        overtimeDetails = overtimeDetails.ToArray(),
                        salaryAdjustment = salaryAdjustment,
                        absentLateUndertime = absentLateUndertime,
                        absentLateUndertimeDetails = absentLateUndertimeDetails.ToArray(),
                        accountsPayable = accountsPayable,
                        riceSubsidy = riceSubsidy
                    },
                    totalTaxableIncome = (basicPay + overtimePay + salaryAdjustment) - absentLateUndertime,
                    deductions = new payrollDetailDeductions {
                        witholdingTax = taxDueSemiMonthly
                    }
                });
            });


            var payroll = new payroll {
                from = from,
                to = to,
                details = details.ToArray(),
            };

            return Ok(payroll);
        }

    }
}
