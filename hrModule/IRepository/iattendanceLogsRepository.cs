using kmbi_core_master.hrModule.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace kmbi_core_master.hrModule.Interface
{
    public interface iattendanceLogsRepository
    {
        IEnumerable<attendanceLogs> getAttendanceLogsPerEmployeePerDateRange(String biometricsCode, DateTime from, DateTime to);
        IEnumerable<attendanceLogs> getAttendanceLogsPerDateRange(DateTime from, DateTime to);

        downloadResult download(DateTime from, DateTime to, String employeeCode);
        downloadResult download(DateTime from, DateTime to);
        downloadResult download(Int32 month, Int32 year, String employeeCode);
        downloadResult download(Int32 month, Int32 year);

        downloadResult privateDownload(DateTime from, DateTime to, String employeeCode ="");
    }
}
