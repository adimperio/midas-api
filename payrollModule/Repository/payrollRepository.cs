﻿using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.helpers;
using kmbi_core_master.payrollModule.IRepository;
using kmbi_core_master.payrollModule.Models;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace kmbi_core_master.payrollModule.Repository
{
    public class payrollRepository: dbCon, ipayrollRepository
    {
       
        public payrollRepository(IOptions<dbSettings> settings) : base(settings) { }

        private payroll encryptdecrypt(payroll payroll, Int32 process) {

            var holder = new List<String>();
            if(process == 1) {
                // encrypt
                holder.Add(payroll.basicPay.ToString());
                payroll.basicPay = 0;
                holder.Add(payroll.monthlyAmount.ToString());
                payroll.monthlyAmount = 0;
                holder.Add(payroll.totalAmount.ToString());
                payroll.totalAmount = 0;
                payroll.entities.ForEach(e=> {
                    holder.Add(e.amount.ToString());
                    e.amount = 0;
                });
                var longString = String.Join(",",holder);
                var toEncrypt = new List<String>();
                for(Int32 ctr = 0; ctr<=longString.Length;ctr+=10) {
                    var cutter = 10;
                    if(ctr+cutter>=longString.Length) {
                        cutter = longString.Length - ctr;
                    }
                    toEncrypt.Add(longString.Substring(ctr,cutter));
                }
                payroll.encryptedAmounts = String.Join(",",toEncrypt.Select(x=>encryptDecrypt.EncryptString(x)));
            } else if(process == -1) {
                // decrypt

                var values = payroll.encryptedAmounts.Split(",").ToArray();
                var decValues = values.Select(x=>encryptDecrypt.DecryptString(x)).ToArray();
                var joined = String.Join("",decValues);
                var realValues = joined.Split(",");
                payroll.basicPay = Convert.ToDouble(realValues[0]);
                payroll.monthlyAmount = Convert.ToDouble(realValues[1]);
                payroll.totalAmount = Convert.ToDouble(realValues[2]);
                var ctr = 3;
                payroll.entities.ForEach(e=> {
                    e.amount = Convert.ToDouble(realValues[ctr]);
                    ctr++;
                });
                payroll.encryptedAmounts = "";
            }
        
            return payroll;
        }

        public void Add(payroll c) {

            c.createdAt = DateTime.Now;
            c.updatedAt = DateTime.Now;
            c = encryptdecrypt(c,1);
            _db.GetCollection<payroll>("payroll").InsertOneAsync(c);
        }

       
        public IEnumerable<payroll> getAll() {
            var c = _db.GetCollection<payroll>("payroll").Find(new BsonDocument()).ToList();
            return c.Select(x=>encryptdecrypt(x,-1));
        }

        public payroll Get(String id) {
            var query = Builders<payroll>.Filter.Eq(e => e.Id, id);
            var c = _db.GetCollection<payroll>("payroll").Find(query).FirstOrDefault();
            
            return encryptdecrypt(c,-1);
        }

       
        public payroll getSlug(String slug) {
            var query = Builders<payroll>.Filter.Eq(e => e.slug, slug);
            var fields = Builders<payroll>.Projection
                .Include(u => u.Id)
                .Include(u => u.slug)
                .Include(u => u.employee)
                .Include(u => u.branch)
                .Include(u => u.basicPay)
                .Include(u => u.cutoffPeriod)
                .Include(u => u.entities)
                .Include(u => u.totalAmount)
                .Include(u => u.createdAt)
                .Include(u => u.updatedAt);

            var c = _db.GetCollection<payroll>("payroll").Find(query).Project<payroll>(fields).FirstOrDefault();
            return encryptdecrypt(c,-1);
        }

        public void Update(String slug, payroll p) {
            
            p = encryptdecrypt(p,1);
            var filter = Builders<payroll>.Filter.Eq(e => e.slug, slug);
            var update = Builders<payroll>.Update
                .Set("employee", p.employee)
                .Set("branch", p.branch)
                .Set("basicPay", p.basicPay)
                .Set("cutoffPeriod", p.cutoffPeriod)
                .Set("entities", p.entities)
                .Set("totalAmount", p.totalAmount)
                .CurrentDate("updatedAt");
            var result = _db.GetCollection<payroll>("payroll").UpdateOneAsync(filter, update);

        }

        public void Remove(String slug) {
            var query = Builders<payroll>.Filter.Eq(e => e.slug, slug);
            var result = _db.GetCollection<payroll>("payroll").DeleteOneAsync(query);
            
        }

        public payroll checkSlug(String slug) {
            var query = Builders<payroll>.Filter.Eq(e => e.slug, slug);
            var c = _db.GetCollection<payroll>("payroll").Find(query).FirstOrDefault();

            return encryptdecrypt(c,-1);
        }

        public IEnumerable<payroll> search(String name) {
           
            var query = Builders<payroll>.Filter.Regex("name", new BsonRegularExpression(name, options: "i"));
            
            var fields = Builders<payroll>.Projection
                .Include(u => u.Id)
                .Include(u => u.slug)
                .Include(u => u.employee)
                .Include(u => u.branch)
                .Include(u => u.basicPay)
                .Include(u => u.cutoffPeriod)
                .Include(u => u.entities)
                .Include(u => u.totalAmount)
                .Include(u => u.createdAt)
                .Include(u => u.updatedAt);

            var c = _db.GetCollection<payroll>("payroll").Find(query).Project<payroll>(fields).ToList().AsQueryable();
            return c.Select(x=>encryptdecrypt(x,-1));
        }

        public List<payroll> getPayroll(String employeeSlug, DateTime from, DateTime to) {
            BsonDocument filter = new BsonDocument {
                {"employee.slug",employeeSlug},
                {"cutoffPeriod.from", new BsonDocument {
                    {"$gte",from.Date}
                }},
                {"cutoffPeriod.to", new BsonDocument {
                    {"$lte",to.Date.AddDays(1).AddSeconds(-1)}
                }}
            };

            IMongoCollection<payroll> coll = _db.GetCollection<payroll>("payroll");
            var list = coll.Find(filter).ToList();
            return list.Select(x=>encryptdecrypt(x,-1)).ToList();
        }

        public List<payroll> getPayrollByBranchSlug(String branchSlug, DateTime from, DateTime to) {
            BsonDocument filter = new BsonDocument {
                {"branch.slug",branchSlug},
                {"cutoffPeriod.from", new BsonDocument {
                    {"$gte",from.Date}
                }},
                {"cutoffPeriod.to", new BsonDocument {
                    {"$lte",to.Date.AddDays(1).AddSeconds(-1)}
                }}
            };

            IMongoCollection<payroll> coll = _db.GetCollection<payroll>("payroll");
            var list = coll.Find(filter).ToList();
            return list.Select(x=>encryptdecrypt(x,-1)).ToList();
        }

        public void addPayrolls(List<payroll> payrolls, DateTime from, DateTime to) {
            payrolls.ForEach(p=> {
                p = encryptdecrypt(p,1);
               var slug = p.employee.slug;
                var filter = new BsonDocument {
                    {"employee.slug",p.employee.slug},
                    {"cutoffPeriod.from", new BsonDocument {
                        {"$gte", from.Date},
                        {"$lte", from.Date.AddDays(1).AddSeconds(-1)}
                    }},
                    {"cutoffPeriod.to", new BsonDocument {
                        {"$gte", to.Date},
                        {"$lte", to.Date.AddDays(1).AddSeconds(-1)}
                    }}
                };

                var item = _db.GetCollection<payroll>("payroll").Find(filter).FirstOrDefault();

                if(item!=null) {
                    _db.GetCollection<payroll>("payroll").DeleteMany(filter);
                }
                var now = DateTime.Now;

                p.createdAt = now;
                p.updatedAt = now;
                _db.GetCollection<payroll>("payroll").InsertOne(p);
            });
        }

        //ENTITIES

        public payrollEntityList getEntitySlug(string entitySlug)
        {

            var query = Builders<payrollEntityList>.Filter.ElemMatch(x => x.entities, x => x.slug == entitySlug);
            var fields = Builders<payrollEntityList>.Projection
                .Include(u => u.Id)
                .Include(u => u.slug)
                .Include(u => u.employee)
                .ElemMatch(x => x.entities, x => x.slug == entitySlug);

            var unit = _db.GetCollection<payrollEntityList>("payroll").Find(query).Project<payrollEntityList>(fields).ToListAsync();
            return unit.Result.FirstOrDefault();
        }

        public void addEntity(payrollEntities u, string payrollSlug)
        {
            var query = Builders<payroll>.Filter.Eq(e => e.slug, payrollSlug);
            var update = Builders<payroll>.Update.Push(e => e.entities, u);
            //u.id = ObjectId.GenerateNewId().ToString();
            _db.GetCollection<payroll>("payroll").FindOneAndUpdateAsync(query, update);
        }

        public void updateEntity(string payrollSlug, string entitySlug, payrollEntities u)
        {

            var query = Builders<payroll>.Filter.And(Builders<payroll>.Filter.Eq(x => x.slug, payrollSlug),
            Builders<payroll>.Filter.ElemMatch(x => x.entities, x => x.slug == entitySlug));

            var update = Builders<payroll>.Update
                .Set("entities.$.name", u.name)
                .Set("entities.$.type", u.type)
                .Set("entities.$.amount", u.amount)
                .CurrentDate("updatedAt");

            var result = _db.GetCollection<payroll>("payroll").UpdateOneAsync(query, update);
        }

        public void removeEntity(String payrollSlug, string entitySlug)
        {

            var pull = Builders<payroll>.Update.PullFilter(x => x.entities, a => a.slug == entitySlug);
            var filter1 = Builders<payroll>.Filter.And(Builders<payroll>.Filter.Eq(a => a.slug, payrollSlug),
                          Builders<payroll>.Filter.ElemMatch(q => q.entities, t => t.slug == entitySlug));
            var result = _db.GetCollection<payroll>("payroll").UpdateOneAsync(filter1, pull);
        }

        public List<payEnt> getPayroll(string branchSlug)
        {

            //GET EMPLOYEES
            var gGetAllEmployees = Builders<users>.Filter.Eq(x => x.employment.branch, branchSlug);
            var employees = _db.GetCollection<users>("users").Find(gGetAllEmployees).ToListAsync().Result;

            List<payEnt> newPayroll = new List<payEnt>();
            foreach (var emp in employees)
            {
                string empSlug = emp.slug;
                double salary = Double.Parse(encryptDecrypt.DecryptString(emp.employment.salary));
                string taxCode = emp.taxCode;
                double sss = 0;
                double phil = 0;

                //GET PAGIBIG
                double pagibigContri = 100;

                //GET SSS CONTRIBUTION
                var qSal1 = Builders<sss>.Filter.Lte(e => e.salaryRange.from, salary);
                var qSal2 = Builders<sss>.Filter.Gte(e => e.salaryRange.to, salary);
                var getSSS = Builders<sss>.Filter.And(qSal1, qSal2);
                if (salary == 0)
                { sss = 0; }
                else
                { sss = _db.GetCollection<sss>("sss").Find(getSSS).FirstOrDefault().contribution.ee; }

                //GET PHIL CONTRIBUTION
                var qPhil1 = Builders<philhealth>.Filter.Lte(e => e.salaryRange.from, salary);
                var qPhil2 = Builders<philhealth>.Filter.Gte(e => e.salaryRange.to, salary);
                var getPhil = Builders<philhealth>.Filter.And(qPhil1, qPhil2);
                if (salary == 0)
                { phil = 0; }
                else
                { phil = _db.GetCollection<philhealth>("philhealth").Find(getPhil).FirstOrDefault().contribution.ee; }

                
                
                // start overwrite phil by new philhealth computation //

                phil = (salary * .0275) / 2;

                // end overwrite phil by new philhealth computation //



                //GET RICE SUBSIDY
                var qRice = Builders<users>.Filter.ElemMatch(e => e.employment.benefits, x => x.name == "Rice Subsidy");
                var rice = _db.GetCollection<users>("users").Find(qRice).FirstOrDefault();
                double rCheck = 0;
                if (rice == null)
                {
                    rCheck = 0.0;
                }
                else
                {
                    rCheck = rice.employment.benefits[0].amount;
                }


                //GET TAX
                // salary - deductions
                double annualTaxableCompensation = (sss + phil + pagibigContri) * 12;
                double exemptionAmount = 0;
                var qtax = Builders<tax>.Filter.Eq("exemption.status", taxCode);
                var tax = _db.GetCollection<tax>("tax").Find(qtax).FirstOrDefault();
                if (tax == null)
                {
                    exemptionAmount = 0.0;
                }
                else
                {
                    exemptionAmount = tax.exemption.amount;
                }


                double annualGrossPay = salary * 12;
                //double netTaxableIncome = ((annualGrossPay + ot) - exemption) - annualTaxableCompensation;

                ////FIX TAX
                //double fixTax = 0;
                //double fixRate = 0;
                //double excessOver = 0;
                //if (netTaxableIncome < 10000)
                //{
                //    fixTax = 0;
                //    fixRate = 0.05;
                //    excessOver = 0;
                //}
                //if (netTaxableIncome > 10000 && netTaxableIncome < 30000)
                //{
                //    fixTax = 500;
                //    fixRate = 0.1;
                //    excessOver = 10000;
                //}
                //if (netTaxableIncome > 30000 && netTaxableIncome < 70000)
                //{
                //    fixTax = 2500;
                //    fixRate = 0.15;
                //    excessOver = 30000;
                //}
                //if (netTaxableIncome > 70000 && netTaxableIncome < 140000)
                //{
                //    fixTax = 8500;
                //    fixRate = 0.20;
                //    excessOver = 70000;
                //}
                //if (netTaxableIncome > 140000 && netTaxableIncome < 250000)
                //{
                //    fixTax = 22500;
                //    fixRate = 0.25;
                //    excessOver = 140000;
                //}
                //if (netTaxableIncome > 250000 && netTaxableIncome < 500000)
                //{
                //    fixTax = 50000;
                //    fixRate = 0.30;
                //    excessOver = 250000;
                //}
                //if (netTaxableIncome > 500000)
                //{
                //    fixTax = 125000;
                //    fixRate = 0.32;
                //    excessOver = 500000;
                //}

                //var totalTaxAnnual = fixTax + ((netTaxableIncome - excessOver) * fixRate);
                //var taxPerMonth = totalTaxAnnual / 11;
                //var taxPerSemi = taxPerMonth / 2;
                ////return c.Result;

                newPayroll.Add(new payEnt
                {
                    id = emp.Id,
                    empCode = emp.employeeCode,
                    slug = emp.slug,
                    name = emp.name.last + ", " + emp.name.first + " " + emp.extension + " " + emp.name.middle,
                    salary = Double.Parse(encryptDecrypt.DecryptString(emp.employment.salary)),
                    taxCode = emp.taxCode,
                    philhealth = phil,
                    pagibig = pagibigContri,
                    sss = sss,
                    exemptionAmount = exemptionAmount,
                    annualTaxableCompensation = annualTaxableCompensation,
                    annualGrossPay = annualGrossPay
                });
            }

            return newPayroll;
        }


    }
}
