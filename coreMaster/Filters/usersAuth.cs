﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using kmbi_core_master.coreMaster.IRepository;

namespace kmbi_core_master.coreMaster.Filters
{
    public class usersAuth : TypeFilterAttribute
    {
        public usersAuth():base(typeof(ValidateUserIfExist))
        {
        }

        private class ValidateUserIfExist : IAsyncActionFilter
        {
            private readonly iusersRepository _userRepository;

            public ValidateUserIfExist(iusersRepository usersRepository)
            {   
                _userRepository = usersRepository;
            }

            public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
            {

                if (!context.HttpContext.Request.Headers.Keys.Contains("token"))
                {
                    context.HttpContext.Response.StatusCode = 400; //Bad Request                
                    context.Result = new JsonStringResult("{\"type\":\"error\",\"message\":\"Page not found\"}");
                    return;
                }
                else
                {
                    if (!_userRepository.checkValidUserKey(context.HttpContext.Request.Headers["token"]))
                    {
                        context.HttpContext.Response.StatusCode = 401; //UnAuthorized
                        //context.Result = new NotFoundObjectResult("Unauthorized user");
                        context.Result = new JsonStringResult("{\"type\":\"error\",\"message\":\"Unauthorized user\"}");
                        return;
                    }
                }
                await next();
            }
        }
    }
}
