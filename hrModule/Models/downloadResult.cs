﻿using System;
using System.Collections.Generic;

namespace kmbi_core_master.hrModule.Models
{    
    public class downloadResult
    {
        public DateTime startTime { get; set; }
        public DateTime endTime { get; set; }
        public Int32 duration { get; set; }
        public List<log> logs {get;set;}
        public String status {get;set;}
        public String specError {get;set;}
    }

    public class log
    {
        public String employeeCode {get;set;}
        public String name {get;set;}
        public DateTime time {get;set;}
        public String type {get;set;}
    }
}
