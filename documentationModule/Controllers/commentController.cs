using System;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Microsoft.Extensions.Options;
using System.Linq;

using kmbi_core_master.documentationModule.Models;
using kmbi_core_master.coreMaster.Filters;
using kmbi_core_master.coreMaster.Models;

using kmbi_core_master.documentationModule.Service;
using Microsoft.AspNetCore.Cors;

namespace kmbi_core_master.documentationModule.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/comment")]
    [errorHandler]
    public class commentController : Controller
    {
        private readonly commentService service;
        public commentController(IOptions<dbSettings> settings) 
        {
            service = new commentService(settings);
        }

        [HttpGetAttribute("")]
        public IActionResult Get()
        {
            IEnumerable<comment> comments = service.GetComments();

            return Ok(from com in comments select new {
                    slug=com.slug, 
                    email=com.email,
                    message=com.message,
                    commentOnType=com.commentOnType,
                    commentOnId= com.commentOnId
                }
                );
        }

        [HttpGet("{commentSlug}")]
        public IActionResult Get(String commentSlug)
        {
            comment comment = service.GetCommentBySlug(commentSlug);
            if(comment!=null)
            {
                return Ok(comment);
            }
            else
            {
                return Ok(new {});
            }
        }

        [HttpGet("{commentSlug}/{replySlug}")]
        public IActionResult Get(String commentSlug, String replySlug)
        {
           reply reply = service.GetReplyByCommentSlugReplySlug(commentSlug,replySlug);
           if(reply!=null)
           {
               return Ok(reply);
           } 
           else
           {
               return Ok(new {});
           }
        }
        
        [HttpPost("")]
        public IActionResult Post([FromBody]comment comment)
        {
            bool res = service.InsertComment(comment);
            if (res)
            {
                return Ok(new {
                    type="success",
                    message="Comment created successfully."
                });
            }
            else
            {
                return BadRequest(new {
                    type="error",
                    message="Comment create failed."
                });
            }
        }

        [HttpPost("{commentSlug}")]
        public IActionResult Post(String commentSlug, [FromBody]reply reply)
        {
            bool res = service.InsertReply(commentSlug,reply);
            if (res)
            {
                return Ok(new {
                    type="success",
                    message="Reply created successfully."
                });
            }
            else
            {
                return BadRequest(new {
                    type="error",
                    message="Reply create failed."
                });
            }
        }

        [HttpPutAttribute("{commentSlug}")]
        public IActionResult Put(String commentSlug, [FromBody]comment comment)
        {
            bool res = service.UpdateComment(commentSlug, comment);
            if(res)
            {
                return Ok(new {
                    type="success",
                    message= "Comment updated successfully."
                });
            }
            else
            {
                return BadRequest(new {
                    type="error",
                    message="Comment update failed."
                });
            }
        }

        [HttpPutAttribute("{commentSlug}/{replySlug}")]
        public IActionResult Put(String commentSlug, String replySlug, [FromBody]reply reply)
        {
            bool res = service.UpdateReply(commentSlug, replySlug, reply);
            if(res)
            {
                return Ok(new {
                    type="success",
                    message= "Reply updated successfully."
                });
            }
            else
            {
                return BadRequest(new {
                    type="error",
                    message="Reply update failed."
                });
            }
        }

        [HttpDeleteAttribute("{commentSlug}")]
        public IActionResult Delete(String commentSlug)
        {
            bool res = service.DeleteComment(commentSlug);

            if(res)
            {
                return Ok(new {
                    type="success",
                    message= "Comment deleted successfully."
                });
            }
            else
            {
                return BadRequest(new {
                    type="error",
                    message="Comment delete failed."
                });
            }
        }

        [HttpDeleteAttribute("{commentSlug}/{replySlug}")]
        public IActionResult Delete(String commentSlug, String replySlug)
        {
            bool res = service.DeleteReply(commentSlug, replySlug);
            if(res)
            {
                return Ok(new {
                    type="success",
                    message= "Reply deleted successfully."
                });
            }
            else
            {
                return BadRequest(new {
                    type="error",
                    message="Reply delete failed."
                });
            }
        }
    }
}