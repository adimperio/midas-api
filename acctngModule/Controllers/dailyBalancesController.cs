﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Filters;
using Microsoft.AspNetCore.Cors;
using kmbi_core_master.acctngModule.Interface;
using kmbi_core_master.acctngModule.Models;
using kmbi_core_master.coreMaster;
using System;

using System.Dynamic;
using System.Linq;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.coreMaster.IRepository;

namespace kmbi_core_master.acctngModule.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/dailyBalances")]
    [usersAuth]
    public class dailyBalancesController : Controller
    {
        private iAccountingShared _acc;
        private iusersRepository _usr;

        public dailyBalancesController(iAccountingShared acc, iusersRepository usr)
        {
            _acc = acc;
            _usr = usr;
        }

        // GET: 
        //[HttpGet]
        //public IActionResult Get([FromQuery]DateTime date)
        //{
        //    return Ok(_acc.dailyBalances(date));
        //}

        [HttpGet]
        public IActionResult Get([FromQuery]DateTime from, [FromQuery]DateTime to)
        {
            return Ok(_acc.dailyBalances(from, to));
        }

        [HttpGet("transactionList")]
        public IActionResult transactionList([FromQuery]DateTime from, [FromQuery]DateTime to) {
            return Ok(_acc.transactionList(from, to));
        }

        [HttpGet("employeeList")]
        public IEnumerable<users> GetAll()
        {
            var users = _usr.AllUsers();
            return users;
        }
    }
}
