﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Filters;
using Microsoft.AspNetCore.Cors;

namespace kmbi_core_master.coreMaster.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/loanMembers")]
    [usersAuth]
    public class loanMembersController : Controller
    {
        private readonly iloanMembersRepository _Repository;
        private readonly iusersRepository _logInfo;
        private string token;

        public loanMembersController(iloanMembersRepository Repositor, iusersRepository logInfo)
        {
            this._Repository = Repositor;
            _logInfo = logInfo;
        }

        // GET: 
        [HttpGet]
        public IEnumerable<loanMembers> GetAll()
        {
            var c = _Repository.getAll();
            return c;

        }

        public void logger(string message)
        {
            //Insert Log//
            token = HttpContext.Request.Headers["token"];
            var lInfo = _logInfo.getLoginInfo(token);
            string log = message;
            global.createLog(lInfo.id, lInfo.fullname, lInfo.employment.branch, log);
            //Insert Log//
        }

        //GET officer Slug
        //[HttpGet("{id}")]
        //public IActionResult getId(string id)
        //{

        //    var item = _Repository.getId(id);
        //    if (item == null)
        //    {
        //        return new JsonStringResult("[]");
        //    }

        //    return new ObjectResult(item);
        //}

        //[HttpGet("slug/{slug}")]
        //public IActionResult getSlug(string slug)
        //{

        //    var item = _Repository.getSlug(slug);
        //    if (item == null)
        //    {
        //        return new JsonStringResult("[]");
        //    }

        //    return new ObjectResult(item);
        //}


        //[HttpGet("loanId/{loanId}/memberId/{memberId}/scheduleId/{scheduleId}")]
        //public IActionResult getSchedule(string loanId, string memberId, string scheduleId)
        //{

        //    var item = _Repository.getSchedule(loanId, memberId, scheduleId);
        //    if (item == null)
        //    {
        //        return new JsonStringResult("[]");
        //    }

        //    return new ObjectResult(item);
        //}

        [HttpPost]
        //[errorHandler]
        public IActionResult create([FromBody] loanMembers b)
        {

            _Repository.add(b);

            logger("Insert new loan member " + b.name );

            return Ok(new
            {
                type = "success",
                message = b.name + " loan member created successfully.",
                slug = b.slug
            });

        }


        


        //[HttpPut("{id}")]
        //public IActionResult update(string id, [FromBody] loanMembers b)
        //{
        //    var item = _Repository.getId(id);
        //    if (item == null)
        //    {
        //        return BadRequest(new
        //        {
        //            type = "error",
        //            message = "No record found"
        //        });
        //    }
        //    _Repository.update(id, b);

        //    //Insert Log//
        //    string log = "Modified loan member " + b.name;
        //    int level = 1;
        //    string link = "link here";
        //    global.log(log, level, link);
        //    //Insert Log//

        //    return Ok(new
        //    {
        //        type = "success",
        //        message = b.name + " loan member updated successfully."
        //    });
        //}


        //[HttpPut("slug/{slug}")]
        //public IActionResult slugupdate(string slug, [FromBody] loanMembers b)
        //{
        //    var item = _Repository.getSlug(slug);
        //    if (item == null)
        //    {
        //        return BadRequest(new
        //        {
        //            type = "error",
        //            message = "No record found"
        //        });
        //    }
        //    _Repository.updateSlug(slug, b);

        //    //Insert Log//
        //    string log = "Modified loan member " + b.name;
        //    int level = 1;
        //    string link = "link here";
        //    global.log(log, level, link);
        //    //Insert Log//

        //    return Ok(new
        //    {
        //        type = "success",
        //        message = b.name + " loan member updated successfully."
        //    });
        //}



        //[HttpDelete("{id}")]
        //public IActionResult delete(string id)
        //{

        //    var item = _Repository.getId(id);
        //    if (item == null)
        //    {
        //        return BadRequest(new
        //        {
        //            type = "error",
        //            message = "No record found."
        //        });
        //    }

        //    _Repository.remove(id);

        //    //Insert Log//
        //    string log = "Delete loan member " + item.name;
        //    int level = 1;
        //    string link = "link here";
        //    global.log(log, level, link);
        //    //Insert Log//

        //    return Ok(new
        //    {
        //        type = "success",
        //        message = item.name + " loan member deleted successfully."
        //    });
        //}


        //[HttpDelete("slug/{slug}")]
        //public IActionResult slugdelete(string slug)
        //{

        //    var item = _Repository.getSlug(slug);
        //    if (item == null)
        //    {
        //        return BadRequest(new
        //        {
        //            type = "error",
        //            message = "No record found."
        //        });
        //    }

        //    _Repository.removeSlug(slug);

        //    //Insert Log//
        //    string log = "Delete loan member " + item.name;
        //    int level = 1;
        //    string link = "link here";
        //    global.log(log, level, link);
        //    //Insert Log//

        //    return Ok(new
        //    {
        //        type = "success",
        //        message = item.name + " loan member deleted successfully."
        //    });
        //}


    }
}
