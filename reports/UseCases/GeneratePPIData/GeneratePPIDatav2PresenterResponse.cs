namespace kmbi_core_master.reports.UseCases.GeneratePPIData
{
    public class GeneratePPIDatav2PresenterResponse {
        public GeneratePPIDataResponse data {get;set;}
        public byte[] bytes {get;set;}
    }
}