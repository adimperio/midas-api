﻿

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

using kmbi_core_master.coreMaster;
using kmbi_core_master.coreMaster.Filters;
using kmbi_core_master.payrollModule.IRepository;
using kmbi_core_master.payrollModule.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.helpers.interfaces;

namespace kmbi_core_master.payrollModule.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/payroll")]
    [usersAuth]
    public class payrollController : Controller
    {
        private readonly ipayrollRepository _Repository;
        private readonly iusersRepository _logInfo;
        private readonly iNotification _inotification;
        private string token;

        public payrollController(ipayrollRepository Repository, iusersRepository logInfo, iNotification inotification)
        {
            this._Repository = Repository;
            _logInfo = logInfo;

            _inotification = inotification;
        }

        public void logger(string message)
        {
            //Insert Log//
            token = HttpContext.Request.Headers["token"];
            var lInfo = _logInfo.getLoginInfo(token);
            string log = message;
            global.createLog(lInfo.id, lInfo.fullname, lInfo.employment.branch, log);
            //Insert Log//
        }

        // GET: 
        [HttpGet]
        public IEnumerable<payroll> GetAll()
        {
            var p = _Repository.getAll();
            return p;

        }

        // GET id
        [HttpGet("{id:length(24)}")]
        public IActionResult Get(string id)
        {
            var item = _Repository.Get(id);
            if (item == null)
            {
               
                return Ok(new { });
            }

            return new ObjectResult(item);
        }

        
        [HttpGet("{slug}")]
        public IActionResult getSlug(string slug)
        {

            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return Ok(new { });
            }
           
            return new ObjectResult(item);
        }

       

        // SEARCH name
        [HttpGet("search")]
        public IActionResult search([FromQuery]string name)
        {

            var item = _Repository.search(name);
            if (item.Count() == 0)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }



        // GET check unique slug
        [HttpGet("checkSlug")]
        public IActionResult checkSlug([FromQuery]string slug)
        {

            var item = _Repository.checkSlug(slug);
            if (item == null)
            {
                return Ok(new
                {
                    available = 1
                });
            }

            return Ok(new
            {
                available = 0
            });
        }


       
        // POST 
        [HttpPost]
        //[usersAccess(access: "user-add")]
        public IActionResult createEntity([FromBody] payroll p)
        {
            
            _Repository.Add(p);

            logger( "Insert new payroll for " + p.employee.name);
           
            return Ok(new
            {
                type = "success",
                message = p.employee.name + " created successfully.",
                slug = p.slug
            });

        }

        
        // PUT 
        [HttpPut("{slug}")]
        public IActionResult Put(string slug, [FromBody] payroll p)
        {
            var item = _Repository.checkSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.Update(slug, p);

            logger("Modified payroll for " + p.employee.name);

            return Ok(new
            {
                type = "success",
                message = p.employee.name + " updated successfully."
            });
        }

        // DELETE

        [HttpDelete("{slug}")]
        public IActionResult delete(string slug)
        {

            var item = _Repository.checkSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.Remove(slug);

            logger("Remove payroll for " + item.employee.name);

            return Ok(new
            {
                type = "success",
                message = item.employee.name + " deleted successfully.",
                slug = item.slug
            });
        }

        // POST Entity
        [HttpPost("entitySlug={slug}")]
        public IActionResult addUnits([FromBody] payrollEntities u, string slug)
        {
            _Repository.addEntity(u, slug);

            logger("Insert new entity " + u.name);

            return Ok(new
            {
                type = "success",
                message = u.name + " created successfully.",
                slug = u.slug,
                id = u.id
            });
        }

        // PUT entities
        [HttpPut("payrollSlug={payrollSlug}&entitySlug={entitySlug}")]
        public IActionResult updateEntity(string payrollSlug, string entitySlug, [FromBody] payrollEntities u)
        {
            var item = _Repository.getEntitySlug(entitySlug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _Repository.updateEntity(payrollSlug, entitySlug, u);

            logger("Modified entity " + u.name);

            return Ok(new
            {
                type = "success",
                message = u.name + " updated successfully."
            });

        }

        //DELETE Entity
        [HttpDelete("payrollSlug={payrollSlug}&entitySlug={entitySlug}")]
        public IActionResult deleteUnits(string payrollSlug, string entitySlug)
        {
            var item = _Repository.getEntitySlug(entitySlug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.removeEntity(payrollSlug, entitySlug);

            logger("Delete entity " + item.entities[0].name);

            return Ok(new
            {
                type = "success",
                message = item.entities[0].name + " deleted successfully."
            });
        }


        [HttpGet("getPayroll/{branchSlug}")]
        public IActionResult getPayroll(string branchSlug)
        {

            var item = _Repository.getPayroll(branchSlug);
            if (item == null)
            {
                return Ok(new { });
            }

            return new ObjectResult(item);
        }

        [HttpGet("getPayroll/employee/{employeeSlug}")]
        public IActionResult getPayroll(String employeeSlug, [FromQuery]DateTime from, [FromQuery]DateTime to){
            var item = _Repository.getPayroll(employeeSlug, from, to);
            if (item != null){
                return Ok(item);
            } else {
                return Ok(new {});
            }
        }

        [HttpPost("list")]
        public IActionResult addPayrolls([FromBody]List<payroll> ps, [FromQuery]DateTime from,[FromQuery]DateTime to) {
            var err = "";
            if(from==null&&to==null) {
                err += "Invalid parameter: from and to";
            }

            if(err!="") {
                err += ", ";
            }

            if(ps==null) {
                err += "Invalid parameter: payroll array";
            }

            if(err!="") {
                return BadRequest(new {
                    type = "error",
                    message = err
                });
            }
            
            _Repository.addPayrolls(ps, from, to);

            logger("Insert new payrolls [from:" + from.Date + ", to:" + to.Date + "]");

            return Ok(new {
                type = "success",
                message = "List of payroll succesfully saved"
            });

        }

        [HttpGet("getPayroll/branch/{branchSlug}")]
        public IActionResult getPayrollByBranchSlug(String branchSlug, [FromQuery]DateTime from, [FromQuery]DateTime to){
            var item = _Repository.getPayrollByBranchSlug(branchSlug, from, to);
            if (item != null){
                return Ok(item);
            } else {
                return Ok(new {});
            }
        }

        public class payrollKeyChangeObject {
            public uploader uploader {get;set;}
            public List<String> emails {get;set;}
        }

        public class uploader {
            public String name {get;set;}
            public String email {get;set;}
        }
        
        [HttpPost("emailPayrollKeyChange")]
        public IActionResult emailPayrollKeyChange([FromBody]payrollKeyChangeObject payrollKeyChangeObject) {
            _inotification.from = "no-reply@kmbi.org.ph";
            var tos = "";

            payrollKeyChangeObject.emails.ForEach(e=>{
                tos += e + ", ";
            });
            tos = tos.Substring(0,tos.Length -2);
            _inotification.to = tos;
            _inotification.subject = "Payroll Key Changes";
            _inotification.message = String.Format(@"Hi Sha/Marvs/Wi,<br><br>
                                        A new payroll key has been uploaded by {0}. Please be guided accordingly.<br><br>
                                    Thanks,<br><br>System Administrator",payrollKeyChangeObject.uploader.name);
            iResult result = _inotification.Send();
            
            return Ok(new {
                type = result.code,
                message = result.description
            });
        }
    }

    
}
