﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.coreMaster.Models
{
    public class officers
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("unitId")]
        public string unitId { get; set; }

        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("designation")]
        public string designation { get; set; }

        [BsonElement("centers")]
        public IList<centers> centers { get; set; }

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }
    }

    public class centers
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("address")]
        public string address { get; set; }

        [BsonElement("code")]
        public string code { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }
    }


    public class centerList
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("centers")]
        public IList<centers> centers { get; set; }


    }

    public class officersCenterList
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("unitId")]
        public string unitId { get; set; }

        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("designation")]
        public string designation { get; set; }

        [BsonElement("centers")]
        public IList<centers> centers { get; set; }
        
        //[BsonElement("createdAt")]
        //public DateTime createdAt { get; set; }

        //[BsonElement("updatedAt")]
        //public DateTime updatedAt { get; set; }

        public IList<centerIns> centerInstances { get; set; }

    }

    public class centerIns
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("referenceId")]
        public string referenceId { get; set; }

        [BsonElement("centerId")]
        public string centerId { get; set; }

        [BsonElement("branchId")]
        public string branchId { get; set; }

        [BsonElement("branchSlug")]
        public string branchSlug { get; set; }

        [BsonElement("unitId")]
        public string unitId { get; set; }

        [BsonElement("unitSlug")]
        public string unitSlug { get; set; }

        [BsonElement("officerId")]
        public String officerId { get; set; }

        [BsonElement("officerSlug")]
        public String officerSlug { get; set; }

        [BsonElement("address")]
        public string address { get; set; }

        [BsonElement("code")]
        public string code { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("loanCycle")]
        public int loanCycle { get; set; }

        [BsonElement("status")]
        public string status { get; set; }

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }
    }

    public class syncOfficers
    {
        [BsonId]
        public ObjectId Id { get; set; }

        [BsonElement("unitId")]
        public string unitId { get; set; }

        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("designation")]
        public string designation { get; set; }

        [BsonElement("centers")]
        public IList<centers> centers { get; set; }

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }
    }
}
