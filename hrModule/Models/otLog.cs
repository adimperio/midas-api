﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace kmbi_core_master.hrModule.Models
{
    public class otLog
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }
        public String slug { get; set; }
        public String type { get; set; }
        public String code { get; set; }
        public otLogEmployee employee { get; set; }
        public otLogDates dates { get; set; }
        public String holidayType {get;set;}
        public Double duration {get;set;}
        public Double nd {get;set;}
        public String reason { get; set; }
        public String attachmentPath { get; set; }
        public String receivedBy { get; set; }
        public DateTime? dateReceived { get; set; }
        public DateTime? dateFiled { get; set; }
        public String supervisor{get;set;}
        public String comment {get;set;}
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
    }

    public class otLogEmployee {
        public String slug {get;set;}
        public String code {get;set;}
        public otLogEmployeeName name {get;set;}
    }

    public class otLogEmployeeName {
        public String last {get;set;}
        public String first {get;set;}
        public String middle {get;set;}
        public String full {get;set;}
    }

    public class otLogDates {
        public DateTime from {get;set;}
        public DateTime to {get;set;}
    }
}
