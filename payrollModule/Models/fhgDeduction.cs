﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace kmbi_core_master.payrollModule.Models
{
    public class fhgDeduction
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        public fhgDeductionCutOff cutoff {get;set;}
        public fhgDeductionDeductions deductions {get;set;}
        public String employeeCode {get;set;}
        public String fullname {get;set;}
        public DateTime createdAt {get;set;}
        public DateTime updatedAt {get;set;}
    }

    public class fhgDeductionCutOff
    {
        public DateTime from {get;set;}
        public DateTime to {get;set;}
    }

    public class fhgDeductionDeductions
    {
        public Double fidelityBond {get;set;}
        public Double fhgLoanPayable {get;set;}
        public Double fhgShareCapital {get;set;}
        public Double fhgSavings {get;set;}
        public Double fhgInsurance {get;set;}
        public Double fhgOthers {get;set;}
    }
}
