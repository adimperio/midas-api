using System.Collections.Generic;
using System.Linq;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.spm.IRepository;
using kmbi_core_master.spm.Models;
using Microsoft.Extensions.Options;

namespace kmbi_core_master.spm.Repository {
    public class LikelihoodRepository : iLikelihoodRepository
    {
        private IOptions<likelihoodsSettings> _lsSettings;

        public LikelihoodRepository(IOptions<likelihoodsSettings> lsSettings) {
            _lsSettings = lsSettings;
        }

        public Likelihood GetLikelihoodByScore(int Score)
        {
            var item = _lsSettings.Value._likelihoods.Where(xy=>xy.Score == Score).FirstOrDefault();
            return item;
        }

        public Likelihood[] GetLikelihoods()
        {
            return _lsSettings.Value._likelihoods;
            
        }
    }
}