﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using Microsoft.Net.Http.Headers;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using AspNetCoreCsvImportExport.Formatters;
using kmbi_core_master.acctngModule.Interface;
using kmbi_core_master.acctngModule.Repository;
using kmbi_core_master.acctngModule.Shared;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.coreMaster.Repository;
using kmbi_core_master.documentationModule.iRepositories;
using kmbi_core_master.documentationModule.Repositories;
using kmbi_core_master.filesModule.Interface;
using kmbi_core_master.filesModule.Repository;
using kmbi_core_master.helpers;
using kmbi_core_master.helpers.interfaces;
using kmbi_core_master.hrModule.Interface;
using kmbi_core_master.hrModule.Repository;
using kmbi_core_master.payrollModule.IRepository;
using kmbi_core_master.payrollModule.Repository;
using kmbi_core_master.reports.IRepository;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using kmbi_core_master.spm.Repository;
using kmbi_core_master.spm.IRepository;
using kmbi_core_master.ticketing.Repository;
using kmbi_core_master.ticketing.IRepository;
using kmbi_core_master.adminModule.Inv.IRepository;
using kmbi_core_master.adminModule.Inv.Repository;
using kmbi_core_master.acctngModule.Models;
using kmbi_core_master.reports.UseCases;
using kmbi_core_master.reports.UseCases.GenerateLrCbuSchedule;
using kmbi_core_master.reports.UseCases.GenerateResignedSummary;
using kmbi_core_master.reports.UseCases.GeneratePPIData;
using kmbi_core_master.trainingModule.Repository;
using kmbi_core_master.trainingModule.IRepository;
using kmbi_core_master.reports.UseCases.GenerateTINIssuanceRepot;
using UseCases.GenerateDtrForPayroll;
using UseCases.GenerateLeavesForPayroll;
using UseCases.GenerateOtForPayroll;
using kmbi_core_master.hrModule.UseCase;

namespace kmbi_core_master
{
    public class Startup
    {
        IHostingEnvironment _env;
        String _curdir = "";

        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            _env = env;
            _curdir = env.ContentRootPath;

            if (env.IsEnvironment("Production") || env.IsEnvironment("Staging") || env.IsEnvironment("Offline"))
            {
                if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux) || RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
                    _curdir = "/var/www/aspnetcore/";
            }

            var builder = new ConfigurationBuilder()
                .SetBasePath(_curdir)
                .AddEnvironmentVariables()
                //.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddJsonFile($"dbConfig.{env.EnvironmentName}.json", optional: true)
                //.AddJsonFile("dbConfig.json", optional: true)
                .AddJsonFile("awsConfig.json", optional: true)
                .AddJsonFile("fsSetting.json")
                .AddJsonFile("fsSetting2.json")
                .AddJsonFile("likelihoodsSettings.json");

            if (env.IsEnvironment("Development"))
            {
                // This will push telemetry data through Application Insights pipeline faster, allowing you to view results immediately.
                builder.AddApplicationInsightsSettings(developerMode: true);
            }

            builder.AddEnvironmentVariables();
            configuration = builder.Build();
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            Stimulsoft.Base.StiLicense.Key = "6vJhGtLLLz2GNviWmUTrhSqnOItdDwjBylQzQcAOiHk0HJXigydLF7FKpx5iaKNH3hI02uXREug4W3kVLt9KkBwGiZ" +
            "+335Y/gQ19Wq4opnvv581jbnn/t0TF1vtLYHiFd5U/5WwcSJAdDvHoskkdL/Rxr1YNFUNVx2phFISzcXcIXS3gOisH" +
            "PzN+0iRCD1ukU36u3ZTz7P/UNUaGEtocx8yrkwId+y8pSjaapgiDmLYz2aH/kORwNU720omCyxPhkOz1DN1eFmfrtz" +
            "hCRKY+KCoCJqo1hQR5gaZv6BMcgH+DQDSIjWrGJtQ2wugK2Lzcg/RQD1TAbZtCkWMvvIb6a8K9xDZkuUngzUlypr5H" +
            "jcg79EUnazanLaCKRgdO3cr3KMFJlO7z3b4S6JgwsGHTYZLyJHRbUB/04hnu5tfjor4nwWvlnKHG368z7Ap0bt611M" +
            "0LIAm+UWxyRk3SCDfKJqYv32EjQ8HxShcblMuyXoTxfh/bX4OdTSMSKRj4IgH1MddZZJCtVb7NI5F2IyKgeZK89W4f" +
            "QagPgYwar2yp4zRD3lPPF7QiXifaJf5n8PeO";

            //Allow CORS
            services.AddCors(options =>
            {
                options.AddPolicy("allowCors",
                    builder => builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            });

            services.AddOptions();
            services.AddApplicationInsightsTelemetry(Configuration);
            services.Configure<dbSettings>(Configuration);
            services.Configure<awsSettings>(Configuration);
            services.Configure<biometricSettings>(Configuration.GetSection("biometricSettings"));
            services.Configure<departmentAcronymSettings>(Configuration.GetSection("departmentAcronymSettings"));
            services.Configure<syncSettings>(Configuration.GetSection("syncSettings"));
            services.Configure<signatoriesSettings>(Configuration.GetSection("signatoriesSettings"));
            services.Configure<reportSetting>(Configuration.GetSection("reportSettings"));
            services.Configure<fsSetting>(Configuration.GetSection("fsSettings"));
            services.Configure<fsSetting2>(Configuration.GetSection("fsSettings2"));
            services.Configure<miSettings>(Configuration.GetSection("miSettings"));
            services.Configure<rootUrlSettings>(Configuration.GetSection("rootUrlSettings"));
            services.Configure<localFileRepositorySettings>(Configuration.GetSection("localFileRepositorySettings"));
            services.Configure<likelihoodsSettings>(Configuration);
            //services.AddDirectoryBrowser();

            //DOCUMENTATION MODULE
            services.AddSingleton<iholidayRepository, holidayRepository>();
            services.AddSingleton<itutorialRepository, tutorialRepository>();

            //CORE MODULE
            services.AddSingleton<iusersRepository, usersRepository>();
            services.AddSingleton<iregionsRepository, regionsRepository>();
            services.AddSingleton<iauthRepository, authRepository>();
            services.AddSingleton<iactivityRepository, activityRepository>();
            services.AddSingleton<iresetPasswordRepository, resetPasswordRepository>();
            services.AddSingleton<ibranchRepository, branchRepository>();
            services.AddSingleton<iofficersRepository, officersRepository>();
            services.AddSingleton<iNotification, emailv2>();
            services.AddSingleton<iglobal2, global2>();
            services.AddSingleton<idepartmentRepository, departmentRepository>();
            services.AddSingleton<ipositionRepository, positionRepository>();

            if (!_env.IsProduction())
                services.AddSingleton<iupload, localUpload>();
            else
                services.AddSingleton<iupload, s3Upload>();

            services.AddSingleton<iimportCsvRepository, importCSVRepository>();
            services.AddSingleton<iaddressRepository, addressRepository>();
            services.AddSingleton<ilogsRepository, logsRepository>();

            //MF MODULE
            services.AddSingleton<iclientsRepository, clientsRepository>();
            services.AddSingleton<iloanTypesRepository, loanTypesRepository>();
            services.AddSingleton<icenterInstanceRepository, centerInstanceRepository>();
            services.AddSingleton<iloanRepository, loanRepository>();
            services.AddSingleton<iloanMembersRepository, loanMembersRepository>();
            services.AddSingleton<iprovidersRepository, providersRepository>();
            services.AddSingleton<icheckVoucherRepository, checkVoucherRepository>();
            services.AddSingleton<ipaymentLoansRepository, paymentLoansRepository>();
            services.AddSingleton<ipaymentUpfrontRepository, paymentUpfrontRepository>();
            services.AddSingleton<ipaymentLoanFullRepository, paymentLoansFullRepository>();
            services.AddSingleton<icbuLogsRepository, cbuLogsRepository>();
            services.AddSingleton<icbuWithdrawalRepository, cbuWithdrawalRepository>();
            services.AddSingleton<iamortRepository, amortRepository>();

            //HR MODULE
            services.AddSingleton<iattendanceLogsRepository, attendanceLogsRepository>();
            services.AddSingleton<idtrRepository, dtrRepository>();
            services.AddSingleton<ileaveLogsRepository, leaveLogsRepository>();
            services.AddSingleton<ipayrollDeductionRepository, payrollDeductionRepository>();
            services.AddSingleton<iotLogsRepository, otLogRepository>();
            services.AddSingleton<isalaryUpdateRepository, salaryUpdateRepository>();
            services.AddSingleton<IUploadDtrTemplate<UploadDtrTemplateRequest,UploadDtrTemplateRespnse>, UploadDtrTemplate>();

            //ACCOUNTING MODULE
            services.AddSingleton<icoaRepository, coaRepository>();
            services.AddSingleton<iacctngRfpRepository, acctngRfpRepository>();
            services.AddSingleton<iewtRepository, ewtRepository>();
            services.AddSingleton<iacctngEntityRepository, acctngEntityRepository>();
            services.AddSingleton<icashReceiptRepository, cashReceiptRepository>();
            services.AddSingleton<iacctngCvRepository, acctngCvRepository>();
            services.AddSingleton<iacctngJvRepository, acctngJvRepository>();
            services.AddSingleton<iacctEntryTemplateCVRepository, acctEntryTemplateCVRepository>();
            services.AddSingleton<iacctEntryTemplateJVRepository, acctEntryTemplateJVRepository>();
            services.AddSingleton<iacctEntryTemplateORRepository, acctEntryTemplateORRepository>();
            services.AddSingleton<iacctngDbaRepository, acctngDbaRepository>();
            services.AddSingleton<iAccountingShared, AccountingShared>();
            services.AddSingleton<ibudgetRepository, budgetRepository>();
            services.AddSingleton<idepartmentBudgetRepository, departmentBudgetRepository>();

            //REPORTS MODULE
            services.AddSingleton<iReport, report>();
            services.AddSingleton<ireporting, reporting>();
            services.AddSingleton<ihtmlBuilder, htmlBuilder>();
            services.AddSingleton<idtrManualRepository, dtrManualRepository>();


            //PAYROLL MODULE
            services.AddSingleton<ipayrollEntityRepository, payrollEntityRepository>();
            services.AddSingleton<ipayrollRepository, payrollRepository>();
            services.AddSingleton<isssRepository, sssRepository>();
            services.AddSingleton<iphilhealthRepository, philhealthRepository>();
            services.AddSingleton<itaxRepository, taxRepository>();
            services.AddSingleton<icutOffRepository, cutOffRepository>();
            services.AddSingleton<ikeysRepository, keysRepository>();
            services.AddSingleton<ifhgDeductionsRepository, fhgDeductionsRepository>();

            //FILES MODULE
            services.AddSingleton<ifilesRepository, filesRepository>();
            services.AddSingleton<icategoryRepository, categoryRepository>();

            //SPM MODULE
            services.AddSingleton<iquestionairesRepository, questionairesRepository>();
            services.AddSingleton<iclientProfilinRepository, clientProfilingRepository>();
            services.AddSingleton<ippiRepository, ppiRepository>();
            services.AddSingleton<iLikelihoodRepository, LikelihoodRepository>();
            
            //TICKETING MODULE
            services.AddSingleton<IIssueRepository, IssueRepository>();

            //INVENTORY MODULE
            services.AddSingleton<iitemsRepository, itemsRepository>();
            services.AddSingleton<iinventoryRepository, inventoryRepository>();

            //TRAINING MODULE
            services.AddSingleton<itrainingRepository, trainingRepository>();
            services.AddSingleton<itrainingInstancesRepository, trainingInstancesRepository>();

            //SHARED
            services.AddSingleton<GetBackendInfo>();
            services.AddSingleton<UrlGetter>();
            

            //REPORTS
            services.AddSingleton<IUseCase<GenerateLrCbuScheduleRequest, GenerateLrCbuScheduleResponse>, GenerateLrCbuSchedule>();
            services.AddSingleton<IPresent<GenerateLrCbuScheduleResponse, GenerateLrCbuShedulePresenterResponse>, GenerateLrCbuShedulePresenter>();

            services.AddSingleton<IUseCase<GenerateResignedSummaryRequest, GenerateResignedSummaryResponse>, GenerateResignedSummary>();
            services.AddSingleton<IPresent<GenerateResignedSummaryResponse, GenerateResignedSummaryPresenterResponse>, GenerateResignedSummaryPresenter>();
            
            services.AddSingleton<IUseCase<GeneratePPIDataRequest, GeneratePPIDataResponse>, GeneratePPIData>();
            services.AddSingleton<IPresent<GeneratePPIDataResponse, GeneratePPIDataPresenterResponse>, GeneratePPIDataPresenter>();
            services.AddSingleton<IUseCase<GenerateTINIssuanceReportRequest, GenerateTINIssuanceReportResponse>, GenerateTINIssuanceReport>();
            services.AddSingleton<IPresent<GenerateTINIssuanceReportResponse, GenerateTINIssuanceReportPresenterResponse>, GenerateTINIssuanceReportPresenter>();
            services.AddSingleton<IPresent<GeneratePPIDataResponse, GeneratePPIDatav2PresenterResponse>, GeneratePPIDatav2Presenter>();
            
            services.AddSingleton<IUseCase<GenerateDtrForPayrollUseCaseRequest, GenerateDtrForPayrollUseCaseResponse>, GenerateDtrForPayrollUseCase>();
            services.AddSingleton<IUseCase<GenerateLeavesForPayrollUseCaseRequest, GenerateLeavesForPayrollUseCaseResponse>, GenerateLeavesForPayrollUseCase>();
            services.AddSingleton<IUseCase<GenerateOtForPayrollUseCaseRequest, GenerateOtForPayrollUseCaseResponse>, GenerateOtForPayrollUseCase>();


            var cult = new Microsoft.AspNetCore.Localization.RequestCulture("en-US");
            cult.Culture.DateTimeFormat = new DateTimeFormatInfo
            {
                ShortDatePattern = "MM/dd/yyyy",
            };

            cult.Culture.NumberFormat = new NumberFormatInfo {
                NumberDecimalDigits = 2,
                CurrencyDecimalDigits = 2,
                PercentDecimalDigits = 2
            };

            services.Configure<RequestLocalizationOptions>(options =>
            {
                options.DefaultRequestCulture = cult;
            });

            services.AddMvc();
            //services.AddApiVersioning();

            services.AddDistributedMemoryCache(); // Adds a default in-memory implementation of IDistributedCache
            services.AddSession();

            var csvFormatterOptions = new CsvFormatterOptions();

            services.AddMvc(options =>
            {
                options.InputFormatters.Add(new CsvInputFormatter(csvFormatterOptions));
                options.OutputFormatters.Add(new CsvOutputFormatter(csvFormatterOptions));
                options.FormatterMappings.SetMediaTypeMappingForFormat("csv", MediaTypeHeaderValue.Parse("text/csv"));
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            //mongodb options for fixing when retrieving UTC dates.
            MongoDB.Bson.Serialization.BsonSerializer.RegisterSerializer(typeof(DateTime), MongoDB.Bson.Serialization.Serializers.DateTimeSerializer.LocalInstance);

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            if (!_env.IsProduction())
            {

                if (!_env.IsProduction()) {
                    if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux) || RuntimeInformation.IsOSPlatform(OSPlatform.OSX)) {
                        _curdir = Configuration["localFileRepositorySettings:pathLinx"];
                    } else {
                        _curdir = Configuration["localFileRepositorySettings:pathWin"];
                    }
                }

                if (!System.IO.Directory.Exists(_curdir + "/wwwroot/kmbidevrepo"))
                    System.IO.Directory.CreateDirectory(_curdir + "/wwwroot/kmbidevrepo");

                if (!System.IO.Directory.Exists(env.ContentRootPath + "/wwwroot/kmbidevrepo/images"))
                    System.IO.Directory.CreateDirectory(env.ContentRootPath + "/wwwroot/kmbidevrepo/images");

                if (!System.IO.Directory.Exists(_curdir + "/wwwroot/kmbidevrepo/uploads"))
                    System.IO.Directory.CreateDirectory(_curdir + "/wwwroot/kmbidevrepo/uploads");

                if (!System.IO.Directory.Exists(_curdir + "/wwwroot/kmbidevrepo/uploads/apk"))
                    System.IO.Directory.CreateDirectory(_curdir + "/wwwroot/kmbidevrepo/uploads/apk");

                if (!System.IO.Directory.Exists(_curdir + "/wwwroot/kmbidevrepo/uploads/employees"))
                    System.IO.Directory.CreateDirectory(_curdir + "/wwwroot/kmbidevrepo/uploads/employees");

                if (!System.IO.Directory.Exists(_curdir + "/wwwroot/kmbidevrepo/uploads/employees/documents"))
                    System.IO.Directory.CreateDirectory(_curdir + "/wwwroot/kmbidevrepo/uploads/employees/documents");

                if (!System.IO.Directory.Exists(_curdir + "/wwwroot/kmbidevrepo/uploads/employees/pictures"))
                    System.IO.Directory.CreateDirectory(_curdir + "/wwwroot/kmbidevrepo/uploads/employees/pictures");

                if (!System.IO.Directory.Exists(_curdir + "/wwwroot/kmbidevrepo/uploads/clients/signatures"))
                    System.IO.Directory.CreateDirectory(_curdir + "/wwwroot/kmbidevrepo/uploads/clients/signatures");
                
                if (!System.IO.Directory.Exists(_curdir + "/wwwroot/kmbidevrepo/uploads/clients/pictures"))
                    System.IO.Directory.CreateDirectory(_curdir + "/wwwroot/kmbidevrepo/uploads/clients/pictures");

                if (!System.IO.Directory.Exists(_curdir + "/wwwroot/kmbidevreporeports"))
                    System.IO.Directory.CreateDirectory(_curdir + "/wwwroot/kmbidevreporeports");

                // logs

                if (!System.IO.Directory.Exists(env.ContentRootPath + "/logs"))
                    System.IO.Directory.CreateDirectory(env.ContentRootPath + "/logs");

                app.UseStaticFiles(new StaticFileOptions()
                {
                    FileProvider = new PhysicalFileProvider(
                    Path.Combine(env.ContentRootPath, @"wwwroot/kmbidevrepo/images")),
                    RequestPath = new PathString("/kmbidevrepo/images")
                });

                app.UseStaticFiles(new StaticFileOptions()
                {
                    FileProvider = new PhysicalFileProvider(
                    Path.Combine(_curdir, @"wwwroot/kmbidevrepo/uploads/employees/documents")),
                    RequestPath = new PathString("/kmbidevrepo/uploads/employees/documents")
                });

                app.UseStaticFiles(new StaticFileOptions()
                {
                    FileProvider = new PhysicalFileProvider(
                    Path.Combine(_curdir, @"wwwroot/kmbidevrepo/uploads/employees/pictures")),
                    RequestPath = new PathString("/kmbidevrepo/uploads/employees/pictures")
                });

                app.UseStaticFiles(new StaticFileOptions()
                {
                    FileProvider = new PhysicalFileProvider(
                    Path.Combine(_curdir, @"wwwroot/kmbidevrepo/uploads/clients/pictures")),
                    RequestPath = new PathString("/kmbidevrepo/uploads/clients/pictures")
                });

                app.UseStaticFiles(new StaticFileOptions()
                {
                    FileProvider = new PhysicalFileProvider(
                    Path.Combine(_curdir, @"wwwroot/kmbidevrepo/uploads/clients/signatures")),
                    RequestPath = new PathString("/kmbidevrepo/uploads/clients/signatures")
                });
                app.UseStaticFiles(new StaticFileOptions()
                {
                    ServeUnknownFileTypes = true,
                    DefaultContentType = "application/vnd.android.package-archive",
                    FileProvider = new PhysicalFileProvider(
                    Path.Combine(_curdir, @"wwwroot/kmbidevrepo/uploads/apk")),
                    RequestPath = new PathString("/kmbidevrepo/uploads/apk")
                });
                app.UseStaticFiles(new StaticFileOptions()
                {
                    FileProvider = new PhysicalFileProvider(
                    Path.Combine(_curdir, @"wwwroot/kmbidevreporeports")),
                    RequestPath = new PathString("/kmbidevreporeports")
                });
            }

            app.UseSession();
            app.UseCors("allowCors");

            app.UseRequestLocalization();
            app.UseMvc();
        }
    }
}
