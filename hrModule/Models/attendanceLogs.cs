﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace kmbi_core_master.hrModule.Models
{    
    public class attendanceLogs
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("logId")]
        public int logId { get; set; }

        [BsonElement("userId")]
        public int userId { get; set; }

        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("checkTime")]
        public DateTime checkTime { get; set; }

        [BsonElement("checkType")]
        public string checkType { get; set; }

        [BsonElement("statusText")]
        public string statusText { get; set; }

        [BsonElement("clientId")]
        public int clientId { get; set; }

        [BsonElement("clientName")]
        public string clientName { get; set; }
    }
}
