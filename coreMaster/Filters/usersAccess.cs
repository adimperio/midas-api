﻿using kmbi_core_master.coreMaster.IRepository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Threading.Tasks;

namespace kmbi_core_master.coreMaster.Filters
{
    public class usersAccess : TypeFilterAttribute
    {
        public static string _access { get; set; }
        public usersAccess(string access) : base(typeof(ValidateUserAccess))
        {
            _access = access;
        }

        private class ValidateUserAccess : IAsyncActionFilter
        {
            private readonly iusersRepository _usersRepository;
            public ValidateUserAccess(iusersRepository usersRepository)
            {
                _usersRepository = usersRepository;
            }

            public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
            {
                if ((_usersRepository.getAccess()).Contains(_access))
                {
                    await next();
                }
                else
                {
                    context.HttpContext.Response.StatusCode = 401; 
                    context.Result = new JsonStringResult("{\"type\":\"error\",\"message\":\"You are not authorized to do this action.\"}");
                    return;
                }

                await next();

            }
        }
    }
}
