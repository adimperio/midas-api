﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Filters;
using Microsoft.AspNetCore.Cors;
using kmbi_core_master.acctngModule.Interface;
using kmbi_core_master.acctngModule.Models;
using kmbi_core_master.coreMaster;
using System;

using System.Dynamic;
using System.Linq;

namespace kmbi_core_master.acctngModule.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/generalLedger")]
    [usersAuth]
    public class generalLedgerController : Controller
    {
        private iAccountingShared _acc;

        public generalLedgerController(iAccountingShared acc)
        {
            _acc = acc;
        }

        [HttpGet]
        public IActionResult Get([FromQuery]Int32? account, [FromQuery]Int32? year) {
            var list  = _acc.generalLedger(account, year);
            if(list!=null) {
                return Ok(list);
            } else {
                return BadRequest(new {
                    type = "information",
                    message = "No data to generate"
                });
            }
        }
    }
}
