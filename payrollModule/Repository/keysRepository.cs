﻿using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.helpers;
using kmbi_core_master.payrollModule.IRepository;
using kmbi_core_master.payrollModule.Models;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace kmbi_core_master.payrollModule.Repository
{
    public class keysRepository: dbCon, ikeysRepository
    {
       
        public keysRepository(IOptions<dbSettings> settings) : base(settings)
        {
        }

       
       
        public IEnumerable<keys> getAll()
        {
            var c = _db.GetCollection<keys>("keys").Find(new BsonDocument()).ToListAsync();
            return c.Result;
        }

        
       
        public keys Get(String name)
        {
            var query = Builders<keys>.Filter.Eq(e => e.name, name);
            var fields = Builders<keys>.Projection
                .Include(u => u.Id)
                .Include(u => u.name)
                .Include(u => u.value)
                .Include(u => u.updatedAt);

            var c = _db.GetCollection<keys>("keys").Find(query).Project<keys>(fields).ToListAsync();
            return c.Result.FirstOrDefault();
        }

        public void Update(String name, keys p)
        {
            
            var filter = Builders<keys>.Filter.Eq(e => e.name, name);
            var update = Builders<keys>.Update
                .Set("value", p.value)
                .CurrentDate("updatedAt");
            var result = _db.GetCollection<keys>("keys").UpdateOneAsync(filter, update);

        }

        public users getPassword(String slug)
        {
            var query = Builders<users>.Filter.Eq(e => e.slug, slug);
            var fields = Builders<users>.Projection
                .Include(u => u.password);

            var c = _db.GetCollection<users>("users").Find(query).Project<users>(fields).ToListAsync();
            return c.Result.FirstOrDefault();
        }

    }
}
