﻿using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.payrollModule.IRepository;
using kmbi_core_master.payrollModule.Models;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace kmbi_core_master.payrollModule.Repository
{
    public class philhealthRepository: dbCon, iphilhealthRepository
    {
       
        public philhealthRepository(IOptions<dbSettings> settings) : base(settings)
        {
        }

        public philhealth getSalary(double salary)
        {
            var q1 = Builders<philhealth>.Filter.Lte(e => e.salaryRange.from, salary);
            var q2 = Builders<philhealth>.Filter.Gte(e => e.salaryRange.to, salary);
            var query = Builders<philhealth>.Filter.And(q1, q2);
            var c = _db.GetCollection<philhealth>("philhealth").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }
    }
}
