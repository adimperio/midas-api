using System;
using kmbi_core_master.coreMaster.Models;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;


namespace kmbi_core_master.reports.Models
{
    public class signatories
    {
        public String reportName {get;set;}
        public String preparedByPosition {get;set;}
        public String preparedBy {get;set;}
        public String checkedByPosition {get;set;}
        public String checkedBy {get;set;}
        public String approvedByPosition {get;set;}
        public String approvedBy {get;set;}
        public String notedByPosition {get;set;}
        public String notedBy {get;set;}
    }
}