﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace kmbi_core_master.acctngModule.Models
{
    public class acctEntryTemplateCV
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("slug")]
        public String slug { get; set; }

        [BsonElement("particulars")]
        public string particulars { get; set; }

        [BsonElement("accounts")]
        public cvAccounts accounts { get; set; }
        
        [BsonElement("supplier")]
        public cvSupplier supplier { get; set; }

        [BsonElement("timestamps")]
        public cvTimestamps timestamps { get; set; }
    }

    
}
