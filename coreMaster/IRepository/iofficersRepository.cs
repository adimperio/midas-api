﻿using kmbi_core_master.coreMaster.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.coreMaster.IRepository
{
    public interface iofficersRepository
    {
        IEnumerable<officersCenterList> allOfficers();

        officersCenterList getOfficerSlug(String slug);

        IEnumerable<officersCenterList> getOfficerunitId(String unitId);

        centerList getCenterSlug(string centerSlug);

        centerList getCenterCode(string centerCode);

        void addOfficer(officers b);

        void updateOfficer(String slug, officers b);

        void addCenter(centers u, string Id);

        void updateCenter(String officerSlug, string centerSlug, centers u);

        void removeOfficer(String slug);

        void removeCenter(String officerSlug, string centerSlug);

        IEnumerable<officers> searchOfficer(string name);

        IEnumerable<centerList> searchCenter(string code);

        officers checkOfficerSlug(String slug);

        IEnumerable<officers> getByUnit(string slug);
    }
}
