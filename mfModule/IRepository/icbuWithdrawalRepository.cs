﻿using kmbi_core_master.coreMaster.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.coreMaster.IRepository
{
    public interface icbuWithdrawalRepository
    {
        IEnumerable<cbuWithdrawal> all();

        cbuWithdrawal getSlug(String slug);

        List<cbuWithdrawal> getBSlug(String branchSlug);
        List<cbuWithdrawal> getBSlug(String branchSlug, DateTime from, DateTime to);

        void add(cbuWithdrawal b);

        void update(String slug, cbuWithdrawal b);

        void remove(String slug);

        banksList getBranchSlug(String branchId);

        List<cbuWithdrawal> getByDate(DateTime from, DateTime to, string branchSlug);

        List<cbuWithdrawal> getByCenterSulg(String centerSlug);

    }
}
