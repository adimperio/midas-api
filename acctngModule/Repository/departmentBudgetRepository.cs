﻿using Microsoft.Extensions.Options;

using System;
using System.Collections.Generic;
using System.Linq;

using MongoDB.Bson;
using MongoDB.Driver;

using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.acctngModule.Models;
using kmbi_core_master.acctngModule.Interface;

namespace kmbi_core_master.acctngModule.Repository
{
    public class departmentBudgetRepository : dbCon, idepartmentBudgetRepository
    {
        
        public departmentBudgetRepository(IOptions<dbSettings> settings) : base(settings) {}

        public void add(departmentBudget b)
        {

            b.createdAt = DateTime.Now;
            b.updatedAt = DateTime.Now;
           
                //for (int r = 0; r <= b.department.budget.Count() - 1; r++)
                //{
                //    b.department.budget[r].Id = ObjectId.GenerateNewId().ToString();
                //}
           
            var coll = _db.GetCollection<departmentBudget>("departmentBudget");
            _db.GetCollection<departmentBudget>("departmentBudget").InsertOneAsync(b);
        }
        

        public IEnumerable<departmentBudget> all()
        {
            var c = _db.GetCollection<departmentBudget>("departmentBudget").Find(new BsonDocument()).ToListAsync();
            return c.Result;
        }

        public departmentBudget getSlug(String slug)
        {
            var query = Builders<departmentBudget>.Filter.Eq(e => e.slug, slug);
            var c = _db.GetCollection<departmentBudget>("departmentBudget").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }

        public List<departmentBudget> getbyYear(int year)
        {
            var query = Builders<departmentBudget>.Filter.Eq(e => e.budgetYear, year);
            var c = _db.GetCollection<departmentBudget>("departmentBudget").Find(query).ToListAsync();

            return c.Result;
        }

        public void update(String slug, departmentBudget p)
        {
            p.slug = slug;
            var query = Builders<departmentBudget>.Filter.Eq(e => e.slug, slug);
            var update = Builders<departmentBudget>.Update
                .Set("budgetYear", p.budgetYear)
                .Set("department", p.departmentName)
                .Set("code", p.code)
                .CurrentDate("updatedAt");

            var branch = _db.GetCollection<departmentBudget>("departmentBudget").UpdateOneAsync(query, update);

        }
        public void remove(String slug)
        {
            var query = Builders<departmentBudget>.Filter.Eq(e => e.slug, slug);
            var result = _db.GetCollection<departmentBudget>("departmentBudget").DeleteOneAsync(query);
        }

        public void removeByDept(String name, int year, departmentBudget b)
        {
            var query1 = Builders<departmentBudget>.Filter.Eq(e => e.departmentName, name);
            var query2 = Builders<departmentBudget>.Filter.Eq(e => e.budgetYear, year);
            var query = Builders<departmentBudget>.Filter.And(query1, query2);
            var result = _db.GetCollection<departmentBudget>("departmentBudget").DeleteOneAsync(query);

            b.createdAt = DateTime.Now;
            b.updatedAt = DateTime.Now;
           
                for (int r = 0; r <= b.budget.Count() - 1; r++)
                {
                    b.budget[r].Id = ObjectId.GenerateNewId().ToString();
                }

            var coll = _db.GetCollection<departmentBudget>("departmentBudget");
            _db.GetCollection<departmentBudget>("departmentBudget").InsertOneAsync(b);
        }


        //CREDIT
        public budgetDept getId(string id)
        {

            var query = Builders<budgetDept>.Filter.ElemMatch(x => x.budget, x => x.Id == id);
            var fields = Builders<budgetDept>.Projection
                .Include(u => u.budget)
                .ElemMatch(x => x.budget, x => x.Id == id);

            var unit = _db.GetCollection<budgetDept>("budgetDept").Find(query).Project<budgetDept>(fields).ToListAsync();
            return unit.Result.FirstOrDefault();
        }

        //public void addBudget(string depSlug, budBudget u, string slug)
        //{
        //    var query = Builders<departmentBudget>.Filter.Eq(e => e.slug, slug);
        //    var update = Builders<departmentBudget>.Update.Push(e => e.department.budget, u);
        //    u.Id = ObjectId.GenerateNewId().ToString();
        //    _db.GetCollection<departmentBudget>("departmentBudget").FindOneAndUpdateAsync(query, update);
        //}


        //public void updateBudget(string slug, string id, budBudget u)
        //{

        //    var query = Builders<departmentBudget>.Filter.And(Builders<departmentBudget>.Filter.Eq(x => x.slug, slug),
        //    Builders<departmentBudget>.Filter.ElemMatch(x => x.department.budget, x => x.Id == id));

        //    var update = Builders<departmentBudget>.Update
        //        .Set("budget.$.name", u.name)
        //        .Set("budget.$.code", u.name)
        //        .Set("budget.$.amount", u.amount);
        //    var result = _db.GetCollection<departmentBudget>("departmentBudget").UpdateOneAsync(query, update);
        //}

        //public void removeBudget(String slug, string id)
        //{
        //    var pull = Builders<departmentBudget>.Update.PullFilter(x => x.department.budget, a => a.Id == id);

        //    var filter1 = Builders<departmentBudget>.Filter.And(Builders<departmentBudget>.Filter.Eq(a => a.slug, slug),
        //                  Builders<departmentBudget>.Filter.ElemMatch(q => q.department.budget, t => t.Id == id));

        //    var result = _db.GetCollection<departmentBudget>("departmentBudget").UpdateOneAsync(filter1, pull);
        //}



    }
}
