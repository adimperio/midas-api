﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.coreMaster.Models
{

    public class providers

    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("description")]
        public string description { get; set; }

        public premium premium { get; set; }

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }
    }


    public class premium
    {
        
        public pClient client { get; set; }

        public IList<pDependents> dependents { get; set; }
    }


    public class pClient
    {

        [BsonElement("pNew")]
        public double pNew { get; set; }

       
        [BsonElement("pOld")]
        public double pOld { get; set; }

    }

    public class pDependents
    {

        [BsonElement("name")]
        public string name { get; set; }


        [BsonElement("relationship")]
        public string relationship { get; set; }

        [BsonElement("value")]
        public double value { get; set; }

    }

}
