﻿using kmbi_core_master.acctngModule.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.acctngModule.Interface
{
    public interface iacctngDbaRepository
    {
        IEnumerable<acctngDba> all();

        acctngDba getSlug(String slug);

        void add(acctngDba b);

        void update(String slug, acctngDba b);

        void remove(String slug);
    }
}
