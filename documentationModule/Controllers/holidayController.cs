using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

using kmbi_core_master.documentationModule.Models;
using kmbi_core_master.coreMaster.Filters;
using kmbi_core_master.coreMaster.Models;
using System.Collections.Generic;
using kmbi_core_master.documentationModule.Service;

using System.Linq;
using Microsoft.AspNetCore.Cors;
using kmbi_core_master.documentationModule.iRepositories;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.helpers.interfaces;

namespace kmbi_core_master.documentationModule.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/holiday")]
    [errorHandler]
    public class holidayController : Controller
    {
        private readonly iholidayRepository _holidayRepo;
        private readonly iglobal2 _global2;
        public holidayController(iholidayRepository holidayRepo, iglobal2 global2) 
        {
            _holidayRepo = holidayRepo;
            _global2 = global2;
        }

        private void logger(String message)
        {
            //global.log(message, 1, "link here");
        }  

        [HttpGetAttribute]
        public IActionResult Get([FromQueryAttribute]Int32 month = 0, [FromQueryAttribute]Int32 year = 0)
        {
            IEnumerable<holiday> list = _holidayRepo.GetHolidays(month,year);

            if(list!=null)
            {
                return Ok(from x in list select new {
                    Id=x.Id, 
                    name = x.name, 
                    date = x.date, 
                    slug = x.slug,
                    type = x.type
                });
            }
            else
            {
                return Ok(new List<holiday> {});
            }
        }

        [HttpGet("{slug}")]
        public IActionResult Get(String slug)
        { 
            holiday holiday = _holidayRepo.GetHolidayBySlug(slug);
            if(holiday!=null)
            {
                return Ok(holiday);;
            }
            else
            {
                return Ok(new{});
            }
        }

        [HttpPost()]
        public IActionResult Post([FromBody]holiday holiday)
        {
            Boolean isSuccess = _holidayRepo.InsertHoliday(holiday);

            if(isSuccess)
            {
                // _global2.reupdateSchedule();
                logger(holiday.name + " created successfully");
                return Ok(new {
                    type="success", 
                    message = holiday.name + "created succesfully.",
                    slug=holiday.slug,
                    holiday=holiday
                });
            }
            else
            {
                return Ok(new {
                    type="error", 
                    message = "Holiday create failed."
                });
            }
        }

        [HttpPut("{slug}")]
        public IActionResult Put(String slug, [FromBody]holiday holiday)
        {
            Boolean isSuccess = _holidayRepo.UpdateHoliday(slug, holiday);

            if(isSuccess)
            {
                // _global2.reupdateSchedule();
                logger(holiday.name + " updated successfully");
                return Ok(new {
                    type="success",
                    message= holiday.name + " updated successfully.",
                    holiday = holiday
                });
            }
            else
            {
                return BadRequest(new {
                    type="error",
                    message="Holiday update failed."
                });
            }
        }

        [HttpDelete("{slug}")]
        public IActionResult Delete(String slug)
        {
            holiday holiday = _holidayRepo.GetHolidayBySlug(slug);

            bool res = _holidayRepo.DeleteHoliday(slug);

            if(res)
            {
                // _global2.reupdateSchedule();
                logger(holiday.name + " deleted successfully");
                return Ok(new {
                    type="success",
                    message= holiday.name + " deleted successfully."
                });
            }
            else
            {
                return BadRequest(new {
                    type="error",
                    message="Holiday delete failed."
                });
            }
        }

        [HttpGetAttribute("list/")]
        public IActionResult GetAsListOfDateTime()
        {
            IEnumerable<holiday> list = _holidayRepo.GetHolidays(0,0);

            if(list!=null)
            {
                return Ok(from x in list select x.date);
            }
            else
            {
                return Ok(new List<holiday> {});
            }
        }

        [HttpGet("getNextThirtyDaysHolidays")]
        public IActionResult getNextThirtyDaysHolidays()
        {
            var res = _holidayRepo.getNextThirtyDaysHolidays();
            if(res!=null)
                return Ok(res);
            else 
                return Ok(new Object[]{});
        }

        [HttpGet("getNext365Holidays")]
        public IActionResult getNext365Holidays([FromQuery] DateTime date)
        {
            var res = _holidayRepo.getNext365Holidays(date);
            if (res != null)
                return Ok(res);
            else
                return Ok(new Object[] { });
        }

        [HttpGetAttribute("getHolidays")]
        public IActionResult GetHolidays([FromQueryAttribute]DateTime  from, [FromQueryAttribute]DateTime to)
        {
            IEnumerable<holiday> list = _holidayRepo.GetHolidaysDateRange(from, to);

            if (list != null)
            {
                return Ok(from x in list
                          select new
                          {
                              Id = x.Id,
                              name = x.name,
                              date = x.date,
                              slug = x.slug,
                              type = x.type
                          });
            }
            else
            {
                return Ok(new List<holiday> { });
            }
        }
    }
}