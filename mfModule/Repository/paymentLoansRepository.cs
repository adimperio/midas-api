﻿using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Models;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace kmbi_core_master.coreMaster.Repository
{
    public class paymentLoansRepository : dbCon, ipaymentLoansRepository
    {
        public paymentLoansRepository(IOptions<dbSettings> settings) : base(settings)
        {
        }

        public void add(paymentLoans b)
        {
            
            b.createdAt = DateTime.Now;
            b.updatedAt = DateTime.Now;

            _db.GetCollection<paymentLoans>("paymentLoans").InsertOneAsync(b);

        }

        public IEnumerable<paymentLoans> all()
        {
            var c = _db.GetCollection<paymentLoans>("paymentLoans").Find(new BsonDocument()).ToListAsync();
            return c.Result;
        }

        public paymentLoans getSlug(String slug)
        {
            var query = Builders<paymentLoans>.Filter.Eq(e => e.slug, slug);
            var c = _db.GetCollection<paymentLoans>("paymentLoans").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }

        public paymentLoans getId(String id)
        {
            var query = Builders<paymentLoans>.Filter.Eq(e => e.id, id);
            var sort = Builders<paymentLoans>.Sort.Descending("officialReceipt.paidAt");
            var c = _db.GetCollection<paymentLoans>("paymentLoans").Find(query).Sort(sort).ToListAsync();

            return c.Result.FirstOrDefault();
        }

        //public IEnumerable<paymentLoans> getIdWeekNo(String id, int? weekNo)
        //{
        //    var q1 = Builders<paymentLoans>.Filter.Eq(e => e.weekNo, weekNo);
        //    var q2 = Builders<paymentLoans>.Filter.Eq(e => e.loan.referenceId, id);
        //    var query=q1;
        //    if (weekNo == null)
        //    {
        //        query = q2;
        //    }
        //    else
        //    {
        //        query = Builders<paymentLoans>.Filter.And(q1, q2);
        //    }
        //    var c = _db.GetCollection<paymentLoans>("paymentLoans").Find(query).ToListAsync();

        //    return c.Result;
        //}

        public IEnumerable<paymentLoans> getIdWeekNo(String id, string memberSlug, int? weekNo)
        {

            var collPayments = _db.GetCollection<paymentLoans>("paymentLoans");
            var c = collPayments.Aggregate()
                .Match(e => e.loan.referenceId == id)
                .Unwind("members")
                .Match(new BsonDocument{ { "members.slug", memberSlug } })
                .Unwind("members.terms")
                .Match(new BsonDocument { { "members.terms.term", weekNo } })
                .Project<paymentLoans>(new BsonDocument
                        {
                            {"_id",1},
                            {"slug", 1},
                            {"loan", 1},
                            {"members", 1},
                            {"officialReceipt",1},
                            {"cv", 1},
                            {"payee",1},
                            {"mode",1},
                            {"remarks",1},
                            {"type", 1}
                        })
                .ToListAsync();
            //var q1 = Builders<paymentLoans>.Filter.Eq(e => e.loan.referenceId, id);
            //var q2 = Builders<paymentLoans>.Filter.ElemMatch(e => e.members, memberSlug);
            ////var q3 = Builders<paymentLoans>.Filter.ElemMatch(e => e.members, memberSlug);

            //foreach (paymentLoansMembers members in q2)
            //{

            //}

            //var query = q1;
            //if (weekNo == null)
            //{
            //    query = q2;
            //}
            //else
            //{
            //    query = Builders<paymentLoans>.Filter.And(q1, q2);
            //}
            //var c = _db.GetCollection<paymentLoans>("paymentLoans").Find(query).ToListAsync();

            return c.Result;
        }

        public IEnumerable<paymentLoans> getIdWeekNo(String id)
        {
           
            var query = Builders<paymentLoans>.Filter.Eq(e => e.loan.referenceId, id);
            var sort = Builders<paymentLoans>.Sort.Descending("officialReceipt.paidAt");
            var c = _db.GetCollection<paymentLoans>("paymentLoans").Find(query).Sort(sort).ToListAsync();

            return c.Result;
        }

        public void update(String id, paymentLoans p)
        {
            p.id = id;
            var query = Builders<paymentLoans>.Filter.Eq(e => e.id, id);
            var update = Builders<paymentLoans>.Update
                .Set("loan", p.loan)
                .Set("members", p.members)
                .Set("officialReceipt", p.officialReceipt)
                .Set("cv", p.cv)
                .Set("payee", p.payee)
                .Set("mode", p.mode)
                .Set("remarks", p.remarks)
                .Set("type", p.type)
                .CurrentDate("updatedAt");

            var l = _db.GetCollection<paymentLoans>("paymentLoans").UpdateOneAsync(query, update);

        }


        public void updateSlug(String slug, paymentLoans p)
        {
            p.slug = slug;
            var query = Builders<paymentLoans>.Filter.Eq(e => e.slug, slug);
            var update = Builders<paymentLoans>.Update
                .Set("loan", p.loan)
                .Set("members", p.members)
                .Set("officialReceipt", p.officialReceipt)
                .Set("cv", p.cv)
                .Set("payee", p.payee)
                .Set("mode", p.mode)
                .Set("remarks", p.remarks)
                .Set("type", p.type)
                .CurrentDate("updatedAt");

            var l = _db.GetCollection<paymentLoans>("paymentLoans").UpdateOneAsync(query, update);

        }


        public void remove(String id)
        {
            var query = Builders<paymentLoans>.Filter.Eq(e => e.id, id);
            var result = _db.GetCollection<paymentLoans>("paymentLoans").DeleteOneAsync(query);
        }


        public void removeSlug(String slug)
        {
            var query = Builders<paymentLoans>.Filter.Eq(e => e.slug, slug);
            var result = _db.GetCollection<paymentLoans>("paymentLoans").DeleteOneAsync(query);
        }


        public List<paymentLoansWithSched> paidAt(DateTime p, string branchSlug)
        {
            var dtTo = p.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
            var dtFrom = p;
            //var q1 = Builders<paymentLoans>.Filter.Gte(x => x.officialReceipt.paidAt, dtFrom);
            //var q2 = Builders<paymentLoans>.Filter.Lte(x => x.officialReceipt.paidAt, dtTo);
            //var q3 = Builders<paymentLoans>.Filter.Eq(x => x.loan.branch.slug, branchSlug);
            //var query = Builders<paymentLoans>.Filter.And(q1, q2, q3);
            //var c = _db.GetCollection<paymentLoans>("paymentLoans").Find(query).ToListAsync();


            var coll = _db.GetCollection<paymentLoansWithSched>("paymentLoans");
            var iResult = coll.Aggregate()
                .Match(new BsonDocument { { "loan.branch.slug", branchSlug } })
                .Match<paymentLoansWithSched>(e => e.officialReceipt.paidAt >= dtFrom)
                .Match<paymentLoansWithSched>(e => e.officialReceipt.paidAt <= dtTo)
                .Lookup("loans", "loan.referenceId", "referenceId", "loans")
                .Unwind("loans")
                //.Unwind("loans.members")
                //.Match(new BsonDocument { { "loans.members.slug", clientSlugs[i].ToString() } })
                //.Match(new BsonDocument { { "loans.status", status } })
                .Project<paymentLoansWithSched>(new BsonDocument
                {
                        {"slug", 1},
                        {"loan",1},
                        {"members",1},
                        {"officialReceipt",1},
                        {"cv", 1},
                        {"payee", 1},
                        {"mode", 1},
                        {"remarks", 1},
                        {"type", 1},
                         {"createdAt", 1},
                         {"updatedAt", 1},
                    //{ "loans.members.schedule.weekNo",1},
                    //{ "loans.members.schedule.collectionDate" ,1}
                        {"schedules", new BsonDocument{ { "$arrayElemAt", new BsonArray { { "$loans.members.schedule"}, 0 }  } } }
                })
                .ToListAsync();

        

            return iResult.Result;
        }

        public List<paymentLoans> getPayment(List<string> refId)
        {
            BsonDocument bson = new BsonDocument("loan.referenceId", new BsonDocument("$in", new BsonArray(refId)));
            var sort = Builders<paymentLoans>.Sort.Descending(e => e.officialReceipt.paidAt);
            var c = _db.GetCollection<paymentLoans>("paymentLoans").Find(bson).Sort(sort).ToListAsync();

            return c.Result;
        }
    }
}
