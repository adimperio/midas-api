﻿using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.spm.IRepository;
using kmbi_core_master.spm.Models;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace kmbi_core_master.spm.Repository
{
    public class ppiRepository : dbCon, ippiRepository
    {
        public ppiRepository(IOptions<dbSettings> settings) : base(settings)
        {
        }

        public void add(ppi b)
        {

            b.createdAt = DateTime.Now;
            b.updatedAt = DateTime.Now;

            _db.GetCollection<ppi>("ppi").InsertOneAsync(b);
        }

        public IEnumerable<ppi> getAll()
        {
            var c = _db.GetCollection<ppi>("ppi").Find(new BsonDocument()).ToListAsync();

            return c.Result;
        }

        public IEnumerable<ppi> getByLoanSlug(String loanSlug)
        {
            var query = Builders<ppi>.Filter.Eq(e => e.loan.slug, loanSlug);
            var c = _db.GetCollection<ppi>("ppi").Find(query).ToListAsync();
            return c.Result;
        }

        public ppi getBySlug(String slug)
        {
            var query = Builders<ppi>.Filter.Eq(e => e.slug, slug);
            var c = _db.GetCollection<ppi>("ppi").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }

        public ppi getByClientSlug(String slug)
        {
            var query = Builders<ppi>.Filter.Eq(e => e.client.slug, slug);
            var c = _db.GetCollection<ppi>("ppi").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }

       
        public void update(String slug, ppi p)
        {
           // p.slug = slug;
            var query = Builders<ppi>.Filter.Eq(e => e.slug, slug);
            var update = Builders<ppi>.Update
                .Set("client", p.client)
                .Set("loan", p.loan)
                .Set("questionaire", p.questionaire)
                .Set("questions", p.questions)
                .Set("score", p.score)
                .CurrentDate("updatedAt");

            var l = _db.GetCollection<ppi>("ppi").UpdateOneAsync(query, update);

        }

        public void remove(String slug)
        {
            var query = Builders<ppi>.Filter.Eq(e => e.slug, slug);
            var result = _db.GetCollection<ppi>("ppi").DeleteOneAsync(query);
        }

        public List<loans> getppiclients(DateTime from, DateTime to, string region, string area, string branch, string unit, string po, string center) {
            
            var dtTo = to.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
            var dtFrom = from;

            ////get area            
            //var qRegion = Builders<regions>.Filter.Eq(x => x.name, region);
            //var rRegion = _db.GetCollection<regions>("region").Find(qRegion).FirstOrDefault();
            //regions xRegion = new regions {
            //   areas = rRegion.areas
            //};
            //get  

            //params
            var qDFrom = Builders<loans>.Filter.Gte(x => x.disbursementDate, dtFrom);
            var qDTo = Builders<loans>.Filter.Lte(x => x.disbursementDate, dtTo);
            var qStatus = Builders<loans>.Filter.Lte(x => x.status, "for-payment");
            
           // var qArea = Builders<regions>.Filter.ElemMatch(e => e.areas, x => x.name == area);
            var qBranch = Builders<loans>.Filter.Eq(x => x.branchName, branch);
            var qUnit = Builders<loans>.Filter.Eq(x => x.unitCode, unit);
            var qPo = Builders<loans>.Filter.Eq(x => x.officerName, po);
            var qCenter = Builders<loans>.Filter.Eq(x => x.centerName.Substring(0, 8), center);

            

            //FilterDefinition<loans> qActiveClients = null;
            List<loans> activeClients = null;

            //get active clients - REGION
            if (region == "All" && area == "All" && branch == "All" && unit == "All" && po == "All" && center == "All")
            {
                 var qDates = Builders<loans>.Filter.And(qDFrom, qDTo, qStatus);
                 activeClients = _db.GetCollection<loans>("loans").Find(qDates).ToListAsync().Result;
            }

            if (region != "All" && area == "All" && branch == "All" && unit == "All" && po == "All" && center == "All")
            {
                var qRegion = Builders<regions>.Filter.Eq(x => x.name, region);
                var resRegionId = _db.GetCollection<regions>("region").Find(qRegion).FirstOrDefault().Id;

                var qDates = Builders<loans>.Filter.And(qDFrom, qDTo);
                var resClients = _db.GetCollection<loans>("loans").Find(qDates).ToListAsync().Result;

                var qArea = Builders<branch>.Filter.Eq(x => x.regionId, resRegionId);
                var resBranch = _db.GetCollection<branch>("branch").Find(qArea).ToListAsync().Result;
                foreach (var item in resBranch)
                {
                    var qBranchSlug = Builders<loans>.Filter.Eq(x => x.branchSlug, item.slug);
                    var qF = Builders<loans>.Filter.And(qDates, qBranchSlug);
                    activeClients = _db.GetCollection<loans>("loans").Find(qF).ToListAsync().Result;
                }
                    
            }
            
            return activeClients;
        }
        public ppiScore getByDateRange(DateTime from, DateTime to, string region, string area, string branch, string unit, string po, string center)
        {
            List<ppi> list = new List<ppi>();
            var activeClients = getppiclients(from, to, region, area, branch, unit, po, center);

            //get client in PPI
            //var qPPIClient = _db.GetCollection<ppi>("ppi").Find(new BsonDocument()).ToListAsync().Result;
            var total = 0;
            foreach (var item in activeClients)
            {
                var members = item.members;
                total+= members.Count;

                foreach (var mem in members)
                {
                    var memSlug = mem.slug;
                    var qPPIClient = Builders<ppi>.Filter.Eq(x => x.client.slug, memSlug);
                    var clientList = _db.GetCollection<ppi>("ppi").Find(qPPIClient).FirstOrDefault();
                    if (clientList != null)
                    {
                        list.Add(new ppi
                        {
                            client = clientList.client,
                            //loan = clientList.loan,
                            questions = clientList.questions
                        });
                    }
                    
                }
            }

            var data = "";
            var food = 0.0;
            var national = 0.0;
            var onePerDay = 0.0;
            var twoPerDay = 0.0;

            foreach (var item in list)
            {
                foreach (var s in item.questions)
                {
                    data = s.answer.value;
                    var anslength = data.Split(',').Length;
                    if (anslength == 4)
                    {

                        food += double.Parse(data.Split(',')[0]);
                        national += double.Parse(data.Split(',')[1]);
                        onePerDay += double.Parse(data.Split(',')[2]);
                        twoPerDay += double.Parse(data.Split(',')[3]);
                    }
                    
                }
            }
            //ppiScore ppiScorelist = new ppiScore();
            return new ppiScore {
                foodScore = Math.Round(food / total, 2),
                nationalScore = Math.Round(national / total,2),
                onePerDayScore = Math.Round(onePerDay / total,2),
                twoPerDayScore = Math.Round(twoPerDay / total,2)
            };
        }

        public List<ppiExtraction> extractPPI()
        {
            //Get PPI
            var getPPI = _db.GetCollection<ppi>("ppi").Find(new BsonDocument()).ToListAsync();

            List<ppiExtraction> ePPI = new List<ppiExtraction>();
            foreach (var item in getPPI.Result)
            {
                //clients
                var qClients = Builders<clients>.Filter.Eq(e => e.slug, item.client.slug);
                var clients = _db.GetCollection<clients>("clients").Find(qClients).FirstOrDefault();

                //center
                var qCenter = Builders<centerInstance>.Filter.Eq(e => e.slug, clients.centerSlug);
                var center = _db.GetCollection<centerInstance>("centerInstance").Find(qCenter).FirstOrDefault();
                var centerName = "";
                if (center != null)
                {
                    centerName = center.code;
                }

                //branch
                var qBranch = Builders<branch>.Filter.Eq(e => e.slug, clients.branchSlug);
                var branch = _db.GetCollection<branch>("branch").Find(qBranch).FirstOrDefault();
                var branchName = "";
                if (branch != null)
                {
                    branchName = branch.name;
                }

                
                if (clients != null)
                {
                    ePPI.Add(new ppiExtraction
                    {
                        //Id = item.Id,
                        clientSlug = item.client.slug,
                        firstName = clients.name.first,
                        middleName = clients.name.middle,
                        lastName = clients.name.last,
                        loanCycle = clients.loanCycle,
                        branch = branchName ,
                        center = centerName,
                        score = item.score
                    });
                }
                
            }

            return ePPI;

        }

        public ppiScore newGetByDateRange(DateTime from, DateTime to, string region, string area, string branch, string unit, string po, string center)
        {
            // var NactiveClients = new List<MongoDB.Driver.FilterDefinition<kmbi_core_master.coreMaster.Models.loans>>();
            var dtTo = to.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
            var dtFrom = from;

            var qDFrom = Builders<loans>.Filter.Gte(x => x.disbursementDate, dtFrom);
            var qDTo = Builders<loans>.Filter.Lte(x => x.disbursementDate, dtTo);
            var qStatus = Builders<loans>.Filter.Eq(x => x.status, "for-payment");

            var qDates = Builders<loans>.Filter.And(qDFrom, qDTo);

            var qRegion = Builders<regions>.Filter.Eq(x => x.name, region);

            var qRegion1 = Builders<loans>.Filter.Eq(x => x.regionSlug, region);

            var qArea = Builders<regions>.Filter.And(Builders<regions>.Filter.Eq(x => x.name, region),
                Builders<regions>.Filter.ElemMatch(x => x.areas, x => x.name == area));

            var qArea1 = Builders<loans>.Filter.Eq(x => x.areaSlug, area);

            var qBranch = Builders<loans>.Filter.Eq(x => x.branchSlug, branch);

            var qUnit = Builders<loans>.Filter.Eq(x => x.unitCode, unit);

            var qPo = Builders<loans>.Filter.Eq(x => x.officerName, po);

            var qCenter = Builders<loans>.Filter.Eq(x => x.centerSlug, center);

            //   var qF = Builders<loans>.Filter.And(qDates, qBranch, qUnit, qPo, qCenter, qStatus);
            //FilterDefinition<loans> qActiveClients = null;
            List<loans> activeClients = null;

            //get active clients - REGION
            if (region == "All" && area == "All" && branch == "All" && unit == "All" && po == "All" && center == "All")
            {
                var qDates2 = Builders<loans>.Filter.And(qDFrom, qDTo, qStatus);
                activeClients = _db.GetCollection<loans>("loans").Find(qDates2).ToListAsync().Result;
            }

            else if (region != "All" && area == "All" && branch == "All" && unit == "All" && po == "All" && center == "All")
            {
                var qF = Builders<loans>.Filter.And(qDates, qStatus, qRegion1);
                activeClients = _db.GetCollection<loans>("loans").Find(qF).ToListAsync().Result;
            }

            else if (region != "All" && area != "All" && branch == "All" && unit == "All" && po == "All" && center == "All")
            {
                var qF = Builders<loans>.Filter.And(qDates, qStatus, qRegion1, qArea1);
                activeClients = _db.GetCollection<loans>("loans").Find(qF).ToListAsync().Result;
            }

            else if (region != "All" && area != "All" && branch != "All" && unit == "All" && po == "All" && center == "All")
            {
                var qF = Builders<loans>.Filter.And(qDates, qStatus, qRegion1, qArea1, qBranch);
                activeClients = _db.GetCollection<loans>("loans").Find(qF).ToListAsync().Result;
            }

            else if (region != "All" && area != "All" && branch != "All" && unit != "All" && po == "All" && center == "All")
            {
                var qF = Builders<loans>.Filter.And(qDates, qStatus, qRegion1, qArea1, qBranch, qUnit);
                activeClients = _db.GetCollection<loans>("loans").Find(qF).ToListAsync().Result;
            }

            else if (region != "All" && area != "All" && branch != "All" && unit != "All" && po != "All" && center == "All")
            {
                var qF = Builders<loans>.Filter.And(qDates, qStatus, qRegion1, qArea1, qBranch, qUnit, qPo);
                activeClients = _db.GetCollection<loans>("loans").Find(qF).ToListAsync().Result;
            }

            else if (region != "All" && area != "All" && branch != "All" && unit != "All" && po != "All" && center != "All")
            {
                var qF = Builders<loans>.Filter.And(qDates, qStatus, qRegion1, qArea1, qBranch, qUnit, qPo, qCenter);
                activeClients = _db.GetCollection<loans>("loans").Find(qF).ToListAsync().Result;
            }
            List<ppi> list = new List<ppi>();
            var total = 0;
            foreach (var item in activeClients)
            {
                var members = item.members;
                total += members.Count;

                foreach (var mem in members)
                {
                    var memSlug = mem.slug;
                    var qPPIClient = Builders<ppi>.Filter.Eq(x => x.client.slug, memSlug);
                    var clientList = _db.GetCollection<ppi>("ppi").Find(qPPIClient).FirstOrDefault();
                    if (clientList != null)
                    {
                        list.Add(new ppi
                        {
                            client = clientList.client,
                            //loan = clientList.loan,
                            questions = clientList.questions
                        });
                    }

                }
            }

            var data = "";
            var food = 0.0;
            var national = 0.0;
            var onePerDay = 0.0;
            var twoPerDay = 0.0;

            foreach (var item in list)
            {
                foreach (var s in item.questions)
                {
                    data = s.answer.value;
                    var anslength = data.Split(',').Length;

                    try
                    {
                        if (anslength <= 4)
                        {
                            food += double.Parse(data.Split(',')[0]);
                            national += double.Parse(data.Split(',')[1]);
                            onePerDay += double.Parse(data.Split(',')[2]);
                            twoPerDay += double.Parse(data.Split(',')[3]);
                        }
                    }
                    catch
                    {

                    }


                }
            }
            //ppiScore ppiScorelist = new ppiScore();
            return new ppiScore
            {
                foodScore = Math.Round(food / total, 2),
                nationalScore = Math.Round(national / total, 2),
                onePerDayScore = Math.Round(onePerDay / total, 2),
                twoPerDayScore = Math.Round(twoPerDay / total, 2)
            };
        }

        public IEnumerable<ClientProfiling> getAllClientsByBranch(string branchSlug)
        {
            //clients 
            var qBranchSlug = Builders<clients>.Filter.Eq(e => e.branchSlug, branchSlug);
            var c = _db.GetCollection<clients>("clients").Find(qBranchSlug).ToListAsync();

            List<ClientProfiling> cProfiling = new List<ClientProfiling>();
            if (c != null)
            {
                foreach (var itemBranch in c.Result)
                {
                    //branch
                    var qCenterSlug = Builders<centerInstance>.Filter.Eq(e => e.slug, itemBranch.centerSlug);
                    var resCenterSlug = _db.GetCollection<centerInstance>("centerInstance").Find(qCenterSlug).FirstOrDefault();
                    var centerCode = "";
                    if (resCenterSlug != null)
                    {
                        centerCode = resCenterSlug.code;
                    }

                    var qBranch = Builders<branch>.Filter.Eq(e => e.slug, itemBranch.branchSlug);
                    var resBranch = _db.GetCollection<branch>("branch").Find(qBranch).ToListAsync();
                    var areaName = "";
                    if (resBranch != null)
                    {
                        foreach (var item in resBranch.Result)
                        {
                            var qArea1 = Builders<regions>.Filter.Eq(e => e.Id, item.regionId);
                            var resRegion = _db.GetCollection<regions>("region").Find(qArea1).FirstOrDefault().areas;
                            if (resRegion != null)
                            {
                                foreach (var areaItem in resRegion)
                                {
                                    if (areaItem.Id == item.areaId)
                                    {
                                        areaName = areaItem.name;
                                    }

                                }
                            }

                            //var qArea2 = Builders<regions>.Filter.ElemMatch(e => e.areas, e => e.Id == item.areaId);
                            //var qArea = Builders<regions>.Filter.And(qArea1, qArea2);
                            //var resArea = _db.GetCollection<regions>("region").Find(qArea).FirstOrDefault();

                            int now = int.Parse(DateTime.Now.ToString("yyyyMMdd"));
                            int dob = int.Parse(itemBranch.birthdate.Value.Date.ToString("yyyyMMdd"));
                            int age = (now - dob) / 10000;


                            cProfiling.Add(new ClientProfiling
                            {
                                area = areaName,
                                branch = item.name,
                                name = itemBranch.name.first.ToUpper() + " " + itemBranch.name.middle.ToUpper() + " " + itemBranch.name.last.ToUpper(),
                                cycle = itemBranch.loanCycle,
                                age = age,
                                civilStatus = itemBranch.civilStatus.ToUpper(),
                                business = itemBranch.business.type.ToUpper(),
                                code = centerCode
                            });
                        }
                    }

                }
            }

            return cProfiling;
        }
    }
}
