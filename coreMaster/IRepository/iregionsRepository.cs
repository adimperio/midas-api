﻿using kmbi_core_master.coreMaster.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.coreMaster.IRepository
{
    public interface iregionsRepository
    {
        IEnumerable<regions> allRegion();

        regions getRegionSlug(String slug);

        regions getRegion(String Id);

        regions getArea(string areaSlug);

        void addRegion(regions rec);

        regions updateRegion(String slug, regions p);

        void addArea(areas area, string Id);

        void updateArea(String regId, string areaId,  areas p);

        void removeRegion(String slug);

        void removeArea(String regSlug, string areaSlug);

        IEnumerable<regions> searchRegion(string rec);

        regions checkSlug(String slug);

        regions checkRegionCode(String regionCode);

        regions checkAreaCode(String areaCode);
    }
}
