﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Filters;
using Microsoft.AspNetCore.Cors;
using kmbi_core_master.spm.IRepository;
using kmbi_core_master.spm.Models;
using kmbi_core_master.coreMaster;

namespace kmbi_core_master.spm.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/clientProfiling")]
    [usersAuth]
    public class clientProfilingController : Controller
    {
        private readonly iclientProfilinRepository _Repository;
        private readonly iusersRepository _logInfo;
        private string token;

        public clientProfilingController(iclientProfilinRepository Repositor, iusersRepository logInfo)
        {
            this._Repository = Repositor;
            _logInfo = logInfo;
        }

        public void logger(string message)
        {
            //Insert Log//
            token = HttpContext.Request.Headers["token"];
            var lInfo = _logInfo.getLoginInfo(token);
            string log = message;
            global.createLog(lInfo.id, lInfo.fullname, lInfo.employment.branch, log);
            //Insert Log//
        }

        // GET: 
        [HttpGet]
        public IEnumerable<clientProfiling> GetAll()
        {
            var c = _Repository.getAll();
            return c;

        }

        [HttpGet("{slug}")]
        public IActionResult getSlug(string slug)
        {

            var item = _Repository.getBySlug(slug);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

       

        [HttpPost]
        [errorHandler]
        public IActionResult create([FromBody] clientProfiling b)
        {

            _Repository.add(b);

            logger( "Insert new profiling for " + b.client.name);

            return Ok(new
            {
                type = "success",
                message = b.client.name + " created successfully.",
                slug = b.slug
            });

        }



        [HttpPut("{slug}")]
        public IActionResult slugupdate(string slug, [FromBody] clientProfiling b)
        {
            var item = _Repository.getBySlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _Repository.update(slug, b);

           logger("Modified profiling for " + b.client.name);

            return Ok(new
            {
                type = "success",
                message = b.client.name + " updated successfully."
            });
        }




        [HttpDelete("{slug}")]
        public IActionResult slugdelete(string slug)
        {

            var item = _Repository.getBySlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.remove(slug);

            logger("Delete profiling for " + item.client.name);

            return Ok(new
            {
                type = "success",
                message = item.client.name + " deleted successfully."
            });
        }


    }
}
