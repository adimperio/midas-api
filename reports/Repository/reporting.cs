﻿using kmbi_core_master.reports.Models;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.acctngModule.Models;
using kmbi_core_master.mfModule.Models;

using System.Collections.Generic;
using System;

using kmbi_core_master.coreMaster.IRepository;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using MongoDB.Bson;

using System.Linq;
using kmbi_core_master.payrollModule.Models;
using kmbi_core_master.acctngModule.Interface;
using kmbi_core_master.payrollModule.IRepository;
using Microsoft.Extensions.Logging;
using kmbi_core_master.helpers;
using System.Text.RegularExpressions;

namespace kmbi_core_master.reports.IRepository
{
    public class reporting : ireporting
    {
        iloanRepository _loanrep;

        public static IMongoDatabase _db;
        private IOptions<departmentAcronymSettings> _depAcroSett;
        private iAccountingShared _acc;
        private IOptions<signatoriesSettings> _sigSetts;
        private IOptions<reportSetting> _reportSetting;
        private IOptions<fsSetting> _fsSetting;
        private icoaRepository _coa;
        private ipayrollRepository _payrollRep;
        private ILogger<reporting> _logger;
        private IOptions<fsSetting2> _fsSetting2;
        public IOptions<miSettings> _miSettings { get; }
        public ibranchRepository _branchRepo { get; }
        public iclientsRepository _clientRepo { get; }

        documentationModule.iRepositories.iholidayRepository _holidayRepo;

        public reporting(
            IOptions<dbSettings> settings, 
            iloanRepository loanrep, 
            IOptions<departmentAcronymSettings> depAcroSett, 
            iAccountingShared acc, 
            IOptions<signatoriesSettings> sigSetts, 
            IOptions<reportSetting> reportSetting, 
            IOptions<fsSetting> fsSetting, 
            icoaRepository coa, 
            ipayrollRepository payrollRep, 
            ILogger<reporting> logger, 
            IOptions<fsSetting2> fsSetting2,
            documentationModule.iRepositories.iholidayRepository holidayRepo,
            IOptions<miSettings> miSettings,
            ibranchRepository branchRepo,
            iclientsRepository clientRepo
            )
        {
            var coll = new MongoClient(settings.Value.MongoConnection);
            _db = coll.GetDatabase(settings.Value.Database);
            _loanrep = loanrep;
            _depAcroSett = depAcroSett;
            _acc = acc;
            _sigSetts = sigSetts;
            _reportSetting = reportSetting;
            _fsSetting = fsSetting;
            _coa = coa;
            _payrollRep = payrollRep;
            _logger = logger;
            _fsSetting2 = fsSetting2;
            _holidayRepo = holidayRepo;
            _miSettings = miSettings;
            _branchRepo = branchRepo;
            _clientRepo = clientRepo;
        }

        private miSettingsSet getMiFees(int loanCycle, int age) {
            miSettingsSet toReturn = null;
            _miSettings.Value.sets.ToList().ForEach(x=>{
                if((loanCycle >= x.loanCycleFrom && loanCycle <= x.loanCycleTo) && (age >= x.ageFrom && age <= x.ageTo) ){
                    toReturn = x;
                }
            });
            return toReturn;
        }

        private int CalculateAge(DateTime? dateOfBirth)  
        {  
            int age = 0;  
            if(dateOfBirth!=null) {
                var thisDate = Convert.ToDateTime(dateOfBirth);
                age = DateTime.Now.Year - thisDate.Year;  
                if (DateTime.Now.DayOfYear < thisDate.DayOfYear)  
                    age = age - 1;
            }
            return age;
        }

        private repEcrModel calculateEcr2(String refId)
        {

            var collLoans = _db.GetCollection<loans>("loans");

            // loan info
            var repLoan = collLoans.Aggregate()
                .Match(e => e.referenceId == refId)
                .Lookup("centerInstance", "centerSlug", "slug", "center")
                .Unwind("center",new AggregateUnwindOptions<center> {
                    PreserveNullAndEmptyArrays = true
                })
                .Lookup("officers", "officerSlug", "slug", "officers")
                .Unwind("officers",new AggregateUnwindOptions<center> {
                    PreserveNullAndEmptyArrays = true
                })
                .Project<repEcr>(new BsonDocument
                        {
                            {"_id",1},
                            {"centerDescription", "$center.code"},
                            {"centerAddress", "$center.address"},
                            {"assignedPA", "$officers.name"},
                            {"cycleNo","$centerLoanCycle"},
                            {"loanDate", "$disbursementDate"},
                            {"loanAmount","$amounts.principalAmount"},
                            {"interestRate","$loanType.interestRate"},
                            {"interestType","$loanType.interestType"},
                            {"transactionNo", "$referenceId"},
                            {"term", "$loanType.term"},
                            {"members.slug",1},
                            {"members.memberId",1},
                            {"members.fullname",1},
                            {"members.loanCycle",1},
                            {"members.principalAmount",1},
                            {"members.schedule.weekNo",1},
                            {"members.schedule.currentPrincipalDue",1},
                            {"members.schedule.currentInterestDue",1},
                            {"members.schedule.currentCbuDue",1},
                            {"members.schedule.collectionDate",1},
                            {"firstPaymentDate",1},
                            {"center",1}
                        })
                .FirstOrDefault();
            
            // payments
            var collPaymentLoans = _db.GetCollection<paymentLoans>("paymentLoans");
            var repPaymentLoan = collPaymentLoans.Find(Builders<paymentLoans>.Filter.Eq("loan.referenceId",repLoan.transactionNo)).ToList();
            
            // members
            var collClients = _db.GetCollection<clients>("clients");
            var repClients = collClients.Find(Builders<clients>.Filter.In(c=>c.Id,(from y in repLoan.members select y.memberId))).ToList();
            


            if (repLoan!=null)
            {

                // get cbu withdrawal
                var collCbuWith = _db.GetCollection<cbuWithdrawal>("cbuWithdrawal"); 
                var filterCbuWith = Builders<cbuWithdrawal>.Filter.Eq(x=>x.loan.refId,refId);
                var repCbuWith = collCbuWith.Find(filterCbuWith).ToList();
                
                repEcrModel ecr = new repEcrModel();
                
                ecr.center = repLoan.center;
                ecr.centerDescription = repLoan.centerDescription;
                ecr.centerAddress = repLoan.centerAddress;
                ecr.assignedPA = repLoan.assignedPA;
                ecr.cycleNo = repLoan.cycleNo;
                ecr.loanDate = repLoan.loanDate;
                ecr.maturityDate = repLoan.loanDate.AddMonths(6);
                ecr.loanAmount = repLoan.loanAmount;
                ecr.interestRate = repLoan.interestRate;
                ecr.interestType = repLoan.interestType;
                ecr.term = repLoan.term;
                ecr.transactionNo = repLoan.transactionNo;
                ecr.paymentSchedule  = repLoan.firstPaymentDate;

                List<repEcrLine> ecrLines = new List<repEcrLine>();
                var obituaries = new List<String>();
                foreach(loanMembers loanMembers in repLoan.members) 
                {
                    decimal balancePrincipal = (decimal)loanMembers.principalAmount;
                    decimal balanceInterest = (decimal)loanMembers.principalAmount * (decimal)repLoan.interestRate;
                    // get current cbu balance of client
                    decimal balanceCbu = (decimal)repClients.Find(x=>x.Id == loanMembers.memberId).cbuBalance;

                    // subtract payments to get cbubalance prior cbu payments
                    foreach(paymentLoans m in repPaymentLoan) {
                        foreach(paymentLoansMembers pl in from y in m.members where y.slug == loanMembers.slug select y) {
                            balanceCbu -= (decimal)pl.amounts.cbu;
                        }
                    }

                    decimal currentPrincipal = 0;
                    decimal currentInterest = 0;
                    decimal currentCbu = 0;

                    decimal paymentPrincipal = 0;
                    decimal paymentInterest = 0;
                    decimal paymentCbu = 0;

                    for(int ctr=1;ctr<=repLoan.term;ctr++) 
                    {
                        var ecrLine = new repEcrLine();

                        var p = 0.0;
                        var i = 0.0;
                        var c = 0.0;
                        
                        
                        //cbubalance
                        ecrLine.payments = new List<repEcrPayment>();

                        foreach(paymentLoans m in repPaymentLoan) {
                            
                            foreach(paymentLoansMembers pl in from y in m.members where y.slug == loanMembers.slug select y) {
                                
                                if(m.remarks!=null)
                                    if(m.remarks.ToLower() == "death" && pl.amounts.cbu > 0)
                                        obituaries.Add(pl.slug);


                                var trm = pl.terms.Where(t=>t.term == ctr-1).FirstOrDefault();
                                var lastTerm = 0;

                                if(trm!=null) {

                                    lastTerm = trm.term;
                                    if(lastTerm == ctr - 1) {
                                        if(pl.slug == loanMembers.slug){
                                            p += trm.amounts.principal;
                                            i += trm.amounts.interest;
                                            c += trm.amounts.cbu;

                                            ecrLine.payments.Add(
                                                new repEcrPayment {
                                                    orDate = m.officialReceipt.paidAt,
                                                    orNo = m.officialReceipt.number,
                                                    principal = trm.amounts.principal,
                                                    interest = trm.amounts.interest,
                                                    cbu = trm.amounts.cbu,
                                                    orprincipal = pl.amounts.principal,
                                                    orinterest = pl.amounts.interest,
                                                    orcbu = pl.amounts.cbu,
                                                    weekNo  = ctr-1
                                                }
                                            );
                                        }
                                    }
                                }
                            }
                        }

                        
                        balancePrincipal -= (decimal)p;
                        balanceInterest -= (decimal)i;
                        balanceCbu += (decimal)c;

                        currentPrincipal += (decimal)loanMembers.schedule[ctr-1].currentPrincipalDue - (decimal)p;
                        currentInterest += (decimal)loanMembers.schedule[ctr-1].currentInterestDue - (decimal)i;
                        
                        if(obituaries.Contains(loanMembers.slug))
                            loanMembers.schedule[ctr-1].currentCbuDue = c;
                        
                        currentCbu += (decimal)loanMembers.schedule[ctr-1].currentCbuDue - (decimal)c;
                        paymentPrincipal += (decimal)p;
                        paymentInterest += (decimal)i;
                        paymentCbu += (decimal)c;

                        ecrLine.weeklyRepaymentDate= loanMembers.schedule[ctr-1].collectionDate;
                        ecrLine.weekNo = ctr;
                        ecrLine.loanCyle = loanMembers.loanCycle;
                        ecrLine.memberId = loanMembers.memberId;
                        ecrLine.nameOfClient = loanMembers.fullname;

                        ecrLine.balancePrincipal = (double)balancePrincipal;
                        ecrLine.balanceInterest = (double)balanceInterest;
                        ecrLine.balanceCBU = (double)balanceCbu;

                        
                        
                        ecrLine.currentAmountDuePrincipal = (double)(loanMembers.schedule[ctr-1].currentPrincipalDue - (double)p);
                        ecrLine.currentAmountDueInterest = (double)(loanMembers.schedule[ctr-1].currentInterestDue - (double)i);
                        ecrLine.currentAmountDueCBU = (double)(loanMembers.schedule[ctr-1].currentCbuDue - (double)c);
                        
                        ecrLine.paymentPrincipal = p;
                        ecrLine.paymentInterest = i;
                        ecrLine.paymentCBU = c;
                        
                        ecrLine.pastDuePrincipal = (double)(currentPrincipal) - ecrLine.currentAmountDuePrincipal;
                        ecrLine.pastDueInterest = (double)(currentInterest) - ecrLine.currentAmountDueInterest;
                        ecrLine.pastDueCBU = (double)(currentCbu) - ecrLine.currentAmountDueCBU;

                        ecrLine.amountDueLoan = (double)currentPrincipal + (double)currentInterest;
                        ecrLine.amountDueCBU = (double)currentCbu;

                        ecrLine.lagda = "";

                        // cbu withdrawal inject
                        if (ecrLine.paymentCBU == 0) {
                            repCbuWith.ForEach(rcw=>{
                                rcw.members.ToList().ForEach(m=>{
                                    if(loanMembers.memberId == m.id) {
                                        ecrLine.balanceCBU = (double)ecrLine.balanceCBU - (double)m.amount;
                                    }
                                });
                            });
                        }
                        // cbu withdrawal inject end                    
                        ecrLines.Add(ecrLine);
                    }

                    
                }
                var ecrLineSorted = (from x in ecrLines orderby x.loanCyle descending, x.nameOfClient.Trim() select x).ToList();
                ecr.ecrLines= ecrLineSorted;

                return ecr;
            }
            else
            {
                return null;
            }
            
        }

        public repEcrModel getEcr(String refId, Int32 weekNo){
            repEcrModel ecr = calculateEcr2(refId);
            ecr.weekNo = weekNo;
             // set signatories
            var sigs = GetSignatories("ecr",ecr.center.branchSlug,ecr.center.unitSlug,ecr.center.officerSlug);

            ecr.preparedBy = sigs.preparedBy;
            ecr.checkedBy = sigs.checkedBy;
            ecr.approvedBy = sigs.approvedBy;

            // skip holidays
            var hollist = _holidayRepo.GetHolidays();
            var temp = ecr.paymentSchedule.AddDays(-7);
            for(var wk=0;wk<=ecr.weekNo-1;wk++) {
                temp = temp.AddDays(7);
                while(hollist.Where(x=>x.date == temp.Date).FirstOrDefault() != null) 
                    temp = temp.AddDays(7);
            }

            ecr.paymentSchedule = temp;
            
            List<repEcrLine> line = ecr.ecrLines.FindAll(c=>c.weekNo == weekNo);
            //List<repEcrLine> line = ecr.ecrLines;
            ecr.ecrLines =new List<repEcrLine>{};
            ecr.ecrLines.AddRange(line);
            
            

            return ecr;
        }

        public repEcrBatching getEcrBatching(String refId, Int32 weekNo) {
            repEcrBatching ecr = new repEcrBatching();
            var batch1 = getEcr(refId, weekNo);
            
            var list = _loanrep.getByCenterLoanType(batch1.center.slug,"5897e52464443e05cac895cc","for-payment");

            var modelList = new List<repEcrModel>();
            list.ForEach(x=>{
                var sked = x.members[0].schedule.Where(y=>y.collectionDate == batch1.paymentSchedule).FirstOrDefault();
                if(sked!=null)
                    modelList.Add(getEcr(x.referenceId,sked.weekNo));
            });
            
            
            List<repEcrBatchingItem> items = new List<repEcrBatchingItem>();

            ecr.centerDescription = batch1.centerDescription;
            ecr.centerAddress = batch1.centerAddress;
            ecr.assignedPO = batch1.assignedPA;
            ecr.paymentSchedule = batch1.paymentSchedule.ToShortDateString();
            ecr.interestRate = batch1.interestRate;
            ecr.interestType = batch1.interestType;
            
            List<repEcrBatchInfo> batchInfos= new List<repEcrBatchInfo>();
            modelList.ForEach(x=>{

                // batchinfo

                batchInfos.Add(new repEcrBatchInfo {
                    loanDate = x.loanDate,
                    maturityDate = x.maturityDate,
                    loanAmount = x.loanAmount,
                    referenceNo = x.transactionNo,
                    cycleNo = x.cycleNo,
                    weekNo = x.weekNo
                });

                repEcrBatchingItem item = new repEcrBatchingItem();
                
                item.cycleNo = x.cycleNo;
                item.loanDate = x.loanDate;
                item.maturityDate = x.maturityDate;
                item.loanAmount = x.loanAmount;
                item.interestRate = x.interestRate;
                item.interestType = x.interestType;
                item.referenceNo = x.transactionNo;

                var lineItems = new List<repEcrBatchingItemLines>();
                    var counter = 1;
                    x.ecrLines.ForEach(y=>{
                        var lagda = "";
                        //var lagda = y.paymentPrincipal.ToString() + " | " + y.paymentInterest.ToString() + " | " + y.paymentCBU.ToString();

                        lineItems.Add(new repEcrBatchingItemLines {
                            counter = counter,
                            name = y.nameOfClient,
                            balances = new repEcrBatchingItemBreakdown {
                                principal = y.balancePrincipal,
                                interest = y.balanceInterest,
                                cbu = y.balanceCBU
                            },
                            currentDueAmount = new repEcrBatchingItemBreakdown {
                                principal = y.currentAmountDuePrincipal,
                                interest = y.currentAmountDueInterest,
                                cbu = y.currentAmountDueCBU
                            },
                            pastDueAmount = new repEcrBatchingItemBreakdown {
                                principal = y.pastDuePrincipal,
                                interest = y.pastDueInterest,
                                cbu = y.pastDueCBU
                            },
                            totalAmountDue = new repEcrBatchingItemBreakdown {
                                principal = y.currentAmountDuePrincipal + y.pastDuePrincipal,
                                interest = y.currentAmountDueInterest + y.pastDueInterest,
                                cbu = y.currentAmountDueCBU + y.pastDueCBU
                            },
                            lagda = lagda,
                            loanCycle = y.loanCyle
                        });
                        counter++;
                    });

                

                item.lines = lineItems.ToArray();
                item.total = new repEcrBatchingItemLines {
                    name = "TOTAL",
                    balances = new repEcrBatchingItemBreakdown {
                        principal = item.lines.Sum(xx=>xx.balances.principal),
                        interest = item.lines.Sum(xx=>xx.balances.interest),
                        cbu = item.lines.Sum(xx=>xx.balances.cbu)
                    },
                    currentDueAmount = new repEcrBatchingItemBreakdown {
                        principal = item.lines.Sum(xx=>xx.currentDueAmount.principal),
                        interest = item.lines.Sum(xx=>xx.currentDueAmount.interest),
                        cbu = item.lines.Sum(xx=>xx.currentDueAmount.cbu)
                    },
                    pastDueAmount = new repEcrBatchingItemBreakdown {
                        principal = item.lines.Sum(xx=>xx.pastDueAmount.principal),
                        interest = item.lines.Sum(xx=>xx.pastDueAmount.interest),
                        cbu = item.lines.Sum(xx=>xx.pastDueAmount.cbu)
                    },
                    totalAmountDue = new repEcrBatchingItemBreakdown {
                        principal = item.lines.Sum(xx=>xx.totalAmountDue.principal),
                        interest = item.lines.Sum(xx=>xx.totalAmountDue.interest),
                        cbu = item.lines.Sum(xx=>xx.totalAmountDue.cbu)
                    }
                };

                items.Add(item);
            });


            ecr.batchInfo = batchInfos.ToArray();
            ecr.items = items.ToArray();

            ecr.total = new repEcrBatchingItemLines {
                name = "TOTAL",
                balances = new repEcrBatchingItemBreakdown {
                    principal = ecr.items.Sum(xx=>xx.lines.Sum(xy=>xy.balances.principal)),
                    interest = ecr.items.Sum(xx=>xx.lines.Sum(xy=>xy.balances.interest)),
                    cbu = ecr.items.Sum(xx=>xx.lines.Sum(xy=>xy.balances.cbu)),
                },
                currentDueAmount = new repEcrBatchingItemBreakdown {
                    principal = ecr.items.Sum(xx=>xx.lines.Sum(xy=>xy.currentDueAmount.principal)),
                    interest = ecr.items.Sum(xx=>xx.lines.Sum(xy=>xy.currentDueAmount.interest)),
                    cbu = ecr.items.Sum(xx=>xx.lines.Sum(xy=>xy.currentDueAmount.cbu)),
                },
                pastDueAmount = new repEcrBatchingItemBreakdown {
                    principal = ecr.items.Sum(xx=>xx.lines.Sum(xy=>xy.pastDueAmount.principal)),
                    interest = ecr.items.Sum(xx=>xx.lines.Sum(xy=>xy.pastDueAmount.interest)),
                    cbu = ecr.items.Sum(xx=>xx.lines.Sum(xy=>xy.pastDueAmount.cbu)),
                },
                totalAmountDue = new repEcrBatchingItemBreakdown {
                    principal = ecr.items.Sum(xx=>xx.lines.Sum(xy=>xy.totalAmountDue.principal)),
                    interest = ecr.items.Sum(xx=>xx.lines.Sum(xy=>xy.totalAmountDue.interest)),
                    cbu = ecr.items.Sum(xx=>xx.lines.Sum(xy=>xy.totalAmountDue.cbu)),
                }
            };
            
            ecr.preparedBy = batch1.preparedBy;
            ecr.checkedBy = batch1.checkedBy;

            var asWholeItems = new List<repEcrBatchingItemLines>();
            ecr.items.ToList().ForEach(x=>{
                asWholeItems.AddRange(x.lines);
            });

            
            asWholeItems = (from x in asWholeItems orderby x.loanCycle descending, x.name select x).ToList();


            ecr.asWholeItems = asWholeItems.ToArray();

            return ecr;
        }
        

        public repProspectiveClients getProspectiveClients(String refId)
        {
            var coll = _db.GetCollection<repProspectiveClients>("loans");
            var rep = coll.Aggregate()
                .Match(e => e.referenceId == refId)
                .Lookup("branch", "branchSlug", "slug", "branch")
                .Lookup("centerInstance", "centerSlug", "slug", "center")
                .Project<repProspectiveClients>(new BsonDocument
                        {
                            {"referenceId", 1},
                            {"branch", 1},
                            {"center", 1},
                            {"members",1}
                        })
                .FirstOrDefault();
            rep.center[0].loanCycle += 1;
            return rep;
        }

        public repSola getsummaryOfLoanApplication(String refId)
        {
            var coll = _db.GetCollection<loans>("loans");
            var rep = coll.Aggregate()
                .Match(e => e.referenceId == refId)
                .Lookup("centerInstance", "centerSlug", "slug", "center")
                .Project<repSola>(new BsonDocument
                        {
                            {"referenceId", 1},
                            {"centerLoanCycle",1},
                            {"centerSlug",1},
                            {"center", 1},
                            {"members",1}
                        })
                .FirstOrDefault();

            var coll2 = _db.GetCollection<loans>("loans");
            var rep2 = coll2.Aggregate()
                .Match(e => e.centerSlug == rep.centerSlug)
                .Match(e => e.centerLoanCycle == rep.centerLoanCycle -1)
                .Project<repSola>(new BsonDocument
                        {
                            {"referenceId", 1},
                            {"centerLoanCycle",1},
                            {"centerSlug",1},
                            {"center", 1},
                            {"members",1}
                        })
                .FirstOrDefault();
            
            if(rep2!=null) {
                rep.members.ForEach(m=>{
                    rep2.members.ForEach(mm=>{
                        if(m.memberId == mm.memberId) {
                            m.prevLoan = mm.principalAmount;
                        }
                    });
                });
            }
                        
            return rep;
        }
        
        public repSola getsummaryOfLoanApplicationv2(String refId)
        {
            var coll = _db.GetCollection<loans>("loans");
            var rep = coll.Aggregate()
                .Match(e => e.referenceId == refId)
                .Lookup("centerInstance", "centerSlug", "slug", "center")
                .Project<repSola>(new BsonDocument
                        {
                            {"referenceId", 1},
                            {"disbursementDate", 1},
                            {"centerLoanCycle",1},
                            {"centerSlug",1},
                            {"center", 1},
                            {"members",1}
                        })
                .FirstOrDefault();

            // get all loans
            var loans = coll.Find(x=>x.branchSlug == rep.center[0].branchSlug && x.referenceId != refId && x.status != "processing" && x.disbursementDate < rep.disbursementDate).ToList();
            loans = loans.OrderByDescending(x=>x.disbursementDate).ToList();
            
            rep.members.ToList().ForEach(mem=>{
                var foundYet = false;
                loans.ForEach(loan=>{
                    if(!foundYet) {
                        if(loan.members.Select(m=>m.memberId).Contains(mem.memberId)) {
                            mem.prevLoan = loan.members.Where(x=>x.memberId == mem.memberId).FirstOrDefault().principalAmount;
                            foundYet = true;
                        }
                    }
                });
            });

            return rep;
        }

        public repRfpCv getrfpcv(String refId)
        {
            var coll = _db.GetCollection<repRfpCv>("checkVoucher");
            
            var rep = coll.Aggregate()
                    .Match(e => e.referenceId == refId)
                    //.Lookup("loans", "referenceId", "referenceId", "loans")
                    .Lookup("branch", "branchSlug", "slug", "branch")
                    .Lookup("centerInstance", "centerSlug", "slug", "center")
                    .Project<repRfpCv>(new BsonDocument
                            {
                            {"referenceId", 1},
                           // {"loans",1},
                            {"rfp", 1},
                            {"cv", 1},
                            {"branch", 1},
                            {"center", 1}
                            })
                    .FirstOrDefault();
            return rep;
        }

        public repUpfrontFees getupfrontFeeSchedule(String refId)
        {
            var coll = _db.GetCollection<repUpfrontFees>("loans");
            var rep = coll.Aggregate()
                .Match(e => e.referenceId == refId)
                .Lookup("centerInstance", "centerSlug", "slug", "center")
                .Project<repUpfrontFees>(new BsonDocument
                        {
                            {"referenceId", 1},
                            {"center", 1},
                            {"members",1}
                        })
                .FirstOrDefault();
            rep.center[0].loanCycle +=1;

            var memCount = rep.members.Count();
            var totalMi = rep.members.Sum(x=>x.upfrontFees.miPremium);

            var miFee = 0.0;
            var miPA = 0.0;

            rep.members.ForEach(x=>{
                var age = DateTime.Now.Date.Subtract(DateTime.Parse(x.birthdate.ToString())).Days / 365;
                var set = getMiFees(rep.center[0].loanCycle, age);
                miFee += set.admin;
                miPA += set.pai;
            });

            //var miFee = rep.members.Sum(x=>(((x.birthdate == Convert.ToDateTime("01/01/0001 12:00 AM")) ? 0 : (DateTime.Now.Date.Subtract(DateTime.Parse(x.birthdate.ToString())).Days) / 365) >= _miSettings.Value.adminFeeAge) ? _miSettings.Value.adminFeeGte : _miSettings.Value.adminFeelt);

            var miPayable = totalMi - miFee;
            
            //var miPA = memCount * _miSettings.Value.paInsurance;
            
            var miPay = miPayable - miPA;

            rep.newLoan = rep.members.Where(x=>x.loanCycle == 1).Count();
            rep.reLoan = memCount - rep.newLoan;
            rep.dependents = rep.members.Select(x=>x.dependents.Count()).Sum();
            rep.miPA = miPA;
            rep.miPay = miPay;

            rep.cbu = rep.members.Sum(x=>x.upfrontFees.CBUAmount);
            rep.processingFee = rep.members.Sum(x=>x.upfrontFees.loanProcessingFee);
            rep.miPayable = miPayable;
            rep.miFee = miFee;;
            rep.dst = Math.Round(rep.members.Sum(x=>x.upfrontFees.dst),2);
            return rep;
        }

        public repWeeklyScheduleModel getweeklyPaymentSchedule(String refId)
        {
            var coll = _db.GetCollection<repWeeklySchedule>("loans");
            var rep = coll.Aggregate()
                .Match(e => e.referenceId == refId)
                .Lookup("centerInstance", "centerSlug", "slug", "center")
                .Project<repWeeklySchedule>(new BsonDocument
                        {
                            {"referenceId", 1},
                            {"loanType",1},
                            {"branch", 1},
                            {"center", 1},
                            {"members",1}
                        })
                .FirstOrDefault();

            if(rep!=null)
            {
                List<repWeeklyScheduleModelLine> list = new List<repWeeklyScheduleModelLine>();

                for(int ctr=1;ctr<=rep.loanType.term;ctr++)
                {
                    double totalCurrentPrincipalDue=0;
                    double totalCurrentInterestDue=0;
                    double totalCurrentCbuDue=0;
                    DateTime coldate = new DateTime();
                    foreach(loanMembers loan in rep.members)
                    {
                        foreach(schedule sked in from skeds in loan.schedule where skeds.weekNo == ctr select skeds)
                        {
                            coldate = sked.collectionDate;
                            totalCurrentPrincipalDue += sked.currentPrincipalDue;
                            totalCurrentInterestDue += sked.currentInterestDue;
                            totalCurrentCbuDue += sked.currentCbuDue;
                        }
                    }

                    list.Add(new repWeeklyScheduleModelLine {
                        weekNo = ctr,
                        collectionDate = coldate,
                        totalPrincipalDue = totalCurrentPrincipalDue,
                        totalInterestDue = totalCurrentInterestDue,
                        totalCBUDue = totalCurrentCbuDue,
                        total = totalCurrentPrincipalDue + totalCurrentInterestDue + totalCurrentCbuDue
                    });
                }
                
                rep.center[0].loanCycle +=1;
                return new repWeeklyScheduleModel{
                    referenceId = rep.referenceId,
                    loanType = rep.loanType,
                    center = rep.center,
                    lines = list
                };
            }
            else
            {
                return null;
            }
            
        }

        public repPromisorryNote getpromisoryNote(String refId)
        {
            var coll = _db.GetCollection<repPromisorryNote>("loans");
            var rep = coll.Aggregate()
                .Match(e => e.referenceId == refId)
                .Lookup("centerInstance", "centerSlug", "slug", "center")
                .Project<repPromisorryNote>(new BsonDocument
                        {
                            {"referenceId", 1},
                            {"disbursementDate", 1},
                            {"firstPaymentDate", 1},
                            {"amounts", 1},
                            {"loanType", 1},
                            {"members",1},
                            {"center",1}
                        })
                .FirstOrDefault();

            double totalWeekly = 0.0;
            if (rep.loanType.term == 22)
            {
                foreach (var mem in rep.members)
                {
                    foreach (var sched in mem.schedule)
                    {
                        if (sched.weekNo <= 21)
                        {
                            totalWeekly += sched.currentPrincipalDue + sched.currentInterestDue;
                        }
                    
                    }
                }
                totalWeekly = totalWeekly / 21;
            }


            if (rep.loanType.term == 24)
            {
                foreach (var mem in rep.members)
                {
                    foreach (var sched in mem.schedule)
                    {
                        totalWeekly += sched.currentPrincipalDue + sched.currentInterestDue;
                    }
                }
                totalWeekly = totalWeekly / 24;
            }


            rep.weeklyAmount = totalWeekly;
            rep.maturityDate = rep.disbursementDate.AddMonths(6);
            
            rep.version = _reportSetting.Value.pnVersion;
            return rep;
        }

        public repDisclosure getDisclosure(String refId)
        {
            var coll = _db.GetCollection<repDisclosure>("loans");
            var rep = coll.Aggregate()
                .Match(e => e.referenceId == refId)
                .Lookup("centerInstance", "centerSlug", "slug", "center")
                .Lookup("branch", "branchSlug", "slug", "branch")
                .Project<repDisclosure>(new BsonDocument
                        {
                            {"referenceId", 1},
                            {"disbursementDate", 1},
                            {"firstPaymentDate", 1},
                            {"amounts", 1},
                            {"loanType", 1},
                            {"members",1},
                            {"center",1},
                            {"branch",1}
                        })
                .FirstOrDefault();

            rep.members = rep.members.OrderByDescending(x=>x.loanCycle).ThenBy(x=>x.name.last).ToList();
            return rep;
        }

        public repDisclosure getDisclosureSummary(String refId)
        {
            var coll = _db.GetCollection<repDisclosure>("loans");
            var rep = coll.Aggregate()
                .Match(e => e.referenceId == refId)
                .Lookup("centerInstance", "centerSlug", "slug", "center")
                .Lookup("branch", "branchSlug", "slug", "branch")
              //  .Unwind("branch")
                .Project<repDisclosure>(new BsonDocument
                        {
                            {"referenceId", 1},
                            {"disbursementDate", 1},
                            {"firstPaymentDate", 1},
                            {"amounts", 1},
                            {"loanType", 1},
                            {"members",1},
                            {"center",1},
                            {"branch",1}
                        })
                .FirstOrDefault();

            rep.members = rep.members.OrderByDescending(x=>x.loanCycle).ThenBy(x=>x.name.last).ToList();
            rep.members.ForEach(x=>{
                x.schedule = x.schedule.Where(y=>y.weekNo == 1).ToList();
            });
            return rep;
        }

        public repDstModel getDst(string branchId, string month, int? year)
        {
            DateTime from;
            Boolean isValid = DateTime.TryParse(month + "/1/" + year, out from);

            if(isValid)
            {
                DateTime to = from.AddMonths(1).AddSeconds(-1);

                var coll = _db.GetCollection<repDst>("loans");
                var rep = coll.Aggregate()
                    .Match(e => e.disbursementDate >= from && e.disbursementDate <= to)
                    .Match(e => e.branchId == branchId)
                    .Match(e => e.status == "for-payment")
                    .Lookup("centerInstance", "centerSlug", "slug", "center")
                    .Lookup("branch", "branchSlug", "slug", "branch")
                    .Project<repDst>(new BsonDocument
                            {
                                {"referenceId", 1},
                                {"branchId", 1},
                                {"disbursementDate", 1},
                                {"firstPaymentDate", 1},
                                {"status", 1},
                                {"amounts", 1},
                                {"members",1},
                                {"center",1},
                                {"branch",1}
                            })
                   .ToList();

                if(rep!=null)
                {
                    if(rep.Count>0)
                    {
                        // List<string> centers = new List<string>();
                        // foreach(repDst _dst in rep)
                        // {
                        //     if(_dst.center!=null) {
                        //         if(_dst.center.Count > 0) {
                        //             if (!centers.Exists(f => f == _dst.center[0].code))
                        //             {
                        //                 centers.Add(_dst.center[0].code);
                        //             }
                        //         }
                                
                        //     }
                            
                        // }

                        // List<repDstModelLine> lines = new List<repDstModelLine>();
                        // int ctr=1;
                        // foreach(String c in centers)
                        // {
                            
                        //     double tota = 0;
                        //     double totb = 0;
                        //     foreach(repDst _dst in from x in rep where x.center[0].code == c select x)
                        //     {
                        //         tota += _dst.amounts.principalAmount;
                        //         totb +=(from y in rep[0].members select y.upfrontFees.dst).Sum();

                        //     }
                            
                        //         lines.Add(new repDstModelLine {
                        //             seqNo = ctr,
                        //             centerName = c,
                        //             dateOfRelease = rep[0].disbursementDate,
                        //             dateOfMaturity = rep[0].disbursementDate.AddMonths(6),
                        //             atc = "DS106",
                        //             amountRelease = tota,
                        //             dstCollected = ((tota/200)*168)/365 //totb
                        //         });
                        //         ctr+=1;
                        // }
                    
                    
                        
                        List<repDstModelLine> lines = new List<repDstModelLine>();
                        Int32 ctr = 1;
                        rep.ForEach((x) => {

                            lines.Add(new repDstModelLine {
                                seqNo = ctr,
                                centerName = x.center[0].code,
                                dateOfRelease = x.disbursementDate,
                                dateOfMaturity = x.disbursementDate.AddMonths(6),
                                atc = "DS106",
                                amountRelease = x.amounts.principalAmount,
                                dstCollected = (from y in x.members select y.upfrontFees.dst).Sum() //((tota/200)*168)/365 //totb
                            });
                            ctr++;
                        });

                        return new repDstModel {
                            branchName = rep[0].branch[0].name,
                            tin = rep[0].branch[0].tinNumber,
                            lines = lines
                        };




                    }
                    else
                    {
                        return null;
                    }
                    
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        private repMiModel converMi(repMi rep)
        {
            List<repMiModelLine> lines = new List<repMiModelLine>();
            
            // clients for beneficiaryinfo
            
            List<clients> clients = _db.GetCollection<clients>("clients").Find(c=>c.branchId == rep.branch[0].Id).ToList();
            
            foreach(loanMembers m in rep.members)
            {

                DateTime birthdate = Convert.ToDateTime("01/01/0001 12:00 AM");;
                DateTime spouseDateOfBirth = Convert.ToDateTime("01/01/0001 12:00 AM");
                
                if(m.birthdate != null)
                {
                    birthdate =m.birthdate;
                }
                if(m.spouse!=null) {
                    if(m.spouse.birthdate != null)
                    {
                        spouseDateOfBirth = DateTime.Parse(m.spouse.birthdate.ToString());
                    }
                }

                // get beneficiary info from client
                var cl = clients.Find(x=>x.Id == m.memberId);
                var pbenbdate = DateTime.Now;
                var pbenage = 0;

                if(cl!=null) {
                    pbenbdate = Convert.ToDateTime(cl.beneficiary.birthdate);
                    pbenage = (DateTime.Now.Date.Subtract(DateTime.Parse(pbenbdate.ToString())).Days) / 365;
                }
                
                repMiModelLine _l = new repMiModelLine{
                    lastname = m.name.last,
                    firstname = m.name.first,
                    middlename = m.name.middle,
                    principalBeneficiary = m.beneficiary,
                    principalBeneficiaryBirthdate = pbenbdate,
                    principalBeneficiaryAge = pbenage,
                    dateOfLoanRelease = rep.disbursementDate,
                    dateOfLoanMaturity = rep.disbursementDate.AddMonths(6),
                    LoanCycle = m.loanCycle,
                    memberDateOfBirth = birthdate,
                    age =(birthdate == Convert.ToDateTime("01/01/0001 12:00 AM")) ? 0 : (DateTime.Now.Date.Subtract(DateTime.Parse(birthdate.ToString())).Days) / 365,
                    maritalStatus = m.civilStatus
                };

                Int32 count = 1;
                Int32 c_count = 0;

                String[] childComparer = {"child","chiild","son","daughter"};
                String[] parentComparer = {"parent","father","mother"};
                String[] siblingComparer = {"brother","sister","sibling"};

                foreach(loanMemberDependents _d in m.dependents) {

                    if (_d.included==true) {
                        if(count==1 && childComparer.Contains(_d.type.ToLower())){
                            _l.child1Name = _d.name;
                            _l.child1DateOfBirth = DateTime.Parse(_d.birthdate.ToString());
                            _l.child1Age = (DateTime.Now.Date.Subtract(DateTime.Parse(_d.birthdate.ToString())).Days) / 365;
                            c_count++;
                            count++;
                        } else if(count==2 && childComparer.Contains(_d.type.ToLower())){
                            _l.child2Name = _d.name;
                            _l.child2DateOfBirth = DateTime.Parse(_d.birthdate.ToString());
                            _l.child2Age = (DateTime.Now.Date.Subtract(DateTime.Parse(_d.birthdate.ToString())).Days) / 365;
                            c_count++;
                            count++;
                        } else if(count==3 && childComparer.Contains(_d.type.ToLower())){
                            _l.child3Name = _d.name;
                            _l.child3DateOfBirth = DateTime.Parse(_d.birthdate.ToString());
                            _l.child3Age = (DateTime.Now.Date.Subtract(DateTime.Parse(_d.birthdate.ToString())).Days) / 365;
                            c_count++;
                            count++;
                        } else if(count==4 && childComparer.Contains(_d.type.ToLower())){
                            _l.child4Name = _d.name;
                            _l.child4DateOfBirth = DateTime.Parse(_d.birthdate.ToString());
                            _l.child4Age = (DateTime.Now.Date.Subtract(DateTime.Parse(_d.birthdate.ToString())).Days) / 365;
                            c_count++;
                            count++;
                        } else if(count==5 && childComparer.Contains(_d.type.ToLower())){
                            _l.child5Name = _d.name;
                            _l.child5DateOfBirth = DateTime.Parse(_d.birthdate.ToString());
                            _l.child5Age = (DateTime.Now.Date.Subtract(DateTime.Parse(_d.birthdate.ToString())).Days) / 365;
                            c_count++;
                            count++;
                        } else if(count==6 && childComparer.Contains(_d.type.ToLower())){
                            _l.child6Name = _d.name;
                            _l.child6DateOfBirth = DateTime.Parse(_d.birthdate.ToString());
                            _l.child6Age = (DateTime.Now.Date.Subtract(DateTime.Parse(_d.birthdate.ToString())).Days) / 365;
                            c_count++;
                            count++;
                        } else if(count==7 && childComparer.Contains(_d.type.ToLower())){
                            _l.child7Name = _d.name;
                            _l.child7DateOfBirth = DateTime.Parse(_d.birthdate.ToString());
                            _l.child7Age = (DateTime.Now.Date.Subtract(DateTime.Parse(_d.birthdate.ToString())).Days) / 365;
                            c_count++;
                            count++;
                        } else if(count==8 && childComparer.Contains(_d.type.ToLower())){
                            _l.child8Name = _d.name;
                            _l.child8DateOfBirth = DateTime.Parse(_d.birthdate.ToString());
                            _l.child8Age = (DateTime.Now.Date.Subtract(DateTime.Parse(_d.birthdate.ToString())).Days) / 365;
                            c_count++;
                            count++;
                        } else if(count==9 && childComparer.Contains(_d.type.ToLower())){
                            _l.child9Name = _d.name;
                            _l.child9DateOfBirth = DateTime.Parse(_d.birthdate.ToString());
                            _l.child9Age = (DateTime.Now.Date.Subtract(DateTime.Parse(_d.birthdate.ToString())).Days) / 365;
                            c_count++;
                            count++;
                        } else if(count==10 && childComparer.Contains(_d.type.ToLower())){
                            _l.child10Name = _d.name;
                            _l.child10DateOfBirth = DateTime.Parse(_d.birthdate.ToString());
                            _l.child10Age = (DateTime.Now.Date.Subtract(DateTime.Parse(_d.birthdate.ToString())).Days) / 365;
                            c_count++;
                            count++;
                        }
                    }
                }

                count=0;
                Int32 p_count = 0;
                foreach(loanMemberDependents _d in m.dependents)
                {
                    if (_d.included == true) {
                        if(p_count==1 && parentComparer.Contains(_d.type.ToLower())){
                            _l.parent2Name = _d.name;
                            _l.parent2DateOfBirth = DateTime.Parse(_d.birthdate.ToString());
                            _l.parent2Age = (DateTime.Now.Date.Subtract(DateTime.Parse(_d.birthdate.ToString())).Days) / 365;
                            p_count++;
                        } else if (parentComparer.Contains(_d.type.ToLower())) {
                            _l.parent1Name = _d.name;
                            _l.parent1DateOfBirth = DateTime.Parse(_d.birthdate.ToString());
                            _l.parent1Age = (DateTime.Now.Date.Subtract(DateTime.Parse(_d.birthdate.ToString())).Days) / 365;
                            p_count++;
                        }
                    }
                    count++;
                }

                count=0;
                Int32 s_count = 0;
                foreach(loanMemberDependents _d in m.dependents)
                {
                    count++;
                    if (_d.included==true) {
                        
                        if(s_count==0 && siblingComparer.Contains(_d.type.ToLower())){
                            _l.sibling1Name = _d.name;
                            _l.sibling1DateOfBirth = DateTime.Parse(_d.birthdate.ToString());
                            _l.sibling1Age = (DateTime.Now.Date.Subtract(DateTime.Parse(_d.birthdate.ToString())).Days) / 365;
                            s_count++;
                        } else if(s_count==1 && siblingComparer.Contains(_d.type.ToLower())){
                            _l.sibling2Name = _d.name;
                            _l.sibling2DateOfBirth = DateTime.Parse(_d.birthdate.ToString());
                            _l.sibling2Age = (DateTime.Now.Date.Subtract(DateTime.Parse(_d.birthdate.ToString())).Days) / 365;
                            s_count++;
                        } else if(s_count==2 && siblingComparer.Contains(_d.type.ToLower())){
                            _l.sibling3Name = _d.name;
                            _l.sibling3DateOfBirth = DateTime.Parse(_d.birthdate.ToString());
                            _l.sibling3Age = (DateTime.Now.Date.Subtract(DateTime.Parse(_d.birthdate.ToString())).Days) / 365;
                            s_count++;
                        } else if(s_count==3 && siblingComparer.Contains(_d.type.ToLower())){
                            _l.sibling4Name = _d.name;
                            _l.sibling4DateOfBirth = DateTime.Parse(_d.birthdate.ToString());
                            _l.sibling4Age = (DateTime.Now.Date.Subtract(DateTime.Parse(_d.birthdate.ToString())).Days) / 365;
                            s_count++;
                        } else if(s_count==4 && siblingComparer.Contains(_d.type.ToLower())){
                            _l.sibling5Name = _d.name;
                            _l.sibling5DateOfBirth = DateTime.Parse(_d.birthdate.ToString());
                            _l.sibling5Age = (DateTime.Now.Date.Subtract(DateTime.Parse(_d.birthdate.ToString())).Days) / 365;
                            s_count++;
                        } 
                    }
                }                
                
                count=0;
                Int32 sp_count = 0;

                foreach(loanMemberDependents _d in m.dependents)
                {
                    if(_d.type.ToLower() == "spouse") {
                        if(m.spouse!=null && _d.included==true) {
                            sp_count +=1;

                            _l.spouseName = _d.name;
                            _l.spouseDateOfBirth = DateTime.Parse(_d.birthdate.ToString());
                            _l.spouseAge = (_d.birthdate == Convert.ToDateTime("01/01/0001 12:00 AM")) ? 0 : (DateTime.Now.Date.Subtract(DateTime.Parse(_d.birthdate.ToString())).Days) / 365;        
                            // if(m.spouse.name.first != "-" && m.spouse.name.first != "" && m.spouse.name.first != null && m.spouse.name.first != "N/A" ) {
                            //     _l.spouseName = m.spouse.name.full;
                            //     _l.spouseDateOfBirth = DateTime.Parse(spouseDateOfBirth.ToString());
                            //     _l.spouseAge = (spouseDateOfBirth == Convert.ToDateTime("01/01/0001 12:00 AM")) ? 0 : (DateTime.Now.Date.Subtract(DateTime.Parse(spouseDateOfBirth.ToString())).Days) / 365;        
                            // }
                        }
                    }
                }
          
                
                
                _l.clientCount = 1;
                _l.childCount = c_count;
                _l.parentCount = p_count;
                _l.siblingCount = s_count;
                _l.spouseCount = sp_count;

                var set = getMiFees(rep.members[0].loanCycle, _l.age);
                _l.premiumForFirstLife = m.upfrontFees.miPremium - set.pai;
                _l.personalAccidentInsurance = set.pai;
                
                _l.feeRetainedByKmbi  = set.admin;

                _l.premiumForFirstLife -= _l.feeRetainedByKmbi;
                
                _l.paidByClient = _l.premiumForFirstLife + _l.personalAccidentInsurance + _l.feeRetainedByKmbi;
                
                lines.Add(_l);
            }
            return new repMiModel{
                branchName = rep.branch[0].name,
                unitName = rep.branch[0].units[0].name,
                center = rep.center[0],
                centerCode = rep.center[0].code,
                month = rep.disbursementDate.ToString("MMMM"),
                lines = lines
            };
        }

        public repMiModel getMiPerReferenceId(String refId)
        {
            var coll = _db.GetCollection<repMi>("loans");
            var rep = coll.Aggregate()
                .Match(e => e.referenceId == refId)
                .Lookup("centerInstance", "centerSlug", "slug", "center")
                .Lookup("branch", "branchSlug", "slug", "branch")
                .Project<repMi>(new BsonDocument
                        {
                            {"referenceId", 1},
                            {"disbursementDate", 1},
                            {"firstPaymentDate", 1},
                            {"amounts", 1},
                            {"members",1},
                            {"center",1},
                            {"branch",1}
                        })
                .FirstOrDefault();
            if(rep!=null)
            {
                return converMi(rep);
            }
            else
            {
                return null;
            }
        }

        public List<repMiModel> getMiPerMonthYearPerBranch(Int32 month, Int32 year, String branchSlug)
        {
            DateTime _from = DateTime.Parse(month.ToString() + "/1/" + year.ToString());
            DateTime _to = _from.AddMonths(1).AddDays(-1);

            var coll = _db.GetCollection<loans>("loans");
            var rep = coll.Aggregate()
                .Match(e => e.branchSlug == branchSlug)
                .Match(e => e.disbursementDate >= _from && e.disbursementDate <= _to)
                .Lookup("centerInstance", "centerSlug", "slug", "center")
                .Lookup("branch", "branchSlug", "slug", "branch")
                .Project<repMi>(new BsonDocument
                        {
                            {"referenceId", 1},
                            {"disbursementDate", 1},
                            {"firstPaymentDate", 1},
                            {"amounts", 1},
                            {"members",1},
                            {"center",1},
                            {"branch",1}
                        })
                .ToList();

            List<repMiModel> list = new List<repMiModel>();

            if (rep!=null)
            {
                foreach(repMi r in rep)
                {
                    list.Add(converMi(r));
                }

                return list;
            }
            else
            {
                return null;
            }
            
        }

        

        

        public List<repCic> getCic()
        {
            var coll = _db.GetCollection<clients>("clients");
            var rep = coll.Aggregate()
                .Lookup("branch", "branchSlug", "slug", "branch")
                .Unwind("branch")
                .Project<repCic>(new BsonDocument
                        {
                            {"branchCode", "$branch.code"},
                            {"title", 1},
                            {"name", 1},
                            {"gender", 1},
                            {"birthdate",1},
                            {"dependents",new BsonDocument
                                {
                                    {"$size","$dependents"}
                                }
                            },
                            {"spouse.name",1},
                            {"civilStatus",1},
                            {"addresses",1},
                            {"ids",1},
                            {"contacts",1}
                        })
                .ToList();
            
            var coll2 = _db.GetCollection<loans>("loans");
            var rep2 = coll2.Aggregate()
                .Match(e => (e.loanType.name=="Group Loan" || e.loanType.name=="GROUP LOAN") && e.status==status)
                .Unwind("members")
                .Project<loanMembers>(new BsonDocument
                    {
                        {"_id",0},
                        {"memberId","$members.memberId"}
                    })
                .ToList();
            
            IEnumerable<String> distinctMemberId = (from x in rep2 select x.memberId).Distinct().ToList();

            List<repCic> repfinal = new List<repCic>();
            foreach(String id in distinctMemberId)
            {
                foreach(repCic r in from x in rep where x.Id == id select x)
                {
                    repfinal.Add(r);
                }
            }
            return repfinal;
        }

        String status = "for-payment";

        public List<repMidas> getMidas(DateTime cutoffDate, string branchSlug)
        {
            // get list of loans that are status = 'for-payment' and loantype.name = 'group loan'
            var coll = _db.GetCollection<loans>("loans");
            var rep = coll.Aggregate()
                .Match(e => e.branchSlug == branchSlug)
                .Match(e => (e.loanType.name.ToLower() == "group loan") && e.status == status && e.firstPaymentDate < cutoffDate)
                .Unwind("members")
                .Lookup("clients", "members.slug", "slug", "lmaidenName")
                .Unwind("lmaidenName") 
                .Project<repMidas>(new BsonDocument
                    {
                        {"_id",0},
                        {"referenceId",1},
                        {"disbursementDate",1},
                        {"lastPaymentDate",1},
                        {"loanType.interestRate",1},
                        {"loanType.frequency",1},
                        {"loanType.term",1},
                        {"members",1}
                    ,
                        {"lmaidenName.maidenName",1},
                        {"lmaidenName.birthplace",1}
                    })
                .ToList();


            //LOANS
            //var query1 = Builders<loans>.Filter.Eq(e => e.loanType.name.ToLower(), "group loan");
            //var query2 = Builders<loans>.Filter.Eq(e => e.status, status);
            //var query3 = Builders<loans>.Filter.Lt(e => e.firstPaymentDate, cutoffDate);
            //var query = Builders<loans>.Filter.And(query1, query2, query3);
            //var loans = _db.GetCollection<loans>("loans").Find(query).ToListAsync();
            //var loanResult = loans.Result;

            //CLIENTS
            //var cl = _db.GetCollection<clients>("clients").Find(new BsonDocument()).ToListAsync();
            //var clients = cl.Result;

            //maidenName mN = new maidenName();
            //List<repMidas> rep = new List<repMidas>();

            //foreach (var item in loanResult)
            //{
                
            //    var cQuery = Builders<clients>.Filter.Eq(e => e.slug, item.members.slug);
            //    var cRes = _db.GetCollection<clients>("clients").Find(cQuery).FirstOrDefault();
               

            //    if (cRes == null)
            //    {
            //        mN.last = "";
            //        mN.first = "";
            //        mN.middle = "";
            //    }
            //    else
            //    {
            //        mN.last = cRes.maidenName.last;
            //        mN.first = cRes.maidenName.first;
            //        mN.middle = cRes.maidenName.middle;
            //    }

            //    rep.Add(new repMidas {
            //        Id = item.Id,
            //        referenceId = item.referenceId,
            //        disbursementDate = item.disbursementDate,
            //        loanType = item.loanType,
            //        members = item.members,
            //        maidenName = mN
            //    });
            //}


            // get all clients
            var coll2 = _db.GetCollection<clients>("clients");
            var rep2 = coll2.Find(Builders<clients>.Filter.In(x=>x.Id,rep.Select(x=>x.members.memberId).ToArray())).ToList();
            
            // get all payments
            var coll3 = _db.GetCollection<paymentLoans>("paymentLoans");
            var rep3 = coll3.Find(Builders<paymentLoans>.Filter.In(x=>x.loan.referenceId,rep.Select(x=>x.referenceId).Distinct().ToArray())).ToList();

            var now = cutoffDate;
            // loop loans
            rep.ForEach((x)=>{
                
                    // fix namespace 
                    x.members.name.first = fixNames(x.members.name.first);
                    x.members.name.middle = fixNames(x.members.name.middle);
                    x.members.name.last = fixNames(x.members.name.last);

                    if(x.lmaidenName.birthplace == null)
                    {
                    x.lmaidenName.birthplace = "-";
                    }
                    // check birthdate
                    int _now = int.Parse(DateTime.Now.ToString("yyyyMMdd"));
                    int dob = int.Parse(x.members.birthdate.ToString("yyyyMMdd"));
                    int age = (_now - dob) / 10000;

                    if(age>100) {
                        x.members.birthdate = x.members.birthdate.AddYears(age - 99);
                        int _dob = int.Parse(x.members.birthdate.ToString("yyyyMMdd"));
                        int _age = (_now - _dob) / 10000;
                    }
                    
                   //get amount due 
                    var prinAmtDue = 0.0;
                    var intAmtDue = 0.0;
                    var lastLoanTerm =0;
                    var monthlyAmort = 0.0;
                    foreach(schedule sked in x.members.schedule) {
                        if(sked.collectionDate<now) {
                            prinAmtDue += sked.currentPrincipalDue;
                            intAmtDue += sked.currentInterestDue;
                            monthlyAmort = sked.currentPrincipalDue + sked.currentInterestDue;
                            lastLoanTerm = sked.weekNo;
                        }
                    }
                
                    // get payments per loan
                    var payments = rep3.FindAll(payment=>payment.loan.referenceId == x.referenceId && payment.officialReceipt.paidAt < now).ToList();
                    
                    var payms = 0.0;
                    var lastPayment = 0.0;
                    var lastTerm = 0;
                    payments.ForEach((pmt) => {
                        foreach(var mem in from m in pmt.members where m.slug == x.members.slug select m) {
                             payms += mem.amounts.principal;
                             lastPayment = mem.amounts.principal;
                             if(mem.terms.Count() > 0)
                                lastTerm = mem.terms.Last().term;  
                        }
                    });
                    
                    // temporarily put principalBalance in totalAmount;
                    x.members.totalAmount = Math.Round(x.members.principalAmount - payms,2);
                    if(x.members.totalAmount<0)
                        x.members.totalAmount = 0;

                    // temporariy put good or bad in health
                    // bad = 0, with past due
                    // good = 1, without past due
                    if(Math.Round(prinAmtDue,2)<= Math.Round(payms,2)) {
                        x.members.health = 1;
                    } else {
                        x.members.health = 0;
                    }
                    
                    var overdudays = 0;
                    overdudays = (lastLoanTerm -1) - lastTerm;
                    var unpaidTerms = overdudays;
                    if (overdudays <= 0) {
                        overdudays = 0;
                        unpaidTerms = 0;
                    } else if(overdudays >=1 && overdudays <=4) {
                        overdudays = 1;
                    } else if (overdudays >=5 && overdudays <=8) {
                        overdudays = 2;
                    } else if (overdudays >=9 && overdudays <=12) {
                        overdudays = 3;
                    } else if (overdudays >=13 && overdudays <=24) {
                        overdudays = 4;
                    } else if (overdudays >=24 && overdudays <=48) {
                        overdudays = 5;
                    } else if (overdudays >=49) {
                        overdudays = 6;
                    }

                    // temporariy put overdudays|monthlypaymentamount|noofoutstandingpayment|amountoflastpayment to purpose.code
                    x.members.purpose = new purpose();
                    x.members.purpose.code = overdudays.ToString()+"|"+monthlyAmort.ToString()+"|"+unpaidTerms.ToString()+"|"+lastPayment.ToString()+"|";
                    
                    if(unpaidTerms == 0)
                        x.members.health = 3;


                    var c = (from z in rep2 where z.Id == x.members.memberId select z).FirstOrDefault();
                    if(c==null) {
                        c = (from z in rep2 where z.fullname == x.members.fullname select z).FirstOrDefault();
                    }

                    if(c!=null){
                        x.members.address = c.addresses[0].street + "|" + c.addresses[0].barangay.name + "|" + c.addresses[0].municipality.name + "|" + c.addresses[0].province.name;
                    
                    //temporariy put gender|sss|pagibig|philhealth to businessName field
                    var gender = c.gender;
                        var sssgsis = "";
                        var pagibig = "";
                        var philhealth = "";
                        c.ids.ForEach(i=>{
                            switch(i.type.ToLower()) {
                                case "sss card":
                                    sssgsis = idFormatter(i.number);
                                    break;
                                case "gsis":
                                    sssgsis = idFormatter(i.number);
                                    break;
                                case "philhealth card":
                                    philhealth = idFormatter(i.number);
                                    break;
                            }

                        });
                        x.members.issuedId.number = idFormatter(x.members.issuedId.number);
                        x.members.businessName = gender+"|"+idFormatter(sssgsis)+"|"+idFormatter(pagibig)+"|"+idFormatter(philhealth);
                        //temporariy put contacts to position field
                        x.members.position = "";
                        if(c.contacts.Count > 0) {
                            x.members.position = c.contacts[0].number;
                        }
                        //temporary put tin to beneficiary field
                        x.members.beneficiary = "";
                        if(c.ids!=null) {
                            if (c.ids.Count > 0)
                            {
                                id id = c.ids.Find(i=>i.type.ToLower()=="tin");
                                if(id!=null) {

                                    x.members.beneficiary = idFormatter(id.number);
                                } 
                            }
                        }
                        x.members.memberId = c.accountId;
                    } else {

                        x.members.address = " | | | ";
                        x.members.businessName = " | | | ";
                        x.members.position = "";
                        x.members.beneficiary = "";
                        x.members.memberId = "EXCLUDED";
                    }
            });

            return rep;
        }

        private string idFormatter(string id) {
            var formattedId = id;
            // remove alpha characters
            formattedId = string.Join("", formattedId.ToCharArray().Where(Char.IsDigit));
            // cut to 12 chars only
            if(formattedId.Length > 12)
                formattedId = formattedId.Substring(1,12);

            // pad 0 to suffice  9 chars if less than 9 chars
            formattedId = formattedId.PadLeft(12,'0');

            if(formattedId == "000000000000")
                return "";
            else
                return formattedId;
        }

        private string fixNames(string name) {
            var formmatedName = name;
            
            formmatedName = string.Join("", formmatedName.ToCharArray().Where(Char.IsLetter));

            return formmatedName;
        }

        public List<repOr> getOrList(DateTime dateFrom, DateTime dateTo)
        {
            var orList = new List<repOr>();

            var coll = _db.GetCollection<paymentLoans>("paymentLoans");
            var rep = coll.Aggregate()
                .Match(c => c.officialReceipt.paidAt >= dateFrom)
                .Match(c => c.officialReceipt.paidAt <= dateTo)
                .Project<repOr>(new BsonDocument
                    {
                        {"_id",0},
                        {"no","$officialReceipt.number"},
                        {"paidAt","$officialReceipt.paidAt"},
                        {"amount","$officialReceipt.amounts.total"}
                    })
                .ToList();
            
            orList.AddRange(rep);
            
            var coll2 = _db.GetCollection<paymentLoanFull>("paymentLoanFull");
            var rep2 = coll2.Aggregate()
                .Match(c => c.officialReceipt.paidAt >= dateFrom)
                .Match(c => c.officialReceipt.paidAt <= dateTo)
                .Project<repOr>(new BsonDocument
                    {
                        {"_id",0},
                        {"no","$officialReceipt.number"},
                        {"paidAt","$officialReceipt.paidAt"},
                        {"amount","$officialReceipt.amounts.total"}
                    })
                .ToList();

            orList.AddRange(rep2);
            var sortedList = orList.OrderBy(c=>c.paidAt).ThenBy(c=>c.no).ToList();
            return sortedList;
        }

        public List<repCashReceipt> getCashReceiptList(DateTime dateFrom, DateTime dateTo)
        {
            var cashReceiptList = new List<repCashReceipt>();

            var coll = _db.GetCollection<repCashReceipt>("cashReceipt");
            var rep = coll.Aggregate()
                .Match(c => c.timestamps.issuedAt >= dateFrom)
                .Match(c => c.timestamps.issuedAt <= dateTo)
                .ToList();
            
            cashReceiptList.AddRange(rep);

            var sortedList = cashReceiptList.OrderBy(c=>c.timestamps.issuedAt).ThenBy(c=>c.number).ToList();
            return sortedList;
        }

        public List<repDtrDetailed> getDtrRegion(string p, DateTime dateFrom, DateTime dateTo)
        {
            
            // var coll = _db.GetCollection<repDtr>("dtr");
            // var rep = coll.Aggregate()
            //     .Match(c => c.date >= dateFrom)
            //     .Match(c => c.date <= dateTo)
            //     .Lookup("users", "employeeCode", "employeeCode", "users")
            //     .Unwind("users")
            //     .Match(new BsonDocument { { "users.employment.region", p } })
            //     .Project<repDtr>(new BsonDocument
            //             {
            //                 {"_id",1},
            //                 {"date", 1},
            //                 {"name", "$users.fullname"},
            //                 {"position", "$users.employment.position"},
            //                 {"group", "$users.employment.group"},
            //                 {"department","$users.employment.department"},
            //                 {"division", "$users.employment.division"},
            //                 {"section","$users.employment.section"},
            //                 {"region","$users.employment.region"},
            //                 {"area","$users.employment.area"},
            //                 {"branch", "$users.employment.branch"},
            //                 {"unit", "$users.employment.unit"},
            //                 {"status","$users.employment.status"},
            //                 {"employeeCode",1},
            //                 {"slug",1},
            //                 {"schedule",1},
            //                 {"actual",1},
            //                 {"manual",1},
            //                 {"late",1},
            //                 {"undertime",1},
            //                 {"absent",1},
            //                 {"overtime",1},
            //                 {"hoursWork",1},
            //                 {"nd",1},
            //                 {"remarks",1}
            //             })
            //     .ToListAsync();

            // return rep.Result;

            return dtrDetailed("employment.region",p,dateFrom,dateTo);
        }

        public List<repDtrDetailed> getDtrArea(string p, DateTime dateFrom, DateTime dateTo)
        {

            // var coll = _db.GetCollection<repDtr>("dtr");
            // var rep = coll.Aggregate()
            //     .Match(c => c.date >= dateFrom)
            //     .Match(c => c.date <= dateTo)
            //     .Lookup("users", "employeeCode", "employeeCode", "users")
            //     .Unwind("users")
            //     .Match(new BsonDocument { { "users.employment.area", p } })
            //     .Project<repDtr>(new BsonDocument
            //             {
            //                 {"_id",1},
            //                 {"date", 1},
            //                 {"name", "$users.fullname"},
            //                 {"position", "$users.employment.position"},
            //                 {"group", "$users.employment.group"},
            //                 {"department","$users.employment.department"},
            //                 {"division", "$users.employment.division"},
            //                 {"section","$users.employment.section"},
            //                 {"region","$users.employment.region"},
            //                 {"area","$users.employment.area"},
            //                 {"branch", "$users.employment.branch"},
            //                 {"unit", "$users.employment.unit"},
            //                 {"status","$users.employment.status"},
            //                 {"employeeCode",1},
            //                 {"slug",1},
            //                 {"schedule",1},
            //                 {"actual",1},
            //                 {"manual",1},
            //                 {"late",1},
            //                 {"undertime",1},
            //                 {"absent",1},
            //                 {"overtime",1},
            //                 {"hoursWork",1},
            //                 {"nd",1},
            //                 {"remarks",1}
            //             })
            //     .ToListAsync();

            return dtrDetailed("employment.area",p,dateFrom,dateTo);
        }

        public repLedgerIndividualModel getLedgerIndividual(String refId, String memberId){
            repEcrModel list = calculateEcr2(refId);
            List<repEcrLine> line = list.ecrLines.FindAll(c=> c.memberId == memberId);
            list.ecrLines =new List<repEcrLine>{};
            list.ecrLines.AddRange(line);



            repLedgerIndividualModel model = new repLedgerIndividualModel();
            model.accountNo = "";
            model.clientName = list.ecrLines[0].nameOfClient;
            model.address = list.centerAddress;
            model.center = list.centerDescription;
            model.business = "";
            model.transactionNo = list.transactionNo;
            model.principalAmount = list.loanAmount;
            model.interestAmount = list.interestRate * list.loanAmount;
            model.repLedgerIndividualLine = new List<repLedgerIndividualLine>();
            
            list.ecrLines.ForEach((x) => {
                model.repLedgerIndividualLine.Add(
                    new repLedgerIndividualLine {
                        weekNo = x.weekNo,
                        payments =x.payments,
                        pastDuePrincipal = x.pastDuePrincipal,
                        pastDueInterest = x.pastDueInterest,
                        pastDueCBU = x.pastDueCBU,
                        paymentPrincipal = x.paymentPrincipal,
                        paymentInterest = x.paymentInterest,
                        paymentCBU = x.paymentCBU,
                        balancePrincipal = x.balancePrincipal,
                        balanceInterest = x.balanceInterest,
                        balanceCBU = x.balanceCBU,
                        totalPayment = x.paymentPrincipal + x.paymentInterest + x.paymentCBU
                    }
                );
            });
            

            return model;
        }

        public repRfpCvCBUWidthdrawal getRfpCvCBUWidthdrawal(String cbuWithdrawalId)
        {
            var coll = _db.GetCollection<cbuWithdrawal>("cbuWithdrawal");
            var rep = coll.Aggregate()
                    .Match(e => e.Id == cbuWithdrawalId)
                    .Project<repRfpCvCBUWidthdrawal>(new BsonDocument
                            {
                            {"members", 1},
                            {"rfp", 1},
                            {"cv", 1},
                            {"branch", 1},
                            {"center", 1}
                            })
                    .FirstOrDefault();

            //var coll2 = _db.GetCollection<clients>("clients");
            // var rep2 = coll2.Find(Builders<clients>.Filter.Eq(x => x.Id, rep.rfp.payee)).FirstOrDefault();
            // rep.rfp.payee = rep2.fullname;
            // rep.cv.payee = rep2.fullname;

            var coll2 = _db.GetCollection<centerInstance>("centerInstance");
            var rep2 = coll2.Find(Builders<centerInstance>.Filter.Eq(x => x.Id, rep.center.id)).FirstOrDefault();
            rep.center.address = rep2.address;
            return rep;
        }

        public List<repCashDisbursement> getCashDisbursementList(DateTime dateFrom, DateTime dateTo)
        {
            var cashDisbursmentList = new List<repCashDisbursement>();

            var coll = _db.GetCollection<repCashDisbursement>("acctngCv");
            var rep = coll.Aggregate()
                .Match(c => c.timestamps.issuedAt >= dateFrom)
                .Match(c => c.timestamps.issuedAt <= dateTo)
                .ToList();
            
            cashDisbursmentList.AddRange(rep);

            var sortedList = cashDisbursmentList.OrderBy(c=>c.timestamps.issuedAt).ThenBy(c=>c.chequeNumber).ToList();
            return sortedList;
        }

        public repCheckVoucher checkVoucher(String cvNumber)
        {

            var coll = _db.GetCollection<acctngCv>("acctngCv");
            var rep = coll.Aggregate()
                .Match(e =>e.cvNumber == cvNumber)
                .Project<repCheckVoucher>(new BsonDocument{
                    {"chequeNumber",1},
                    {"cvNumber",1},
                    {"particulars",1},
                    {"accounts",1},
                    {"timestamps",1},
                    {"supplier",1},
                    {"amount",1}
                }).FirstOrDefault();
            return rep;
        }


        public List<repDtrDetailed> dtrDetailedPerEmployee(String employeeCode, DateTime from, DateTime to) {
            return dtrDetailed("employeeCode",employeeCode,from,to);
        }

        public List<repDtrDetailed> dtrDetailedPerBranch(String branchSlug, DateTime from, DateTime to) {
            return dtrDetailed("employment.branch",branchSlug,from,to);
        }

        public List<repDtrDetailed> dtrDetailedPerDepartment(String departmentName, DateTime from, DateTime to) {
            
            if(departmentName.ToLower()=="all") {
                var coll = _db.GetCollection<coreMaster.Models.branch>("branch");
                var item = coll.Find(new BsonDocument {
                    {"name","HEAD OFFICE"}
                }).FirstOrDefault();

                if(item!=null) 
                    return dtrDetailedPerBranch(item.slug,from,to);
                else
                    return null;
            }
                
            else
                return dtrDetailed("employment.department",departmentName,from,to);

        }
        
        private List<repDtrDetailed> dtrDetailed(String field, String value, DateTime from, DateTime to) {
            var coll = _db.GetCollection<users>("users");
            var rep = coll.Aggregate()
                .Match(new BsonDocument{
                    {field,value}
                })
                .Lookup("dtr","employeeCode","employeeCode","dtr")
                .Lookup("leaveLogs","employeeCode","employee.code","leaveLogs")
                .Lookup("branch","employment.branch","slug","branch")
                .Project<repDtrDetailed>(
                    new BsonDocument{
                        {"_id",0},
                        {"slug",1},
                        {"employeeCode",1},
                        {"fullname",1},
                        {"branch",1},
                        {"employment.department",1},
                        {"employment.position",1},
                        {"employment.branch",1},
                        {"employment.status",1},
                        {"employment.jobGrade",1},
                        {"employeeSchedules.timeInAm",1},
                        {"employeeSchedules.timeOutAm",1},
                        {"employeeSchedules.timeInPm",1},
                        {"employeeSchedules.timeOutPm",1},
                        {
                            "dtr",new BsonDocument{
                                {"$filter",new BsonDocument{
                                    {"input","$dtr"},
                                    {"as","dtri"},
                                    {"cond", new BsonDocument {
                                        {"$and", new BsonArray {
                                            new BsonDocument{
                                                {"$gte",new BsonArray{
                                                    {"$$dtri.date"},
                                                    {from}
                                                }}
                                            },
                                            new BsonDocument{
                                                {"$lte",new BsonArray{
                                                    {"$$dtri.date"},
                                                    {to}
                                                }}
                                            }
                                        }
                                        }
                                        }
                                    }
                                }}
                            }
                        }
                    }
                );
            return rep.ToList();
        }

        public List<repDtrDetailed> timekeepingAndAttendanceSummary(DateTime from, DateTime to) {
            to = to.AddDays(1).AddSeconds(-1);
            var coll = _db.GetCollection<users>("users");
            var rep = coll.Aggregate()
                .Match(a=>a.employment.status!="RESIGNED")
                .Lookup("dtr","employeeCode","employeeCode","dtr")
                .Lookup("leaveLogs","employeeCode","employee.code","leaveLogs")
                .Lookup("branch","employment.branch","slug","branch")
                .Project<repDtrDetailed>(
                    new BsonDocument{
                        {"_id",0},
                        {"employeeCode",1},
                        {"fullname",1},
                        {"branch",1},
                        {"employment.department",1},
                        {"employment.position",1},
                        {"employment.branch",1},
                        {"employment.status",1},
                        {"employment.jobGrade",1},
                        {"employeeSchedules.timeInAm",1},
                        {"employeeSchedules.timeOutAm",1},
                        {"employeeSchedules.timeInPm",1},
                        {"employeeSchedules.timeOutPm",1},
                        {
                            "dtr",new BsonDocument{
                                {"$filter",new BsonDocument{
                                    {"input","$dtr"},
                                    {"as","dtri"},
                                    {"cond", new BsonDocument {
                                        {"$and", new BsonArray {
                                            new BsonDocument{
                                                {"$gte",new BsonArray{
                                                    {"$$dtri.date"},
                                                    {from}
                                                }}
                                            },
                                            new BsonDocument{
                                                {"$lte",new BsonArray{
                                                    {"$$dtri.date"},
                                                    {to}
                                                }}
                                            }
                                        }
                                        }
                                        }
                                    }
                                }}
                            }
                        },
                        {"leaveLogs",1}
                    }
                );
            return rep.ToList();
        }

        public List<repDtrDetailed> timekeepingAndAttendanceSummary(DateTime from, DateTime to, String branchSlug) {
            to = to.AddDays(1).AddSeconds(-1);
            var coll = _db.GetCollection<users>("users");
            var rep = coll.Aggregate()
                .Match(a=>a.employment.status != "RESIGNED")
                .Match(a=>a.employment.branch == branchSlug)
                .Lookup("dtr","employeeCode","employeeCode","dtr")
                .Lookup("leaveLogs","employeeCode","employee.code","leaveLogs")
                .Lookup("branch","employment.branch","slug","branch")
                .Project<repDtrDetailed>(
                    new BsonDocument{
                        {"_id",0},
                        {"employeeCode",1},
                        {"fullname",1},
                        {"branch",1},
                        {"employment.department",1},
                        {"employment.position",1},
                        {"employment.branch",1},
                        {"employment.status",1},
                        {"employment.jobGrade",1},
                        {"employeeSchedules.timeInAm",1},
                        {"employeeSchedules.timeOutAm",1},
                        {"employeeSchedules.timeInPm",1},
                        {"employeeSchedules.timeOutPm",1},
                        {
                            "dtr",new BsonDocument{
                                {"$filter",new BsonDocument{
                                    {"input","$dtr"},
                                    {"as","dtri"},
                                    {"cond", new BsonDocument {
                                        {"$and", new BsonArray {
                                            new BsonDocument{
                                                {"$gte",new BsonArray{
                                                    {"$$dtri.date"},
                                                    {from}
                                                }}
                                            },
                                            new BsonDocument{
                                                {"$lte",new BsonArray{
                                                    {"$$dtri.date"},
                                                    {to}
                                                }}
                                            }
                                        }
                                        }
                                        }
                                    }
                                }}
                            }
                        },
                        {"leaveLogs",1}
                    }
                );
            return rep.ToList();
        }

        
        public repLedgerPerWeekModel getLedgerPerWeek(String refId){
            repEcrModel list = calculateEcr2(refId);
            

            repLedgerPerWeekModel model = new repLedgerPerWeekModel();
            model.transactionNo= list.transactionNo;
            model.centerName = list.centerDescription;
            model.loanAmountPrincipal = list.loanAmount;
            model.loanAmountInterest = list.interestRate * list.loanAmount;
            model.loanBalancePrincipal = (list.loanAmount) - list.ecrLines.Sum(x=>x.paymentPrincipal) ;
            model.loanBalanceInterest = (list.interestRate * list.loanAmount) - list.ecrLines.Sum(x=>x.paymentInterest);
            
            model.repLedgerPerWeekLine = new List<repLedgerPerWeekLine>();

            
        
            for(var ctr=1;ctr<=list.term;ctr++) {

                DateTime? _datePaid = null;
                var _referenceNumber = "";
                DateTime? _weeklyRepaymentDate = null;

                var _paymentPrincipal = 0.0;
                var _paymentInterest = 0.0;
                var _balancePrincipal = 0.0;
                var _balanceInterest = 0.0;
                var _cbuPayment = 0.0;
                var _cbuBalance = 0.0;

                list.ecrLines.Where(ecrLine=> ecrLine.weekNo == ctr).ToList().ForEach((x) => {
                    _paymentPrincipal += x.paymentPrincipal;
                    _paymentInterest += x.paymentInterest;
                    _balancePrincipal += x.balancePrincipal - x.paymentPrincipal;
                    _balanceInterest += x.balanceInterest - x.paymentInterest;
                    _cbuPayment += x.paymentCBU;
                    _cbuBalance = x.balanceCBU + x.paymentCBU;
                });
                
                
                if(list.ecrLines!=null) {
                    if(list.ecrLines.Count > 0) {

                        //get first line
                        repEcrLine e = list.ecrLines.Where(ecrLine=> ecrLine.weekNo == ctr).FirstOrDefault();
                        if(e!=null) {
                            if(e.payments!=null) {
                                if(e.payments.Count() > 0) {
                                    _datePaid =  e.payments.First().orDate;
                                    _referenceNumber = e.payments.First().orNo;
                                    
                                }
                            }

                            _weeklyRepaymentDate = e.weeklyRepaymentDate;
                        }
                        
                        
                        
                    }
                }

                
                model.repLedgerPerWeekLine.Add(
                    new repLedgerPerWeekLine {
                        weekNo = ctr,
                        repaymentDate = (DateTime)_weeklyRepaymentDate,
                        datePaid = _datePaid,
                        referenceNumber = _referenceNumber,
                        paymentPrincipal = _paymentPrincipal,
                        paymentInterest = _paymentInterest,
                        balancePrincipal = _balancePrincipal,
                        balanceInterest = _balanceInterest,
                        cbuPayment = _cbuPayment,
                        cbuBalance = _cbuBalance,
                    }
                );

            }
            
            model.cbuBalance = model.repLedgerPerWeekLine.OrderByDescending(x=> x.weekNo).First().cbuBalance;
            return model;
        }

        public repLedgerPerClientPerWeekModel getLedgerPerClientPerWeek(String refId, Double weekNo) {
            repEcrModel list = calculateEcr2(refId);
            
            if(list!=null) {
                repLedgerPerClientPerWeekModel model = new repLedgerPerClientPerWeekModel();
                model.transactionNo= list.transactionNo;
                model.centerName = list.centerDescription;
                model.loanAmountPrincipal = list.loanAmount;
                model.loanAmountInterest = list.interestRate * list.loanAmount;
                model.loanBalancePrincipal = (list.loanAmount) - list.ecrLines.Sum(x=>x.paymentPrincipal) ;
                model.loanBalanceInterest = (list.interestRate * list.loanAmount) - list.ecrLines.Sum(x=>x.paymentInterest);
                
                model.repLedgerPerClientPerWeekLine = new List<repLedgerPerClientPerWeekLine>();    

                list.ecrLines.Where(y=>y.weekNo == weekNo).ToList().ForEach((x) => {
                    model.repLedgerPerClientPerWeekLine.Add(new repLedgerPerClientPerWeekLine {
                        clientName = x.nameOfClient,
                        weekNo = x.weekNo,
                        loanCycle = list.cycleNo,
                        paymentPrincipal = x.paymentPrincipal,
                        paymentInterest = x.paymentInterest,
                        balancePrincipal = x.balancePrincipal - x.paymentPrincipal,
                        balanceInterest = x.balancePrincipal - x.paymentInterest,
                        cbuPayment = x.paymentCBU,
                        cbuBalance = x.balanceCBU - x.paymentCBU,
                    });
                });

                model.cbuBalance = model.repLedgerPerClientPerWeekLine.OrderByDescending(x=> x.weekNo).First().cbuBalance;

                return model;
            } else {
                return null;
            }

        }

        public List<repGeneralJournal> getGeneralJournalList(DateTime dateFrom, DateTime dateTo)
        {
            var generalJournalList = new List<repGeneralJournal>();

            var coll = _db.GetCollection<repGeneralJournal>("acctngJv");
            var rep = coll.Aggregate()
                .Match(c => c.timestamps.issuedAt >= dateFrom)
                .Match(c => c.timestamps.issuedAt <= dateTo)
                .ToList();
            
            generalJournalList.AddRange(rep);

            var sortedList = generalJournalList.OrderBy(c=>c.timestamps.issuedAt).ThenBy(c=>c.number).ToList();
            return sortedList;
        }

        public List<repJournalVoucher> journalVoucher(DateTime date) {
            var filter = new BsonDocument {
                {"timestamps.issuedAt",
                    new BsonDocument {
                        {"$gte",date.Date}
                    }
                }
            };

            var filter2 = new BsonDocument {
                {"timestamps.issuedAt",
                    new BsonDocument {
                        {"$lte",date.Date.AddDays(1).AddSeconds(-1)}
                    }
                }
            };

            var filter3 = new BsonDocument {
                {"$and",new BsonArray {
                    filter,filter2
                }
            }};

            var proj = new BsonDocument {
                {"_id",0},
                {"jvNumber",1},
                {"particulars",1},
                {"accounts",1}
            };

            var coll = _db.GetCollection<acctngJv>("acctngJv");
            var items = coll.Find(filter3).Project<repJournalVoucher>(proj).ToList(); 

            return items;
        }

        public repGeneralLedger getGeneralLedger(Int32 year) {

            return new repGeneralLedger();
        }

        public repPayrollModel getPayroll(DateTime from, DateTime to, Int32 jobGrade, String branchSlug) {
            
            var payrollSum = new repPayrollModel();
            
            payrollSum.periodFrom = from;
            payrollSum.periodTo = to;

            if(from.Day == 1) {
                payrollSum.cutOffFrom = from.AddMonths(-1).AddDays(21);
                payrollSum.cutOffTo = from.AddDays(5);
            } else if(from.Day == 16) {
                payrollSum.cutOffFrom = from.AddDays(-9);
                payrollSum.cutOffTo = from.AddDays(6);
            }
            
            var list = new List<repPayrollModelDetail>();

            
            var filterDefs = new List<FilterDefinition<users>>();
            if(branchSlug!="")
                filterDefs.Add(Builders<users>.Filter.Eq(x=>x.employment.branch, branchSlug));
            
            switch (jobGrade) {
                case 0:
                    filterDefs.Add(Builders<users>.Filter.Or
                    (
                        Builders<users>.Filter.Eq("employment.jobGrade", "1"),
                        Builders<users>.Filter.Eq("employment.jobGrade", "2")
                    ));

                    payrollSum.jobGradeDesc = "RANK & FILE";
                    break;
                case 1:
                    filterDefs.Add(Builders<users>.Filter.Or
                    (
                        Builders<users>.Filter.Eq("employment.jobGrade", "3"),
                        Builders<users>.Filter.Eq("employment.jobGrade", "4")
                    ));
                    payrollSum.jobGradeDesc = "OFFICERS";
                    break;
                case 2:
                    filterDefs.Add(Builders<users>.Filter.Or
                    (
                        Builders<users>.Filter.Eq("employment.jobGrade", "5"),
                        Builders<users>.Filter.Eq("employment.jobGrade", "6"),
                        Builders<users>.Filter.Eq("employment.jobGrade", "7")
                    ));
                    payrollSum.jobGradeDesc = "EXECUTIVES AND MANAGERS";
                    break;
            }

            var usersList = _db.GetCollection<users>("users").Find(Builders<users>.Filter.And(filterDefs)).ToList();
            var payrollList = _payrollRep.getPayrollByBranchSlug(branchSlug,from, to);
            
            usersList.ToList().ForEach(u=>{
                var item = new repPayrollModelDetail();
                item.branch = u.employment.branch;
                
                item.department = getDepartmentAcro(u.employment.department);
                item.employeeCode = u.employeeCode;
                item.jobGrade = u.employment.jobGrade;
                item.fullname = u.name.last + " " + u.name.first + " " + u.name.middle + " " + u.extension;
                item.taxCode = u.taxCode;

                var pr = payrollList.ToList().Find(p=>p.employee.slug == u.slug);
                
                if(pr!=null) {
                    item.monthlyRate = (decimal)pr.basicPay;
                    item.ratePerDay = Math.Round((decimal)(pr.basicPay) / 22,2);
                    item.basicPay = (decimal)pr.basicPay / 2;
                    if(pr.entities !=null) {
                        pr.entities.ForEach(e=>{
                            decimal amount = (decimal)e.amount;
                            switch (e.name.ToLower()) {
                                case "salary adjustment":
                                    item.adjustment = amount; //ADJUSTMENT
                                    break;
                                case "holiday pay":
                                    item.holidayPay = amount; //HOLIDAY PAY
                                    break;
                                case "overtime pay":
                                    item.overtimePay = amount; //OVERTIME PAY
                                    break;
                                case "absences":
                                    item.absentLate = amount; //ABSENT LATE
                                    break;
                                case "rice subsidy":
                                    item.riceSubsidy = amount; //RICE SUBSIDY
                                    break;
                                case "accounts payable":
                                    item.ap = amount; //AR
                                    break;
                                case "withholding tax":
                                    item.witholdingTax = amount; //WITHOLDING TAX
                                    break;
                                case "hdmf premiumcon":
                                    item.hdmfPremiumContribution = amount; //HDMF PREMIUM CONTRIBUTION
                                    break;
                                 case "hdmf calamity":
                                    item.hdmfCalamity = amount; //HDMF CALAMITY
                                    break;
                                case "hdmf housing":
                                    item.hdmfHousing = amount; //HDMF HOUSING
                                    break;
                                case "hdmf mpl":
                                    item.hdmfMpl = amount; //HDMF MPL
                                    break;
                                case "hdmf mp2":
                                    item.hdmfMp2 = amount; //HDMF MP2
                                    break;
                                case "sss premiumcon":
                                    item.sssPremiumContribution = amount; //SSS PREMIUM CONTRIBUTION
                                    break;
                                case "sss salary loan":
                                    item.sssSalaryLoan = amount; //SSS SALARY LOAN
                                    break;
                                case "phic premiumcon":
                                    item.phicPremiumContribution = amount; //PHILHEALTH CONTRIBUTION
                                    break;
                                case "fidelity bond":
                                    item.fidelity = amount; //FIDELITY BOND
                                    break;
                                case "fhg loan payable":
                                    item.fhgLoanPayable = amount; //FHG LOAN PAYABLE
                                    break;
                                case "fhg share capital":
                                    item.fhgShareCapital = amount; //FHG SHARE CAPITAL
                                    break;
                                case "fhg savings":
                                    item.fhgSavings = amount; //FHG SAVINGS
                                    break;
                                case "fhg insurance":
                                    item.fhgInsurance = amount; //FHG INSURANCE
                                    break;
                                case "fhg others":
                                    item.fhgOthers = amount; //FHG OTHERS
                                    break;
                                case "accounts receivable":
                                    item.ar = amount; //AR
                                    break;
                                case "ar: others":
                                    item.arOthers = amount; //AR OTHERS
                                    break;
                                case "ar: misc":
                                    item.arMisc = amount; //AR MISC
                                    break;
                                case "ar: personal calls":
                                    item.arPersonalCalls = amount; //AR PERSONAL CALLS
                                    break;
                            } 
                            item.statutory = item.fidelity + item.sssPremiumContribution + item.phicPremiumContribution + item.hdmfPremiumContribution;
                        });
                    }

                    item.grossTaxableIncome = 
                        (
                            item.basicPay + 
                            item.adjustment + 
                            item.holidayPay + 
                            item.overtimePay
                        ) 
                            - item.absentLate;
                            
                    item.grossPay = 
                        (
                            item.grossTaxableIncome + 
                            item.riceSubsidy + 
                            item.ap
                        );

                    item.totalDeduction = 
                        (
                            item.witholdingTax + 
                            item.hdmfPremiumContribution +
                            item.hdmfCalamity + 
                            item.hdmfHousing + 
                            item.hdmfMpl + 
                            item.hdmfMp2 + 
                            item.sssPremiumContribution + 
                            item.sssSalaryLoan + 
                            item.phicPremiumContribution + 
                            item.fidelity + 
                            item.fhgLoanPayable + 
                            item.fhgShareCapital + 
                            item.fhgSavings + 
                            item.fhgInsurance + 
                            item.fhgOthers + 
                            item.ar + 
                            item.arOthers + 
                            item.arMisc +
                            item.arPersonalCalls
                        );

                    item.netPay = item.grossPay - item.totalDeduction;

                }

                list.Add(item);
            });

            payrollSum.details = list;
            
            var totals = new repPayrollModelDetail();

            totals.monthlyRate = list.Sum(x=>x.monthlyRate);
            totals.ratePerDay = list.Sum(x=>x.ratePerDay);
            totals.basicPay = list.Sum(x=>x.basicPay);

            totals.adjustment = list.Sum(x=>x.adjustment);
            totals.holidayPay = list.Sum(x=>x.holidayPay);
            totals.overtimePay = list.Sum(x=>x.overtimePay);
            totals.absentLate = list.Sum(x=>x.absentLate);
            totals.riceSubsidy = list.Sum(x=>x.riceSubsidy);
            totals.ap = list.Sum(x=>x.ap);
            totals.witholdingTax = list.Sum(x=>x.witholdingTax);
            totals.hdmfPremiumContribution = list.Sum(x=>x.hdmfPremiumContribution);
            totals.hdmfCalamity = list.Sum(x=>x.hdmfCalamity);
            totals.hdmfHousing = list.Sum(x=>x.hdmfHousing);
            totals.hdmfMpl = list.Sum(x=>x.hdmfMpl);
            totals.hdmfMp2 = list.Sum(x=>x.hdmfMp2);
            totals.sssPremiumContribution = list.Sum(x=>x.sssPremiumContribution);
            totals.sssSalaryLoan = list.Sum(x=>x.sssSalaryLoan);
            totals.phicPremiumContribution = list.Sum(x=>x.phicPremiumContribution);
            totals.fidelity = list.Sum(x=>x.fidelity);
            totals.fhgLoanPayable = list.Sum(x=>x.fhgLoanPayable);
            totals.fhgShareCapital = list.Sum(x=>x.fhgShareCapital);
            totals.fhgSavings = list.Sum(x=>x.fhgSavings);
            totals.fhgInsurance = list.Sum(x=>x.fhgInsurance);
            totals.fhgOthers = list.Sum(x=>x.fhgOthers);
            totals.ar = list.Sum(x=>x.ar);
            totals.arOthers = list.Sum(x=>x.arOthers);
            totals.arMisc = list.Sum(x=>x.arMisc);
            totals.statutory = list.Sum(x=>x.statutory);

            totals.grossTaxableIncome = list.Sum(x=>x.grossTaxableIncome);
            totals.grossPay = list.Sum(x=>x.grossPay);
            totals.totalDeduction = list.Sum(x=>x.totalDeduction);
            totals.netPay = list.Sum(x=>x.netPay);


            payrollSum.totals = totals;

            return payrollSum;
        }
        
		public String getDepartmentAcro(String department) {
            var acro = department;
            _depAcroSett.Value.list.ToList().ForEach(d=>{
                if(d.department == department) {
                    acro = d.acronym;
                }
            });
            return acro;
        }
		public alphalist alphalist(Int32 year, String branchSlug, Int32 jobGrade)
        {
            coreMaster.Models.branch branch = _db.GetCollection<coreMaster.Models.branch>("branch").Find(new BsonDocument{{"slug",branchSlug}}).FirstOrDefault();
            String branchName = "";
            if(branch!=null)
                branchName = branch.name;
            // 0 is rnk, 1 & 2
            // 1 is officers, 3 & 4
            // 2 is executive and managers, 5 & 7
            BsonDocument query = new BsonDocument();
            String position = "";
            switch (jobGrade) {
                case 0:
                    query = new BsonDocument {
                        {"$or",new BsonArray {
                            new BsonDocument {{"employment.jobGrade","1"}},
                            new BsonDocument {{"employment.jobGrade","2"}}
                        }},
                        {"employment.branch",branchSlug}
                    };
                    position = "Rank & File";
                    break;
                case 1:
                    query = new BsonDocument {
                        {"$or",new BsonArray {
                            new BsonDocument {{"employment.jobGrade","3"}},
                            new BsonDocument {{"employment.jobGrade","4"}}
                        }},
                        {"employment.branch",branchSlug}
                    };
                    position = "Officers";
                    break;
                case 2:
                    query = new BsonDocument {
                        {"$or",new BsonArray {
                            new BsonDocument {{"employment.jobGrade","5"}},
                            new BsonDocument {{"employment.jobGrade","6"}},
                            new BsonDocument {{"employment.jobGrade","7"}}
                        }},
                        {"employment.branch",branchSlug}
                    };
                    position = "Executive & Managers";
                    break;
            }

            List<users> employees = _db.GetCollection<users>("users").Find(query).ToList();
            
            var from  = Convert.ToDateTime("1/1/" + DateTime.Now.Year.ToString());
            var to  = from.AddYears(1).AddDays(-1);

            
            List<payroll> payrolls = _db.GetCollection<payroll>("payroll").Find(Builders<payroll>.Filter.Empty).ToList();



          



            alphalist _alphalist = new alphalist();

            _alphalist.year = year;
            _alphalist.position = branchName + " - " + position.ToUpper();
            _alphalist.employees = new List<alphalistEmployee>();


            

            employees.ToList().ForEach(e=>{

                List<alphalistEntry> entries = new List<alphalistEntry>();


                List<alphalistEntryDetail> grossPaydetails = new List<alphalistEntryDetail>();
                List<alphalistEntryDetail> taxWithheldDetails = new List<alphalistEntryDetail>();
                List<alphalistEntryDetail> sssDetails = new List<alphalistEntryDetail>();
                List<alphalistEntryDetail> phicDetails = new List<alphalistEntryDetail>();
                List<alphalistEntryDetail> hdmfDetails = new List<alphalistEntryDetail>();
                List<alphalistEntryDetail> mealsDetails = new List<alphalistEntryDetail>();
                List<alphalistEntryDetail> riceDetails = new List<alphalistEntryDetail>();
                List<alphalistEntryDetail> hiDetails = new List<alphalistEntryDetail>();
                
                
                for(DateTime ctr = from; ctr<= to; ctr = ctr.AddMonths(1)) {
                    var _from = ctr;
                    var _to = ctr.AddMonths(1).AddSeconds(-1);
                    // gross
                    var grossPay = 0.0;
                    payrolls.Where(p=>p.employee.id == e.employeeCode).ToList().ForEach(p=>{
                        if(p.cutoffPeriod.from >= _from && p.cutoffPeriod.to <= _to) {
                            var overtimePay = p.entities.Find(ent=>ent.name.ToLower() == "overtime pay");
                            var holidayPay = p.entities.Find(ent=>ent.name.ToLower() == "holiday pay");
                            var salaryAdjustment = p.entities.Find(ent=>ent.name.ToLower() == "salary adjustment");
                            var absences = p.entities.Find(ent=>ent.name.ToLower() == "absences");
                            var accountsPayable = p.entities.Find(ent=>ent.name.ToLower() == "accounts payable");
                            var riceSubsidy = p.entities.Find(ent=>ent.name.ToLower() == "rice subsidy");
                            
                            grossPay += p.basicPay / 2;
                            
                            if(overtimePay!=null)
                                grossPay+= overtimePay.amount;

                            if(holidayPay!=null)
                                grossPay+= holidayPay.amount;

                            if(salaryAdjustment!=null)
                                grossPay+= salaryAdjustment.amount;
                            
                            if(absences!=null)
                                grossPay-= absences.amount;

                            if(accountsPayable!=null)
                                grossPay+= accountsPayable.amount;

                            if(riceSubsidy!=null)
                                grossPay+= riceSubsidy.amount;
                        }
                    });

                    grossPaydetails.Add(new alphalistEntryDetail {
                        month = ctr.Month,
                        amount = grossPay,
                    });

                    // tax
                    var taxWithheld = 0.0;

                    payrolls.Where(p=>p.employee.id == e.employeeCode).ToList().ForEach(p=>{
                        if(p.cutoffPeriod.from >= _from && p.cutoffPeriod.to <= _to) {
                            var withholdingTax = p.entities.Find(ent=>ent.name.ToLower() == "withholding tax");

                            if(withholdingTax!=null)
                                taxWithheld+= withholdingTax.amount;
                        }
                    });

                    taxWithheldDetails.Add(new alphalistEntryDetail {
                        month = ctr.Month,
                        amount = taxWithheld,
                    });

                    // sss
                    var sss = 0.0;

                    payrolls.Where(p=>p.employee.id == e.employeeCode).ToList().ForEach(p=>{
                        if(p.cutoffPeriod.from >= _from && p.cutoffPeriod.to <= _to) {
                            var sssPremiumCon = p.entities.Find(ent=>ent.name.ToLower() == "sss premiumcon");

                            if(sssPremiumCon!=null)
                                sss+= sssPremiumCon.amount;
                        }
                    });

                    sssDetails.Add(new alphalistEntryDetail {
                        month = ctr.Month,
                        amount = sss,
                    });

                    // phic
                    var phic = 0.0;

                    payrolls.Where(p=>p.employee.id == e.employeeCode).ToList().ForEach(p=>{
                        if(p.cutoffPeriod.from >= _from && p.cutoffPeriod.to <= _to) {
                            var phicPremiumCon = p.entities.Find(ent=>ent.name.ToLower() == "phic premiumcon");

                            if(phicPremiumCon!=null)
                                phic+= phicPremiumCon.amount;
                        }
                    });

                    phicDetails.Add(new alphalistEntryDetail {
                        month = ctr.Month,
                        amount = phic,
                    });

                    // pagibig
                    var hdmf = 0.0;

                    payrolls.Where(p=>p.employee.id == e.employeeCode).ToList().ForEach(p=>{
                        if(p.cutoffPeriod.from >= _from && p.cutoffPeriod.to <= _to) {
                            var hdmfPremiumCon = p.entities.Find(ent=>ent.name.ToLower() == "hdmf premiumcon");

                            if(hdmfPremiumCon!=null)
                                hdmf+= hdmfPremiumCon.amount;
                        }
                    });

                    hdmfDetails.Add(new alphalistEntryDetail {
                        month = ctr.Month,
                        amount = hdmf,
                    });

                    // meals
                    var meals = 0.0;

                    payrolls.Where(p=>p.employee.id == e.employeeCode).ToList().ForEach(p=>{
                        if(p.cutoffPeriod.from >= _from && p.cutoffPeriod.to <= _to) {
                            var _meals = p.entities.Find(ent=>ent.name.ToLower() == "meals");

                            if(_meals!=null)
                                meals+= _meals.amount;
                        }
                    });

                    mealsDetails.Add(new alphalistEntryDetail {
                        month = ctr.Month,
                        amount = meals,
                    });

                    // rice
                    var rice = 0.0;

                    payrolls.Where(p=>p.employee.id == e.employeeCode).ToList().ForEach(p=>{
                        if(p.cutoffPeriod.from >= _from && p.cutoffPeriod.to <= _to) {
                            var riceSubsidy = p.entities.Find(ent=>ent.name.ToLower() == "rice subsidy");

                            if(riceSubsidy!=null)
                                rice+= riceSubsidy.amount;
                        }
                    });

                    riceDetails.Add(new alphalistEntryDetail {
                        month = ctr.Month,
                        amount = rice,
                    });

                    // u=hi
                    var hi = 0.0;

                    payrolls.Where(p=>p.employee.id == e.employeeCode).ToList().ForEach(p=>{
                        if(p.cutoffPeriod.from >= _from && p.cutoffPeriod.to <= _to) {
                            var healthInsurance = p.entities.Find(ent=>ent.name.ToLower() == "health insurance");

                            if(healthInsurance!=null)
                                hi+= healthInsurance.amount;

                            hi = 2886.24;
                        }
                    });

                    hiDetails.Add(new alphalistEntryDetail {
                        month = ctr.Month,
                        amount = hi,
                    });
                }

                _alphalist.employees.Add(new alphalistEmployee {
                    tin = e.tin,
                    employeeCode = e.employeeCode,
                    last = e.name.last,
                    first = e.name.first,
                    middle = e.name.middle,
                    status = e.taxCode,
                    jobGrade = e.employment.jobGrade,
                    entries = new List<alphalistEntry> {
                        new alphalistEntry {
                            description = "Gross Pay",
                            amount = grossPaydetails.Sum(d=>d.amount),
                            details = grossPaydetails
                        },
                        new alphalistEntry {
                            description = "Tax Withheld",
                            amount = taxWithheldDetails.Sum(d=>d.amount),
                            details = taxWithheldDetails
                        },
                        new alphalistEntry {
                            description = "SSS Premium",
                            amount = sssDetails.Sum(d=>d.amount),
                            details = sssDetails
                        },
                        new alphalistEntry {
                            description = "Philhealth Premium",
                            amount = phicDetails.Sum(d=>d.amount),
                            details = phicDetails
                        },
                        new alphalistEntry {
                            description = "Pagibig Premium",
                            amount = hdmfDetails.Sum(d=>d.amount),
                            details = hdmfDetails
                        },
                        new alphalistEntry {
                            description = "Meals",
                            amount = hdmfDetails.Sum(d=>d.amount),
                            details = mealsDetails
                        },
                        new alphalistEntry {
                            description = "Rice",
                            amount = hdmfDetails.Sum(d=>d.amount),
                            details = riceDetails
                        },
                        new alphalistEntry {
                            description = "Hi",
                            amount = hdmfDetails.Sum(d=>d.amount),
                            details = hiDetails
                        }
                    }
                });

              
            });


            return _alphalist;
        }

        public repAlphalistReportModel alphalist2(Int32 year, String branchSlug, Int32 jobGrade)
        {
            coreMaster.Models.branch branch = _db.GetCollection<coreMaster.Models.branch>("branch").Find(new BsonDocument{{"slug",branchSlug}}).FirstOrDefault();
            String branchName = "";
            if(branch!=null)
                branchName = branch.name;
            // 0 is rnk, 1 & 2
            // 1 is officers, 3 & 4
            // 2 is executive and managers, 5 & 7
            BsonDocument query = new BsonDocument();
            String position = "";
            switch (jobGrade) {
                case 0:
                    query = new BsonDocument {
                        {"$or",new BsonArray {
                            new BsonDocument {{"employment.jobGrade","1"}},
                            new BsonDocument {{"employment.jobGrade","2"}}
                        }},
                        {"employment.branch",branchSlug}
                    };
                    position = "Rank & File";
                    break;
                case 1:
                    query = new BsonDocument {
                        {"$or",new BsonArray {
                            new BsonDocument {{"employment.jobGrade","3"}},
                            new BsonDocument {{"employment.jobGrade","4"}}
                        }},
                        {"employment.branch",branchSlug}
                    };
                    position = "Officers";
                    break;
                case 2:
                    query = new BsonDocument {
                        {"$or",new BsonArray {
                            new BsonDocument {{"employment.jobGrade","5"}},
                            new BsonDocument {{"employment.jobGrade","6"}},
                            new BsonDocument {{"employment.jobGrade","7"}}
                        }},
                        {"employment.branch",branchSlug}
                    };
                    position = "Executive & Managers";
                    break;
            }

            List<users> employees = _db.GetCollection<users>("users").Find(query).ToList();
            
            var from  = Convert.ToDateTime("1/1/" + DateTime.Now.Year.ToString());
            var to  = from.AddYears(1).AddDays(-1);

            List<payroll> payrolls = _db.GetCollection<payroll>("payroll").Find(Builders<payroll>.Filter.Empty).ToList();

            repAlphalistReportModel _alphalist = new repAlphalistReportModel();

            _alphalist.year = year;
            _alphalist.position = branchName + " - " + position.ToUpper();
            _alphalist.alphalist = new List<baseAlphalist>();
            _alphalist.gross = new List<baseYearlyData>();
            _alphalist.taxWithheld = new List<baseYearlyData>();
            _alphalist.sss = new List<baseYearlyData>();
            _alphalist.philHealth = new List<baseYearlyData>();
            _alphalist.pagibig = new List<baseYearlyData>();
            _alphalist.rice = new List<baseYearlyData>();
            _alphalist.hi = new List<baseYearlyData>();

            employees.ToList().ForEach(e=>{

                Double[] grossPay = new Double[12];
                Double[] taxWithheld = new Double[12];
                Double[] sss = new Double[12];
                Double[] phic = new Double[12];
                Double[] pagibig = new Double[12];
                Double[] rice = new Double[12];
                Double[] hi = new Double[12];

                for(DateTime ctr = from; ctr<= to; ctr = ctr.AddMonths(1)) {
                    
                    var _from = ctr;
                    var _to = ctr.AddMonths(1).AddSeconds(-1);

                    payrolls.Where(p=>p.employee.id == e.employeeCode).ToList().ForEach(p=>{
                        if(p.cutoffPeriod.from >= _from && p.cutoffPeriod.to <= _to) {

                            // gross

                            var overtimePay = p.entities.Find(ent=>ent.name.ToLower() == "overtime pay");
                            var holidayPay = p.entities.Find(ent=>ent.name.ToLower() == "holiday pay");
                            var salaryAdjustment = p.entities.Find(ent=>ent.name.ToLower() == "salary adjustment");
                            var absences = p.entities.Find(ent=>ent.name.ToLower() == "absences");
                            var accountsPayable = p.entities.Find(ent=>ent.name.ToLower() == "accounts payable");
                            var riceSubsidy = p.entities.Find(ent=>ent.name.ToLower() == "rice subsidy");
                            
                            grossPay[ctr.Month-1] += p.basicPay / 2;
                            
                            if(overtimePay!=null)
                                grossPay[ctr.Month-1] += overtimePay.amount;

                            if(holidayPay!=null)
                                grossPay[ctr.Month-1] += holidayPay.amount;

                            if(salaryAdjustment!=null)
                                grossPay[ctr.Month-1] += salaryAdjustment.amount;
                            
                            if(absences!=null)
                                grossPay[ctr.Month-1] -= absences.amount;

                            if(accountsPayable!=null)
                                grossPay[ctr.Month-1] += accountsPayable.amount;

                            if(riceSubsidy!=null)
                                grossPay[ctr.Month-1] += riceSubsidy.amount;

                            // tax
                            if(p.cutoffPeriod.from >= _from && p.cutoffPeriod.to <= _to) {
                                var withholdingTax = p.entities.Find(ent=>ent.name.ToLower() == "withholding tax");

                                if(withholdingTax!=null)
                                    taxWithheld[ctr.Month-1]+= withholdingTax.amount;
                            }

                            // sss
                            if(p.cutoffPeriod.from >= _from && p.cutoffPeriod.to <= _to) {
                                var sssPremiumCon = p.entities.Find(ent=>ent.name.ToLower() == "sss premiumcon");

                                if(sssPremiumCon!=null)
                                    sss[ctr.Month-1]+= sssPremiumCon.amount;
                            }
                    
                            // phic

                            if(p.cutoffPeriod.from >= _from && p.cutoffPeriod.to <= _to) {
                                var phicPremiumCon = p.entities.Find(ent=>ent.name.ToLower() == "phic premiumcon");

                                if(phicPremiumCon!=null)
                                    phic[ctr.Month-1]+= phicPremiumCon.amount;
                            }
                    
                            // pagibig

                            if(p.cutoffPeriod.from >= _from && p.cutoffPeriod.to <= _to) {
                                var hdmfPremiumCon = p.entities.Find(ent=>ent.name.ToLower() == "hdmf premiumcon");

                                if(hdmfPremiumCon!=null)
                                    pagibig[ctr.Month-1]+= hdmfPremiumCon.amount;
                            }
                    

                            // meals
                    
                            if(p.cutoffPeriod.from >= _from && p.cutoffPeriod.to <= _to) {
                                var _meals = p.entities.Find(ent=>ent.name.ToLower() == "meals");

                                //if(_meals!=null)
                                    //meals[ctr.Month-1]+= _meals.amount;
                            }
                   

                            // rice
                    
                            if(p.cutoffPeriod.from >= _from && p.cutoffPeriod.to <= _to) {
                                var _riceSubsidy = p.entities.Find(ent=>ent.name.ToLower() == "rice subsidy");

                                if(riceSubsidy!=null)
                                    rice[ctr.Month-1]+= _riceSubsidy.amount;
                            }
                
                            // hi
                    
                            if(p.cutoffPeriod.from >= _from && p.cutoffPeriod.to <= _to) {
                                var healthInsurance = p.entities.Find(ent=>ent.name.ToLower() == "health insurance");

                                if(healthInsurance!=null)
                                    hi[ctr.Month-1]+= healthInsurance.amount;

                                //hi = 2886.24;
                            }
                        }
                    });
                }
                
                _alphalist.gross.Add(new baseYearlyData {
                    code = e.tin,
                    name = e.fullname,
                    empType = "",
                    status = e.taxCode,
                    jan = grossPay[0],
                    feb = grossPay[1],
                    mar = grossPay[2],
                    apr = grossPay[3],
                    may = grossPay[4],
                    jun = grossPay[5],
                    jul = grossPay[6],
                    aug = grossPay[7],
                    sep = grossPay[8],
                    oct = grossPay[9],
                    nov = grossPay[10],
                    dec = grossPay[11]
                });

                _alphalist.taxWithheld.Add(new baseYearlyData {
                    code = e.tin,
                    name = e.fullname,
                    empType = "",
                    status = e.taxCode,
                    jan = taxWithheld[0],
                    feb = taxWithheld[1],
                    mar = taxWithheld[2],
                    apr = taxWithheld[3],
                    may = taxWithheld[4],
                    jun = taxWithheld[5],
                    jul = taxWithheld[6],
                    aug = taxWithheld[7],
                    sep = taxWithheld[8],
                    oct = taxWithheld[9],
                    nov = taxWithheld[10],
                    dec = taxWithheld[11]
                });

                _alphalist.sss.Add(new baseYearlyData {
                    code = e.tin,
                    name = e.fullname,
                    empType = "",
                    status = e.taxCode,
                    jan = sss[0],
                    feb = sss[1],
                    mar = sss[2],
                    apr = sss[3],
                    may = sss[4],
                    jun = sss[5],
                    jul = sss[6],
                    aug = sss[7],
                    sep = sss[8],
                    oct = sss[9],
                    nov = sss[10],
                    dec = sss[11]
                });

                _alphalist.philHealth.Add(new baseYearlyData {
                    code = e.tin,
                    name = e.fullname,
                    empType = "",
                    status = e.taxCode,
                    jan = phic[0],
                    feb = phic[1],
                    mar = phic[2],
                    apr = phic[3],
                    may = phic[4],
                    jun = phic[5],
                    jul = phic[6],
                    aug = phic[7],
                    sep = phic[8],
                    oct = phic[9],
                    nov = phic[10],
                    dec = phic[11]
                });

                _alphalist.pagibig.Add(new baseYearlyData {
                    code = e.tin,
                    name = e.fullname,
                    empType = "",
                    status = e.taxCode,
                    jan = pagibig[0],
                    feb = pagibig[1],
                    mar = pagibig[2],
                    apr = pagibig[3],
                    may = pagibig[4],
                    jun = pagibig[5],
                    jul = pagibig[6],
                    aug = pagibig[7],
                    sep = pagibig[8],
                    oct = pagibig[9],
                    nov = pagibig[10],
                    dec = pagibig[11]
                });

                _alphalist.rice.Add(new baseYearlyData {
                    code = e.tin,
                    name = e.fullname,
                    empType = "",
                    status = e.taxCode,
                    jan = rice[0],
                    feb = rice[1],
                    mar = rice[2],
                    apr = rice[3],
                    may = rice[4],
                    jun = rice[5],
                    jul = rice[6],
                    aug = rice[7],
                    sep = rice[8],
                    oct = rice[9],
                    nov = rice[10],
                    dec = rice[11]
                });

                _alphalist.hi.Add(new baseYearlyData {
                    code = e.tin,
                    name = e.fullname,
                    empType = "",
                    status = e.taxCode,
                    jan = hi[0],
                    feb = hi[1],
                    mar = hi[2],
                    apr = hi[3],
                    may = hi[4],
                    jun = hi[5],
                    jul = hi[6],
                    aug = hi[7],
                    sep = hi[8],
                    oct = hi[9],
                    nov = hi[10],
                    dec = hi[11]
                });
                
            });

            return _alphalist;
        }

        public repMonthlyRemittance getMonthlyRemittance(Int32 month, Int32 year, String branchSlug)
        {
            List<users> employees = _db.GetCollection<users>("users").Find(Builders<users>.Filter.Empty).ToList();
            var from  = Convert.ToDateTime(month.ToString() + "/1/" + DateTime.Now.Year.ToString());
            var to  = from.AddMonths(1).AddSeconds(-1);

            List<payroll> payrolls = _db.GetCollection<payroll>("payroll").Find(Builders<payroll>.Filter.Empty).ToList();
            
            List<repMonthlyRemittanceDetail> rems = new List<repMonthlyRemittanceDetail>();

            employees.ForEach(e=>{

                var monthlyRate = 0.0;
                var grossPay = 0.0;
                var tax = 0.0;
                payrolls.Where(p=>p.employee.id == e.employeeCode).ToList().ForEach(p=>{
                    if(p.cutoffPeriod.from >= from && p.cutoffPeriod.to <= to) {
                        // monthlyRate
                        monthlyRate += p.basicPay / 2;
                         
                        // gross
                        var overtimePay = p.entities.Find(ent=>ent.name.ToLower() == "overtime pay");
                        var holidayPay = p.entities.Find(ent=>ent.name.ToLower() == "holiday pay");
                        var salaryAdjustment = p.entities.Find(ent=>ent.name.ToLower() == "salary adjustment");
                        var absences = p.entities.Find(ent=>ent.name.ToLower() == "absences");
                        var accountsPayable = p.entities.Find(ent=>ent.name.ToLower() == "accounts payable");
                        var riceSubsidy = p.entities.Find(ent=>ent.name.ToLower() == "rice subsidy");
                        
                        grossPay += p.basicPay / 2;
                        
                        if(overtimePay!=null)
                            grossPay+= overtimePay.amount;

                        if(holidayPay!=null)
                            grossPay+= holidayPay.amount;

                        if(salaryAdjustment!=null)
                            grossPay+= salaryAdjustment.amount;
                        
                        if(absences!=null)
                            grossPay-= absences.amount;

                        if(accountsPayable!=null)
                            grossPay+= accountsPayable.amount;

                        if(riceSubsidy!=null)
                            grossPay+= riceSubsidy.amount;

                        // tax

                        var withholdingTax = p.entities.Find(ent=>ent.name.ToLower() == "withholding tax");

                        if(withholdingTax!=null)
                            tax += withholdingTax.amount;
                    }
                });
                
                rems.Add(new repMonthlyRemittanceDetail {
                    fullname = e.fullname,
                    taxCode = e.taxCode,
                    monthlyRate = monthlyRate,
                    grossCompensation = grossPay,
                    thirteenthMonthAccrual = 0.0,
                    witholdingTax = tax,
                    sssEe = 100,
                    sssEr = 100,
                    ec = 200,
                    sssTotal = 100 + 100 + 200,
                    phicEe = 100,
                    phicEr = 100,
                    phicTotal = 200,
                    hdmfEe = 100,
                    hdmfEr = 100,
                    hdmfTotal = 200,
                    hdmfHousingLoan = 0,
                    hdmfMpl = 0,
                    hdmfCalamity = 0,
                    sssLoan = 0,
                    mp2 = 0
                });
            });
            return new repMonthlyRemittance {
                month = month,
                year = year,
                details = rems
            };
        }

        public IEnumerable<repClientDetailedSearch> clientDetailedSearch(string branchSlug, String field, Double from, Double to)
        {
            FilterDefinition<clients> query = Builders<clients>.Filter.Empty;


            switch (field.ToLower())
            {
                case "loanamount":
                    var loans = _db.GetCollection<loans>("loans").Find(
                        Builders<loans>.Filter.And(
                            Builders<loans>.Filter.Gte("members.principalAmount", from),
                            Builders<loans>.Filter.Lte("members.principalAmount", to),
                            Builders<loans>.Filter.Eq("branchSlug", branchSlug)
                        )
                    ).ToList();

                    List<loanMembers> mems = new List<loanMembers>();
                    loans.ForEach(l => {
                        mems.AddRange(l.members);
                    });

                    query = Builders<clients>.Filter.In(c => c.Id,
                        mems.Select(m => m.memberId).ToList()
                    );
                    break;
                case "cbuamount":
                    query = Builders<clients>.Filter.And(
                        Builders<clients>.Filter.Gte("cbuBalance", from),
                        Builders<clients>.Filter.Lte("cbuBalance", to),
                        Builders<clients>.Filter.Eq("branchSlug", branchSlug)
                    );
                    break;
                case "age":
                    var now = DateTime.Now.Date;
                    query = Builders<clients>.Filter.And(
                        Builders<clients>.Filter.Gte("birthdate", now.AddYears((Int32)(to + 1) * -1)),
                        Builders<clients>.Filter.Lte("birthdate", now.AddYears((Int32)(from) * -1).AddSeconds(-1)),
                        Builders<clients>.Filter.Eq("branchSlug", branchSlug)
                    );
                    break;
                case "cycle":
                    query = Builders<clients>.Filter.And(
                        Builders<clients>.Filter.Gte(c => c.loanCycle, from),
                        Builders<clients>.Filter.Lte(c => c.loanCycle, to),
                        Builders<clients>.Filter.Eq("branchSlug", branchSlug)
                    );
                    break;
            }

            List<clients> clients = new List<clients>();
            clients = _db.GetCollection<clients>("clients").Find(query).ToList();

            var branchName = _db.GetCollection<coreMaster.Models.branch>("branch").Find(Builders<coreMaster.Models.branch>.Filter.Eq("slug", branchSlug)).FirstOrDefault().name;

            //var collClients = _db.GetCollection<clients>("clients");
            //var clients = collClients.Aggregate()
            //    .Lookup("branch", "branchSlug", "slug", "branch")
            //    .Unwind("branch")
            //    .Match(new BsonDocument { query })
            //    .Project<repClientsDet>(new BsonDocument
            //            {
            //                //{"id",1},
            //                {"slug", 1},
            //                {"name", 1},
            //                {"birthdate", 1},
            //                {"cbuBalance", 1},
            //                {"loanAmount", 1},
            //                {"classification", 1},
            //                {"loanCycle", 1},
            //                {"branch.name", 1},
            //            })
            //    .ToList();

            var loans1 = _db.GetCollection<loans>("loans").Find(
                Builders<loans>.Filter.And
                (
                    Builders<loans>.Filter.In("members.memberId", clients.Select(c => c.Id).ToList()),
                    Builders<loans>.Filter.Eq("branchSlug", branchSlug)
                )
            ).ToList();

            List<repClientDetailedSearch> detClients = new List<repClientDetailedSearch>();

            clients.ForEach(c => {

                var loanAmount = 0.0;
                //var age = (c.birthdate == Convert.ToDateTime("01/01/0001 12:00 AM")) ? 0 : (DateTime.Now.Date.Subtract(DateTime.Parse(c.birthdate.ToString())).Days) / 365;              

                loans1.ForEach(l => {
                    l.members.ToList().ForEach(m => {
                        if (c.Id == m.memberId)
                            loanAmount += m.principalAmount;
                    });
                });

                Boolean toAdd = false;
                if (field.ToLower() == "loanamount")
                {
                    if (loanAmount >= from && loanAmount <= to)
                    {
                        toAdd = true;
                    }
                }
                else
                {
                    toAdd = true;
                }

                if (toAdd)
                {
                    detClients.Add(new repClientDetailedSearch
                    {
                        id = c.Id,
                        branchName = branchName,
                        slug = c.slug,
                        name = c.name,
                        birthdate = c.birthdate,
                        cbuAmount = c.cbuBalance,
                        loanAmount = loanAmount,
                        classification = c.classification,
                        loanCycleNo = c.loanCycle,
                        field = field,
                        from = from,
                        to =to
                    });
                }
            });

            return detClients;
        }


        private string swapPositionAcronymWithCompletePosition(string position) {
            var  compPos = "";
            switch (position) {
                case "bm" :{
                    compPos = "BRANCH MANAGER";
                    break;
                }
                case "ba" :{
                    compPos = "BRANCH ACCOUNTANT";
                    break;
                }
                case "baa" :{
                    compPos = "BRANCH ACCOUNTING ASSISTANT";
                    break;
                }
                case "puh" :{
                    compPos = "PROGRAM UNIT HEAD";
                    break;
                }
                case "po" :{
                    compPos = "PROGRAM OPFFICER";
                    break;
                }
                default : {
                    break;
                }
            }
            return compPos;
        }
        public signatories GetSignatories(String reportName, String branchSlug, String unitSlug, String officerSlug) {
            //bm, ba, baa, puh, po;

            // get from branch and officers, puh, po

            var branch = _db.GetCollection<kmbi_core_master.coreMaster.Models.branch>("branch").Find(x=>x.slug == branchSlug).FirstOrDefault();
            var unit = branch.units.Where(x=>x.slug == unitSlug).FirstOrDefault();
            var officer = _db.GetCollection<officers>("officers").Find(x=>x.slug == officerSlug).FirstOrDefault();

            var pos = new List<String>();
            // bm
            if(branch.head != null)
                pos.Add("bm|" + branch.head);

            // ba
            if(branch.ba != null)
                pos.Add("ba|" + branch.ba);
            // baa
            if(branch.baa != null)
                pos.Add("baa|" + branch.baa);

            // puh
            if(unit!=null)
                pos.Add("puh|"+unit.head);

            // po
            if(officer!=null)
                pos.Add("po|"+officer.name);

            var item = _sigSetts.Value.list.Where(x=>x.report == reportName).FirstOrDefault();
            var sigs = new signatories();

            if(item!=null) {
                sigs.reportName = item.report;
                sigs.preparedByPosition = swapPositionAcronymWithCompletePosition(item.preparedBy);
                sigs.preparedBy = pos.Where(x=>x.Split("|")[0] == item.preparedBy).Select(y=>y.Split("|")[1]).FirstOrDefault();
                sigs.checkedByPosition = swapPositionAcronymWithCompletePosition(sigs.checkedByPosition = item.checkedBy);
                sigs.checkedBy = pos.Where(x=>x.Split("|")[0] == item.checkedBy).Select(y=>y.Split("|")[1]).FirstOrDefault();
                sigs.approvedByPosition = swapPositionAcronymWithCompletePosition(item.approvedBy);
                sigs.approvedBy = pos.Where(x=>x.Split("|")[0] == item.approvedBy).Select(y=>y.Split("|")[1]).FirstOrDefault();
                sigs.notedByPosition = swapPositionAcronymWithCompletePosition(item.notedBy);
                sigs.notedBy = pos.Where(x=>x.Split("|")[0] == item.notedBy).Select(y=>y.Split("|")[1]).FirstOrDefault();
            }
            
            return sigs;
        }

        public balanceSheet balanceSheet(DateTime asOf) {
            var bs = new balanceSheet();
            var now = asOf;
            bs.currentYear = now.Year;
            bs.previousYear = now.Year - 1;
            bs.asOf = asOf;
            
            List<transactionDetailed> previousDatalist = _acc.transactionDetailedList(Convert.ToDateTime("1/1/0001"), Convert.ToDateTime("12/31/" + bs.previousYear.ToString()), 0, true);
            List<transactionDetailed> currentDatalist = _acc.transactionDetailedList(Convert.ToDateTime("1/1/" + bs.currentYear.ToString()), bs.asOf,0,true);

            // get accounts
            var coa = _coa.all();
            
            
            bs.name = _fsSetting.Value.balanceSheetSettings.name;
            var level1List = new List<balanceSheetDetailLevel1>();
            _fsSetting.Value.balanceSheetSettings.details.ToList().ForEach(d1=>{
                var level1Item = new balanceSheetDetailLevel1();    
                level1Item.name = d1.name;
                var level2List = new List<balanceSheetDetailLevel2>();
                d1.details.ToList().ForEach(d2=>{
                    var level2Item = new balanceSheetDetailLevel2();
                    level2Item.name = d2.name;

                    var level3List = new List<fsItemDetail>();
                    d2.details.ToList().ForEach(d3=>{



                        //details


                        var coaCodes = new List<Int32>();
                        coa.Where(x=>d3.accountCodes.Contains(Convert.ToInt32(x.code.ToString().Substring(0,3)))).ToList().ForEach(c=>{
                            coaCodes.Add(c.code);
                            coaCodes.AddRange(c.subsidiary.Select(x=>x.code));
                        });

                        var _listPrev = new List<accountDetail>();
                        var _listCur = new List<accountDetail>();
                        
                        foreach(Int32 item in coaCodes) {

                            _listPrev.Add(new accountDetail {
                                accountCode = item,
                                amount = previousDatalist.Where(x=> x.code == item &&  x.accountType == "debit").Sum(x=>x.amount) - previousDatalist.Where(x=> x.code == item &&  x.accountType == "credit").Sum(x=>x.amount)
                            });

                            

                            _listCur.Add(new accountDetail {
                                accountCode = item,
                                amount = currentDatalist.Where(x=> x.code == item &&  x.accountType == "debit").Sum(x=>x.amount) - currentDatalist.Where(x=> x.code == item &&  x.accountType == "credit").Sum(x=>x.amount)
                            });
                        }

                        
                        var currentYearBalance = 0.0;
                        currentYearBalance = _listCur.Sum(x=>x.amount);
                        if(d3.balanceYear == bs.currentYear) 
                            currentYearBalance += d3.balance;

                        currentYearBalance = Math.Round(currentYearBalance,2);

                        var previousYearBalance = 0.0;
                        previousYearBalance = _listPrev.Sum(x=>x.amount);
                        if(d3.balanceYear == bs.previousYear) 
                            previousYearBalance += d3.balance;

                        previousYearBalance = Math.Round(previousYearBalance,2);

                        var percentage = 0.0;
                        if(previousYearBalance != 0) {
                            percentage = Math.Round((currentYearBalance - previousYearBalance) / previousYearBalance,2);
                        }
                        
                        

                        level3List.Add(new fsItemDetail {
                            name = d3.name,
                            currentYearAmount = currentYearBalance,
                            previousYearAmount = previousYearBalance,
                            increaseDecrease = currentYearBalance - previousYearBalance,
                            percentage = percentage
                        });





                    });

                    level2Item.details = level3List.ToArray();
                    // level2Item.totals = new fsItemDetail {
                    //     name = "TOTAL",
                    //     currentYearAmount = level2Item.details.Sum(x=>x.currentYearAmount),
                    //     previousYearAmount = level2Item.details.Sum(x=>x.previousYearAmount),
                    //     increaseDecrease = level2Item.details.Sum(x=>x.increaseDecrease),
                    //     percentage = (level2Item.details.Sum(x=>x.increaseDecrease) != 0) ? Math.Round(level2Item.details.Sum(x=>x.increaseDecrease) / level2Item.details.Sum(x=>x.previousYearAmount),2) : 0
                    // };
                    level2List.Add(level2Item);
                });

                level1Item.details = level2List.ToArray();
                // level1Item.totals = new fsItemDetail {
                //     name = "TOTAL",
                //     currentYearAmount = level1Item.details.Sum(x=>x.totals.currentYearAmount),
                //     previousYearAmount = level1Item.details.Sum(x=>x.totals.previousYearAmount),
                //     increaseDecrease = level1Item.details.Sum(x=>x.totals.increaseDecrease),
                //     percentage = (level1Item.details.Sum(x=>x.totals.increaseDecrease) != 0) ? Math.Round(level1Item.details.Sum(x=>x.totals.increaseDecrease) / level1Item.details.Sum(x=>x.totals.previousYearAmount),2) : 0
                // };
                level1List.Add(level1Item);

            });
            bs.details = level1List.ToArray();
            // bs.totals = new fsItemDetail {
            //     name = "TOTAL",
            //     currentYearAmount = bs.details.Sum(x=>x.totals.currentYearAmount),
            //     previousYearAmount = bs.details.Sum(x=>x.totals.previousYearAmount),
            //     increaseDecrease = bs.details.Sum(x=>x.totals.currentYearAmount) - bs.details.Sum(x=>x.totals.previousYearAmount),
            //     percentage = (bs.details.Sum(x=>x.totals.currentYearAmount) - bs.details.Sum(x=>x.totals.previousYearAmount) != 0) ? Math.Round(bs.details.Sum(x=>x.totals.currentYearAmount) - bs.details.Sum(x=>x.totals.previousYearAmount) / bs.details.Sum(x=>x.totals.previousYearAmount),2) : 0
            // };
            return bs;
        }

        public incomeStatement incomeStatement(DateTime asOf) {
            var _is = new incomeStatement();
            var now = asOf;
            _is.currentYear = now.Year;
            _is.previousYear = now.Year - 1;
            _is.asOf = asOf;
            
            List<transactionDetailed> previousDatalist = _acc.transactionDetailedList(Convert.ToDateTime("1/1/0001"), Convert.ToDateTime("12/31/" + _is.previousYear.ToString()), 0, true);
            List<transactionDetailed> currentDatalist = _acc.transactionDetailedList(Convert.ToDateTime("1/1/" + _is.currentYear.ToString()), _is.asOf,0,true);

            // get accounts
            var coa = _coa.all();
            
            
            _is.name = _fsSetting.Value.incomeStatementSettings.name;
            var level1List = new List<incomeStatementDetailLevel1>();
            _fsSetting.Value.incomeStatementSettings.details.ToList().ForEach(d1=>{
                var level1Item = new incomeStatementDetailLevel1();    
                level1Item.name = d1.name;
                var level2List = new List<incomeStatementDetailLevel2>();
                
                d1.details.ToList().ForEach(d2=>{
                    var level2Item = new incomeStatementDetailLevel2();
                    level2Item.name = d2.name;
                    
                    var level3List = new List<fsItemDetail>();

                    d2.details.ToList().ForEach(d3=>{
                        
                        
                        
                        
                        
                        
                        //details


                        var coaCodes = new List<Int32>();
                        coa.Where(x=>d3.accountCodes.Contains(Convert.ToInt32(x.code.ToString().Substring(0,3)))).ToList().ForEach(c=>{
                            coaCodes.Add(c.code);
                            coaCodes.AddRange(c.subsidiary.Select(x=>x.code));
                        });

                        var _listPrev = new List<accountDetail>();
                        var _listCur = new List<accountDetail>();
                        
                        foreach(Int32 item in coaCodes) {

                            _listPrev.Add(new accountDetail {
                                accountCode = item,
                                amount = previousDatalist.Where(x=> x.code == item &&  x.accountType == "debit").Sum(x=>x.amount) - previousDatalist.Where(x=> x.code == item &&  x.accountType == "credit").Sum(x=>x.amount)
                            });

                            

                            _listCur.Add(new accountDetail {
                                accountCode = item,
                                amount = currentDatalist.Where(x=> x.code == item &&  x.accountType == "debit").Sum(x=>x.amount) - currentDatalist.Where(x=> x.code == item &&  x.accountType == "credit").Sum(x=>x.amount)
                            });
                        }

                        
                        var currentYearBalance = 0.0;
                        currentYearBalance = _listCur.Sum(x=>x.amount);
                        if(d3.balanceYear == _is.currentYear) 
                            currentYearBalance += d3.balance;

                        currentYearBalance = Math.Round(currentYearBalance,2);

                        var previousYearBalance = 0.0;
                        previousYearBalance = _listPrev.Sum(x=>x.amount);
                        if(d3.balanceYear == _is.previousYear) 
                            previousYearBalance += d3.balance;

                        previousYearBalance = Math.Round(previousYearBalance,2);

                        var percentage = 0.0;
                        if(previousYearBalance != 0) {
                            percentage = Math.Round((currentYearBalance - previousYearBalance) / previousYearBalance,2);
                        }
                        
                        

                        level3List.Add(new fsItemDetail {
                            name = d3.name,
                            currentYearAmount = currentYearBalance,
                            previousYearAmount = previousYearBalance,
                            increaseDecrease = currentYearBalance - previousYearBalance,
                            percentage = percentage
                        });




                    });

                    level2Item.details = level3List.ToArray();
                    // level2Item.totals = new fsItemDetail {
                    //     name = "TOTAL",
                    //     currentYearAmount = level2Item.details.Sum(x=>x.currentYearAmount),
                    //     previousYearAmount = level2Item.details.Sum(x=>x.previousYearAmount),
                    //     increaseDecrease = level2Item.details.Sum(x=>x.increaseDecrease),
                    //     percentage = (level2Item.details.Sum(x=>x.increaseDecrease) != 0) ? Math.Round(level2Item.details.Sum(x=>x.increaseDecrease) / level2Item.details.Sum(x=>x.previousYearAmount),2) : 0
                    // };
                    level2List.Add(level2Item);
                });
                level1Item.details = level2List.ToArray();
                // level1Item.totals = new fsItemDetail {
                //     name = "TOTAL",
                //     currentYearAmount = level1Item.details.Sum(x=>x.totals.currentYearAmount),
                //     previousYearAmount = level1Item.details.Sum(x=>x.totals.previousYearAmount),
                //     increaseDecrease = level1Item.details.Sum(x=>x.totals.increaseDecrease),
                //     percentage = (level1Item.details.Sum(x=>x.totals.increaseDecrease) != 0) ? Math.Round(level1Item.details.Sum(x=>x.totals.increaseDecrease) / level1Item.details.Sum(x=>x.totals.previousYearAmount),2) : 0
                // };
                level1List.Add(level1Item);

            });
            _is.details = level1List.ToArray();
            // _is.totals = new fsItemDetail {
            //     name = "TOTAL",
            //     currentYearAmount = _is.details.Sum(x=>x.totals.currentYearAmount),
            //     previousYearAmount = _is.details.Sum(x=>x.totals.previousYearAmount),
            //     increaseDecrease = _is.details.Sum(x=>x.totals.currentYearAmount) - _is.details.Sum(x=>x.totals.previousYearAmount),
            //     percentage = (_is.details.Sum(x=>x.totals.currentYearAmount) - _is.details.Sum(x=>x.totals.previousYearAmount) != 0) ? Math.Round(_is.details.Sum(x=>x.totals.currentYearAmount) - _is.details.Sum(x=>x.totals.previousYearAmount) / _is.details.Sum(x=>x.totals.previousYearAmount),2) : 0
            // };
            level1List.Clear();
            return _is;
        }

        public repLrCbu lrCbuReport(String branchSlug, DateTime date) {
            // get branch by branch slug
            var branch = _db.GetCollection<coreMaster.Models.branch>("branch").Find(Builders<coreMaster.Models.branch>.Filter.Eq(x=>x.slug, branchSlug)).FirstOrDefault();
            // get officers from branch's  unit
            var officers = _db.GetCollection<officers>("officers").Find(Builders<officers>.Filter.In(x=>x.unitId, branch.units.Select(x=>x.Id))).ToList();
            // get centers of officers
            var center = _db.GetCollection<centerInstance>("centerInstance").Find(Builders<centerInstance>.Filter.In(z=>z.officerSlug, officers.Select(x=>x.slug))).ToList();

            // get loans by centers
            var loans = _db.GetCollection<loans>("loans").Find(Builders<loans>.Filter.And(
                Builders<loans>.Filter.In(z=>z.centerSlug, center.Select(x=>x.slug)),
                Builders<loans>.Filter.Lte(x=>x.disbursementDate, date)
            )).ToList();

            //centerClients = centerClients.Where(x=>x.re).ToList();

            // get payments of loans and paid at is less than as of
            var payments = _db.GetCollection<paymentLoans>("paymentLoans").Find(Builders<paymentLoans>.Filter.And(
                Builders<paymentLoans>.Filter.Lte(x=>x.officialReceipt.paidAt, date), 
                Builders<paymentLoans>.Filter.In(x=>x.loan.referenceId, loans.Select(x=>x.referenceId))
            )).ToList();

            var totalPayments = _db.GetCollection<paymentLoans>("paymentLoans").Find(Builders<paymentLoans>.Filter.In(x=>x.loan.referenceId, loans.Select(x=>x.referenceId))).ToList();

            // get clients of centers
            var centerClients = _db.GetCollection<clients>("clients").Find(Builders<clients>.Filter.In(z=>z.centerSlug, center.Select(x=>x.slug))).ToList();
            
            // get cbuwithdrawal per year
            var cbuWithdrawal = _db.GetCollection<cbuWithdrawal>("cbuWithdrawal").Find(Builders<cbuWithdrawal>.Filter.And(
                Builders<cbuWithdrawal>.Filter.Gte(x=>x.rfp.datePrepared, Convert.ToDateTime("1/1/" + date.Year.ToString())),
                Builders<cbuWithdrawal>.Filter.Lte(x=>x.rfp.datePrepared, Convert.ToDateTime("12/31/" + date.Year.ToString())), 
                Builders<cbuWithdrawal>.Filter.In(z=>z.loan.refId, loans.Select(x=>x.referenceId))
            )).ToList();

            // get offsets per year
            var offsets = _db.GetCollection<paymentLoans>("paymentLoans").Find(Builders<paymentLoans>.Filter.And(
                Builders<paymentLoans>.Filter.Gte(x=>x.officialReceipt.paidAt, Convert.ToDateTime("1/1/" + date.Year.ToString())),
                Builders<paymentLoans>.Filter.Lte(x=>x.officialReceipt.paidAt, Convert.ToDateTime("12/31/" + date.Year.ToString())), 
                Builders<paymentLoans>.Filter.In(z=>z.loan.referenceId, loans.Select(x=>x.referenceId)),
                Builders<paymentLoans>.Filter.Eq(z=>z.mode, "offset")
            )).ToList();

            // declare lrcbu object
            var item = new repLrCbu();

            item.date = date;
            item.branch = branch.name;
            
            // declare unit container
            var unitList = new List<repLrCbuUnit>();

            // get cbu beg balance of clients
            
            //var cbuOfBeg = _db.GetCollection<cbuLogs>("cbuLogs").Find(Builders<cbuLogs>.Filter.Eq(x=>x.type,null)).ToList();
            


            // loop units of branch
            branch.units.ToList().ForEach(uni=>{
                
                // declare po container
                var offList = new List<repLrCbuPo>();

                // get po per unit
                officers.Where(off=>off.unitId == uni.Id).OrderBy(x=>x.designation).ToList().ForEach(off=>{
                    
                    // set po name
                    var poName = "";
                    if(off.name == "") 
                        poName = off.designation;
                    else
                        poName = off.designation + " - " + off.name;

                    // declare center container
                    var centList = new List<repLrCbuCenter>();

                    center.Where(cent=>cent.officerSlug == off.slug).ToList().ForEach(cent=> {
                        // main logic

                        // get loan per center
                        var loanList = loans.Where(x=>x.centerId == cent.Id).ToList();
                        // get each payments of loanList
                        var payList = payments.Where(x=> loanList.Select(y=>y.referenceId).Contains(x.loan.referenceId)).ToList();
                        var totalPayList = totalPayments.Where(x=> loanList.Select(y=>y.referenceId).Contains(x.loan.referenceId)).ToList();

                        
                        // get BALANCES section

                        // principal due - principal payment
                        var loanReceivable = loanList.Where(x=>x.status != "processing").Sum(x=>x.amounts.principalAmount) - payList.Sum(x=>x.officialReceipt.amounts.principal);
                        // intereest due - interest payment
                        var interesetReceivable = loanList.Where(x=>x.status != "processing").Sum(x=>x.amounts.interestAmount) - payList.Sum(x=>x.officialReceipt.amounts.interest);
                        // cbu payments
                        //var cbu = payList.Sum(x=>x.members.ToList().Sum(y=>y.amounts.cbu));
                        var clientList = new List<String>();
                        
                        loanList.ForEach(x=>{
                            clientList.AddRange(x.members.Select(y=>y.memberId).ToList());
                        });
                        
                        //cbu += cbuOfBeg.Where(y=> clientList.Contains(y.clientId)) .Sum(x=>x.amount);
                        var curCbuBal = centerClients.Where(y=> clientList.Contains(y.Id)).Sum(x=>x.cbuBalance);
                        var totalCbuPayments = totalPayList.Sum(x=>x.members.ToList().Sum(y=>y.amounts.cbu));
                        var cbuPayments = payList.Sum(x=>x.members.ToList().Sum(y=>y.amounts.cbu));

                        var cbu = (curCbuBal - totalCbuPayments) + cbuPayments;

                        // get clients slug with active loan
                        var loanMembers = new List<String>();
                        // loop through active loan
                        loanList.Where(x=>x.status=="for-payment").ToList().ForEach(c=>{
                            loanMembers.AddRange(c.members.Select(x=>x.slug));
                        });

                        // get clients slug with pending loan
                        var pendingLoanMembers = new List<String>();
                        // loop through active loan
                        loanList.Where(x=>x.status=="processing").ToList().ForEach(c=>{
                            pendingLoanMembers.AddRange(c.members.Select(x=>x.slug));
                        });

                        // get distinct members with active loan from the collection of loan members of loans
                        var distinctLoanMembers = loanMembers.Distinct();

                        // get distinct members with pending loan from the collection of loan members of loans
                        var distinctPendingLoanMembers = pendingLoanMembers.Distinct();

                        // get cbu withdrawal
                        var cbuPerMonth = cbuWithdrawal.Where(x=>
                            x.rfp.datePrepared>= Convert.ToDateTime(date.Month + "/1/" + date.Year) && 
                            x.rfp.datePrepared<= Convert.ToDateTime(date.Month + "/1/" + date.Year).AddMonths(1).AddSeconds(-1)).Select(x=>x.members.Select(y=>y.slug));
                            
                        // get offsets
                        var offsetPerMonth = offsets.Where(x=>
                            x.officialReceipt.paidAt >= Convert.ToDateTime(date.Month + "/1/" + date.Year) && 
                            x.officialReceipt.paidAt<= Convert.ToDateTime(date.Month + "/1/" + date.Year).AddMonths(1).AddSeconds(-1)).Select(x=>x.members.Select(y=>y.slug));

                        var clientsWithCbuWithAndOffsets = new List<String>();
                        
                        cbuPerMonth.ToList().ForEach(x=>{
                            clientsWithCbuWithAndOffsets.AddRange(x);
                        });

                        offsetPerMonth.ToList().ForEach(x=>{
                            clientsWithCbuWithAndOffsets.AddRange(x);
                        });

                        // select distinct client slugs from cbuwithdrawal
                        var distinctClientsWithCbuAndOffsets = clientsWithCbuWithAndOffsets.Distinct();

                        // from list of centerClients, match clients with active loan

                        // clients of center
                        var centerClientsFiltered = centerClients.Where(x=>x.centerSlug == cent.slug).ToList();

                        // clients with loan
                        var clientWithLoan = centerClientsFiltered.Where(x=>distinctLoanMembers.Contains(x.slug)).Count();
                        
                        // subtract clients with loan to number of center clients
                        var clientWithoutLoan = centerClientsFiltered.Where(x=>!distinctLoanMembers.Contains(x.slug)).Count();

                        // clients dormant
                        var clientsDormant = centerClientsFiltered.Where(x=>!distinctLoanMembers.Contains(x.slug) && x.cbuBalance > 0).Count();

                        // clients pending for reloan
                        var clientsPendingForReloan = centerClientsFiltered.Where(x=>distinctPendingLoanMembers.Contains(x.slug)).Count();


                        // clients default
                        var clientsDefault = 0;

                        // clients returning
                        var clientsReturning = centerClientsFiltered.Where(x=>x.loanCycle > 1).Count();

                        // clients resigned
                        // clients matured

                        // match clients in dictinct cbuwithdrawal and offsets with center client with 0 cbu balance
                        var clientResigned = centerClients.Where(x=> distinctClientsWithCbuAndOffsets.Contains(x.slug) && x.centerSlug == cent.slug && x.cbuBalance == 0).Count();

                        // client total is with loan and without loan
                        var clientLoanStatusesTotal = clientWithLoan + clientWithoutLoan;

                        // get disbursement of the month by loans disbursement date
                        var disbursementForTheMonth = loanList.Where(x=>
                            x.disbursementDate<=date && 
                            x.disbursementDate >= Convert.ToDateTime(date.Month.ToString() + "/1/" + date.Year.ToString())).Sum(x=>x.amounts.principalAmount);


                        // get principal due less than date
                        var principalDue = 0.0;

                        loanList.ForEach(l=>{
                            l.members.ToList().ForEach(m=>{
                                m.schedule.ToList().ForEach(s=>{
                                    if(s.collectionDate<=date) {
                                        principalDue+=s.currentPrincipalDue;
                                    }
                                });
                            });
                        });

                        // get principal payment less than date
                        var principalPay = 0.0;
                        payList.ForEach(p=>{
                            if(p.officialReceipt.paidAt<=date) {
                                principalPay += p.officialReceipt.amounts.principal;
                            }
                        });

                        // get par amount
                        var parAmount = principalDue - principalPay;

                        // center status
                        // processing, for-payment
                        var _active = 0;
                        var _inactive = 0;
                        var _matured = 0;
                        
                        loanList.ForEach(l=>{
                            if(l.lastPaymentDate<date) {
                                _matured++;
                            } else {
                                if(l.status=="for-payment") {
                                    _active++;
                                } else if(l.status=="processing") {
                                    _inactive++;
                                }
                            }
                        });
                        
                        
                        var centerStatus = "";
                        
                        var clientActive = 0;
                        var clientInActive = 0;
                        var clientMatured = 0;
                        var clientStatusTotal = 0;
                        
                        if(loanList.Count==0) {
                            centerStatus = "NO CENTER";
                        } else if(_matured > 0) {
                            centerStatus = "MATURED";
                            clientActive = 0;
                            clientInActive = clientWithoutLoan;
                            clientMatured = clientWithLoan;
                        } else if(_active > 0) {
                            centerStatus = "ACTIVE";
                            clientActive = clientWithLoan;
                            clientInActive = clientWithoutLoan;
                            clientMatured = 0;
                        } else {
                            centerStatus = "INACTIVE";
                            clientActive = 0;
                            clientInActive = clientWithoutLoan;
                            clientMatured = 0;
                        }

                        clientStatusTotal = clientActive + clientInActive + clientMatured;

                        // continue

                        clientsDefault = clientMatured;
                       

                        var newloan = 0;
                        var reloan = 0;

                        newloan = centerClientsFiltered.Where(x=>distinctLoanMembers.Contains(x.slug) && x.loanCycle == 1).Count();
                        reloan = centerClientsFiltered.Where(x=>distinctLoanMembers.Contains(x.slug) && x.loanCycle > 1).Count();

                        centList.Add(new repLrCbuCenter {
                            name = cent.code + " - " + cent.referenceId,
                            centerName = (cent.code+centerStatus).ToLower(),
                            balances = new repLrCbuBalances {
                                loanReceivable = loanReceivable,
                                interestReceivable = interesetReceivable,
                                cbu = cbu
                            },
                            clientLoanStatuses = new repLrCbuClientLoanStatuses {
                                withLoan = clientWithLoan,
                                pendingForReloan = clientsPendingForReloan,
                                dormantClientsWithCbu = clientsDormant,
                                defaultClients = clientMatured,
                                total = clientWithLoan + clientsPendingForReloan + clientsDormant,
                                returningClients = clientsReturning,
                                withoutLoan = clientWithoutLoan,
                                resigned = clientResigned
                                //total = clientLoanStatusesTotal
                            },
                            disbursementForTheMonth = disbursementForTheMonth,
                            dailyMatured = new repLrCbuDailyMatured {
                                parAmount = parAmount
                            },
                            centerStatus = new repLrCbuCenterStatus {
                                status = centerStatus
                            },
                            clientStatuses = new repLrCbuClientStatuses {
                                active = clientActive,
                                inActive = clientInActive,
                                matured = clientMatured,
                                total = clientStatusTotal
                            },
                            fallout = new repLrCbuFallOut {
                                resigned = clientResigned,
                                rate = (reloan + clientResigned == 0) ? 0 : clientResigned / (reloan + clientResigned) * 100
                            },
                            retention = new repLrCbuRetention {
                                newLoan = newloan,
                                reloan = reloan,
                                rate = (reloan + clientResigned == 0) ? 0 : (reloan / (reloan + clientResigned)) * 100
                            },
                        });
                    });

                    offList.Add(new repLrCbuPo {
                        slug = off.slug,
                        name = poName,
                        center = centList.OrderBy(x=>x.name).ToArray(),
                        totals = new repLrCbuTotal {
                            name = "TOTAL",
                            balances = new repLrCbuBalances {
                                loanReceivable = centList.Sum(x=>x.balances.loanReceivable),
                                interestReceivable = centList.Sum(x=>x.balances.interestReceivable),
                                cbu = centList.Sum(x=>x.balances.cbu)
                            },
                            clientLoanStatuses = new repLrCbuClientLoanStatuses {
                                withLoan = centList.Sum(x=>x.clientLoanStatuses.withLoan),
                                pendingForReloan = centList.Sum(x=>x.clientLoanStatuses.pendingForReloan),
                                dormantClientsWithCbu = centList.Sum(x=>x.clientLoanStatuses.dormantClientsWithCbu),
                                defaultClients = centList.Sum(x=>x.clientLoanStatuses.defaultClients),
                                total = centList.Sum(x=>x.clientLoanStatuses.total),
                                returningClients = centList.Sum(x=>x.clientLoanStatuses.returningClients),
                                resigned = centList.Sum(x=>x.clientLoanStatuses.resigned),
                                withoutLoan = centList.Sum(x=>x.clientLoanStatuses.withoutLoan)
                            },
                            disbursementForTheMonth = centList.Sum(x=>x.disbursementForTheMonth),
                            dailyMatured = new repLrCbuDailyMatured {
                                parAmount = centList.Sum(x=>x.dailyMatured.parAmount)
                            },
                            centerStatus = new repLrCbuCenterStatusTotal {
                                active = centList.Where(x=>x.centerStatus.status == "ACTIVE").Select(x=>x.centerName).Distinct().Count(),
                                inActive = centList.Where(x=>x.centerStatus.status == "INACTIVE").Select(x=>x.centerName).Distinct().Count(), 
                                matured = centList.Where(x=>x.centerStatus.status == "MATURED").Select(x=>x.centerName).Distinct().Count()
                            },
                            clientStatuses = new repLrCbuClientStatuses {
                                active = centList.Sum(x=>x.clientStatuses.active),
                                inActive = centList.Sum(x=>x.clientStatuses.inActive),
                                matured = centList.Sum(x=>x.clientStatuses.matured),
                                total = centList.Sum(x=>x.clientStatuses.total)
                            },
                            fallout = new repLrCbuFallOut {
                                resigned = centList.Sum(x=>x.fallout.resigned),
                                rate =  (centList.Sum(x=>x.retention.reloan + x.fallout.resigned) == 0) ? 0 : centList.Sum(x=>x.fallout.resigned) / (centList.Sum(x=>x.retention.reloan + x.fallout.resigned)) * 100
                            },
                            retention = new repLrCbuRetention {
                                newLoan = centList.Sum(x=>x.retention.newLoan),
                                reloan = centList.Sum(x=>x.retention.reloan),
                                rate = (centList.Sum(x=>x.retention.reloan + x.fallout.resigned) == 0) ? 0 : (centList.Sum(x=>x.retention.reloan) / (centList.Sum(x=>x.retention.reloan + x.fallout.resigned))) * 100
                            }
                        }
                    });
                });
                
                var unitSigs = GetSignatories("lrcbu-per-unit",branch.slug, uni.slug, "");

                unitList.Add(new repLrCbuUnit {
                    name = uni.code,
                    slug = uni.slug,
                    po = offList.OrderBy(x=>x.name).ToArray(),
                    totals = new repLrCbuTotal {
                        name = "TOTAL",
                        balances = new repLrCbuBalances {
                            loanReceivable = offList.Select(x=>x.totals).Sum(x=>x.balances.loanReceivable),
                            interestReceivable = offList.Select(x=>x.totals).Sum(x=>x.balances.interestReceivable),
                            cbu = offList.Select(x=>x.totals).Sum(x=>x.balances.cbu)
                        },
                        clientLoanStatuses = new repLrCbuClientLoanStatuses {
                            withLoan = offList.Select(x=>x.totals).Sum(x=>x.clientLoanStatuses.withLoan),
                            pendingForReloan = offList.Select(x=>x.totals).Sum(x=>x.clientLoanStatuses.pendingForReloan),
                            dormantClientsWithCbu = offList.Select(x=>x.totals).Sum(x=>x.clientLoanStatuses.dormantClientsWithCbu),
                            defaultClients = offList.Select(x=>x.totals).Sum(x=>x.clientLoanStatuses.defaultClients),
                            total = offList.Select(x=>x.totals).Sum(x=>x.clientLoanStatuses.total),
                            returningClients = offList.Select(x=>x.totals).Sum(x=>x.clientLoanStatuses.returningClients),
                            resigned = offList.Select(x=>x.totals).Sum(x=>x.clientLoanStatuses.resigned),
                            withoutLoan = offList.Select(x=>x.totals).Sum(x=>x.clientLoanStatuses.withoutLoan)
                        },
                        disbursementForTheMonth = offList.Select(x=>x.totals).Sum(x=>x.disbursementForTheMonth),
                        dailyMatured = new repLrCbuDailyMatured {
                            parAmount = offList.Select(x=>x.totals).Sum(x=>x.dailyMatured.parAmount)
                        },
                        centerStatus = new repLrCbuCenterStatusTotal {
                            active = offList.Select(x=>x.totals).Sum(x=>x.centerStatus.active),
                            inActive = offList.Select(x=>x.totals).Sum(x=>x.centerStatus.inActive),
                            matured = offList.Select(x=>x.totals).Sum(x=>x.centerStatus.matured)
                        },
                        clientStatuses = new repLrCbuClientStatuses {
                            active = offList.Select(x=>x.totals).Sum(x=>x.clientStatuses.active),
                            inActive = offList.Select(x=>x.totals).Sum(x=>x.clientStatuses.inActive),
                            matured = offList.Select(x=>x.totals).Sum(x=>x.clientStatuses.matured),
                            total = offList.Select(x=>x.totals).Sum(x=>x.clientStatuses.total)
                        },
                        fallout = new repLrCbuFallOut {
                            resigned = offList.Select(x=>x.totals).Sum(x=>x.fallout.resigned),
                            rate = (offList.Select(x=>x.totals).Sum(x=>x.fallout.resigned + x.retention.reloan) == 0) ? 0 : offList.Select(x=>x.totals).Sum(x=>x.fallout.resigned) / (offList.Select(x=>x.totals).Sum(x=>x.fallout.resigned + x.retention.reloan)) * 100
                        },
                        retention = new repLrCbuRetention {
                            newLoan = offList.Select(x=>x.totals).Sum(x=>x.retention.newLoan),
                            reloan = offList.Select(x=>x.totals).Sum(x=>x.retention.reloan),
                            rate = (offList.Select(x=>x.totals).Sum(x=>x.fallout.resigned + x.retention.reloan) == 0) ? 0 : (offList.Select(x=>x.totals).Sum(x=>x.retention.reloan) / ((offList.Select(x=>x.totals).Sum(x=>x.fallout.resigned + x.retention.reloan)))) * 100
                        }
                    },
                    preparedBy = unitSigs.preparedBy,
                    checkedBy = unitSigs.checkedBy,
                    preparedByPosition = unitSigs.preparedByPosition,
                    checkedByPosition = unitSigs.checkedByPosition,
                    notedBy = unitSigs.notedBy,
                    notedByPosition = unitSigs.notedByPosition
                });

                 
            });

            // attach unit container to lrcbu object
            item.unit = unitList.OrderBy(x=>x.name).ToArray();
            
            // ----------------------------------------------------------------------- //
            // summary section
            var summary = new repLrCbuSummary();
            
            var sumUnitList = new List<repLrCbuSummaryUnit>();

            unitList.ForEach(u=>{
                var unit = new  repLrCbuSummaryUnit();
                unit.name = u.name;
                
                var sumPoList = new List<repLrCbuSummaryPo>();

                


                u.po.ToList().ForEach(p=>{
                            
                    // LD

                    // new loan
                    
                    var ldNewLoan = loans.Where(x=>x.centerLoanCycle == 1 && x.officerSlug == p.slug).ToList();
                    var ldReloanNotInLpr = loans.Where(x=>x.centerLoanCycle > 1 && x.officerSlug == p.slug && x.lastPaymentDate < date).ToList();
                    var ldReloanInLpr = loans.Where(x=>x.centerLoanCycle > 1 && x.officerSlug == p.slug && x.lastPaymentDate < date).ToList();
                    var ldTotalCenterDisbursedForTheMonth = loans.Where(x=>x.officerSlug == p.slug && x.disbursementDate.Value.Month == date.Month && x.disbursementDate.Value.Year == date.Year).ToList();


                    var ldNewLoanCount = ldNewLoan.Count;
                    var ldReloanNotInLprCount = ldReloanNotInLpr.Count();
                    var ldReloanInLprCount = ldReloanInLpr.Count();
                    var ldTotalCenterDisbursedCount = ldNewLoanCount + ldReloanNotInLprCount + ldReloanInLprCount;
                    var ldTotalCenterDisbursedForTheMonthCount = ldTotalCenterDisbursedForTheMonth.Count();

                    var centerForTheMonthNewLoanCount = ldTotalCenterDisbursedForTheMonth.Where(x=>x.centerLoanCycle==1 && x.officerSlug == p.slug).Count();
                    var centerForTheMonthReloanLoanCount = ldTotalCenterDisbursedForTheMonth.Where(x=>x.centerLoanCycle>1 && x.officerSlug == p.slug).Count();;

                    var ldNewLoanAmount = ldNewLoan.Sum(x=>x.amounts.principalAmount);
                    var ldReloanNotInLprAmount = ldReloanNotInLpr.Sum(x=>x.amounts.principalAmount);
                    var ldReloanInLprAmount = ldReloanInLpr.Sum(x=>x.amounts.principalAmount);
                    var totalCenterDisbursedAmount = ldReloanNotInLprAmount + ldReloanInLprAmount;

                    var po = new repLrCbuSummaryPo {
                        name = p.name,
                        balances = new repLrCbuSummaryBalances {
                            loan = new repLrCbuSummaryBalancesLoan {
                                lrPrincipal = p.totals.balances.loanReceivable,
                                eir = p.totals.balances.interestReceivable,
                                total = p.totals.balances.loanReceivable + p.totals.balances.interestReceivable
                            },
                            par = new repLrCbuSummaryBalancesPar {
                                amount = p.totals.dailyMatured.parAmount,
                                rate = (p.totals.balances.loanReceivable > 0) ? (p.totals.dailyMatured.parAmount / p.totals.balances.loanReceivable) * 100 : 0
                            },
                            cbu = p.totals.balances.cbu
                        },
                        centers = new repLrCbuSummaryCenters {
                            active = p.totals.centerStatus.active,
                            inActive = p.totals.centerStatus.inActive,
                            matured = p.totals.centerStatus.matured,
                            total = p.totals.centerStatus.active + p.totals.centerStatus.inActive + p.totals.centerStatus.matured
                        },
                        clients = new repLrCbuSummaryClients {
                            active = new repLrCbuSummaryClientsActive {
                                withLoan = p.totals.clientLoanStatuses.withLoan,
                                pendingForReloan = p.totals.clientLoanStatuses.pendingForReloan
                            },
                            inActive = new repLrCbuSummaryClientsInActive {
                                dormantClients = p.totals.clientLoanStatuses.dormantClientsWithCbu,
                                defaultClients = p.totals.clientLoanStatuses.defaultClients
                            },
                            total = p.totals.clientStatuses.total + p.totals.clientStatuses.inActive,
                            forTheMonth = new repLrCbuSummaryClientsForTheMonth {
                                newLoan = p.totals.retention.newLoan,
                                reloan = p.totals.retention.reloan
                            },
                            returningClients = p.totals.clientLoanStatuses.returningClients,
                            resigned = new repLrCbuSummaryClientsResigned {
                                forTheMonth = p.totals.clientLoanStatuses.resigned,
                                cumulative = p.totals.clientLoanStatuses.resigned
                            },
                            withLoan = new repLrCbuSummaryClientsWithLoan {
                                active = p.totals.clientStatuses.active,
                                matured = p.totals.clientStatuses.matured,
                                total = p.totals.clientStatuses.total,
                            },
                            withoutLoan = p.totals.clientStatuses.inActive,
                        },
                        loanDisbursement = new repLrCbuSummaryLoanDisbursement {
                            newLoan = ldNewLoanCount,
                            reloanNotInLPR = ldReloanNotInLprCount,
                            reloanLastPaymentRelease = ldReloanNotInLprCount,
                            totalCenterDisbursed = new repLrCbuSummaryTotalCenterDisbursed {
                                cumulative = ldTotalCenterDisbursedCount,
                                forTheMonth = ldTotalCenterDisbursedForTheMonthCount
                            },
                            centerForTheMonth = new repLrCbuSummaryCenterForTheMonth {
                                newLoan = centerForTheMonthNewLoanCount,
                                reloan = centerForTheMonthReloanLoanCount
                            },
                            amount = new repLrCbuSummaryAmount {
                                newLoan = ldNewLoanAmount,
                                reloan = ldReloanNotInLprAmount,
                                cumulative = ldReloanInLprAmount,
                                forTheMonth = totalCenterDisbursedAmount
                            }
                        }
                    };
                    sumPoList.Add(po);
                });

                unit.po = sumPoList.OrderBy(x=>x.name).ToArray();

                unit.total = new repLrCbuSummaryTotal {
                    name = "TOTAL",
                    balances = new repLrCbuSummaryBalances {
                        loan = new repLrCbuSummaryBalancesLoan {
                            lrPrincipal = sumPoList.Sum(x=>x.balances.loan.lrPrincipal),
                            eir = sumPoList.Sum(x=>x.balances.loan.eir),
                            total = sumPoList.Sum(x=>x.balances.loan.total)
                        },
                        par = new repLrCbuSummaryBalancesPar {
                            amount = sumPoList.Sum(x=>x.balances.par.amount),
                            rate = (sumPoList.Sum(x=>x.balances.par.amount) > 0) ? (sumPoList.Sum(x=>x.balances.par.amount) / sumPoList.Sum(x=>x.balances.loan.lrPrincipal)) * 100 : 0
                        },
                        cbu = sumPoList.Sum(x=>x.balances.cbu)
                    },
                    centers = new repLrCbuSummaryCenters {
                        active = sumPoList.Sum(x=>x.centers.active),
                        inActive = sumPoList.Sum(x=>x.centers.inActive),
                        matured = sumPoList.Sum(x=>x.centers.matured),
                        total = sumPoList.Sum(x=>x.centers.total)
                    },
                    clients = new repLrCbuSummaryClients {
                        active = new repLrCbuSummaryClientsActive {
                            withLoan = sumPoList.Sum(x=>x.clients.active.withLoan),
                            pendingForReloan = sumPoList.Sum(x=>x.clients.active.pendingForReloan),
                        },
                        inActive = new repLrCbuSummaryClientsInActive {
                            dormantClients = sumPoList.Sum(x=>x.clients.inActive.dormantClients),
                            defaultClients = sumPoList.Sum(x=>x.clients.inActive.defaultClients)
                        },
                        total = sumPoList.Sum(x=>x.clients.total),
                        forTheMonth = new repLrCbuSummaryClientsForTheMonth {
                            newLoan = sumPoList.Sum(x=>x.clients.forTheMonth.newLoan),
                            reloan = sumPoList.Sum(x=>x.clients.forTheMonth.reloan)
                        },
                        returningClients = sumPoList.Sum(x=>x.clients.returningClients), 
                        resigned = new repLrCbuSummaryClientsResigned {
                            forTheMonth = sumPoList.Sum(x=>x.clients.resigned.forTheMonth),
                            cumulative = sumPoList.Sum(x=>x.clients.resigned.cumulative)
                        },
                        withLoan = new repLrCbuSummaryClientsWithLoan {
                            active = sumPoList.Sum(x=>x.clients.withLoan.active),
                            matured = sumPoList.Sum(x=>x.clients.withLoan.matured),
                            total = sumPoList.Sum(x=>x.clients.withLoan.total),
                        },
                        withoutLoan = sumPoList.Sum(x=>x.clients.withoutLoan),
                    },
                    loanDisbursement = new repLrCbuSummaryLoanDisbursement {
                        newLoan = sumPoList.Sum(x=>x.loanDisbursement.newLoan),
                        reloanNotInLPR = sumPoList.Sum(x=>x.loanDisbursement.reloanNotInLPR),
                        reloanLastPaymentRelease = sumPoList.Sum(x=>x.loanDisbursement.reloanLastPaymentRelease),
                        totalCenterDisbursed = new repLrCbuSummaryTotalCenterDisbursed {
                            cumulative = sumPoList.Sum(x=>x.loanDisbursement.totalCenterDisbursed.cumulative),
                            forTheMonth = sumPoList.Sum(x=>x.loanDisbursement.totalCenterDisbursed.forTheMonth)
                        },
                        centerForTheMonth = new repLrCbuSummaryCenterForTheMonth {
                            newLoan = sumPoList.Sum(x=>x.loanDisbursement.centerForTheMonth.newLoan),
                            reloan = sumPoList.Sum(x=>x.loanDisbursement.centerForTheMonth.reloan)
                        },
                        amount = new repLrCbuSummaryAmount {
                            newLoan = sumPoList.Sum(x=>x.loanDisbursement.amount.newLoan),
                            reloan = sumPoList.Sum(x=>x.loanDisbursement.amount.reloan),
                            cumulative = sumPoList.Sum(x=>x.loanDisbursement.amount.cumulative),
                            forTheMonth = sumPoList.Sum(x=>x.loanDisbursement.amount.forTheMonth)
                        }
                    }
                };
                sumUnitList.Add(unit);
            });

            
            summary.unit = sumUnitList.ToArray();

            summary.total =  new repLrCbuSummaryTotal {
                name = "GRAND TOTAL",
                balances = new repLrCbuSummaryBalances {
                    loan = new repLrCbuSummaryBalancesLoan {
                        lrPrincipal = sumUnitList.Sum(x=>x.total.balances.loan.lrPrincipal),
                        eir = sumUnitList.Sum(x=>x.total.balances.loan.eir),
                        total = sumUnitList.Sum(x=>x.total.balances.loan.total)
                    },
                    par = new repLrCbuSummaryBalancesPar {
                        amount = sumUnitList.Sum(x=>x.total.balances.par.amount),
                        rate = (sumUnitList.Sum(x=>x.total.balances.par.amount) > 0) ? (sumUnitList.Sum(x=>x.total.balances.par.amount) / sumUnitList.Sum(x=>x.total.balances.loan.lrPrincipal)) * 100 : 0
                    },
                    cbu = sumUnitList.Sum(x=>x.total.balances.cbu)
                },
                centers = new repLrCbuSummaryCenters {
                    active = sumUnitList.Sum(x=>x.total.centers.active),
                    inActive = sumUnitList.Sum(x=>x.total.centers.inActive),
                    matured = sumUnitList.Sum(x=>x.total.centers.matured),
                    dissolved = 0,
                    total = sumUnitList.Sum(x=>x.total.centers.total)
                },
                clients = new repLrCbuSummaryClients {
                    active = new repLrCbuSummaryClientsActive {
                        withLoan = sumUnitList.Sum(x=>x.total.clients.active.withLoan),
                        pendingForReloan = sumUnitList.Sum(x=>x.total.clients.active.pendingForReloan)
                    },
                    inActive = new repLrCbuSummaryClientsInActive {
                        dormantClients = sumUnitList.Sum(x=>x.total.clients.inActive.dormantClients),
                        defaultClients = sumUnitList.Sum(x=>x.total.clients.inActive.defaultClients)
                    },
                    total = sumUnitList.Sum(x=>x.total.clients.total),
                    forTheMonth = new repLrCbuSummaryClientsForTheMonth {
                        newLoan = sumUnitList.Sum(x=>x.total.clients.forTheMonth.newLoan),
                        reloan = sumUnitList.Sum(x=>x.total.clients.forTheMonth.reloan)
                    },
                    returningClients = sumUnitList.Sum(x=>x.total.clients.returningClients),
                    resigned = new repLrCbuSummaryClientsResigned {
                        forTheMonth = sumUnitList.Sum(x=>x.total.clients.resigned.forTheMonth),
                        cumulative = sumUnitList.Sum(x=>x.total.clients.resigned.cumulative)
                    },
                    withLoan = new repLrCbuSummaryClientsWithLoan {
                        active = sumUnitList.Sum(x=>x.total.clients.withLoan.active),
                        matured = sumUnitList.Sum(x=>x.total.clients.withLoan.matured),
                        total = sumUnitList.Sum(x=>x.total.clients.withLoan.total),
                    },
                    withoutLoan = sumUnitList.Sum(x=>x.total.clients.withoutLoan)
                },
                loanDisbursement = new repLrCbuSummaryLoanDisbursement {
                    newLoan = sumUnitList.Sum(x=>x.total.loanDisbursement.newLoan),
                    reloanNotInLPR = sumUnitList.Sum(x=>x.total.loanDisbursement.reloanNotInLPR),
                    reloanLastPaymentRelease = sumUnitList.Sum(x=>x.total.loanDisbursement.reloanLastPaymentRelease),
                    totalCenterDisbursed = new repLrCbuSummaryTotalCenterDisbursed {
                        cumulative = sumUnitList.Sum(x=>x.total.loanDisbursement.totalCenterDisbursed.cumulative),
                        forTheMonth = sumUnitList.Sum(x=>x.total.loanDisbursement.totalCenterDisbursed.forTheMonth)
                    },
                    centerForTheMonth = new repLrCbuSummaryCenterForTheMonth {
                        newLoan = sumUnitList.Sum(x=>x.total.loanDisbursement.centerForTheMonth.newLoan),
                        reloan = sumUnitList.Sum(x=>x.total.loanDisbursement.centerForTheMonth.reloan)
                    },
                    amount = new repLrCbuSummaryAmount {
                        newLoan = sumUnitList.Sum(x=>x.total.loanDisbursement.amount.newLoan),
                        reloan = sumUnitList.Sum(x=>x.total.loanDisbursement.amount.reloan),
                        cumulative = sumUnitList.Sum(x=>x.total.loanDisbursement.amount.cumulative),
                        forTheMonth = sumUnitList.Sum(x=>x.total.loanDisbursement.amount.forTheMonth)
                    },
                }
            };
             
            var summarySigs = GetSignatories("lrcbu-summary",branch.slug,branch.units[0].slug,"");

            summary.preparedBy = summarySigs.preparedBy;
            summary.checkedBy = summarySigs.checkedBy;
            summary.approvedBy = summarySigs.approvedBy;
            summary.preparedByPosition = summarySigs.preparedByPosition;
            summary.checkedByPosition = summarySigs.checkedByPosition;
            summary.approvedByPosition = summarySigs.approvedByPosition;

            item.summary = summary;
            

            


            // ----------------------------------------------------------------------- //
            // end summary section
            
            if(branch!=null) {
                return item;
            } else {
                return null;
            }
        }


        public repGeneralLedgerSchedule generalLedgerSchedule(Int32 accountno, DateTime from, DateTime to)
        {
            var coaColl = _db.GetCollection<coa>("coa");
            var ap = coaColl.Find(new BsonDocument{{"code",accountno}}).FirstOrDefault();
            var codeList = new List<String>();
            var isParent = false;
            if(ap!=null) {
                // add codes of mother and subs
                isParent = true;
                codeList.Add(ap.code.ToString() + "|" + ap.name + "|" + ap.element);
                codeList.AddRange(ap.subsidiary.Select(x=>x.code.ToString() + "|" + x.name + "|" + ap.element).ToList());
            } else {
                var aps = coaColl.Find(new BsonDocument{{"subsidiary.code",accountno}}).FirstOrDefault();
                if(aps!=null) {
                    var s = aps.subsidiary.Where(x=>x.code == accountno).FirstOrDefault();
                    if(s!=null) {
                        codeList.Add(s.code.ToString() + "|" + s.name + "|" + aps.element);
                    }
                }
            }

            
            var sked = new repGeneralLedgerSchedule();

            if(codeList.Count > 0) {


                List<transactionDetailed> list = new List<transactionDetailed>(); 
                List<transactionDetailed> preList = new List<transactionDetailed>();
                if(isParent) {
                    list.AddRange(_acc.transactionDetailedList(from,to,Convert.ToInt32(codeList[0].Split("|")[0]),true));
                    preList.AddRange(_acc.transactionDetailedList(Convert.ToDateTime("1/1/0001"),from.AddDays(-1),Convert.ToInt32(codeList[0].Split("|")[0]),true));
                } else {
                    codeList.ForEach(c=>{
                        list.AddRange(_acc.transactionDetailedList(from,to,Convert.ToInt32(c.Split("|")[0]),true));
                        preList.AddRange(_acc.transactionDetailedList(Convert.ToDateTime("1/1/0001"),from.AddDays(-1),Convert.ToInt32(c.Split("|")[0]),true));
                    });
                }
                
                sked.accountNo = codeList[0].Split("|")[0];
                sked.accountName = codeList[0].Split("|")[1];
                sked.from = from;
                sked.to = to;
                sked.subsidiary = new List<repGeneralLedgerScheduleSubsidiary>(); 
                
                codeList.ForEach(c => {
                    var _code = Convert.ToInt32(c.Split("|")[0]);
                    var _name = c.Split("|")[1];
                    var _element = c.Split("|")[2];
                    var subsidiaryItem = new repGeneralLedgerScheduleSubsidiary();
                    subsidiaryItem.particulars = _name;
                    subsidiaryItem.previousEntryDebit = preList.Where(x=>x.code == _code && x.accountType == "debit").Sum(x=>x.amount);
                    subsidiaryItem.previousEntryCredit = preList.Where(x=>x.code == _code && x.accountType == "credit").Sum(x=>x.amount);
                    subsidiaryItem.previousEntry = subsidiaryItem.previousEntryDebit - subsidiaryItem.previousEntryCredit;
                    
                    subsidiaryItem.details = new List<repGeneralLedgerScheduleSubsidiaryPerMonth>();
                    
                    var f = Convert.ToDateTime(from.Month + "/1/" + from.Year);
                    var t = Convert.ToDateTime(to.Month + "/1/" + to.Year);

                    var runningBal = subsidiaryItem.previousEntry;

                    for(DateTime ctr = f;ctr<=t;ctr = ctr.AddMonths(1)) {
                        var subsidiaryMonth = new repGeneralLedgerScheduleSubsidiaryPerMonth();
                        subsidiaryMonth.month = ctr;
                        subsidiaryMonth.details = new List<repGeneralLedgerScheduleSubsidiaryDetails>();
                        list.Where(l => l.code == _code && (l.issuedAt >= ctr && l.issuedAt <= ctr.AddMonths(1).AddSeconds(-1))).ToList().ForEach(tl => 
                        {
                            var subsidiaryDetailItem = new repGeneralLedgerScheduleSubsidiaryDetails();
                            subsidiaryDetailItem.date = tl.issuedAt;
                            
                            var pref = "";
                            switch (tl.book)
                            {
                                case "CRB":
                                    pref = "CR#";
                                    pref = "CV#";
                                    break;
                                case "JV":
                                    pref = "JV#";
                                    break;
                            }
                            subsidiaryDetailItem.reference = pref + tl.number;
                            subsidiaryDetailItem.particulars = tl.particulars;
                            
                            Double[] amount = new Double[] { 0, 0 };

                            if(_element.ToLower() == "asset" || _element.ToLower() == "income") {
                                if (tl.accountType == "debit") {
                                    subsidiaryDetailItem.dr= tl.amount;
                                    runningBal += tl.amount;
                                } else if (tl.accountType == "credit") {
                                    subsidiaryDetailItem.cr= tl.amount;
                                    runningBal -= tl.amount;
                                }
                            } else {
                                if (tl.accountType == "debit") {
                                    subsidiaryDetailItem.dr= tl.amount;
                                    runningBal -= tl.amount;
                                } else if (tl.accountType == "credit") {
                                    subsidiaryDetailItem.cr= tl.amount;
                                    runningBal += tl.amount;
                                }
                            }

                            subsidiaryDetailItem.balance = runningBal;
                            subsidiaryMonth.details.Add(subsidiaryDetailItem);
                        });
                        //subsidiaryMonth.details = subsidiaryMonth.details.OrderBy(x=>x.date).ToList();
                        subsidiaryMonth.balance = runningBal;
                        subsidiaryItem.details.Add(subsidiaryMonth);
                    }

                    //subsidiaryItem.details = subsidiaryItem.details.OrderBy(x=>x.month).ToList();

                    subsidiaryItem.balance = runningBal;

                    sked.subsidiary.Add(subsidiaryItem);
                });

                
                return sked; 

            } else {
                return null;
            }
        }

        public repIncomeStatement incomeStatement2(DateTime asOf) {

            var _currentYear = asOf.Year;
            var _previousYear = asOf.Year - 1;

            List<transactionDetailed> previousDatalist = _acc.transactionDetailedList(Convert.ToDateTime("1/1/0001"), Convert.ToDateTime("12/31/" + _previousYear.ToString()), 0, true);
            List<transactionDetailed> currentDatalist = _acc.transactionDetailedList(Convert.ToDateTime("1/1/0001"), asOf,0,true);

            previousDatalist.ForEach(x=>{
                if(x.accountType.ToLower() == "credit")
                    x.amount *=-1;

                if(new []{"liabilites","fund balance","income"}.Contains(x.codeElement.ToLower()))
                    x.amount *= -1;
            });

            currentDatalist.ForEach(x=>{
                if(x.accountType.ToLower() == "credit")
                    x.amount *=-1;

                if(new []{"liabilites","fund balance","income"}.Contains(x.codeElement.ToLower()))
                    x.amount *= -1;
            });

            var _setting = _fsSetting2.Value.incomeStatement;

            var coaToIs = new Dictionary<String,Int32[]>();

            var _interestIncomeOnLoansReceivable = new repIncomeStatementLineItem();
            _interestIncomeOnLoansReceivable.name = _setting.interestIncomeOnLoansReceivable.name;
            _interestIncomeOnLoansReceivable.note = "";
            _interestIncomeOnLoansReceivable.currentYear = (decimal)currentDatalist.Where(x=>_setting.interestIncomeOnLoansReceivable.accounts.Contains(x.motherCode)).Sum(x=> (x.amount < 0) ? x.amount * -1 : x.amount);
            _interestIncomeOnLoansReceivable.previousYear = (decimal)previousDatalist.Where(x=>_setting.interestIncomeOnLoansReceivable.accounts.Contains(x.motherCode)).Sum(x=> (x.amount < 0) ? x.amount * -1 : x.amount);
            _interestIncomeOnLoansReceivable.increaseDecrease = _interestIncomeOnLoansReceivable.currentYear - _interestIncomeOnLoansReceivable.previousYear;
            _interestIncomeOnLoansReceivable.percentage =  (_interestIncomeOnLoansReceivable.previousYear == 0) ? 0 : Math.Round((_interestIncomeOnLoansReceivable.increaseDecrease) / _interestIncomeOnLoansReceivable.previousYear,2);
            coaToIs.Add(_setting.interestIncomeOnLoansReceivable.name,_setting.interestIncomeOnLoansReceivable.accounts);

            var _serviceIncome = new repIncomeStatementLineItem ();
            _serviceIncome.name = _setting.serviceIncome.name;
            _serviceIncome.note = "";
            _serviceIncome.currentYear = (decimal)currentDatalist.Where(x=>_setting.serviceIncome.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _serviceIncome.previousYear = (decimal)previousDatalist.Where(x=>_setting.serviceIncome.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _serviceIncome.increaseDecrease = _serviceIncome.currentYear - _serviceIncome.previousYear;
            _serviceIncome.percentage =  (_serviceIncome.previousYear == 0) ? 0 : Math.Round((_serviceIncome.increaseDecrease) / _serviceIncome.previousYear,2);
            coaToIs.Add(_setting.serviceIncome.name,_setting.serviceIncome.accounts);

            var _interestOnDepositAndPlacements = new repIncomeStatementLineItem();
            _interestOnDepositAndPlacements.name = _setting.interestOnDepositAndPlacements.name;
            _interestOnDepositAndPlacements.note = "";
            _interestOnDepositAndPlacements.currentYear = (decimal)currentDatalist.Where(x=>_setting.interestOnDepositAndPlacements.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _interestOnDepositAndPlacements.previousYear = (decimal)previousDatalist.Where(x=>_setting.interestOnDepositAndPlacements.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _interestOnDepositAndPlacements.increaseDecrease = _interestOnDepositAndPlacements.currentYear - _interestOnDepositAndPlacements.previousYear;
            _interestOnDepositAndPlacements.percentage =  (_interestOnDepositAndPlacements.previousYear == 0) ? 0 : Math.Round((_interestOnDepositAndPlacements.increaseDecrease) / _interestOnDepositAndPlacements.previousYear,2);
            coaToIs.Add(_setting.interestOnDepositAndPlacements.name,_setting.interestOnDepositAndPlacements.accounts);

            var _forexGainOrLoss = new repIncomeStatementLineItem ();
            _forexGainOrLoss.name = _setting.forexGainOrLoss.name;
            _forexGainOrLoss.note = "";
            _forexGainOrLoss.currentYear = (decimal)currentDatalist.Where(x=>_setting.forexGainOrLoss.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _forexGainOrLoss.previousYear = (decimal)previousDatalist.Where(x=>_setting.forexGainOrLoss.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _forexGainOrLoss.increaseDecrease = _forexGainOrLoss.currentYear - _forexGainOrLoss.previousYear;
            _forexGainOrLoss.percentage =  (_forexGainOrLoss.previousYear == 0) ? 0 : Math.Round((_forexGainOrLoss.increaseDecrease) / _forexGainOrLoss.previousYear,2);
            coaToIs.Add(_setting.forexGainOrLoss.name,_setting.forexGainOrLoss.accounts);

            var _otherIncome = new repIncomeStatementLineItem ();
            _otherIncome.name = _setting.otherIncome.name;
            _otherIncome.note = "";
            _otherIncome.currentYear = (decimal)currentDatalist.Where(x=>_setting.otherIncome.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _otherIncome.previousYear = (decimal)previousDatalist.Where(x=>_setting.otherIncome.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _otherIncome.increaseDecrease = _otherIncome.currentYear - _otherIncome.previousYear;
            _otherIncome.percentage =  (_otherIncome.previousYear == 0) ? 0 : Math.Round((_otherIncome.increaseDecrease) / _otherIncome.previousYear,2);
            coaToIs.Add(_setting.otherIncome.name,_setting.otherIncome.accounts);

            var _interestExpenseOnClientsCbu = new repIncomeStatementLineItem();
            _interestExpenseOnClientsCbu.name = _setting.interestExpenseOnClientsCbu.name;
            _interestExpenseOnClientsCbu.note = "";
            _interestExpenseOnClientsCbu.currentYear = (decimal)currentDatalist.Where(x=>_setting.interestExpenseOnClientsCbu.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _interestExpenseOnClientsCbu.previousYear = (decimal)previousDatalist.Where(x=>_setting.interestExpenseOnClientsCbu.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _interestExpenseOnClientsCbu.increaseDecrease = _interestExpenseOnClientsCbu.currentYear - _interestExpenseOnClientsCbu.previousYear;
            _interestExpenseOnClientsCbu.percentage =  (_interestExpenseOnClientsCbu.previousYear == 0) ? 0 : Math.Round((_interestExpenseOnClientsCbu.increaseDecrease) / _interestExpenseOnClientsCbu.previousYear,2);
            coaToIs.Add(_setting.interestExpenseOnClientsCbu.name,_setting.interestExpenseOnClientsCbu.accounts);

            var _badDebtsExpense = new repIncomeStatementLineItem ();
            _badDebtsExpense.name = _setting.badDebtsExpense.name;
            _badDebtsExpense.note = "";
            _badDebtsExpense.currentYear = (decimal)currentDatalist.Where(x=>_setting.badDebtsExpense.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _badDebtsExpense.previousYear = (decimal)previousDatalist.Where(x=>_setting.badDebtsExpense.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _badDebtsExpense.increaseDecrease = _badDebtsExpense.currentYear - _badDebtsExpense.previousYear;
            _badDebtsExpense.percentage =  (_badDebtsExpense.previousYear == 0) ? 0 : Math.Round((_badDebtsExpense.increaseDecrease) / _badDebtsExpense.previousYear,2);
            coaToIs.Add(_setting.badDebtsExpense.name,_setting.badDebtsExpense.accounts);

            var _totalGrossIncome = new repIncomeStatementLineItem {
                name = "  Total Gross Income",
                note = "",
                currentYear = 
                    _interestIncomeOnLoansReceivable.currentYear +
                    _serviceIncome.currentYear +
                    _interestOnDepositAndPlacements.currentYear +
                    _forexGainOrLoss.currentYear +
                    _otherIncome.currentYear,
                previousYear = 
                    _interestIncomeOnLoansReceivable.previousYear +
                    _serviceIncome.previousYear +
                    _interestOnDepositAndPlacements.previousYear +
                    _forexGainOrLoss.previousYear +
                    _otherIncome.previousYear
            };
            _totalGrossIncome.increaseDecrease = _totalGrossIncome.currentYear - _totalGrossIncome.previousYear;
            _totalGrossIncome.percentage =  (_totalGrossIncome.previousYear == 0) ? 0 : Math.Round((_totalGrossIncome.increaseDecrease) / _totalGrossIncome.previousYear,2);

            var _totalFinancialCost = new repIncomeStatementLineItem {
                name = "  Total Financial Cost",
                note = "",
                currentYear = 
                    _interestExpenseOnClientsCbu.currentYear,
                previousYear = 
                    _interestExpenseOnClientsCbu.previousYear
            };
            _totalFinancialCost.increaseDecrease = _totalFinancialCost.currentYear - _totalFinancialCost.previousYear;
            _totalFinancialCost.percentage =  (_totalFinancialCost.previousYear == 0) ? 0 : Math.Round((_totalFinancialCost.increaseDecrease) / _totalFinancialCost.previousYear,2);

            var _netMargin = new repIncomeStatementLineItem {
                name = "Net Margin",
                note = "",
                currentYear = 
                    _totalGrossIncome.currentYear -
                    _totalFinancialCost.currentYear -
                    _badDebtsExpense.currentYear,
                previousYear = 
                    _totalGrossIncome.previousYear -
                    _totalFinancialCost.previousYear -
                    _badDebtsExpense.previousYear
            };
            _netMargin.increaseDecrease = _netMargin.currentYear - _netMargin.previousYear;
            _netMargin.percentage =  (_netMargin.previousYear == 0) ? 0 : Math.Round((_netMargin.increaseDecrease) / _netMargin.previousYear,2);
            

        
            var _personnelCosts = new repIncomeStatementLineItem();
            _personnelCosts.name = _setting.personnelCosts.name;
            _personnelCosts.note = "";
            _personnelCosts.currentYear = (decimal)currentDatalist.Where(x=>_setting.personnelCosts.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _personnelCosts.previousYear = (decimal)previousDatalist.Where(x=>_setting.personnelCosts.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _personnelCosts.increaseDecrease = _personnelCosts.currentYear - _personnelCosts.previousYear;
            _personnelCosts.percentage =  (_personnelCosts.previousYear == 0) ? 0 : Math.Round((_personnelCosts.increaseDecrease) / _personnelCosts.previousYear,2);
            coaToIs.Add(_setting.personnelCosts.name,_setting.personnelCosts.accounts);

            var _outstation = new repIncomeStatementLineItem();
            _outstation.name = _setting.outstation.name;
            _outstation.note = "";
            _outstation.currentYear = (decimal)currentDatalist.Where(x=>_setting.outstation.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _outstation.previousYear = (decimal)previousDatalist.Where(x=>_setting.outstation.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _outstation.increaseDecrease = _outstation.currentYear - _outstation.previousYear;
            _outstation.percentage =  (_outstation.previousYear == 0) ? 0 : Math.Round((_outstation.increaseDecrease) / _outstation.previousYear,2);
            coaToIs.Add(_setting.outstation.name,_setting.outstation.accounts);
            
            var _fringeBenefit = new repIncomeStatementLineItem();
            _fringeBenefit.name = _setting.fringeBenefit.name;
            _fringeBenefit.note = "";
            _fringeBenefit.currentYear = (decimal)currentDatalist.Where(x=>_setting.fringeBenefit.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _fringeBenefit.previousYear = (decimal)previousDatalist.Where(x=>_setting.fringeBenefit.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _fringeBenefit.increaseDecrease = _fringeBenefit.currentYear - _fringeBenefit.previousYear;
            _fringeBenefit.percentage =  (_fringeBenefit.previousYear == 0) ? 0 : Math.Round((_fringeBenefit.increaseDecrease) / _fringeBenefit.previousYear,2);
            coaToIs.Add(_setting.fringeBenefit.name,_setting.fringeBenefit.accounts);

            var _retirement = new repIncomeStatementLineItem();
            _retirement.name = _setting.retirement.name;
            _retirement.note = "";
            _retirement.currentYear = (decimal)currentDatalist.Where(x=>_setting.retirement.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _retirement.previousYear = (decimal)previousDatalist.Where(x=>_setting.retirement.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _retirement.increaseDecrease = _retirement.currentYear - _retirement.previousYear;
            _retirement.percentage =  (_retirement.previousYear == 0) ? 0 : Math.Round((_retirement.increaseDecrease) / _retirement.previousYear,2);
            coaToIs.Add(_setting.retirement.name,_setting.retirement.accounts);

            var _outsideServices = new repIncomeStatementLineItem();
            _outsideServices.name = _setting.outsideServices.name;
            _outsideServices.note = "";
            _outsideServices.currentYear = (decimal)currentDatalist.Where(x=>_setting.outsideServices.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _outsideServices.previousYear = (decimal)previousDatalist.Where(x=>_setting.outsideServices.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _outsideServices.increaseDecrease = _outsideServices.currentYear - _outsideServices.previousYear;
            _outsideServices.percentage =  (_outsideServices.previousYear == 0) ? 0 : Math.Round((_outsideServices.increaseDecrease) / _outsideServices.previousYear,2);
            coaToIs.Add(_setting.outsideServices.name,_setting.outsideServices.accounts);
            
            var _professionalFees = new repIncomeStatementLineItem();
            _professionalFees.name = _setting.professionalFees.name;
            _professionalFees.note = "";
            _professionalFees.currentYear = (decimal)currentDatalist.Where(x=>_setting.professionalFees.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _professionalFees.previousYear = (decimal)previousDatalist.Where(x=>_setting.professionalFees.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _professionalFees.increaseDecrease = _professionalFees.currentYear - _professionalFees.previousYear;
            _professionalFees.percentage =  (_professionalFees.previousYear == 0) ? 0 : Math.Round((_professionalFees.increaseDecrease) / _professionalFees.previousYear,2);
            coaToIs.Add(_setting.professionalFees.name,_setting.professionalFees.accounts);

            var _meetingsTrainingsAndConferences = new repIncomeStatementLineItem();
            _meetingsTrainingsAndConferences.name = _setting.meetingsTrainingsAndConferences.name;
            _meetingsTrainingsAndConferences.note = "";
            _meetingsTrainingsAndConferences.currentYear = (decimal)currentDatalist.Where(x=>_setting.meetingsTrainingsAndConferences.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _meetingsTrainingsAndConferences.previousYear = (decimal)previousDatalist.Where(x=>_setting.meetingsTrainingsAndConferences.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _meetingsTrainingsAndConferences.increaseDecrease = _meetingsTrainingsAndConferences.currentYear - _meetingsTrainingsAndConferences.previousYear;
            _meetingsTrainingsAndConferences.percentage =  (_meetingsTrainingsAndConferences.previousYear == 0) ? 0 : Math.Round((_meetingsTrainingsAndConferences.increaseDecrease) / _meetingsTrainingsAndConferences.previousYear,2);
            coaToIs.Add(_setting.meetingsTrainingsAndConferences.name,_setting.meetingsTrainingsAndConferences.accounts);
            
            var _transformationExpense = new repIncomeStatementLineItem();
            _transformationExpense.name = _setting.transformationExpense.name;
            _transformationExpense.note = "";
            _transformationExpense.currentYear = (decimal)currentDatalist.Where(x=>_setting.transformationExpense.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _transformationExpense.previousYear = (decimal)previousDatalist.Where(x=>_setting.transformationExpense.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _transformationExpense.increaseDecrease = _transformationExpense.currentYear - _transformationExpense.previousYear;
            _transformationExpense.percentage =  (_transformationExpense.previousYear == 0) ? 0 : Math.Round((_transformationExpense.increaseDecrease) / _transformationExpense.previousYear,2);
            coaToIs.Add(_setting.transformationExpense.name,_setting.transformationExpense.accounts);

            var _litigation = new repIncomeStatementLineItem();
            _litigation.name = _setting.litigation.name;
            _litigation.note = "";
            _litigation.currentYear = (decimal)currentDatalist.Where(x=>_setting.litigation.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _litigation.previousYear = (decimal)previousDatalist.Where(x=>_setting.litigation.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _litigation.increaseDecrease = _litigation.currentYear - _litigation.previousYear;
            _litigation.percentage =  (_litigation.previousYear == 0) ? 0 : Math.Round((_litigation.increaseDecrease) / _litigation.previousYear,2);
            coaToIs.Add(_setting.litigation.name,_setting.litigation.accounts);

            var _supplies = new repIncomeStatementLineItem();
            _supplies.name = _setting.supplies.name;
            _supplies.note = "";
            _supplies.currentYear = (decimal)currentDatalist.Where(x=>_setting.supplies.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _supplies.previousYear = (decimal)previousDatalist.Where(x=>_setting.supplies.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _supplies.increaseDecrease = _supplies.currentYear - _supplies.previousYear;
            _supplies.percentage =  (_supplies.previousYear == 0) ? 0 : Math.Round((_supplies.increaseDecrease) / _supplies.previousYear,2);
            coaToIs.Add(_setting.supplies.name,_setting.supplies.accounts);

            var _utilities = new repIncomeStatementLineItem();
            _utilities.name = _setting.utilities.name;
            _utilities.note = "";
            _utilities.currentYear = (decimal)currentDatalist.Where(x=>_setting.utilities.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _utilities.previousYear = (decimal)previousDatalist.Where(x=>_setting.utilities.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _utilities.increaseDecrease = _utilities.currentYear - _utilities.previousYear;
            _utilities.percentage =  (_utilities.previousYear == 0) ? 0 : Math.Round((_utilities.increaseDecrease) / _utilities.previousYear,2);
            coaToIs.Add(_setting.utilities.name,_setting.utilities.accounts);

            var _communication = new repIncomeStatementLineItem();
            _communication.name = _setting.communication.name;
            _communication.note = "";
            _communication.currentYear = (decimal)currentDatalist.Where(x=>_setting.communication.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _communication.previousYear = (decimal)previousDatalist.Where(x=>_setting.communication.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _communication.increaseDecrease = _communication.currentYear - _communication.previousYear;
            _communication.percentage =  (_communication.previousYear == 0) ? 0 : Math.Round((_communication.increaseDecrease) / _communication.previousYear,2);
            coaToIs.Add(_setting.communication.name,_setting.communication.accounts);

            var _courierCharges = new repIncomeStatementLineItem();
            _courierCharges.name = _setting.courierCharges.name;
            _courierCharges.note = "";
            _courierCharges.currentYear = (decimal)currentDatalist.Where(x=>_setting.courierCharges.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _courierCharges.previousYear = (decimal)previousDatalist.Where(x=>_setting.courierCharges.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _courierCharges.increaseDecrease = _courierCharges.currentYear - _courierCharges.previousYear;
            _courierCharges.percentage =  (_courierCharges.previousYear == 0) ? 0 : Math.Round((_courierCharges.increaseDecrease) / _courierCharges.previousYear,2);
            coaToIs.Add(_setting.courierCharges.name,_setting.courierCharges.accounts);

            var _rental = new repIncomeStatementLineItem();
            _rental.name = _setting.rental.name;
            _rental.note = "";
            _rental.currentYear = (decimal)currentDatalist.Where(x=>_setting.rental.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _rental.previousYear = (decimal)previousDatalist.Where(x=>_setting.rental.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _rental.increaseDecrease = _rental.currentYear - _rental.previousYear;
            _rental.percentage =  (_rental.previousYear == 0) ? 0 : Math.Round((_rental.increaseDecrease) / _rental.previousYear,2);
            coaToIs.Add(_setting.rental.name,_setting.rental.accounts);

            var _insurance = new repIncomeStatementLineItem();
            _insurance.name = _setting.insurance.name;
            _insurance.note = "";
            _insurance.currentYear = (decimal)currentDatalist.Where(x=>_setting.insurance.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _insurance.previousYear = (decimal)previousDatalist.Where(x=>_setting.insurance.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _insurance.increaseDecrease = _insurance.currentYear - _insurance.previousYear;
            _insurance.percentage =  (_insurance.previousYear == 0) ? 0 : Math.Round((_insurance.increaseDecrease) / _insurance.previousYear,2);
            coaToIs.Add(_setting.insurance.name,_setting.insurance.accounts);

            var _transporationAndTravel = new repIncomeStatementLineItem();
            _transporationAndTravel.name = _setting.transporationAndTravel.name;
            _transporationAndTravel.note = "";
            _transporationAndTravel.currentYear = (decimal)currentDatalist.Where(x=>_setting.transporationAndTravel.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _transporationAndTravel.previousYear = (decimal)previousDatalist.Where(x=>_setting.transporationAndTravel.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _transporationAndTravel.increaseDecrease = _transporationAndTravel.currentYear - _transporationAndTravel.previousYear;
            _transporationAndTravel.percentage =  (_transporationAndTravel.previousYear == 0) ? 0 : Math.Round((_transporationAndTravel.increaseDecrease) / _transporationAndTravel.previousYear,2);
            coaToIs.Add(_setting.transporationAndTravel.name,_setting.transporationAndTravel.accounts);

            var _taxesAndLicenses = new repIncomeStatementLineItem();
            _taxesAndLicenses.name = _setting.taxesAndLicenses.name;
            _taxesAndLicenses.note = "";
            _taxesAndLicenses.currentYear = (decimal)currentDatalist.Where(x=>_setting.taxesAndLicenses.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _taxesAndLicenses.previousYear = (decimal)previousDatalist.Where(x=>_setting.taxesAndLicenses.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _taxesAndLicenses.increaseDecrease = _taxesAndLicenses.currentYear - _taxesAndLicenses.previousYear;
            _taxesAndLicenses.percentage =  (_taxesAndLicenses.previousYear == 0) ? 0 : Math.Round((_taxesAndLicenses.increaseDecrease) / _taxesAndLicenses.previousYear,2);
            coaToIs.Add(_setting.taxesAndLicenses.name,_setting.taxesAndLicenses.accounts);

            var _repairsAndMaintenance = new repIncomeStatementLineItem();
            _repairsAndMaintenance.name = _setting.repairsAndMaintenance.name;
            _repairsAndMaintenance.note = "";
            _repairsAndMaintenance.currentYear = (decimal)currentDatalist.Where(x=>_setting.repairsAndMaintenance.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _repairsAndMaintenance.previousYear = (decimal)previousDatalist.Where(x=>_setting.repairsAndMaintenance.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _repairsAndMaintenance.increaseDecrease = _repairsAndMaintenance.currentYear - _repairsAndMaintenance.previousYear;
            _repairsAndMaintenance.percentage =  (_repairsAndMaintenance.previousYear == 0) ? 0 : Math.Round((_repairsAndMaintenance.increaseDecrease) / _repairsAndMaintenance.previousYear,2);
            coaToIs.Add(_setting.repairsAndMaintenance.name,_setting.repairsAndMaintenance.accounts);
            
            var _depreciationAndAmortization = new repIncomeStatementLineItem();
            _depreciationAndAmortization.name = _setting.depreciationAndAmortization.name;
            _depreciationAndAmortization.note = "";
            _depreciationAndAmortization.currentYear = (decimal)currentDatalist.Where(x=>_setting.depreciationAndAmortization.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _depreciationAndAmortization.previousYear = (decimal)previousDatalist.Where(x=>_setting.depreciationAndAmortization.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _depreciationAndAmortization.increaseDecrease = _depreciationAndAmortization.currentYear - _depreciationAndAmortization.previousYear;
            _depreciationAndAmortization.percentage =  (_depreciationAndAmortization.previousYear == 0) ? 0 : Math.Round((_depreciationAndAmortization.increaseDecrease) / _depreciationAndAmortization.previousYear,2);
            coaToIs.Add(_setting.depreciationAndAmortization.name,_setting.depreciationAndAmortization.accounts);

            var _lossOnAssetWriteOff = new repIncomeStatementLineItem();
            _lossOnAssetWriteOff.name = _setting.lossOnAssetWriteOff.name;
            _lossOnAssetWriteOff.note = "";
            _lossOnAssetWriteOff.currentYear = (decimal)currentDatalist.Where(x=>_setting.lossOnAssetWriteOff.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _lossOnAssetWriteOff.previousYear = (decimal)previousDatalist.Where(x=>_setting.lossOnAssetWriteOff.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _lossOnAssetWriteOff.increaseDecrease = _lossOnAssetWriteOff.currentYear - _lossOnAssetWriteOff.previousYear;
            _lossOnAssetWriteOff.percentage =  (_lossOnAssetWriteOff.previousYear == 0) ? 0 : Math.Round((_lossOnAssetWriteOff.increaseDecrease) / _lossOnAssetWriteOff.previousYear,2);
            coaToIs.Add(_setting.lossOnAssetWriteOff.name,_setting.lossOnAssetWriteOff.accounts);

            var _others = new repIncomeStatementLineItem();
            _others.name = _setting.others.name;
            _others.note = "";
            _others.currentYear = (decimal)currentDatalist.Where(x=>_setting.others.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _others.previousYear = (decimal)previousDatalist.Where(x=>_setting.others.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _others.increaseDecrease = _others.currentYear - _others.previousYear;
            _others.percentage =  (_others.previousYear == 0) ? 0 : Math.Round((_others.increaseDecrease) / _others.previousYear,2);
            coaToIs.Add(_setting.others.name,_setting.others.accounts);
            
            var _totalExpenses = new repIncomeStatementLineItem {
                name = "  Total Expenses",
                note = "",
                currentYear = 
                    _personnelCosts.currentYear + 
                    _outstation.currentYear +
                    _fringeBenefit.currentYear +
                    _retirement.currentYear +
                    _outsideServices.currentYear +
                    _professionalFees.currentYear +
                    _meetingsTrainingsAndConferences.currentYear +
                    _transformationExpense.currentYear +
                    _litigation.currentYear +
                    _supplies.currentYear +
                    _utilities.currentYear +
                    _communication.currentYear +
                    _courierCharges.currentYear +
                    _rental.currentYear +
                    _insurance.currentYear +
                    _transporationAndTravel.currentYear +
                    _taxesAndLicenses.currentYear +
                    _repairsAndMaintenance.currentYear +
                    _depreciationAndAmortization.currentYear +
                    _lossOnAssetWriteOff.currentYear +
                    _others.currentYear,
                previousYear = 
                    _personnelCosts.previousYear +
                    _outstation.previousYear +
                    _fringeBenefit.previousYear +
                    _retirement.previousYear +
                    _outsideServices.previousYear +
                    _professionalFees.previousYear +
                    _meetingsTrainingsAndConferences.previousYear +
                    _transformationExpense.previousYear +
                    _litigation.previousYear +
                    _supplies.previousYear +
                    _utilities.previousYear +
                    _communication.previousYear +
                    _courierCharges.previousYear +
                    _rental.previousYear +
                    _insurance.previousYear +
                    _transporationAndTravel.previousYear +
                    _taxesAndLicenses.previousYear +
                    _repairsAndMaintenance.previousYear +
                    _depreciationAndAmortization.previousYear +
                    _lossOnAssetWriteOff.previousYear +
                    _others.previousYear
            };
            _totalExpenses.increaseDecrease = _totalExpenses.currentYear - _totalExpenses.previousYear;
            _totalExpenses.percentage =  (_totalExpenses.previousYear == 0) ? 0 : Math.Round((_totalExpenses.increaseDecrease) / _totalExpenses.previousYear,2);
            
            var _incomeLossBeforeTax = new repIncomeStatementLineItem {
                name = "INCOME (LOSS) BEFORE TAX",
                note = "",
                currentYear = 
                    _netMargin.currentYear -
                    _totalExpenses.currentYear,
                previousYear = 
                    _netMargin.previousYear -
                    _totalExpenses.previousYear,
            };
            _incomeLossBeforeTax.increaseDecrease = _incomeLossBeforeTax.currentYear - _incomeLossBeforeTax.previousYear;
            _incomeLossBeforeTax.percentage =  (_incomeLossBeforeTax.previousYear == 0) ? 0 : Math.Round((_incomeLossBeforeTax.increaseDecrease) / _incomeLossBeforeTax.previousYear,2);

            var _incomeTaxExpense = new repIncomeStatementLineItem();
            _incomeTaxExpense.name = _setting.incomeTaxExpense.name;
            _incomeTaxExpense.note = "";
            _incomeTaxExpense.currentYear = (decimal)currentDatalist.Where(x=>_setting.incomeTaxExpense.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _incomeTaxExpense.previousYear = (decimal)previousDatalist.Where(x=>_setting.incomeTaxExpense.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _incomeTaxExpense.increaseDecrease = _incomeTaxExpense.currentYear - _incomeTaxExpense.previousYear;
            _incomeTaxExpense.percentage =  (_incomeTaxExpense.previousYear == 0) ? 0 : Math.Round((_incomeTaxExpense.increaseDecrease) / _incomeTaxExpense.previousYear,2);
            coaToIs.Add(_setting.incomeTaxExpense.name,_setting.incomeTaxExpense.accounts);

            var _netIncomeLoss = new repIncomeStatementLineItem {
                name = "NET INCOME (LOSS)",
                note = "",
                currentYear = 
                    _incomeLossBeforeTax.currentYear -
                    _incomeTaxExpense.currentYear,
                previousYear = 
                    _incomeLossBeforeTax.previousYear -
                    _incomeTaxExpense.previousYear,
            };
            _netIncomeLoss.increaseDecrease = _netIncomeLoss.currentYear - _netIncomeLoss.previousYear;
            _netIncomeLoss.percentage =  (_netIncomeLoss.previousYear == 0) ? 0 : Math.Round((_netIncomeLoss.increaseDecrease) / _netIncomeLoss.previousYear,2);

            var _details = new List<transactionDetailedIs>();
            _details.AddRange(currentDatalist.Select(x=>new transactionDetailedIs {
                slug = x.slug,
                book = x.book,
                number = x.number,
                particulars = x.particulars,
                accountType = x.accountType,
                motherCode = x.motherCode,
                code = x.code,
                codeElement = x.codeElement,
                name = x.name,
                amount = x.amount, // (x.accountType == "credit" ) ?  x.amount * -1 : x.amount,
                issuedAt = x.issuedAt,
                isItem = coaToIs.ToList().Where(y=>y.Value.Contains(x.motherCode)).Select(z=>z.Key).FirstOrDefault()
            }));

            // _details.AddRange(previousDatalist.Select(x=>new transactionDetailedIs {
            //     slug = x.slug,
            //     book = x.book,
            //     number = x.number,
            //     particulars = x.particulars,
            //     accountType = x.accountType,
            //     motherCode = x.motherCode,
            //     code = x.code,
            //     codeElement = x.codeElement,
            //     name = x.name,
            //     amount = (x.accountType == "credit" ) ?  x.amount * -1 : x.amount,
            //     issuedAt = x.issuedAt,
            //     isItem = coaToIs.ToList().Where(y=>y.Value.Contains(x.motherCode)).Select(z=>z.Key).FirstOrDefault()
            // }));
            //populate
            var _is = new repIncomeStatement {
                currentYear = _currentYear,
                previousYear = _previousYear,
                asOf = asOf,
                revenue = new repIncomeStatementRevenue {
                    interestIncomeOnLoansReceivable = _interestIncomeOnLoansReceivable,
                    serviceIncome = _serviceIncome,
                    interestOnDepositAndPlacements = _interestOnDepositAndPlacements,
                    forexGainOrLoss = _forexGainOrLoss,
                    otherIncome = _otherIncome,
                    totalGrossIncome = _totalGrossIncome,
                    interestExpenseOnClientsCbu = _interestExpenseOnClientsCbu,
                    totalFinancialCost = _totalFinancialCost,
                    badDebtsExpense = _badDebtsExpense,
                    netMargin = _netMargin
                },
                expenses = new repIncomeStatementExpenses {
                    personnelCosts = _personnelCosts,
                    outstation = _outstation,
                    fringeBenefit = _fringeBenefit,
                    retirement = _retirement,
                    outsideServices = _outsideServices,
                    professionalFees = _professionalFees,
                    meetingsTrainingsAndConferences = _meetingsTrainingsAndConferences,
                    transformationExpense = _transformationExpense,
                    litigation = _litigation,
                    supplies = _supplies,
                    utilities = _utilities,
                    communication = _communication,
                    courierCharges = _courierCharges,
                    rental = _rental,
                    insurance = _insurance,
                    transporationAndTravel = _transporationAndTravel,
                    taxesAndLicenses = _taxesAndLicenses,
                    repairsAndMaintenance = _repairsAndMaintenance,
                    depreciationAndAmortization = _depreciationAndAmortization,
                    lossOnAssetWriteOff = _lossOnAssetWriteOff,
                    others = _others,
                    totalExpenses = _totalExpenses
                },
                netIncomeLossBeforeTax = _incomeLossBeforeTax,
                incomeTaxExpense = _incomeTaxExpense,
                netIncomeLoss = _netIncomeLoss,
                details = _details
            };

            return _is;
        }

        public repBalanceSheet balanceSheet2(DateTime asOf) {
            var _currentYear = asOf.Year;
            var _previousYear = asOf.Year - 1;

            List<transactionDetailed> previousDatalist = _acc.transactionDetailedList(Convert.ToDateTime("1/1/0001"), Convert.ToDateTime("12/31/" + _previousYear.ToString()), 0, true);
            List<transactionDetailed> currentDatalist = _acc.transactionDetailedList((Convert.ToDateTime("1/1/0001")), asOf,0,true);

            previousDatalist.ForEach(x=>{
                if(x.accountType.ToLower() == "credit")
                    x.amount *=-1;

                if(new []{"liabilites","fund balance","income"}.Contains(x.codeElement.ToLower()))
                    x.amount *= -1;
            });

            currentDatalist.ForEach(x=>{
                if(x.accountType.ToLower() == "credit")
                    x.amount *=-1;

                if(new []{"liabilites","fund balance","income"}.Contains(x.codeElement.ToLower()))
                    x.amount *= -1;
            });

            var _setting = _fsSetting2.Value.balanceSheet;

            var coaToIs = new Dictionary<String,Int32[]>();

            var _cashAndCashEquivalents = new repIncomeStatementLineItem();
            _cashAndCashEquivalents.name = _setting.cashAndCashEquivalents.name;
            _cashAndCashEquivalents.note = "";
            _cashAndCashEquivalents.currentYear = (decimal)currentDatalist.Where(x=>_setting.cashAndCashEquivalents.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _cashAndCashEquivalents.previousYear = (decimal)previousDatalist.Where(x=>_setting.cashAndCashEquivalents.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _cashAndCashEquivalents.increaseDecrease = _cashAndCashEquivalents.currentYear - _cashAndCashEquivalents.previousYear;
            _cashAndCashEquivalents.percentage =  (_cashAndCashEquivalents.previousYear == 0) ? 0 : Math.Round((_cashAndCashEquivalents.increaseDecrease) / _cashAndCashEquivalents.previousYear,2);
            coaToIs.Add(_setting.cashAndCashEquivalents.name,_setting.cashAndCashEquivalents.accounts);

            var _marketableEquitySecuritiesNet = new repIncomeStatementLineItem();
            _marketableEquitySecuritiesNet.name = _setting.marketableEquitySecuritiesNet.name;
            _marketableEquitySecuritiesNet.note = "";
            _marketableEquitySecuritiesNet.currentYear = (decimal)currentDatalist.Where(x=>_setting.marketableEquitySecuritiesNet.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _marketableEquitySecuritiesNet.previousYear = (decimal)previousDatalist.Where(x=>_setting.marketableEquitySecuritiesNet.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _marketableEquitySecuritiesNet.increaseDecrease = _marketableEquitySecuritiesNet.currentYear - _marketableEquitySecuritiesNet.previousYear;
            _marketableEquitySecuritiesNet.percentage =  (_marketableEquitySecuritiesNet.previousYear == 0) ? 0 : Math.Round((_marketableEquitySecuritiesNet.increaseDecrease) / _marketableEquitySecuritiesNet.previousYear,2);
            coaToIs.Add(_setting.marketableEquitySecuritiesNet.name,_setting.marketableEquitySecuritiesNet.accounts);

            var _loansReceivableNet = new repIncomeStatementLineItem();
            _loansReceivableNet.name = _setting.loansReceivableNet.name;
            _loansReceivableNet.note = "";
            _loansReceivableNet.currentYear = (decimal)currentDatalist.Where(x=>_setting.loansReceivableNet.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _loansReceivableNet.previousYear = (decimal)previousDatalist.Where(x=>_setting.loansReceivableNet.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _loansReceivableNet.increaseDecrease = _loansReceivableNet.currentYear - _loansReceivableNet.previousYear;
            _loansReceivableNet.percentage =  (_loansReceivableNet.previousYear == 0) ? 0 : Math.Round((_loansReceivableNet.increaseDecrease) / _loansReceivableNet.previousYear,2);
            coaToIs.Add(_setting.loansReceivableNet.name,_setting.loansReceivableNet.accounts);

            var _interestReceivable = new repIncomeStatementLineItem();
            _interestReceivable.name = _setting.interestReceivable.name;
            _interestReceivable.note = "";
            _interestReceivable.currentYear = (decimal)currentDatalist.Where(x=>_setting.interestReceivable.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _interestReceivable.previousYear = (decimal)previousDatalist.Where(x=>_setting.interestReceivable.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _interestReceivable.increaseDecrease = _interestReceivable.currentYear - _interestReceivable.previousYear;
            _interestReceivable.percentage =  (_interestReceivable.previousYear == 0) ? 0 : Math.Round((_interestReceivable.increaseDecrease) / _interestReceivable.previousYear,2);
            coaToIs.Add(_setting.interestReceivable.name,_setting.interestReceivable.accounts);

            var _otherReceivablesNet = new repIncomeStatementLineItem();
            _otherReceivablesNet.name = _setting.otherReceivablesNet.name;
            _otherReceivablesNet.note = "";
            _otherReceivablesNet.currentYear = (decimal)currentDatalist.Where(x=>_setting.otherReceivablesNet.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _otherReceivablesNet.previousYear = (decimal)previousDatalist.Where(x=>_setting.otherReceivablesNet.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _otherReceivablesNet.increaseDecrease = _otherReceivablesNet.currentYear - _otherReceivablesNet.previousYear;
            _otherReceivablesNet.percentage =  (_otherReceivablesNet.previousYear == 0) ? 0 : Math.Round((_otherReceivablesNet.increaseDecrease) / _otherReceivablesNet.previousYear,2);
            coaToIs.Add(_setting.otherReceivablesNet.name,_setting.otherReceivablesNet.accounts);

            var _otherCurrentAssets = new repIncomeStatementLineItem();
            _otherCurrentAssets.name = _setting.otherCurrentAssets.name;
            _otherCurrentAssets.note = "";
            _otherCurrentAssets.currentYear = (decimal)currentDatalist.Where(x=>_setting.otherCurrentAssets.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _otherCurrentAssets.previousYear = (decimal)previousDatalist.Where(x=>_setting.otherCurrentAssets.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _otherCurrentAssets.increaseDecrease = _otherCurrentAssets.currentYear - _otherCurrentAssets.previousYear;
            _otherCurrentAssets.percentage =  (_otherCurrentAssets.previousYear == 0) ? 0 : Math.Round((_otherCurrentAssets.increaseDecrease) / _otherCurrentAssets.previousYear,2);
            coaToIs.Add(_setting.otherCurrentAssets.name,_setting.otherCurrentAssets.accounts);

            var _totalCurrentAssets = new repIncomeStatementLineItem {
                name = "  Total Current Assets",
                note = "",
                currentYear = 
                    _cashAndCashEquivalents.currentYear +
                    _marketableEquitySecuritiesNet.currentYear +
                    _loansReceivableNet.currentYear +
                    _interestReceivable.currentYear +
                    _otherReceivablesNet.currentYear +
                    _otherCurrentAssets.currentYear,
                previousYear = 
                    _cashAndCashEquivalents.previousYear +
                    _marketableEquitySecuritiesNet.previousYear +
                    _loansReceivableNet.previousYear +
                    _interestReceivable.previousYear +
                    _otherReceivablesNet.previousYear +
                    _otherCurrentAssets.previousYear,
            };
            _totalCurrentAssets.increaseDecrease = _totalCurrentAssets.currentYear - _totalCurrentAssets.previousYear;
            _totalCurrentAssets.percentage =  (_totalCurrentAssets.previousYear == 0) ? 0 : Math.Round((_totalCurrentAssets.increaseDecrease) / _totalCurrentAssets.previousYear,2);

            var _availableForSaleAfsFinancialAsset = new repIncomeStatementLineItem();
            _availableForSaleAfsFinancialAsset.name = _setting.availableForSaleAfsFinancialAsset.name;
            _availableForSaleAfsFinancialAsset.note = "";
            _availableForSaleAfsFinancialAsset.currentYear = (decimal)currentDatalist.Where(x=>_setting.availableForSaleAfsFinancialAsset.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _availableForSaleAfsFinancialAsset.previousYear = (decimal)previousDatalist.Where(x=>_setting.availableForSaleAfsFinancialAsset.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _availableForSaleAfsFinancialAsset.increaseDecrease = _availableForSaleAfsFinancialAsset.currentYear - _availableForSaleAfsFinancialAsset.previousYear;
            _availableForSaleAfsFinancialAsset.percentage =  (_availableForSaleAfsFinancialAsset.previousYear == 0) ? 0 : Math.Round((_availableForSaleAfsFinancialAsset.increaseDecrease) / _availableForSaleAfsFinancialAsset.previousYear,2);
            coaToIs.Add(_setting.availableForSaleAfsFinancialAsset.name,_setting.availableForSaleAfsFinancialAsset.accounts);

            var _restrictedCash = new repIncomeStatementLineItem();
            _restrictedCash.name = _setting.restrictedCash.name;
            _restrictedCash.note = "";
            _restrictedCash.currentYear = (decimal)currentDatalist.Where(x=>_setting.restrictedCash.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _restrictedCash.previousYear = (decimal)previousDatalist.Where(x=>_setting.restrictedCash.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _restrictedCash.increaseDecrease = _restrictedCash.currentYear - _restrictedCash.previousYear;
            _restrictedCash.percentage =  (_restrictedCash.previousYear == 0) ? 0 : Math.Round((_restrictedCash.increaseDecrease) / _restrictedCash.previousYear,2);
            coaToIs.Add(_setting.restrictedCash.name,_setting.restrictedCash.accounts);

            var _propertyAndEquipment = new repIncomeStatementLineItem();
            _propertyAndEquipment.name = _setting.propertyAndEquipment.name;
            _propertyAndEquipment.note = "";
            _propertyAndEquipment.currentYear = (decimal)currentDatalist.Where(x=>_setting.propertyAndEquipment.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _propertyAndEquipment.previousYear = (decimal)previousDatalist.Where(x=>_setting.propertyAndEquipment.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _propertyAndEquipment.increaseDecrease = _propertyAndEquipment.currentYear - _propertyAndEquipment.previousYear;
            _propertyAndEquipment.percentage =  (_propertyAndEquipment.previousYear == 0) ? 0 : Math.Round((_propertyAndEquipment.increaseDecrease) / _propertyAndEquipment.previousYear,2);
            coaToIs.Add(_setting.propertyAndEquipment.name,_setting.propertyAndEquipment.accounts);

            var _advancesToAffiliates = new repIncomeStatementLineItem();
            _advancesToAffiliates.name = _setting.advancesToAffiliates.name;
            _advancesToAffiliates.note = "";
            _advancesToAffiliates.currentYear = (decimal)currentDatalist.Where(x=>_setting.advancesToAffiliates.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _advancesToAffiliates.previousYear = (decimal)previousDatalist.Where(x=>_setting.advancesToAffiliates.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _advancesToAffiliates.increaseDecrease = _advancesToAffiliates.currentYear - _advancesToAffiliates.previousYear;
            _advancesToAffiliates.percentage =  (_advancesToAffiliates.previousYear == 0) ? 0 : Math.Round((_advancesToAffiliates.increaseDecrease) / _advancesToAffiliates.previousYear,2);
            coaToIs.Add(_setting.advancesToAffiliates.name,_setting.advancesToAffiliates.accounts);

            var _otherNoncurrentAssets = new repIncomeStatementLineItem();
            _otherNoncurrentAssets.name = _setting.otherNoncurrentAssets.name;
            _otherNoncurrentAssets.note = "";
            _otherNoncurrentAssets.currentYear = (decimal)currentDatalist.Where(x=>_setting.otherNoncurrentAssets.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _otherNoncurrentAssets.previousYear = (decimal)previousDatalist.Where(x=>_setting.otherNoncurrentAssets.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _otherNoncurrentAssets.increaseDecrease = _otherNoncurrentAssets.currentYear - _otherNoncurrentAssets.previousYear;
            _otherNoncurrentAssets.percentage =  (_otherNoncurrentAssets.previousYear == 0) ? 0 : Math.Round((_otherNoncurrentAssets.increaseDecrease) / _otherNoncurrentAssets.previousYear,2);
            coaToIs.Add(_setting.otherNoncurrentAssets.name,_setting.otherNoncurrentAssets.accounts);

            var _totalNonCurrentAssets = new repIncomeStatementLineItem {
                name = "  Total NonCurrent Assets",
                note = "",
                currentYear = 
                    _availableForSaleAfsFinancialAsset.currentYear +
                    _restrictedCash.currentYear +
                    _propertyAndEquipment.currentYear +
                    _advancesToAffiliates.currentYear +
                    _otherNoncurrentAssets.currentYear,
                previousYear = 
                    _availableForSaleAfsFinancialAsset.previousYear +
                    _restrictedCash.previousYear +
                    _propertyAndEquipment.previousYear +
                    _advancesToAffiliates.previousYear +
                    _otherNoncurrentAssets.previousYear
            };
            _totalNonCurrentAssets.increaseDecrease = _totalNonCurrentAssets.currentYear - _totalNonCurrentAssets.previousYear;
            _totalNonCurrentAssets.percentage =  (_totalNonCurrentAssets.previousYear == 0) ? 0 : Math.Round((_totalNonCurrentAssets.increaseDecrease) / _totalNonCurrentAssets.previousYear,2);

            var _totalAssets = new repIncomeStatementLineItem {
                name = "",
                note = "",
                currentYear = 
                    _totalCurrentAssets.currentYear +
                    _totalNonCurrentAssets.currentYear,
                previousYear = 
                    _totalCurrentAssets.previousYear +
                    _totalNonCurrentAssets.previousYear
            };
            _totalAssets.increaseDecrease = _totalAssets.currentYear - _totalAssets.previousYear;
            _totalAssets.percentage =  (_totalAssets.previousYear == 0) ? 0 : Math.Round((_totalAssets.increaseDecrease) / _totalAssets.previousYear,2);

            var _tradeAndOtherPayables = new repIncomeStatementLineItem();
            _tradeAndOtherPayables.name = _setting.tradeAndOtherPayables.name;
            _tradeAndOtherPayables.note = "";
            _tradeAndOtherPayables.currentYear = (decimal)currentDatalist.Where(x=>_setting.tradeAndOtherPayables.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _tradeAndOtherPayables.previousYear = (decimal)previousDatalist.Where(x=>_setting.tradeAndOtherPayables.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _tradeAndOtherPayables.increaseDecrease = _tradeAndOtherPayables.currentYear - _tradeAndOtherPayables.previousYear;
            _tradeAndOtherPayables.percentage =  (_tradeAndOtherPayables.previousYear == 0) ? 0 : Math.Round((_tradeAndOtherPayables.increaseDecrease) / _tradeAndOtherPayables.previousYear,2);
            coaToIs.Add(_setting.tradeAndOtherPayables.name,_setting.tradeAndOtherPayables.accounts);

            var _clientsCbu = new repIncomeStatementLineItem();
            _clientsCbu.name = _setting.clientsCbu.name;
            _clientsCbu.note = "";
            _clientsCbu.currentYear = (decimal)currentDatalist.Where(x=>_setting.clientsCbu.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _clientsCbu.previousYear = (decimal)previousDatalist.Where(x=>_setting.clientsCbu.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _clientsCbu.increaseDecrease = _clientsCbu.currentYear - _clientsCbu.previousYear;
            _clientsCbu.percentage =  (_clientsCbu.previousYear == 0) ? 0 : Math.Round((_clientsCbu.increaseDecrease) / _clientsCbu.previousYear,2);
            coaToIs.Add(_setting.clientsCbu.name,_setting.clientsCbu.accounts);

            var _taxPayable = new repIncomeStatementLineItem();
            _taxPayable.name = _setting.taxPayable.name;
            _taxPayable.note = "";
            _taxPayable.currentYear = (decimal)currentDatalist.Where(x=>_setting.taxPayable.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _taxPayable.previousYear = (decimal)previousDatalist.Where(x=>_setting.taxPayable.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _taxPayable.increaseDecrease = _taxPayable.currentYear - _taxPayable.previousYear;
            _taxPayable.percentage =  (_taxPayable.previousYear == 0) ? 0 : Math.Round((_taxPayable.increaseDecrease) / _taxPayable.previousYear,2);
            coaToIs.Add(_setting.taxPayable.name,_setting.taxPayable.accounts);

            var _miCgliPayables = new repIncomeStatementLineItem();
            _miCgliPayables.name = _setting.miCgliPayables.name;
            _miCgliPayables.note = "";
            _miCgliPayables.currentYear = (decimal)currentDatalist.Where(x=>_setting.miCgliPayables.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _miCgliPayables.previousYear = (decimal)previousDatalist.Where(x=>_setting.miCgliPayables.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _miCgliPayables.increaseDecrease = _miCgliPayables.currentYear - _miCgliPayables.previousYear;
            _miCgliPayables.percentage =  (_miCgliPayables.previousYear == 0) ? 0 : Math.Round((_miCgliPayables.increaseDecrease) / _miCgliPayables.previousYear,2);
            coaToIs.Add(_setting.miCgliPayables.name,_setting.miCgliPayables.accounts);

            var _unearnedInterestIncome = new repIncomeStatementLineItem();
            _unearnedInterestIncome.name = _setting.unearnedInterestIncome.name;
            _unearnedInterestIncome.note = "";
            _unearnedInterestIncome.currentYear = (decimal)currentDatalist.Where(x=>_setting.unearnedInterestIncome.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _unearnedInterestIncome.previousYear = (decimal)previousDatalist.Where(x=>_setting.unearnedInterestIncome.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _unearnedInterestIncome.increaseDecrease = _unearnedInterestIncome.currentYear - _unearnedInterestIncome.previousYear;
            _unearnedInterestIncome.percentage =  (_unearnedInterestIncome.previousYear == 0) ? 0 : Math.Round((_unearnedInterestIncome.increaseDecrease) / _unearnedInterestIncome.previousYear,2);
            coaToIs.Add(_setting.unearnedInterestIncome.name,_setting.unearnedInterestIncome.accounts);

            var _provisionForContingencies = new repIncomeStatementLineItem();
            _provisionForContingencies.name = _setting.provisionForContingencies.name;
            _provisionForContingencies.note = "";
            _provisionForContingencies.currentYear = (decimal)currentDatalist.Where(x=>_setting.provisionForContingencies.accounts.Contains(x.motherCode)).Sum(x=> x.amount);
            _provisionForContingencies.previousYear = (decimal)previousDatalist.Where(x=>_setting.provisionForContingencies.accounts.Contains(x.motherCode)).Sum(x=> (x.accountType == "debit") ? x.amount : x.amount * -1);
            _provisionForContingencies.increaseDecrease = _provisionForContingencies.currentYear - _provisionForContingencies.previousYear;
            _provisionForContingencies.percentage =  (_provisionForContingencies.previousYear == 0) ? 0 : Math.Round((_provisionForContingencies.increaseDecrease) / _provisionForContingencies.previousYear,2);
            coaToIs.Add(_setting.provisionForContingencies.name,_setting.provisionForContingencies.accounts);

            var _totalCurrentLiabilities = new repIncomeStatementLineItem {
                name = " Total Current Liabilities",
                note = "",
                currentYear = 
                    _tradeAndOtherPayables.currentYear +
                    _clientsCbu.currentYear +
                    _taxPayable.currentYear +
                    _miCgliPayables.currentYear +
                    _unearnedInterestIncome.currentYear +
                    _provisionForContingencies.currentYear,
                previousYear = 
                    _tradeAndOtherPayables.previousYear +
                    _clientsCbu.previousYear +
                    _taxPayable.previousYear +
                    _miCgliPayables.previousYear +
                    _unearnedInterestIncome.previousYear +
                    _provisionForContingencies.previousYear,
            };
            _totalCurrentLiabilities.increaseDecrease = _totalCurrentLiabilities.currentYear - _totalCurrentLiabilities.previousYear;
            _totalCurrentLiabilities.percentage =  (_totalCurrentLiabilities.previousYear == 0) ? 0 : Math.Round((_totalCurrentLiabilities.increaseDecrease) / _totalCurrentLiabilities.previousYear,2);

             var _deferredTaxLiability = new repIncomeStatementLineItem();
            _deferredTaxLiability.name = _setting.deferredTaxLiability.name;
            _deferredTaxLiability.note = "";
            _deferredTaxLiability.currentYear = (decimal)currentDatalist.Where(x=>_setting.deferredTaxLiability.accounts.Contains(x.motherCode)).Sum(x=> (x.accountType == "debit") ? x.amount : x.amount * -1);
            _deferredTaxLiability.previousYear = (decimal)previousDatalist.Where(x=>_setting.deferredTaxLiability.accounts.Contains(x.motherCode)).Sum(x=> (x.accountType == "debit") ? x.amount : x.amount * -1);
            _deferredTaxLiability.increaseDecrease = _deferredTaxLiability.currentYear - _deferredTaxLiability.previousYear;
            _deferredTaxLiability.percentage =  (_deferredTaxLiability.previousYear == 0) ? 0 : Math.Round((_deferredTaxLiability.increaseDecrease) / _deferredTaxLiability.previousYear,2);
            coaToIs.Add(_setting.deferredTaxLiability.name,_setting.deferredTaxLiability.accounts);

            var _retirementBenefitLiability = new repIncomeStatementLineItem();
            _retirementBenefitLiability.name = _setting.retirementBenefitLiability.name;
            _retirementBenefitLiability.note = "";
            _retirementBenefitLiability.currentYear = (decimal)currentDatalist.Where(x=>_setting.retirementBenefitLiability.accounts.Contains(x.motherCode)).Sum(x=> (x.accountType == "debit") ? x.amount : x.amount * -1);
            _retirementBenefitLiability.previousYear = (decimal)previousDatalist.Where(x=>_setting.retirementBenefitLiability.accounts.Contains(x.motherCode)).Sum(x=> (x.accountType == "debit") ? x.amount : x.amount * -1);
            _retirementBenefitLiability.increaseDecrease = _retirementBenefitLiability.currentYear - _retirementBenefitLiability.previousYear;
            _retirementBenefitLiability.percentage =  (_retirementBenefitLiability.previousYear == 0) ? 0 : Math.Round((_retirementBenefitLiability.increaseDecrease) / _retirementBenefitLiability.previousYear,2);
            coaToIs.Add(_setting.retirementBenefitLiability.name,_setting.retirementBenefitLiability.accounts);

            var _totalNonCurrentLiabilities = new repIncomeStatementLineItem {
                name = "  Total Noncurrent Liabilities",
                note = "",
                currentYear = 
                    _deferredTaxLiability.currentYear +
                    _retirementBenefitLiability.currentYear,
                previousYear = 
                    _deferredTaxLiability.previousYear +
                    _retirementBenefitLiability.previousYear
            };
            _totalNonCurrentLiabilities.increaseDecrease = _totalNonCurrentLiabilities.currentYear - _totalNonCurrentLiabilities.previousYear;
            _totalNonCurrentLiabilities.percentage =  (_totalNonCurrentLiabilities.previousYear == 0) ? 0 : Math.Round((_totalNonCurrentLiabilities.increaseDecrease) / _totalNonCurrentLiabilities.previousYear,2);

            var _totalLiabilities = new repIncomeStatementLineItem {
                name = "  Total Liabilities",
                note = "",
                currentYear = 
                    _totalCurrentLiabilities.currentYear +
                    _totalNonCurrentLiabilities.currentYear,
                previousYear = 
                    _totalCurrentLiabilities.previousYear +
                    _totalNonCurrentLiabilities.previousYear,
            };
            _totalLiabilities.increaseDecrease = _totalLiabilities.currentYear - _totalLiabilities.previousYear;
            _totalLiabilities.percentage =  (_totalLiabilities.previousYear == 0) ? 0 : Math.Round((_totalLiabilities.increaseDecrease) / _totalLiabilities.previousYear,2);

            var _retainedEarnings = new repIncomeStatementLineItem();
            _retainedEarnings.name = _setting.retainedEarnings.name;
            _retainedEarnings.note = "";
            _retainedEarnings.currentYear = (decimal)currentDatalist.Where(x=>_setting.retainedEarnings.accounts.Contains(x.motherCode)).Sum(x=> (x.accountType == "debit") ? x.amount : x.amount * -1);
            _retainedEarnings.previousYear = (decimal)previousDatalist.Where(x=>_setting.retainedEarnings.accounts.Contains(x.motherCode)).Sum(x=> (x.accountType == "debit") ? x.amount : x.amount * -1);
            _retainedEarnings.increaseDecrease = _retainedEarnings.currentYear - _retainedEarnings.previousYear;
            _retainedEarnings.percentage =  (_retainedEarnings.previousYear == 0) ? 0 : Math.Round((_retainedEarnings.increaseDecrease) / _retainedEarnings.previousYear,2);
            coaToIs.Add(_setting.retainedEarnings.name,_setting.retainedEarnings.accounts);

            var _cumulativeRemeasurementGainsOnRetirementBenefit = new repIncomeStatementLineItem();
            _cumulativeRemeasurementGainsOnRetirementBenefit.name = _setting.cumulativeRemeasurementGainsOnRetirementBenefit.name;
            _cumulativeRemeasurementGainsOnRetirementBenefit.note = "";
            _cumulativeRemeasurementGainsOnRetirementBenefit.currentYear = (decimal)currentDatalist.Where(x=>_setting.cumulativeRemeasurementGainsOnRetirementBenefit.accounts.Contains(x.motherCode)).Sum(x=> (x.accountType == "debit") ? x.amount : x.amount * -1);
            _cumulativeRemeasurementGainsOnRetirementBenefit.previousYear = (decimal)previousDatalist.Where(x=>_setting.cumulativeRemeasurementGainsOnRetirementBenefit.accounts.Contains(x.motherCode)).Sum(x=> (x.accountType == "debit") ? x.amount : x.amount * -1);
            _cumulativeRemeasurementGainsOnRetirementBenefit.increaseDecrease = _cumulativeRemeasurementGainsOnRetirementBenefit.currentYear - _cumulativeRemeasurementGainsOnRetirementBenefit.previousYear;
            _cumulativeRemeasurementGainsOnRetirementBenefit.percentage =  (_cumulativeRemeasurementGainsOnRetirementBenefit.previousYear == 0) ? 0 : Math.Round((_cumulativeRemeasurementGainsOnRetirementBenefit.increaseDecrease) / _cumulativeRemeasurementGainsOnRetirementBenefit.previousYear,2);
            coaToIs.Add(_setting.cumulativeRemeasurementGainsOnRetirementBenefit.name,_setting.cumulativeRemeasurementGainsOnRetirementBenefit.accounts);

            var _fairValueReserveOnAfsFinancialAsset = new repIncomeStatementLineItem();
            _fairValueReserveOnAfsFinancialAsset.name = _setting.fairValueReserveOnAfsFinancialAsset.name;
            _fairValueReserveOnAfsFinancialAsset.note = "";
            _fairValueReserveOnAfsFinancialAsset.currentYear = (decimal)currentDatalist.Where(x=>_setting.fairValueReserveOnAfsFinancialAsset.accounts.Contains(x.motherCode)).Sum(x=> (x.accountType == "debit") ? x.amount : x.amount * -1);
            _fairValueReserveOnAfsFinancialAsset.previousYear = (decimal)previousDatalist.Where(x=>_setting.fairValueReserveOnAfsFinancialAsset.accounts.Contains(x.motherCode)).Sum(x=> (x.accountType == "debit") ? x.amount : x.amount * -1);
            _fairValueReserveOnAfsFinancialAsset.increaseDecrease = _fairValueReserveOnAfsFinancialAsset.currentYear - _fairValueReserveOnAfsFinancialAsset.previousYear;
            _fairValueReserveOnAfsFinancialAsset.percentage =  (_fairValueReserveOnAfsFinancialAsset.previousYear == 0) ? 0 : Math.Round((_fairValueReserveOnAfsFinancialAsset.increaseDecrease) / _fairValueReserveOnAfsFinancialAsset.previousYear,2);
            coaToIs.Add(_setting.fairValueReserveOnAfsFinancialAsset.name,_setting.fairValueReserveOnAfsFinancialAsset.accounts);

            var _totalFundBalance = new repIncomeStatementLineItem {
                name = "  Total Fund Balance",
                note = "",
                currentYear = 
                    _retainedEarnings.currentYear +
                    _cumulativeRemeasurementGainsOnRetirementBenefit.currentYear +
                    _fairValueReserveOnAfsFinancialAsset.currentYear,
                previousYear = 
                    _retainedEarnings.previousYear +
                    _cumulativeRemeasurementGainsOnRetirementBenefit.previousYear +
                    _fairValueReserveOnAfsFinancialAsset.previousYear
            };
            _totalFundBalance.increaseDecrease = _totalFundBalance.currentYear - _totalFundBalance.previousYear;
            _totalFundBalance.percentage =  (_totalFundBalance.previousYear == 0) ? 0 : Math.Round((_totalFundBalance.increaseDecrease) / _totalFundBalance.previousYear,2);

            var _totalLiabilitiesAndFundBalance = new repIncomeStatementLineItem {
                name = "",
                note = "",
                currentYear = 
                    _totalLiabilities.currentYear + 
                    _totalFundBalance.currentYear,
                previousYear = 
                    _totalLiabilities.previousYear + 
                    _totalFundBalance.previousYear
            };
            _totalLiabilitiesAndFundBalance.increaseDecrease = _totalLiabilitiesAndFundBalance.currentYear - _totalLiabilitiesAndFundBalance.previousYear;
            _totalLiabilitiesAndFundBalance.percentage =  (_totalLiabilitiesAndFundBalance.previousYear == 0) ? 0 : Math.Round((_totalLiabilitiesAndFundBalance.increaseDecrease) / _totalLiabilitiesAndFundBalance.previousYear,2);

            var _totals = new repIncomeStatementLineItem {
                name = "",
                note = "",
                currentYear = 
                    _totalAssets.currentYear +
                    _totalLiabilitiesAndFundBalance.currentYear,
                previousYear = 
                    _totalAssets.previousYear +
                    _totalLiabilitiesAndFundBalance.previousYear
            };
            _totals.increaseDecrease = _totals.currentYear - _totals.previousYear;
            _totals.percentage =  (_totals.previousYear == 0) ? 0 : Math.Round((_totals.increaseDecrease) / _totalLiabilitiesAndFundBalance.previousYear,2);


            
            var _details = new List<transactionDetailedIs>();
            _details.AddRange(currentDatalist.Select(x=>new transactionDetailedIs {
                slug = x.slug,
                book = x.book,
                number = x.number,
                particulars = x.particulars,
                accountType = x.accountType,
                motherCode = x.motherCode,
                code = x.code,
                codeElement = x.codeElement,
                name = x.name,
                amount = x.amount, // (x.accountType == "credit" ) ?  x.amount * -1 : x.amount,
                issuedAt = x.issuedAt,
                isItem = coaToIs.ToList().Where(y=>y.Value.Contains(x.motherCode)).Select(z=>z.Key).FirstOrDefault()
            }));

            // _details.AddRange(previousDatalist.Select(x=>new transactionDetailedIs {
            //     slug = x.slug,
            //     book = x.book,
            //     number = x.number,
            //     particulars = x.particulars,
            //     accountType = x.accountType,
            //     motherCode = x.motherCode,
            //     code = x.code,
            //     codeElement = x.codeElement,
            //     name = x.name,
            //     amount = (x.accountType == "credit" ) ?  x.amount * -1 : x.amount,
            //     issuedAt = x.issuedAt,
            //     isItem = coaToIs.ToList().Where(y=>y.Value.Contains(x.motherCode)).Select(z=>z.Key).FirstOrDefault()
            // }));
            //populate
            var _bs = new repBalanceSheet {
                currentYear = _currentYear,
                previousYear = _previousYear,
                asOf = asOf,
                assets = new repBalanceSheetAssets {
                    currentAssets = new repBalanceSheetCurrentAssets {
                        cashAndCashEquivalents = _cashAndCashEquivalents,
                        marketableEquitySecuritiesNet = _marketableEquitySecuritiesNet,
                        loansReceivableNet = _loansReceivableNet,
                        interestReceivable = _interestReceivable,
                        otherReceivablesNet = _otherReceivablesNet,
                        otherCurrentAssets = _otherCurrentAssets,
                        totalCurrentAssets = _totalCurrentAssets
                    },
                    nonCurrentAssets = new repBalanceSheetNonCurrentAssets {
                        availableForSaleAfsFinancialAsset = _availableForSaleAfsFinancialAsset,
                        restrictedCash = _restrictedCash,
                        propertyAndEquipment = _propertyAndEquipment,
                        advancesToAffiliates = _advancesToAffiliates,
                        otherNoncurrentAssets = _otherNoncurrentAssets,
                        totalNonCurrentAssets = _totalNonCurrentAssets
                    },
                    totalAssets = _totalAssets
                },
                liabilitiesAndFundBalance = new repBalanceSheetliabilitiesAndFundBalance {
                    currentLiabilities = new repBalanceSheetCurrentLiabilities {
                        tradeAndOtherPayables = _tradeAndOtherPayables,
                        clientsCbu = _clientsCbu,
                        taxPayable = _taxPayable,
                        miCgliPayables = _miCgliPayables,
                        unearnedInterestIncome = _unearnedInterestIncome,
                        provisionForContingencies = _provisionForContingencies,
                        totalCurrentLiabilities = _totalCurrentLiabilities
                    },
                    nonCurrentLiabilities  = new repBalanceSheetNonCurrentLiabilities {
                        deferredTaxLiability = _deferredTaxLiability,
                        retirementBenefitLiability = _retirementBenefitLiability,
                        totalNonCurrentLiabilities = _totalNonCurrentLiabilities
                    },
                    totalLiabilities = _totalLiabilities,
                    fundBalance = new repBalanceSheetFundBalance {
                        retainedEarnings = _retainedEarnings,
                        cumulativeRemeasurementGainsOnRetirementBenefit = _cumulativeRemeasurementGainsOnRetirementBenefit,
                        fairValueReserveOnAfsFinancialAsset = _fairValueReserveOnAfsFinancialAsset,
                        totalFundBalance = _totalFundBalance
                    },
                    totalLiabilitiesAndFundBalance = _totalLiabilitiesAndFundBalance
                },
                totals = _totals,
                details = _details
            };

            return _bs;

        }
        
        public repDailyBalance dailyBalance(DateTime from, DateTime to) {
            var coa = _coa.all();
            var trans = _acc.transactionDetailedList(Convert.ToDateTime("1/1/0001"), to, 0, true);

            var incSub = new[]{212,114};
            
            var repDB = new repDailyBalance();
            
            repDB.from = from;
            repDB.to = to;
            repDB.assetDetails = coa.Where(x=>x.element.ToLower() == "asset").Select(x => new repDailyBalanceDetail {
                acct = x.code.ToString(),
                accountTitle = x.name,
                previous = (Decimal)trans.Where(z=>z.codeElement.ToLower() == "asset" && z.motherCode == x.code && (z.issuedAt < from)).Sum(y=>(y.accountType=="debit") ? y.amount : -y.amount),
                today = (Decimal)trans.Where(z=>z.codeElement.ToLower() == "asset" && z.motherCode == x.code && (z.issuedAt >= from)).Sum(y=>(y.accountType=="debit") ? y.amount : -y.amount),
                toDate = (Decimal)trans.Where(z=>z.codeElement.ToLower() == "asset" && z.motherCode == x.code).Sum(y=>(y.accountType=="debit") ? y.amount : -y.amount),
                subsidiaries = x.subsidiary.Where(z=>incSub.Contains(x.code)).Select(z=>new repDailyBalanceDetailSubsi{
                    acct = "",
                    accountTitle = z.name,
                    previous = 0,
                    today = 0,
                    toDate = 0
                }).ToArray()
            }).ToArray();

            repDB.liabilitiesDetails = coa.Where(x=>x.element.ToLower() == "liabilities").Select(x => new repDailyBalanceDetail {
                acct = x.code.ToString(),
                accountTitle = x.name,
                previous = (Decimal)trans.Where(z=>z.codeElement.ToLower() == "liabilities" && z.motherCode == x.code && (z.issuedAt < from)).Sum(y=>(y.accountType=="debit") ? y.amount : -y.amount),
                today = (Decimal)trans.Where(z=>z.codeElement.ToLower() == "liabilities" && z.motherCode == x.code && (z.issuedAt >= from)).Sum(y=>(y.accountType=="debit") ? y.amount : -y.amount),
                toDate = (Decimal)trans.Where(z=>z.codeElement.ToLower() == "liabilities" && z.motherCode == x.code).Sum(y=>(y.accountType=="debit") ? y.amount : -y.amount),
                subsidiaries = x.subsidiary.Where(z=>incSub.Contains(x.code)).Select(z=>new repDailyBalanceDetailSubsi{
                    acct = "",
                    accountTitle = z.name,
                    previous = 0,
                    today = 0,
                    toDate = 0
                }).ToArray()
            }).ToArray();

            repDB.fundBalanceDetails = coa.Where(x=>x.element.ToLower() == "fund balance").Select(x => new repDailyBalanceDetail {
                acct = x.code.ToString(),
                accountTitle = x.name,
                previous = (Decimal)trans.Where(z=>z.codeElement.ToLower() == "fund balance" && z.motherCode == x.code && (z.issuedAt < from)).Sum(y=>(y.accountType=="debit") ? y.amount : -y.amount),
                today = (Decimal)trans.Where(z=>z.codeElement.ToLower() == "fund balance" && z.motherCode == x.code && (z.issuedAt >= from)).Sum(y=>(y.accountType=="debit") ? y.amount : -y.amount),
                toDate = (Decimal)trans.Where(z=>z.codeElement.ToLower() == "fund balance" && z.motherCode == x.code).Sum(y=>(y.accountType=="debit") ? y.amount : -y.amount),
                subsidiaries = x.subsidiary.Where(z=>incSub.Contains(x.code)).Select(z=>new repDailyBalanceDetailSubsi{
                    acct = "",
                    accountTitle = z.name,
                    previous = 0,
                    today = 0,
                    toDate = 0
                }).ToArray()
            }).ToArray();

            repDB.incomeDetails = coa.Where(x=>x.element.ToLower() == "income").Select(x => new repDailyBalanceDetail {
                acct = x.code.ToString(),
                accountTitle = x.name,
                previous = (Decimal)trans.Where(z=>z.codeElement.ToLower() == "income" && z.motherCode == x.code && (z.issuedAt < from)).Sum(y=>(y.accountType=="debit") ? y.amount : -y.amount),
                today = (Decimal)trans.Where(z=>z.codeElement.ToLower() == "income" && z.motherCode == x.code && (z.issuedAt >= from)).Sum(y=>(y.accountType=="debit") ? y.amount : -y.amount),
                toDate = (Decimal)trans.Where(z=>z.codeElement.ToLower() == "income" && z.motherCode == x.code).Sum(y=>(y.accountType=="debit") ? y.amount : -y.amount),
                subsidiaries = x.subsidiary.Where(z=>incSub.Contains(x.code)).Select(z=>new repDailyBalanceDetailSubsi{
                    acct = "",
                    accountTitle = z.name,
                    previous = 0,
                    today = 0,
                    toDate = 0
                }).ToArray()
            }).ToArray();

            repDB.expensesDetails = coa.Where(x=>x.element.ToLower() == "expenses").Select(x => new repDailyBalanceDetail {
                acct = x.code.ToString(),
                accountTitle = x.name,
                previous = (Decimal)trans.Where(z=>z.codeElement.ToLower() == "expenses" && z.motherCode == x.code && (z.issuedAt < from)).Sum(y=>(y.accountType=="debit") ? y.amount : -y.amount),
                today = (Decimal)trans.Where(z=>z.codeElement.ToLower() == "expenses" && z.motherCode == x.code && (z.issuedAt >= from)).Sum(y=>(y.accountType=="debit") ? y.amount : -y.amount),
                toDate = (Decimal)trans.Where(z=>z.codeElement.ToLower() == "expenses" && z.motherCode == x.code).Sum(y=>(y.accountType=="debit") ? y.amount : -y.amount),
                subsidiaries = x.subsidiary.Where(z=>incSub.Contains(x.code)).Select(z=>new repDailyBalanceDetailSubsi{
                    acct = "",
                    accountTitle = z.name,
                    previous = 0,
                    today = 0,
                    toDate = 0
                }).ToArray()
            }).ToArray();

            repDB.variance = new repDailyBalanceDetail {
                acct = "",
                accountTitle = "",
                previous = 
                    (Decimal)repDB.assetDetails.Sum(x=>x.previous) +
                    repDB.liabilitiesDetails.Sum(x=>x.previous) +
                    repDB.fundBalanceDetails.Sum(x=>x.previous) +
                    repDB.incomeDetails.Sum(x=>x.previous) +
                    repDB.expensesDetails.Sum(x=>x.previous),
                today = (Decimal)repDB.assetDetails.Sum(x=>x.today) +
                    repDB.liabilitiesDetails.Sum(x=>x.today) +
                    repDB.fundBalanceDetails.Sum(x=>x.today) +
                    repDB.incomeDetails.Sum(x=>x.today) +
                    repDB.expensesDetails.Sum(x=>x.today),
                toDate = (Decimal)repDB.assetDetails.Sum(x=>x.toDate) +
                    repDB.liabilitiesDetails.Sum(x=>x.toDate) +
                    repDB.fundBalanceDetails.Sum(x=>x.toDate) +
                    repDB.incomeDetails.Sum(x=>x.toDate) +
                    repDB.expensesDetails.Sum(x=>x.toDate)
            };
            
            repDB.summary = new repDailyBalanceSummary {
                assetSummary = new repDailyBalanceDetail {
                    previous = repDB.assetDetails.Sum(x=>x.previous),
                    today = repDB.assetDetails.Sum(x=>x.today),
                    toDate = repDB.assetDetails.Sum(x=>x.toDate)
                },
                liabilitiesSummary = new repDailyBalanceDetail {
                    previous = (repDB.liabilitiesDetails.Sum(x=>x.previous) * -1),
                    today = (repDB.liabilitiesDetails.Sum(x=>x.today) * -1),
                    toDate = (repDB.liabilitiesDetails.Sum(x=>x.toDate) * -1)
                },
                fundBalanceSummary = new repDailyBalanceDetail {
                    previous = (repDB.fundBalanceDetails.Sum(x=>x.previous) * -1),
                    today = (repDB.fundBalanceDetails.Sum(x=>x.today) * -1),
                    toDate = (repDB.fundBalanceDetails.Sum(x=>x.toDate) * -1)
                },
                incomeSummary = new repDailyBalanceDetail {
                    previous = (repDB.incomeDetails.Sum(x=>x.previous) * -1),
                    today = (repDB.incomeDetails.Sum(x=>x.today) * -1),
                    toDate = (repDB.incomeDetails.Sum(x=>x.toDate) * -1)
                },
                expensesSummary = new repDailyBalanceDetail {
                    previous = repDB.expensesDetails.Sum(x=>x.previous),
                    today = repDB.expensesDetails.Sum(x=>x.today),
                    toDate = repDB.expensesDetails.Sum(x=>x.toDate)
                }
            };

            repDB.summary.netIncomeLoss = new repDailyBalanceDetail {
                previous = 
                    repDB.summary.assetSummary.previous + 
                    repDB.summary.liabilitiesSummary.previous + 
                    repDB.summary.fundBalanceSummary.previous + 
                    repDB.summary.incomeSummary.previous + 
                    repDB.summary.expensesSummary.previous,
                today = 
                    repDB.summary.assetSummary.today + 
                    repDB.summary.liabilitiesSummary.today + 
                    repDB.summary.fundBalanceSummary.today + 
                    repDB.summary.incomeSummary.today + 
                    repDB.summary.expensesSummary.today,
                toDate = 
                    repDB.summary.assetSummary.toDate + 
                    repDB.summary.liabilitiesSummary.toDate + 
                    repDB.summary.fundBalanceSummary.toDate + 
                    repDB.summary.incomeSummary.toDate + 
                    repDB.summary.expensesSummary.toDate
            };

            return repDB;
        }

        public orReportModel orPrinting(string orNo) {

            // check if upfront fee or weekly payment
            
            var collPaymentUpfront = _db.GetCollection<paymentUpfront>("paymentUpfront");
            var collPaymentLoan = _db.GetCollection<paymentLoans>("paymentLoans");

            var paymentUpfront = collPaymentUpfront.Find(Builders<paymentUpfront>.Filter.Eq(x=>x.officialReceipt.number, orNo)).FirstOrDefault();
            var paymentLoan = collPaymentLoan.Find(Builders<paymentLoans>.Filter.Eq(x=>x.officialReceipt.number, orNo)).FirstOrDefault();

            if(paymentUpfront!=null) {
                //paymentUpfront
                return new orReportModel {
                    branch = paymentUpfront.loan.branch.name,
                    dateIssued = paymentUpfront.officialReceipt.paidAt,
                    receivedFrom = paymentUpfront.payee.name,
                    tin = "",
                    address = (paymentUpfront.payee.address==null) ? "" : paymentUpfront.payee.address,
                    businessStyle = "",
                    amountInWords = num2words.Convert(Convert.ToDecimal(paymentUpfront.officialReceipt.total)),
                    amountInNumber = Convert.ToDouble(paymentUpfront.officialReceipt.total),
                    paymentFor = "Upfront Fees",
                    loanPrincipal = 0,
                    loanInterest = 0,
                    capitalBuildUp = Math.Round(paymentUpfront.officialReceipt.fees.Where(x=>x.type.ToLower() == "cbuamount").First().amount,2),
                    processingFee = Math.Round(paymentUpfront.officialReceipt.fees.Where(x=>x.type.ToLower() == "loanprocessingfee").First().amount,2),
                    miPremium = Math.Round(paymentUpfront.officialReceipt.fees.Where(x=>x.type.ToLower() == "mipremium").First().amount,2),
                    miFee = 0,
                    cgliPremium = Math.Round(paymentUpfront.officialReceipt.fees.Where(x=>x.type.ToLower() == "cgli").First().amount,2),
                    cgliFee = 0,
                    dst = Math.Round(paymentUpfront.officialReceipt.fees.Where(x=>x.type.ToLower() == "dst").First().amount,2),
                    others1Amount = 0,
                    others1Description = 0,
                    others2Amount = 0,
                    others2Description = 0,
                    others3Amount = 0,
                    others3Description = 0,
                    totalSales = Convert.ToDouble(paymentUpfront.officialReceipt.total),
                    lessScPwdDisc = 0,
                    amountDuelessScPwdDisc = Convert.ToDouble(paymentUpfront.officialReceipt.total),
                    lessWitholdingTax = 0,
                    amountDuelessWitholdingTax = Convert.ToDouble(paymentUpfront.officialReceipt.total)
                };
            } else if(paymentLoan!=null) {
                return new orReportModel {
                    branch = paymentLoan.loan.branch.name,
                    dateIssued = paymentLoan.officialReceipt.paidAt,
                    receivedFrom = paymentLoan.payee.name,
                    tin = "",
                    address = (paymentLoan.payee.address==null) ? "" : paymentLoan.payee.address,
                    businessStyle = "",
                    amountInWords = num2words.Convert(Convert.ToDecimal(paymentLoan.officialReceipt.amounts.total)),
                    amountInNumber = Convert.ToDouble(paymentLoan.officialReceipt.amounts.total),
                    paymentFor = "Upfront Fees",
                    loanPrincipal = Math.Round(paymentLoan.officialReceipt.amounts.principal,2),
                    loanInterest = Math.Round(paymentLoan.officialReceipt.amounts.interest,2),
                    capitalBuildUp =  Math.Round(paymentLoan.officialReceipt.amounts.cbu,2),
                    processingFee = 0,
                    miPremium = 0,
                    miFee = 0,
                    cgliPremium = 0,
                    cgliFee = 0,
                    dst = 0,
                    others1Amount = 0,
                    others1Description = 0,
                    others2Amount = 0,
                    others2Description = 0,
                    others3Amount = 0,
                    others3Description = 0,
                    totalSales = Convert.ToDouble(paymentLoan.officialReceipt.amounts.total),
                    lessScPwdDisc = 0,
                    amountDuelessScPwdDisc = Convert.ToDouble(paymentLoan.officialReceipt.amounts.total),
                    lessWitholdingTax = 0,
                    amountDuelessWitholdingTax = Convert.ToDouble(paymentLoan.officialReceipt.amounts.total)
                };
            } else {
                return null;
            }
        }

        public repMafModel mafPrinting(clients client) {
            
            if(client!=null) {

                var maf = new repMafModel {
                    PersonalInformation = new repMafModelPersonnalInformation {
                        Title = "",
                        Firstname = "",
                        Middlename = "",
                        LastName = "",
                        FullName = "",
                        Birthdate = null,
                        CivilStatus = "",
                        EducationAttainment = "",
                        Contacts = new repMafModelPersonnalInformationContact[]{},
                        Address = new repMafModelPersonnalInformationAddress {
                            Region = "",
                            Province = "",
                            Municipality = "",
                            Barangay = "",
                            Subdivision = "",
                            Street = "",
                            Address = "",
                            Postal = "",
                            YearOfStay = 0,
                            Year = 0
                        },
                        PicturePath = "",
                        Signature = "",
                        Age = 0,
                        Birthplace = ""
                    },
                    Guarantor = new repMafModelGuarantor {
                        Firstname = "",
                        Middlename = "",
                        Lastname = "",
                        FullName = "",
                        Relationship = "",
                        Birthdate = null,
                        BusinessTypeEmployerName = "",
                        Position = "",
                        Address = "",
                        OfficeTelephone = "",
                        MobileNumber = "",
                        Age = 0
                    },
                    BusinessInformation = new repMafModelBusinessInformation {
                        TypeOfBusiness = "",
                        NatureOfBusiness = "",
                        BusinessAddress = "",
                        NoOfYears = 0,
                        Year = 0,
                        OtherBusinessOfTheFamily = "",
                        OtherLoanProviders = new repMafModelBusinessInformationOtherLoanProviders[]{}
                    },
                    PreQualificationInquiries = new repMafModelPreQualificationInquiries {
                        QualificationInquiries = new repMafModelIncomeAndFinances {
                            WeeklyIncome = 0,
                            WeeklyExpense = 0,
                            NetDisposableIncome = 0,
                            CreditLimit = 0,
                            DateOfCreditInvestigation = null
                        }
                    },
                    InterviewWithNeighbors = new repMafModelInterviewWithNeighbors {
                        Name = "",
                        FeedbackFromNeighbors = ""
                    },
                    DunamisInfo = new repMafModelDunamisInfo {
                        Center = "",
                        CurrentLoanCycle = 0,
                        CurrentCbuBalance = 0,
                        Ids = new repMafModelDunamisInfoIds[]{},
                        AgriLoan = new repMafModelDunamisInfoAgriLoan {
                            LandHectares = ""
                        }
                    },
                    Mi = new repMafModelMi {
                        Beneficiary = new repMafModelBeneficiary {
                            Relationship = "",
                            Firstname = "",
                            Middlename = "",
                            Lastname = "",
                            FullName = "",
                            Birthdate = null,
                            Signature = "",
                            Age = 0
                        },
                        Dependents = new repMafModelDependents[]{}
                    }
                };
                
                maf.PersonalInformation.PicturePath = client.imageLocation;
                maf.PersonalInformation.Title = client.title;
                maf.PersonalInformation.Firstname = client.name.first;
                maf.PersonalInformation.Middlename = client.name.middle;
                maf.PersonalInformation.LastName = client.name.last;
                maf.PersonalInformation.FullName = client.name.last + ", " + client.name.first + " " + client.name.middle;
                maf.PersonalInformation.Birthdate = client.birthdate;
                maf.PersonalInformation.CivilStatus = client.civilStatus;
                maf.PersonalInformation.EducationAttainment = client.educationalAttainment;
                maf.PersonalInformation.Contacts = client.contacts.Select(cont=>new repMafModelPersonnalInformationContact {
                    Type = cont.name, 
                    Value = cont.number
                }).ToArray();
                var addr = client.addresses[0];
                maf.PersonalInformation.Address = new repMafModelPersonnalInformationAddress {
                    Region = addr.region.name,
                    Province = addr.province.name,
                    Municipality = addr.municipality.name,
                    Barangay = addr.barangay.name,
                    Subdivision = addr.subdivision,
                    Street = addr.street,
                    Address = addr.street + " " + addr.subdivision + " " + addr.barangay.name + " " + addr.municipality.name + ", " + addr.province.name + " " + addr.region.name,
                    Postal = addr.postal,
                    YearOfStay = (DateTime.Now.Year - addr.yearStayStarted),
                    Year = addr.yearStayStarted
                };
                maf.PersonalInformation.PicturePath = (client.imageLocation == null) ? "" : client.imageLocation;
                maf.PersonalInformation.Signature = (client.signature == null) ? "" : client.signature;
                maf.PersonalInformation.Age = CalculateAge(client.birthdate);
                maf.PersonalInformation.Birthplace = client.birthplace;
                
                if(client.guarantor!=null) {
                    maf.Guarantor.Firstname = client.guarantor.name.first;
                    maf.Guarantor.Middlename = client.guarantor.name.middle;
                    maf.Guarantor.Lastname = client.guarantor.name.last;
                    maf.Guarantor.FullName = client.guarantor.name.last + ", " + client.guarantor.name.first + " " + client.guarantor.name.middle;
                    maf.Guarantor.Relationship = client.guarantor.relationship;
                    maf.Guarantor.Birthdate = client.guarantor.birthdate;
                    maf.Guarantor.BusinessTypeEmployerName = client.business.type;
                    maf.Guarantor.Position = client.guarantor.business.position;
                    maf.Guarantor.Address = client.guarantor.business.address;
                    maf.Guarantor.OfficeTelephone = client.guarantor.business.phone;
                    maf.Guarantor.MobileNumber = client.guarantor.mobile;
                    maf.Guarantor.Age = CalculateAge(client.guarantor.birthdate);
                }

                
                maf.BusinessInformation.TypeOfBusiness = client.business.name;
                maf.BusinessInformation.NatureOfBusiness = client.business.type;
                maf.BusinessInformation.BusinessAddress = client.business.address;
                maf.BusinessInformation.NoOfYears = client.business.yearStayStarted;
                maf.BusinessInformation.Year = client.business.yearStarted;
                maf.BusinessInformation.OtherBusinessOfTheFamily = client.business.other;
                if(client.business.otherLoanProviders!=null) {
                    maf.BusinessInformation.OtherLoanProviders = client.business.otherLoanProviders.Select(oth=>new repMafModelBusinessInformationOtherLoanProviders {
                        NameOfProvider = oth.name,
                        MemberSince = oth.memberSince,
                        LoanType = oth.loanType,
                        Amount = oth.amount,
                        Term = oth.term
                    }).ToArray();
                }

                if(client.creditLimit!=null) {
                    if(client.creditLimit.Count > 0) {
                        maf.PreQualificationInquiries.QualificationInquiries = new repMafModelIncomeAndFinances {
                            WeeklyIncome = client.creditLimit[0].income,
                            WeeklyExpense = client.creditLimit[0].expense,
                            NetDisposableIncome = client.creditLimit[0].netDisposableIncome,
                            CreditLimit = client.creditLimit[0].creditLimitAmount,
                            DateOfCreditInvestigation = client.creditLimit[0].date
                        };
                    }
                }


                if(client.interview!=null) {
                    maf.InterviewWithNeighbors.Name = client.interview.name;
                    maf.InterviewWithNeighbors.FeedbackFromNeighbors = client.interview.feedback;
                }            

                string centerSlug = "";
                try {
                    var splittedCenterSlug = client.centerSlug.Split("-");
                    centerSlug = splittedCenterSlug[splittedCenterSlug.Length - 4].ToUpper() + "-" + splittedCenterSlug[splittedCenterSlug.Length - 3].ToUpper();
                } catch { }

                maf.DunamisInfo.Center = centerSlug; 
                maf.DunamisInfo.CurrentLoanCycle = client.loanCycle;
                maf.DunamisInfo.CurrentCbuBalance = client.cbuBalance;
                if(client.ids!=null) {
                    maf.DunamisInfo.Ids = client.ids.Select(id=>new repMafModelDunamisInfoIds {
                        Type = id.type,
                        Value = id.number,
                        DateIssued = id.dateIssued,
                        PlaceIssued = id.placeIssued
                    }).ToArray();
                }

                if(client.agri!=null) {
                    maf.DunamisInfo.AgriLoan = new repMafModelDunamisInfoAgriLoan {
                        LandHectares = client.agri.hectares
                    };
                }

                if(client.beneficiary!=null) {
                    maf.Mi.Beneficiary = new repMafModelBeneficiary {
                        Relationship = client.beneficiary.relationship,
                        Firstname = client.beneficiary.first,
                        Middlename = client.beneficiary.middle,
                        Lastname = client.beneficiary.last,
                        FullName = client.beneficiary.last + ", " + client.beneficiary.first + " " + client.beneficiary.middle,
                        Birthdate = client.beneficiary.birthdate,
                        Signature = (client.beneficiary.signature == null) ? "" : client.beneficiary.signature,
                        Age = CalculateAge(client.beneficiary.birthdate)
                    };
                }

                if(client.dependents!=null) {
                    maf.Mi.Dependents = client.dependents.Select(dp=>new repMafModelDependents{
                        Relationship = dp.relationship,
                        Firstname = dp.first,
                        Middlename = dp.middle,
                        Lastname = dp.last,
                        FullName = dp.last + ", " + dp.first + " " + dp.middle,
                        Birthdate = dp.birthdate,
                        SchoolIncomeSourceEmployer = dp.school,
                        GradePosition = dp.gradePosition,
                        Age = CalculateAge(dp.birthdate)
                    }).ToArray();
                }

                if(maf.Mi.Dependents.Count() == 0) {
                    maf.Mi.Dependents = new List<repMafModelDependents> {
                        new repMafModelDependents {
                            FullName = "No Dependents Enrolled"
                        }
                    }.ToArray();
                }



                
                var clientLoansActive = _loanrep.getByClientSlugs (new List<String>{client.slug},"for-payment").ToList();
                var clientLoansClosed = _loanrep.getByClientSlugs(new List<String>{client.slug},"closed").ToList();

                
                loans finLoan = null;

                if(clientLoansActive!=null) {
                    if(clientLoansActive[0].loans.Count > 0) {
                        finLoan = clientLoansActive[0].loans.OrderByDescending(x=>x.disbursementDate).FirstOrDefault();
                    }
                }
        
                if(clientLoansClosed!=null) {
                    if(clientLoansClosed[0].loans.Count > 0) {
                        finLoan = clientLoansClosed[0].loans.OrderByDescending(x=>x.disbursementDate).FirstOrDefault();
                    }
                } 



                maf.FirstLoan = finLoan;
                if(maf.FirstLoan == null) {
                    maf.FirstLoan = new loans {
                        amounts = new coreMaster.Models.amounts()
                    };
                }

                var branch1 = _branchRepo.getBranchSlug(client.branchSlug);
                if(branch1!=null) {
                    maf.BranchName = branch1.name;
                } else {
                    maf.BranchName = "";
                }

                maf.CreatedAt = Convert.ToDateTime(client.createdAt);
                maf.UpdatedAt = Convert.ToDateTime(client.updatedAt);



                return maf;
            } else {
                return null;
            }
        }
    
    
        public repAddress exportAddress() {
            var addr = new repAddress();
            var items = new List<repAddressItem>();
            
            var barsColl = _db.GetCollection<addBarangay>("addBarangay");
            var bars = barsColl.Find(new BsonDocument()).ToList();

            var munsColl = _db.GetCollection<addMunicipality>("addMunicipality");
            var muns = munsColl.Find(new BsonDocument()).ToList();

            var prosColl = _db.GetCollection<addProvince>("addProvince");
            var pros = prosColl.Find(new BsonDocument()).ToList();

            var regsColl = _db.GetCollection<addRegion>("addRegion");
            var regs = regsColl.Find(new BsonDocument()).ToList();

            bars.ForEach(b=>{
               var _b = new[]{b.municipalitySlug,b.slug,b.name,};
               var _m = muns.Where(x=>x.slug == _b[0]).Select(x=>new[]{x.provinceSlug,x.slug, x.name}).FirstOrDefault();
               var _p = pros.Where(x=>x.slug == _m[0]).Select(x=>new[]{x.regionSlug,x.slug, x.name}).FirstOrDefault();
               var _r = regs.Where(x=>x.slug == _p[0]).Select(x=>new[]{x.slug, x.name}).FirstOrDefault();

               var _i = new repAddressItem();
               _i.barangaySlug = _b[1];
               _i.barangayDesc = _b[2];
               _i.municipalitySlug = _m[1];
               _i.municipalityDesc = _m[2];
               _i.provinceSlug = _p[1];
               _i.provinceDesc = _p[2];
               _i.regionSlug = _r[0];
               _i.regionDesc = _r[1];
            
               items.Add(_i);
            });

            addr.items = items.OrderBy(x=>x.regionDesc).ThenBy(x=>x.provinceDesc).ThenBy(x=>x.municipalityDesc).ThenBy(x=>x.barangayDesc).ToArray();
            return addr;
        }

        public ecrDetailsOutput getNewECR(ecrDetails b, string refId)
        {
            //get other infor
            repEcrModel ecr = calculateEcr2(refId);
            
            //loans
            var loans = Builders<loans>.Filter.Eq(x => x.referenceId, refId);
            var resultLoan = _db.GetCollection<loans>("loans").Find(loans).FirstOrDefault();

            //center
            var centerInstance = Builders<centerInstance>.Filter.Eq(x => x.slug, resultLoan.centerSlug);
            var resultcenterInstance = _db.GetCollection<centerInstance>("centerInstance").Find(centerInstance).FirstOrDefault();


            // set signatories
            var sigs = GetSignatories("ecr", resultLoan.branchSlug, resultLoan.unitSlug, resultLoan.officerSlug);

            
            ecrDetailsOutput ecrDetails = new ecrDetailsOutput();

            ecrDetails.centerDescription = resultcenterInstance.code;
            ecrDetails.loanCycle = resultcenterInstance.loanCycle;
            ecrDetails.centerAddress = resultcenterInstance.address;
            ecrDetails.assignedPO = ecr.assignedPA;
            ecrDetails.interestRate = ecr.interestRate;
            ecrDetails.paymentSchedule = b.paymentSchedule;
            ecrDetails.interestType = ecr.interestType;


            ecrDetails.batches = b.batches;
            //ecrDetails.clients = b.clients;
            List <ecrClientsOutput> ecrClients = new List<ecrClientsOutput>();
            foreach (var c in b.clients)
            {
                //clients
                var client = Builders<clients>.Filter.Eq(x => x.slug, c.slug);
                var resultClient = _db.GetCollection<clients>("clients").Find(client).FirstOrDefault();
                var cbuBalance = 0.0;
                if (resultClient == null)
                {
                    cbuBalance = 0;
                }
                else
                {
                    cbuBalance = resultClient.cbuBalance;
                }

                ecrClients.Add(new ecrClientsOutput
                {
                    cycle = c.cycle,
                    name = c.name,
                    slug = c.slug,
                    balancePrincipal = c.balances.principal,
                    balanceInterest = c.balances.interest,
                    balanceCbu = cbuBalance,
                    currentPrincipal = c.dues.principal,
                    currentInterest = c.dues.interest,
                    currentCbu = c.dues.cbu,
                    pastPrincipal = c.pastDues.principal,
                    pastInterest = c.pastDues.interest,
                    pastCbu = c.pastDues.cbu,
                    totalPrincipal = c.totalDues.principal,
                    totalInterest = c.totalDues.interest,
                    totalCbu = c.totalDues.cbu,
                    totalPrincipalInterest = c.totalDues.principal + c.totalDues.interest,
                    totalTotalDue = c.totalDues.principal + c.totalDues.interest + c.totalDues.cbu
                });
            }
            ecrDetails.clients = ecrClients;
            ecrDetails.preparedBy = sigs.preparedBy;
            ecrDetails.checkedBy = sigs.checkedBy;
            ecrDetails.approvedBy = sigs.approvedBy;

            return ecrDetails;
        }
    }

}