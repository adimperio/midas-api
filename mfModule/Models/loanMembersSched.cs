﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.coreMaster.Models
{

    public class loanMembersSched

    {
        //[BsonId]
        //[BsonRepresentation(BsonType.ObjectId)]
        //public String Id { get; set; }

        [BsonElement("slug")]
        public string slug { get; set; }

        [BsonElement("name")]
        public name name { get; set; }

        [BsonElement("fullname")]
        public string fullname { get; set; }
       

        [BsonElement("schedule")]
        public IList<schedule> schedule { get; set; }

       
    }

   

}
