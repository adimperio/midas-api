﻿using System;

namespace kmbi_core_master.coreMaster.Models
{
    public class departmentAcronymSettings
    {
        public accronym[] list {get;set;}
    }

    public class accronym {
        public string department {get;set;}
        public string acronym {get;set;}
    }

}
