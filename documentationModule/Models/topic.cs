using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.ComponentModel.DataAnnotations;

namespace kmbi_core_master.documentationModule.Models
{
    public class topic
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }
        [BsonElement("title")]
        [RequiredAttribute(ErrorMessage="title is required")]
        public String title {get;set;}
        [BsonElementAttribute("slug")]
        public String slug {get;set;}
        [BsonElement("content")]
        [RequiredAttribute(ErrorMessage="content is required")]
        public String content{get;set;}
        [BsonElement("createdAt")]
        public DateTime createdAt {get;set;}
        [BsonElement("updatedAt")]
        public DateTime updatedAt {get;set;}
    }
}