﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Filters;
using Microsoft.AspNetCore.Cors;
using System;
using Microsoft.AspNetCore.Http;
using System.IO;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace kmbi_core_master.coreMaster.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/clients")]
    [usersAuth]
    public class clientsController : Controller
    {
        private readonly iclientsRepository _clientsRepository;
        private readonly iusersRepository _logInfo;
        private string token;

        public clientsController(iclientsRepository clientsRepository, iusersRepository logInfo)
        {
            this._clientsRepository = clientsRepository;
            _logInfo = logInfo;
        }

        public void logger(string message)
        {
            //Insert Log//
            token = HttpContext.Request.Headers["token"];
            var lInfo = _logInfo.getLoginInfo(token);
            string log = message;
            global.createLog(lInfo.id, lInfo.fullname, lInfo.employment.branch, log);
            //Insert Log//
        }

        // GET: 
        [HttpGet]
        public IEnumerable<clients> GetAll()
        {
            var clients = _clientsRepository.allClients();
            return clients;

        }


        // GET: 
        [HttpGet("clientsIds")]
        public IEnumerable<string> GetAllId()
        {
            var clients = _clientsRepository.allClientId();
            return clients;

        }

        // GET id
        [HttpGet("{id:length(24)}")]
        public IActionResult Get(string id)
        {
            var item = _clientsRepository.Get(id);
            if (item == null)
            {
                //var msg = "{\"type\":\"error\",\"message\":\"No record found.\"}";
                //return handleBadRequest.badRequest(msg);
                return Ok(new { });
            }

            return new ObjectResult(item);
        }


        [HttpGet("searchSlug")]
        public IActionResult getSlug([FromQuery]string slug)
        {

            var item = _clientsRepository.getSlug(slug);
            if (item == null)
            {
                return Ok(new { });
            }

            return new ObjectResult(item);
        }

        [HttpGet("lastAccountNo")]
        public IActionResult lastAccountNo()
        {

            var item = _clientsRepository.lastAccountNo();
            if (item == null)
            {
                return Ok(new { });
            }

            return new ObjectResult(item);
        }

        // SEARCH fullname
        [HttpGet("search")]
        public IActionResult searchName([FromQuery]string name, [FromQuery]string branchId)
        {

            var item = _clientsRepository.searchClientName(name, branchId);
            if (item.Count() == 0)
            {
                //return new JsonStringResult("{\"type\":\"info\",\"message\":\"No record found.\"}");
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }



        // GET check unique slug
        [HttpGet("checkSlug")]
        public IActionResult checkSlug([FromQuery]string slug)
        {

            var item = _clientsRepository.checkSlug(slug);
            if (item == null)
            {
                return Ok(new
                {
                    available = 1
                });
            }

            return Ok(new
            {
                available = 0
            });
        }


        // GET check unique TIN
        [HttpGet("checkTin")]
        public IActionResult checkTin([FromQuery]string tinNumber)
        {

            var item = _clientsRepository.checkTin(tinNumber);
            if (item == null)
            {
                return Ok(new
                {
                    available = 1
                });
            }

            return Ok(new
            {
                available = 0
            });
        }

        [HttpPost("updateCbu")]
        //[errorHandler]
        public IActionResult updateCbu([FromBody] List<loansCBU> p, [FromQuery] string type)
        {
            //try
            //{
            if (type != "increment" && type != "decrement")
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "type not define"
                });
            }
            _clientsRepository.updateClients(p, type);
            return Ok(new
            {
                type = "success",
                message = "Update successful"
            });
            //}
            //catch (Exception x)
            //{
            //    return BadRequest(new
            //    {
            //        type = "error",
            //        message = x.Message
            //    });
            //}
        }


        [HttpPost("getAllClients")]
        //[errorHandler]
        public IActionResult getClients([FromBody] List<string> p)
        {
            //try
            //{
            var item = _clientsRepository.getClients(p);
            if (item.Count() == 0)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
            //}
            //catch (Exception x)
            //{
            //    return BadRequest(new
            //    {
            //        type = "error",
            //        message = x.Message
            //    });
            //}
        }


        [HttpPost("incrementLoanCycle")]
        //[errorHandler]
        public IActionResult incLoanCycle([FromBody] List<string> p)
        {
            _clientsRepository.incLoanCycle(p);
            return Ok(new
            {
                type = "success",
                message = "Update successful"
            });

        }

        [HttpPost("getIds")]
        //[errorHandler]
        public IActionResult getIds([FromBody] List<string> slug)
        {

            var item = _clientsRepository.getIds(slug);
            if (item.Count() == 0)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);

        }


        // POST 
        [errorHandler]
        [HttpPost]
        //[usersAccess(access: "user-add")]
        public IActionResult createClients([FromBody]clients client)
        {   
            try {
                var res =_clientsRepository.checkIfClientSlugIsExisting(new clients[] {client});
                var finClientsToAdd = new List<clients>();
                var finClientsToEdit = new List<clients>();
                var finStatuses = new List<Tuple<String, String, String>>();

                res.ForEach(x=>{
                    var c = _clientsRepository.getSlug(x.Item1);
                    if(x.Item3 == "existing") {
                        finClientsToEdit.Add(c);
                    } else if(x.Item3 == "not-existing") {
                        finClientsToAdd.Add(client);
                    }
                });

                
                // add
                _clientsRepository.Add(finClientsToAdd.ToArray());
                finClientsToAdd.ForEach(x=>{
                    var c = _clientsRepository.getSlug(x.slug);
                    if(c!=null) 
                        finStatuses.Add(new Tuple<String, String, String>("create", c.fullname, c.slug));
                    else
                        finStatuses.Add(new Tuple<String, String, String>("failed", c.fullname, c.slug));
                });

                // update
                finClientsToEdit.ForEach(y=>{
                    finStatuses.Add(new Tuple<String, String, String>("update", y.fullname, y.slug));
                    _clientsRepository.Update(y.slug,y);
                });

                logger("Insert new client " + String.Join(", ",finStatuses.Select(x=>x)));

                return Ok(new
                {
                    type = "success",
                    message = client.name.first + " " + client.name.last + " created successfully.",
                    details = new {
                        create = finStatuses.Where(x=>x.Item1 == "create").Select(x=>new {slug=x.Item3, name=x.Item2}),
                        update = finStatuses.Where(x=>x.Item1 == "update").Select(x=>new {slug=x.Item3, name=x.Item2}),
                        failed = finStatuses.Where(x=>x.Item1 == "failed").Select(x=>new {slug=x.Item3, name=x.Item2})
                    }
                });
            }
            catch(Exception ex) {
                return Ok(new{
                    type = "failed",
                    message = ex.Message,
                    stackTrace = ex.StackTrace
                });
            }
        }

        [errorHandler]
        [HttpPost("multiple")]
        public IActionResult createClients([FromBody] clients[] clients)
        {
            try {
                var res =_clientsRepository.checkIfClientSlugIsExisting(clients);
                var finClientsToAdd = new List<clients>();
                var finClientsToEdit = new List<clients>();
                var finStatuses = new List<Tuple<String, String, String>>();

                res.ForEach(x=>{
                    var c = clients.Where(y=>y.slug == x.Item1).First();
                    if(x.Item3 == "existing") {
                        finClientsToEdit.Add(c);
                    } else if(x.Item3 == "not-existing") {
                        finClientsToAdd.Add(c);
                    }
                });

                _clientsRepository.Add(finClientsToAdd.ToArray());
                finClientsToAdd.ForEach(x=>{
                    var c = _clientsRepository.getSlug(x.slug);
                    if(c!=null) 
                        finStatuses.Add(new Tuple<String, String, String>("create", c.fullname, c.slug));
                    else
                        finStatuses.Add(new Tuple<String, String, String>("failed", c.fullname, c.slug));
                });

                finClientsToEdit.ForEach(y=>{
                    finStatuses.Add(new Tuple<String, String, String>("update", y.fullname, y.slug));
                    _clientsRepository.Update(y.slug,y);
                });


                logger("Sync client(s) " + String.Join(", ",finStatuses.Select(x=>x)));

                return Ok(new
                {
                    type = "success",
                    message ="Client syncing successful",
                    details = new {
                        create = finStatuses.Where(x=>x.Item1 == "create").Select(x=>new {slug=x.Item3, name=x.Item2}),
                        update = finStatuses.Where(x=>x.Item1 == "update").Select(x=>new {slug=x.Item3, name=x.Item2}),
                        failed = finStatuses.Where(x=>x.Item1 == "failed").Select(x=>new {slug=x.Item3, name=x.Item2})
                    }
                });
            }
            catch(Exception ex) {
                return Ok(new{
                    type = "failed",
                    message = ex.Message,
                    stackTrace = ex.StackTrace
                });
            }
        }

        // POST dependents
        [HttpPost("dependents/{slug}")]
        public IActionResult addDependents([FromBody] clientDependents u, string slug)
        {
            _clientsRepository.addDependents(u, slug);

            logger("Insert new dependent " + u.first + " " + u.last);

            return Ok(new
            {
                type = "success",
                message = u.first + " " + u.last + " created successfully."
            });
        }


        // POST creditlimit
        [HttpPost("creditLimit/{slug}")]
        public IActionResult addCreditLimit([FromBody] creditLimit u, string slug)
        {
            _clientsRepository.addCreditLimit(u, slug);

            logger("Insert new credit limit " + u.creditLimitAmount);

            return Ok(new
            {
                type = "success",
                message = u.creditLimitAmount + " created successfully."
            });
        }

        // PUT 
        [errorHandler]
        [HttpPut("{slug}")]
        public IActionResult Put(string slug, [FromBody] clients client)
        {
            var item = _clientsRepository.checkSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _clientsRepository.Update(slug, client);

            logger("Modified client " + client.name.first + " " + client.name.last);

            return Ok(new
            {
                type = "success",
                message = client.name.first + " " + client.name.last + " updated successfully."
            });
        }


        // PUT dependents
        //[errorHandler]
        [HttpPut("clientSlug={clientsSlug}&depId={depId}")]
        public IActionResult updateDependents(string clientsSlug, string depId, [FromBody] clientDependents u)
        {
            var item = _clientsRepository.getDepId(depId);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _clientsRepository.updateDependents(clientsSlug, depId, u);

            logger("Modified dependent " + u.first + " " + u.last);

            return Ok(new
            {
                type = "success",
                message = u.first + " " + u.last + " updated successfully."
            });

        }


        // PUT credit limit
        //[errorHandler]
        //[HttpPut("clientSlug={clientsSlug}&creditLimitId={creditLimitId}")]
        //public IActionResult updateCreditLimit(string clientsSlug, string creditLimitId, [FromBody] creditLimit u)
        //{
        //    var item = _clientsRepository.getCreditLimitId(creditLimitId);
        //    if (item == null)
        //    {
        //        return BadRequest(new
        //        {
        //            type = "error",
        //            message = "No record found"
        //        });
        //    }
        //    _clientsRepository.updateCreditLimit(clientsSlug, creditLimitId, u);

        //    //Insert Log//
        //    string log = "Modified credit limit " + u.creditLimitAmount;
        //    int level = 1;
        //    string link = "link here";
        //    global.log(log, level, link);
        //    //Insert Log//

        //    return Ok(new
        //    {
        //        type = "success",
        //        message = u.creditLimitAmount + " updated successfully."
        //    });

        //}

        // DELETE

        [HttpDelete("{slug}")]
        public IActionResult deleteClients(string slug)
        {

            var item = _clientsRepository.checkSlug(slug);
            var braSlug = _clientsRepository.getBranchSlug(item.branchId);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _clientsRepository.Remove(slug);

            logger("Remove user " + item.name.first + " " + item.name.last);

            //Insert Deletions
            global.insertDeletion("clients", slug, item.branchSlug);

            return Ok(new
            {
                type = "success",
                message = item.name.first + " " + item.name.last + " deleted successfully.",
                slug = braSlug.slug
            });
        }

        //DELETE dependent
        [HttpDelete("removeDependent/clientSlug={clientSlug}&depId={depId}")]
        public IActionResult deleteDependent(string clientSlug, string depId)
        {
            var item = _clientsRepository.getDepId(depId);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _clientsRepository.removeDependent(clientSlug, depId);

            logger("Delete dependent " + item.dependents[0].first + " " + item.dependents[0].last);

            return Ok(new
            {
                type = "success",
                message = item.dependents[0].first + " " + item.dependents[0].last + " deleted successfully."

            });
        }


        //DELETE credit limit
        //[HttpDelete("removeCreditLimit/clientSlug={clientSlug}&creditLimitId={creditLimitId}")]
        //public IActionResult deleteCreditLimit(string clientSlug, string creditLimitId)
        //{
        //    var item = _clientsRepository.getCreditLimitId(creditLimitId);
        //    if (item == null)
        //    {
        //        return BadRequest(new
        //        {
        //            type = "error",
        //            message = "No record found."
        //        });
        //    }

        //    _clientsRepository.removeCreditLimit(clientSlug, creditLimitId);

        //    //Insert Log//
        //    string log = "Delete credit limit " + item.creditLimit[0].creditLimitAmount;
        //    int level = 1;
        //    string link = "link here";
        //    global.log(log, level, link);
        //    //Insert Log//

        //    return Ok(new
        //    {
        //        type = "success",
        //        message = item.creditLimit[0].creditLimitAmount + " deleted successfully."

        //    });
        //}

        [HttpGet("getClients")]
        //[errorHandler]
        public IActionResult perBranch([FromQuery] string branchSlug)
        {

            var item = _clientsRepository.perBranch(branchSlug);
            if (item.Count() == 0)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);

        }

        [HttpGet("clientCnt")]
        //[errorHandler]
        public IActionResult clientCnt([FromQuery] string branchSlug)
        {

            var item = _clientsRepository.cntClients(branchSlug);
            return Ok(new
            {
                count = item
            });
        }

        [HttpGet("getSumCbu")]
        //[errorHandler]
        public IActionResult getSumCbu([FromQuery] string branchSlug)
        {

            var item = _clientsRepository.getSumCBU(branchSlug);
            return Ok(new
            {
                totalCbu = item
            });
        }


        public byte[] ConvertIFormFileToBytes(IFormFile file)
        {
            if (file != null)
            {
                BinaryReader reader = new BinaryReader(file.OpenReadStream());
                return reader.ReadBytes((int)file.Length);
            }
            else
            {
                return null;
            }

        }

        [HttpPost("uploadPicture")]
        public IActionResult UploadPicture([FromQuery] string clientSlug, [FromFormAttribute]IFormFile picture)
        {

            byte[] bytes = new byte[] { };
            Boolean IsSuccess = false; ;
            if (picture != null)
            {
                BinaryReader reader = new BinaryReader(picture.OpenReadStream());
                bytes = reader.ReadBytes((int)picture.Length);

                var a = Request;
                IsSuccess = _clientsRepository.UploadPicture(clientSlug, bytes);
            }

            if (IsSuccess)
            {
                var users = _clientsRepository.getSlug(clientSlug);
                logger("Picture upload [" + clientSlug + "]");
                return Ok(new { type = "success", message = "Picture successfully uploaded.", path = users.imageLocation });
            }
            else
            {
                return Ok(new { type = "error", message = "Uploading failed." });
            }
        }

        public class b64 {
            public string data {get;set;}
        }

        [HttpPost("uploadPicture/base64")]
        public IActionResult UploadPicture([FromQuery] string clientSlug, [FromBody]b64 b64)
        {
            Boolean IsSuccess = false; ;
            if (b64 != null)
            {
                IsSuccess = _clientsRepository.UploadPicture(clientSlug, b64.data);
            }

            if (IsSuccess)
            {
                var users = _clientsRepository.getSlug(clientSlug);
                logger("Picture upload [" + clientSlug + "]");
                return Ok(new { type = "success", message = "Picture successfully uploaded.", path = users.imageLocation });
            }
            else
            {
                return Ok(new { type = "error", message = "Uploading failed." });
            }
        }

        [HttpPost("UploadSignature/base64")]
        public IActionResult UploadSignature([FromQuery] string clientSlug, [FromBody]b64 b64, [FromQuery]Boolean flip = false)
        {
            Boolean IsSuccess = false; ;
            if (b64 != null)
            {
                IsSuccess = _clientsRepository.UploadSignature(clientSlug, b64.data, flip);
            }

            if (IsSuccess)
            {
                var users = _clientsRepository.getSlug(clientSlug);
                logger("Signature upload [" + clientSlug + "]");
                return Ok(new { type = "success", message = "Signature successfully uploaded.", path = users.signature });
            }
            else
            {
                return Ok(new { type = "error", message = "Uploading failed." });
            }
        }

        [HttpPost("UploadBeneficiarySignature/base64")]
        public IActionResult UploadBeneficiarySignature([FromQuery] string clientSlug, [FromBody]b64 b64, [FromQuery]Boolean flip = false)
        {
            Boolean IsSuccess = false; ;
            if (b64 != null)
            {
                IsSuccess = _clientsRepository.UploadBeneficiarySignature(clientSlug, b64.data, flip);
            }

            if (IsSuccess)
            {
                var users = _clientsRepository.getSlug(clientSlug);
                logger("Beneficiary Signature upload [" + clientSlug + "]");
                return Ok(new { type = "success", message = "Beneficiary Signature successfully uploaded.", path = users.signature });
            }
            else
            {
                return Ok(new { type = "error", message = "Uploading failed." });
            }
        }

        [HttpGet("clientsByCenterSlug/{centerSlug}")]
        public IActionResult clientsByCenterSlug(String centerSlug)
        {
            var result = _clientsRepository.clientsPerCenter(centerSlug);
            if (result != null)
                return Ok(result);
            else
                return Ok(new Object[] { });
        }

        [HttpGet("detailed-search")]
        public IActionResult detailedSearch([FromQuery]String field, [FromQuery]Double from, [FromQuery]Double to)
        {
            if (field == null || from < 0 || to < 0)
            {
                return BadRequest(new
                {
                    type = "failed",
                    message = "Invalid Parameter"
                });
            }
            else
            {
                var result = _clientsRepository.clientDetailedSearch(field, from, to);
                if (result != null)
                    return Ok(result);
                else
                    return Ok(new Object[] { });
            }
        }


        [HttpGet("clientsByCreatedDate")]
        public IActionResult getClientsByCreatedDate([FromQuery]DateTime createdDate, [FromQuery]String branchSlug)
        {
            var result = _clientsRepository.clientsByCreatedDate(createdDate, branchSlug);
            if (result != null)
                return Ok(result);
            else
                return Ok(new Object[] { });
        }

        [HttpPost("change-classification")]
        public IActionResult updateClassification([FromBody] changeClassification client)
        {
            _clientsRepository.updateClassification(client);

            logger("Modified client classification ");

            return Ok(new
            {
                type = "success",
                message = " Updated successfully."
            });
        }

        [HttpPatch("change-loan-cycle")]
        public IActionResult updateLoanCycle([FromBody] changeLoanCycle client)
        {
            _clientsRepository.updateLoanCycle(client);

            logger("Modified client Loan Cycle ");

            return Ok(new
            {
                type = "success",
                message = " Updated successfully."
            });
        }

        // GET by process Status
        [HttpGet("getByProcessStatus")]
        public IActionResult getByProcessStatus([FromQuery] string status, [FromQuery] string branchSlug)
        {
            var item = _clientsRepository.getByProcessStatus(status, branchSlug);
            if (item == null)
            {
                return Ok(new { });
            }

            return new ObjectResult(item);
        }


        [HttpGet("getClientsByProcessStatus")]
        public IActionResult getClientsByProcessStatus([FromQuery] string status, [FromQuery] string branchSlug)
        {
            var item = _clientsRepository.getClientsByProcessStatus(status, branchSlug);
            if (item == null)
            {
                return Ok(new { });
            }

            return new ObjectResult(item);
        }


        // Update checkedAt, approvedAt by Status
        [HttpPut("updateByStatus")]
        public IActionResult updateByStatus([FromQuery] string slug, [FromQuery] string status)
        {
            var item = _clientsRepository.checkSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _clientsRepository.updateByStatus(slug, status);

            logger("Modified client " + item.name.first + " " + item.name.last);

            return Ok(new
            {
                type = "success",
                message = item.name.first + " " + item.name.last + " updated successfully."
            });
        }

        [HttpGet("activeClients")]
        public IActionResult getActiveClients([FromQuery] string slug, [FromQuery]String type = "")
        {
            var count = 0;
            var amount = 0.0;

            if(!new []{"center", "branch"}.Contains(type.ToLower()))
                throw new ArgumentException(@"type parameter must be either ""center"" or ""branch""");

            var item = _clientsRepository.activeClients(slug, type);

            if(item!=null) {
                //throw new ArgumentException(@"No record found for " + type + " slug of '" + slug + "'" );
                count = item.count;
                amount = Math.Round(item.amount,2);
            }

            return Ok(new {
                count = count,
                amount = amount
            });

        }

    }


}
