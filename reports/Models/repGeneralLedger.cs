using System;

namespace kmbi_core_master.reports.Models
{
    public class repGeneralLedger
    {
        public Int32 accountCode {get;set;}
        public String accountName {get;set;}
        public Int32 year {get;set;}
        public repGeneralLedgerDetail details {get;set;}

    }

    public class repGeneralLedgerDetail {
        public String month {get;set;}
        public Double debit {get;set;}
        public Double credit {get;set;}
        public Double balanceForwarded {get;set;}
    }
}