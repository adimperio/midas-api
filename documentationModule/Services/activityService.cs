using Microsoft.Extensions.Options;
using System;

namespace kmbi_core_master.documentationModule.Service
{
    public class activityService
    {
        context _context;

        public activityService(IOptions<kmbi_core_master.coreMaster.Models.dbSettings> settings)
        {
            _context = new context(settings);
        }

        public void log(String _log)
        {
            _context.activities.log(new kmbi_core_master.coreMaster.Models.activity {
                log = _log,
                level = 1,
                link = "link here",
                createdAt = DateTime.Now
            });
        }
    }
}