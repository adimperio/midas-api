using System;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Microsoft.Extensions.Options;
using System.Linq;

using kmbi_core_master.documentationModule.Models;
using kmbi_core_master.coreMaster.Filters;
using kmbi_core_master.coreMaster.Models;

using kmbi_core_master.documentationModule.Service;
using Microsoft.AspNetCore.Cors;

namespace kmbi_core_master.documentationModule.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/ticket")]
    [errorHandler]
    public class ticketController : Controller
    {
        private readonly ticketService service;
        public ticketController(IOptions<dbSettings> settings) 
        {
            service = new ticketService(settings);
        }

        [HttpGet]
        public IActionResult Get()
        {
            var list = service.GetTickets();
            
            if(list != null)
            {
                return Ok(from y in list select new {
                        Id = y.Id, 
                        slug = y.slug,
                        ticketId = y.ticketId, 
                        requestorId = y.requestorId,
                        subject = y.subject,
                        category = y.category,
                        status = y.status,
                        closedBy = y.closedBy,
                        closedDate = y.closedDate
                    });
            }
            else
            {
                return Ok(new List<tutorial>());
            }
            
        }

        [HttpGet("{slug}")]
        public IActionResult Get(String slug)
        {
            var tick = service.GetTicketBySlug(slug);
            if (tick != null)
            {
                return Ok(tick);
            }
            else
            {
                return Ok(new {});
            }
        }

        [HttpGet("{ticketSlug}/{messageId}")]
        public IActionResult Get(String ticketSlug, String messageId)
        {
            var mess = service.GetSpecificMessageByTicketSlugMessageSlug(ticketSlug, messageId);
            
            if(mess != null)
            {
                return Ok(mess);
            }
            else
            {
                return Ok(new {});
            }
            
        }

        [HttpPost()]
        public IActionResult Post([FromBody]ticket ticket)
        {
            var res = service.InsertTicket(ticket);
            
            if(res)
            {
                return Ok(new {
                    type="success",
                    message= ticket.ticketId.ToString() + " created successfully.",
                });
            }
            else
            {
                return BadRequest(new {
                    type="error",
                    message= "Ticket create failed."
                });
            }
        }

        [HttpPost("{ticketSlug}")]
        public IActionResult Post(String ticketSlug, [FromBody]message message)
        {
            var res = service.InsertMessage(ticketSlug, message);

            if(res)
            {
                return Ok(new {
                    type="success",
                    message= "Message created successfully."
                });
            }
            else
            {
                return BadRequest(new {
                    type="error",
                    message= "Message create failed."
                });
            }
        }

        [HttpPut("{ticketSlug}")]
        public IActionResult Put(String ticketSlug, [FromBody]ticket ticket)
        {
            var res = service.UpdateTicket(ticketSlug, ticket);

            if(res)
            {
                return Ok(new {
                    type="success",
                    message= "Ticket updated successfully."
                });
            }
            else
            {
                return BadRequest(new {
                    type="error",
                    message= "Ticket update failed."
                });
            }
        }

        [HttpPut("{ticketSlug}/{messageId}")]
        public IActionResult Put(String ticketSlug, String messageId, [FromBody]message message)
        {
            var res = service.UpdateMessage(ticketSlug, messageId, message);
            if(res)
            {
                return Ok(new {
                    type="success",
                    message= "Message updated successfully."
                });
            }
            else
            {
                return BadRequest(new {
                    type="error",
                    message= "Message update failed."
                });
            }
        }

        [HttpDelete("{ticketSlug}")]
        public IActionResult Delete(String ticketSlug)
        {
            var tick = service.GetTicketBySlug(ticketSlug);
            var res = service.DeleteTicket(ticketSlug);

            if(res)
            {
                return Ok(new {
                    type="success",
                    message= tick.Id + " deleted successfully."
                });
            }
            else
            {
                 return BadRequest(new {
                    type="error",
                    message= "Ticket delete failed."
                });
            }
        }

        [HttpDelete("{ticketSlug}/{messageId}")]
        public IActionResult Delete(String ticketSlug, String messageId)
        {
            ticket tick = null;
            message mess = null;
            tick = service.GetTicketBySlug(ticketSlug);

            if (tick!=null)
            {
                mess = tick.messages.Find(t => t.Id == messageId);
            }

            var res = service.DeleteMessage(ticketSlug, messageId);

            if(res)
            {
                return Ok(new {
                    type="success",
                    message= "Message deleted successfully."
                });
            }
            else
            {
                return BadRequest(new {
                    type="error",
                    message= "Message delete failed."
                });
            }
        }
    }
}