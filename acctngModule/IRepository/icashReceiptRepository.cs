﻿using kmbi_core_master.acctngModule.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.acctngModule.Interface
{
    public interface icashReceiptRepository
    {
        IEnumerable<cashReceipt> all();

        IEnumerable<cashReceipt> all(DateTime from, DateTime to);

        cashReceipt getSlug(String slug);

        void add(cashReceipt b);

        void update(String slug, cashReceipt b);

        void remove(String slug);

        List<cashReceipt> issuedAt(DateTime from, DateTime to);

        //CREDIT
        crAccounts getCreditId(string creditId);

        void addCredit(crCredit u, string crSlug);

        void updateCredit(String crlug, string creditId, crCredit u);

        void removeCredit(String crSlug, string creditId);


        //DEBIT
        cashReceipt getDebitId(string debitId);

        void addDebit(crDebit u, string crSlug);

        void updateDebit(String crSlug, string debitId, crDebit u);

        void removeDebit(String crSlug, string debitId);

        //Check existing receiptNo
        cashReceipt checkOr(String orNo);

        void updateIsCancelled(String slug, cashReceipt b);

        void updateIsClosed(DateTime from, DateTime to, cashReceipt b);
    }
}
