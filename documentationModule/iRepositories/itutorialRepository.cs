using System;
using System.Collections.Generic;
using kmbi_core_master.documentationModule.Models;

namespace kmbi_core_master.documentationModule.iRepositories {

    public interface itutorialRepository {
        IEnumerable<tutorial> GetTutorials();
        tutorial GetTutorialBySlug(String slug);
        topic GetTutorialSpecificTopic(String tutorialSlug, String topicSlug);
        Boolean InsertTutorial(tutorial tutorial);
        Boolean InsertTopic(String slug, topic topic);
        Boolean UpdateTutorial(String tutorialSlug, tutorial tutorial);
        Boolean UpdateTopic(String tutorialSlug, String topicSlug, topic topic);
        Boolean DeleteTutorial(String tutorialSlug);
        Boolean DeleteTopic(String tutorialSlug, String topicSlug);
    }
}