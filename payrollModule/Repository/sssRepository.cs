﻿using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.payrollModule.IRepository;
using kmbi_core_master.payrollModule.Models;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace kmbi_core_master.payrollModule.Repository
{
    public class sssRepository: dbCon, isssRepository
    {
       
        public sssRepository(IOptions<dbSettings> settings) : base(settings)
        {
        }

        public sss getSalary(double salary)
        {
            var q1 = Builders<sss>.Filter.Lte(e => e.salaryRange.from, salary);
            var q2 = Builders<sss>.Filter.Gte(e => e.salaryRange.to, salary);
            var query = Builders<sss>.Filter.And(q1, q2);
            var c = _db.GetCollection<sss>("sss").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }
    }
}
