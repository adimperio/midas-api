﻿using Microsoft.Extensions.Options;

using System;
using System.Collections.Generic;
using System.Linq;

using MongoDB.Bson;
using MongoDB.Driver;

using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.acctngModule.Models;
using kmbi_core_master.acctngModule.Interface;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;

namespace kmbi_core_master.acctngModule.Repository
{
    public class acctEntryTemplateJVRepository : dbCon, iacctEntryTemplateJVRepository
    {
        
        public acctEntryTemplateJVRepository(IOptions<dbSettings> settings) : base(settings) {}

        public void add(acctEntryTemplateJV b)
        {

            b.timestamps.createdAt = DateTime.Now;
            b.timestamps.updatedAt = DateTime.Now;
            for (int i = 0; i <= b.accounts.credit.Count() - 1; i++)
            {
                b.accounts.credit[i].Id = ObjectId.GenerateNewId().ToString();
            }

            for (int i = 0; i <= b.accounts.debit.Count() - 1; i++)
            {
                b.accounts.debit[i].Id = ObjectId.GenerateNewId().ToString();
            }
            var coll = _db.GetCollection<acctEntryTemplateJV>("acctEntryTemplateJV");
            _db.GetCollection<acctEntryTemplateJV>("acctEntryTemplateJV").InsertOneAsync(b);
        }
        

        public IEnumerable<acctEntryTemplateJV> all()
        {
            var c = _db.GetCollection<acctEntryTemplateJV>("acctEntryTemplateJV").Find(new BsonDocument()).ToListAsync();
            return c.Result;
        }

        public acctEntryTemplateJV getSlug(String slug)
        {
            var query = Builders<acctEntryTemplateJV>.Filter.Eq(e => e.slug, slug);
            var c = _db.GetCollection<acctEntryTemplateJV>("acctEntryTemplateJV").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }

        
        public void update(String slug, acctEntryTemplateJV p)
        {
            
            p.slug = slug;
            var query = Builders<acctEntryTemplateJV>.Filter.Eq(e => e.slug, slug);
            var update = Builders<acctEntryTemplateJV>.Update
                .Set("particulars", p.particulars)
                .Set("accounts", p.accounts)
                .Set("amount", p.amount)
                .Set("timestamps.issuedAt", p.timestamps.issuedAt)
                .Set("timestamps.issuedBy", p.timestamps.issuedBy)
                .Set("timestamps.createdBy", p.timestamps.createdBy)
                .Set("timestamps.updatedBy", p.timestamps.updatedBy)
                .CurrentDate("timestamps.updatedAt");

            var jv = _db.GetCollection<acctEntryTemplateJV>("acctEntryTemplateJV").UpdateOneAsync(query, update);

        }
        public void remove(String slug)
        {
            var query = Builders<acctEntryTemplateJV>.Filter.Eq(e => e.slug, slug);
            var result = _db.GetCollection<acctEntryTemplateJV>("acctEntryTemplateJV").DeleteOneAsync(query);
        }

        
    }
}
