using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.ComponentModel.DataAnnotations;

namespace kmbi_core_master.documentationModule.Models
{
    public class message
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("userId")]
        public String userId {get;set;}

        [BsonElementAttribute("message")]
        [RequiredAttribute(ErrorMessage="Content is required")]
        public String content {get;set;}

        [BsonElement("createdAt")]
        public DateTime createdAt {get;set;}
        
        [BsonElement("updatedAt")]
        public DateTime updatedAt {get;set;}
    }
}