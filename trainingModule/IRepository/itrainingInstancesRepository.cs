﻿using kmbi_core_master.coreMaster.Controllers;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.spm.Models;
using kmbi_core_master_v2.trainingModule.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.trainingModule.IRepository
{
    public interface itrainingInstancesRepository
    {
        IEnumerable<trainingInstances> getAll();

        trainingInstances getBySlug(String slug);

        void add(trainingInstances b);

        void update(String slug, trainingInstances b);

        void remove(string slug);

        trainingInstances[] getByYear(Int32 Year);
    }
}
