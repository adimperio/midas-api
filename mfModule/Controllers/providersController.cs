﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Filters;
using Microsoft.AspNetCore.Cors;

namespace kmbi_core_master.coreMaster.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/providers")]
    [usersAuth]
    public class providersController : Controller
    {
        private readonly iprovidersRepository _Repository;
        private readonly iusersRepository _logInfo;
        private string token;

        public providersController(iprovidersRepository Repositor, iusersRepository logInfo)
        {
            this._Repository = Repositor;
            _logInfo = logInfo;
        }

        public void logger(string message)
        {
            //Insert Log//
            token = HttpContext.Request.Headers["token"];
            var lInfo = _logInfo.getLoginInfo(token);
            string log = message;
            global.createLog(lInfo.id, lInfo.fullname, lInfo.employment.branch, log);
            //Insert Log//
        }

        // GET: 
        [HttpGet]
        public IEnumerable<providers> GetAll()
        {
            var c = _Repository.allProviders();
            return c;

        }

        //GET officer Slug
        [HttpGet("{id}")]
        public IActionResult getId(string id)
        {

            var item = _Repository.getId(id);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        [HttpGet("slug/{slug}")]
        public IActionResult getSlug(string slug)
        {

            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

       

        [HttpPost]
        [errorHandler]
        public IActionResult create([FromBody] providers b)
        {

            _Repository.add(b);

            logger( "Insert new provider " + b.name);

            return Ok(new
            {
                type = "success",
                message = b.name + " created successfully.",
                slug = b.slug
            });

        }


        // POST Pdependents
        [HttpPost("dependents/{slug}")]
        public IActionResult addDependents([FromBody] pDependents u, string slug)
        {
            _Repository.addPdependent(u, slug);

            logger("Insert new premium dependent " + u.name + " " + u.relationship);

            return Ok(new
            {
                type = "success",
                message = u.name + " " + u.relationship + " created successfully."
            });
        }


        [HttpPut("{id}")]
        public IActionResult update(string id, [FromBody] providers b)
        {
            var item = _Repository.getId(id);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _Repository.update(id, b);

            logger( "Modified provider " + b.name);

            return Ok(new
            {
                type = "success",
                message = b.name + " updated successfully."
            });
        }


        [HttpPut("slug/{slug}")]
        public IActionResult slugupdate(string slug, [FromBody] providers b)
        {
            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _Repository.updateSlug(slug, b);

           logger("Modified provider " + b.name);

            return Ok(new
            {
                type = "success",
                message = b.name + " updated successfully."
            });
        }



        [HttpDelete("{id}")]
        public IActionResult delete(string id)
        {

            var item = _Repository.getId(id);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.remove(id);

           logger("Delete provider " + item.name);

            return Ok(new
            {
                type = "success",
                message = item.name + " deleted successfully."
            });
        }


        [HttpDelete("slug/{slug}")]
        public IActionResult slugdelete(string slug)
        {

            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.removeSlug(slug);

            logger("Delete provider " + item.name);

            return Ok(new
            {
                type = "success",
                message = item.name + " deleted successfully."
            });
        }


    }
}
