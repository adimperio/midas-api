﻿using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.helpers;
using kmbi_core_master.hrModule.Interface;
using kmbi_core_master.hrModule.Models;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace kmbi_core_master.coreMaster.Repository
{
    public class salaryUpdateRepository : dbCon, isalaryUpdateRepository
    {
        public salaryUpdateRepository(IOptions<dbSettings> settings) : base(settings)
        {
        }

        public void create(salaryUpdate b)
        {

            b.createdAt = DateTime.Now;
            b.updatedAt = DateTime.Now;

            b.salary = encryptDecrypt.EncryptString(b.salary.ToString());

            _db.GetCollection<salaryUpdate>("salaryUpdate").InsertOneAsync(b);
        }

        

        public List<salaryUpdate> getAll()
        {
            var d = _db.GetCollection<salaryUpdate>("salaryUpdate").Find(new BsonDocument()).ToListAsync();
            return d.Result;
        }



        public salaryUpdate getBySlug(String slug)
        {
            var query = Builders<salaryUpdate>.Filter.Eq(e => e.slug, slug);
            var d = _db.GetCollection<salaryUpdate>("salaryUpdate").Find(query).ToListAsync().Result.FirstOrDefault();
            if (d != null)
            {
                d.salary = encryptDecrypt.DecryptString(d.salary);
            }
            return d;
        }

        public List<salaryUpdate> getByApproved(int approved)
        {
            var query = Builders<salaryUpdate>.Filter.Eq(e => e.isApproved, approved);
            var d = _db.GetCollection<salaryUpdate>("salaryUpdate").Find(query).ToListAsync().Result;
            foreach (var item in d)
            {
                item.salary = encryptDecrypt.DecryptString(item.salary);
            }
            return d;
        }


        public void update(String slug, salaryUpdate p)
        {
            //UPDATE updateSalary
            var query = Builders<salaryUpdate>.Filter.Eq(e => e.slug, slug);
            var update = Builders<salaryUpdate>.Update
                .Set("salary", encryptDecrypt.EncryptString(p.salary.ToString()))
                .Set("isApproved", p.isApproved)
                .Set("approvedBy", p.approvedBy)
                .CurrentDate("updatedAt");

            var d = _db.GetCollection<salaryUpdate>("salaryUpdate").UpdateOneAsync(query, update);

            //UPDATE users
            var query1 = Builders<users>.Filter.Eq(e => e.slug, slug);
            var update1 = Builders<users>.Update
                .Set("employment.salary", encryptDecrypt.EncryptString(p.salary.ToString()))
                .CurrentDate("updatedAt");

            var d1 = _db.GetCollection<users>("users").UpdateOneAsync(query1, update1);
        }

        
        public void delete(String slug)
        {
            var query = Builders<salaryUpdate>.Filter.Eq(e => e.slug, slug);
            var d = _db.GetCollection<salaryUpdate>("salaryUpdate").DeleteOneAsync(query);
        }

       

        public List<salaryUpdate> search(String name)
        {

            var query = Builders<salaryUpdate>.Filter.Regex("name", new BsonRegularExpression(name, options: "i"));
            var d = _db.GetCollection<salaryUpdate>("salaryUpdate").Find(query).ToListAsync();

            return d.Result;
        }

       
    }
}
