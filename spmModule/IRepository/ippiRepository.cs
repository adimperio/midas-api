﻿using kmbi_core_master.coreMaster.Controllers;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.spm.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.spm.IRepository
{
    public interface ippiRepository
    {

        IEnumerable<ppi> getAll();

        IEnumerable<ClientProfiling> getAllClientsByBranch(string branchSlug);

        ppi getBySlug(String slug);

        ppi getByClientSlug(String slug);

        IEnumerable<ppi> getByLoanSlug(String loanSlug);

        void add(ppi b);

        void update(String slug, ppi b);

        void remove(string slug);

        ppiScore getByDateRange(DateTime from, DateTime to, string region, string area, string branch, string unit, string po, string center);

        List<loans> getppiclients(DateTime from, DateTime to, string region, string area, string branch, string unit, string po, string center);

        List<ppiExtraction> extractPPI();

        ppiScore newGetByDateRange(DateTime from, DateTime to, string region, string area, string branch, string unit, string po, string center);
    }
}
