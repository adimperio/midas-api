﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.acctngModule.Models
{
    public class acctngDba
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("slug")]
        public String slug { get; set; }

        [BsonElement("status")]
        public String status { get; set; }

        [BsonElement("timestamps")]
        public dbaTimestamps timestamps { get; set; }

        [BsonElement("perAccount")]
        public List<dbaPerAccount> perAccount { get; set; }
    }

    public class dbaTimestamps
    {
        [BsonElement("closedAt")]
        public DateTime closedAt { get; set; }

        [BsonElement("closedBy")]
        public string closedBy { get; set; }
    }

    public class dbaPerAccount
    {
        [BsonElement("code")]
        public int code { get; set; }

        [BsonElement("name")]
        public String name { get; set; }

        [BsonElement("previousAmount")]
        public double previousAmount { get; set; }

        [BsonElement("todayAmount")]
        public double todayAmount { get; set; }

        [BsonElement("todateAmount")]
        public double todateAmount { get; set; }

        [BsonElement("subsidiaryAccount")]
        public List<dbaSubsidiaryAccount> subsidiaryAccount { get; set; }
    }

    public class dbaSubsidiaryAccount
    {
        [BsonElement("code")]
        public int code { get; set; }

        [BsonElement("name")]
        public String name { get; set; }

        [BsonElement("previousAmount")]
        public double previousAmount { get; set; }

        [BsonElement("todayAmount")]
        public double todayAmount { get; set; }

        [BsonElement("todateAmount")]
        public double todateAmount { get; set; }
    }
}
