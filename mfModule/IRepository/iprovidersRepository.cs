﻿using kmbi_core_master.coreMaster.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.coreMaster.IRepository
{
    public interface iprovidersRepository
    {
        IEnumerable<providers> allProviders();

        providers getSlug(String slug);

        providers getId(String id);

        void add(providers b);

        void addPdependent(pDependents u, string slug);

        void update(String id, providers b);

        void updateSlug(String slug, providers b);

        void remove(string id);

        void removeSlug(string slug);

    }
}
