using System;
using System.Globalization;
using System.Linq;
using OfficeOpenXml;

namespace kmbi_core_master.reports.UseCases.GenerateTINIssuanceRepot {
    public class GenerateTINIssuanceReportPresenter : IPresent<GenerateTINIssuanceReportResponse, GenerateTINIssuanceReportPresenterResponse> {

        public GenerateTINIssuanceReportPresenterResponse Present(GenerateTINIssuanceReportResponse request)
        {
            using (OfficeOpenXml.ExcelPackage ep = new OfficeOpenXml.ExcelPackage())
            {
                var _from = request.from.ToString("MM-dd-yy");
                var _to = request.to.ToString("MM-dd-yy");
                ExcelWorksheet ti = ep.Workbook.Worksheets.Add(String.Format("tin-issuance-{0}-{1}",_from,_to));
                
                ti.Cells[1,1].Value = "Fistname";
                ti.Cells[1,2].Value = "Middlename";
                ti.Cells[1,3].Value = "Lastname";
                ti.Cells[1,4].Value = "MaidenFirstName";
                ti.Cells[1,5].Value = "MaidenMiddlename";
                ti.Cells[1,6].Value = "MaidenLastname";
                ti.Cells[1,7].Value = "Birthdate";
                ti.Cells[1,8].Value = "Gender";
                ti.Cells[1,9].Value = "CivilStatus";
                ti.Cells[1,10].Value = "Address";
                ti.Cells[1,11].Value = "Contact Person";
                ti.Cells[1,12].Value = "Contact No";
                ti.Cells[1,13].Value = "Spouse Firstname";
                ti.Cells[1,14].Value = "Spouse Middlename";
                ti.Cells[1,15].Value = "Spouse Lastname";
                ti.Cells[1,16].Value = "Spouse TIN";

                

                var ctr = 2;
                foreach(TINIssuance item in request.items) {
                    TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
                    ti.Cells[ctr,1].Value = textInfo.ToTitleCase(item.FirstName);
                    ti.Cells[ctr,2].Value = item.MiddleName;
                    ti.Cells[ctr,3].Value = item.LastName;
                    ti.Cells[ctr,4].Value = item.MaidenFirstName;
                    ti.Cells[ctr,5].Value = item.MaidenMiddleName;
                    ti.Cells[ctr,6].Value = item.MaidenLastName;
                    ti.Cells[ctr,7].Value = item.Birthdate.ToString("MM/dd/yyyy");


                    var formattedGender = textInfo.ToTitleCase(item.Gender);

                    ti.Cells[ctr,8].Value = formattedGender;
                    ti.Cells[ctr,9].Value = item.CivilStatus;
                    ti.Cells[ctr,10].Value = item.CompleteResidenceAddress;
                    ti.Cells[ctr,11].Value = item.ContactPerson;
                    ti.Cells[ctr,12].Value = item.ContactNo;
                    ti.Cells[ctr,13].Value = item.SpouseFirstName;
                    ti.Cells[ctr,14].Value = item.SpouseMiddleName;
                    ti.Cells[ctr,15].Value = item.SpouseLastName;
                    ti.Cells[ctr,16].Value = item.SpouseTIN;
                    ctr++;
                }

                ti.Column(1).Width = 17;
                ti.Column(2).Width = 17;
                ti.Column(3).Width = 17;
                ti.Column(4).Width = 17;
                ti.Column(5).Width = 17;
                ti.Column(6).Width = 17;
                ti.Column(7).Width = 12;
                ti.Column(8).Width = 12;
                ti.Column(9).Width = 12;

                ti.Column(10).Width = 80;
                ti.Column(11).Width = 30;
                ti.Column(12).Width = 12;

                ti.Column(13).Width = 17;
                ti.Column(14).Width = 17;
                ti.Column(15).Width = 17;
                ti.Column(16).Width = 17;

                ti.Row(1).Style.Font.Bold = true;

                byte[] bytes = ep.GetAsByteArray();
                ep.Dispose();
                return new GenerateTINIssuanceReportPresenterResponse {
                    bytes = bytes,
                    test = "Hello"
                };
            }
        }
    }

    public class GenerateTINIssuanceReportPresenterResponse
    {
        public byte[] bytes {get;set;}
        public String test {get;set;}
    }

}