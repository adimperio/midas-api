using System;
using System.Collections.Generic;
using System.Linq;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.documentationModule.iRepositories;
using kmbi_core_master.documentationModule.Models;
using kmbi_core_master.helpers.interfaces;

namespace kmbi_core_master.helpers
{
    public class global2 : iglobal2
    {
        private readonly iholidayRepository _holidayRepo;
        private readonly iloanRepository _loansRepo;
        private readonly ibranchRepository _branchRepo;

        public global2(iholidayRepository h, iloanRepository l, ibranchRepository b)
        {
            _holidayRepo = h;
            _loansRepo = l;
            _branchRepo = b;
        }

        public void reupdateSchedule()
        {
            
            int cnt = 0;
            
            IEnumerable<holiday> list =  _holidayRepo.GetHolidays(0,0);
            IEnumerable<DateTime> holidays = from x in list select x.date;

            IEnumerable<branch> branches = _branchRepo.allBranch();
            

            var loans = _loansRepo.allLoans();
           
            foreach(loans l in loans)
            {
                DateTime disbursementDate = l.firstPaymentDate.Value.Date.AddDays(-7);
                int term = l.loanType.term;

                // New Schedule
                for (int i = 0; i <= term - 1; i++)
                {
                    cnt += 1;
                    disbursementDate = disbursementDate.AddDays(7);
                    while (holidays.Contains(disbursementDate))
                    {
                        disbursementDate = disbursementDate.AddDays(7);
                    }

                    //get branches holidays
                    // foreach(branch b in branches)
                    // {
                    //     if(b.Id == l.branchId)
                    //     {
                    //         if(b.holidays!=null){     
                    //             foreach(holiday h in b.holidays)
                    //             {
                    //                 if(h.date.Date==disbursementDate.Date)
                    //                 {
                    //                     disbursementDate = disbursementDate.AddDays(7);
                    //                 }                                
                    //             }
                    //         }
                    //     }
                    // }


                    //Update each member
                    for (int r = 0; r <= l.members.Count - 1; r++)
                    {
                        l.members[r].schedule[i].collectionDate = disbursementDate;
                        l.updatedAt = DateTime.Now;                        
                    }
                }
            }

            var arrLoans = loans.ToArray();
            for(Int32 ctr=0;ctr<=arrLoans.Length -1;ctr++) {
                
                _loansRepo.update(arrLoans[ctr].Id,arrLoans[ctr]);
            }
        }
    }
}