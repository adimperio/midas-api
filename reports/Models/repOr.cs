﻿using kmbi_core_master.coreMaster.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.reports.Models
{
    public class repOr
    {
        public String no { get; set; }
        public DateTime paidAt {get;set;}
        public Double amount { get; set; }
    }
}
