﻿using kmbi_core_master.coreMaster.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.coreMaster.IRepository
{
    public interface iloanMembersRepository
    {
        IEnumerable<loanMembers> getAll();

        //loanMembers getSlug(String slug);

        //loanMembers getId(String id);

        //loanMembers getSchedule(String loanId, string memberId, string scheduleId);

        void add(loanMembers b);

        //void update(String id, loanMembers b);

        void updateSlug(String slug, loanMembers b);

        //void remove(String id);

        void removeSlug(String slug);

    }
}
