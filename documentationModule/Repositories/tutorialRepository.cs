using System;
using System.Collections.Generic;
using Microsoft.Extensions.Options;

using kmbi_core_master.documentationModule.Models;
using kmbi_core_master.documentationModule.iRepositories;
using kmbi_core_master.coreMaster.Models;
using MongoDB.Driver;
using System.Linq;
using MongoDB.Bson;

namespace kmbi_core_master.documentationModule.Repositories {
    public class tutorialRepository :  dbCon, itutorialRepository
    {
        IMongoCollection<tutorial> _coll;
        public tutorialRepository(IOptions<dbSettings> settings) : base(settings) {
            _coll = _db.GetCollection<tutorial>("tutorial");
        }

        public IEnumerable<tutorial> GetTutorials()
        {
           return _coll.Find(Builders<tutorial>.Filter.Empty).ToList();
        }

        public tutorial GetTutorialBySlug(string slug)
        {
            return _coll.Find(Builders<tutorial>.Filter.Eq("slug",slug)).FirstOrDefault();
        }

        public topic GetTutorialSpecificTopic(string tutorialSlug, string topicSlug)
        {
            var filterTutorial = Builders<tutorial>.Filter.Eq("slug",tutorialSlug);
            var filterTopic = Builders<tutorial>.Filter.Eq("topics.slug",topicSlug);
            var filterCombine = Builders<tutorial>.Filter.And(filterTutorial, filterTopic);

            var inc = Builders<tutorial>.Projection.Include("topics.$");

            var tutorial = _coll.Find(filterCombine).Project<tutorial>(inc).FirstOrDefault();

            if (tutorial!=null)
            {
                if(tutorial.topics != null)
                {
                    if(tutorial.topics.Count > 0)
                    {
                        return tutorial.topics[0];
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }              
            }
            else
            {
                return null;
            }
        }

        public bool InsertTutorial(tutorial tutorial)
        {
            foreach (topic t in tutorial.topics)
            {
                t.Id = ObjectId.GenerateNewId().ToString();
                t.createdAt = DateTime.Now;
                t.updatedAt = DateTime.Now;
            }
            
            tutorial.createdAt = DateTime.Now;
            tutorial.updatedAt = DateTime.Now;

            var list = _coll.Find(new BsonDocument()).ToList();
            int ord = 0;
            foreach(tutorial t in list)
            {
                ord +=1;
            }
            tutorial.order = ord + 1;
            _coll.InsertOne(tutorial);

            tutorial _tut = GetTutorialBySlug(tutorial.slug);

            if(_tut!=null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool InsertTopic(string slug, topic topic)
        {
            var filterTutorial = Builders<tutorial>.Filter.Eq("slug",slug);
            var update = Builders<tutorial>
                .Update
                .AddToSet("topics",new topic
                {
                    Id = ObjectId.GenerateNewId().ToString(),
                    title = topic.title,
                    slug = topic.slug,
                    content = topic.content
                })
                .CurrentDate("updatedAt");
            _coll.UpdateOne(filterTutorial,update);

            topic _top = GetTutorialSpecificTopic(slug, topic.slug);

            if(_top!=null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool UpdateTutorial(string tutorialSlug, tutorial tutorial)
        {
            var filterTutorial = Builders<tutorial>.Filter.Eq("slug",tutorialSlug);
            var update = Builders<tutorial>
                .Update
                .Set("name",tutorial.name)
                .CurrentDate("updatedAt");

            var IsSucess = true;
            _coll.UpdateOne(filterTutorial, update);

            if(IsSucess)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public bool UpdateTopic(string tutorialSlug, string topicSlug, topic topic)
        {
            var filterTutorial = Builders<tutorial>.Filter.Eq("slug",tutorialSlug);
            var filterTopic = Builders<tutorial>.Filter.Eq("topics.slug",topicSlug);
            var filterCombine = Builders<tutorial>.Filter.And(filterTutorial, filterTopic);
            var update = Builders<tutorial>
                .Update
                .Set("topics.$.title", topic.title)
                .Set("topics.$.slug", topicSlug)
                .Set("topics.$.content", topic.content)
                .CurrentDate("updatedAt");
            
            var IsSuccess = true;
            _coll.UpdateOne(filterCombine,update);
            if(IsSuccess)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        public bool DeleteTutorial(string tutorialSlug)
        {  
            tutorial tut = GetTutorialBySlug(tutorialSlug);
            var IsSucess = true;
            _coll.DeleteOne(Builders<tutorial>.Filter.Eq("slug",tutorialSlug));
            if (IsSucess)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool DeleteTopic(string tutorialSlug, string topicSlug)
        {
            var filterTutorial = Builders<tutorial>.Filter.Eq("slug",tutorialSlug);
            var filterTopic = Builders<topic>.Filter.Eq("slug",topicSlug);

            var update = Builders<tutorial>
                .Update
                .CurrentDate("updatedAt")
                .PullFilter("topics",filterTopic);
            
            topic top = GetTutorialSpecificTopic(tutorialSlug, topicSlug);
            var IsSucess = true;_coll.UpdateOne(filterTutorial,update);

            if(IsSucess)
            {
                return true;
            }
            else
            {
                return false;
            }
        }




    }
}