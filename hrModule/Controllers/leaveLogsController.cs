﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Filters;
using kmbi_core_master.hrModule.Interface;
using kmbi_core_master.hrModule.Models;
using Microsoft.AspNetCore.Cors;
using System;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Linq;

namespace kmbi_core_master.coreMaster.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/leaveLogs")]
    [usersAuth]
    public class leaveLogsController : Controller
    {
        private readonly ileaveLogsRepository _Repository;

        public leaveLogsController(ileaveLogsRepository Repository)
        {
            this._Repository = Repository;
        }

        private void logger(String message)
        {
            //global.log(message, 1, "link here");
        }  
 
        private String fixEmployeeCode(String code) {
            if(code.Length<=9)
                return code.PadRight(9,' ');
            else
                return code;
        }

        [HttpGetAttribute("")]
        public IActionResult get([FromQueryAttribute]String code, [FromQueryAttribute]DateTime? date)
        {
            code = fixEmployeeCode(code);
            List<leaveLog> leaveLogs = new List<leaveLog>();
            if(code==null && date == null) {
                leaveLogs.AddRange(_Repository.getLeaveLogs());
            } else if(code!=null && date==null) {
                leaveLogs.AddRange(_Repository.getLeaveLogsByEmployeeCode(code));
            } else if(code==null && date!=null) {
                leaveLogs.AddRange(_Repository.getLeaveLogsByDate(Convert.ToDateTime(date)));
            } else if(code!=null && date!=null) {
                leaveLogs.AddRange(_Repository.getLeaveLogsByEmployeeCodeByDate(code,Convert.ToDateTime(date)));
            }
            
            return Ok(leaveLogs);
        }

        [HttpGetAttribute("{slug}")]
        public IActionResult getLeaveLogBySlug(String slug)
        {
            var leaveLog = _Repository.getLeaveLogBySlug(slug);
            if (leaveLog!=null) {
                return Ok(leaveLog);
            } else {
                return Ok(new {});
            };
        }

        [HttpPostAttribute]
        public IActionResult createLeaveLog([FromBodyAttribute]leaveLog leaveLog)
        {
            leaveLog.employee.name.full = leaveLog.employee.name.first + " " + leaveLog.employee.name.middle + " " + leaveLog.employee.name.last;
            leaveLog.createdAt = DateTime.Now;
            leaveLog.updatedAt = DateTime.Now;
            var res = _Repository.createLeaveLog(leaveLog);
            if (res) {
                logger("Leave " + leaveLog.code + " created");
                return Ok(new {type="success", message="Leave " + leaveLog.code + " created successfully"});
            } else {
                return Ok(new {type="failed", message="Leave create failed"});
            };
        }

        [HttpPutAttribute("{slug}")]
        public IActionResult updateLeaveLogBySlug(String slug,[FromBodyAttribute]leaveLog leaveLog)
        {
            leaveLog.employee.name.full = leaveLog.employee.name.first + " " + leaveLog.employee.name.middle + " " + leaveLog.employee.name.last;
            var res = _Repository.updateLeavelogBySlug(slug,leaveLog);
            if (res) {
                logger("Leave " + leaveLog.code + " updated");
                return Ok(new {type="success", message="Leave " + leaveLog.code + " updated successfully"});
            } else {
                return Ok(new {type="failed", message="Leave update failed"});
            };
        }

        [HttpDeleteAttribute("{slug}")]
        public IActionResult deleteLeaveLogBySlug(String slug)
        {
            var lv = _Repository.getLeaveLogBySlug(slug);
            var res = _Repository.deleteLeaveLogBySlug(slug);
            if (res) {
                logger("Leave " + lv.code + " deleted");
                return Ok(new {type="success", message="Leave " + lv.code + " deleted successfuly"});
            } else {
                return Ok(new {type="failed", message="Leave delete failed"});
            };
        }

        [HttpPostAttribute("uploadAttachment/")]
        public IActionResult UploadAttachment([FromFormAttribute]IFormFile attachment)
        {
            BinaryReader reader = new BinaryReader(attachment.OpenReadStream());
            byte[] b = reader.ReadBytes((int)attachment.Length);

            String link = _Repository.uploadAttachment(attachment.FileName.Replace(" ","_"), b);

            if(link!="") {
                return Ok(
                    new 
                    {
                        type="success",
                        message="Uploading of attachment successful",
                        path=link
                    }
                );
            } else{
                return Ok(
                    new 
                    {
                        type="failed",
                        message="Uploading of attachment failed"
                    }
                );
            }
            
        }

        [HttpGetAttribute("getOverlappedLeaves/{employeeCode}")]
        public IActionResult getOverlappedLeaves(String employeeCode, [FromQuery]DateTime from, [FromQuery]DateTime to) {
            //List<leaveLog> list = _Repository.getOverlappedLeaves(employeeCode,from,to);
            // if(list!=null) {
            //     if(list.Count>0) {
            //         String lv = "";
            //         foreach(var l in list) {
            //             lv += l.type + " - " + l.dates.from.ToString() + " to " + l.dates.to.ToString() + ", ";
            //         }
            //         lv = lv.Substring(0, lv.Length - 2);
            //         return Ok(new {
            //             type="failed",
            //             message="Leave application overlapped with the following existing leave(s) : [" + lv + "]"}
            //         );
            //     } else {
            //         return Ok(new List<leaveLog>());    
            //     }
                
            // } else{
            //     return Ok(new List<leaveLog>());
            // }

            // if(list!=null) {
            //     return Ok(from x in list select new {type = x.type, datefrom = x.dates.@from, dateto = x.dates.to});
            // } else {
            //     return Ok(new List<leaveLog>());
            // }
            employeeCode = fixEmployeeCode(employeeCode);
            return Ok(new {overlap = _Repository.getOverlappedLeaves(employeeCode,from,to)});
        }

        [HttpGetAttribute("leavesPerBranchPerDateRange/{branchSlug}")]
        public IActionResult leavesPerBranchPerDateRange(String branchSlug, [FromQuery]DateTime from, [FromQuery]DateTime to) {
            var list = _Repository.leavesPerBranchPerDateRange(branchSlug,from,to);
            return Ok(from x in list select new 
            {
                type = x.type, 
                name = x.employee.name.last + ", " + x.employee.name.first + " " + x.employee.name.extension + " " + x.employee.name.middle, 
                datefrom = x.dates.@from, 
                dateto = x.dates.to
            });
        }

        [HttpPostAttribute("deleteAttachment/{leaveSlug}")]
        public IActionResult DeleteAttachment(String leaveSlug)
        {
            var lv = _Repository.getLeaveLogBySlug(leaveSlug);
            if(lv!=null) {
                if(lv.attachmentPath!=null) {
                    _Repository.deleteAttachment(lv.attachmentPath);
                    return Ok(
                        new 
                        {
                            type="success",
                            message="Deletion of attachment successful",
                        }
                    );           
                } else {
                    return Ok(
                        new 
                        {
                            type="information",
                            message="No attachment to be deleted",
                        }
                    );           
                }
            } else {
                return Ok(
                    new 
                    {
                        type="information",
                        message="Leave not found",
                    }
                );           
            }
            
            
        }

        [HttpGet("leaveLogPerEmployee/{employeeCode}")]
        public IActionResult leaveLogPerEmployee(String employeeCode, [FromQuery]Int32 year = 0, [FromQuery]DateTime? from = null, [FromQuery]DateTime? to = null) 
        {
            employeeCode = fixEmployeeCode(employeeCode);
            if(year!=0 && from ==null && to ==null) {
                return Ok(_Repository.leavesPerEmployeePerYear(employeeCode, year));
            } else if(year==0 && from !=null && to !=null) {
                return Ok(_Repository.leavesPerEmployeePerDateRange(employeeCode, Convert.ToDateTime(from), Convert.ToDateTime(to)));
            } else {
                return BadRequest(
                    new {
                        type = "failed",
                        message = "Invalid Parameter"
                    }
                );
            }
        }
    }
}
