﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.acctngModule.Models
{
    public class departmentBudget    
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }
       
        [BsonElement("slug")]
        public String slug { get; set; }

        [BsonElement("budgetYear")]
        public int budgetYear { get; set; }

        [BsonElement("departmentName")]
        public string departmentName { get; set; }


        [BsonElement("budget")]
        public List<budBudget> budget { get; set; }

        [BsonElement("code")]
        public String code { get; set; }

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }
    }

    public class budgetDept
    {
        [BsonElement("id")]
        public String id { get; set; }

        [BsonElement("name")]
        public String name { get; set; }

        [BsonElement("slug")]
        public String slug { get; set; }

        [BsonElement("budget")]
        public List<budBudget> budget { get; set; }

    }

    public class budBudget
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id { get; set; }

        [BsonElement("slug")]
        public String slug { get; set; }

        [BsonElement("name")]
        public String name { get; set; }

        [BsonElement("code")]
        public String code { get; set; }

        [BsonElement("budgetLedger")]
        public List<budBudgetLedger> budgetLedger { get; set; }

        [BsonElement("amount")]
        public double amount { get; set; }

    }

    public class budBudgetLedger
    {
        [BsonElement("slug")]
        public String slug { get; set; }

        [BsonElement("name")]
        public String name { get; set; }

        [BsonElement("code")]
        public String code { get; set; }

        [BsonElement("amount")]
        public double amount { get; set; }

        [BsonElement("createdAt")]
        public DateTime createdAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime updatedAt { get; set; }
    }


}
