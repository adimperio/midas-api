using System;
using kmbi_core_master.coreMaster.Models;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace kmbi_core_master.reports.Models
{
    public class repCic
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public String Id {get;set;}

        public String branchCode {get;set;}
        public String title {get;set;}
        public clientFullname name {get;set;}
        public String gender {get;set;}
        public DateTime birthdate {get;set;}
        public Int32 dependents {get;set;}
        public spouse spouse {get;set;}
        public String civilStatus {get;set;}
        public List<clientAddress> addresses {get;set;}
        public List<id> ids {get;set;}
        public IList<clientContact> contacts {get;set;}
                
    }
}