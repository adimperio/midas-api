﻿using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Models;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace kmbi_core_master.coreMaster.Repository
{
    public class cbuWithdrawalRepository : dbCon, icbuWithdrawalRepository
    {
        public cbuWithdrawalRepository(IOptions<dbSettings> settings) : base(settings)
        {
        }

        public void add(cbuWithdrawal b)
        {

            b.createdAt = DateTime.Now;
            b.updatedAt = DateTime.Now;


            _db.GetCollection<cbuWithdrawal>("cbuWithdrawal").InsertOneAsync(b);

            //for (int i = 0; i <= b.members.Count() - 1; i++)
            //{
            //    cbuLogs log = new cbuLogs
            //    {
            //        Id = ObjectId.GenerateNewId().ToString(),
            //        type = "withdrawal",
            //        slug = b.slug + '-' + global.slugify(8),
            //        clientId = b.members[i].id,
            //        amount = b.members[i].amount,
            //        cbuBalance = b.members[i].cbuBalance - b.members[i].amount,
            //        createdAt = DateTime.Now,
            //        updatedAt = DateTime.Now
            //    };
            //    _db.GetCollection<cbuLogs>("cbuLogs").InsertOneAsync(log);
            //}
                
        }

        public IEnumerable<cbuWithdrawal> all()
        {
            var c = _db.GetCollection<cbuWithdrawal>("cbuWithdrawal").Find(new BsonDocument()).ToListAsync();
            return c.Result;
        }

        public cbuWithdrawal getSlug(String slug)
        {
            var query = Builders<cbuWithdrawal>.Filter.Eq(e => e.slug, slug);
            var c = _db.GetCollection<cbuWithdrawal>("cbuWithdrawal").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }

        public List<cbuWithdrawal> getBSlug(String branchSlug)
        {
            var query = Builders<cbuWithdrawal>.Filter.Eq(e => e.branch.slug, branchSlug);
            var c = _db.GetCollection<cbuWithdrawal>("cbuWithdrawal").Find(query).ToListAsync();

            return c.Result;
        }

        public List<cbuWithdrawal> getBSlug(String branchSlug,  DateTime from, DateTime to)
        {
            var query0 = Builders<cbuWithdrawal>.Filter.Eq(e => e.branch.slug, branchSlug);
            var query1 = Builders<cbuWithdrawal>.Filter.Gte(e => e.rfp.datePrepared, from);
            var query2 = Builders<cbuWithdrawal>.Filter.Lte(e => e.rfp.datePrepared, to);
            var query = Builders<cbuWithdrawal>.Filter.And(query0,query1,query2);
            var c = _db.GetCollection<cbuWithdrawal>("cbuWithdrawal").Find(query).ToListAsync();

            return c.Result;
        }


        public void update(String slug, cbuWithdrawal p)
        {
            // p.slug = slug;
            var query = Builders<cbuWithdrawal>.Filter.Eq(e => e.slug, slug);
            var update = Builders<cbuWithdrawal>.Update
                .Set("members", p.members)
                .Set("rfp", p.rfp)
                .Set("cv", p.cv)
                .Set("branch", p.branch)
                .Set("center", p.center)
                .Set("loan", p.loan)
                .CurrentDate("updatedAt");

            var l = _db.GetCollection<cbuWithdrawal>("cbuWithdrawal").UpdateOneAsync(query, update);
            
        }


        public void remove(String slug)
        {
            var query = Builders<cbuWithdrawal>.Filter.Eq(e => e.slug, slug);
            var result = _db.GetCollection<cbuWithdrawal>("cbuWithdrawal").DeleteOneAsync(query);
            
        }


        public banksList getBranchSlug(String branchId)
        {
            var query = Builders<banksList>.Filter.Eq(x => x.Id, branchId);
            var fields = Builders<banksList>.Projection
                .Include(u => u.Id)
                .Include(u => u.slug)
                .Include(u => u.code)
                .Include(u => u.banks);

            var bank = _db.GetCollection<banksList>("branch").Find(query).Project<banksList>(fields).ToListAsync();
            return bank.Result.FirstOrDefault();
        }

        public List<cbuWithdrawal> getByDate(DateTime from, DateTime to, string branchSlug)
        {
            var dtTo = to.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
            var q1 = Builders<cbuWithdrawal>.Filter.Gte(x => x.rfp.datePrepared, from);
            var q2 = Builders<cbuWithdrawal>.Filter.Lte(x => x.rfp.datePrepared, dtTo);
            var q3 = Builders<cbuWithdrawal>.Filter.Eq(x => x.branch.slug, branchSlug);
            var query = Builders<cbuWithdrawal>.Filter.And(q1, q2, q3);
            var c = _db.GetCollection<cbuWithdrawal>("cbuWithdrawal").Find(query).ToListAsync();


            return c.Result;
        }

        public List<cbuWithdrawal> getByCenterSulg(string centerSlug)
        {
            
            var query = Builders<cbuWithdrawal>.Filter.Eq(x => x.center.slug, centerSlug);
            var sort = Builders<cbuWithdrawal>.Sort.Descending("rfp.preparedBy");
            var c = _db.GetCollection<cbuWithdrawal>("cbuWithdrawal").Find(query).Sort(sort).ToListAsync();


            return c.Result;
        }
    }
}
