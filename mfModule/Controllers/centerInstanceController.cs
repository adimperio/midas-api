﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Filters;
using Microsoft.AspNetCore.Cors;

namespace kmbi_core_master.coreMaster.Controllers
{
    [EnableCors("allowCors")]
    [Route("api/centerInstance")]
    [usersAuth]
    public class centerInstanceController : Controller
    {
        private readonly icenterInstanceRepository _Repository;
        private readonly iusersRepository _logInfo;
        private string token;

        public centerInstanceController(icenterInstanceRepository Repositor, iusersRepository logInfo)
        {
            this._Repository = Repositor;
            _logInfo = logInfo;
        }

        public void logger(string message)
        {
            //Insert Log//
            token = HttpContext.Request.Headers["token"];
            var lInfo = _logInfo.getLoginInfo(token);
            string log = message;
            global.createLog(lInfo.id, lInfo.fullname, lInfo.employment.branch, log);
            //Insert Log//
        }

        // GET: 
        [HttpGet]
        public IEnumerable<centerInstance> GetAll()
        {
            var c = _Repository.allCenters();
            return c;

        }

        //GET officer Slug
        [HttpGet("{slug}")]
        public IActionResult getSlug(string slug)
        {

            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }

        //GET officer id
        [HttpGet("officerId={officerId}")]
        public IEnumerable<centerInstance> getOfficer(string officerId)
        {

            var item = _Repository.getOfficer(officerId);
            return item;
            //if (item == null)
            //{
            //    return new JsonStringResult("{}");
            //}

            //return new ObjectResult(item);
        }

        [HttpGet("officerSlug={officerSlug}")]
        public IEnumerable<centerInstance> getOfficerSlug(string officerSlug)
        {

            var item = _Repository.getOfficerSlug(officerSlug);
            return item;
            //if (item == null)
            //{
            //    return new JsonStringResult("{}");
            //}

            //return new ObjectResult(item);
        }


        //search code
        [HttpGet("code={code}/branchId={branchId}")]
        public IActionResult searchCode(string code, string branchId)
        {

            var item = _Repository.searchCode(code, branchId);

            if (item == null)
            {
                return new JsonStringResult("{}");
            }

            return new ObjectResult(item);
        }

        //search by branch
        [HttpGet("getByBranch")]
        public IActionResult getByBranch([FromQuery] string branchSlug)
        {

            var item = _Repository.getByBranch(branchSlug);

            if (item == null)
            {
                return new JsonStringResult("[]");
            }

            return new ObjectResult(item);
        }


        [HttpPost]
        public IActionResult create([FromBody] centerInstance b)
        {

            _Repository.add(b);

            logger("Insert new center instance " + b.code);

            return Ok(new
            {
                type = "success",
                message = b.code + " created successfully.",
                slug = b.slug
            });

        }
        

      
        [HttpPut("{slug}")]
        public IActionResult update(string slug, [FromBody] centerInstance b)
        {
            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found"
                });
            }
            _Repository.update(slug, b);

            logger( "Modified officer " + b.code);

            return Ok(new
            {
                type = "success",
                message = b.code + " updated successfully."
            });
        }

        

        [HttpDelete("{slug}")]
        public IActionResult delete(string slug)
        {

            var item = _Repository.getSlug(slug);
            if (item == null)
            {
                return BadRequest(new
                {
                    type = "error",
                    message = "No record found."
                });
            }

            _Repository.remove(slug);

            logger("Delete officer " + item.code);

            //Insert Deletions
            global.insertDeletion("centerInstance", slug, item.branchSlug);

            return Ok(new
            {
                type = "success",
                message = item.code + " deleted successfully."
            });
        }

       
    }
}
