﻿using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.coreMaster.Models;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace kmbi_core_master.coreMaster.Repository
{
    public class providersRepository : dbCon, iprovidersRepository
    {
        public providersRepository(IOptions<dbSettings> settings) : base(settings)
        {
        }

        public void add(providers b)
        {

            b.createdAt = DateTime.Now;
            b.updatedAt = DateTime.Now;

            _db.GetCollection<providers>("providers").InsertOneAsync(b);
        }

        public IEnumerable<providers> allProviders()
        {
            var c = _db.GetCollection<providers>("providers").Find(new BsonDocument()).ToListAsync();
            return c.Result;
        }

        public providers getSlug(String slug)
        {
            var query = Builders<providers>.Filter.Eq(e => e.slug, slug);
            var c = _db.GetCollection<providers>("providers").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }

        public providers getId(String id)
        {
            var query = Builders<providers>.Filter.Eq(e => e.Id, id);
            var c = _db.GetCollection<providers>("providers").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }

        public void addPdependent(pDependents u, string slug)
        {
            var query = Builders<providers>.Filter.Eq(e => e.slug, slug);
            var update = Builders<providers>.Update.Push(e => e.premium.dependents, u);
            _db.GetCollection<providers>("providers").FindOneAndUpdateAsync(query, update);
        }

        public void update(String id, providers p)
        {
            p.Id = id;
            var query = Builders<providers>.Filter.Eq(e => e.Id, id);
            var update = Builders<providers>.Update
                .Set("name", p.name)
                .Set("description", p.description)
                .Set("premium", p.premium)
                .CurrentDate("updatedAt");

            var l = _db.GetCollection<providers>("providers").UpdateOneAsync(query, update);

        }

        public void updateSlug(String slug, providers p)
        {
           // p.slug = slug;
            var query = Builders<providers>.Filter.Eq(e => e.slug, slug);
            var update = Builders<providers>.Update
                .Set("name", p.name)
                .Set("description", p.description)
                .Set("premium", p.premium)
                .CurrentDate("updatedAt");

            var l = _db.GetCollection<providers>("providers").UpdateOneAsync(query, update);

        }


        public void remove(String id)
        {
            var query = Builders<providers>.Filter.Eq(e => e.Id, id);
            var result = _db.GetCollection<providers>("providers").DeleteOneAsync(query);
        }


        public void removeSlug(String slug)
        {
            var query = Builders<providers>.Filter.Eq(e => e.slug, slug);
            var result = _db.GetCollection<providers>("providers").DeleteOneAsync(query);
        }
    }
}
