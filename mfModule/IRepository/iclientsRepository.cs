﻿using kmbi_core_master.coreMaster.Models;
using System;
using System.Collections.Generic;

namespace kmbi_core_master.coreMaster.IRepository
{
    public interface iclientsRepository
    {
        IEnumerable<clients> allClients();

        IEnumerable<string> allClientId();

        IEnumerable<clients> getClients(List<string> p);
        IEnumerable<clients> GetClients(String[] ids);

        clients lastAccountNo();

        clients Get(String id);

        clients getSlug(String slug);

        clients getDepId(string depId);

        IEnumerable<idList> getIds(List<string> p);

        // clients getCreditLimitId(string CreditLimitId);

        void Add(clients c);

        void Update(String slug, clients c);

        void addDependents(clientDependents u, string Id);

        void updateDependents(String clientsSlug, string dependentsId, clientDependents u);

        void Remove(String slug);

        void removeDependent(String clientsSlug, string dependentsId);

        void addCreditLimit(creditLimit u, string Id);

        //void updateCreditLimit(String clientsSlug, string creditLimitId, creditLimit u);

        //void removeCreditLimit(String clientsSlug, string creditLimitId);

        clients checkSlug(String slug);

        clients checkTin(String tinNumber);

        //IEnumerable<clientSearch> searchClientName(string fullname, string branchId);

        IEnumerable<clients> searchClientName(string fullname, string branchId);

        branch getBranchSlug(String branchId);

        void updateClients(List<loansCBU> p, string type);

        void incLoanCycle(List<string> p);

        List<clients> perBranch(String branchSlug);

        long cntClients(String branchSlug);

        double getSumCBU(String branchSlug);

        Boolean UploadPicture(String slug, Byte[] bytes);
        Boolean UploadPicture(String slug, String B64STring);

        Boolean UploadSignature(String clientSlug, String B64String, Boolean flip = false);

        Boolean UploadBeneficiarySignature(String clientSlug, String B64String, Boolean flip = false);


        IEnumerable<clientsDetailedSearch> clientDetailedSearch(String field, Double from, Double to);

        IEnumerable<clients> clientsPerCenter(String centerSlug);

        IEnumerable<clients> clientsByCreatedDate(DateTime createdDate, String branchSlug);

        void updateClassification(changeClassification c);

        void updateLoanCycle(changeLoanCycle c);

        List<clients> getByProcessStatus(String status, string branchSlug);

        List<clientsByProcessStatus> getClientsByProcessStatus(String status, string branchSlug);

        void updateByStatus(String slug, string status);

        mfModule.Models.activeClientsWithCbuBalance activeClients(string slug, string type);
        
        void Add(clients[] clients);

        List<Tuple<String, String, String>> checkIfClientSlugIsExisting(clients[] clients);

        List<extractCsvClients> extractClients();
    }
}
