﻿using kmbi_core_master.coreMaster.Models;
using Microsoft.Extensions.Options;
using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Linq;
using System.Text.RegularExpressions;
using kmbi_core_master.helpers;

namespace kmbi_core_master.coreMaster.Repository
{
    public class importCSVRepository: dbCon, kmbi_core_master.coreMaster.IRepository.iimportCsvRepository
    {
        public importCSVRepository(IOptions<dbSettings> settings) : base(settings)
        {
        }

        public List<messages> importClients(IFormFile file, String branchId)
        {
            //get branch
            var branchColl = _db.GetCollection<branch>("branch");
            var branchItem = branchColl.Find(Builders<branch>.Filter.Eq(x=>x.Id,branchId)).FirstOrDefault();

            //regionList
            var regionColl = _db.GetCollection<addRegion>("addRegion");
            var regionList = regionColl.Find(Builders<addRegion>.Filter.Empty).ToList();

            //provinceList
            var provinceColl = _db.GetCollection<addProvince>("addProvince");
            var provinceList = provinceColl.Find(Builders<addProvince>.Filter.Empty).ToList();

            //municipalityList
            var municipalityColl = _db.GetCollection<addMunicipality>("addMunicipality");
            var municipalityList = municipalityColl.Find(Builders<addMunicipality>.Filter.Empty).ToList();

            //barangayList
            var barangayColl = _db.GetCollection<addBarangay>("addBarangay");
            var barangayList = barangayColl.Find(Builders<addBarangay>.Filter.Empty).ToList();
            
            // read file uploaded
            StreamReader fs = new StreamReader(file.OpenReadStream(), Encoding.ASCII);

            // parse per line and store to list array
            List<String> list = new List<String>();
            while (!fs.EndOfStream)
            {
                list.Add(fs.ReadLine());
            }
            // remove first line (column row)
            list.RemoveAt(0);


            // list of populated clients
            List<clients> clients = new List<clients>();
            // list of cbuLogs for beginning balance 
            List<cbuLogs> cbuLogs = new List<cbuLogs>();

            // error collections
            List<messages> messages = new List<messages>();
            
            Int32 lineNo = 1;
            foreach(String ln in list)
            {
                lineNo++;
                List<String> errors = new List<String>();
                //map csv
                String[] values = Regex.Split(ln, ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");

                
                //set null on empty string
                var isEmptyLine = false;
                var emptyLineChecker = "";
                for(var ctr=0;ctr<=105;ctr++) {
                    emptyLineChecker += values[ctr];
                    if(values[ctr]=="") {
                        values[ctr] = null;
                    }
                }

                if(emptyLineChecker=="") {
                    isEmptyLine = true;
                }
                

                var _firstName = values[0]; //req
                var _middleName = values[1]; //req
                var _lastName = values[2]; //req
                var _region = values[3]; //req
                var _province = values[4]; //req
                var _municipality = values[5]; //req
                var _barangay = values[6]; //req
                var _street = values[7]; //req
                var _postal = values[8]; //req
                var _subdivision = values[9]; //req
                var _yearStayStarted = values[10];
                var _contactName1 = values[11];  //req in system (either 1)
                var _contactNumber1 = values[12];  //req in system (either 1)
                var _contactName2 = values[13];  //req in system (either 1)
                var _contactNumber2 = values[14];  //req in system (either 1)
                var _contactName3 = values[15];  //req in system (either 1)
                var _contactNumber3 = values[16];  //req in system (either 1)
                var _gender = values[17]; //req
                var _birthdate = values[18]; //req
                var _civilStatus = values[19]; //req in system
                var _religion = values[20];
                var _educationalAttainment = values[21];
                var _idType1 = values[22]; //req in system (either 1)
                var _idNumber1 = values[23]; //req in system (either 1)
                var _idDateIssued1 = values[24]; //req in system (either 1)
                var _idPlaceIssued1 = values[25]; //req in system (either 1)
                var _idType2 = values[26]; //req in system (either 1)
                var _idNumber2 = values[27]; //req in system (either 1)
                var _idDateIssued2 = values[28]; //req in system (either 1)
                var _idPlaceIssued2 = values[29]; //req in system (either 1)
                var _idType3 = values[30]; //req in system (either 1)
                var _idNumber3 = values[31]; //req in system (either 1)
                var _idDateIssued3 = values[32]; //req in system (either 1)
                var _idPlaceIssued3 = values[33]; //req in system (either 1)
                var _spouseFirstName = values[34];
                var _spouseMiddleName = values[35];
                var _spouseLastName = values[36];
                var _spouseBusinessAddress = values[37];
                var _spouseOccupation = values[38];
                var _spouseEmployerAddress = values[39];
                var _spouseOfficePhone = values[40];
                var _spouseBirthdate = values[41];
                var _creditLimit = values[42]; //req
                var _beneficiaryLastName = values[43]; //req
                var _beneficiaryFirstName = values[44]; //req
                var _beneficiaryMiddleName = values[45]; //req
                var _beneficiaryRelationship = values[46]; //req
                var _beneficiaryBirthdate = values[47]; //req
                var _dependentLastName1 = values[48];
                var _dependentFirstName1 = values[49];
                var _dependentMiddleName1 = values[50];
                var _dependentRelationship1 = values[51];
                var _dependentBirthdate1 = values[52];
                var _dependentLastName2 = values[53];
                var _dependentFirstName2 = values[54];
                var _dependentMiddleName2 = values[55];
                var _dependentRelationship2 = values[56];
                var _dependentBirthdate2 = values[57];
                var _dependentLastName3 = values[58];
                var _dependentFirstName3 = values[59];
                var _dependentMiddleName3 = values[60];
                var _dependentRelationship3 = values[61];
                var _dependentBirthdate3 = values[62];
                var _dependentLastName4 = values[63];
                var _dependentFirstName4 = values[64];
                var _dependentMiddleName4 = values[65];
                var _dependentRelationship4 = values[66];
                var _dependentBirthdate4 = values[67];
                var _dependentLastName5 = values[68];
                var _dependentFirstName5 = values[69];
                var _dependentMiddleName5 = values[70];
                var _dependentRelationship5 = values[71];
                var _dependentBirthdate5 = values[72];
                var _dependentLastName6 = values[73];
                var _dependentFirstName6 = values[74];
                var _dependentMiddleName6 = values[75];
                var _dependentRelationship6 = values[76];
                var _dependentBirthdate6 = values[77];
                var _dependentLastName7 = values[78];
                var _dependentFirstName7 = values[79];
                var _dependentMiddleName7 = values[80];
                var _dependentRelationship7 = values[81];
                var _dependentBirthdate7 = values[82];
                var _dependentLastName8 = values[83];
                var _dependentFirstName8 = values[84];
                var _dependentMiddleName8 = values[85];
                var _dependentRelationship8 = values[86];
                var _dependentBirthdate8 = values[87];
                var _dependentLastName9 = values[88];
                var _dependentFirstName9 = values[89];
                var _dependentMiddleName9 = values[90];
                var _dependentRelationship9 = values[91];
                var _dependentBirthdate9 = values[92];
                var _dependentLastName10 = values[93];
                var _dependentFirstName10 = values[94];
                var _dependentMiddleName10 = values[95];
                var _dependentRelationship10 = values[96];
                var _dependentBirthdate10 = values[97];
                var _businessName = values[98]; //req in system
                var _businessAddress = values[99];  //req in system
                var _businessYearStayStarted = values[100];  //req in system
                var _businessYearStarted = values[101];  //req in system
                var _loanCyle = values[102];
                var _cbuBalance = values[103];
                var _dateOfCreditInvestigation = "";
                var _title = values[105];
                var _statusType = values[106];
                //old template fix
                DateTime d;
                if(!DateTime.TryParse(values[104],out d)) {
                    _dateOfCreditInvestigation = DateTime.Now.AddDays(-1).ToString();
                    _title = values[104];
                    _statusType = values[105];
                } else {
                    _dateOfCreditInvestigation = d.ToString();
                    _title = values[105];
                    _statusType = values[106];
                }
                
                


                // required validation

                Boolean passedValidation = true;
                if(_firstName == null){
                    errors.Add(" First name is required");
                    passedValidation = false;
                }
                if(_middleName == null){
                    errors.Add(" Middle name is required");
                    passedValidation = false;
                }
                if(_lastName == null){
                    errors.Add(" Last name is required");
                    passedValidation = false;
                }
                
                addRegion regionItem = null;
                addProvince provinceItem = null;
                addMunicipality municipalityItem = null;
                addBarangay barangayItem = null;

                if(_region == null){
                    errors.Add(" Region is required");
                    passedValidation = false;
                } else {
                    regionItem = regionList.Find(x=>x.name == _region.Replace("\"",""));
                    if(regionItem==null) {
                        errors.Add(" Region name is invalid");
                    } else {

                        if(_province == null){
                            errors.Add(" Province is required");
                            passedValidation = false;
                        } else {
                            provinceItem = provinceList.Find(x=>x.name == _province.Replace("\"","") && x.regionSlug == regionItem.slug);
                            if(provinceItem==null) {
                                errors.Add(" Province name is invalid");
                            } else {
                                if(_municipality == null){
                                    errors.Add(" Municipality is required");
                                    passedValidation = false; 
                                } else {
                                    municipalityItem = municipalityList.Find(x=>x.name == _municipality.Replace("\"","") && x.provinceSlug == provinceItem.slug);
                                    if(municipalityItem==null) {
                                        errors.Add(" Municipality name is invalid");
                                        passedValidation = false;
                                    } else {
                                        if(_barangay == null){
                                            errors.Add(" Barangay name is required");
                                            passedValidation = false;
                                        } else {
                                            barangayItem = barangayList.Find(x=>x.name == _barangay.Replace("\"","") && x.municipalitySlug == municipalityItem.slug);
                                            if(barangayItem==null) {
                                                errors.Add(" Barangay name is invalid");
                                                passedValidation = false;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                
                

                
                
                
                
                
                if(_street == null){
                    errors.Add(" Street is required");
                    passedValidation = false;
                }
                if(_postal == null){
                    errors.Add(" Postal is required");
                    passedValidation = false;
                }
                if(_subdivision == null){
                    errors.Add(" Subdivision is required");
                    passedValidation = false;
                }
                if(_gender == null){
                    errors.Add(" Gender is required");
                    passedValidation = false;
                }
                if(_birthdate == null){
                    errors.Add(" Birthdate is required");
                    passedValidation = false;
                }
                if(_creditLimit == null){
                    errors.Add(" Credit Limit is required");
                    passedValidation = false;
                }
                if(_beneficiaryLastName == null){
                    errors.Add(" Beneficiary Last name is required");
                    passedValidation = false;
                }
                if(_beneficiaryFirstName == null){
                    errors.Add(" Beneficiary First name is required");
                    passedValidation = false;
                }
                if(_beneficiaryMiddleName == null){
                    errors.Add(" Beneficiary Middle name is required");
                    passedValidation = false;
                }
                if(_beneficiaryRelationship == null){
                    errors.Add(" Beneficiary relationship is required");
                    passedValidation = false;
                }
                if(_beneficiaryBirthdate == null){
                    errors.Add(" Beneficiary birthdate is required");
                    passedValidation = false;
                }

                //bypass middles
                if(_dependentFirstName1!=null && _dependentLastName1 !=null && _dependentBirthdate1 !=null && _dependentRelationship1 !=null) {
                    _dependentMiddleName1 = _dependentMiddleName1 == null ? "-" : _dependentMiddleName1;
                }
                if(_dependentFirstName2!=null && _dependentLastName2 !=null && _dependentBirthdate2 !=null && _dependentRelationship2 !=null) {
                    _dependentMiddleName2 = _dependentMiddleName2 == null ? "-" : _dependentMiddleName2;
                }
                if(_dependentFirstName3!=null && _dependentLastName3 !=null && _dependentBirthdate3 !=null && _dependentRelationship3 !=null) {
                    _dependentMiddleName3 = _dependentMiddleName3 == null ? "-" : _dependentMiddleName3;
                }
                if(_dependentFirstName4!=null && _dependentLastName4 !=null && _dependentBirthdate4 !=null && _dependentRelationship4 !=null) {
                    _dependentMiddleName4 = _dependentMiddleName4 == null ? "-" : _dependentMiddleName4;
                }
                if(_dependentFirstName5!=null && _dependentLastName5 !=null && _dependentBirthdate5 !=null && _dependentRelationship5 !=null) {
                    _dependentMiddleName5 = _dependentMiddleName5 == null ? "-" : _dependentMiddleName5;
                }
                if(_dependentFirstName6!=null && _dependentLastName6 !=null && _dependentBirthdate6 !=null && _dependentRelationship6 !=null) {
                    _dependentMiddleName6 = _dependentMiddleName6 == null ? "-" : _dependentMiddleName6;
                }
                if(_dependentFirstName7!=null && _dependentLastName7 !=null && _dependentBirthdate7 !=null && _dependentRelationship7 !=null) {
                    _dependentMiddleName7 = _dependentMiddleName7 == null ? "-" : _dependentMiddleName7;
                }
                if(_dependentFirstName8!=null && _dependentLastName8 !=null && _dependentBirthdate8 !=null && _dependentRelationship8 !=null) {
                    _dependentMiddleName8 = _dependentMiddleName8 == null ? "-" : _dependentMiddleName8;
                }
                if(_dependentFirstName9!=null && _dependentLastName9 !=null && _dependentBirthdate9 !=null && _dependentRelationship9 !=null) {
                    _dependentMiddleName9 = _dependentMiddleName9 == null ? "-" : _dependentMiddleName9;
                }
                if(_dependentFirstName10!=null && _dependentLastName10 !=null && _dependentBirthdate10 !=null && _dependentRelationship10 !=null) {
                    _dependentMiddleName10 = _dependentMiddleName10 == null ? "-" : _dependentMiddleName10;
                }
                
                if(!(_dependentFirstName1 == null && 
                    _dependentMiddleName1 == null &&
                    _dependentLastName1 == null &&
                    _dependentBirthdate1 == null &&
                    _dependentRelationship1 == null) || 
                    (_dependentFirstName1 != null && 
                    _dependentMiddleName1 != null &&
                    _dependentLastName1 != null &&
                    _dependentBirthdate1 != null &&
                    _dependentRelationship1 != null)
                    ) {
                    if(_dependentFirstName1==null) {errors.Add(" Dependent 1 Firstname is required");passedValidation = false;}
                    if(_dependentMiddleName1==null) {errors.Add(" Dependent 1 Middle is required");passedValidation = false;}
                    if(_dependentLastName1==null) {errors.Add(" Dependent 1 LastName is required");passedValidation = false;}
                    if(_dependentBirthdate1==null) {errors.Add(" Dependent 1 Birthdate is required");passedValidation = false;}
                    if(_dependentRelationship1==null) {errors.Add(" Dependent 1 Relationship is required");passedValidation = false;}
                    
                }

                if(!(_dependentFirstName2 == null && 
                    _dependentMiddleName2 == null &&
                    _dependentLastName2 == null &&
                    _dependentBirthdate2 == null &&
                    _dependentRelationship2 == null) || 
                    (_dependentFirstName2 != null && 
                    _dependentMiddleName2 != null &&
                    _dependentLastName2 != null &&
                    _dependentBirthdate2 != null &&
                    _dependentRelationship2 != null)
                    ) {
                    if(_dependentFirstName2==null) {errors.Add(" Dependent 2 Firstname is required");passedValidation = false;}
                    if(_dependentMiddleName2==null) {errors.Add(" Dependent 2 Middle is required");passedValidation = false;}
                    if(_dependentLastName2==null) {errors.Add(" Dependent 2 LastName is required");passedValidation = false;}
                    if(_dependentBirthdate2==null) {errors.Add(" Dependent 2 Birthdate is required");passedValidation = false;}
                    if(_dependentRelationship2==null) {errors.Add(" Dependent 2 Relationship is required");passedValidation = false;}
                }

                if(!(_dependentFirstName3 == null && 
                    _dependentMiddleName3 == null &&
                    _dependentLastName3 == null &&
                    _dependentBirthdate3 == null &&
                    _dependentRelationship3 == null) || 
                    (_dependentFirstName3 != null && 
                    _dependentMiddleName3 != null &&
                    _dependentLastName3 != null &&
                    _dependentBirthdate3 != null &&
                    _dependentRelationship3 != null)
                    ) {
                    if(_dependentFirstName3==null) {errors.Add(" Dependent 3 Firstname is required");passedValidation = false;}
                    if(_dependentMiddleName3==null) {errors.Add(" Dependent 3 Middle is required");passedValidation = false;}
                    if(_dependentLastName3==null) {errors.Add(" Dependent 3 LastName is required");passedValidation = false;}
                    if(_dependentBirthdate3==null) {errors.Add(" Dependent 3 Birthdate is required");passedValidation = false;}
                    if(_dependentRelationship3==null) {errors.Add(" Dependent 3 Relationship is required");passedValidation = false;}
                }

                if(!(_dependentFirstName4 == null && 
                    _dependentMiddleName4 == null &&
                    _dependentLastName4 == null &&
                    _dependentBirthdate4 == null &&
                    _dependentRelationship4 == null) || 
                    (_dependentFirstName4 != null && 
                    _dependentMiddleName4 != null &&
                    _dependentLastName4 != null &&
                    _dependentBirthdate4 != null &&
                    _dependentRelationship4 != null)
                    ) {
                    if(_dependentFirstName4==null) {errors.Add(" Dependent 4 Firstname is required");passedValidation = false;}
                    if(_dependentMiddleName4==null) {errors.Add(" Dependent 4 Middle is required");passedValidation = false;}
                    if(_dependentLastName4==null) {errors.Add(" Dependent 4 LastName is required");passedValidation = false;}
                    if(_dependentBirthdate4==null) {errors.Add(" Dependent 4 Birthdate is required");passedValidation = false;}
                    if(_dependentRelationship4==null) {errors.Add(" Dependent 4 Relationship is required");passedValidation = false;}
                }

                if(!(_dependentFirstName5 == null && 
                    _dependentMiddleName5 == null &&
                    _dependentLastName5 == null &&
                    _dependentBirthdate5 == null &&
                    _dependentRelationship5 == null) || 
                    (_dependentFirstName5 != null && 
                    _dependentMiddleName5 != null &&
                    _dependentLastName5 != null &&
                    _dependentBirthdate5 != null &&
                    _dependentRelationship5 != null)
                    ) {
                    if(_dependentFirstName5==null) {errors.Add(" Dependent 5 Firstname is required");passedValidation = false;}
                    if(_dependentMiddleName5==null) {errors.Add(" Dependent 5 Middle is required");passedValidation = false;}
                    if(_dependentLastName5==null) {errors.Add(" Dependent 5 LastName is required");passedValidation = false;}
                    if(_dependentBirthdate5==null) {errors.Add(" Dependent 5 Birthdate is required");passedValidation = false;}
                    if(_dependentRelationship5==null) {errors.Add(" Dependent 5 Relationship is required");passedValidation = false;}
                }

                if(!(_dependentFirstName6 == null && 
                    _dependentMiddleName6 == null &&
                    _dependentLastName6 == null &&
                    _dependentBirthdate6 == null &&
                    _dependentRelationship6 == null) || 
                    (_dependentFirstName6 != null && 
                    _dependentMiddleName6 != null &&
                    _dependentLastName6 != null &&
                    _dependentBirthdate6 != null &&
                    _dependentRelationship6 != null)
                    ) {
                    if(_dependentFirstName6==null) {errors.Add(" Dependent 6 Firstname is required");passedValidation = false;}
                    if(_dependentMiddleName6==null) {errors.Add(" Dependent 6 Middle is required");passedValidation = false;}
                    if(_dependentLastName6==null) {errors.Add(" Dependent 6 LastName is required");passedValidation = false;}
                    if(_dependentBirthdate6==null) {errors.Add(" Dependent 6 Birthdate is required");passedValidation = false;}
                    if(_dependentRelationship6==null) {errors.Add(" Dependent 6 Relationship is required");passedValidation = false;}
                }

                if(!(_dependentFirstName7 == null && 
                    _dependentMiddleName7 == null &&
                    _dependentLastName7 == null &&
                    _dependentBirthdate7 == null &&
                    _dependentRelationship7 == null) || 
                    (_dependentFirstName7 != null && 
                    _dependentMiddleName7 != null &&
                    _dependentLastName7 != null &&
                    _dependentBirthdate7 != null &&
                    _dependentRelationship7 != null)
                    ) {
                    if(_dependentFirstName7==null) {errors.Add(" Dependent 7 Firstname is required");passedValidation = false;}
                    if(_dependentMiddleName7==null) {errors.Add(" Dependent 7 Middle is required");passedValidation = false;}
                    if(_dependentLastName7==null) {errors.Add(" Dependent 7 LastName is required");passedValidation = false;}
                    if(_dependentBirthdate7==null) {errors.Add(" Dependent 7 Birthdate is required");passedValidation = false;}
                    if(_dependentRelationship7==null) {errors.Add(" Dependent 7 Relationship is required");passedValidation = false;}
                }

                if(!(_dependentFirstName8 == null && 
                    _dependentMiddleName8 == null &&
                    _dependentLastName8 == null &&
                    _dependentBirthdate8 == null &&
                    _dependentRelationship8 == null) || 
                    (_dependentFirstName8 != null && 
                    _dependentMiddleName8 != null &&
                    _dependentLastName8 != null &&
                    _dependentBirthdate8 != null &&
                    _dependentRelationship8 != null)
                    ) {
                    if(_dependentFirstName8==null) {errors.Add(" Dependent 8 Firstname is required");passedValidation = false;}
                    if(_dependentMiddleName8==null) {errors.Add(" Dependent 8 Middle is required");passedValidation = false;}
                    if(_dependentLastName8==null) {errors.Add(" Dependent 8 LastName is required");passedValidation = false;}
                    if(_dependentBirthdate8==null) {errors.Add(" Dependent 8 Birthdate is required");passedValidation = false;}
                    if(_dependentRelationship8==null) {errors.Add(" Dependent 8 Relationship is required");passedValidation = false;}
                }

                if(!(_dependentFirstName9 == null && 
                    _dependentMiddleName9 == null &&
                    _dependentLastName9 == null &&
                    _dependentBirthdate9 == null &&
                    _dependentRelationship9 == null) || 
                    (_dependentFirstName9 != null && 
                    _dependentMiddleName9 != null &&
                    _dependentLastName9 != null &&
                    _dependentBirthdate9 != null &&
                    _dependentRelationship9 != null)
                    ) {
                    if(_dependentFirstName9==null) {errors.Add(" Dependent 9 Firstname is required");passedValidation = false;}
                    if(_dependentMiddleName9==null) {errors.Add(" Dependent 9 Middle is required");passedValidation = false;}
                    if(_dependentLastName9==null) {errors.Add(" Dependent 9 LastName is required");passedValidation = false;}
                    if(_dependentBirthdate9==null) {errors.Add(" Dependent 9 Birthdate is required");passedValidation = false;}
                    if(_dependentRelationship9==null) {errors.Add(" Dependent 9 Relationship is required");passedValidation = false;}
                }

                if(!(_dependentFirstName10 == null && 
                    _dependentMiddleName10 == null &&
                    _dependentLastName10 == null &&
                    _dependentBirthdate10 == null &&
                    _dependentRelationship10 == null) || 
                    (_dependentFirstName10 != null && 
                    _dependentMiddleName10 != null &&
                    _dependentLastName10 != null &&
                    _dependentBirthdate10 != null &&
                    _dependentRelationship10 != null)
                    ) {
                    if(_dependentFirstName10==null) {errors.Add(" Dependent 10 Firstname is required");passedValidation = false;}
                    if(_dependentMiddleName10==null) {errors.Add(" Dependent 10 Middle is required");passedValidation = false;}
                    if(_dependentLastName10==null) {errors.Add(" Dependent 10 LastName is required");passedValidation = false;}
                    if(_dependentBirthdate10==null) {errors.Add(" Dependent 10 Birthdate is required");passedValidation = false;}
                    if(_dependentRelationship10==null) {errors.Add(" Dependent 10 Relationship is required");passedValidation = false;}
                }


                //CHECK DUPLICATE TIN
                if(_idType1!=null) {
                    if(_idType1.ToLower() == "tin") {
                        var tin = getTin(_idNumber1);
                        if(tin!=null) {
                            errors.Add(" Duplicate tin " + _idNumber1);
                            passedValidation = false;
                        }
                    }
                }
                
                if(_idType2!=null) {
                    if(_idType2.ToLower() == "tin") {
                        var tin = getTin(_idNumber2);
                        if(tin!=null) {
                            errors.Add(" Duplicate tin " + _idNumber2);
                            passedValidation = false;
                        }
                    }
                }
                
                if(_idType3!=null) {
                    if(_idType3.ToLower() == "tin") {
                        var tin = getTin(_idNumber3);
                        if(tin!=null) {
                            errors.Add(" Duplicate tin " + _idNumber3);
                            passedValidation = false;
                        }
                    }
                }
                
                //CHECK VALID BIRTHDATE DATE
                DateTime _bdate;
                Boolean isValidDate = DateTime.TryParse(_birthdate,out _bdate);
                if(!isValidDate) {
                    errors.Add(" Invalid birthdate");
                    passedValidation = false;
                } else {
                    //CHECK DUPLICATE NAME AND BIRTHDATE
                    var fullnameBirthdate = getFullnameBirthdate(_firstName + " " + _middleName + " " + _lastName, _bdate);
                    if(fullnameBirthdate != null) {
                        errors.Add(" Duplicate name and birthdate.");
                        passedValidation = false;
                    }
                }
                
                //CHECK COMPLETE NAME
                if(_firstName==null || _middleName ==null || _lastName==null) {
                    errors.Add(" Complete name is required");
                    passedValidation = false;
                }

                //CHECK COMPLETE ADDRESS
                if(_region==null || _province==null || _municipality==null || _barangay==null || _street ==null || _postal==null || _subdivision ==null) {
                    errors.Add(" Complete address is required");
                    passedValidation = false;
                }

                if(passedValidation) {
                    //process
                    //accountid
                    var accountId = @String.Format("{0:yyyyMM}",DateTime.Now.Date) + "-" + global.RandomNumber(4) + "-" + global.RandomLetter(4);
                    //slug
                    var slug = _firstName.ToLower() + "-" + _lastName.ToLower() + "-" + global.slugify2(7);
                    //name
                    clientFullname name = new clientFullname {
                        first = _firstName,
                        middle = _middleName,
                        last = _lastName
                    };
                    //fullname
                    var fullname = name.first + " " + name.middle  + " " + name.last;
                    //address
                    List<clientAddress> addresses = new List<clientAddress>();
                    Int32 _yss = 0;
                    Int32.TryParse(_yearStayStarted,out _yss);
                    if(provinceItem==null || municipalityItem==null || barangayItem == null) {

                    } else {
                        clientAddress address = new clientAddress {
                                full = String.Format("{0},{1},{2},{3},{4},{5}",_postal,_street,_barangay,_municipality,_province,_region),
                                region = new region {name = regionItem.name, slug = regionItem.slug},
                                province = new province { name = provinceItem.name , slug=provinceItem.slug},
                                municipality = new municipality { name = municipalityItem.name, slug=municipalityItem.slug},
                                barangay = new barangay { name = barangayItem.name, slug=barangayItem.slug},
                                street = _street,
                                postal = _postal,
                                subdivision = _subdivision,
                                yearStayStarted = _yss
                        };
                        addresses.Add(address);
                        addresses.Add(address);
                    }

                    //contacts

                    // Main phone
                    // Additional phone
                    // Mobile phone
                    // Additional Mobile phone
                    // Fax
                    // Additional Fax
                    // E-mail
                    // Additional e-mail
                    // Social Network Link
                    String[] contactLower = {
                        "Main phone",
                        "Additional phone",
                        "Mobile phone",
                        "Additional Mobile phone",
                        "Fax",
                        "Additional Fax",
                        "E-mail",
                        "Additional e-mail",
                        "Social Network Link"
                    };

                    List<clientContact> contacts = new List<clientContact>();
                    if(_contactName1!=null&&_contactNumber1!=null) {    
                        contactLower.ToList().ForEach(x=>{
                            if(x.ToLower() == _contactName1.ToLower()) {
                                contacts.Add(
                                    new clientContact {
                                        name = x,
                                        number = _contactNumber1
                                    }
                                );
                            }
                        });
                    }
                    if(_contactName2!=null&&_contactNumber2!=null) {
                        contactLower.ToList().ForEach(x=>{
                            if(x.ToLower() == _contactName2.ToLower()) {
                                contacts.Add(
                                    new clientContact {
                                        name = x,
                                        number = _contactNumber2
                                    }
                                );
                            }
                        });
                    }
                    if(_contactName3!=null&&_contactNumber3!=null) {
                        contactLower.ToList().ForEach(x=>{
                            if(x.ToLower() == _contactName3.ToLower()) {
                                contacts.Add(
                                    new clientContact {
                                        name = x,
                                        number = _contactNumber3
                                    }
                                );
                            }
                        });
                    }
                    if(contacts.Count==0){
                        contacts.Add(
                            new clientContact {
                                name = "Main phone",
                                number = "-"
                            }
                        );
                    }
                    //gender
                    String gender = _gender.ToLower();
                    //birthdate 
                    DateTime birthdate = _bdate;
                    //civilStatus
                    // Single
                    // Married
                    // Separated
                    // Divorced
                    // Widow
                    String civilStatus = _civilStatus.ToLower().Trim();
                    
                    if(civilStatus==null) {
                        civilStatus = "married";
                    } else {
                        if(civilStatus == "divorced" || civilStatus == "separated") {
                            civilStatus = "Divorced/Separated";
                        }
                        if(civilStatus == "widowed") {
                            civilStatus = "Widow";
                        }
                    }

                    civilStatus = civilStatus.Substring(0,1).ToUpper() + civilStatus.Substring(1,civilStatus.Length-1);

                    // religion
                    String religion = _religion;
                    if(religion==null) {
                        religion = "Others";
                    } else {

                        switch (religion.ToUpper().Trim()) {
                            case "SEVENTH DAY ADVENTIST":
                                religion = "Seven Day Adventist";
                                break;
                            case "BAPTIST":
                                religion = "Baptist";
                                break;
                            case "BORN AGAIN CHRISTIAN":
                                religion = "Born Again Christian";
                                break;
                            case "IGLESIA FILIPINA INDEPENDIENTE":
                                religion = "Iglesia Filipina Independiente";
                                break;
                            case "IGLESIA NI CRISTO":
                                religion = "Iglesia ni Cristo";
                                break;
                            case "ISLAM":
                                religion = "Islam";
                                break;
                            case "JEHOVAH WITNESSES":
                                religion = "Jehova's Witness";
                                break;
                            case "LATTER DAY SAINT":
                                religion = "Latter Day Saint";
                                break;
                            case "METHODIST":
                                religion = "Methodist";
                                break;
                            case "OTHERS":
                                religion = "Others";
                                break;
                            case "AGLIPAY":
                                religion = "Others";
                                break;
                            case "PENTECOST":
                                religion = "Others";
                                break;
                            case "PROTESTANT":
                                religion = "Protestant";
                                break;
                            case "ROMAN CATHOLIC":
                                religion = "Catholic";
                                break;
                            default:
                                religion = "Others";
                                break;
                        }
                    }
                

                    //educationalAttainment
                    String educationalAttainment = _educationalAttainment;


                    // Elementary Level
                    // Elementary Graduate
                    // High School Level
                    // High School Graduate
                    // College Level
                    // College Graduate

                    // elementary
                    // high school
                    // vocational
                    // collage
                    switch (educationalAttainment.ToLower()) {
                        case "elementary level":
                            educationalAttainment = "elementary";
                            break;
                        case "elementary graduate":
                            educationalAttainment = "elementary";
                            break;
                        case "elementary":
                            educationalAttainment = "elementary";
                            break;
                        case "high school level":
                            educationalAttainment = "high school";
                            break;
                        case "high school graduate":
                            educationalAttainment = "high school";
                            break;
                        case "high school":
                            educationalAttainment = "high school";
                            break;
                        case "college level":
                            educationalAttainment = "college";
                            break;
                        case "college graduate":
                            educationalAttainment = "college";
                            break;
                        case "vocational":
                            educationalAttainment = "vocational";
                            break;
                        default :
                            educationalAttainment = "no education";
                            break;
                    }
                   

                    //ids

                    // CTC
                    // Voter's ID
                    // Cooperative ID
                    // TIN
                    // SSS Card
                    // GSIS Card
                    // Philhealth Card
                    // Senior Citizen Card
                    // UMID
                    // SEC Registration Number
                    // DTI Registration Number
                    // CDA Registration Number
                    var idTypeChecker = new []{_idType1, _idType2, _idType3};
                    for(var ctr= 0;ctr<=idTypeChecker.Length -1;ctr++) {
                        switch (idTypeChecker[ctr]) {
                            case "GSIS Card":
                                idTypeChecker[ctr] = "GSIS";
                                break;
                            case "Senior Citizen Card":
                                idTypeChecker[ctr] = "Senior Citizen card";
                                break;
                            case "SEC Registration Number":
                                idTypeChecker[ctr] = "SEC registration number";
                                break;
                            case "DTI Registration Number":
                                idTypeChecker[ctr] = "DTI registration number";
                                break;
                            default :
                                break;
                        }
                    }
                    List<id> ids = new List<id>();
                    if(_idType1!=null && _idNumber1!=null && _idDateIssued1!=null && _idPlaceIssued1 !=null) {
                        DateTime dateIssued1 = Convert.ToDateTime("01/01/0001");
                        DateTime.TryParse(_idDateIssued1,out dateIssued1);
                        var placeIssued = _idPlaceIssued1 == "" ? "-" : _idPlaceIssued1;
                        
                        ids.Add(new id{
                                type = idTypeChecker[0],
                                number = _idNumber1,
                                dateIssued = dateIssued1,
                                placeIssued = placeIssued
                            }
                        );
                    }
                    if(_idType2!=null && _idNumber2!=null && _idDateIssued2!=null && _idPlaceIssued2 !=null) {
                        DateTime dateIssued2 = Convert.ToDateTime("01/01/0001");
                        DateTime.TryParse(_idDateIssued2,out dateIssued2);
                        var placeIssued = _idPlaceIssued2 == "" ? "-" : _idPlaceIssued2;
                        ids.Add(new id{
                                type = idTypeChecker[1],
                                number = _idNumber2,
                                dateIssued = dateIssued2,
                                placeIssued = placeIssued
                            }
                        );
                    }
                    if(_idType3!=null && _idNumber3!=null && _idDateIssued3!=null && _idPlaceIssued3 !=null) {
                        DateTime dateIssued3 = Convert.ToDateTime("01/01/0001");
                        DateTime.TryParse(_idDateIssued3,out dateIssued3);
                        var placeIssued = _idPlaceIssued3 == "" ? "-" : _idPlaceIssued3;
                        ids.Add(new id{
                                type = idTypeChecker[2],
                                number = _idNumber3,
                                dateIssued = dateIssued3,
                                placeIssued = placeIssued
                            }
                        );
                    }
                    if(ids.Count==0) {
                        ids.Add(new id{
                            type = "Cooperative ID",
                            number = "-",
                            dateIssued = DateTime.Now.Date,
                            placeIssued = "-"
                        });
                    }
                    //spouse
                    DateTime spouseBirthdate = Convert.ToDateTime("01/01/0001");
                    DateTime.TryParse(_spouseBirthdate,out spouseBirthdate);
                    spouse spouse = new spouse();
                    String spouseFull = "";
                    if(_spouseFirstName !=null) {
                        spouseFull += _spouseFirstName + " ";
                    }

                    if(_spouseMiddleName !=null) {
                        spouseFull += _spouseMiddleName + " ";
                    }

                    if(_spouseLastName !=null) {
                        spouseFull += _spouseLastName + " ";
                    }
                    spouseFull = spouseFull.Trim();
                    if(spouseFull=="") {
                        spouseFull = null;
                    }
                    spouse = new spouse{
                        name = new spouseName {
                            first = _spouseFirstName,
                            middle = _spouseMiddleName,
                            last = _spouseLastName,
                            full = spouseFull
                        },
                        businessAddress = _spouseBusinessAddress,
                        occupation = _spouseOccupation,
                        employerAddress = _spouseEmployerAddress,
                        officePhone = _spouseOfficePhone,
                        birthdate = spouseBirthdate
                    };
                    
                    
                    //imageLocation
                    var imageLocation = "";
                    //branchSlug
                    var branchSlug = branchItem.slug;
                    //branchId
                    //var branchId = branchItem.Id;
                    //officerId
                    String officerId = null;
                    //unitId
                    String unitId = null;
                    //centerId
                    String centerId = null;
                    //beneficiary

                    DateTime beneficiaryDate = Convert.ToDateTime("01/01/0001");
                    DateTime.TryParse(_beneficiaryBirthdate,out beneficiaryDate);

                    _beneficiaryRelationship = _beneficiaryRelationship.ToLower();
                  
                    clientBeneficiary beneficiary = new clientBeneficiary{
                        last = _beneficiaryLastName,
                        first = _beneficiaryFirstName,
                        middle = _beneficiaryMiddleName,
                        relationship = _beneficiaryRelationship,
                        birthdate = beneficiaryDate
                    };

                    //dependents
                    List<clientDependents> dependents = new List<clientDependents>();
                    //1
                    if(_dependentFirstName1!=null||_dependentMiddleName1!=null||_dependentLastName1!=null||_dependentRelationship1!=null) {
                        DateTime depBirthdate1 = Convert.ToDateTime("01/01/0001");
                        DateTime.TryParse(_dependentBirthdate1,out depBirthdate1);
                        var rela1 = _dependentRelationship1;
                        if(rela1!="") {
                            rela1 = rela1.ToLower();
                        }
                        dependents.Add(new clientDependents{
                            Id = ObjectId.GenerateNewId().ToString(),
                            last = _dependentLastName1,
                            first = _dependentFirstName1,
                            middle = _dependentMiddleName1,
                            relationship = rela1,
                            birthdate = depBirthdate1
                        });
                    }
                   
                    //2
                    if(_dependentFirstName2!=null||_dependentMiddleName2!=null||_dependentLastName2!=null||_dependentRelationship2!=null) {
                        DateTime depBirthdate2 = Convert.ToDateTime("01/01/0001");
                        DateTime.TryParse(_dependentBirthdate2,out depBirthdate2);
                        var rela2 = _dependentRelationship2;
                        if(rela2!="") {
                            rela2 = rela2.ToLower();
                        }
                        dependents.Add(new clientDependents{
                            Id = ObjectId.GenerateNewId().ToString(),
                            last = _dependentLastName2,
                            first = _dependentFirstName2,
                            middle = _dependentMiddleName2,
                            relationship = rela2,
                            birthdate = depBirthdate2
                        });
                    }
                    //3
                    if(_dependentFirstName3!=null||_dependentMiddleName3!=null||_dependentLastName3!=null||_dependentRelationship3!=null) {
                        DateTime depBirthdate3 = Convert.ToDateTime("01/01/0001");
                        DateTime.TryParse(_dependentBirthdate3,out depBirthdate3);
                        var rela3 = _dependentRelationship3;
                        if(rela3!="") {
                            rela3 = rela3.ToLower();
                        }
                        
                        dependents.Add(new clientDependents{
                            Id = ObjectId.GenerateNewId().ToString(),
                            last = _dependentLastName3,
                            first = _dependentFirstName3,
                            middle = _dependentMiddleName3,
                            relationship = rela3,
                            birthdate = depBirthdate3
                        });
                    }
                    //4
                    if(_dependentFirstName4!=null||_dependentMiddleName4!=null||_dependentLastName4!=null||_dependentRelationship4!=null) {
                        DateTime depBirthdate4 = Convert.ToDateTime("01/01/0001");
                        DateTime.TryParse(_dependentBirthdate4,out depBirthdate4);
                        var rela4 = _dependentRelationship4;
                        if(rela4!="") {
                            rela4 = rela4.ToLower();
                        }
                        dependents.Add(new clientDependents{
                            Id = ObjectId.GenerateNewId().ToString(),
                            last = _dependentLastName4,
                            first = _dependentFirstName4,
                            middle = _dependentMiddleName4,
                            relationship = rela4,
                            birthdate = depBirthdate4
                        });
                    }
                    //5
                    if(_dependentFirstName5!=null||_dependentMiddleName5!=null||_dependentLastName5!=null||_dependentRelationship5!=null) {
                        DateTime depBirthdate5 = Convert.ToDateTime("01/01/0001");
                        DateTime.TryParse(_dependentBirthdate5,out depBirthdate5);
                        var rela5 = _dependentRelationship5;
                        if(rela5!="") {
                            rela5 = rela5.ToLower();
                        }
                        dependents.Add(new clientDependents{
                            Id = ObjectId.GenerateNewId().ToString(),
                            last = _dependentLastName5,
                            first = _dependentFirstName5,
                            middle = _dependentMiddleName5,
                            relationship = rela5,
                            birthdate = depBirthdate5
                        });
                    }
                    //6
                    if(_dependentFirstName6!=null||_dependentMiddleName6!=null||_dependentLastName6!=null||_dependentRelationship6!=null) {
                        DateTime depBirthdate6 = Convert.ToDateTime("01/01/0001");
                        DateTime.TryParse(_dependentBirthdate6,out depBirthdate6);
                        var rela6 = _dependentRelationship6;
                        if(rela6!="") {
                            rela6 = rela6.ToLower();
                        }
                        dependents.Add(new clientDependents{
                            Id = ObjectId.GenerateNewId().ToString(),
                            last = _dependentLastName6,
                            first = _dependentFirstName6,
                            middle = _dependentMiddleName6,
                            relationship = rela6,
                            birthdate = depBirthdate6
                        });
                    }
                    //7
                    if(_dependentFirstName7!=null||_dependentMiddleName7!=null||_dependentLastName7!=null||_dependentRelationship7!=null) {
                        DateTime depBirthdate7 = Convert.ToDateTime("01/01/0001");
                        DateTime.TryParse(_dependentBirthdate7,out depBirthdate7);
                        var rela7 = _dependentRelationship7;
                        if(rela7!="") {
                            rela7 = rela7.ToLower();
                        }
                        dependents.Add(new clientDependents{
                            Id = ObjectId.GenerateNewId().ToString(),
                            last = _dependentLastName7,
                            first = _dependentFirstName7,
                            middle = _dependentMiddleName7,
                            relationship = rela7,
                            birthdate = depBirthdate7
                        });
                    }
                    //8
                    if(_dependentFirstName8!=null||_dependentMiddleName8!=null||_dependentLastName8!=null||_dependentRelationship8!=null) {
                        DateTime depBirthdate8 = Convert.ToDateTime("01/01/0001");
                        DateTime.TryParse(_dependentBirthdate8,out depBirthdate8);
                        var rela8 = _dependentRelationship8;
                        if(rela8!="") {
                            rela8 = rela8.ToLower();
                        }
                        dependents.Add(new clientDependents{
                            Id = ObjectId.GenerateNewId().ToString(),
                            last = _dependentLastName8,
                            first = _dependentFirstName8,
                            middle = _dependentMiddleName8,
                            relationship = rela8,
                            birthdate = depBirthdate8
                        });
                    }
                    //9
                    if(_dependentFirstName9!=null||_dependentMiddleName9!=null||_dependentLastName9!=null||_dependentRelationship9!=null) {
                        DateTime depBirthdate9 = Convert.ToDateTime("01/01/0001");
                        DateTime.TryParse(_dependentBirthdate9,out depBirthdate9);
                        var rela9 = _dependentRelationship9;
                        if(rela9!="") {
                            rela9 = rela9.ToLower();
                        }
                        dependents.Add(new clientDependents{
                            Id = ObjectId.GenerateNewId().ToString(),
                            last = _dependentLastName9,
                            first = _dependentFirstName9,
                            middle = _dependentMiddleName9,
                            relationship = rela9,
                            birthdate = depBirthdate9
                        });
                    }
                    //10
                    if(_dependentFirstName10!=null||_dependentMiddleName10!=null||_dependentLastName10!=null||_dependentRelationship10!=null) {
                        DateTime depBirthdate10 = Convert.ToDateTime("01/01/0001");
                        DateTime.TryParse(_dependentBirthdate10,out depBirthdate10);
                        var rela10 = _dependentRelationship10;
                        if(rela10!="") {
                            rela10 = rela10.ToLower();
                        }
                        dependents.Add(new clientDependents{
                            Id = ObjectId.GenerateNewId().ToString(),
                            last = _dependentLastName10,
                            first = _dependentFirstName10,
                            middle = _dependentMiddleName10,
                            relationship = rela10,  
                            birthdate = depBirthdate10
                        });     
                    }
                    //creditLimit
                    List<creditLimit> creditLimit = new List<creditLimit>();
                    Double _cl = 0.0;
                    var ac= _creditLimit.Replace("\"","").Replace(",","");
                    DateTime _doci = Convert.ToDateTime(_dateOfCreditInvestigation);
                    DateTime.TryParse(_dateOfCreditInvestigation,out _doci);
                    Double.TryParse(ac, out _cl);
                    if(_cl != 0) {
                        creditLimit.Add(new creditLimit{
                            income = Convert.ToDouble(_cl) / 16.8,
                            creditLimitAmount = Convert.ToDouble(_cl),
                            expense = 0,
                            netDisposableIncome = (Convert.ToDouble(_cl) / 16.8) * 0.7,
                            date = _doci
                        });
                    }
                    
                    //business

                    var yss = 0;
                    var ys = 0;
                    Int32.TryParse(_businessYearStayStarted,out yss);
                    Int32.TryParse(_businessYearStarted, out ys);
                    _businessName = _businessName == null ? "-" : _businessName;
                    _businessAddress = _businessAddress == null ? "-" : _businessAddress;

                    business business = new business {
                        name = _businessName,
                        address = _businessAddress,
                        yearStayStarted = yss,
                        yearStarted  = ys
                    };

                    //loanCycle
                    
                    var loanCycle = 0;
                    Int32.TryParse(_loanCyle,out loanCycle);

                    //cbuBalance
                    var cbuBalance = 0.0;
                    _cbuBalance = _cbuBalance.Replace("\"","");
                    Double.TryParse(_cbuBalance, out cbuBalance);

                    //title
                    var title = "Ms";
                    if(title!=null){
                        title = title.ToLower().Replace(".","");
                        title = title.Substring(0,1).ToUpper() + title.Substring(1,title.Length -1).ToLower();
                    }
                    
                    
                    //status
                    status status = new status();
                    String now = String.Format("{0:MMddyyyyHHmmss}",DateTime.Now);
                    status = new status {
                        type="imported",
                        classification = new classification {
                            code = now,
                            name = file.FileName
                        }
                    };
                    // if(values[105].ToLower() =="regular loans") {
                    //     status = new status {
                    //         type = "good",
                    //         classification = new classification {
                    //             code = "50-01",
                    //             name = _statusType
                    //         }
                    //     };
                    // } else if(values[105].ToLower() =="matured loan") {
                    //     status = new status {
                    //         type = "bad",
                    //         classification = new classification {
                    //             code = "05-01",
                    //             name = _statusType
                    //         }
                    //     };
                    // }


                    clients cl = new clients {
                        Id = ObjectId.GenerateNewId().ToString(),
                        accountId = accountId,
                        slug = slug,
                        name = name,
                        fullname = fullname,
                        addresses = addresses,
                        contacts = contacts,
                        gender = gender,
                        birthdate = birthdate,
                        civilStatus = civilStatus,
                        religion = religion,
                        educationalAttainment = educationalAttainment,
                        ids = ids,
                        spouse = spouse,
                        imageLocation = imageLocation,
                        branchId = branchId,
                        branchSlug = branchSlug,
                        officerId = officerId,
                        unitId = unitId,
                        centerId = centerId,
                        beneficiary = beneficiary,
                        dependents = dependents,
                        creditLimit = creditLimit,
                        business = business,
                        loanCycle = loanCycle,
                        cbuBalance = cbuBalance,
                        title = title,
                        status = status,
                        createdAt = DateTime.Now,
                        updatedAt = DateTime.Now
                    };

                    clients.Add(cl);

                    cbuLogs log = new cbuLogs { 
                        Id = ObjectId.GenerateNewId().ToString(), 
                        type = "beginning balance", 
                        slug = cl.slug + '-' + global.slugify(8), 
                        clientId = cl.Id, 
                        amount = cl.cbuBalance, 
                        cbuBalance =+ cl.cbuBalance, 
                        createdAt = DateTime.Now, 
                        updatedAt = DateTime.Now 
                    }; 
 
                    cbuLogs.Add(log); 
                } 

                var mess = "";
                errors.ForEach((x)=>{
                    if(!isEmptyLine) {
                        mess+= x + ",";
                    }
                });
                if(mess.Length > 0) {
                    mess = mess.Substring(1,mess.Length-2) + ".";
                    messages.Add(new messages {
                        message = "Error on row no. (" + lineNo + ") : " + mess
                    });
                }
            }

            if(messages.Count == 0) {
                if(clients.Count > 0) {
                    _db.GetCollection<clients>("clients").InsertMany(clients);
                    _db.GetCollection<cbuLogs>("cbuLogs").InsertMany(cbuLogs);
                    messages.Add(new messages {
                        message = clients.Count + " client(s) of " + branchItem.name + " branch uploaded successfully",
                        success = 1
                    });
                } else {
                    messages.Add(new messages {
                        message = "no clients to upload",
                        success = 0
                    });
                }
            }
            
            return messages;
        }

        public List<messages> importUsers(IFormFile file)
        {
            StreamReader fs = new StreamReader(file.OpenReadStream(), Encoding.UTF8);
            List<String> list = new List<String>();
            List<messages> message = new List<messages>();
            List<users> users = new List<users>();
            while (!fs.EndOfStream)
            {
                list.Add(fs.ReadLine().Replace("?","ñ"));
            }

            var ch = list[0].Split(',');
            if(ch[0]=="BASIC INFO")
            {
                list.RemoveAt(0);
                list.RemoveAt(0);
                var excelLineNo = 2;
                foreach(String ln in list)
                {
                    excelLineNo++;
                    var v = Regex.Split(ln, ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                    for(Int32 ctr=0;ctr<v.Count();ctr++){
                        if(v[ctr]==""){
                            v[ctr] = null;
                        }
                    }

                    var mess = "";

                    //BASIC INFO
                    var emailAddress = v[0];      
                    if(emailAddress==null){
                        mess += "Email Address is required. ";
                    }
                    // else if(true) {
                    //     message.Add(new messages {
                    //        message = "Email Address is invalid"
                    //     });
                    // };
                    var employeeNumber = v[1];
                    if(employeeNumber==null){
                        mess += "Employee Number is required. ";
                    };
                    var nickname = v[2];
                    var title = v[3];
                    if(title==null){
                        mess += "Title is required. ";
                    } else {
                        title = title.ToLower().Replace(".","");
                    };
                    var first = v[4];
                    if(first==null){
                        mess += "Firstname is required. ";
                    };
                    var middle = v[5];
                    if(middle==null){
                        mess += "Middlename is required. ";
                    };
                    var last = v[6];
                    if(last==null){
                        mess += "Lastname is required. ";
                    };
                    var name = new Name {
                        first = first,
                        middle = middle,
                        last = last
                    };
                    var extension = v[7];
                    var mothersMaidenNameFirst = v[8];
                    if(mothersMaidenNameFirst==null){
                        mess += "Mother's maiden firstname is required. ";
                    };
                    var mothersMaidenNameMiddle = v[9];
                    if(mothersMaidenNameMiddle==null){
                        mess += "Mother's maiden middlename is required. ";
                    } else {
                        mothersMaidenNameMiddle = mothersMaidenNameMiddle.Replace(".","");
                    };
                    var mothersMaidenNameLast = v[10];
                    if(mothersMaidenNameLast==null){
                        mess += "Mother's maiden lastname is required. ";
                    };
                    var mothersMaidenName = new mothersMaidenName{
                        first = mothersMaidenNameFirst,
                        middle = mothersMaidenNameMiddle,
                        last = mothersMaidenNameLast,
                        fullname = mothersMaidenNameFirst + " " + mothersMaidenNameMiddle + " " + mothersMaidenNameLast
                    };
                    var birthdate = v[11];
                    if(birthdate==null){
                        mess += "Birthdate is required. ";
                    } else {
                        DateTime d;
                        if(!DateTime.TryParse(birthdate,out d)){
                            mess += "Birthdate is invalid. ";
                        }
                    };
                    var gender = v[12];
                    if(gender==null){
                        mess += "Gender is required. ";
                    } else {
                        gender = gender.Substring(0,1).ToUpper() + gender.Substring(1).ToLower();
                    };
                    var civilStatus = v[13];
                    if(civilStatus==null){
                        mess += "Civil status is required. ";
                    } else {
                        civilStatus = civilStatus.ToLower();
                    };
                    var religion = v[14];
                    var birthplace = v[15];
                    var height = v[16];
                    var weight = v[17];
                    var bloodtype = v[18];
                    var sss = v[19];
                    if(sss==null){
                        mess += "SSS is required. ";
                    };
                    var pagibig = v[20];
                    var philhealth = v[21];
                    var taxcode = v[22];
                    if(taxcode==null){
                        mess += "Tax code is required. ";
                    }  else {
                        taxcode = taxcode.ToLower();
                    };
                    var tin = v[23];
                    var addressLocationPermanent = v[24];
                    addressLocationPermanent = addressLocationPermanent.Replace("\"","");
                    
                    if(addressLocationPermanent==null){
                        mess += "Permanent address is required. ";
                    };
                    var addressLocationPresent = v[25];
                    addressLocationPresent = addressLocationPresent.Replace("\"","");

                    if(addressLocationPresent==null){
                        mess += "Present address is required. ";
                    };
                    var addresses = new List<address> {
                        new address {
                            slug = global.Slug(20,""),
                            type = "Permanent Address",
                            location = addressLocationPermanent,
                            createdAt = DateTime.Now,
                            updatedAt = DateTime.Now
                        },
                        new address {
                            slug = global.Slug(20,""),
                            type = "Present Address",
                            location = addressLocationPresent,
                            createdAt = DateTime.Now,
                            updatedAt = DateTime.Now
                        }
                    };
                    var contactMobile = v[26];
                    if(contactMobile==null){
                        mess += "Mobile number is required. ";
                    };
                    var contactLandline = v[27];
                    if(contactLandline==null){
                        contactLandline = null;
                    }
                    List<contact> contacts = new List<contact>{
                        new contact {
                            slug = global.Slug(20,""),
                            mobile = contactMobile,
                            landline = contactLandline,
                            createdAt = DateTime.Now,
                            updatedAt = DateTime.Now
                        }
                    };
                   

                    var emergencyContactPersons = new List<emergencyContactPerson>();
                    if(v[28]!=null)
                    {
                         var emergencyContactPerson = v[28];
                        var emergencyContactNumber = v[29];
                        var emergencyContactAddress = v[30];
                        var emergencyContactRelationship = v[31];
                        emergencyContactPersons = new List<emergencyContactPerson> {
                            new emergencyContactPerson {
                                slug = global.Slug(20,""),
                                contactPerson = emergencyContactPerson,
                                contactNumber = emergencyContactNumber,
                                address = emergencyContactAddress,
                                relationship = emergencyContactRelationship,
                                createdAt = DateTime.Now,
                                updatedAt = DateTime.Now    
                            }
                        };
                    };
                    
                    var languagesSpoken = new List<language>();
                    if (v[32]!=null)
                    {
                        var languageSpokenName = v[32];
                        languagesSpoken = new List<language>{
                            new language {
                                slug = global.Slug(20,""),
                                name = languageSpokenName,
                                createdAt = DateTime.Now,
                                updatedAt = DateTime.Now
                            }
                        };
                    }
                    
                    var skills = new List<skill>();
                    if(v[33]!=null)
                    {
                        var skillType = v[33];
                        skills = new List<skill>{
                            new skill {
                                slug = global.Slug(20,""),
                                type = skillType,
                                createdAt = DateTime.Now,
                                updatedAt = DateTime.Now
                            }
                        };
                    }
                    //EMPLOYMENT
                    var position = v[34];
                    if(position==null){
                        mess += "Position is required. ";
                    };
                    var isHeadOffice = v[35];
                    if(isHeadOffice==null) {
                        isHeadOffice = "false";
                    } else {
                        if(isHeadOffice=="yes") {
                            isHeadOffice = "true";
                        } else {
                            isHeadOffice = "false";
                        }
                    }
                    var jobGrade = v[36];
                    var jobClassification = v[37];
                    var jobCategory = v[38];
                    var group = v[39];
                    var department = v[40];
                    var division = v[41];
                    var section = v[42];
                    var branch = v[43];
                    var salary = v[44];
                    var dateHired = v[45];
                    if(dateHired==null){
                        mess += "Date Hired is required. ";
                    } else {
                        DateTime d;
                        if(!DateTime.TryParse(dateHired,out d)){
                            mess += "Date Hired is invalid. ";
                        }
                    };
                    if(salary==null){
                        mess += "Salary is required. ";
                    } else {
                        salary = encryptDecrypt.EncryptString(salary);
                    };
                    var status = v[46];
                    var effectivityDateFrom = v[47];
                    if(effectivityDateFrom==null){
                        mess += "Effectivity Date From is required. ";
                    } else {
                        DateTime d;
                        if(!DateTime.TryParse(effectivityDateFrom,out d)){
                            mess += "Effectivity Date From is invalid. ";
                        }
                    };
                    
                    var allowances = new List<allowance>();
                    if(v[48]!=null)
                    {
                        var allowanceName = v[48];
                        var allowanceAmount = v[49];
                        if(allowanceAmount==null) {
                            allowanceAmount = "0";
                        }
                        var allowanceEffectivityFrom = v[50];
                        if(allowanceEffectivityFrom==null){
                            mess += "Allowance Effectivity Date From is required. ";
                        } else {
                            DateTime d;
                            if(!DateTime.TryParse(allowanceEffectivityFrom,out d)){
                                mess += "Allowance Effectivity Date From is invalid. ";
                            }
                        };
                        var allowanceEffectivityTo = v[51];
                        if(allowanceEffectivityTo==null){
                            mess += "Allowance Effectivity Date To is required. ";
                        } else {
                            DateTime d;
                            if(!DateTime.TryParse(allowanceEffectivityTo,out d)){
                                mess += "Allowance Effectivity Date To is invalid. ";
                            }
                        };
                        var isAllowanceTemporary = v[52];
                        if(isAllowanceTemporary==null) {
                            isAllowanceTemporary = "false";
                        } else {
                            if(isAllowanceTemporary.ToLower() == "yes") {
                                isAllowanceTemporary = "true";
                            } else {
                                isAllowanceTemporary = "false";
                            }
                        }
                        allowances = new List<allowance> {
                            new allowance {
                                name = allowanceName,
                                amount = Convert.ToDouble(allowanceAmount),
                                temporary = Convert.ToBoolean(isAllowanceTemporary),
                                effectivityDateFrom = Convert.ToDateTime(allowanceEffectivityFrom),
                                effectivityDateTo = Convert.ToDateTime(allowanceEffectivityTo),
                            }
                        };
                    }
                    

                    var benefits = new List<benefit>();

                    if(v[53]!=null)
                    {
                            var benefitName = v[53];
                            var benefitAmount = v[54];
                            if(benefitAmount==null) {
                                benefitAmount = "0";
                            }
                            var benefitEffectivityFrom = v[55];
                            if(benefitEffectivityFrom==null){
                                mess += "Benefit Effectivity Date From is required. ";
                            } else {
                                DateTime d;
                                if(!DateTime.TryParse(benefitEffectivityFrom,out d)){
                                    mess += "Benefit Effectivity Date From is invalid. ";
                                }
                            };
                            var benefitEffectivityTo = v[56];
                            if(benefitEffectivityTo==null){
                                mess += "Benefit Effectivity Date To is required. ";
                            } else {
                                DateTime d;
                                if(!DateTime.TryParse(benefitEffectivityTo,out d)){
                                    mess += "Benefit Effectivity Date To is invalid. ";
                                }
                            };

                            benefits = new List<benefit> {
                                new benefit {
                                    name = benefitName,
                                    amount = Convert.ToDouble(benefitAmount),
                                    effectivityDateFrom = Convert.ToDateTime(benefitEffectivityFrom),
                                    effectivityDateTo = Convert.ToDateTime(benefitEffectivityTo)
                                }
                            };
                    }
                    
                    var _region = "";
                    var _section = "";

                    

                    if(branch==null) {
                        _region = null;
                        _section = null;
                    } else {
                        group = "Credit Operations Group";
                        String x = getAreaRegionNamesSepByComma(branch);
                        if(x=="") {
                            branch = null;
                            _region = null;
                            _section = null;
                        } else {
                            branch = x.Split(',')[0];
                            _region = x.Split(',')[1];
                            _section = x.Split(',')[2];
                        }
                        
                    }
                    
                    var employment = new employment {
                            position = position,
                            group = group,
                            department = department,
                            division = division,
                            section = section,
                            region = _region,
                            area = _section,
                            branch = branch,
                            unit = "",
                            jobGrade = jobGrade,
                            jobCategory = jobCategory,
                            jobClassification = jobClassification,
                            status = status,
                            effectivityDateFrom = Convert.ToDateTime(effectivityDateFrom),
                            salary = salary,
                            allowances = allowances,
                            benefits = benefits,
                            isHeadOffice = Convert.ToBoolean(isHeadOffice),
                            createdAt = DateTime.Now,
                            updatedAt = DateTime.Now
                        };
                    //WORK SCHEDULE
                    var scheduleTimeInAm = v[57];
                    var scheduleTimeOutAm = v[58];
                    var scheduleTimeInPm = v[59];
                    var scheduleTimeOutPm = v[60];
                    var scheduleEffectivityDateFrom = v[61];
                    var scheduleEffectivityDateTo = v[62];
                    if(scheduleTimeInAm==null){
                        mess += "Time In AM is required. ";
                    } else {
                        scheduleTimeInAm = Convert.ToDateTime(DateTime.Now.Date.ToString("MM/dd/yyyy") + " " + scheduleTimeInAm).ToString("HH:mm");
                    };
                    if(scheduleTimeOutAm==null){
                        mess += "Time Out AM is required. ";
                    } else {
                        scheduleTimeOutAm = Convert.ToDateTime(DateTime.Now.Date.ToString("MM/dd/yyyy") + " " + scheduleTimeOutAm).ToString("HH:mm");
                    };
                    if(scheduleTimeInPm==null){
                        mess += "Time In PM is required. ";
                    } else {
                        scheduleTimeInPm = Convert.ToDateTime(DateTime.Now.Date.ToString("MM/dd/yyyy") + " " + scheduleTimeInPm).ToString("HH:mm");
                    };
                    if(scheduleTimeOutPm==null){
                        mess += "Time Out PM is required. ";
                    } else {
                        scheduleTimeOutPm = Convert.ToDateTime(DateTime.Now.Date.ToString("MM/dd/yyyy") + " " + scheduleTimeOutPm).ToString("HH:mm");
                    };
                    if(scheduleEffectivityDateFrom==null){
                        mess += "Schedule Effectivity Date From is required. ";
                    } else {
                        DateTime d;
                        if(!DateTime.TryParse(scheduleEffectivityDateFrom,out d)){
                            mess += "Schedule Effectivity Date From is invalid. ";
                        }
                    };
                    if(scheduleEffectivityDateTo==null){
                        mess += "Schedule Effectivity Date To is required. ";
                    } else {
                        DateTime d;
                        if(!DateTime.TryParse(scheduleEffectivityDateTo,out d)){
                            mess += "Schedule Effectivity Date To is invalid. ";
                        }
                    };
                    List<employeeSchedule> employeeSchedules = new List<employeeSchedule> {
                        new employeeSchedule {
                            slug = global.Slug(20,""),
                            timeInAm = scheduleTimeInAm,
                            timeOutAm = scheduleTimeOutAm,
                            timeInPm = scheduleTimeInPm,
                            timeOutPm = scheduleTimeOutPm,
                            effectivityDateFrom = Convert.ToDateTime(scheduleEffectivityDateFrom),
                            effectivityDateTo = Convert.ToDateTime(scheduleEffectivityDateTo),
                            createdAt = DateTime.Now,
                            updatedAt = DateTime.Now
                        }
                    };
                    //FAMILY
                    var familyFirst = v[63];
                    var familyMiddle = v[64];
                    var familyLast = v[65];
                    var familyBirthdate = v[66];
                    var relationship = v[67];
                    var occupation = v[68];
                    var contact = v[69];
                    var highestEducationalAttainment = v[70];
                    List<String> dependents = new List<String>();
                    var sssDependent = v[71];
                    List<familyMember> family = new List<familyMember>();
                    if(familyFirst!=null)
                    {
                        if(sssDependent!=null){
                            if(sssDependent.ToLower() == "yes"){
                                dependents.Add("sss");
                            }
                        }
                        var philhealthDependent = v[72];
                        if(philhealthDependent!=null){
                            if(philhealthDependent== "yes"){
                                dependents.Add("philhealth");
                            }
                        }
                        var birDependent = v[73];
                        if(birDependent!=null){
                            if(birDependent == "yes"){
                                dependents.Add("bir");
                            };
                        }
                        var pagibigDependent = v[74];
                        if(pagibigDependent!=null){
                            if(pagibigDependent == "yes"){
                                dependents.Add("pagibig");
                            }
                        }

                        family.Add(new familyMember {
                            slug = global.Slug(20,""),
                            last = familyLast,
                            first = familyFirst,
                            middle = familyMiddle,
                            relationship = relationship,
                            birthdate = Convert.ToDateTime(familyBirthdate),
                            occupation = occupation,
                            contact = contact,
                            dependents = dependents,
                            createdAt = DateTime.Now,
                            updatedAt = DateTime.Now
                        });
                    }
                    //EDUCATION
                    var educationType = v[75];
                    var notYetGraduated = v[76];
                    var yearLevel = v[77];
                    var noOfUnits = v[78];
                    var school = v[79];
                    var address = v[80];
                    var inclusiveFrom = v[81];
                    var inclusiveTo = v[82];
                    var degree = v[83];
                    var major = v[84];
                    var award = v[85];
                    List<education> educations = new List<education>();
                    if(educationType!=null)
                    {
                        if(notYetGraduated == "yes"){
                            notYetGraduated = "false";
                        } else {
                            notYetGraduated = "true";
                        }

                        Int32 i;
                        if(!Int32.TryParse(yearLevel,out i)) {
                            yearLevel = "0";
                        }

                        if(!Int32.TryParse(noOfUnits,out i)) {
                            noOfUnits = "0";
                        }

                        if(inclusiveFrom==null){
                            mess += "Inclusive From is required. ";
                        } else {
                            Int32 i1;
                            if(!Int32.TryParse(inclusiveFrom,out i1)){
                                mess += "Inclusive From is invalid. ";
                            }
                        };

                        if(inclusiveTo==null){
                            mess += "Inclusive To is required. ";
                        } else {
                            Int32 i2;
                            if(!Int32.TryParse(scheduleEffectivityDateTo,out i2)){
                                mess += "Inclusive To is invalid. ";
                            }
                        };
                        educations.Add(new education{
                            slug = global.Slug(20,""),
                            educationType = educationType,
                            school = school,
                            address = address,
                            inclusiveDateFrom = Convert.ToInt32(inclusiveFrom),
                            inclusiveDateTo = Convert.ToInt32(inclusiveTo),
                            degree = degree,
                            major = major,
                            award = award,
                            notYetGraduated = Convert.ToBoolean(notYetGraduated),
                            noOfUnits = Convert.ToInt32(noOfUnits),
                            yearLevel = Convert.ToInt32(yearLevel),
                            documents = new List<document>()
                        });
                    }
                    //TRAINING
                    var trainingTitle = v[86];
                    var sponsor = v[87];
                    var venue = v[88];
                    var trainingInclusiveFrom = v[89];
                    var trainingInclusiveTo = v[90];
                    var isResourceSpeaker = v[91];
                    var personalCost = v[92];
                    var noOfHours = v[93];
                    var trainingCost = v[94];
                    List<training> trainings = new List<training>();
                    if(trainingTitle!=null) {
                        if(trainingInclusiveFrom==null){
                            mess += "Training Inclusive From is required. ";
                        } else {
                            Int32 d;
                            if(!Int32.TryParse(trainingInclusiveFrom,out d)){
                                mess += "Training Inclusive From is invalid. ";
                            }
                        };

                         if(trainingInclusiveTo==null){
                            mess += "Training Inclusive To is required. ";
                        } else {
                            Int32 d;
                            if(!Int32.TryParse(trainingInclusiveTo,out d)){
                                mess += "Training Inclusive To is invalid. ";
                            }
                        };

                        if(personalCost==null){
                            personalCost = "0";
                        }
                        if(noOfHours==null){
                            noOfHours = "0";
                        }
                        if(trainingCost==null){
                            trainingCost = "0";
                        }
                        trainings.Add(new training {
                            slug = global.Slug(20,""),
                            title = trainingTitle,
                            dateFrom = Convert.ToDateTime(trainingInclusiveFrom),
                            dateTo = Convert.ToDateTime(trainingInclusiveTo),
                            sponsor = sponsor,
                            venue = venue,
                            isResourceSpeaker = Convert.ToBoolean(isResourceSpeaker),
                            cost = Convert.ToDouble(trainingCost),
                            personalCost = Convert.ToDouble(personalCost),
                            noOfHours =Convert.ToInt32(noOfHours),
                            documents = new List<document>(),
                            createdAt = DateTime.Now,
                            updatedAt = DateTime.Now
                        });
                    }
                    //PREVIOUS EMPLOYMENT
                    var preEmpCompany = v[95];
                    var preEmpAddress = v[96];
                    var preEmpTelNo = v[97];
                    var preEmpInclusiveFrom = v[98];
                    var preEmpInclusiveTo = v[99];
                    var preEmpPosition = v[100];
                    var preEmpStatus = v[101];
                    var preEmpSalaryStart = v[102];
                    var preEmpSalaryEnd = v[103];
                    var preEmpReasonOfSeverance = v[104];
                    var preEmpImmediateSupervisor = v[105];
                    var preEmpJobFunction = v[106];
                    List<previousEmployment> previousEmployments = new List<previousEmployment>();
                    if(preEmpCompany!=null)
                    {
                        if(preEmpInclusiveFrom==null){
                            mess += "Previous Employment Inclusive From is required. ";
                        } else {
                            Int32 i1;
                            if(!Int32.TryParse(preEmpInclusiveFrom,out i1)){
                                mess += "Previous Employment Inclusive From is invalid. ";
                            }
                        };

                        if(preEmpInclusiveTo==null){
                            mess += "Previous Employment Inclusive To is required. ";
                        } else {
                            Int32 i2;
                            if(!Int32.TryParse(preEmpInclusiveTo,out i2)){
                                mess += "Previous Employment Inclusive To is invalid. ";
                            }
                        };

                        if(preEmpSalaryStart==null)
                        {
                            preEmpSalaryStart = "0";
                        }

                        if(preEmpSalaryEnd==null)
                        {
                            preEmpSalaryEnd = "0";
                        }

                        previousEmployments.Add(new previousEmployment {
                            slug = global.Slug(20,""),
                            company = preEmpCompany,
                            address = preEmpAddress,
                            telNo = preEmpTelNo,
                            inclusiveDateFrom = Convert.ToDateTime(preEmpInclusiveFrom),
                            inclusiveDateTo = Convert.ToDateTime(preEmpInclusiveTo),
                            position = preEmpPosition,
                            statusOfEmployment = preEmpStatus,
                            salaryStart = Convert.ToDouble(preEmpSalaryStart),
                            salaryEnd = Convert.ToDouble(preEmpSalaryEnd),
                            reasonOfSeverance = preEmpReasonOfSeverance,
                            immediateSupervisor = preEmpImmediateSupervisor,
                            jobFunction = preEmpJobFunction,
                            documents = new List<document>(),
                            createdAt = DateTime.Now,
                            updatedAt = DateTime.Now
                        });
                    }
                    //EMPLOYEE RELATION
                    var empRelDocketNumber = v[107];
                    var empRelCocViolation = v[108];
                    var empRelNatureOfOffense = v[109];
                    var empRelDateOfOffense = v[110];

                    var empRelDateOfResolution = v[111];
                   
                    var empRelSanction = v[112];
                    var empRelCaseStatus = v[113];
                    List<employeeRelation> employeeRelations = new List<employeeRelation>();
                    if(empRelDocketNumber!=null)
                    {
                        if(empRelDateOfOffense==null){
                            mess += "Date of offense is required. ";
                        } else {
                            Int32 i2;
                            if(!Int32.TryParse(empRelDateOfOffense,out i2)){
                                mess += "Date of offense is invalid. ";
                            }
                        };

                        if(empRelDateOfResolution==null){
                            mess += "Date of resolution is required. ";
                        } else {
                            Int32 i2;
                            if(!Int32.TryParse(empRelDateOfResolution,out i2)){
                                mess += "Date of resolution is invalid. ";
                            }
                        };
                        employeeRelations.Add(new employeeRelation {
                            docketNumber = empRelDocketNumber,
                            cocViolation = empRelCocViolation,
                            natureOfOffense = empRelNatureOfOffense,
                            dateOfOffense = Convert.ToDateTime(empRelDateOfOffense),
                            dateOfResolution = Convert.ToDateTime(empRelDateOfResolution),
                            sanction = empRelSanction,
                            caseStatus = empRelCaseStatus,
                            documents = new List<document>(),
                            createdAt = DateTime.Now,
                            updatedAt = DateTime.Now
                        });
                    }

                    //leave computation
                    leaves leaves = new leaves();
                    Double remai = 0.0;
                    if (employment.status.ToLower() == "regular") {
                        remai = 15.0;
                    }
                    leaves.types = new List<types> {
                        new types {slug="sickLeaveSlug", type="Sick Leave", remaining = remai},
                        new types {slug="sickLeaveSlug", type="Vacation Leave", remaining = remai}
                    };

                    //check if employeeCode is existing

                    if(checkIfEmployeeCodeIsExisting(employeeNumber)) {
                        mess += "Employee Code " + employeeNumber + " already exists. [name of template: " + name.first + ", " + name.middle + ", " + name.last + "]";
                    }

                    //
                    if(mess!=""){
                        //excelLineNo ++;
                        message.Add(new messages{
                            message = mess + " - [row no. " + excelLineNo + "]"
                        });
                    } else {
                        
                        users.Add(new users{
                            slug = global.Slug(20,""),
                            email = emailAddress,
                            password = BCrypt.Net.BCrypt.HashPassword("qweasdzxc"),
                            token = global.Slug(30,""),
                            verificationToken = global.Slug(20,""),
                            isActive = 1,
                            isVerified = 1,
                            isPending = false,
                            permissions = new List<String>(),
                            employeeCode = employeeNumber,
                            biometricsCode = employeeNumber.Substring(employeeNumber.Length -4),
                            nickname = nickname,
                            salutation = title,
                            name = name,
                            fullname = first + " " + middle + " " + last,
                            extension = extension,
                            mothersMaidenName = mothersMaidenName,
                            birthdate = Convert.ToDateTime(birthdate),
                            gender = gender,
                            civilStatus = civilStatus,
                            religion = religion,
                            birthplace = birthplace,
                            height = height,
                            weight = weight,
                            bloodType = bloodtype,
                            sss = sss,
                            pagibig = pagibig,
                            philhealth = philhealth,
                            taxCode = taxcode,
                            tin = tin,
                            addresses = addresses,
                            contacts = contacts,
                            emergencyContactPersons = emergencyContactPersons,
                            languagesSpoken = languagesSpoken,
                            skills = skills,
                            createdAt = DateTime.Now,
                            updatedAt = DateTime.Now,
                            employment = employment,
                            dateHired = Convert.ToDateTime(dateHired),
                            initials = first.Substring(0,1).ToUpper() + middle.Substring(0,1).ToUpper() + last.Substring(0,1).ToUpper(),
                            employeeSchedules = employeeSchedules,
                            family = family,
                            educations = educations,
                            trainings = trainings,
                            previousEmployments = previousEmployments,
                            employeeRelations = employeeRelations,
                            employmentHistory = new List<employmentHistoryItem>(),
                            leaves = leaves
                        });

                        message.Add(new messages{
                            message = first + " " + middle + " " + last + " imported succesfully. - [excel row no. " + excelLineNo + "]",
                            success = 1
                        });
                    }
                }

                if(users.Count > 0){
                    _db.GetCollection<users>("users").InsertMany(users);
                }
            }
            else
            {
                message.Add(new messages{message="Invalid Template"});
            }
            
            return message;
        }

        public clients getFullnameBirthdate(String fullname, DateTime? birthdate)
        {
            var query1 = Builders<clients>.Filter.Eq(e => e.fullname, fullname);
            var query2 = Builders<clients>.Filter.Eq(e => e.birthdate, birthdate);
            var query = Builders<clients>.Filter.And(query1, query2);
            var c = _db.GetCollection<clients>("clients").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }

        public clients getTin(String tinNumber)
        {
            // var query = Builders<clients>.Filter.Eq(e => e.tinNumber, tinNumber);
            // var c = _db.GetCollection<clients>("clients").Find(query).ToListAsync();

            // return c.Result.FirstOrDefault();

            var query = Builders<clients>.Filter.And(Builders<clients>.Filter.Eq("ids.number", tinNumber),Builders<clients>.Filter.Eq("ids.type", "TIN"));
            var c = _db.GetCollection<clients>("clients").Find(query).ToListAsync();
            return c.Result.FirstOrDefault();

        }

        public users getSlug(String token)
        {
            var query = Builders<users>.Filter.Eq(e => e.token, token);
            var c = _db.GetCollection<users>("users").Find(query).ToListAsync();

            return c.Result.FirstOrDefault();
        }

        public branch getBranchSlug(String branchId)
        {
            var query = Builders<branch>.Filter.Eq(e => e.Id, branchId);
            var branch = _db.GetCollection<branch>("branch").Find(query).ToListAsync();

            return branch.Result.FirstOrDefault();
        }

        protected bool CheckDate(String date)
        {
            try
            {
                DateTime dt = DateTime.Parse(date);
                return true;
            }
            catch
            {
                return false;
            }
        }

        private Boolean checkIfEmployeeCodeIsExisting(String employeeCode) {
            var query = Builders<users>.Filter.Eq(e => e.employeeCode, employeeCode);
            var users = _db.GetCollection<users>("users").Find(query).FirstOrDefault();
            if(users!=null) {
                return true;
            } else {
                return false;
            };
        }

        private String getAreaRegionNamesSepByComma(String branchName) {
            branch branch;
            List<regions> regions;
            List<areas> areas;
            String strings = "";
            var query1 = Builders<branch>.Filter.Regex("name", new BsonRegularExpression(branchName.ToLower(),"i"));
            var query2 = Builders<branch>.Filter.Regex("name", new BsonRegularExpression((branchName + " BRANCH").ToLower() ,"i"));
            branch = _db.GetCollection<branch>("branch").Find(Builders<branch>.Filter.Or(query1,query2)).FirstOrDefault();
            
            if(branch!=null) {
                regions = _db.GetCollection<regions>("region").Find(Builders<regions>.Filter.Empty).ToList();
                if(regions!=null) {
                    var region = regions.Find(f => f.Id == branch.regionId);
                    if(region!=null) {
                        strings += region.name + ",";
                    } else {
                        strings += ",";
                    }
                } else {
                    strings += ",";
                }
                
                areas = regions.Find(f => f.Id == branch.regionId).areas.ToList();
                if(areas!=null) {
                    var area = areas.Find(a => a.Id == branch.areaId);
                    if(area!=null) {
                        strings += area.name + ",";
                    } else {
                        strings += ",";
                    }
                    
                } else {
                    strings += ",";
                }
                strings = branch.slug + "," + strings;
            }
            
            return strings;
        }
    }
}
