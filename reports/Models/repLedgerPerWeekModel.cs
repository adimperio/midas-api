using System;
using System.Collections.Generic;

namespace kmbi_core_master.reports.Models
{
    public class repLedgerPerWeekModel {
        public String transactionNo {get;set;}
        public String centerName {get;set;}
        public Double loanAmountPrincipal {get;set;}
        public Double loanAmountInterest {get;set;}
        public Double loanBalancePrincipal {get;set;}
        public Double loanBalanceInterest {get;set;}
        public Double cbuBalance {get;set;}
        public List<repLedgerPerWeekLine> repLedgerPerWeekLine {get;set;}    
    }

    public class repLedgerPerWeekLine {
        public Int32 weekNo {get;set;}
        public DateTime repaymentDate {get;set;}
        public DateTime? datePaid {get;set;}
        public String referenceNumber {get;set;}
        public Double paymentPrincipal {get;set;}
        public Double paymentInterest {get;set;}
        public Double balancePrincipal {get;set;}
        public Double balanceInterest {get;set;}
        public Double cbuPayment {get;set;}
        public Double cbuBalance {get;set;}
    }
}