using System;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using System.IO;
using kmbi_core_master.helpers.interfaces;
using kmbi_core_master.coreMaster.Models;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Hosting;

namespace kmbi_core_master.helpers
{
    public class s3Upload : iupload
    {
        private AmazonS3Client client;
        private const String root = "https://s3-ap-southeast-1.amazonaws.com/";
        public s3Upload(IOptions<awsSettings> settings) : base()
        {
            client = new AmazonS3Client(settings.Value.accessKey, settings.Value.secretKey, RegionEndpoint.APSoutheast1);
        }

        public String getRoot() {
            return root;
        }

        public String upload(Byte[] bytes, String fullPath)
        {
            String bucketName = getBucketName(fullPath);
            Stream stream = new MemoryStream(bytes);
            PutObjectResponse response = client.PutObjectAsync(new PutObjectRequest {
                BucketName = bucketName,
                Key = fullPath.Replace(bucketName + "/",""),
                InputStream = stream
            }).Result;

            return root + fullPath;
        }

        public void delete(String fullPath) {
            String bucketName = getBucketName(fullPath);
            DeleteObjectResponse response = client.DeleteObjectAsync(new DeleteObjectRequest {
                BucketName = bucketName,
                Key= fullPath
            }).Result;
        }

        private String getBucketName(String fullPath) {
            return fullPath.Split('/')[0];
        }
    }   
}