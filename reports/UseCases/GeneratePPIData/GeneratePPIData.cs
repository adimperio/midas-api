using kmbi_core_master.coreMaster.IRepository;
using kmbi_core_master.spm.IRepository;
using System.Linq;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using kmbi_core_master.coreMaster.Models;
using System;

namespace kmbi_core_master.reports.UseCases.GeneratePPIData
{
    public class GeneratePPIData : IUseCase<GeneratePPIDataRequest, GeneratePPIDataResponse> {
        public ippiRepository _ppiRepo { get; }
        public ILogger<GeneratePPIData> _logger { get; }
        public iLikelihoodRepository _likelihoodRepo { get; }

        public GeneratePPIData(
            ippiRepository ppiRepo,
            ILogger<GeneratePPIData> logger,
            iLikelihoodRepository likelihoodRepo
        ) {
            _ppiRepo = ppiRepo;
            _logger = logger;
            _likelihoodRepo = likelihoodRepo;
        }
        public GeneratePPIDataResponse Handle(GeneratePPIDataRequest request) {

            _logger.LogInformation("Retrieving Active loans and clients ... ");            
    
            var activeLoans = _ppiRepo.getppiclients(Convert.ToDateTime("1/1/2019"), request.AsOf, "All","All","All","All","All","All");

            _logger.LogInformation("Retrieving PPIs ... ");
            var ppis = _ppiRepo.getAll().ToList();

            _logger.LogInformation("Retrieving Likelihoods ... ");
            var likelihoods = _likelihoodRepo.GetLikelihoods().ToList();
            
            List<PPI> PPIs = new List<PPI>();

            if(activeLoans!=null) {
                activeLoans.ForEach(al=>{
                    al.members.ToList().ForEach(am=>{
                        var ppi = ppis.Where(xy=>xy.client.slug == am.slug).FirstOrDefault();
                        var _name = am.fullname;
                        var _center = al.centerName.Substring(0,8);
                        var _branch = al.branchName.Trim();
                        if(ppi!=null) {
                            var _questionsAnswers = new List<PPIQuestionAnswer>();
                            ppi.questions.ForEach(q=>{
                                _questionsAnswers.Add(new PPIQuestionAnswer {
                                    Question = q.question,
                                    Answer = q.answer.choice
                                });
                            });

                            var _score = ppi.score;

                            var _likelihood = likelihoods.Where(x=>x.Score == _score).FirstOrDefault();

                            PPIs.Add(new PPI {
                                Name = _name,
                                Branch = _branch,
                                Center = _center,
                                QuestionsAnswers = _questionsAnswers.ToArray(),
                                Score = _score,
                                Likelihood = _likelihood
                            });

                        }
                    });
                });
            }
            
            return new GeneratePPIDataResponse {
                AsOf = request.AsOf,
                Details = PPIs.ToArray(),
                PPIScore = new spm.Models.ppiScore {
                    foodScore = PPIs.Sum(xy=>xy.Likelihood.FoodPovertyLine / PPIs.Count) * 100,
                    nationalScore = (PPIs.Sum(xy=>xy.Likelihood.NationalPovertyLine) / PPIs.Count) * 100,
                    onePerDayScore = (PPIs.Sum(xy=>xy.Likelihood._1dot25perDay2005) / PPIs.Count) * 100,
                    twoPerDayScore = (PPIs.Sum(xy=>xy.Likelihood._2dot50perDay2005) / PPIs.Count) * 100
                }
            };
        }
    }


}