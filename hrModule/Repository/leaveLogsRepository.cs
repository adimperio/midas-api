﻿using Microsoft.Extensions.Options;

using System;
using System.Collections.Generic;
using System.Linq;

using MongoDB.Bson;
using MongoDB.Driver;

using kmbi_core_master.coreMaster.Models;
using kmbi_core_master.hrModule.Models;
using kmbi_core_master.hrModule.Interface;

using System.IO;

using kmbi_core_master.helpers.interfaces;

namespace kmbi_core_master.hrModule.Repository
{
    public class leaveLogsRepository : dbCon, ileaveLogsRepository
    {
        
        IMongoCollection<leaveLog> _coll;
        iupload _upload;

        public leaveLogsRepository(IOptions<dbSettings> settings, iupload upload) : base(settings) {
            _coll = _db.GetCollection<leaveLog>("leaveLogs");
            _upload = upload;
        }

        public IEnumerable<leaveLog> getLeaveLogs() {
            var filter = Builders<leaveLog>.Filter.Empty;
            return _coll.Find(filter).ToList();
        }
        
        public leaveLog getLeaveLogBySlug(String slug) {
            var filter = Builders<leaveLog>.Filter.Eq("slug",slug);
            return _coll.Find(filter).FirstOrDefault();
        }

        public Boolean createLeaveLog(leaveLog leaveLog) {
            _coll.InsertOne(leaveLog);
            var l = getLeaveLogBySlug(leaveLog.slug);
            if (l!=null) {
                return true;
            } else {
                return false;
            }
        }
        
        public Boolean updateLeavelogBySlug(String slug, leaveLog leaveLog) {
            var leave = getLeaveLogBySlug(slug);
            var filter = Builders<leaveLog>.Filter.Eq("slug",slug);
            var update = Builders<leaveLog>.Update
                .Set("type", leaveLog.type)
                .Set("code", leaveLog.code)
                .Set("employee", leaveLog.employee)
                .Set("dates", leaveLog.dates)
                .Set("reason", leaveLog.reason)
                .Set("attachmentPath", leaveLog.attachmentPath)
                .Set("receivedBy", leaveLog.receivedBy)
                .Set("withPay", leaveLog.withPay)
                .Set("dateReceived", leaveLog.dateReceived)
                .Set("dateFiled", leaveLog.dateFiled)
                .Set("supervisor",leaveLog.supervisor)
                .Set("comment",leaveLog.comment)
                .Set("status", leaveLog.status)
                .CurrentDate("createdAt")
                .CurrentDate("updatedAt");

            var res = _coll.UpdateOne(filter,update);
            return res.MatchedCount > 0;
        }

        public Boolean deleteLeaveLogBySlug(String slug) {
            var filter = Builders<leaveLog>.Filter.Eq("slug",slug);
            var res = _coll.DeleteOne(filter);
            return res.DeletedCount > 0;
        }

        public IEnumerable<leaveLog> getLeaveLogsByEmployeeCode(String code) {
            var filter = Builders<leaveLog>.Filter.Eq("employee.code",code);
            var filter2 = Builders<leaveLog>.Filter.Eq("employee.code",code.Trim());
            return _coll.Find(Builders<leaveLog>.Filter.Or(filter,filter2)).ToList();
        }

        public IEnumerable<leaveLog> getLeaveLogsByEmployeeCodeByDate(String code, DateTime date) {
            var filter = Builders<leaveLog>.Filter.Eq("employee.code",code);
            var filter1dot5 = Builders<leaveLog>.Filter.Eq("employee.code",code.Trim());
            var filterEmpCode = Builders<leaveLog>.Filter.Or(filter, filter1dot5);
            var filter1 = Builders<leaveLog>.Filter.Gte("dates.from",date);
            var filter2 = Builders<leaveLog>.Filter.Lte("dates.to",date);
            
            var filterAnd = Builders<leaveLog>.Filter.And(filterEmpCode,filter1,filter2);
            return _coll.Find(filterAnd).ToList();
        }

        public IEnumerable<leaveLog> getLeaveLogsByDate(DateTime date) {
            var filter = Builders<leaveLog>.Filter.Gte("dates.from",date);
            var filter1 = Builders<leaveLog>.Filter.Lte("dates.to",date);
            var filterAnd = Builders<leaveLog>.Filter.And(filter,filter1);
            return _coll.Find(filterAnd).ToList();
        }

        public String uploadAttachment(String Filename, Byte[] bytes) {
            return _upload.upload(new MemoryStream(bytes).ToArray(), @"kmbidevrepo/uploads/employees/documents/" + Filename);
        }

        public Int32 getOverlappedLeaves(String employeeCode, DateTime from, DateTime to) {
            // var tot = 0.0;
           
            // for(DateTime ctr = from; ctr<=to;ctr = ctr.AddDays(1)) {
            //     if(ctr.DayOfWeek != DayOfWeek.Saturday && ctr.DayOfWeek != DayOfWeek.Sunday) {
            //         var hours = 0.0;
                 
            //             hours = (to - ctr).TotalHours;
                   
            //         if (hours > 4) {
            //             tot += (hours - 1) / 8;
            //         } else {
            //             tot += hours / 8;
            //         }
            //     }
            // }

            // if(tot % 1 == 0) {
                var filter = Builders<leaveLog>.Filter.Gte("dates.from",from);
                var filter1 = Builders<leaveLog>.Filter.Lte("dates.from",to);
                
                var filter2 = Builders<leaveLog>.Filter.Gte("dates.to",from);
                var filter3 = Builders<leaveLog>.Filter.Lte("dates.to",to);

                var filter4 = Builders<leaveLog>.Filter.Lte("dates.from",from);
                var filter5 = Builders<leaveLog>.Filter.Gte("dates.to",to);

                
                var filter6 = Builders<leaveLog>.Filter.Eq(l => l.employee.code, employeeCode);
                
                var filterAnd = Builders<leaveLog>.Filter.And(filter,filter1);
                var filterAnd1 = Builders<leaveLog>.Filter.And(filter2,filter3);
                var filterAnd2 = Builders<leaveLog>.Filter.And(filter4,filter5);

                var filterOr = Builders<leaveLog>.Filter.Or(filterAnd,filterAnd1,filterAnd2);

                var filterAnd3 = Builders<leaveLog>.Filter.And(filter6,filterOr);

                return (from x in _coll.Find(filterAnd3).ToList() select x).Count() > 0 ? 1 : 0;
            // } else {
            //     return 0;
            // }
        }

        public IEnumerable<leaveLog> leavesPerBranchPerDateRange(String branchSlug, DateTime from, DateTime to) {
            var filter = Builders<leaveLog>.Filter.Gte("dates.from",from);
            var filter1 = Builders<leaveLog>.Filter.Lte("dates.from",Convert.ToDateTime(to.ToString("MM/dd/yyyy") + " 23:59:59"));
            
            var filter2 = Builders<leaveLog>.Filter.Gte("dates.to",from);
            var filter3 = Builders<leaveLog>.Filter.Lte("dates.to",Convert.ToDateTime(to.ToString("MM/dd/yyyy") + " 23:59:59"));

            var filter4 = Builders<leaveLog>.Filter.Lte("dates.from",from);
            var filter5 = Builders<leaveLog>.Filter.Gte("dates.to",Convert.ToDateTime(to.ToString("MM/dd/yyyy") + " 23:59:59"));


            var _coll2 = _db.GetCollection<users>("users");
            var list = from x in (_coll2.Find(Builders<users>.Filter.Eq(e=> e.employment.branch, branchSlug)).ToList()) select x.employeeCode;
            
            var filter6 = Builders<leaveLog>.Filter.In(l => l.employee.code, list);
            
            var filterAnd = Builders<leaveLog>.Filter.And(filter,filter1);
            var filterAnd1 = Builders<leaveLog>.Filter.And(filter2,filter3);
            var filterAnd2 = Builders<leaveLog>.Filter.And(filter4,filter5);

            var filterOr = Builders<leaveLog>.Filter.Or(filterAnd,filterAnd1,filterAnd2);

            var filterAnd3 = Builders<leaveLog>.Filter.And(filter6,filterOr);

            return from x in _coll.Find(filterAnd3).ToList() select x;
        }

        public void deleteAttachment(String Filename) {
            var filename = Path.GetFileName(Filename);
            _upload.delete("kmbidevrepo/uploads/employees/documents/" + filename);
        }

        public IEnumerable<leaveLog> getLeaveLogsByEmployeeCodeByDateRange(String employeeCode, DateTime from, DateTime to) {
            var filter = Builders<leaveLog>.Filter.Gte("dates.from",from);
            var filter1 = Builders<leaveLog>.Filter.Lte("dates.from",Convert.ToDateTime(to.ToString("MM/dd/yyyy") + " 23:59:59"));
            
            var filter2 = Builders<leaveLog>.Filter.Gte("dates.to",from);
            var filter3 = Builders<leaveLog>.Filter.Lte("dates.to",Convert.ToDateTime(to.ToString("MM/dd/yyyy") + " 23:59:59"));

            var filter4 = Builders<leaveLog>.Filter.Lte("dates.from",from);
            var filter5 = Builders<leaveLog>.Filter.Gte("dates.to",Convert.ToDateTime(to.ToString("MM/dd/yyyy") + " 23:59:59"));


            var _coll2 = _db.GetCollection<users>("users");

            var filterEmp = Builders<users>.Filter.Or(Builders<users>.Filter.Eq(x=>x.employeeCode, employeeCode),Builders<users>.Filter.Eq(x=>x.employeeCode, employeeCode.Trim()));
            var list = from x in (_coll2.Find(filterEmp).ToList()) select x.employeeCode;
            
            var filter6 = Builders<leaveLog>.Filter.In(l => l.employee.code, list);
            
            var filterAnd = Builders<leaveLog>.Filter.And(filter,filter1);
            var filterAnd1 = Builders<leaveLog>.Filter.And(filter2,filter3);
            var filterAnd2 = Builders<leaveLog>.Filter.And(filter4,filter5);

            var filterOr = Builders<leaveLog>.Filter.Or(filterAnd,filterAnd1,filterAnd2);

            var filterAnd3 = Builders<leaveLog>.Filter.And(filter6,filterOr);

            return from x in _coll.Find(filterAnd3).ToList() select x;
        }

        public IEnumerable<leaveLog> leavesPerEmployeePerYear(String employeeSlug, Int32 year)
        {
            return getLeaveLogsByEmployeeCodeByDateRange(employeeSlug, Convert.ToDateTime("1/1/" + year.ToString()), Convert.ToDateTime("12/31/" + year.ToString()).AddHours(23).AddMinutes(59));
        }
        
        public IEnumerable<leaveLog> leavesPerEmployeePerDateRange(String employeeSlug, DateTime from, DateTime to)
        {
            return getLeaveLogsByEmployeeCodeByDateRange(employeeSlug, from, to);
        }

        public IEnumerable<leaveLog> leavesPerDateRange(DateTime from, DateTime to)
        {
            var filter = Builders<leaveLog>.Filter.Gte("dates.from",from);
            var filter1 = Builders<leaveLog>.Filter.Lte("dates.from",Convert.ToDateTime(to.ToString("MM/dd/yyyy") + " 23:59:59"));
            
            var filter2 = Builders<leaveLog>.Filter.Gte("dates.to",from);
            var filter3 = Builders<leaveLog>.Filter.Lte("dates.to",Convert.ToDateTime(to.ToString("MM/dd/yyyy") + " 23:59:59"));

            var filter4 = Builders<leaveLog>.Filter.Lte("dates.from",from);
            var filter5 = Builders<leaveLog>.Filter.Gte("dates.to",Convert.ToDateTime(to.ToString("MM/dd/yyyy") + " 23:59:59"));

            
            var filterAnd = Builders<leaveLog>.Filter.And(filter,filter1);
            var filterAnd1 = Builders<leaveLog>.Filter.And(filter2,filter3);
            var filterAnd2 = Builders<leaveLog>.Filter.And(filter4,filter5);

            var filterOr = Builders<leaveLog>.Filter.Or(filterAnd,filterAnd1,filterAnd2);

            return from x in _coll.Find(filterOr).ToList() select x;
        }
    }
}
